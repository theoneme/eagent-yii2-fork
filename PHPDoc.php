<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 20:57
 */

use common\components\CacheLayer;
use common\components\CurrencyHelper;
use common\components\MediaLayer;
use common\models\User;
use frostealth\yii2\aws\s3\Service;

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

/**
 * Class BaseApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 *
 * @property MediaLayer $mediaLayer
 * @property CacheLayer $cacheLayer
 * @property CurrencyHelper $currencyLayer
 * @property Service $s3
 */
abstract class BaseApplication extends yii\base\Application
{
}

/**
 * Class WebApplication
 * Include only Web application related components here
 *
 * @property \yii\web\User $user
 * @property \frontend\components\CompanyDbManager $companyAuthManager
 */
class WebApplication extends yii\web\Application
{
}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 * @property Service $s3ea
 * @property Service $s3uJobs
 */
class ConsoleApplication extends yii\console\Application
{
}