<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace console\forms\ar;

use common\forms\ar\UserForm as BaseUserForm;

/**
 * Class UserForm
 * @package common\forms\ar
 */
class UserForm extends BaseUserForm
{
    /**
     * @var string
     */
    public $password_hash;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['password_hash', 'string']
        ]);
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        if(!empty($this->password_hash)) {
            $this->_user->password_hash = $this->password_hash;
        }

        return $this->_user->save($withValidation);
    }
}