<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace console\forms\ar;

use common\forms\ar\RequestForm as BaseRequestForm;
use common\models\Request;

/**
 * Class RequestForm
 * @package console\forms\ar
 */
class RequestForm extends BaseRequestForm
{
    /**
     * @var integer
     */
    public $created_at;
    /**
     * @var integer
     */
    public $updated_at;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status', 'type'], 'integer'],
            [['status'], 'required'],
            [['status'], 'in', 'range' => [
                Request::STATUS_DELETED,
                Request::STATUS_ACTIVE,
                Request::STATUS_REQUIRES_MODERATION,
                Request::STATUS_REQUIRES_MODIFICATION
            ]],
            [['user_id'], 'required'],
            [['created_at', 'updated_at'], 'integer']
        ]);
    }
}