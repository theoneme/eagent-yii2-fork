<?php

use common\repositories\sql\CategoryRepository;
use yii\db\Migration;

/**
 * Class m190408_073002_set_attribute_groups
 */
class m190408_073002_set_attribute_groups extends Migration
{
    /**
     * @var CategoryRepository
     */
    private $_categoryRepository;

    /**
     * m190408_073002_set_attribute_groups constructor.
     * @param array $config
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository, array $config = [])
    {
        parent::__construct($config);

        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $flatsCategories = $this->_categoryRepository->joinWith(['translations'], false)->findColumnByCriteria([
            'value' => ['flats', 'flats-kvartiry', 'flats-apartamenty', 'flats-pentkhauzy', 'flats-lofty', 'flats-mezonety', 'flats-studii', 'flats-drugoe'],
            'key' => \common\models\Translation::KEY_SLUG,
            'locale' => 'ru-RU'
        ], 'category.id');
        $this->_categoryRepository->updateManyByCriteria(['id' => $flatsCategories], ['attribute_group_alias' => 'flats', 'form_group' => 'flats']);

        $housesCategories = $this->_categoryRepository->joinWith(['translations'], false)->findColumnByCriteria([
            'value' => ['houses', 'houses-doma', 'houses-dachi', 'houses-taunkhauzy', 'houses-villy', 'houses-dupleksy', 'houses-shale', 'houses-zamki', 'houses-drugoe'],
            'key' => \common\models\Translation::KEY_SLUG,
            'locale' => 'ru-RU'
        ], 'category.id');
        $this->_categoryRepository->updateManyByCriteria(['id' => $housesCategories], ['attribute_group_alias' => 'houses', 'form_group' => 'houses']);

        $commercialCategories = $this->_categoryRepository->joinWith(['translations'], false)->findColumnByCriteria([
            'value' => [
                'commercial-property',
                'commercial-property-ofisnye-zdaniya',
                'commercial-property-ofisnye-pomeshcheniya',
                'commercial-property-torgovye-pomeshcheniya',
                'commercial-property-restorany-bary-kafe',
                'commercial-property-torgovye-tsentry',
                'commercial-property-bazy-otdykha-pansionaty',
                'commercial-property-meditsinskie-tsentry',
                'commercial-property-salony',
                'commercial-property-trenazhernye-zaly',
                'commercial-property-kluby',
                'commercial-property-zapravochnye-stantsii',
                'commercial-property-otdelno-stoyashchie-zdaniya',
                'commercial-property-pomeshcheniya-svobodnogo-naznacheniya',
                'commercial-property-garazhnye-pomeshcheniya',
                'commercial-property-sklady-i-skladskie-kompleksy',
                'commercial-property-dokhodnye-doma',
                'commercial-property-industrialnye-parki',
                'commercial-property-drugoe'
            ],
            'key' => \common\models\Translation::KEY_SLUG,
            'locale' => 'ru-RU'
        ], 'category.id');
        $this->_categoryRepository->updateManyByCriteria(['id' => $commercialCategories], ['attribute_group_alias' => 'commercial-property', 'form_group' => 'commercial-property']);

        $landCategories = $this->_categoryRepository->joinWith(['translations'], false)->findColumnByCriteria([
            'value' => [
                'land',
                'land-uchastki-pod-zhiluyu-zastroyku',
                'land-zemlya-kommercheskogo-naznacheniya',
                'land-zemlya-promyshlennogo-naznacheniya',
                'land-zemlya-selskokhozyaystvennogo-naznacheniya',
                'land-zemlya-rekreatsionnogo-naznacheniya',
                'land-zemlya-prirodno-zapovednogo-naznacheniya',
                'land-ostrova',
                'land-drugoe',
            ],
            'key' => \common\models\Translation::KEY_SLUG,
            'locale' => 'ru-RU'
        ], 'category.id');
        $this->_categoryRepository->updateManyByCriteria(['id' => $landCategories], ['attribute_group_alias' => 'land', 'form_group' => 'land']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->_categoryRepository->updateManyByCriteria([], ['attribute_group_alias' => null, 'form_group' => null]);
    }
}
