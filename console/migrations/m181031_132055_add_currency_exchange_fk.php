<?php

use yii\db\Migration;

/**
 * Class m181031_132055_add_currency_exchange_fk
 */
class m181031_132055_add_currency_exchange_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_currency_exchange_code_from', 'currency_exchange');
        $this->addForeignKey('fk_currency_exchange_code_from', 'currency_exchange', 'code_from', 'currency', 'code', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_currency_exchange_code_from', 'currency_exchange');
        $this->addForeignKey('fk_currency_exchange_code_from', 'currency_exchange', 'code_from', 'currency', 'code', 'CASCADE');
    }
}
