<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m190313_132038_new_attributes
 */
class m190313_132038_new_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'house_number', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Номер дома'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_NONE]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'heating', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Отопление'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'cooling', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Охлаждение и вентиляция'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'terraces', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Количество терасс'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'mansard', 'type' => Attribute::TYPE_BOOLEAN]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Мансарда'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'village_amenity', 'type' => Attribute::TYPE_BOOLEAN]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Преимущества поселка'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'stead_area', 'type' => Attribute::TYPE_BOOLEAN]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Площадь участка'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_RANGE]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'stead_status', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Статус участка'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_NONE]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'stead_amenity', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Преимущества участка'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => [
            'house_number',
            'heating',
            'cooling',
            'terraces',
            'mansard',
            'village_amenity',
            'stead_area',
            'stead_status',
            'stead_amenity',
        ]])->all();

        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
