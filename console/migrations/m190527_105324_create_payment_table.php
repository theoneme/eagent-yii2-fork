<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment`.
 */
class m190527_105324_create_payment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'payment_method_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'total' => $this->integer()->notNull(),
            'currency_code' => $this->string(3)->notNull(),
            'code' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'custom_data' => $this->binary(),
        ]);

        $this->createIndex('payment_type_index', 'payment', 'type');
        $this->createIndex('payment_status_index', 'payment', 'status');

        $this->addForeignKey('fk_payment_user_id', 'payment', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_payment_payment_method_id', 'payment', 'payment_method_id', 'payment_method', 'id', 'CASCADE');
        $this->addForeignKey('fk_payment_currency_code', 'payment', 'currency_code', 'currency', 'code', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment');
    }
}
