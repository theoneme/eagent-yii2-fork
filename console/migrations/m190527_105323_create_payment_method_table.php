<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment_method`.
 */
class m190527_105323_create_payment_method_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment_method', [
            'id' => $this->primaryKey(),
            'alias' => $this->integer()->notNull(),
            'logo' => $this->string(),
            'sort' => $this->integer(),
            'enabled' => $this->boolean(),
        ]);

        $this->createIndex('payment_method_alias_index', 'payment_method', 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_method');
    }
}
