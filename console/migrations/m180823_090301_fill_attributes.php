<?php

use common\models\Attribute;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180823_090301_fill_attributes
 */
class m180823_090301_fill_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'property_area', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Площадь (м²)'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'living_area', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Жилая площадь (м²)'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'kitchen_area', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Площадь кухни (м²)'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'material', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Материал'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'building_type', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Тип дома'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'year_built', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Год постройки'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'is_furnished', 'type' => Attribute::TYPE_BOOLEAN]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'С мебелью'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'are_pets_allowed', 'type' => Attribute::TYPE_BOOLEAN]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Можно с животными'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'bathrooms', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Количество санузлов'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'floor', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Этаж'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'floors', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Этажность'];
        $attribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Attribute::deleteAll();
        Translation::deleteAll(['entity' => Translation::ENTITY_ATTRIBUTE]);
    }
}
