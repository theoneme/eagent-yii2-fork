<?php

use yii\db\Migration;

/**
 * Class m180927_112815_city_is_big
 */
class m180927_112815_city_is_big extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('city', 'is_big', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('city', 'is_big');
    }
}
