<?php

use yii\db\Migration;

/**
 * Class m190405_075608_attribute_groups_table
 */
class m190405_075608_attribute_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('attribute_group', 'entity_id');
        $this->dropIndex('index_attribute_group_type', 'attribute_group');

        $this->addColumn('attribute_group', 'alias', $this->string(35));

        $this->dropIndex('index_attribute_group_entity', 'attribute_group');
        $this->createIndex('index_attribute_group_alias_entity_type', 'attribute_group', ['alias', 'type', 'entity'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_attribute_group_alias_entity_type', 'attribute_group');
        $this->dropColumn('attribute_group', 'alias');

        $this->addColumn('attribute_group', 'entity_id', $this->integer());
        $this->createIndex('index_attribute_group_entity_id', 'attribute_group', 'entity_id');
        $this->createIndex('index_attribute_group_type', 'attribute_group', 'type');
        $this->createIndex('index_attribute_group_entity', 'attribute_group', 'entity');
    }
}
