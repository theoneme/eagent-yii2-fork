<?php

use yii\db\Migration;

/**
 * Class m181108_064240_user_attributes_table
 */
class m181108_064240_user_attributes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_attribute}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'value' => $this->integer()->notNull(),
            'entity_alias' => $this->string(75)->notNull(),
            'value_alias' => $this->string(75)->notNull(),
        ]);
        $this->addForeignKey('fk_user_attribute_attribute_id', 'user_attribute', 'attribute_id', 'attribute', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_attribute_user_id', 'user_attribute', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_attribute_value', 'user_attribute', 'value', 'attribute_value', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_attribute_attribute_id', 'user_attribute');
        $this->dropForeignKey('fk_user_attribute_user_id', 'user_attribute');
        $this->dropForeignKey('fk_user_attribute_value', 'user_attribute');
        
        $this->dropTable('user_attribute');
    }
}
