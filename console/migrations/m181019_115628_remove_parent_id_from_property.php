<?php

use yii\db\Migration;

/**
 * Class m181019_115628_remove_parent_id_from_property
 */
class m181019_115628_remove_parent_id_from_property extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_property_parent_id', 'property');
        $this->dropColumn('property', 'parent_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('property', 'parent_id', $this->integer()->null());
        $this->addForeignKey('fk_property_parent_id', 'property', 'parent_id', 'property', 'id', 'CASCADE');
    }
}
