<?php

use yii\db\Migration;

/**
 * Class m181126_145959_translation_index
 */
class m181126_145959_translation_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('index_translation_entity_key', 'translation');
        $this->execute("ALTER TABLE `translation` ADD INDEX `index_comboned_entity_key_value` (`entity`, `key`, `value`(15));");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_comboned_entity_key_value', 'translation');
        $this->createIndex('index_translation_entity_key', 'translation', ['entity', 'key']);
    }
}
