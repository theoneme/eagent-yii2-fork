<?php

use yii\db\Migration;

/**
 * Class m181203_105456_request_attributes
 */
class m181203_105456_request_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%request_attribute}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer()->notNull(),
            'request_id' => $this->integer()->notNull(),
            'value' => $this->binary(),
        ]);
        $this->addForeignKey('fk_request_attribute_attribute_id', 'request_attribute', 'attribute_id', 'attribute', 'id', 'CASCADE');
        $this->addForeignKey('fk_request_attribute_request_id', 'request_attribute', 'request_id', 'request', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_request_attribute_attribute_id', 'request_attribute');
        $this->dropForeignKey('fk_request_attribute_request_id', 'request_attribute');
        $this->dropTable('request_attribute');
    }
}
