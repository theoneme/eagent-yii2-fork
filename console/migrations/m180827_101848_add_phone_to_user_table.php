<?php

use yii\db\Migration;

/**
 * Class m180827_101848_add_phone_to_user_table
 */
class m180827_101848_add_phone_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'phone', $this->string());
        $this->createIndex('user_phone_index', 'user', 'phone');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'phone');
    }
}
