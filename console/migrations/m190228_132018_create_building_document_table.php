<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_document}}`.
 */
class m190228_132018_create_building_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_document}}', [
            'id' => $this->primaryKey(),
            'building_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'type' => $this->smallInteger(),
            'file' => $this->string(),
        ]);
        $this->addForeignKey('fk_building_document_building_id', 'building_document', 'building_id', 'building', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%building_document}}');
    }
}
