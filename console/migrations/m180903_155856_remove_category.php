<?php

use common\models\Category;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180903_155856_remove_category
 */
class m180903_155856_remove_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $categoryId = Translation::find()->where(['value' => 'rent', 'key' => 'slug', 'entity' => Translation::ENTITY_CATEGORY])->select('entity_id')->scalar();

        if($categoryId) {
            $category = Category::findOne($categoryId);

            if($category !== null) {
                $category->delete();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $root = Category::find()->where(['lvl' => 0])->one();

        $rent = new Category();
        $title = $rent->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Аренда'];
        $slug = $rent->bind('translations');
        $slug->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_SLUG, 'value' => 'rent'];
        $rent->appendTo($root, false);
    }
}
