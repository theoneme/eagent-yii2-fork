<?php

use yii\db\Migration;

/**
 * Class m190401_125945_user_password_null
 */
class m190401_125945_user_password_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'password_hash', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user', 'password_hash', $this->string()->notNull());
    }
}
