<?php

use yii\db\Migration;

/**
 * Class m181106_130546_one_collation_auth
 */
class m181106_130546_one_collation_auth extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('auth_rule', true) === null) {
            die('-------------- Please execute yii migrate --migrationPath=@yii/rbac/migrations ------------');
        }
        $db = Yii::$app->getDb();
        $dbName = null;
        if (preg_match('/' . 'dbname' . '=([^;]*)/', $db->dsn, $match)) {
            $dbName = $match[1];
        }

        if($dbName === null) {
            throw new \yii\base\InvalidConfigException('Db?????');
        }

        Yii::$app->db->createCommand("
            SET foreign_key_checks = 0;
            ALTER TABLE `{$dbName}`.`auth_rule` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
            ALTER TABLE `{$dbName}`.`auth_item_child` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
            ALTER TABLE `{$dbName}`.`auth_item` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
            ALTER TABLE `{$dbName}`.`auth_assignment` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
            SET foreign_key_checks = 1;
        ")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
