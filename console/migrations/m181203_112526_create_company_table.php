<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m181203_112526_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'logo' => $this->string(),
            'banner' => $this->string(),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_company_user_id', 'company', 'user_id', 'user', 'id', 'CASCADE');
        $this->createIndex('company_status_index', 'company', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company');
    }
}
