<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_auth_assignment`.
 */
class m190314_071902_create_company_auth_assignment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company_auth_assignment', [
            'item_name' => $this->string(64)->notNull(),
            'member_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('', 'company_auth_assignment', ['item_name', 'member_id']);
        $this->addForeignKey('fk_company_aa_item_name', 'company_auth_assignment', 'item_name', 'auth_item', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_company_aa_member_id', 'company_auth_assignment', 'member_id', 'company_member', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company_auth_assignment');
    }
}
