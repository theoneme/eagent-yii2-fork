<?php

use yii\db\Migration;

/**
 * Class m190207_112315_attribute_group_adjustments
 */
class m190207_112315_attribute_group_adjustments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('attribute_group', 'entity_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('attribute_group', 'entity_id', $this->integer()->notNull());
    }
}
