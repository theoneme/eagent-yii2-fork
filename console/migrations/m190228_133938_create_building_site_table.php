<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_site}}`.
 */
class m190228_133938_create_building_site_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_site}}', [
            'id' => $this->primaryKey(),
            'building_id' => $this->integer()->notNull(),
            'type' => $this->smallInteger(),
            'name' => $this->string(),
            'transport' => $this->smallInteger(),
            'distance' => $this->integer(),
            'time' => $this->smallInteger(),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
        ]);
        $this->addForeignKey('fk_building_site_building_id', 'building_site', 'building_id', 'building', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%building_site}}');
    }
}
