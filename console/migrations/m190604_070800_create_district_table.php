<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Handles the creation of table `district`.
 */
class m190604_070800_create_district_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('district', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'polygon' => $this->getDb()->getSchema()->createColumnSchemaBuilder('polygon'),
        ]);

        $this->addForeignKey('fk_district_city_id', 'district', 'city_id', 'city', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('district');
    }
}
