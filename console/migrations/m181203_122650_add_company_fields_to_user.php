<?php

use yii\db\Migration;

/**
 * Class m181203_122650_add_company_fields_to_user
 */
class m181203_122650_add_company_fields_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'company_user_id', $this->integer());
        $this->addColumn('user', 'is_company', $this->boolean()->defaultValue(false));

        $this->addForeignKey('fk_user_company_user_id', 'user', 'company_user_id', 'user', 'id', 'SET NULL');
        $this->createIndex('user_is_company_index', 'user', 'is_company');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_company_user_id', 'user');
        $this->dropIndex('user_is_company_index', 'user');

        $this->dropColumn('user', 'company_user_id');
        $this->dropColumn('user', 'is_company');
    }
}
