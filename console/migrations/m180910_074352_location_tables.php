<?php

use yii\db\Migration;

/**
 * Class m180910_074352_location_tables
 */
class m180910_074352_location_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%country}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(2)->notNull(),
        ]);
        $this->createIndex('index_code', 'country', 'code');

        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(2)->notNull(),
            'country_id' => $this->integer()->notNull(),
        ]);
        $this->createIndex('index_code', 'region', 'code');
        $this->addForeignKey('fk_region_country_id', 'region', 'country_id', 'country', 'id', 'CASCADE');

        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'region_id' => $this->integer()->notNull(),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
        ]);
        $this->addForeignKey('fk_city_country_id', 'city', 'country_id', 'country', 'id', 'CASCADE');
        $this->addForeignKey('fk_city_region_id', 'city', 'region_id', 'region', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('city');
        $this->dropTable('region');
        $this->dropTable('country');
    }
}
