<?php

use yii\db\Migration;

/**
 * Class m181225_133723_db_optimizations
 */
class m181225_133723_db_optimizations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('index_property_status', 'property');
        $this->dropIndex('index_property_locale', 'property');
        $this->dropIndex('property_type_index', 'property');

//        $this->createIndex('index_property_type_status', 'property', ['type', 'locale']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->dropIndex('index_property_type_status', 'property');

        $this->createIndex('index_property_status', 'property', 'status');
        $this->createIndex('index_property_locale', 'property', 'locale');
        $this->createIndex('property_type_index', 'property', 'type');
    }
}
