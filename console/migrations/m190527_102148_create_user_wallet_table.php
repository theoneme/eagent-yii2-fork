<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_wallet`.
 */
class m190527_102148_create_user_wallet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_wallet', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'currency_code' => $this->string(3)->notNull(),
            'balance' => $this->integer()->defaultValue(0)
        ]);
        $this->execute("ALTER TABLE user_wallet AUTO_INCREMENT=100000000;");
        $this->addForeignKey('fk_user_wallet_user_id', 'user_wallet', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_user_wallet_currency_code', 'user_wallet', 'currency_code', 'currency', 'code', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_wallet');
    }
}
