<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use yii\db\Migration;

/**
 * Class m180918_114925_attribute_filter_table
 */
class m180918_114925_attribute_filter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attribute_filter}}', [
            'attribute_id' => $this->integer()->notNull()->unique(),
            'type' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('fk_attribute_filter_attribute', 'attribute_filter', 'attribute_id', 'attribute', 'id', 'CASCADE');

        $numberAttributes = Attribute::find()->select('id')->where(['type' => Attribute::TYPE_NUMBER])->all();
        foreach($numberAttributes as $attribute) {
            $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_RANGE]);
            $filterAttribute->save();
        }
        $textAttributes = Attribute::find()->select('id')->where(['type' => [Attribute::TYPE_STRING, Attribute::TYPE_DROPDOWN]])->all();
        foreach($textAttributes as $attribute) {
            $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
            $filterAttribute->save();
        }
        $textAttributes = Attribute::find()->select('id')->where(['type' => Attribute::TYPE_BOOLEAN])->all();
        foreach($textAttributes as $attribute) {
            $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_CHECKBOX]);
            $filterAttribute->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_attribute_filter_attribute', 'attribute_filter');
        $this->dropTable('attribute_filter');
    }
}
