<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m181120_132956_amenities_attribute
 */
class m181120_132956_amenities_attribute extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'property_amenitie', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Удобства'];
        $attribute->save();

        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'building_amenitie', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Удобства'];
        $attribute->save();

        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => ['property_amenitie', 'building_amenitie']])->all();
        foreach ($attributes as $attribute) {
            $attributeFilter = AttributeFilter::find()->where(['attribute_id' => $attribute->id])->one();
            if($attributeFilter !== null) {
                $attributeFilter->delete();
            }

            $attribute->delete();
        }
    }
}
