<?php

use yii\db\Migration;

/**
 * Class m181011_091849_report_table_changes
 */
class m181011_091849_report_table_changes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('report', 'to_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('report', 'to_id', $this->integer()->notNull());
    }
}
