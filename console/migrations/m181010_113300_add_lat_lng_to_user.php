<?php

use yii\db\Migration;

/**
 * Class m181010_113300_add_lat_lng_to_user
 */
class m181010_113300_add_lat_lng_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'lat', $this->decimal(10, 8)->null());
        $this->addColumn('user', 'lng', $this->decimal(11, 8)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'lat');
        $this->dropColumn('user', 'lng');
    }
}
