<?php

use yii\db\Migration;

/**
 * Class m181031_142554_exchange_index
 */
class m181031_142554_exchange_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk_currency_exchange_code_from', 'currency_exchange');
        $this->dropForeignKey('fk_currency_exchange_code_to', 'currency_exchange');
//        $this->dropPrimaryKey('pk_code_from_code_to', 'currency_exchange');
        $this->dropIndex('fk_currency_exchange_code_to', 'currency_exchange');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addForeignKey('fk_currency_exchange_code_from', 'currency_exchange', 'code_from', 'currency', 'code', 'CASCADE');
        $this->addForeignKey('fk_currency_exchange_code_to', 'currency_exchange', 'code_to', 'currency', 'code', 'CASCADE');
//        $this->addPrimaryKey('pk_code_from_code_to', 'currency_exchange', ['code_from', 'code_to']);
    }
}
