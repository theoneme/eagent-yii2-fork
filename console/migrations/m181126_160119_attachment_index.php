<?php

use yii\db\Migration;

/**
 * Class m181126_160119_attachment_index
 */
class m181126_160119_attachment_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('index_attachment_entity', 'attachment');
        $this->dropIndex('index_attachment_entity_id', 'attachment');
        $this->createIndex('index_attachment_entity_entity_id', 'attachment', ['entity', 'entity_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_attachment_entity_entity_id', 'attachment');
        $this->createIndex('index_attachment_entity', 'attachment', 'entity');
        $this->createIndex('index_attachment_entity_id', 'attachment', 'entity_id');
    }
}
