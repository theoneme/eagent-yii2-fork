<?php

use yii\db\Migration;

/**
 * Class m181031_134830_property_index
 */
class m181031_134830_property_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_composite', 'property', ['locale', 'type', 'status', 'category_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_composite', 'property');
    }
}
