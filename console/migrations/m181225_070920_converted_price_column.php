<?php

use yii\db\Migration;

/**
 * Class m181225_070920_converted_price_column
 */
class m181225_070920_converted_price_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'default_price', $this->integer()->null());
        $this->createIndex('index_property_default_price', 'property', 'default_price');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property', 'default_price');
    }
}
