<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m181107_145725_new_building_attributes
 */
class m181107_145725_new_building_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'building_renovation_year', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Год капитального ремонта'];
        $attribute->save();

        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_INPUT]);
        $filterAttribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => 'building_renovation_year'])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();

            $attributeFilter = AttributeFilter::find()->where(['attribute_id' => $attribute->id])->one();
            if ($attributeFilter !== null) {
                $attributeFilter->delete();
            }
        }
    }
}
