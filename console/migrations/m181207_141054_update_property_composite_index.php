<?php

use yii\db\Migration;

/**
 * Class m181207_141054_update_property_composite_index
 */
class m181207_141054_update_property_composite_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('index_composite', 'property');
        $this->createIndex('index_composite', 'property', ['category_id', 'status', 'type']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_composite', 'property');
        $this->createIndex('index_composite', 'property', ['category_id', 'status', 'type', 'locale']);
    }
}
