<?php

use yii\db\Migration;

/**
 * Class m181009_145015_report_table
 */
class m181009_145015_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%report}}', [
            'id' => $this->primaryKey(),
            'from_id' => $this->integer()->notNull(),
            'to_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
            'custom_data' => $this->binary()
        ]);

        $this->addForeignKey('fk_report_from_id', 'report', 'from_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_report_to_id', 'report', 'to_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_report_from_id', 'report');
        $this->dropForeignKey('fk_report_to_id', 'report');

        $this->dropTable('report');
    }
}
