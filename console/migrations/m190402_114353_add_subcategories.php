<?php

use common\forms\ar\CategoryForm;
use common\helpers\UtilityHelper;
use common\models\Category;
use common\models\Translation;
use common\services\entities\CategoryService;
use yii\db\Migration;

/**
 * Class m190402_114353_add_subcategories
 */
class m190402_114353_add_subcategories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = [
            'flats' => [
                ['title' => 'Квартиры'],
                ['title' => 'Апартаменты'],
                ['title' => 'Пентхаузы'],
                ['title' => 'Лофты'],
                ['title' => 'Мезонеты'],
                ['title' => 'Студии'],
                ['title' => 'Другое'],
            ],
            'houses' => [
                ['title' => 'Дома'],
                ['title' => 'Дачи'],
                ['title' => 'Таунхаузы'],
                ['title' => 'Виллы'],
                ['title' => 'Дуплексы'],
                ['title' => 'Шале'],
                ['title' => 'Замки'],
                ['title' => 'Другое'],
            ],
            'commercial-property' => [
                ['title' => 'Офисные здания'],
                ['title' => 'Офисные помещения'],
                ['title' => 'Торговые помещения'],
                ['title' => 'Рестораны / Бары / Кафе'],
                ['title' => 'Торговые центры'],
                ['title' => 'Базы отдыха / Пансионаты'],
                ['title' => 'Медицинские центры'],
                ['title' => 'Салоны'],
                ['title' => 'Тренажерные залы'],
                ['title' => 'Клубы'],
                ['title' => 'Заправочные станции'],
                ['title' => 'Отдельно стоящие здания'],
                ['title' => 'Помещения свободного назначения'],
                ['title' => 'Гаражные помещения'],
                ['title' => 'Склады и складские комплексы'],
                ['title' => 'Доходные дома'],
                ['title' => 'Индустриальные парки'],
                ['title' => 'Другое'],
            ],
            'land' => [
                ['title' => 'Участки под жилую застройку'],
                ['title' => 'Земля коммерческого назначения'],
                ['title' => 'Земля промышленного назначения'],
                ['title' => 'Земля сельскохозяйственного назначения'],
                ['title' => 'Земля рекреационного назначения '],
                ['title' => 'Земля природно-заповедного назначения'],
                ['title' => 'Острова'],
                ['title' => 'Другое'],
            ],
        ];
        foreach ($categories as $parent => $children) {
            $parentData = $categoryService->getOne(['category_translations.value' => $parent, 'category_translations.key' => 'slug']);
            if ($parentData) {
                foreach ($children as $child) {
                    $form = new CategoryForm(['_category' => new Category(), 'parent_id' => $parentData['id']]);
                    $form->load([
                        'MetaForm' => [
                            'ru-RU' => [
                                Translation::KEY_TITLE => $child['title'],
                                Translation::KEY_SLUG => $parentData['slug'] . "-" . UtilityHelper::transliterate($child['title']),
                            ]
                        ]
                    ]);
                    $form->bindData();
                    $form->save(false);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('category', ['>=', 'lvl', 2]);
    }
}
