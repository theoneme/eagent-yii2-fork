<?php

use yii\db\Migration;

/**
 * Class m190408_102059_zip_fix
 */
class m190408_102059_zip_fix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('property', 'zip', $this->string(10)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('property', 'zip', $this->string(5)->null());
    }
}
