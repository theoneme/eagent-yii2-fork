<?php

use yii\db\Migration;

/**
 * Class m181127_083054_translation_index
 */
class m181127_083054_translation_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('index_translation_value', 'translation');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("ALTER TABLE translation ADD FULLTEXT INDEX index_translation_value (value ASC)");
    }
}
