<?php

use yii\db\Migration;

/**
 * Class m181126_162111_property_attribute_index
 */
class m181126_162111_property_attribute_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('index_property_attribute_entity_alias', 'property_attribute');
        $this->dropIndex('index_property_attribute_value_alias', 'property_attribute');
        $this->dropIndex('index_property_attribute_value_number', 'property_attribute');

        $this->createIndex('index_combined_property_attribute', 'property_attribute', ['property_id', 'entity_alias', 'value_alias']);
        $this->createIndex('index_combined_property_attribute_two', 'property_attribute', ['property_id', 'entity_alias', 'value_number']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_combined_property_attribute', 'property_attribute');
        $this->dropIndex('index_combined_property_attribute_two', 'property_attribute');

        $this->createIndex('index_property_attribute_entity_alias', 'property_attribute', 'entity_alias');
        $this->createIndex('index_property_attribute_value_alias', 'property_attribute', 'value_alias');
        $this->createIndex('index_property_attribute_value_number', 'property_attribute', 'value_number');
    }
}
