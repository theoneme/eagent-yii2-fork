<?php

use yii\db\Migration;

/**
 * Class m180905_115233_add_type_to_property
 */
class m180905_115233_add_type_to_property extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'type', $this->integer());
        $this->createIndex('property_type_index', 'property', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property', 'type');
    }
}