<?php

use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Category;
use common\repositories\sql\CategoryRepository;
use common\services\seo\PropertySeoService;
use yii\db\Migration;

/**
 * Class m190409_162619_add_seo_template_to_category_custom_data
 */
class m190409_162619_add_seo_template_to_category_custom_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = Yii::$container->get(CategoryRepositoryInterface::class);
        $categories = [
            'flats' => PropertySeoService::TEMPLATE_APARTMENT,
            'houses' => PropertySeoService::TEMPLATE_HOUSE,
            'commercial-property' => PropertySeoService::TEMPLATE_COMMERCIAL,
            'land' => PropertySeoService::TEMPLATE_LAND,
        ];
        foreach ($categories as $category => $template) {
            /** @var Category $parentData */
            $parent = $categoryRepository->joinWith(['translations'])->findOneByCriteria(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
            $parentAndChildren = $categoryRepository->findManyByCriteria(['and', ['>=', 'lft', $parent['lft']], ['<=', 'rgt', $parent['rgt']], ['root' => $parent['root']]]);
            foreach ($parentAndChildren as $item) {
                $customData = $item['customDataArray'];
                $customData = array_merge($customData, ['seoTemplate' => $template]);
                $categoryRepository->updateOneByCriteria(['id' => $item['id']], ['custom_data' => json_encode($customData)], CategoryRepository::MODE_LIGHT);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = Yii::$container->get(CategoryRepositoryInterface::class);
        $categories = [
            'flats',
            'houses',
            'commercial-property',
            'land',
        ];
        foreach ($categories as $category) {
            /** @var Category $parentData */
            $parent = $categoryRepository->joinWith(['translations'])->findOneByCriteria(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
            $parentAndChildren = $categoryRepository->findManyByCriteria(['and', ['>=', 'lft', $parent['lft']], ['<=', 'rgt', $parent['rgt']], ['root' => $parent['root']]]);
            foreach ($parentAndChildren as $item) {
                $customData = $item['customDataArray'];
                unset($customData['seoTemplate']);
                $categoryRepository->updateOneByCriteria(['id' => $item['id']], ['custom_data' => json_encode($customData)], CategoryRepository::MODE_LIGHT);
            }
        }
    }
}
