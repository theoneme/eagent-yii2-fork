<?php

use yii\db\Migration;

/**
 * Class m181126_124820_translaton_index
 */
class m181126_124820_translaton_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE translation ADD FULLTEXT INDEX index_translation_value (value)");
        $this->createIndex('index_translation_entity_key', 'translation', ['entity', 'key']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_translation_value', 'translation');
        $this->dropIndex('index_translation_entity_key', 'translation');
    }
}
