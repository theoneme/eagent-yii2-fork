<?php

use yii\db\Migration;

/**
 * Class m180904_115432_add_origin_to_import
 */
class m180904_115432_add_origin_to_import extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('import', 'origin', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('import', 'origin');
    }
}
