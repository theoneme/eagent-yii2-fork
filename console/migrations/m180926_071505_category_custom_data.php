<?php

use common\models\Category;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180926_071505_category_custom_data
 */
class m180926_071505_category_custom_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('category', 'custom_data', $this->binary()->null());

        $flatsCategoryId = Translation::find()->where(['value' => 'flats', 'key' => \common\models\Translation::KEY_SLUG])->select('entity_id')->scalar();
        if($flatsCategoryId) {
            Category::updateAll(['custom_data' => json_encode([
                'circle' => 'red'
            ])], ['id' => $flatsCategoryId]);
        }

        $housesCategoryId = Translation::find()->where(['value' => 'houses', 'key' => \common\models\Translation::KEY_SLUG])->select('entity_id')->scalar();
        if($housesCategoryId) {
            Category::updateAll(['custom_data' => json_encode([
                'circle' => 'magenta'
            ])], ['id' => $housesCategoryId]);
        }

        $landCategoryId = Translation::find()->where(['value' => 'land', 'key' => \common\models\Translation::KEY_SLUG])->select('entity_id')->scalar();
        if($landCategoryId) {
            Category::updateAll(['custom_data' => json_encode([
                'circle' => 'blue'
            ])], ['id' => $landCategoryId]);
        }

        $commercialCategoryId = Translation::find()->where(['value' => 'commercial-property', 'key' => \common\models\Translation::KEY_SLUG])->select('entity_id')->scalar();
        if($commercialCategoryId) {
            Category::updateAll(['custom_data' => json_encode([
                'circle' => 'yellow'
            ])], ['id' => $commercialCategoryId]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'custom_data');
    }
}
