<?php

use yii\db\Migration;

/**
 * Handles the creation of table `building_attribute`.
 */
class m181018_090031_create_building_attribute_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_attribute}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer()->notNull(),
            'building_id' => $this->integer()->notNull(),
            'value' => $this->integer()->notNull(),
            'entity_alias' => $this->string(75)->notNull(),
            'value_alias' => $this->string(75)->notNull(),
            'value_number' => $this->float()->null(),
        ]);
        $this->addForeignKey('fk_building_attribute_attribute_id', 'building_attribute', 'attribute_id', 'attribute', 'id', 'CASCADE');
        $this->addForeignKey('fk_building_attribute_building_id', 'building_attribute', 'building_id', 'building', 'id', 'CASCADE');
        $this->addForeignKey('fk_building_attribute_value', 'building_attribute', 'value', 'attribute_value', 'id', 'CASCADE');
        $this->createIndex('index_building_attribute_entity_alias', 'building_attribute', 'entity_alias');
        $this->createIndex('index_building_attribute_value_alias', 'building_attribute', 'value_alias');
        $this->createIndex('index_building_attribute_value_number', 'building_attribute', 'value_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('building_attribute');
    }
}
