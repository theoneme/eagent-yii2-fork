<?php

use yii\db\Migration;

/**
 * Class m181213_073903_location_slugs
 */
class m181213_073903_location_slugs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('country', 'slug', $this->string(55)->null());
        $this->addColumn('region', 'slug', $this->string(55)->null());
        $this->addColumn('city', 'slug', $this->string(55)->null());

        $this->createIndex('index_slug', 'country', 'slug', true);
        $this->createIndex('index_slug', 'region', 'slug', true);
        $this->createIndex('index_slug', 'city', 'slug', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('country', 'slug');
        $this->dropColumn('region', 'slug');
        $this->dropColumn('city', 'slug');
    }
}
