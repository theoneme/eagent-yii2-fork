<?php

use yii\db\Migration;

/**
 * Class m181018_114743_user_role_field
 */
class m181018_114743_user_role_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'role', $this->integer()->null()->defaultValue(0));

        $this->createIndex('index_user_role', 'user', 'role');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'role');
    }
}
