<?php

use yii\db\Migration;

/**
 * Handles the creation of table `building`.
 */
class m181018_085441_create_building_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(100)->notNull()->unique(),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
            'status' => $this->integer()
        ]);

        $this->createIndex('index_building_status', 'building', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('building');
    }
}
