<?php

use common\models\Attribute;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180903_122717_add_attributes
 */
class m180903_122717_add_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'layout', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Планировка'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'bathroom_type', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Санузел'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'condition', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Состояние'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'balconies', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Количество балконов'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'property_type', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Тип собственности'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'loggias', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Количество лоджий'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'internet', 'type' => Attribute::TYPE_BOOLEAN]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Интернет'];
        $attribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => ['layout', 'bathroom_type', 'condition', 'balconies', 'property_type', 'loggias', 'internet']])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
