<?php

use yii\db\Migration;

/**
 * Class m190211_081841_building_timestamps
 */
class m190211_081841_building_timestamps extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('building', 'created_at', $this->integer()->null());
        $this->addColumn('building', 'updated_at', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('building', 'created_at');
        $this->dropColumn('building', 'updated_at');
    }
}
