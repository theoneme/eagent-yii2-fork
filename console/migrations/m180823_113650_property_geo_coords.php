<?php

use yii\db\Migration;

/**
 * Class m180823_113650_property_geo_coords
 */
class m180823_113650_property_geo_coords extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'lat', $this->decimal(10, 8)->null());
        $this->addColumn('property', 'lng', $this->decimal(11, 8)->null());
        $this->addColumn('property', 'zip', $this->string(5)->null());
        $this->addColumn('property', 'address', $this->string(125)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property', 'lat');
        $this->dropColumn('property', 'lng');
        $this->dropColumn('property', 'zip');
        $this->dropColumn('property', 'address');
    }
}
