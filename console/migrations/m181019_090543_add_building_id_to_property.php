<?php

use yii\db\Migration;

/**
 * Class m181019_090543_add_building_id_to_property
 */
class m181019_090543_add_building_id_to_property extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'building_id', $this->integer()->null());
        $this->addForeignKey('fk_property_building_id', 'property', 'building_id', 'building', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_property_building_id', 'property');
        $this->dropColumn('property', 'building_id');
    }
}
