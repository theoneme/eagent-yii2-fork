<?php

use yii\db\Migration;

/**
 * Class m190328_115656_default_values
 */
class m190328_115656_default_values extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('building', 'slug', $this->string(100)->null());
        $this->alterColumn('property', 'slug', $this->string(100)->null());
        $this->alterColumn('user', 'auth_key', $this->string(32)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('building', 'slug', $this->string(100));
        $this->alterColumn('property', 'slug', $this->string(100));
        $this->alterColumn('user', 'auth_key', $this->string(32));
    }
}
