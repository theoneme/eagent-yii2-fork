<?php

use yii\db\Migration;

/**
 * Class m190124_135205_add_type_to_request
 */
class m190124_135205_add_type_to_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('request', 'type', $this->integer()->defaultValue(0));
        $this->createIndex('request_type_index', 'request', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('request', 'type');
    }
}
