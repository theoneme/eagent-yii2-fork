<?php

use yii\db\Migration;

/**
 * Class m190402_155943_flag_table_alter
 */
class m190402_155943_flag_table_alter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('flag', 'entity', $this->string(15)->null());
        $this->alterColumn('flag', 'value', $this->string(40)->null());
        $this->alterColumn('flag', 'type', $this->string(20)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('flag', 'entity', $this->string(255)->null());
        $this->alterColumn('flag', 'value', $this->string(255)->null());
        $this->alterColumn('flag', 'type', $this->string(255)->null());
    }
}
