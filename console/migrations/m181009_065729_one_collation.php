<?php

use yii\db\Migration;

/**
 * Class m181009_065729_one_collation
 */
class m181009_065729_one_collation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $db = Yii::$app->getDb();
        $dbName = null;
        if (preg_match('/' . 'dbname' . '=([^;]*)/', $db->dsn, $match)) {
            $dbName = $match[1];
        }

        if($dbName === null) {
            throw new \yii\base\InvalidConfigException('Db?????');
        }

        Yii::$app->db->createCommand("
SET foreign_key_checks = 0;
ALTER TABLE `{$dbName}`.`attachment` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`address_translation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`attribute` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`attribute_filter` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`attribute_value` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`category` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`category_attribute` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`city` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`country` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`currency` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`currency_exchange` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`flag` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`import` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`migration` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`property` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`property_attribute` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`region` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`request` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`translation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `{$dbName}`.`user` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
SET foreign_key_checks = 1;
")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
