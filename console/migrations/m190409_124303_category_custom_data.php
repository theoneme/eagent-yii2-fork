<?php

use common\interfaces\repositories\CategoryRepositoryInterface;
use common\repositories\sql\CategoryRepository;
use yii\db\Migration;

/**
 * Class m190409_124303_category_custom_data
 */
class m190409_124303_category_custom_data extends Migration
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;

    /**
     * m190409_124303_category_custom_data constructor.
     * @param CategoryRepository $categoryRepository
     * @param array $config
     */
    public function __construct(CategoryRepository $categoryRepository, array $config = [])
    {
        parent::__construct($config);

        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $flatsParent = $this->_categoryRepository->findOneByCriteria(['id' => 2]);
        $parentCustomData = $flatsParent['customDataArray'];
        $parentCustomData = array_merge($parentCustomData, ['createBuilding' => true]);
        $this->_categoryRepository->updateOneByCriteria(['id' => 2], ['custom_data' => json_encode($parentCustomData)], CategoryRepository::MODE_LIGHT);

        $this->_categoryRepository->updateManyByCriteria(['and', ['>', 'lft', $flatsParent['lft']], ['<', 'rgt', $flatsParent['rgt']], ['root' => $flatsParent['root']]], [
            'custom_data' => json_encode(['createBuilding' => true])
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $flatsParent = $this->_categoryRepository->findOneByCriteria(['id' => 2]);
        $this->_categoryRepository->updateOneByCriteria(['id' => 2], ['custom_data' => json_encode(['circle' => 'red'])], CategoryRepository::MODE_LIGHT);

        $this->_categoryRepository->updateManyByCriteria(['and', ['>', 'lft', $flatsParent['lft']], ['<', 'rgt', $flatsParent['rgt']], ['root' => $flatsParent['root']]], [
            'custom_data' => null
        ]);
    }
}
