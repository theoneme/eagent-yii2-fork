<?php

use yii\db\Migration;

/**
 * Class m190320_112744_add_ads_allowed
 */
class m190320_112744_add_ads_allowed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'ads_allowed', $this->boolean()->defaultValue(true));
        $this->addColumn('property', 'ads_allowed_partners', $this->boolean()->defaultValue(true));

        $this->createIndex('property_ads_allowed', 'property', 'ads_allowed');
        $this->createIndex('property_ads_allowed_partners', 'property', 'ads_allowed_partners');

        $this->addColumn('building', 'ads_allowed', $this->boolean()->defaultValue(true));
        $this->addColumn('building', 'ads_allowed_partners', $this->boolean()->defaultValue(true));

        $this->createIndex('building_ads_allowed', 'building', 'ads_allowed');
        $this->createIndex('building_ads_allowed_partners', 'building', 'ads_allowed_partners');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property', 'ads_allowed');
        $this->dropColumn('property', 'ads_allowed_partners');

        $this->dropColumn('building', 'ads_allowed');
        $this->dropColumn('building', 'ads_allowed_partners');
    }
}
