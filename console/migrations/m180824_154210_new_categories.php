<?php

use common\models\Category;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180824_154210_new_categories
 */
class m180824_154210_new_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $root = Category::find()->where(['lvl' => 0])->one();

        $houses = new Category();
        $title = $houses->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Дома'];
        $slug = $houses->bind('translations');
        $slug->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_SLUG, 'value' => 'houses'];
        $houses->appendTo($root, false);

        $land = new Category();
        $title = $land->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Земля'];
        $slug = $land->bind('translations');
        $slug->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_SLUG, 'value' => 'land'];
        $land->appendTo($root, false);

        $rent = new Category();
        $title = $rent->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Аренда'];
        $slug = $rent->bind('translations');
        $slug->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_SLUG, 'value' => 'rent'];
        $rent->appendTo($root, false);

        $commercialProperty = new Category();
        $title = $commercialProperty->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Коммерческая недвижимость'];
        $slug = $commercialProperty->bind('translations');
        $slug->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_SLUG, 'value' => 'commercial-property'];
        $commercialProperty->appendTo($root, false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $categories = Category::find()->joinWith(['translations'])->where(['category_translations.value' => ['commercial-property', 'rent', 'land', 'houses']])->all();

        foreach($categories as $category) {
            $category->delete();
        }
    }
}
