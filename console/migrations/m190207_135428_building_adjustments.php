<?php

use common\models\Building;
use yii\db\Migration;

/**
 * Class m190207_135428_building_adjustments
 */
class m190207_135428_building_adjustments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('building', 'status', $this->integer()->defaultValue(Building::STATUS_REQUIRES_MODERATION));
        $this->addColumn('building', 'type', $this->integer()->defaultValue(Building::TYPE_DEFAULT));
        $this->addColumn('building', 'user_id', $this->integer()->null());
        $this->addForeignKey('fk_building_user_id_user', 'building', 'user_id', 'user', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('building', 'status', $this->integer()->null());
        $this->dropColumn('building', 'type');
        $this->dropForeignKey('fk_building_user_id_user', 'building');
        $this->dropColumn('building', 'user_id');
    }
}
