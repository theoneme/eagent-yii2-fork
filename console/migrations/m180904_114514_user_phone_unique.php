<?php

use yii\db\Migration;

/**
 * Class m180904_114514_user_phone_unique
 */
class m180904_114514_user_phone_unique extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('user_phone_index', 'user');
        $this->createIndex('user_phone_index', 'user', 'phone', true);
        $this->alterColumn('user', 'email', $this->string()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('user_phone_index', 'user');
        $this->createIndex('user_phone_index', 'user', 'phone');
        $this->alterColumn('user', 'email', $this->string()->notNull());
    }
}
