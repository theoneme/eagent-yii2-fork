<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use yii\db\Migration;

/**
 * Class m181106_210800_new_attribute_filter
 */
class m181106_210800_new_attribute_filter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $saleTypeAttribute = Attribute::find()->select('id')->where(['alias' => 'sale_type'])->one();
        if ($saleTypeAttribute) {
            $filterAttribute = new AttributeFilter(['attribute_id' => $saleTypeAttribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
            $filterAttribute->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $saleTypeAttribute = Attribute::find()->select('id')->where(['alias' => 'sale_type'])->one();
        if ($saleTypeAttribute) {
            $attributeFilter = AttributeFilter::find()->where(['attribute_id' => $saleTypeAttribute->id])->one();
            if($attributeFilter !== null) {
                $attributeFilter->delete();
            }
        }
    }
}
