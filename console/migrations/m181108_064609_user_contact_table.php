<?php

use yii\db\Migration;

/**
 * Class m181108_064609_user_contact_table
 */
class m181108_064609_user_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_contact}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'value' => $this->string(75)->notNull(),
        ]);
        $this->addForeignKey('fk_user_contact_user_id', 'user_contact', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_contact_user_id', 'user_contact');
        $this->dropTable('user_contact');
    }
}
