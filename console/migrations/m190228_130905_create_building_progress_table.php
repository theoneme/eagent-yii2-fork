<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_progress}}`.
 */
class m190228_130905_create_building_progress_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_progress}}', [
            'id' => $this->primaryKey(),
            'building_id' => $this->integer()->notNull(),
            'year' => $this->smallInteger(4),
            'quarter' => $this->tinyInteger(1),
        ]);
        $this->addForeignKey('fk_building_progress_building_id', 'building_progress', 'building_id', 'building', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%building_progress}}');
    }
}
