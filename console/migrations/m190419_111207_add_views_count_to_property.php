<?php

use yii\db\Migration;

/**
 * Class m190419_111207_add_views_count_to_property
 */
class m190419_111207_add_views_count_to_property extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'views_count', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property', 'views_count');
    }
}
