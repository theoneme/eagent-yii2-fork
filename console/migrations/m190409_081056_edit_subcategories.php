<?php

use common\forms\ar\CategoryForm;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Category;
use common\models\Translation;
use common\repositories\sql\CategoryRepository;
use common\services\entities\CategoryService;
use yii\db\Migration;

/**
 * Class m190409_081056_edit_subcategories
 */
class m190409_081056_edit_subcategories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = Yii::$container->get(CategoryRepositoryInterface::class);
        $categories = [
            'Офисные здания' => ['title' => 'Офисные здания / помещения'],
            'Офисные помещения' => 'delete',
            'Торговые помещения' => ['title' => 'Торговые помещения / центры'],
            'Торговые центры' => 'delete',
            'Медицинские центры' => ['title' => 'Медицинские центры / салоны'],
            'Салоны' => 'delete',
            'Клубы' => 'delete',
            'Гаражные помещения' => 'delete',
            'Доходные дома' => ['title' => 'Доходная недвижимость'],
            'Индустриальные парки' => 'delete',
            'Земля природно-заповедного назначения' => 'delete',
            'Острова' => 'delete',
            'Земля рекреационного назначения ' => ['title' => 'Земля рекреационного назначения'],
        ];
        foreach ($categories as $title => $data) {
            $category = $categoryService->getOne(['category_translations.value' => $title, 'category_translations.key' => Translation::KEY_TITLE]);
            if ($category) {
                if ($data === 'delete') {
                    $categoryService->delete(['id' => $category['id']]);
                } else {
                    $form = new CategoryForm();
                    $form->_category = $categoryRepository->findOneByCriteria(['category.id' => $category['id']]);
                    $form->prepareUpdate($category);
                    $form->load([
                        'MetaForm' => [
                            'ru-RU' => [
                                Translation::KEY_TITLE => $data['title'],
                                Translation::KEY_SLUG => $category['slug'],
                            ]
                        ]
                    ]);
                    $form->bindData();
                    $form->save();
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = Yii::$container->get(CategoryRepositoryInterface::class);
        $categories = [
            'Офисные здания / помещения' => ['title' => 'Офисные здания'],
            ['title' => 'Офисные помещения', 'parent' => 'commercial-property'],
            'Торговые помещения / центры' => ['title' => 'Торговые помещения'],
            ['title' => 'Торговые центры', 'parent' => 'commercial-property'],
            'Медицинские центры / салоны' => ['title' => 'Медицинские центры'],
            ['title' => 'Салоны', 'parent' => 'commercial-property'],
            ['title' => 'Клубы', 'parent' => 'commercial-property'],
            ['title' => 'Гаражные помещения', 'parent' => 'commercial-property'],
            'Доходная недвижимость' => ['title' => 'Доходные дома'],
            ['title' => 'Индустриальные парки', 'parent' => 'commercial-property'],
            ['title' => 'Земля природно-заповедного назначения', 'parent' => 'land'],
            ['title' => 'Острова', 'parent' => 'land'],
            'Земля рекреационного назначения' => ['title' => 'Земля рекреационного назначения '],
        ];
        foreach ($categories as $title => $data) {
            if (is_integer($title)) {
                $parentData = $categoryService->getOne(['category_translations.value' => $data['parent'], 'category_translations.key' => 'slug']);
                $form = new CategoryForm(['_category' => new Category(), 'parent_id' => $parentData['id']]);
                $slug = $parentData['slug'] . "-" . UtilityHelper::transliterate($data['title']);
            } else {
                $category = $categoryService->getOne(['category_translations.value' => $title, 'category_translations.key' => Translation::KEY_TITLE]);
                $form = new CategoryForm();
                $form->_category = $categoryRepository->findOneByCriteria(['category.id' => $category['id']]);
                $form->prepareUpdate($category);
                $slug = $category['slug'];
            }
            $form->load([
                'MetaForm' => [
                    'ru-RU' => [
                        Translation::KEY_TITLE => $data['title'],
                        Translation::KEY_SLUG => $slug,
                    ]
                ]
            ]);
            $form->bindData();
            $form->save();
        }
    }
}
