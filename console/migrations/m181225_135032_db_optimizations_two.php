<?php

use yii\db\Migration;

/**
 * Class m181225_135032_db_optimizations_two
 */
class m181225_135032_db_optimizations_two extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_address_translation_lat_lng', 'address_translation', ['lat', 'lng']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_address_translation_lat_lng', 'address_translation');
    }
}
