<?php

use yii\db\Migration;

/**
 * Class m190405_144421_category_attribute_group
 */
class m190405_144421_category_attribute_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('category', 'attribute_group_alias', $this->string(35)->null());
        $this->addColumn('category', 'form_group', $this->string(25)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'attribute_group_alias');
        $this->dropColumn('category', 'form_group');
    }
}
