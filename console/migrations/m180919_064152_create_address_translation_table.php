<?php

use yii\db\Migration;

/**
 * Handles the creation of table `address_translation`.
 */
class m180919_064152_create_address_translation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('address_translation', [
            'lat' => $this->decimal(10, 8)->notNull(),
            'lng' => $this->decimal(11, 8)->notNull(),
            'locale' => $this->string(5)->notNull(),
            'title' => $this->string(),
            'custom_data' => $this->binary(),
        ]);
        $this->addPrimaryKey('', 'address_translation', ['lat', 'lng', 'locale']);
        $this->createIndex('address_translation_locale_index', 'address_translation', 'locale');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('address_translation');
    }
}
