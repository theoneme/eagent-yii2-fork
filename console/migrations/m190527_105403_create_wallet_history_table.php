<?php

use yii\db\Migration;

/**
 * Handles the creation of table `wallet_history`.
 */
class m190527_105403_create_wallet_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('wallet_history', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'payment_id' => $this->integer(),
            'type' => $this->integer(),
            'total' => $this->integer(),
            'currency_code' => $this->string(3)->notNull(),
            'created_at' => $this->integer(),
            'custom_data' => $this->binary(),
            'template' => $this->integer(),
        ]);

        $this->createIndex('wallet_history_user_id_index', 'wallet_history', 'user_id');
        $this->createIndex('wallet_history_type_index', 'wallet_history', 'type');

        $this->addForeignKey('fk_wallet_history_user_id', 'wallet_history', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_wallet_payment_id', 'wallet_history', 'payment_id', 'payment', 'id', 'CASCADE');
        $this->addForeignKey('fk_wallet_currency_code', 'wallet_history', 'currency_code', 'currency', 'code', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('wallet_history');
    }
}
