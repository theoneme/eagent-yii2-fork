<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use common\models\AttributeValue;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m181127_120239_new
 */
class m181127_120239_new extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'property_owner_status', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Статус лица размещаемого объявление'];
        $attribute->save();

        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attributeValue = new AttributeValue(['alias' => 'agent', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Агент'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => 'owner', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Собственник'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => 'builder', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Застройщик'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => 'representative', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Представитель собственника'];
        $attributeValue->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => 'property_owner_status'])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
