<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use yii\db\Migration;

/**
 * Class m181015_062904_new_attribute_filter
 */
class m181015_062904_new_attribute_filter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $buildingAttribute = Attribute::find()->select('id')->where(['alias' => 'new_building'])->one();
        if($buildingAttribute) {
            $filterAttribute = new AttributeFilter(['attribute_id' => $buildingAttribute->id, 'type' => AttributeFilter::TYPE_CHECKBOX]);
            $filterAttribute->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
