<?php

use yii\db\Migration;

/**
 * Class m181207_123523_new_index_translation
 */
class m181207_123523_new_index_translation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_translation_entity_entity_id_locale', 'translation', ['entity', 'entity_id', 'locale']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_translation_entity_entity_id_locale', 'translation');
    }
}
