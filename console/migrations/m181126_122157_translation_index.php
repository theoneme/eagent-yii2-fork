<?php

use yii\db\Migration;

/**
 * Class m181126_122157_translation_index
 */
class m181126_122157_translation_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('index_translation_entity', 'translation');
        $this->dropIndex('index_translation_entity_id', 'translation');
        $this->createIndex('index_translation_entity_entity_id', 'translation', ['entity', 'entity_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_translation_entity_entity_id', 'translation');
        $this->createIndex('index_translation_entity', 'translation', 'entity');
        $this->createIndex('index_translation_entity_id', 'translation', 'entity_id');
    }
}
