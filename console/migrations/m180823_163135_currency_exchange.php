<?php

use yii\db\Migration;

/**
 * Class m180823_163135_currency_exchange
 */
class m180823_163135_currency_exchange extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('currency_exchange', [
            'code_from' => $this->string(3),
            'code_to' => $this->string(3),
            'ratio' => $this->decimal(8, 4)
        ]);

        $this->addPrimaryKey('pk_code_from_code_to', 'currency_exchange', ['code_from', 'code_to']);
        $this->addForeignKey('fk_currency_exchange_code_from', 'currency_exchange', 'code_from', 'currency', 'code', 'CASCADE');
        $this->addForeignKey('fk_currency_exchange_code_to', 'currency_exchange', 'code_to', 'currency', 'code', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency_exchange');
    }
}
