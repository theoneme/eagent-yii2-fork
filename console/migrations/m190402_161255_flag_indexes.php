<?php

use yii\db\Migration;

/**
 * Class m190402_161255_flag_indexes
 */
class m190402_161255_flag_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('flag_composite_index', 'flag');
        $this->createIndex('index_flag_entity_id_entity', 'flag', ['entity_id', 'entity']);
        $this->createIndex('index_flag_type', 'flag', 'type');
        $this->createIndex('index_flag_value', 'flag', 'value');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_flag_entity_id_entity', 'flag');
        $this->dropIndex('index_flag_type', 'flag');
        $this->dropIndex('index_flag_value', 'flag');
        $this->createIndex('flag_composite_index', 'flag', ['entity_id', 'entity', 'type']);
    }
}
