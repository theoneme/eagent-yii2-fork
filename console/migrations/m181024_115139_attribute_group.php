<?php

use yii\db\Migration;

/**
 * Class m181024_115139_attribute_group
 */
class m181024_115139_attribute_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attribute_group}}', [
            'id' => $this->primaryKey(),
            'entity' => $this->integer()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'type' => $this->integer()
        ]);

        $this->createIndex('index_attribute_group_entity', 'attribute_group', 'entity');
        $this->createIndex('index_attribute_group_entity_id', 'attribute_group', 'entity_id');
        $this->createIndex('index_attribute_group_type', 'attribute_group', 'type');

        $this->createTable('{{%attribute_to_group}}', [
            'attribute_group_id' => $this->integer()->notNull(),
            'attribute_id' => $this->integer()->notNull(),
        ]);
        $this->createIndex('attribute_to_group_link', 'attribute_to_group', ['attribute_group_id', 'attribute_id'], true);
        $this->addForeignKey('fk_attribute_to_group_attribute_group', 'attribute_to_group', 'attribute_group_id', 'attribute_group', 'id', 'CASCADE');
        $this->addForeignKey('fk_attribute_to_group_attribute', 'attribute_to_group', 'attribute_id', 'attribute', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attribute_to_group');
        $this->dropTable('attribute_group');
    }
}
