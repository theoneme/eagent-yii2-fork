<?php

use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Category;
use common\repositories\sql\CategoryRepository;
use common\services\entities\CategoryService;
use yii\db\Migration;

/**
 * Class m190409_132624_move_properties_to_subcategories
 */
class m190409_132624_move_properties_to_subcategories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        /** @var \common\repositories\sql\PropertyRepository $propertyRepository */
        $propertyRepository = Yii::$container->get(\common\repositories\sql\PropertyRepository::class);
        /** @var \common\repositories\elastic\PropertyRepository $propertyElasticRepository */
        $propertyElasticRepository = Yii::$container->get(\common\repositories\elastic\PropertyRepository::class);
        $categories = [
            'flats' => 'flats-kvartiry',
            'houses' => 'houses-doma',
            'commercial-property' => 'commercial-property-pomeshcheniya-svobodnogo-naznacheniya',
            'land' => 'land-uchastki-pod-zhiluyu-zastroyku',
        ];
        foreach ($categories as $parent => $child) {
            $parentData = $categoryService->getOne(['category_translations.value' => $parent, 'category_translations.key' => 'slug']);
            $childData = $categoryService->getOne(['category_translations.value' => $child, 'category_translations.key' => 'slug']);
            if (!empty($parentData) && !empty($childData)) {
                $propertyRepository->updateManyByCriteria(['category_id' => $parentData['id']], ['category_id' => $childData['id']]);
//                $propertyElasticRepository->updateManyByCriteria(['category_id' => $parentData['id']], ['category_id' => $childData['id']]);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = Yii::$container->get(CategoryRepositoryInterface::class);
        /** @var \common\repositories\sql\PropertyRepository $propertyRepository */
        $propertyRepository = Yii::$container->get(\common\repositories\sql\PropertyRepository::class);
        /** @var \common\repositories\elastic\PropertyRepository $propertyElasticRepository */
        $propertyElasticRepository = Yii::$container->get(\common\repositories\elastic\PropertyRepository::class);
        $categories = [
            'flats',
            'houses',
            'commercial-property',
            'land',
        ];
        foreach ($categories as $parent) {
            /** @var Category $parentData */
            $parentData = $categoryRepository->joinWith(['translations'])->findOneByCriteria(['category_translations.value' => $parent, 'category_translations.key' => 'slug']);
            $childData = $parentData->children(1)->select(['id'])->asArray()->column();
            if (!empty($parentData) && !empty($childData)) {
                $propertyRepository->updateManyByCriteria(['category_id' => $childData], ['category_id' => $parentData['id']]);
//                $propertyElasticRepository->updateManyByCriteria(['category_id' => $childData], ['category_id' => $parentData['id']]);
            }
        }
    }
}
