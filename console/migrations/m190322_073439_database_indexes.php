<?php

use yii\db\Migration;

/**
 * Class m190322_073439_database_indexes
 */
class m190322_073439_database_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('property_ads_allowed', 'property');
        $this->dropIndex('property_ads_allowed_partners', 'property');

        $this->dropForeignKey('fk_property_category_id', 'property');
        $this->dropIndex('index_composite', 'property');
        $this->addForeignKey('fk_property_category_id', 'property', 'category_id', 'category', 'id', 'CASCADE');

        $this->createIndex('index_type_status_ads_cat_reg', 'property', ['region_id', 'category_id', 'type', 'status', 'ads_allowed']);

        $this->dropIndex('index_translation_locale', 'translation');
        $this->dropIndex('index_translation_entity_entity_id_locale', 'translation');
        $this->dropIndex('index_comboned_entity_key_value', 'translation');
        $this->dropIndex('index_translation_entity_entity_id', 'translation');

        $this->createIndex('index_translation_entity_entity_id', 'translation', ['entity_id', 'entity']);
        $this->createIndex('index_translation_entity_locale_entity_id', 'translation', ['entity_id', 'locale', 'key', 'entity']);
        $this->execute("ALTER TABLE translation ADD FULLTEXT INDEX index_translation_value_fulltext (value)");

        $this->dropIndex('flag_entity_index', 'flag');
        $this->dropIndex('flag_entity_id_index', 'flag');
        $this->dropIndex('flag_type_index', 'flag');
        $this->createIndex('flag_composite_index', 'flag', ['entity_id', 'entity', 'type']);

        $this->dropIndex('index_attachment_is_primary', 'attachment');

        $this->alterColumn('attribute', 'alias', $this->string(40));

        $this->alterColumn('translation', 'key', $this->string(20));
        $this->alterColumn('translation', 'entity', $this->tinyInteger());

        $this->dropIndex('index_attribute_value', 'attribute_value');
        $this->createIndex('index_attribute_value_alias', 'attribute_value', 'alias');

        $this->dropIndex('index_building_attribute_entity_alias', 'building_attribute');
        $this->dropIndex('index_building_attribute_value_alias', 'building_attribute');
        $this->dropIndex('index_building_attribute_value_number', 'building_attribute');
        $this->createIndex('index_combined_building_attribute', 'building_attribute', ['building_id', 'entity_alias', 'value_alias']);
        $this->createIndex('index_combined_building_attribute_two', 'building_attribute', ['building_id', 'entity_alias', 'value_number']);

        $this->dropIndex('building_ads_allowed', 'building');
        $this->dropIndex('building_ads_allowed_partners', 'building');
        $this->dropIndex('index_building_status', 'building');
        $this->createIndex('index_type_status_ads_reg', 'building', ['region_id', 'type', 'status', 'ads_allowed']);

        $this->alterColumn('property_attribute', 'entity_alias', $this->string(35));
        $this->alterColumn('property_attribute', 'value_alias', $this->string(55));
        $this->alterColumn('building_attribute', 'entity_alias', $this->string(35));
        $this->alterColumn('building_attribute', 'value_alias', $this->string(55));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('property_attribute', 'entity_alias', $this->string(75));
        $this->alterColumn('property_attribute', 'value_alias', $this->string(75));
        $this->alterColumn('building_attribute', 'entity_alias', $this->string(75));
        $this->alterColumn('building_attribute', 'value_alias', $this->string(75));

        $this->dropIndex('index_type_status_ads_reg', 'building');
        $this->createIndex('building_ads_allowed', 'building', 'ads_allowed');
        $this->createIndex('building_ads_allowed_partners', 'building', 'ads_allowed_partners');
        $this->createIndex('index_building_status', 'building', 'status');

        $this->dropIndex('index_combined_building_attribute', 'building_attribute');
        $this->dropIndex('index_combined_building_attribute_two', 'building_attribute');
        $this->createIndex('index_building_attribute_entity_alias', 'building_attribute', 'entity_alias');
        $this->createIndex('index_building_attribute_value_alias', 'building_attribute', 'value_alias');
        $this->createIndex('index_building_attribute_value_number', 'building_attribute', 'value_number');

        $this->dropIndex('index_attribute_value_alias', 'attribute_value');
        $this->createIndex('index_attribute_value', 'attribute_value', 'status');

        $this->alterColumn('translation', 'key', $this->string(55));
        $this->alterColumn('translation', 'entity', $this->integer());

        $this->alterColumn('attribute', 'alias', $this->string(65));

        $this->createIndex('index_attachment_is_primary', 'attachment', 'is_primary');

        $this->dropIndex('flag_composite_index', 'flag');
        $this->createIndex('flag_entity_index', 'flag', 'entity');
        $this->createIndex('flag_entity_id_index', 'flag', 'entity_id');
        $this->createIndex('flag_type_index', 'flag', 'type');

        $this->dropIndex('index_translation_entity_locale_entity_id', 'translation');
        $this->dropIndex('index_translation_value_fulltext', 'translation');
        $this->dropIndex('index_translation_entity_entity_id', 'translation');

        $this->createIndex('index_translation_entity_entity_id', 'translation', ['entity', 'entity_id']);
        $this->createIndex('index_translation_locale', 'translation', 'locale');
        $this->createIndex('index_translation_entity_entity_id_locale', 'translation', ['entity', 'entity_id', 'locale']);
        $this->execute("ALTER TABLE `translation` ADD INDEX `index_comboned_entity_key_value` (`entity`, `key`, `value`(15));");

        $this->dropForeignKey('fk_property_category_id', 'property');
        $this->dropIndex('index_type_status_ads_cat_reg', 'property');
        $this->createIndex('index_composite', 'property', ['category_id', 'status', 'type']);
        $this->addForeignKey('fk_property_category_id', 'property', 'category_id', 'category', 'id', 'CASCADE');
        $this->createIndex('property_ads_allowed', 'property', 'ads_allowed');
        $this->createIndex('property_ads_allowed_partners', 'property', 'ads_allowed_partners');
    }
}
