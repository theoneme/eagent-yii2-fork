<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use common\models\AttributeValue;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m190208_110720_new_building_attributes
 */
class m190208_110720_new_building_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'building_phase', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Очередь строительства'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'building_state', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Стадия строительства дома'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();
        $attributeValue = new AttributeValue(['alias' => 'built', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Построен, но не сдан'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => 'hand-over', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Сдан в эксплуатацию'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => 'unfinished', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Строится'];
        $attributeValue->save();

        $attribute = new Attribute(['alias' => 'ready_quarter', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Квартал сдачи дома'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();
        $attributeValue = new AttributeValue(['alias' => '1', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => '1 квартал'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => '2', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => '2 квартал'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => '3', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => '3 квартал'];
        $attributeValue->save();
        $attributeValue = new AttributeValue(['alias' => '4', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => '4 квартал'];
        $attributeValue->save();

        $attribute = new Attribute(['alias' => 'ready_year', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Год сдачи дома'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'building_series', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Серия дома'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'ceiling_height', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Высота потолков в метрах'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_DROPDOWN]);
        $filterAttribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => ['ceiling_height', 'building_state', 'building_series', 'ready_year', 'ready_quarter', 'building_phase']])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
