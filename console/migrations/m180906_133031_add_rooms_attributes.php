<?php

use common\models\Attribute;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180906_133031_add_rooms_attributes
 */
class m180906_133031_add_rooms_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'rooms', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Количество комнат'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'bedrooms', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Количество спален'];
        $attribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => ['rooms', 'bedrooms']])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
