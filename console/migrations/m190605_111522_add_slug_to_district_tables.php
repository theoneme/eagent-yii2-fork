<?php

use yii\db\Migration;

/**
 * Class m190605_111522_add_slug_to_district_tables
 */
class m190605_111522_add_slug_to_district_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('district', 'slug', $this->string(55)->null());
        $this->addColumn('micro_district', 'slug', $this->string(55)->null());

        $this->createIndex('index_district_slug', 'district', 'slug');
        $this->createIndex('index_micro_district_slug', 'micro_district', 'slug');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('district', 'slug');
        $this->dropColumn('micro_district', 'slug');
    }
}
