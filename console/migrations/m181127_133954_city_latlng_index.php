<?php

use yii\db\Migration;

/**
 * Class m181127_133954_city_latlng_index
 */
class m181127_133954_city_latlng_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_city_latlng', 'city', ['lat', 'lng']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_city_latlng', 'city');
    }
}
