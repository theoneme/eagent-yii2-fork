<?php

use yii\db\Migration;

/**
 * Class m190128_153408_property_latlng_index
 */
class m190128_153408_property_latlng_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_property_latlng', 'property', ['lat', 'lng']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_property_latlng', 'property');
    }
}
