<?php

use yii\db\Migration;

/**
 * Handles the creation of table `flag`.
 */
class m180918_104405_create_flag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('flag', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(),
            'entity_id' => $this->integer(),
            'type' => $this->string()
        ]);

        $this->createIndex('flag_entity_index', 'flag', 'entity');
        $this->createIndex('flag_entity_id_index', 'flag', 'entity_id');
        $this->createIndex('flag_type_index', 'flag', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('flag');
    }
}
