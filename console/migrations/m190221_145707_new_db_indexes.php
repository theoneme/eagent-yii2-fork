<?php

use yii\db\Migration;

/**
 * Class m190221_145707_new_db_indexes
 */
class m190221_145707_new_db_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('slug', 'property');
        $this->createIndex('index_property_created_at', 'property', 'created_at');
        $this->createIndex('index_user_created_at', 'user', 'created_at');
        $this->createIndex('index_request_created_at', 'request', 'created_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('slug', 'property', 'slug');
        $this->dropIndex('index_property_created_at', 'property');
        $this->dropIndex('index_user_created_at', 'user');
        $this->dropIndex('index_request_created_at', 'request');
    }
}
