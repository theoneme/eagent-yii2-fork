<?php

use yii\db\Migration;

/**
 * Class m190515_130524_video_tables
 */
class m190515_130524_video_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('property_video', [
            'id' => $this->primaryKey(),
            'property_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'content' => $this->string(155)->notNull()
        ]);
        $this->addForeignKey('fk_property_video_property_id_property', 'property_video', 'property_id', 'property', 'id', 'CASCADE');

        $this->createTable('building_video', [
            'id' => $this->primaryKey(),
            'building_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'content' => $this->string(155)->notNull()
        ]);
        $this->addForeignKey('fk_building_video_building_id_building', 'building_video', 'building_id', 'building', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_property_video_property_id_property', 'property_video');
        $this->dropForeignKey('fk_building_video_building_id_building', 'building_video');

        $this->dropTable('property_video');
        $this->dropTable('building_video');
    }
}
