<?php

use yii\db\Migration;

/**
 * Class m190329_070313_new_building_index
 */
class m190329_070313_new_building_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_building_latlng', 'building', ['lat', 'lng']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_building_latlng', 'building');
    }
}
