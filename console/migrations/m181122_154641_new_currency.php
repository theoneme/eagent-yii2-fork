<?php

use yii\db\Migration;

/**
 * Class m181122_154641_new_currency
 */
class m181122_154641_new_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('currency', [
            'title' => 'Yuan',
            'code' => 'CNY',
            'symbol_left' => '¥',
            'symbol_right' => '',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('currency', ['code' => 'CNY']);
    }
}
