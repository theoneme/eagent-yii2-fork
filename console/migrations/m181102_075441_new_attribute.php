<?php

use common\models\Attribute;
use common\models\AttributeValue;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m181102_075441_new_attribute
 */
class m181102_075441_new_attribute extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'sale_type', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Параметры продажи'];
        $attribute->save();

        $attributeValue = new AttributeValue(['alias' => 'bargain', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Торг'];
        $attributeValue->save();

        $attributeValue = new AttributeValue(['alias' => 'clean_sale', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Чистая продажа'];
        $attributeValue->save();

        $attributeValue = new AttributeValue(['alias' => 'mortgage', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Ипотека'];
        $attributeValue->save();

        $attributeValue = new AttributeValue(['alias' => 'exchange', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Возможен обмен'];
        $attributeValue->save();

        $attributeValue = new AttributeValue(['alias' => 'deposit', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Внесен задаток'];
        $attributeValue->save();

        $attributeValue = new AttributeValue(['alias' => 'pledge', 'attribute_id' => $attribute->id]);
        $title = $attributeValue->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Находится в залоге'];
        $attributeValue->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => 'sale_type'])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
