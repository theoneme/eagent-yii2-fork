<?php

use yii\db\Migration;

/**
 * Class m190327_124519_another_optimize
 */
class m190327_124519_another_optimize extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE `translation` ADD INDEX `index_key_value` (`key`, `value`(100));");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_key_value', 'translation');
    }
}
