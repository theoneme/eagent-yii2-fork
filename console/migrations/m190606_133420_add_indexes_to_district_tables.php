<?php

use yii\db\Migration;

/**
 * Class m190606_133420_add_indexes_to_district_tables
 */
class m190606_133420_add_indexes_to_district_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_district_slug_city_id', 'district', ['slug', 'city_id'], true);
        $this->createIndex('index_micro_district_slug_city_id', 'micro_district', ['slug', 'city_id'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_district_slug_city_id', 'district');
        $this->dropIndex('index_micro_district_slug_city_id', 'micro_district');
    }
}
