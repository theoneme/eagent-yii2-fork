<?php

use yii\db\Migration;

/**
 * Class m181009_075241_index_test
 */
class m181009_075241_index_test extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property_attribute', 'value_number', $this->float()->null());

        $this->createIndex('index_property_attribute_value_number', 'property_attribute', 'value_number');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_property_attribute_value_number', 'property_attribute');

        $this->dropColumn('property_attribute', 'value_number');
    }
}
