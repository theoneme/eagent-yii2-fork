<?php

use yii\db\Migration;

/**
 * Class m180823_170330_new_currency
 */
class m180823_170330_new_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('currency', [
            'title' => 'Ruble',
            'code' => 'RUB',
            'symbol_left' => '',
            'symbol_right' => 'р.',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \common\models\Currency::deleteAll(['code' => 'RUB']);
    }
}
