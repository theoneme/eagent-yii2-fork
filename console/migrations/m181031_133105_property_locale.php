<?php

use yii\db\Migration;

/**
 * Class m181031_133105_property_locale
 */
class m181031_133105_property_locale extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('index_property_locale', 'property', 'locale');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index_property_locale', 'property');
    }
}
