<?php

use yii\db\Migration;

/**
 * Class m181005_065949_requests
 */
class m181005_065949_requests extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%request}}', [
            'id' => $this->primaryKey(),
            'price' => $this->integer()->null(),
            'currency_code' => $this->string(3)->notNull(),
            'locale' => $this->string(5)->notNull(),
            'slug' => $this->string(100)->notNull()->unique(),
            'category_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
            'status' => $this->integer()->notNull(),
            'contract_price' => $this->boolean()->defaultValue(false)
        ]);

        $this->addForeignKey('fk_request_user_id', 'request', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_request_category_id', 'request', 'category_id', 'category', 'id', 'CASCADE');
        $this->addForeignKey('fk_request_currency_code', 'request', 'currency_code', 'currency', 'code', 'CASCADE');
        $this->createIndex('index_request_status', 'request', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_request_user_id', 'request');
        $this->dropForeignKey('fk_request_category_id', 'request');
        $this->dropForeignKey('fk_request_currency_code', 'request');

        $this->dropTable('request');
    }
}
