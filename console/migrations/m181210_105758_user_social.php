<?php

use yii\db\Migration;

/**
 * Class m181210_105758_user_social
 */
class m181210_105758_user_social extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_social', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'source' => $this->string()->notNull(),
            'source_id' => $this->string()->notNull(),
            'data' => $this->binary()
        ]);

        $this->addForeignKey('fk_user_social_user_id', 'user_social', 'user_id', 'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_social_user_id', 'user_social');
        $this->dropTable('user_social');
    }
}
