<?php

use yii\db\Migration;

/**
 * Class m181002_064844_add_value_to_flag_table
 */
class m181002_064844_add_value_to_flag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('flag', 'value', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('flag', 'value');
    }
}
