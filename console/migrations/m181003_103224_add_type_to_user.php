<?php

use common\models\User;
use yii\db\Migration;

/**
 * Class m181003_103224_add_type_to_user
 */
class m181003_103224_add_type_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'type', $this->integer()->defaultValue(User::TYPE_DEFAULT));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'type');
    }
}
