<?php

use yii\db\Migration;

/**
 * Handles the creation of table `microdistrict`.
 */
class m190604_113703_create_micro_district_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('micro_district', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->notNull(),
            'district_id' => $this->integer()->null(),
            'polygon' => $this->getDb()->getSchema()->createColumnSchemaBuilder('polygon'),
        ]);

        $this->addForeignKey('fk_micro_district_city_id', 'micro_district', 'city_id', 'city', 'id', 'CASCADE');
        $this->addForeignKey('fk_micro_district_district_id', 'micro_district', 'district_id', 'district', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('micro_district');
    }
}
