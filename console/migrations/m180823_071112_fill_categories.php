<?php

use common\models\Category;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180823_071112_fill_categories
 */
class m180823_071112_fill_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $root = new Category();
        $title = $root->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Root'];
        $slug = $root->bind('translations');
        $slug->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_SLUG, 'value' => 'root'];
        $root->makeRoot(false);

        $flats = new Category();
        $title = $flats->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Квартиры'];
        $slug = $flats->bind('translations');
        $slug->attributes = ['entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU', 'key' => Translation::KEY_SLUG, 'value' => 'flats'];
        $flats->appendTo($root, false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Category::deleteAll();
        Translation::deleteAll(['entity' => Translation::ENTITY_CATEGORY]);
    }
}
