<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_phase}}`.
 */
class m190228_124104_create_building_phase_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%building_phase}}', [
            'id' => $this->primaryKey(),
            'building_id' => $this->integer()->notNull(),
            'year' => $this->smallInteger(4),
            'quarter' => $this->tinyInteger(1),
            'status' => $this->smallInteger(),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
            'floors' => $this->smallInteger()
        ]);

        $this->addForeignKey('fk_building_phase_building_id', 'building_phase', 'building_id', 'building', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%building_phase}}');
    }
}
