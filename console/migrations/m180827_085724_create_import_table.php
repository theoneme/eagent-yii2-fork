<?php

use yii\db\Migration;

/**
 * Handles the creation of table `import`.
 */
class m180827_085724_create_import_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('import', [
            'id' => $this->primaryKey(),
            'source' => $this->string(),
            'entity' => $this->string(),
            'entity_id' => $this->integer(),
        ]);

        $this->createIndex('import_source_index', 'import', 'source');
        $this->createIndex('import_entity_index', 'import', 'entity');
        $this->createIndex('import_entity_id_index', 'import', 'entity_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('import');
    }
}
