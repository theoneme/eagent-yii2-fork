<?php

use yii\db\Migration;

/**
 * Handles the creation of table `service`.
 */
class m181023_075820_create_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('service', [
            'id' => $this->primaryKey(),
            'price' => $this->integer()->null(),
            'currency_code' => $this->string(3)->notNull(),
            'locale' => $this->string(5)->notNull(),
            'created_at' => $this->integer()->null(),
            'updated_at' => $this->integer()->null(),
            'url' => $this->string()
        ]);
        $this->addForeignKey('fk_service_currency_code', 'service', 'currency_code', 'currency', 'code', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('service');
    }
}
