<?php

use yii\db\Migration;

/**
 * Class m190319_140132_add_ads_allowed_to_user
 */
class m190319_140132_add_ads_allowed_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'ads_allowed', $this->boolean()->defaultValue(true));
        $this->addColumn('user', 'ads_allowed_partners', $this->boolean()->defaultValue(true));

        $this->createIndex('user_ads_allowed', 'user', 'ads_allowed');
        $this->createIndex('user_ads_allowed_partners', 'user', 'ads_allowed_partners');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'ads_allowed');
        $this->dropColumn('user', 'ads_allowed_partners');
    }
}
