<?php

use yii\db\Migration;

/**
 * Class m180903_141618_remove_unique_from_attribute_alias
 */
class m180903_141618_remove_unique_from_attribute_alias extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('alias', 'attribute_value');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('alias', 'attribute_value', 'alias', true);
    }
}
