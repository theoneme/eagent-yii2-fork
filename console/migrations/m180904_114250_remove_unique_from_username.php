<?php

use yii\db\Migration;

/**
 * Class m180904_114250_remove_unique_from_username
 */
class m180904_114250_remove_unique_from_username extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('username', 'user');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('username', 'user', 'username', true);
    }
}
