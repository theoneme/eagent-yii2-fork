<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m190218_084445_new_property_attributes
 */
class m190218_084445_new_property_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'flat_number', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Номер квартиры'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_NONE]);
        $filterAttribute->save();

        $attribute = new Attribute(['alias' => 'cadastral_number', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Кадастровый номер'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_NONE]);
        $filterAttribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => [
            'flat_number',
            'cadastral_number',
        ]])->all();

        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
