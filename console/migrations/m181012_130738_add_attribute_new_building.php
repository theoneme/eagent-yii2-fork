<?php

use common\models\Attribute;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m181012_130738_add_attribute_new_building
 */
class m181012_130738_add_attribute_new_building extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'new_building', 'type' => Attribute::TYPE_BOOLEAN]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Новостройка'];
        $attribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => ['new_building']])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
