<?php

use common\models\Attribute;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m180905_131638_new_attributes
 */
class m180905_131638_new_attributes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'amenities', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Дополнительные удобства'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'garages', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Гаражных мест'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'exterior_features', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Наружные удобства'];
        $attribute->save();

        $attribute = new Attribute(['alias' => 'interior_features', 'type' => Attribute::TYPE_STRING]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Внутренние удобства'];
        $attribute->save();
    }

    /**
     * @return bool|void
     * @throws Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => ['amenities', 'garages', 'exterior_features', 'interior_features']])->all();
        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
