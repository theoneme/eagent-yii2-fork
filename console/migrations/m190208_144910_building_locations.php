<?php

use yii\db\Migration;

/**
 * Class m190208_144910_building_locations
 */
class m190208_144910_building_locations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('building', 'country_id', $this->integer()->null());
        $this->addColumn('building', 'region_id', $this->integer()->null());
        $this->addColumn('building', 'city_id', $this->integer()->null());

        $this->addForeignKey('fk_building_country_id_country', 'building', 'country_id', 'country', 'id', 'SET NULL');
        $this->addForeignKey('fk_building_region_id_region', 'building', 'region_id', 'region', 'id', 'SET NULL');
        $this->addForeignKey('fk_building_city_id_city', 'building', 'city_id', 'city', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_building_country_id_country', 'building');
        $this->dropForeignKey('fk_building_region_id_region', 'building');
        $this->dropForeignKey('fk_building_city_id_city', 'building');

        $this->dropColumn('building', 'country_id');
        $this->dropColumn('building', 'region_id');
        $this->dropColumn('building', 'city_id');
    }
}
