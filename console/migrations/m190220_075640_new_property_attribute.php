<?php

use common\models\Attribute;
use common\models\AttributeFilter;
use common\models\Translation;
use yii\db\Migration;

/**
 * Class m190220_075640_new_property_attribute
 */
class m190220_075640_new_property_attribute extends Migration
{
    public function safeUp()
    {
        $attribute = new Attribute(['alias' => 'agent_commission', 'type' => Attribute::TYPE_NUMBER]);
        $title = $attribute->bind('translations');
        $title->attributes = ['entity' => Translation::ENTITY_ATTRIBUTE, 'locale' => 'ru-RU', 'key' => Translation::KEY_TITLE, 'value' => 'Комиссия агента (%)'];
        $attribute->save();
        $filterAttribute = new AttributeFilter(['attribute_id' => $attribute->id, 'type' => AttributeFilter::TYPE_NONE]);
        $filterAttribute->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $attributes = Attribute::find()->where(['alias' => [
            'agent_commission',
        ]])->all();

        foreach ($attributes as $attribute) {
            $attribute->delete();
        }
    }
}
