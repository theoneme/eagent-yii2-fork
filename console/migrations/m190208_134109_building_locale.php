<?php

use yii\db\Migration;

/**
 * Class m190208_134109_building_locale
 */
class m190208_134109_building_locale extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('building', 'locale', $this->string(5)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('building', 'locale');
    }
}
