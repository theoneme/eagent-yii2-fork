<?php

use yii\db\Migration;

/**
 * Class m180823_112615_added_timestamps_to_property
 */
class m180823_112615_added_timestamps_to_property extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'created_at', $this->integer()->null());
        $this->addColumn('property', 'updated_at', $this->integer()->null());
        $this->addColumn('property', 'status', $this->integer()->notNull());

        $this->createIndex('index_property_status', 'property', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('property', 'created_at');
        $this->dropColumn('property', 'updated_at');
        $this->dropColumn('property', 'status');
    }
}
