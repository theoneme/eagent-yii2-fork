<?php

use yii\db\Migration;

/**
 * Class m181211_140729_property_geo_fields
 */
class m181211_140729_property_geo_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('property', 'country_id', $this->integer()->null());
        $this->addColumn('property', 'region_id', $this->integer()->null());
        $this->addColumn('property', 'city_id', $this->integer()->null());

        $this->addForeignKey('fk_property_country_id_country', 'property', 'country_id', 'country', 'id', 'SET NULL');
        $this->addForeignKey('fk_property_region_id_region', 'property', 'region_id', 'region', 'id', 'SET NULL');
        $this->addForeignKey('fk_property_city_id_city', 'property', 'city_id', 'city', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_property_country_id_country', 'property');
        $this->dropForeignKey('fk_property_region_id_region', 'property');
        $this->dropForeignKey('fk_property_city_id_city', 'property');

        $this->dropColumn('property', 'country_id');
        $this->dropColumn('property', 'region_id');
        $this->dropColumn('property', 'city_id');
    }
}
