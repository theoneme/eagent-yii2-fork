<?php

use yii\db\Migration;

/**
 * Class m181129_112200_page_table
 */
class m181129_112200_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('page', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'user_id' => $this->integer()->null(),
            'image' => $this->string(75)->null()
        ]);

        $this->addForeignKey('fk_page_user_id', 'page', 'user_id', 'user', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_page_user_id', 'page');
        $this->dropTable('page');
    }
}
