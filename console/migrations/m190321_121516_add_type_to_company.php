<?php

use yii\db\Migration;

/**
 * Class m190321_121516_add_type_to_company
 */
class m190321_121516_add_type_to_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company', 'type', $this->smallInteger(0));
        $this->createIndex('company_type_index', 'company', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company', 'type');
    }
}
