<?php

use yii\db\Migration;

/**
 * Class m181210_133353_user_contact_value_length
 */
class m181210_133353_user_contact_value_length extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user_contact', 'value', $this->string(150));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user_contact',  'value', $this->string(75));
    }
}
