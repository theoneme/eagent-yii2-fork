<?php

use yii\db\Migration;

/**
 * Class m180821_142552_property_tables
 */
class m180821_142552_property_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%currency}}', [
            'title' => $this->string(32)->notNull(),
            'code' => $this->string(3)->notNull()->unique(),
            'symbol_left' => $this->string(12)->null(),
            'symbol_right' => $this->string(12)->null(),
        ]);
        $this->addPrimaryKey('pk_currency_code', 'currency', 'code');
        $this->insert('currency', [
            'title' => 'Hryvnia',
            'code' => 'UAH',
            'symbol_left' => '',
            'symbol_right' => 'грн.',
        ]);
        $this->insert('currency', [
            'title' => 'Dollar',
            'code' => 'USD',
            'symbol_left' => '$',
            'symbol_right' => '',
        ]);
        $this->insert('currency', [
            'title' => 'Euro',
            'code' => 'EUR',
            'symbol_left' => '€',
            'symbol_right' => '',
        ]);

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'lvl' => $this->integer()->notNull(),
            'root' => $this->integer()->notNull()->defaultValue(0),
        ]);
        $this->createIndex('index_category_lft', 'category', 'lft');
        $this->createIndex('index_category_rgt', 'category', 'rgt');
        $this->createIndex('index_category_lvl', 'category', 'lvl');
        $this->createIndex('index_category_root', 'category', 'root');

        $this->createTable('{{%attribute}}', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(65)->notNull()->unique(),
            'type' => $this->integer()->notNull(),
        ]);

        $this->createTable('{{%category_attribute}}', [
            'category_id' => $this->integer()->notNull(),
            'attribute_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('primary_category_attribute', 'category_attribute', ['category_id', 'attribute_id']);
        $this->addForeignKey('fk_category_attribute_category_id', 'category_attribute', 'category_id', 'category', 'id', 'CASCADE');
        $this->addForeignKey('fk_category_attribute_attribute_id', 'category_attribute', 'attribute_id', 'attribute', 'id', 'CASCADE');

        $this->createTable('{{%attribute_value}}', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(65)->notNull()->unique(),
            'status' => $this->integer()->notNull(),
            'attribute_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey('fk_attribute_value_attribute_id', 'attribute_value', 'attribute_id', 'attribute', 'id', 'CASCADE');
        $this->createIndex('index_attribute_value', 'attribute_value', 'status');

        $this->createTable('{{%translation}}', [
            'id' => $this->primaryKey(),
            'entity' => $this->integer()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'key' => $this->string(55)->notNull(),
            'locale' => $this->string(5)->notNull(),
            'value' => $this->text()->notNull()
        ]);
        $this->createIndex('index_translation_entity', 'translation', 'entity');
        $this->createIndex('index_translation_entity_id', 'translation', 'entity_id');
        $this->createIndex('index_translation_locale', 'translation', 'locale');

        $this->createTable('{{%property}}', [
            'id' => $this->primaryKey(),
            'price' => $this->integer()->notNull(),
            'currency_code' => $this->string(3)->notNull(),
            'locale' => $this->string(5)->notNull(),
            'slug' => $this->string(100)->notNull()->unique(),
            'category_id' => $this->integer()->notNull(),
            'parent_id' => $this->integer()->null(),
            'user_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_property_parent_id', 'property', 'parent_id', 'property', 'id', 'CASCADE');
        $this->addForeignKey('fk_property_user_id', 'property', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_property_category_id', 'property', 'category_id', 'category', 'id', 'CASCADE');
        $this->addForeignKey('fk_property_currency_code', 'property', 'currency_code', 'currency', 'code', 'CASCADE');

        $this->createTable('{{%property_attribute}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer()->notNull(),
            'property_id' => $this->integer()->notNull(),
            'value' => $this->integer()->notNull(),
            'entity_alias' => $this->string(75)->notNull(),
            'value_alias' => $this->string(75)->notNull(),
        ]);
        $this->addForeignKey('fk_property_attribute_attribute_id', 'property_attribute', 'attribute_id', 'attribute', 'id', 'CASCADE');
        $this->addForeignKey('fk_property_attribute_property_id', 'property_attribute', 'property_id', 'property', 'id', 'CASCADE');
        $this->addForeignKey('fk_property_attribute_value', 'property_attribute', 'value', 'attribute_value', 'id', 'CASCADE');
        $this->createIndex('index_property_attribute_entity_alias', 'property_attribute', 'entity_alias');
        $this->createIndex('index_property_attribute_value_alias', 'property_attribute', 'value_alias');

        $this->createTable('{{%attachment}}', [
            'id' => $this->primaryKey(),
            'entity' => $this->integer()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'content' => $this->string(255)->notNull(),
            'metadata' => $this->binary()->null(),
            'is_primary' => $this->integer(1)
        ]);
        $this->createIndex('index_attachment_is_primary', 'attachment', 'is_primary');
        $this->createIndex('index_attachment_entity', 'attachment', 'entity');
        $this->createIndex('index_attachment_entity_id', 'attachment', 'entity_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_property_parent_id', 'property');
        $this->dropForeignKey('fk_property_user_id', 'property');
        $this->dropForeignKey('fk_property_category_id', 'property');
        $this->dropForeignKey('fk_property_currency_code', 'property');
        $this->dropForeignKey('fk_attribute_value_attribute_id', 'attribute_value');
        $this->dropForeignKey('fk_property_attribute_attribute_id', 'property_attribute');
        $this->dropForeignKey('fk_property_attribute_property_id', 'property_attribute');
        $this->dropForeignKey('fk_property_attribute_value', 'property_attribute');
        $this->dropForeignKey('fk_category_attribute_category_id', 'category_attribute');
        $this->dropForeignKey('fk_category_attribute_attribute_id', 'category_attribute');

        $this->dropTable('property');
        $this->dropTable('currency');
        $this->dropTable('property_attribute');
        $this->dropTable('translation');
        $this->dropTable('attribute_value');
        $this->dropTable('attribute');
        $this->dropTable('category');
        $this->dropTable('category_attribute');
        $this->dropTable('attachment');
    }
}
