<?php

use yii\db\Migration;

/**
 * Class m181025_145541_attribute_to_group_custom_data
 */
class m181025_145541_attribute_to_group_custom_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attribute_to_group', 'custom_data', $this->binary()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('attribute_to_group', 'custom_data');
    }
}
