<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 13:49
 */

namespace console\interfaces;

use common\interfaces\ConverterInterface as BaseConverterInterface;

/**
 * Interface ConverterInterface
 * @package console\interfaces
 */
interface ConverterInterface extends BaseConverterInterface
{
    /**
     * @param $rawData
     * @return mixed
     */
    public function convertObject($rawData);
}