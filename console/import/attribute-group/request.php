<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.10.2018
 * Time: 17:59
 */

use common\models\AttributeGroup;

return [
    AttributeGroup::ENTITY_REQUEST_CATEGORY => [
        'flats' => [
            'common' => [
                'living_area' => [
                    'type' => 'range',
                    'group' => 'area',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the desired area of all residential premises, exclude the area of the kitchen, bathroom, balconies and loggias',
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'kitchen_area' => [
                    'type' => 'range',
                    'group' => 'area',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify desired kitchen area',
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'is_furnished' => [
                    'type' => 'checkbox',
                    'group' => 'additional',
                    'note' => [
                        'title' => 'With or without furniture',
                        'body' => 'Choose if you want to buy a condo with or without furniture',
                    ],
                    'rules' => [
                        'boolean'
                    ]
                ],
                'bathrooms' => [
                    'type' => 'range',
                    'group' => 'bathrooms',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the number of bathrooms'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'bathroom_type' => [
                    'type' => 'dropdownlist',
                    'group' => 'bathrooms',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the type of bathroom',
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'floor' => [
                    'type' => 'range',
                    'group' => 'floor',
                    'note' => [
                        'title' => null,
                        'body' => 'Set the floor where you want property to be on',
                    ],
                    'rules' => [
                        'number',
                        'required'
                    ]
                ],
                'floors' => [
                    'type' => 'range',
                    'group' => 'building',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the number of floors for your request'
                    ],
                    'rules' => [
                        'number',
                        'required'
                    ]
                ],
                'layout' => [
                    'type' => 'dropdownlist',
                    'group' => 'additional',
                    'note' => [
                        'title' => null,
                        'body' => 'Choose a condo planning option'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'condition' => [
                    'type' => 'dropdownlist',
                    'group' => 'additional',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the condition of the condo for your request',
                    ],
                    'rules' => [
                        'integer',
//                        'required'
                    ]
                ],
                'loggias' => [
                    'type' => 'range',
                    'group' => 'balconies',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the number of loggias for your request'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'balconies' => [
                    'type' => 'range',
                    'group' => 'balconies',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the number of balconies for your request'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'rooms' => [
                    'type' => 'range',
                    'group' => 'rooms',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the number of rooms for your request'
                    ],
                    'rules' => [
                        'integer',
                        'required'
                    ]
                ],
                'bedrooms' => [
                    'type' => 'range',
                    'group' => 'rooms',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify the number of separate bedrooms you want to be in property'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'year_built' => [
                    'type' => 'range',
                    'group' => 'building',
                    'note' => [
                        'title' => null,
                        'body' => 'Set which built year should building have'
                    ],
                    'rules' => [
                        'integer',
                    ]
                ],
                'material' => [
                    'type' => 'checkboxlist',
                    'group' => 'building',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify which material should building have'
                    ],
                    'rules' => [
                        'required'
                    ]
                ],
                'new_building' => [
                    'type' => 'checkbox',
                    'group' => 'building',
                    'note' => [
                        'title' => null,
                        'body' => 'Specify if you want property to be in a new construction',
                    ],
                    'rules' => [
                        'boolean'
                    ]
                ],
            ]
        ]
    ]
];