<?php

return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/property.php',
    require __DIR__ . '/building.php',
    require __DIR__ . '/request.php'
);