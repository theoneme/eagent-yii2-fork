<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.10.2018
 * Time: 17:59
 */

use common\models\AttributeGroup;

return [
    AttributeGroup::ENTITY_BUILDING => [
        \common\models\Building::TYPE_NEW_CONSTRUCTION => [
            'floors' => [
                'type' => 'numberbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Specify the number of floors in your building'
                ],
                'rules' => [
                    'number',
//                    'required'
                ]
            ],
            'ready_year' => [
                'type' => 'numberbox',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set the year in which the building will be ready'
                ],
                'rules' => [
                    'integer',
                ]
            ],
            'ceiling_height' => [
                'type' => 'numberbox',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set the height of ceilings'
                ],
                'rules' => [
                    'number',
                ]
            ],
            'ready_quarter' => [
                'type' => 'radiolist',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set the quarter in which the building will be ready'
                ],
                'rules' => [
                    'integer',
                ]
            ],
            'building_state' => [
                'type' => 'dropdownlist',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set the state of building'
                ],
                'rules' => [
                    'integer',
                ]
            ],
            'building_phase' => [
                'type' => 'textbox',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set building phase'
                ],
                'rules' => [
                    'string',
                ]
            ],
            'new_construction_builder' => [
                'type' => 'textbox',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set builder information for this construction'
                ],
                'rules' => [
                    'string',
                ]
            ],
            'new_construction_installment_plan' => [
                'type' => 'textbox',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set installment plan for properties in this construction'
                ],
                'rules' => [
                    'string',
                ]
            ],
            'new_construction_discount' => [
                'type' => 'numberbox',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set discounts for properties in this building'
                ],
                'rules' => [
                    'number',
                ]
            ],
            'new_construction_mortgage' => [
                'type' => 'numberbox',
                'group' => 'building_new',
                'note' => [
                    'title' => null,
                    'body' => 'Set mortgages for properties in this building'
                ],
                'rules' => [
                    'number',
                ]
            ],
            'material' => [
                'type' => 'textbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Specify what the house is built from. The options are: brick, monolith and foam concrete, monolith and brick, panel, wooden, etc.'
                ],
                'rules' => [
                    'string',
//                    'required'
                ]
            ],
            'building_amenitie' => [
                'type' => 'multitextbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Specify all the additional features and advantages that are presented in the house. Write them separated by commas.<br>For example: playground, underground parking, internet in the house, security, intercom, etc.'
                ],
                'rules' => [
                    'validateSelectizeString'
                ]
            ],
        ],
        \common\models\Building::TYPE_DEFAULT => [
            'floors' => [
                'type' => 'numberbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Specify the number of floors in your building'
                ],
                'rules' => [
                    'number',
//                    'required'
                ]
            ],
            'year_built' => [
                'type' => 'numberbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Set the year in which the building was built'
                ],
                'rules' => [
                    'integer',
                ]
            ],
            'building_renovation_year' => [
                'type' => 'numberbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Set the year in which the building was or will be renovated'
                ],
                'rules' => [
                    'integer',
                ]
            ],
            'material' => [
                'type' => 'textbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Specify what the house is built from. The options are: brick, monolith and foam concrete, monolith and brick, panel, wooden, etc.'
                ],
                'rules' => [
                    'string',
//                    'required'
                ]
            ],
            'building_amenitie' => [
                'type' => 'multitextbox',
                'group' => 'building_common',
                'note' => [
                    'title' => null,
                    'body' => 'Specify all the additional features and advantages that are presented in the house. Write them separated by commas.<br>For example: playground, underground parking, internet in the house, security, intercom, etc.'
                ],
                'rules' => [
                    'validateSelectizeString'
                ]
            ],
        ]
    ],
];