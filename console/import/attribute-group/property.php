<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.10.2018
 * Time: 17:59
 */

use common\models\AttributeGroup;

return [
    AttributeGroup::ENTITY_PROPERTY_CATEGORY => [
        'flats' => [
            'common' => [
                'flat_number' => [
                    'type' => 'numberbox',
                    'group' => 'address_additional',
                    'note' => [
                        'title' => 'Apartment number',
                        'body' => '<p>Specify number of your apartment.</p><p><b>For Example:</b> 205 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'cadastral_number' => [
                    'type' => 'numberbox',
                    'group' => 'address_additional',
                    'note' => [
                        'title' => 'Folio or Tax ID number',
                        'body' => '<p>Specify the folio or Tax ID number. This field is optional.</p><p><b>For Example:</b> 941-100-7-55 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'property_area' => [
                    'type' => 'numberbox',
                    'group' => 'area',
                    'note' => [
                        'title' => 'Total area',
                        'body' => '<p>Enter the total area (m²) of your apartment.</p><p><b>For Example:</b> 126 </p>'
                    ],
                    'rules' => [
                        'required',
                        'number'
                    ]
                ],
                'living_area' => [
                    'type' => 'numberbox',
                    'group' => 'area',
                    'note' => [
                        'title' => 'Living area',
                        'body' => '<p>Enter the total living area (m²).</p><p><b>For Example:</b> 53 </p>',
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'kitchen_area' => [
                    'type' => 'numberbox',
                    'group' => 'area',
                    'note' => [
                        'title' => 'Kitchen area',
                        'body' => '<p>Enter the kitchen area (m²).</p><p><b>For Example:</b> 20 </p>',
                    ],
                    'rules' => [
                        'number'
                    ]
                ],

                'ceiling_height' => [
                    'type' => 'numberbox',
                    'group' => 'ceiling',
                    'note' => [
                        'title' => 'Ceiling height',
                        'body' => '<p>Specify property ceiling height in meters.</p><p><b>For Example:</b> 2.8 </p>',
                    ],
                    'rules' => [
                        'number'
                    ]
                ],

                'rooms' => [
                    'type' => 'numberbox',
                    'group' => 'rooms',
                    'note' => [
                        'title' => 'Number of rooms',
                        'body' => '<p>Specify number of rooms in your property.</p><p><b>For Example:</b> 4 </p>'
                    ],
                    'rules' => [
                        'integer',
                        'required'
                    ]
                ],
                'bedrooms' => [
                    'type' => 'numberbox',
                    'group' => 'rooms',
                    'note' => [
                        'title' => 'Number of bedrooms',
                        'body' => '<p>Specify number of bedrooms in your property.</p><p><b>For Example:</b> 2 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],

                'bathrooms' => [
                    'type' => 'numberbox',
                    'group' => 'bathrooms',
                    'note' => [
                        'title' => 'Number of bathrooms',
                        'body' => '<p>Specify number of bathrooms in your property.</p><p><b>For Example:</b> 1 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'bathroom_type' => [
                    'type' => 'dropdownlist',
                    'group' => 'bathrooms',
                    'note' => [
                        'title' => 'Bathroom type',
                        'body' => '<p>Select the type of bathroom from the list.</p><p><b>For Example:</b> Full </p>',
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'floor' => [
                    'type' => 'numberbox',
                    'group' => 'floor',
                    'note' => [
                        'title' => 'Floor',
                        'body' => '<p>Specify the floor where your condo is located.</p><p><b>For Example:</b> 1 </p>',
                    ],
                    'rules' => [
                        'number',
                        'required'
                    ]
                ],
                'floors' => [
                    'type' => 'numberbox',
                    'group' => 'building',
                    'note' => [
                        'title' => 'Total storeys',
                        'body' => '<p>Specify number of storeys in building.</p><p><b>For Example:</b> 2 </p>'
                    ],
                    'rules' => [
                        'number',
                        'required'
                    ]
                ],

                'is_furnished' => [
                    'type' => 'checkbox',
                    'group' => 'additional',
                    'note' => [
                        'title' => 'Furniture',
                        'body' => '<p>Choose whether you sell an property with furniture or not.</p><p><b>For Example:</b> No </p>',
                    ],
                    'rules' => [
                        'boolean'
                    ]
                ],
                'layout' => [
                    'type' => 'dropdownlist',
                    'group' => 'additional',
                    'note' => [
                        'title' => 'Floor plan',
                        'body' => '<p>Select type of floor plan. Not required.</p><p><b>For Example:</b> Loft </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'condition' => [
                    'type' => 'dropdownlist',
                    'group' => 'additional',
                    'note' => [
                        'title' => 'Property condition',
                        'body' => '<p>Choose one of the options.</p><p><b>For Example:</b> Repair required </p>',
                    ],
                    'rules' => [
                        'integer',
//                        'required'
                    ]
                ],
                'property_owner_status' => [
                    'type' => 'dropdownlist',
                    'group' => 'additional',
                    'note' => [
                        'title' => 'Ownership',
                        'body' => '<p>Choose your status.</p><p><b>For Example:</b> Owner </p>'
                    ],
                    'rules' => [
                        'integer',
//                        'required'
                    ]
                ],
                'property_amenitie' => [
                    'type' => 'multitextbox',
                    'group' => 'additional',
                    'note' => [
                        'title' => 'Property amenities',
                        'body' => '<p>Enter all amenities in your property separated by commas.</p><p><b>For Example:</b> AC, Electric owen, Washer, Refrigerator </p>'
                    ],
                    'rules' => [
                        'validateSelectizeString'
                    ]
                ],

                'loggias' => [
                    'type' => 'numberbox',
                    'group' => 'balconies',
                    'note' => [
                        'title' => 'Loggia',
                        'body' => '<p>Specify number of loggias in your property.</p><p><b>For Example:</b> 1 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'balconies' => [
                    'type' => 'numberbox',
                    'group' => 'balconies',
                    'note' => [
                        'title' => 'Balcony',
                        'body' => '<p>Specify number of balconies in your property.</p><p><b>For Example:</b> 1 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'property_type' => [
                    'type' => 'dropdownlist',
                    'group' => 'ownership',
                    'note' => [
                        'title' => 'Ownership',
                        'body' => '<p>Choose your status.</p><p><b>For Example:</b> Owner </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'material' => [
                    'type' => 'textbox',
                    'group' => 'building',
                    'note' => [
                        'title' => 'Construction material',
                        'body' => '<p>Specify the material used in the construction of building.</p><p><b>For Example:</b> Brick </p>'
                    ],
                    'rules' => [
                        'string',
                        'required'
                    ]
                ],
                'year_built' => [
                    'type' => 'numberbox',
                    'group' => 'building',
                    'note' => [
                        'title' => 'Year of build',
                        'body' => '<p>Specify the year when property was completed.</p><p><b>For Example:</b> 2008 </p>'
                    ],
                    'rules' => [
                        'integer',
                    ]
                ],
                'building_renovation_year' => [
                    'type' => 'numberbox',
                    'group' => 'building',
                    'note' => [
                        'title' => 'Overhaul year',
                        'body' => '<p>If the building was overhauled, indicate in what year.</p><p><b>For Example:</b> 2015 </p>'
                    ],
                    'rules' => [
                        'integer',
                    ]
                ],
                'building_amenitie' => [
                    'type' => 'multitextbox',
                    'group' => 'building',
                    'note' => [
                        'title' => 'Building amenities',
                        'body' => '<p>Enter all amenities of your building separated by commas.</p><p><b>For Example:</b> Gym, Security 24/7, Parking </p>'
                    ],
                    'rules' => [
                        'validateSelectizeString'
                    ]
                ],
//                'new_building' => [
//                    'type' => 'checkbox',
//                    'group' => 'building',
//                    'note' => [
//                        'title' => null,
//                        'body' => 'Specify if this is a new construction',
//                    ],
//                    'rules' => [
//                        'boolean'
//                    ]
//                ],
                'sale_type' => [
                    'type' => 'checkboxlist',
                    'group' => 'price',
                    'note' => [
                        'title' => 'Terms of sale',
                        'body' => '<p>Specify sales options.</p><p><b>For Example:</b> Mortgage available </p>'
                    ],
                    'rules' => [
                        'safe'
                    ]
                ],
                'agent_commission' => [
                    'type' => 'numberbox',
                    'group' => 'price',
                    'note' => [
                        'title' => 'Commission to Buyer`s agent',
                        'body' => '<p>Specify commission you are willing to pay to the agent who will lead the buyer in percentage (%).</p><p><b>For Example:</b> 3 </p>'
                    ],
                    'rules' => [
                        ['validator' => 'number', 'options' => ['min' => 0, 'max' => 30]]
                    ]
                ],
            ],
            AttributeGroup::TYPE_RENT => [
                'are_pets_allowed' => [
                    'type' => 'checkbox',
                    'group' => 'additional',
                    'rules' => [
                        'boolean'
                    ]
                ]
            ],
            AttributeGroup::TYPE_SALE => []
        ],
        'houses' => [
            'common' => [
                'cadastral_number' => [
                    'type' => 'numberbox',
                    'group' => 'home_address_additional',
                    'note' => [
                        'title' => 'Folio or Tax ID number',
                        'body' => '<p>Specify the folio or Tax ID number. This field is optional.</p><p><b>For Example:</b> 941-100-7-55 </p>'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],
                'house_number' => [
                    'type' => 'numberbox',
                    'group' => 'home_address_additional',
                    'note' => [
                        'title' => 'House number',
                        'body' => '<p>Specify the house number.</p><p><b>For Example:</b> 28 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'property_type' => [
                    'type' => 'dropdownlist',
                    'group' => 'home_address_additional',
                    'note' => [
                        'title' => 'Type of ownership',
                        'body' => '<p>Select one of the available options.</p><p><b>For Example:</b> Private </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'material' => [
                    'type' => 'textbox',
                    'group' => 'primary',
                    'note' => [
                        'title' => 'Construction material',
                        'body' => '<p>Specify the material used in the construction of house.</p><p><b>For Example:</b> Wood </p>'
                    ],
                    'rules' => [
                        'required',
                        'string'
                    ]
                ],
                'floors' => [
                    'type' => 'numberbox',
                    'group' => 'primary',
                    'note' => [
                        'title' => 'Total storeys',
                        'body' => '<p>Specify number of storeys in the house.</p><p><b>For Example:</b> 2 </p>'
                    ],
                    'rules' => [
                        'required',
                        'number'
                    ]
                ],

                'year_built' => [
                    'type' => 'numberbox',
                    'group' => 'years',
                    'note' => [
                        'title' => 'Year of build',
                        'body' => '<p>Specify the year when property was completed.</p><p><b>For Example:</b> 2008 </p>'
                    ],
                    'rules' => [
                        'required',
                        'number'
                    ]
                ],
                'building_renovation_year' => [
                    'type' => 'numberbox',
                    'group' => 'years',
                    'note' => [
                        'title' => 'Overhaul year',
                        'body' => '<p>If the building was overhauled, indicate in what year.</p><p><b>For Example:</b> 2015 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],

                'property_area' => [
                    'type' => 'numberbox',
                    'group' => 'area',
                    'note' => [
                        'title' => 'Total area',
                        'body' => '<p>Enter the total area of the house.</p><p><b>For Example:</b> 126 </p>'
                    ],
                    'rules' => [
                        'required',
                        'number'
                    ]
                ],
                'living_area' => [
                    'type' => 'numberbox',
                    'group' => 'area',
                    'note' => [
                        'title' => 'Living area',
                        'body' => '<p>Enter the total living area. Not required.</p><p><b>For Example:</b> 53 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'kitchen_area' => [
                    'type' => 'numberbox',
                    'group' => 'area',
                    'note' => [
                        'title' => 'Kitchen area',
                        'body' => '<p>Enter the kitchen area (m²).</p><p><b>For Example:</b> 20 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],

                'rooms' => [
                    'type' => 'numberbox',
                    'group' => 'rooms',
                    'note' => [
                        'title' => 'Number of rooms',
                        'body' => '<p>Specify number of rooms in the house.</p><p><b>For Example:</b> 4 </p>'
                    ],
                    'rules' => [
                        'number',
                        'required'
                    ]
                ],
                'bedrooms' => [
                    'type' => 'numberbox',
                    'group' => 'rooms',
                    'note' => [
                        'title' => 'Number of bedrooms',
                        'body' => '<p>Specify number of bedrooms in the house.</p><p><b>For Example:</b> 2 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],

                'bathrooms' => [
                    'type' => 'numberbox',
                    'group' => 'bathrooms',
                    'note' => [
                        'title' => 'Number of bathrooms',
                        'body' => '<p>Specify number of bathrooms in the house.</p><p><b>For Example:</b> 1 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'bathroom_type' => [
                    'type' => 'dropdownlist',
                    'group' => 'bathrooms',
                    'note' => [
                        'title' => 'Bathroom type',
                        'body' => '<p>Select the type of bathroom from the list.</p><p><b>For Example:</b> Full </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'heating' => [
                    'type' => 'textbox',
                    'group' => 'heatcool',
                    'note' => [
                        'title' => 'Heating type',
                        'body' => '<p>Select your house heating type.</p><p><b>For Example:</b> Autonomous gas heating </p>'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],
                'cooling' => [
                    'type' => 'textbox',
                    'group' => 'heatcool',
                    'note' => [
                        'title' => 'Cooling type',
                        'body' => '<p>Select your house cooling type.</p><p><b>For Example:</b> Split-system </p>'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],

                'is_furnished' => [
                    'type' => 'checkbox',
                    'group' => 'furniture',
                    'note' => [
                        'title' => 'Furniture',
                        'body' => '<p>Do you sell property with furniture?</p><p><b>For Example:</b> Yes </p>'
                    ],
                    'rules' => [
                        'boolean'
                    ]
                ],

                'layout' => [
                    'type' => 'dropdownlist',
                    'group' => 'layout',
                    'note' => [
                        'title' => 'Floor plan',
                        'body' => '<p>Select type of floor plan. Not required.</p><p><b>For Example:</b> Loft </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'condition' => [
                    'type' => 'dropdownlist',
                    'group' => 'condition',
                    'note' => [
                        'title' => 'House condition',
                        'body' => '<p>Choose one of the options.</p><p><b>For Example:</b> Repair required </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'property_amenitie' => [
                    'type' => 'multitextbox',
                    'group' => 'amenities',
                    'note' => [
                        'title' => 'Аmenities inside home',
                        'body' => '<p>Enter all amenities in your house separated by commas.</p><p><b>For Example:</b> AC, Electric owen, Washer, Refrigerator </p>'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],

                'balconies' => [
                    'type' => 'numberbox',
                    'group' => 'balconies',
                    'note' => [
                        'title' => 'Balcony',
                        'body' => '<p>Specify number of balconies in your house.</p><p><b>For Example:</b> 1 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'loggias' => [
                    'type' => 'numberbox',
                    'group' => 'balconies',
                    'note' => [
                        'title' => 'Loggia',
                        'body' => '<p>Specify number of loggias in your house.</p><p><b>For Example:</b> 1 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
                'mansard' => [
                    'type' => 'checkbox',
                    'group' => 'balconies',
                    'note' => [
                        'title' => 'Мansard',
                        'body' => '<p>Do you have mansard in the house?</p><p><b>For Example:</b> Yes </p>'
                    ],
                    'rules' => [
                        'boolean'
                    ]
                ],
                'terraces' => [
                    'type' => 'numberbox',
                    'group' => 'balconies',
                    'note' => [
                        'title' => 'Terrace',
                        'body' => '<p>Specify number of terraces in your house. Not required.</p><p><b>For Example:</b> 1 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'garages' => [
                    'type' => 'numberbox',
                    'group' => 'garages',
                    'note' => [
                        'title' => 'How many cars space in garage?',
                        'body' => '<p>Specify number of car spaces in garage.</p><p><b>For Example:</b> 2 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'sale_type' => [
                    'type' => 'checkboxlist',
                    'group' => 'price',
                    'note' => [
                        'title' => 'Terms of sale',
                        'body' => '<p>Specify sales options.</p><p><b>For Example:</b> Mortgage available </p>'
                    ],
                    'rules' => [
                        'safe'
                    ]
                ],
                'agent_comission' => [
                    'type' => 'numberbox',
                    'group' => 'price',
                    'note' => [
                        'title' => 'Commission to Buyer`s agent',
                        'body' => '<p>Specify commission you are willing to pay to the agent who will lead the buyer in percentage (%).</p><p><b>For Example:</b> 3 </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],

                'village_amenity' => [
                    'type' => 'multitextbox',
                    'group' => 'village',
                    'note' => [
                        'title' => 'Amenities',
                        'body' => '<p>Enter all amenities of the residential complex separated by commas.</p><p><b>For Example:</b> Gated community, swimming pool </p>'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],

                'stead_area' => [
                    'type' => 'numberbox',
                    'group' => 'stead',
                    'note' => [
                        'title' => 'Specify the area of stead',
                        'body' => '<p>Enter the area of the stead in square meters.</p><p><b>For Example:</b> 800 </p>'
                    ],
                    'rules' => [
                        'number'
                    ]
                ],
                'stead_status' => [
                    'type' => 'textbox',
                    'group' => 'stead',
                    'note' => [
                        'title' => 'Land use',
                        'body' => '<p>Specify purpose of use of the land.</p><p><b>For Example:</b> Residential </p>'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],
                'stead_amenity' => [
                    'type' => 'multitextbox',
                    'group' => 'stead',
                    'note' => [
                        'title' => 'Specify the stead amenities',
                        'body' => '<p>Enter all amenities of the stead separated by commas.</p><p><b>For Example:</b> Gas, electricity, water, sewage </p>'
                    ],
                    'rules' => [
                        'string'
                    ]
                ],

                'property_owner_status' => [
                    'type' => 'dropdownlist',
                    'group' => 'status',
                    'note' => [
                        'title' => 'Ownership',
                        'body' => '<p>Choose your status.</p><p><b>For Example:</b> Owner </p>'
                    ],
                    'rules' => [
                        'integer'
                    ]
                ],
            ]
        ],
        'commercial-property' => [
            'common' => [
                'property_area' => [
                    'type' => 'numberbox',
                    'group' => 'primary',
                    'rules' => [
                        'required',
                        'number'
                    ]
                ],
            ]
        ],
//        'asd' => [
//            'common' => [
//                'stead_area' => [
//                    'type' => 'numberbox',
//                    'group' => 'stead',
//                    'note' => [
//                        'title' => 'Specify the area of stead',
//                        'body' => '<p>Enter the area of the stead in square meters.</p><p><b>For Example:</b> 800 </p>'
//                    ],
//                    'rules' => [
//                        'number'
//                    ]
//                ],
//            ],
//            'parent' => 'flats'
//        ]
    ]
];