<?php

namespace console\controllers;

use common\models\AddressTranslation;
use common\models\Company;
use common\models\Property;
use common\repositories\sql\CompanyRepository;
use common\repositories\sql\PropertyRepository;
use common\services\AddressTranslationCreator;
use Yii;
use yii\console\Controller;

/**
 * Class CronController
 * @package console\controllers
 */
class CronController extends Controller
{
    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionGenerateAddressTranslations($limit = 10)
    {
        /* @var PropertyRepository $propertyRepository */
        /* @var Property[] $properties */
        $propertyRepository = Yii::$container->get(PropertyRepository::class);
        $properties = $propertyRepository->limit($limit)->findManyByCriteria(['and',
            ['not', ['property.lat' => null]],
            ['not', ['property.lat' => '']],
            ['not', ['property.lng' => null]],
            ['not', ['property.lng' => '']],
            [
                'not exists',
                AddressTranslation::find()->where('address_translation.lat = property.lat and address_translation.lng = property.lng and address_translation.locale = property.locale')
            ],
        ]);
        foreach ($properties as $property) {
            /** @var AddressTranslationCreator $addressCreator */
            $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
            $addressCreator->create(['lat' => $property->lat, 'lng' => $property->lng, 'locale' => $property->locale]);
        }
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCompanyAddressTranslations($limit = 10)
    {
        /* @var CompanyRepository $repository */
        /* @var Company[] $items */
        $repository = Yii::$container->get(CompanyRepository::class);
        $items = $repository->limit($limit)->findManyByCriteria(['and',
            ['not', ['company.lat' => null]],
            ['not', ['company.lat' => '']],
            ['not', ['company.lng' => null]],
            ['not', ['company.lng' => '']],
            [
                'not exists',
                AddressTranslation::find()->where('address_translation.lat = company.lat and address_translation.lng = company.lng')
            ],
        ]);
        foreach ($items as $item) {
            /** @var AddressTranslationCreator $addressCreator */
            $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
            $addressCreator->create(['lat' => $item->lat, 'lng' => $item->lng, 'locale' => Yii::$app->language]);
        }
    }
}