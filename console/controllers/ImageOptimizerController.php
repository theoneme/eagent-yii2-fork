<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.11.2016
 * Time: 18:51
 */

namespace console\controllers;

use Aws\S3\Exception\S3Exception;
use common\models\Attachment;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\BaseStringHelper;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\imagine\Image;

/**
 * Class ImageOptimizerController
 * @package console\controllers
 */
class ImageOptimizerController extends Controller
{
    /**
     * @var Service
     */
    private $_s3;

    /**
     * ImageOptimizerController constructor.
     * @param string $id
     * @param Module $module
     * @param Service $s3
     * @param array $config
     */
    public function __construct(string $id, Module $module, Service $s3, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_s3 = $s3;
    }

    /**
     * @param int $limit
     * @throws \Throwable
     */
    public function actionConvertAttachments($limit = 1)
    {
        /** @var Attachment[] $pngAttachments */
        $pngAttachments = Attachment::find()->where(['entity' => Attachment::ENTITY_PROPERTY])->andFilterWhere(['like', 'content', '.png'])->groupBy('attachment.id')->limit($limit)->all();
        $countAttachments = count($pngAttachments);
        $webFolder = Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web';

        Console::output("Total PNGs to convert: {$countAttachments}");
        Console::startProgress(0, $countAttachments);

        foreach ($pngAttachments as $key => $pngAttachment) {
            $pathInfo = pathinfo($pngAttachment->content);
            if (!is_dir("{$webFolder}{$pathInfo['dirname']}")) {
                if (!mkdir("{$webFolder}{$pathInfo['dirname']}", 0777, true) && !is_dir("{$webFolder}{$pathInfo['dirname']}")) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', "{$webFolder}{$pathInfo['dirname']}"));
                }
            }
            $path = "{$webFolder}{$pngAttachment->content}";
            try {
                if ($this->_s3->commands()->exist(ltrim($pngAttachment->content, '/'))->execute()) {
                    $this->_s3->commands()->get(ltrim($pngAttachment->content, '/'))->saveAs($path)->execute();
                    $newPath = str_replace('.png', '.jpg', $path);
                    Image::getImagine()
                        ->open($path)
                        ->save($newPath, ['quality' => 87]);

                    $newFile = str_replace('.png', '.jpg', $pngAttachment->content);
                    Yii::$app->mediaLayer->saveToAws($newFile);
                    unlink($path);
                    Yii::$app->mediaLayer->removeMedia($pngAttachment->content);

                    $pngAttachment->content = $newFile;
                    $pngAttachment->save();
                } else {
                    $pngAttachment->delete();
                }
            } catch (S3Exception $e) {
                $pngAttachment->delete();
            }

            Console::updateProgress($key + 1, $countAttachments);
        }

        Console::endProgress("end" . PHP_EOL);
    }
}