<?php

namespace console\controllers\cron;

use common\mappers\forms\UserInvitationMessageMapper;
use common\models\Flag;
use common\models\Import;
use common\repositories\sql\UserRepository;
use common\services\NotificationService;
use Yii;
use yii\console\Controller;

/**
 * Class EmailController
 * @package console\controllers
 */
class EmailController extends Controller
{
    /**
     * @var NotificationService
     */
    private $_notificationService;

    /**
     * @var UserRepository
     */
    private $_userRepository;

    /**
     * EmailController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param UserRepository $userRepository
     * @param array $config
     */
    public function __construct($id, $module, NotificationService $notificationService, UserRepository $userRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_notificationService = $notificationService;
        $this->_userRepository = $userRepository;
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionInvite($limit = 10)
    {
        $users = $this->_userRepository->limit($limit)->findManyByCriteria(['and',
            ['not', ['user.email' => null]],
            ['not', ['user.email' => '']],
            ['exists', Import::find()->where(['entity' => 'user'])->andWhere('import.entity_id = user.id')],
            ['not exists', Flag::find()->where(['entity' => 'user', 'type' => Flag::TYPE_INVITATION_SENT])->andWhere('flag.entity_id = user.id')],
        ]);

        foreach ($users as $user) {
            $password = Yii::$app->security->generateRandomString(10);
            if($this->_userRepository->updateOneByCriteria(['id' => $user['id']], ['password_hash' => Yii::$app->security->generatePasswordHash($password)], UserRepository::MODE_LIGHT)) {
                (new Flag(['entity' => 'user', 'entity_id' => $user['id'], 'type' => Flag::TYPE_INVITATION_SENT]))->save();

                $params = UserInvitationMessageMapper::getMappedData(array_merge($user->attributes, ['password' => $password]));
                $this->_notificationService->sendNotification('email', $params);
            }
        }
    }
}