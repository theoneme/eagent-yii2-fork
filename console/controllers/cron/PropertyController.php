<?php

namespace console\controllers\cron;

use common\helpers\UtilityHelper;
use common\mappers\AddressTranslationsMapper;
use common\mappers\TranslationsMapper;
use common\models\Flag;
use common\models\Property;
use common\models\Translation;
use common\repositories\sql\CityRepository;
use common\repositories\sql\PropertyRepository;
use common\services\elasticsearch\PropertyElasticConverter;
use common\services\entities\CategoryService;
use common\services\entities\CityService;
use common\services\seo\PropertySeoService;
use Yii;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class PropertyController
 * @package console\controllers\cron
 */
class PropertyController extends Controller
{
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;

    /**
     * @var PropertySeoService
     */
    private $_propertySeoService;

    /**
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * PropertyController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param PropertySeoService $propertySeoService
     * @param PropertyRepository $propertyRepository
     * @param CategoryService $categoryService
     * @param array $config
     */
    public function __construct($id, $module, PropertySeoService $propertySeoService, PropertyRepository $propertyRepository, CategoryService $categoryService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyRepository = $propertyRepository;
        $this->_propertySeoService = $propertySeoService;
        $this->_categoryService = $categoryService;
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionDefineCity($limit = 500)
    {
        /** @var CityRepository $cityRepository */
        $cityRepository = Yii::$container->get(CityRepository::class);

        $properties = $this->_propertyRepository->limit($limit)
            ->findManyByCriteria(['and', ['city_id' => null], ['not', ['status' => Property::STATUS_DELETED]], ['not', ['lat' => null]]]);

        Console::startProgress(0, count($properties));

        foreach ($properties as $key => $property) {
            $city = $cityRepository->select(['distance' => "3959 * ACOS(COS(RADIANS({$property->lat})) 
                * COS(RADIANS(city.lat)) 
                * COS(RADIANS(city.lng) - RADIANS({$property->lng})) 
                + SIN(RADIANS({$property->lat})) 
                * SIN(RADIANS(city.lat)))"])
                ->orderBy(new Expression('distance ASC'))
                ->findOneByCriteria(['not', ['lat' => null]], true);
            $property->updateAttributes(['city_id' => $city['id'], 'region_id' => $city['region_id'], 'country_id' => $city['country_id'], 'updated_at' => time()]);
//            $this->_propertyRepository->updateOneByCriteria(['property.id' => $property->id], ]);

            Console::updateProgress($key + 1, count($properties));
        }
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionGenerateTitles($limit = 10)
    {
        $supportedLocales = Yii::$app->params['supportedLocales'];

        /** @var Property[] $properties */
        $properties = $this->_propertyRepository->joinWith(['translations'])->with(['category.translations', 'propertyAttributes'])
            ->select(['property.*', 'translationsCount' => 'count(distinct property_translations.locale)'])
            ->limit($limit)
            ->groupBy('property.id')
            ->having(['and',
                ['<', 'translationsCount', count($supportedLocales)],
                ['>', 'translationsCount', 0],
            ])
            ->findManyByCriteria(['property.status' => Property::STATUS_ACTIVE]);

        Console::output('Generate for: ' . count($properties));
        foreach ($properties as $property) {
            Console::output("Translating property #{$property['id']}");
            $translations = TranslationsMapper::getMappedData($property['translations'], TranslationsMapper::MODE_FULL);
            $locales = array_diff_key($supportedLocales, $translations);
            $success = true;

            $categoryTranslations = TranslationsMapper::getMappedData($property->category->translations);
            $addressTranslations = AddressTranslationsMapper::getMappedData($property->addressTranslations, AddressTranslationsMapper::MODE_FULL);

            $seoParams = ArrayHelper::map($property->propertyAttributes, 'entity_alias', 'value_alias');
            $seoParams['type'] = $property['type'];

            foreach ($locales as $locale => $code) {
                $currentCategoryTranslation = array_key_exists($locale, $categoryTranslations)
                    ? $categoryTranslations[$locale]
                    : array_pop($categoryTranslations);
                Console::output("...to locale '{$locale}'");

                if(count($addressTranslations) > 0) {
                    $addressTranslation = array_key_exists($locale, $addressTranslations)
                        ? $addressTranslations[$locale]
                        : array_values($addressTranslations)[0];
                } else {
                    $addressTranslation = [];
                }

                $seoParams['address'] = $addressTranslation['title'] ?? '';
                if ($property->locale === $locale) {
                    $seoParams['saddress'] = $addressTranslation['title'] ?? '';
                }
                else {
                    if (!empty($property->city_id)) {
                        /* @var CityService $cityService*/
                        $cityService = Yii::$container->get(CityService::class);
                        $city = $cityService->getOne(['id' => $property->city_id]);
                        if ($city) {
                            $currentCityTranslation = array_key_exists($locale, $city['translations'])
                                ? $city['translations'][$locale]
                                : array_pop($city['translations']);
                            $seoParams['saddress'] = $currentCityTranslation['title'];
                        }
                    }
                }
                $seoParams['city'] = $addressTranslation['data']['city'] ?? '';
                $seoParams['category'] = $currentCategoryTranslation[Translation::KEY_ADDITIONAL_TITLE] ?? $currentCategoryTranslation[Translation::KEY_TITLE] ?? '';

                $seo = $this->_propertySeoService->getSeo($property->category->customDataArray['seoTemplate'] ?? null, $seoParams, $locale);
                if (!empty($seo['heading'])) {
                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_TITLE,
                        'value' => $seo['heading'],
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);
                    $success = $success && $translation->save();

                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_SLUG,
                        'value' => UtilityHelper::generateSlug($seo['heading'], 9, 100, $property->getPrimaryKey()),
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);
                    $success = $success && $translation->save();
                }

                if ($success === true) {
                    Console::output("......success");
                } else {
                    Console::output("......fail");
                }
            }
        }
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionRegenerateTitles($limit = 10)
    {
        $supportedLocales = Yii::$app->params['supportedLocales'];
//        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => 'land', 'category_translations.key' => 'slug']);

        /** @var Property[] $properties */
        $properties = $this->_propertyRepository->joinWith(['category', 'translations'])->with(['category.translations', 'propertyAttributes'])
            ->select(['property.*', 'translationsCount' => 'count(distinct property_translations.locale)'])
            ->limit($limit)
            ->groupBy('property.id')
//            ->having(['and',
//                ['<', 'translationsCount', count($supportedLocales)],
//                ['>', 'translationsCount', 0],
//            ])
            ->findManyByCriteria(['and',
                ['not exists', Flag::find()->where(['entity' => 'property', 'type' => Flag::TYPE_TITLE_GENERATED])->andWhere('flag.entity_id = property.id')],
//                ['category.root' => $categoryItem['root']],
//                ['>=', 'category.lft', $categoryItem['lft']],
//                ['<=', 'category.rgt', $categoryItem['rgt']],
                ['property.status' => Property::STATUS_ACTIVE]
            ]);

        Console::output('Generate for: ' . count($properties));
        foreach ($properties as $property) {

            $seoService = Yii::$container->get(PropertySeoService::class);
            $success = true;

            $categoryTranslations = TranslationsMapper::getMappedData($property->category->translations, TranslationsMapper::MODE_FULL);
            $addressTranslations = AddressTranslationsMapper::getMappedData($property->addressTranslations, AddressTranslationsMapper::MODE_FULL);

            $seoParams = ArrayHelper::map($property->propertyAttributes, 'entity_alias', 'value_alias');
            $seoParams['type'] = $property['type'];
            foreach ($supportedLocales as $locale => $code) {
                $currentCategoryTranslation = array_key_exists($locale, $categoryTranslations)
                    ? $categoryTranslations[$locale]
                    : array_pop($categoryTranslations);
                if (count($addressTranslations) > 0) {
                    $addressTranslation = array_key_exists($locale, $addressTranslations)
                        ? $addressTranslations[$locale]
                        : array_values($addressTranslations)[0];
                } else {
                    $addressTranslation = [];
                }
                $seoParams['address'] = $addressTranslation['title'] ?? '';
                if ($property->locale === $locale) {
                    $seoParams['saddress'] = $addressTranslation['title'] ?? '';
                }
                else {
                    if (!empty($property->city_id)) {
                        /* @var CityService $cityService*/
                        $cityService = Yii::$container->get(CityService::class);
                        $city = $cityService->getOne(['id' => $property->city_id]);
                        if ($city) {
                            $currentCityTranslation = array_key_exists($locale, $city['translations'])
                                ? $city['translations'][$locale]
                                : array_pop($city['translations']);
                            $seoParams['saddress'] = $currentCityTranslation['title'];
                        }
                    }
                }
                $seoParams['city'] = $addressTranslation['data']['city'] ?? '';
                $seoParams['category'] = $currentCategoryTranslation[Translation::KEY_ADDITIONAL_TITLE] ?? $currentCategoryTranslation[Translation::KEY_TITLE] ?? '';
                $seo = $seoService->getSeo($property->category->customDataArray['seoTemplate'] ?? null, $seoParams, $locale);

                if (!empty($seo['heading'])) {
                    Translation::deleteAll([
                        'key' => [Translation::KEY_TITLE, Translation::KEY_SLUG],
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property['id'],
                        'locale' => $locale
                    ]);

                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_TITLE,
                        'value' => $seo['heading'],
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);

                    $success = $success && $translation->save();

                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_SLUG,
                        'value' => UtilityHelper::generateSlug($seo['heading'], 9, 100, $property->getPrimaryKey()),
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);
                    $success = $success && $translation->save();
                }
            }

            if ($success) {
                (new Flag(['entity' => 'property', 'entity_id' => $property->id, 'type' => Flag::TYPE_TITLE_GENERATED]))->save();
                /** @var Property $p */
                $p = $this->_propertyRepository->findOneByCriteria(['property.id' => $property->id]);
                $converter = new PropertyElasticConverter($p);
                $converter->process(true);
                Console::output("......success");
            }
            else {
                Console::output("......fail");
            }
        }
    }
}