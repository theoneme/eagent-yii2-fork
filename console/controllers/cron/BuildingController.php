<?php

namespace console\controllers\cron;

use common\helpers\FileHelper;
use common\models\Attachment;
use common\models\Building;
use common\models\Flag;
use common\models\Import;
use common\models\Property;
use common\repositories\sql\BuildingRepository;
use common\repositories\sql\CityRepository;
use common\repositories\sql\PropertyRepository;
use common\services\entities\BuildingService;
use common\services\entities\CategoryService;
use console\services\BuildingConverter;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Console;

/**
 * Class BuildingController
 * @package console\controllers\cron
 */
class BuildingController extends Controller
{
    /**
     * @var BuildingRepository
     */
    private $_buildingRepository;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var Service
     */
    private $_s3;

    /**
     * BuildingController constructor.
     * @param $id
     * @param $module
     * @param Service $s3
     * @param BuildingRepository $buildingRepository
     * @param BuildingService $buildingService
     * @param PropertyRepository $propertyRepository
     * @param array $config
     */
    public function __construct($id, $module, Service $s3, BuildingRepository $buildingRepository, BuildingService $buildingService, PropertyRepository $propertyRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingRepository = $buildingRepository;
        $this->_propertyRepository = $propertyRepository;
        $this->_buildingService = $buildingService;
        $this->_s3 = $s3;
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionDefineCity($limit = 500)
    {
        /** @var CityRepository $cityRepository */
        $cityRepository = Yii::$container->get(CityRepository::class);

        $items = $this->_buildingRepository->limit($limit)
            ->findManyByCriteria(['and', ['city_id' => null], ['not', ['status' => Property::STATUS_DELETED]], ['not', ['lat' => null]]]);

        Console::startProgress(0, count($items));

        foreach ($items as $key => $item) {
            $city = $cityRepository->select(['distance' => "3959 * ACOS(COS(RADIANS({$item->lat})) 
                * COS(RADIANS(city.lat)) 
                * COS(RADIANS(city.lng) - RADIANS({$item->lng})) 
                + SIN(RADIANS({$item->lat})) 
                * SIN(RADIANS(city.lat)))"])
                ->orderBy(new Expression('distance ASC'))
                ->findOneByCriteria(['not', ['lat' => null]], true);
            $item->updateAttributes(['city_id' => $city['id'], 'region_id' => $city['region_id'], 'country_id' => $city['country_id'], 'updated_at' => time()]);

            Console::updateProgress($key + 1, count($items));
        }
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreateBuildings($limit = 10)
    {
        $categoryService = Yii::$container->get(CategoryService::class);
        $categoryItem = $categoryService->getOne(['category_translations.value' => 'flats', 'category_translations.key' => 'slug']);

        /* @var Property[] $properties */
        $properties = $this->_propertyRepository->limit($limit)->groupBy(['property.lat', 'property.lng'])->findManyByCriteria(['and',
            ['category_id' => $categoryItem['id']],
            ['not', ['property.lat' => null]],
            ['not', ['property.lat' => '']],
            ['not', ['property.lng' => null]],
            ['not', ['property.lng' => '']],
            [
                'not exists',
                Building::find()->where('building.lat = property.lat and building.lng = property.lng')
            ],
        ]);
        Console::output('Total: ' . count($properties));
        /** @var BuildingConverter $converter */
        $converter = Yii::$container->get(BuildingConverter::class);
        foreach ($properties as $property) {
            Console::output("Processing property {$property['id']}");

            $buildingData = $converter->convertObject($property);
            if (!empty($buildingData)) {
                $buildingId = $this->_buildingService->create($buildingData);
                if (!empty($buildingId)) {
                    $this->_propertyRepository->updateManyByCriteria(['lat' => $property['lat'], 'lng' => $property['lng']], ['building_id' => $buildingId]);
                }
            }
        }
    }

    /**
     * @param int $limit
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreateImages($limit = 100)
    {
        $buildings = Building::find()
            ->where(['not exists', Attachment::find()->where('building.id = attachment.entity_id')->andWhere([
                'entity' => Attachment::ENTITY_BUILDING
            ])])
            ->andWhere(['exists', Property::find()->joinWith(['attachments'], true, 'INNER JOIN')->where('property.building_id = building.id')])
            ->limit($limit)
            ->groupBy('building.id')
            ->orderBy(['building.id' => SORT_DESC])
            ->all();

        Console::startProgress(0, count($buildings));
        foreach ($buildings as $key => $building) {
            Console::updateProgress($key + 1, count($buildings));
            $property = $this->_propertyRepository->joinWith(['attachments'], true, 'INNER JOIN')->findOneByCriteria([
                'building_id' => $building->id
            ]);

            if ($property !== null) {
                $attachments = $property->attachments;
                if (!empty($attachments)) {
                    $pathInfo = pathinfo($attachments[0]['content']);
                    $year = date('Y');
                    $month = date('m');
                    $day = date('d');
                    $dir = Yii::getAlias('@frontend') . "/web/uploads/building/{$year}/{$month}/{$day}/";
                    BaseFileHelper::createDirectory($dir);

                    $this->_s3->commands()
                        ->get(Yii::$app->mediaLayer->fixMediaPath($attachments[0]['content']))
                        ->saveAs("{$dir}{$building['slug']}.{$pathInfo['extension']}")->execute();

                    $buildingAttachment = new Attachment([
                        'entity' => Attachment::ENTITY_BUILDING,
                        'entity_id' => $building['id'],
                        'content' => "/uploads/building/{$year}/{$month}/{$day}/{$building['slug']}.{$pathInfo['extension']}"
                    ]);
                    Yii::$app->mediaLayer->saveToAws("/uploads/building/{$year}/{$month}/{$day}/{$building['slug']}.{$pathInfo['extension']}");
                    $buildingAttachment->save();
                }
            }
        }
    }

    /**
     * @param int $limit
     */
    public function actionYandexShit($limit = 10)
    {
        $buildings = $this->_buildingRepository->limit($limit)->findManyByCriteria(['and',
            ['>=', 'created_at', 1558569600],
            ['not exists', Attachment::find()->where('building.id = attachment.entity_id')->andWhere(['entity' => Attachment::ENTITY_BUILDING])],
            ['exists', Import::find()->where(['source' => 'yandex', 'entity' => 'building'])->andWhere('import.entity_id = building.id')]
        ]);
        $dataDir = Yii::getAlias('@console') . "/import/yandex";
        $tempDir = FileHelper::createTempDirectory('@frontend');
        $webRoot = Yii::getAlias('@frontend') . '/web';
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $imagesDir = $webRoot . "/uploads/building/{$year}/{$month}/{$day}/";
        if (!is_dir($imagesDir)) {
            if (!mkdir($imagesDir, 0777, true) && !is_dir($imagesDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $imagesDir));
            }
            if (Yii::$app instanceof \yii\console\Application) {
                chown($imagesDir, 'www-data');
                chgrp($imagesDir, 'www-data');
            }
        }

        Console::output('Total: ' . count($buildings));

        foreach ($buildings as $building) {
            Console::output("Processing building {$building['id']}");
            $import = Import::find()->joinWith(['flags'])->where(['import.source' => 'yandex', 'import.entity' => 'building', 'import.entity_id' => $building['id']])->one();
            $flags = ArrayHelper::map($import['flags'], 'type', 'value');
            if (!empty($flags[Flag::TYPE_SOURCE_CITY])) {
                $dataFilePath = $dataDir . "/{$flags[Flag::TYPE_SOURCE_CITY]}/building-data.php";
                $fileData = file_exists($dataFilePath) ? include($dataFilePath) : [];
                foreach ($fileData as $pageData) {
                    foreach ($pageData as $item) {
                        if (!empty($item['url']) && $item['url'] === $import['origin']) {
                            if (isset($item['imagesData'])) {
                                $imagesUploaded = 0;
                                foreach ($item['imagesData'] as $image) {
                                    $imagePath = FileHelper::downloadFile($image, $tempDir);
                                    if ($imagePath) {
                                        $formData['BuildingAttachmentForm'][]['content'] = $imagePath;
                                        $pathInfo = pathinfo($imagePath);
                                        if (empty($pathInfo['extension'])) {
                                            switch (exif_imagetype($webRoot . $imagePath)) {
                                                case IMAGETYPE_JPEG:
                                                case IMAGETYPE_PNG:
                                                    $pathInfo['extension'] = 'jpg';
                                                    break;
                                                case IMAGETYPE_GIF:
                                                    $pathInfo['extension'] = 'gif';
                                                    break;
                                            }
                                        }
                                        $extension = strtolower(!empty($pathInfo['extension']) ? '.' . $pathInfo['extension'] : '');
                                        $newPath = "/uploads/building/{$year}/{$month}/{$day}/{$building['slug']}"  . '-' . uniqid() . $extension;
                                        rename($webRoot . $imagePath, $webRoot . $newPath);

                                        if ((new Attachment(['entity' => Attachment::ENTITY_BUILDING, 'entity_id' => $building['id'], 'content' => $newPath]))->save()) {
                                            $imagesUploaded++;
                                        }
                                    }
                                    if ($imagesUploaded >= 10) {
                                        break;
                                    }
                                }
                                if ($imagesUploaded) {
                                    Console::output("... success");
                                    break 2;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}