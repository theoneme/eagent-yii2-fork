<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.05.2017
 * Time: 18:28
 */

namespace console\controllers;

use common\components\CurrencyHelper;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\mappers\AddressTranslationsMapper;
use common\models\AddressTranslation;
use common\models\Attachment;
use common\models\Attribute;
use common\models\AttributeValue;
use common\models\Building;
use common\models\BuildingAttribute;
use common\models\Category;
use common\models\City;
use common\models\Company;
use common\models\Country;
use common\models\Flag;
use common\models\Page;
use common\models\Property;
use common\models\PropertyAttribute;
use common\models\Region;
use common\models\Request;
use common\models\Service;
use common\models\Translation;
use common\models\User;
use common\repositories\sql\AddressTranslationRepository;
use common\repositories\sql\CategoryRepository;
use common\services\AddressTranslationCreator;
use common\services\entities\CategoryService;
use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\RegionService;
use common\services\GoogleMapsService;
use common\services\seo\BuildingSeoService;
use Yii;
use yii\console\Controller;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Console;


/**
 * Class CommandController
 * @package console\controllers
 */
class CommandController extends Controller
{
    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionTranslationCleanup()
    {
        $translationQuery = Translation::find();
        $count = $translationQuery->count();

        $iterator = 1;
        $deletedCount = 0;
        Console::startProgress($iterator, $count);
        foreach ($translationQuery->batch(100) as $batch) {
            /** @var Translation[] $batch */
            foreach ($batch as $translation) {
                $entity = $translation['entity'];
                $id = $translation['entity_id'];

                switch ($entity) {
                    case Translation::ENTITY_CITY:
                        $exists = City::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_REGION:
                        $exists = Region::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_COUNTRY:
                        $exists = Country::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_ATTRIBUTE:
                        $exists = Attribute::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_ATTRIBUTE_VALUE:
                        $exists = AttributeValue::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_PROPERTY:
                        $exists = Property::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_CATEGORY:
                        $exists = Category::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_USER:
                        $exists = User::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_SERVICE:
                        $exists = Service::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_BUILDING:
                        $exists = Building::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_REQUEST:
                        $exists = Request::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_PAGE:
                        $exists = Page::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                    case Translation::ENTITY_COMPANY:
                        $exists = Company::find()->where(['id' => $id])->exists();
                        if ($exists === false) {
                            $translation->delete();
                            $deletedCount++;
                        }
                        break;
                }

                $iterator++;
                Console::updateProgress($iterator, $count);
            }
        }

        Console::endProgress();
        Console::output("Deleted: {$deletedCount}");
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function actionNumberValuesFix()
    {
        $valuesToFix = AttributeValue::find()
            ->joinWith(['translations'])
            ->where(['or', ['like', 'attr_value_translations.value', '.'], ['like', 'attr_value_translations.value', ',']])
            ->andWhere(['attr_value_translations.key' => Translation::KEY_TITLE])
            ->select('value')
            ->indexBy('alias')
            ->column();

        $count = count($valuesToFix);
        Console::output("Values to fix: {$count}");
        if ($count > 0) {
            $result = true;
            $transaction = Yii::$app->db->beginTransaction();

            foreach ($valuesToFix as $key => $value) {
                Console::output("Fixing value {$key}");

                /** @var AttributeValue $av */
                $av = AttributeValue::find()->where(['alias' => $key])->one();

                $result = $result && $av->updateAttributes(['alias' => UtilityHelper::transliterate($value)]) > 0;
                PropertyAttribute::updateAll(['value_alias' => UtilityHelper::transliterate($value)], ['value_alias' => $key, 'attribute_id' => $av->attribute_id]);
            }

            if ($result === true) {
                $transaction->commit();
                return true;
            }

            $transaction->rollBack();
        }

        return false;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionLocationsCleanup()
    {
        /** @var CountryService $countryService */
        $countryService = Yii::$container->get(CountryService::class);
        /** @var RegionService $regionService */
        $regionService = Yii::$container->get(RegionService::class);
        /** @var CityService $cityService */
        $cityService = Yii::$container->get(CityService::class);

        $transaction = Yii::$app->db->beginTransaction();
        $countries = $countryService->getMany(['code' => 'ru']);
        foreach ($countries['items'] as $country) {
            Console::output("Processing country {$country['title']}");
            $regions = $regionService->getMany(['country_id' => $country['id']]);

            foreach ($regions['items'] as $region) {
                Console::output("Processing region {$region['title']}");
                $cities = $cityService->getMany(['region_id' => $region['id']]);

                $aliases = [];
                foreach ($cities['items'] as $city) {
                    Console::output("Processing city {$city['title']}");
                    if (!in_array($city['slug'], $aliases)) {
                        $aliases[] = $city['slug'];
                    } else {
                        $cityService->delete(['city.id' => $city['id']]);
                        Console::output("Removing city {$city['title']}");
                    }
                }
            }
        }

        $transaction->commit();
        return true;
    }

    /**
     * @return bool
     */
    public function actionPropertyAttributeNumber()
    {
        $query = PropertyAttribute::find()->joinWith(['attr'])->where(['attribute.type' => [Attribute::TYPE_DROPDOWN, Attribute::TYPE_NUMBER, Attribute::TYPE_BOOLEAN]])->groupBy('property_attribute.id');

        foreach ($query->batch(100) as $batch) {
            Console::output("Starting new batch..");

            /** @var PropertyAttribute[] $batch $item */
            foreach ($batch as $item) {
                Console::output("Processing item {$item['id']}");
                $item->updateAttributes(['value_number' => str_replace('-', ',', $item['value_alias'])]);
            }
        }

        return true;
    }

    /**
     * @param int $limit
     */
    public function actionCleanNoPrice($limit = 100)
    {
        $transaction = Yii::$app->db->beginTransaction();
        /* @var Property[] $properties */
        $properties = Property::find()->where(['price' => 0])->limit($limit)->all();

        Console::startProgress(0, count($properties));

        $ids = [];
        foreach ($properties as $key => $property) {
            $ids[] = $property->id;
            foreach ($property->attachments as $attachment) {
                $attachment->delete();
            }
            Translation::deleteAll(['entity' => Translation::ENTITY_PROPERTY, 'entity_id' => $property->id]);
            PropertyAttribute::deleteAll(['property_id' => $property->id]);
            Flag::deleteAll(['entity' => 'property', 'entity_id' => $property->id]);
            Console::startProgress($key + 1, count($properties));
        }

        Console::endProgress();
        Property::deleteAll(['id' => $ids]);

        return $transaction->commit();
    }

    /**
     * @param int $limit
     */
    public function actionUserLatLng($limit = 100)
    {
        $transaction = Yii::$app->db->beginTransaction();
        /* @var User[] $users */
        $users = User::find()
            ->select(['user.*', 'propertyCount' =>
                (new Query())->select('count(p1.id)')->from('property p1')->where('p1.user_id = user.id')->andWhere(['not', ['p1.status' => Property::STATUS_DELETED]])
            ])
            ->joinWith(['properties'])
            ->where(['not', ['property.status' => Property::STATUS_DELETED]])
            ->andWhere(['and',
                ['not', ['property.lat' => null]],
                ['not', ['property.lat' => '']],
                ['not', ['property.lng' => null]],
                ['not', ['property.lng' => '']],
            ])
            ->andWhere(['or',
                ['user.lat' => null],
                ['user.lat' => ''],
                ['user.lng' => null],
                ['user.lng' => ''],
            ])
            ->having(['>=', 'propertyCount', 2])
            ->groupBy('user.id')
            ->limit($limit)
            ->all();

        Console::startProgress(0, count($users));

        foreach ($users as $key => $user) {
            foreach ($user->properties as $property) {
                if (!empty($property->lat) && !empty($property->lng)) {
                    $user->updateAttributes(['lat' => $property->lat, 'lng' => $property->lng, 'updated_at' => time()]);
                    break;
                }
            }
            Console::startProgress($key + 1, count($users));
        }

        Console::endProgress();

        $transaction->commit();
    }

    /**
     * @param int $limit
     */
    public function actionUserNoUsername($limit = 100)
    {
        $transaction = Yii::$app->db->beginTransaction();
        /* @var User[] $users */
        $users = User::find()
            ->andWhere(['or',
                ['user.username' => null],
                ['user.username' => ''],
            ])
            ->groupBy('user.id')
            ->limit($limit)
            ->all();

        Console::startProgress(0, count($users));

        foreach ($users as $key => $user) {
            $user->updateAttributes(['username' => UtilityHelper::generateUsername($user->email, $user->phone), 'updated_at' => time()]);
            Console::startProgress($key + 1, count($users));
        }

        Console::endProgress();

        $transaction->commit();
    }

    /**
     * @param int $limit
     */
    public function actionMissingThumbnails($limit = 100)
    {
        $attachments = Attachment::find()->where('binary content like "%JPG%"')->limit($limit)->all();
        $s3 = Yii::$container->get(\frostealth\yii2\aws\s3\Service::class);

        Console::startProgress(0, count($attachments));
        foreach ($attachments as $key => $attachment) {
            if (file_exists(Yii::getAlias('@frontend') . '/web' . $attachment['content'])) {
                $oldName = Yii::getAlias('@frontend') . '/web' . $attachment['content'];
                $newName = strtolower(Yii::getAlias('@frontend') . '/web' . $attachment['content']);
                rename($oldName, $newName);
            } else {
                $rootDirectory = Yii::getAlias('@frontend');
                $relativePath = dirname($attachment['content']);
                BaseFileHelper::createDirectory("{$rootDirectory}/web{$relativePath}");

                $newAbsolutePath = "{$rootDirectory}/web{$relativePath}/" . strtolower(basename($attachment['content']));

                $s3->commands()
                    ->get(Yii::$app->mediaLayer->fixMediaPath($attachment['content']))
                    ->saveAs($newAbsolutePath)->execute();
            }

            Yii::$app->mediaLayer->saveToAws(strtolower($attachment['content']));
            $attachment->updateAttributes(['content' => strtolower($attachment['content'])]);

            Console::updateProgress($key + 1, count($attachments));
        }

        Console::endProgress();
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionCorruptedBuildings($limit = 100)
    {
        $buildings = Building::find()
            ->where(['or',
                ['not exists', Translation::find()->where('building.id = translation.entity_id')->andWhere(['entity' => Translation::ENTITY_BUILDING])],
                ['lat' => null],
                ['lat' => 0]
            ])
            ->limit($limit)
            ->all();

        Console::startProgress(0, count($buildings));
        foreach ($buildings as $key => $building) {
            $building->delete();

            Console::updateProgress($key + 1, count($buildings));
        }

        Console::endProgress();
        return true;
    }

    /**
     * @return int
     */
    public function actionBuildingFields()
    {
        Building::updateAll(['created_at' => 1546304461, 'updated_at' => 1546304461], ['created_at' => null]);
        Building::updateAll(['locale' => 'ru-RU'], ['locale' => null]);
        return Building::updateAll(['status' => Building::STATUS_ACTIVE], ['status' => null]);
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionSuperCorruptedBuildings($limit = 100)
    {
        /** @var Building[] $buildings */
        $buildings = Building::find()
            ->where(['not exists', Translation::find()->where('building.id = translation.entity_id')->andWhere([
                'entity' => Translation::ENTITY_BUILDING, 'key' => Translation::KEY_TITLE
            ])])
            ->limit($limit)
            ->groupBy('building.id')
            ->all();
        $seoService = Yii::$container->get(BuildingSeoService::class);

        Console::startProgress(0, count($buildings));
        foreach ($buildings as $key => $building) {
            $addressTranslation = AddressTranslationsMapper::getMappedData($building->addressTranslations);
            $seoParams = ArrayHelper::map($building->buildingAttributes, 'entity_alias', 'value_alias');
            $seoParams['address'] = $addressTranslation['title'] ?? '';
            $seo = $seoService->getSeo(BuildingSeoService::TEMPLATE_BUILDING, $seoParams);

            $titleTranslation = new Translation([
                'key' => Translation::KEY_TITLE,
                'entity' => Translation::ENTITY_BUILDING,
                'entity_id' => $building->id,
                'locale' => $building->locale,
                'value' => $seo['heading']
            ]);
            $titleTranslation->save();

            Console::updateProgress($key + 1, count($buildings));
        }

        Console::endProgress();
        return true;
    }

    /**
     * @return bool
     */
    public function actionMissingBuildingAddressTranslations()
    {
        /** @var ActiveQuery $buildings */
        $buildings = Building::find()
            ->where(['not exists', AddressTranslation::find()->where('building.lat = address_translation.lat and building.lng = address_translation.lng')])
            ->groupBy('building.id');
        $seoService = Yii::$container->get(BuildingSeoService::class);
        $addressCreator = Yii::$container->get(AddressTranslationCreator::class);

        Console::output('starting....');
        foreach ($buildings->batch(100) as $key => $batch) {
            Console::output("Processing batch {$key}");
            foreach($batch as $building) {
                $addressCreator->create(['lat' => $building['lat'], 'lng' => $building['lng'], 'locale' => $building['locale']]);

                $building = Building::find()
                    ->where(['id' => $building['id']])
                    ->with(['addressTranslations'])
                    ->one();
                $addressTranslation = AddressTranslationsMapper::getMappedData($building->addressTranslations);
                $seoParams = ArrayHelper::map($building->buildingAttributes, 'entity_alias', 'value_alias');
                $seoParams['address'] = $addressTranslation['title'] ?? '';
                $seo = $seoService->getSeo(BuildingSeoService::TEMPLATE_BUILDING, $seoParams);
                if (!empty($seo['heading']) && !empty($addressTranslation)) {
                    Console::output("Processing {$building['slug']}");
                    Translation::deleteAll(['key' => Translation::KEY_TITLE, 'entity' => Translation::ENTITY_BUILDING, 'entity_id' => $building['id']]);
                    $titleTranslation = new Translation([
                        'key' => Translation::KEY_TITLE,
                        'entity' => Translation::ENTITY_BUILDING,
                        'entity_id' => $building->id,
                        'locale' => $building->locale,
                        'value' => $seo['heading']
                    ]);
                    if ($titleTranslation->save()) {
                        $newSlug = UtilityHelper::generateSlug($seo['heading'], 7, 75, $building->getPrimaryKey());
                        $building->updateAttributes([
                            'slug' => $newSlug,
                            'updated_at' => time()
                        ]);
                        Console::output("Generated {$newSlug}");
                    }
                } else {
                    Console::output("Skipping {$building['slug']}");
                }
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function actionSuperDuperCorruptedBuildings()
    {
        /** @var ActiveQuery $buildings */
        $buildings = Building::find()
            ->where(['lat' => null]);

        foreach ($buildings->batch(100) as $key => $batch) {
            Console::output("Deleting batch ({$key})");
            foreach($batch as $building) {
                /** @var Building $building */
                $building->delete();
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function actionConvertPrice()
    {
        $properties = Property::find()->where(['default_price' => null]);

        foreach ($properties->batch() as $batch) {
            /** @var Property[] $batch */
            Console::startProgress(0, count($batch));
            foreach ($batch as $key => $property) {
                Console::updateProgress($key + 1, count($batch));
                $property->updateAttributes(['default_price' => CurrencyHelper::convert($property['currency_code'], 'RUB', $property['price'])]);
            }
            Console::endProgress();
        }

        return true;
    }

    /**
     * @return bool
     */
    public function actionPurge()
    {
        $query = AttributeValue::find()->joinWith(['attr', 'translations'])->where(['or',
            ['and',
                ['attribute.alias' => 'property_area'],
                ['attr_value_translations.key' => Translation::KEY_TITLE],
                ['>=', 'attr_value_translations.value', 1],
                ['<=', 'attr_value_translations.value', 10000],
            ],
            ['and',
                ['attribute.alias' => 'living_area'],
                ['attr_value_translations.key' => Translation::KEY_TITLE],
                ['>=', 'attr_value_translations.value', 1],
                ['<=', 'attr_value_translations.value', 500],
            ],
            ['and',
                ['attribute.alias' => 'kitchen_area'],
                ['attr_value_translations.key' => Translation::KEY_TITLE],
                ['>=', 'attr_value_translations.value', 1],
                ['<=', 'attr_value_translations.value', 100],
            ],
            ['attribute.alias' => 'year_built'],
        ])->select(['attribute_value.id', 'attribute_value.attribute_id'])->asArray();
        foreach ($query->batch(100) as $batch) {
            AttributeValue::updateAll(['status' => AttributeValue::STATUS_APPROVED], ['id' => array_column($batch, 'id')]);
        }

        return true;
    }

    /**
     *
     */
    public function actionCategoriesToSubcategories()
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        /** @var \common\repositories\sql\PropertyRepository $propertyRepository */
        $propertyRepository = Yii::$container->get(\common\repositories\sql\PropertyRepository::class);
        /** @var \common\repositories\elastic\PropertyRepository $propertyElasticRepository */
        $propertyElasticRepository = Yii::$container->get(\common\repositories\elastic\PropertyRepository::class);
        $categories = [
            'flats' => 'flats-kvartiry',
            'houses' => 'houses-doma',
            'commercial-property' => 'commercial-property-pomeshcheniya-svobodnogo-naznacheniya',
            'land' => 'land-uchastki-pod-zhiluyu-zastroyku',
        ];
        foreach ($categories as $parent => $child) {
            $parentData = $categoryService->getOne(['category_translations.value' => $parent, 'category_translations.key' => 'slug']);
            $childData = $categoryService->getOne(['category_translations.value' => $child, 'category_translations.key' => 'slug']);
            if (!empty($parentData) && !empty($childData)) {
                $propertyRepository->updateManyByCriteria(['category_id' => $parentData['id']], ['category_id' => $childData['id']]);
                $propertyElasticRepository->updateManyByCriteria(['category_id' => $parentData['id']], ['category_id' => $childData['id']]);
            }
        }
    }

    /**
     *
     */
    public function actionSubcategoriesToCategories()
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = Yii::$container->get(CategoryRepositoryInterface::class);
        /** @var \common\repositories\sql\PropertyRepository $propertyRepository */
        $propertyRepository = Yii::$container->get(\common\repositories\sql\PropertyRepository::class);
        /** @var \common\repositories\elastic\PropertyRepository $propertyElasticRepository */
        $propertyElasticRepository = Yii::$container->get(\common\repositories\elastic\PropertyRepository::class);
        $categories = [
            'flats',
            'houses',
            'commercial-property',
            'land',
        ];
        foreach ($categories as $parent) {
            /** @var Category $parentData */
            $parentData = $categoryRepository->joinWith(['translations'])->findOneByCriteria(['category_translations.value' => $parent, 'category_translations.key' => 'slug']);
            $childData = $parentData->children(1)->select(['id'])->asArray()->column();
            if (!empty($parentData) && !empty($childData)) {
                $propertyRepository->updateManyByCriteria(['category_id' => $childData], ['category_id' => $parentData['id']]);
                $propertyElasticRepository->updateManyByCriteria(['category_id' => $childData], ['category_id' => $parentData['id']]);
            }
        }
    }

    /**
     *
     */
    public function actionBuildingAttributesToProperty()
    {
        $aliases = ['material', 'floors', 'year_built'];
        $attributes = BuildingAttribute::find()->with(['building.properties'])
            ->where(['building_attribute.entity_alias' => $aliases])
            ->limit(100);
        $iterator = 0;
        $total = $attributes->count();
        Console::output("Total: {$total}");
        Console::startProgress(0, $total);
        while (true) {
            $batch = $attributes->offset($iterator * 100)->all();
            if (empty($batch)) {
                break;
            }

            Console::updateProgress($iterator * 100, $total);

            $insert = [];
            /**
             * @var BuildingAttribute[] $batch
             */
            foreach ($batch as $attribute) {
                foreach ($attribute->building->properties as $property) {
                    if (!PropertyAttribute::find()->where(['property_id' => $property->id, 'entity_alias' => $attribute['entity_alias']])->exists()) {
                        $insert[] = [
                            'attribute_id' => $attribute->attribute_id,
                            'property_id' => $property->id,
                            'value' => $attribute->value,
                            'entity_alias' => $attribute->entity_alias,
                            'value_alias' => $attribute->value_alias,
                            'value_number' => $attribute->value_number,
                        ];
                    }
                }
            }
            if (!empty($insert)) {
                Yii::$app->db->createCommand()->batchInsert('property_attribute', ['attribute_id', 'property_id', 'value', 'entity_alias', 'value_alias', 'value_number'], $insert)->execute();
            }

            $iterator++;
        }
    }

    /**
     *
     */
    public function actionRemoveUnnamedRoads()
    {
        /* @var AddressTranslation[] $addressTranslations*/
        $addressTranslations = AddressTranslation::find()->where(['like', 'title', 'unnamed road'])->all();
        $googleMapsService = Yii::$container->get(GoogleMapsService::class);
        foreach ($addressTranslations as $addressTranslation) {
            unset($addressTranslation->customDataArray['street']);
            $addressTranslation->title = $googleMapsService->getAddressFromData($addressTranslation->customDataArray);
            $addressTranslation->save();
        }
    }
}