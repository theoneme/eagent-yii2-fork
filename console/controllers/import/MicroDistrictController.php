<?php

namespace console\controllers\import;

use common\models\MicroDistrict;
use common\models\Translation;
use common\services\entities\MicroDistrictService;
use console\services\MicroDistrictFileConverter;
use Yii;
use yii\console\Controller;

/**
 * Class MicroDistrictController
 * @package console\controllers\import
 */
class MicroDistrictController extends Controller
{
    /**
     * @var MicroDistrictService
     */
    private $_microDistrictService;

    /**
     * MicroDistrictController constructor.
     * @param $id
     * @param $module
     * @param MicroDistrictService $microDistrictService
     * @param array $config
     */
    public function __construct($id, $module, MicroDistrictService $microDistrictService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_microDistrictService = $microDistrictService;
    }

    /**
     *
     */
    public function actionFile()
    {
        MicroDistrict::deleteAll(); // Возможно потом на апдейт переделать
        Translation::deleteAll(['entity' => Translation::ENTITY_MICRO_DISTRICT]);
        /** @var MicroDistrictFileConverter $converter */
        $converter = Yii::$container->get(MicroDistrictFileConverter::class);
        $filePath = Yii::getAlias('@console') . "/import/micro-districts.php";
        $districts = file_exists($filePath) ? include($filePath) : [];
        foreach ($districts as $district) {
            $data = $converter->convertObject($district);
            if ($data) {
                $this->_microDistrictService->create($data);
            }
        }
    }
}