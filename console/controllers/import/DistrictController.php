<?php

namespace console\controllers\import;

use common\models\District;
use common\models\Translation;
use common\services\entities\DistrictService;
use console\services\DistrictFileConverter;
use Yii;
use yii\console\Controller;

/**
 * Class DistrictController
 * @package console\controllers\import
 */
class DistrictController extends Controller
{
    /**
     * @var DistrictService
     */
    private $_districtService;

    /**
     * DistrictController constructor.
     * @param $id
     * @param $module
     * @param DistrictService $districtService
     * @param array $config
     */
    public function __construct($id, $module, DistrictService $districtService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_districtService = $districtService;
    }

    /**
     *
     */
    public function actionFile()
    {
        District::deleteAll(); // Возможно потом на апдейт переделать
        Translation::deleteAll(['entity' => Translation::ENTITY_DISTRICT]);
        /** @var DistrictFileConverter $converter */
        $converter = Yii::$container->get(DistrictFileConverter::class);
        $districtsPath = Yii::getAlias('@console') . "/import/districts.php";
        $districts = file_exists($districtsPath) ? include($districtsPath) : [];
        foreach ($districts as $district) {
            $data = $converter->convertObject($district);
            if ($data) {
                $this->_districtService->create($data);
            }
        }
    }
}