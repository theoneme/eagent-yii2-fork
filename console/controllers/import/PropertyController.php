<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.08.2018
 * Time: 12:00
 */

namespace console\controllers\import;

use common\dto\CompanyDTO;
use common\helpers\FileHelper;
use common\models\Category;
use common\models\Company;
use common\models\CompanyMember;
use common\models\Flag;
use common\models\Import;
use common\models\Property;
use common\models\Translation;
use common\models\User;
use common\services\ApiClientService;
use common\services\entities\AttributeService;
use common\services\entities\BuildingService;
use common\services\entities\CompanyMemberService;
use common\services\entities\CompanyService;
use common\services\entities\PropertyService;
use common\services\entities\UserService;
use console\services\EagentPropertyConverter;
use console\services\fake\FakePropertyConverter;
use console\services\FeedPropertyConverter;
use console\services\ImportRegisterService;
use console\services\LuximmoPropertyConverter;
use console\services\N1PropertyConverter;
use console\services\PrianPropertyConverter;
use console\services\UjobsPropertyConverter;
use console\services\UjobsUserConverter;
use console\services\YandexPropertyConverter;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\Console;
use yii\helpers\VarDumper;

/**
 * Class PropertyController
 * @package console\controllers
 */
class PropertyController extends Controller
{
    /**
     * @var ApiClientService
     */
    private $_apiClientService;
    /**
     * @var ImportRegisterService
     */
    private $_importService;
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var CompanyService
     */
    private $_companyService;
    /**
     * @var CompanyMemberService
     */
    private $_companyMemberService;
    /**
     * @var BuildingService
     */
    private $_buildingService;

    /**
     * PropertyController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param ApiClientService $apiClientService
     * @param ImportRegisterService $importRegisterService
     * @param PropertyService $propertyService
     * @param UserService $userService
     * @param CompanyService $companyService
     * @param CompanyMemberService $companyMemberService
     * @param array $config
     */
    public function __construct($id, $module,
                                ApiClientService $apiClientService,
                                ImportRegisterService $importRegisterService,
                                PropertyService $propertyService,
                                UserService $userService,
                                CompanyService $companyService,
                                CompanyMemberService $companyMemberService,
                                BuildingService $buildingService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_apiClientService = $apiClientService;
        $this->_importService = $importRegisterService;
        $this->_propertyService = $propertyService;
        $this->_userService = $userService;
        $this->_companyService = $companyService;
        $this->_companyMemberService = $companyMemberService;
        $this->_buildingService = $buildingService;
    }

    /**
     * @param int $user_id
     * @param int $limit
     * @param int $offset
     * @param bool $clean
     * @return bool
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \common\exceptions\EntityNotFoundException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionEagent($user_id = 83, $limit = 5, $offset = 0, $clean = false)
    {
        $iterator = 0;

        $converter = new EagentPropertyConverter(Yii::$app->s3ea, Yii::$container->get(AttributeService::class));
        $success = true;

        if ($clean !== false) {
            $this->_importService->cleanByCondition(['source' => 'eagent']);
        }

        $properties = Yii::$app->db1->createCommand("SELECT * FROM `property` WHERE entity_type in('property_sale', 'residential_rental') and lat is not null and lon is not null and type in('Condo', 'Single family home', 'Apartment', 'Townhouse', 'Multifamily home', 'Villa')")->queryAll();
        foreach ($properties as $key => $property) {
            $transaction = Yii::$app->db->beginTransaction();

            Console::output("Starting property {$property['id']}..");
            if ($key < $offset) {
                Console::output('Skipping');
                continue;
            }
            $translation = Yii::$app->db1->createCommand("SELECT * FROM `property_translation` WHERE translatable_id = {$property['id']} and locale = 'en'")->queryOne();
            $description = Yii::$app->db1->createCommand("SELECT * FROM `property_sale` WHERE id = {$property['id']}")->queryOne();
            $medias = Yii::$app->db1->createCommand("SELECT * FROM `media__media` left join `property_media` on (`property_media`.`media_id` = `media__media`.`id`) WHERE `property_media`.`property_id` = {$property['id']} limit 5")->queryAll();
            $amenities = Yii::$app->db1->createCommand("SELECT amenitie.name FROM `property_amenitie` left join `amenitie` on (amenitie.id = property_amenitie.amenitie_id) WHERE property_amenitie.property_id = {$property['id']}")->queryAll();
            $exteriors = Yii::$app->db1->createCommand("SELECT exterior_feature.name FROM `property_exteriorfeature` left join `exterior_feature` on (exterior_feature.id = property_exteriorfeature.exteriorfeature_id) WHERE property_exteriorfeature.property_id = {$property['id']}")->queryAll();
            $interiors = Yii::$app->db1->createCommand("SELECT interior_feature.name FROM `property_interiorfeature` left join `interior_feature` on (interior_feature.id = property_interiorfeature.interiorfeature_id) WHERE property_interiorfeature.property_id = {$property['id']}")->queryAll();

            if (empty($medias)) {
                continue;
            }

            $property['user_id'] = $user_id;

            $data = [
                'property' => $property,
                'translation' => $translation,
                'description' => $description,
                'medias' => $medias,
                'amenities' => $amenities,
                'exterior' => $exteriors,
                'interiors' => $interiors
            ];

            Console::output("Submit form for property {$property['id']}");
            $data = $converter->convertObject($data);

            $insertId = $this->_propertyService->create($data);

            $success = $insertId !== null && $success;
            if ($success === false) {
                Console::output("Submit form for property {$property['id']} failed");
                $transaction->rollBack();
                return false;
            } else {
                $this->_importService->registerImport('property', $insertId, 'eagent');
                $transaction->commit();
            }

            $iterator++;
            if ($iterator >= $limit) {
                break;
            }
        }

        return $success;
    }

    /**
     * @param int $limit
     * @param bool $clean
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionUjobs($limit = 5, $clean = false)
    {
        $result = true;
        if ($clean !== false) {
            $result = $this->_importService->cleanByCondition(['source' => 'ujobs']);
        }

        $categories = Translation::find()
            ->where(['key' => 'slug', 'entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU'])
            ->indexBy('value')
            ->select('entity_id')
            ->asArray()
            ->column();

        $catToCat = [
            2478 => $categories['flats'],
            2480 => $categories['houses'],
            2481 => $categories['land'],
            2778 => $categories['commercial-property'],
        ];

        $propertyConverter = new UjobsPropertyConverter(
            $catToCat,
            Yii::$app->s3uJobs,
            Yii::$container->get(AttributeService::class),
            Yii::$container->get(ApiClientService::class)
        );
        $userConverter = new UjobsUserConverter(Yii::$app->s3uJobs);

        $entityAliases = [];
        $data = $this->_apiClientService->makeRequest('get', 'https://ujobs.me/apientry/products', [
            'category_id' => array_keys($catToCat),
            'expand' => 'translation,attachments,extra',
            'pageSize' => $limit,
            'latLongRequired' => true
        ]);

        if ($data === false) {
            Console::output('Request for fetching products failed');
            return false;
        }

        foreach ($data as $item) {
            $transaction = Yii::$app->db->beginTransaction();

            $entityAliases[] = $item['alias'];
            $property = Property::find()->where(['slug' => $item['alias']])->one();

            if ($property instanceof Property) {
                // Update
            } else {
                $userData = $this->_apiClientService->makeRequest('get', "https://ujobs.me/apientry/profiles/{$item['user_id']}", [
                    'primitive-secure' => 'bratishka-pokushat',
                    'expand' => 'profile',
                ]);

                $user = $this->_userService->getOne(['email' => $userData['email']]);
                if ($user === null) {
                    $user = $this->_userService->getOne(['phone' => $userData['phone']]);
                }

                if ($user === null) {
                    $userDataConverted = $userConverter->convertObject($userData);
                    $userId = $this->_userService->create($userDataConverted);

                    if ($userId === null) {
                        Console::output("Saving user {$userData['email']} failed");
                        continue;
                    }

                    $result = $result && $this->_importService->registerImport('user', $userId, 'ujobs', (string)$userData['id']);
                } else {
                    $userId = $user['id'];
                }

                $item['user_id'] = $userId;
                $data = $propertyConverter->convertObject($item);
                $propertyId = $this->_propertyService->create($data);
                $result = $result && $propertyId !== null;
            }

            if ($result === false) {
                Console::output("Property {$item['alias']} went wrong. Rollback. Warns: {$result}");

                $transaction->rollBack();
                return false;
            } else {
                $transaction->commit();
            }
        }

        return $result;
    }

    /**
     * @param int $limit
     * @param bool $clean
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     * @return bool
     */
    public function actionN1($limit = 1, $clean = false)
    {
        if ($clean !== false) {
            $this->_importService->cleanByCondition(['source' => 'n1', 'entity' => 'user']);
        }

        $dirs = FileHelper::findDirectories(Yii::getAlias('@console') . "/import/n1", ['recursive' => false]);
        foreach ($dirs as $cityDir) {
            $cityAlias = basename($cityDir);
            $dataFilePath = "{$cityDir}/property-data.php";
            $cityData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            foreach ($cityData as $actionAlias => $actionData) {
                foreach ($actionData as $categoryAlias => $categoryData) {
                    Console::output("City '$cityAlias' action '$actionAlias' category '$categoryAlias'");
                    $flags = [
                        Flag::TYPE_SOURCE_CITY => $cityAlias,
                        Flag::TYPE_SOURCE_ACTION => $actionAlias,
                        Flag::TYPE_SOURCE_CATEGORY => $categoryAlias
                    ];
                    $importCount = $this->_importService->getCountByConditionAndFlags(['import.source' => 'n1', 'import.entity' => 'property'], $flags);
                    $page = ((int)($importCount / 100)) + 1;
                    $offset = $importCount % 100;
                    /** @var N1PropertyConverter $converter */
                    $converter = Yii::$container->get(N1PropertyConverter::class);
                    for ($i = $offset; $i < $offset + $limit; $i++) {
                        if (!isset($categoryData[$page][$i])) {
                            break;
                        }

                        $transaction = Yii::$app->db->beginTransaction();

                        $item = $categoryData[$page][$i];
                        if (!empty($item['url'])) {
                            $url = explode('?', $item['url'])[0];
                            if (empty($item['price']) || empty($item['userData']['name']) || empty($item['userData']['phone']) || $this->_importService->getCountByCondition(['like', 'origin', $url])) {
                                Console::output("... skipping property #{$i} from page #{$page}");
                                $this->_importService->registerImportWithFlags('property', 0, 'n1', $url, $flags);
                                $offset++;
                                $transaction->commit();
                                continue;
                            }
                        }
                        Console::output("... importing property #{$i} from page #{$page}");

                        $userId = User::find()->where(['phone' => $item['userData']['phone']])->select('id')->scalar();
                        if ($userId === false) {
                            $userId = $this->_userService->create([
                                'UserForm' => [
                                    'username' => $item['userData']['name'],
                                    'phone' => $item['userData']['phone'],
                                ]
                            ]);
                            if ($userId) {
                                $this->_importService->registerImport('user', $userId, 'n1');
                            }
                        }
                        if ($userId) {
                            $item['user_id'] = $userId;
                            $item['type'] = $actionAlias;
                            $item['category'] = $categoryAlias;
                            $data = $converter->convertObject($item);
                            if (!empty($url)) {
                                if (empty($data['GeoForm']['lat']) || empty($data['GeoForm']['lng'])) {
                                    Console::output("... skipping property #{$i} from page #{$page}");
                                    $this->_importService->registerImportWithFlags('property', 0, 'n1', $url, $flags);
                                    $offset++;
                                    $transaction->commit();
                                    continue;
                                }
                            }
                            $insertId = $this->_propertyService->create($data);

                            if ($insertId) {
                                $this->_importService->registerImportWithFlags('property', $insertId, 'n1', $item['url'] ?? null, $flags);
                                $transaction->commit();
                            } else {
                                $transaction->rollBack();
                                return false;
                            }
                        } else {
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param string $country
     * @param int $limit
     * @param bool $clean
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     * @return bool
     */
    public function actionPrian($country = null, $limit = 1, $clean = false)
    {
        if ($clean !== false) {
            $this->_importService->cleanByCondition(['source' => 'prian', 'entity' => 'user']);
        }

        if ($country !== null) {
            $dataFilePath = Yii::getAlias('@console') . "/import/prian/{$country}/property-data.php";
        } else {
            $dirs = FileHelper::findDirectories(Yii::getAlias('@console') . "/import/prian", ['recursive' => false]);
            $lastCountryPath = Yii::getAlias("@console") . "/import/prian/last-country-id.php";
            $lastCountryId = file_exists($lastCountryPath) ? include($lastCountryPath) : -1;
            $countryId = $lastCountryId + 1;
            if ($countryId >= count($dirs)) {
                $countryId = 0;
            }
            file_put_contents($lastCountryPath, "<?php \n return {$countryId};");
            $countryDir = $dirs[$countryId] ?? null;
            if (!$countryDir) {
                Console::output("Data not found");
                return false;
            }
            $dataFilePath = "{$countryDir}/property-data.php";
            $country = basename($countryDir);
        }
        $data = file_exists($dataFilePath) ? include($dataFilePath) : [];
        if (empty($data)) {
            Console::output("No data for '{$country}'");
            return false;
        }
        foreach ($data as $cityAlias => $cityData) {
            foreach ($cityData as $actionAlias => $actionData) {
                foreach ($actionData as $categoryAlias => $categoryData) {
                    Console::output("Country '{$country} 'City '$cityAlias' action '$actionAlias' category '$categoryAlias'");
                    $flags = [
                        Flag::TYPE_SOURCE_COUNTRY => $country,
                        Flag::TYPE_SOURCE_CITY => $cityAlias,
                        Flag::TYPE_SOURCE_ACTION => $actionAlias,
                        Flag::TYPE_SOURCE_CATEGORY => $categoryAlias
                    ];
                    $importCount = $this->_importService->getCountByConditionAndFlags(['import.source' => 'prian', 'import.entity' => 'property'], $flags);
                    $page = ((int)($importCount / 16)) + 1;
                    $offset = $importCount % 16;
                    /** @var PrianPropertyConverter $converter */
                    $converter = Yii::$container->get(PrianPropertyConverter::class);
                    for ($i = $offset; $i < $offset + $limit; $i++) {
                        if (!isset($categoryData[$page][$i])) {
                            break;
                        }

                        $transaction = Yii::$app->db->beginTransaction();

                        $item = $categoryData[$page][$i];
                        if (!empty($item['url'])) {
                            $url = explode('?', $item['url'])[0];
                            if (empty($item['price']) || $this->_importService->getCountByCondition(['like', 'origin', $url])) {
                                Console::output("... skipping property #{$i} from page #{$page}");
                                $this->_importService->registerImportWithFlags('property', 0, 'prian', $url, $flags);
                                $offset++;
                                $transaction->commit();
                                continue;
                            }
                        }

                        Console::output("... importing property #{$i} from page #{$page}");
                        $name = $item['userData']['name'] ?? $item['companyData']['name'] ?? null;
                        $phone = $item['userData']['phone'] ?? $item['companyData']['phone'] ?? null;
                        $email = $item['userData']['email'] ?? $item['companyData']['email'] ?? null;
                        $userId = !empty($phone) || !empty($email) ? User::find()->filterWhere(['or', ['phone' => $phone], ['email' => $email]])->select('id')->scalar() : false;
                        if ($userId === false) {
                            $userFormFields = [
                                'username' => $name,
                                'phone' => $phone,
                                'email' => $email,
                            ];
                            $pathInfo = FileHelper::createTempDirectory('@frontend');
                            if (!empty($item['userData']['avatar'])) {
                                $newAbsolutePath = $pathInfo['basePath'] . basename($item['userData']['avatar']);
                                $newRelativePath = $pathInfo['relativePath'] . basename($item['userData']['avatar']);
                                $headers = get_headers($item['userData']['avatar']);
                                if (strpos($headers[0], '200') !== false) {
                                    $file = file_get_contents($item['userData']['avatar']);
                                    if ($file) {
                                        file_put_contents($newAbsolutePath, $file);
                                        $userFormFields['avatar'] = $newRelativePath;
                                    }
                                }
                            }
                            $userId = $this->_userService->create([
                                'UserForm' => $userFormFields
                            ]);
                            if ($userId) {
                                $this->_importService->registerImport('user', $userId, 'prian');
                            }
                        }
                        if (!empty($item['companyData']['url']) && !preg_match("/prian\.ru\/private/", $item['companyData']['url'])) {
                            $name = $item['companyData']['name'] ?? null;
                            /* @var CompanyDTO $company */
                            $company = $this->_companyService->getOne(['exists',
                                Import::find()->where(['source' => 'prian', 'entity' => 'company', 'origin' => $item['companyData']['url']])->andWhere('import.entity_id = company.id')
                            ]);
                            if ($company) {
                                $userId = $company['user_id'];
                            } else {
                                $companyFormFields = [
                                    'MetaForm' => [
                                        'title' => $name,
                                        'locale' => 'ru-RU'
                                    ],
                                ];
                                $companyId = $this->_companyService->create($companyFormFields);
                                if ($companyId) {
                                    $this->_importService->registerImport('company', $companyId, 'prian', $item['companyData']['url']);
                                    if ($userId) {
                                        $this->_companyMemberService->create([
                                            'user_id' => $userId,
                                            'company_id' => $companyId,
                                            'role' => CompanyMember::ROLE_OWNER
                                        ]);
                                    }
                                    $userId = Company::find()->where(['id' => $companyId])->select('user_id')->scalar();
                                }
                            }
                        }
                        if ($userId) {
                            $item['user_id'] = $userId;
                            $item['type'] = $actionAlias;
                            $item['category'] = $categoryAlias;
                            $data = $converter->convertObject($item);
                            $insertId = $this->_propertyService->create($data);

                            if ($insertId) {
                                $this->_importService->registerImportWithFlags('property', $insertId, 'prian', $item['url'] ?? null, $flags);
                                $transaction->commit();
                            } else {
                                $transaction->rollBack();
                                return false;
                            }
                        } else {
                            if (!empty($url)) {
                                Console::output("... skipping property #{$i} from page #{$page}");
                                $this->_importService->registerImportWithFlags('property', 0, 'prian', $url, $flags);
                                $offset++;
                                $transaction->commit();
                                continue;
                            }
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }


    /**
     * @param int $limit
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     * @return bool
     */
    public function actionLuximmo($limit = 1)
    {
//        if ($clean !== false) {
//            $this->_importService->cleanByCondition(['source' => 'n1', 'entity' => 'user']);
//        }

        $dirs = FileHelper::findDirectories(Yii::getAlias('@console') . "/import/luximmo", ['recursive' => false]);
        foreach ($dirs as $countryDir) {
            $countryAlias = basename($countryDir);
            $dataFilePath = "{$countryDir}/property-data.php";
            $countryData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            foreach ($countryData as $actionAlias => $actionData) {
                foreach ($actionData as $categoryAlias => $categoryData) {
                    Console::output("Country '$countryAlias' action '$actionAlias' category '$categoryAlias'");
                    $flags = [
                        Flag::TYPE_SOURCE_COUNTRY => $countryAlias,
                        Flag::TYPE_SOURCE_ACTION => $actionAlias,
                        Flag::TYPE_SOURCE_CATEGORY => $categoryAlias
                    ];
                    $importCount = $this->_importService->getCountByConditionAndFlags(['import.source' => 'luximmo', 'import.entity' => 'property'], $flags);
                    $page = ((int)($importCount / 15)) + 1;
                    $offset = $importCount % 15;
                    /** @var LuximmoPropertyConverter $converter */
                    $converter = Yii::$container->get(LuximmoPropertyConverter::class);
                    for ($i = $offset; $i < $offset + $limit; $i++) {
                        if (!isset($categoryData[$page][$i])) {
                            break;
                        }

                        $transaction = Yii::$app->db->beginTransaction();

                        $item = $categoryData[$page][$i];
                        if (!empty($item['url'])) {
                            $url = explode('?', $item['url'])[0];
                            if (empty($item['price']) || empty($item['userData']['name']) || empty($item['userData']['phone']) || $this->_importService->getCountByCondition(['like', 'origin', $url])) {
                                Console::output("... skipping property #{$i} from page #{$page}");
                                $this->_importService->registerImportWithFlags('property', 0, 'luximmo', $url, $flags);
                                $offset++;
                                $transaction->commit();
                                continue;
                            }
                        }
                        Console::output("... importing property #{$i} from page #{$page}");

                        if (!empty($item['userData']['phone']) || !empty($item['userData']['email'])) {
                            $userId = User::find()->filterWhere(['or', ['phone' => $item['userData']['phone']], ['email' => $item['userData']['email']]])->select('id')->scalar();
                        }
                        else {
                            $userId = false;
                        }
                        if ($userId === false) {
                            $userFormFields = [
                                'username' => $item['userData']['name'],
                                'phone' => $item['userData']['phone'],
                                'email' => $item['userData']['email'],
                            ];
                            $pathInfo = FileHelper::createTempDirectory('@frontend');
                            if (!empty($item['userData']['avatar'])) {
                                $newAbsolutePath = $pathInfo['basePath'] . basename($item['userData']['avatar']);
                                $newRelativePath = $pathInfo['relativePath'] . basename($item['userData']['avatar']);
                                $headers = get_headers($item['userData']['avatar']);
                                if (strpos($headers[0], '200') !== false) {
                                    $file = file_get_contents($item['userData']['avatar']);
                                    if ($file) {
                                        file_put_contents($newAbsolutePath, $file);
                                        $userFormFields['avatar'] = $newRelativePath;
                                    }
                                }
                            }
                            $userId = $this->_userService->create([
                                'UserForm' => $userFormFields
                            ]);
                            if ($userId) {
                                $this->_importService->registerImport('user', $userId, 'luximmo');
                            }
                        }
                        if ($userId) {
                            $item['user_id'] = $userId;
                            $item['type'] = $actionAlias;
                            $item['category'] = $categoryAlias;
                            $data = $converter->convertObject($item);
                            $insertId = $this->_propertyService->create($data);
                            if ($insertId) {
                                $this->_importService->registerImportWithFlags('property', $insertId, 'luximmo', $item['url'] ?? null, $flags);
                                $transaction->commit();
                            } else {
                                $transaction->rollBack();
                                return false;
                            }
                        } else {
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param $source
     * @param int $limit
     * @return bool
     * @throws InvalidConfigException
     */
    public function actionFeed($source, $limit = 100)
    {
        $dir = Yii::getAlias("@console") . "/import/feed/$source";
        $dataFilePath = "$dir/data.php";
        if (!file_exists($dataFilePath)) {
            throw new InvalidConfigException('Unknown source');
        }
        $data = file_exists($dataFilePath) ? include($dataFilePath) : [];
        if (empty($data)) {
            Console::output("No data for '{$source}'");
            return false;
        }

        $lastIndexPath = "$dir/last-index.php";
        $lastIndex = file_exists($lastIndexPath) ? include($lastIndexPath) : 0;

        /** @var FeedPropertyConverter $propertyConverter */
        $propertyConverter = Yii::$container->get(FeedPropertyConverter::class);
//        /** @var FeedUserConverter $userConverter */
        for ($i = $lastIndex; $i < $lastIndex + $limit; $i++) {
            if (!isset($data['offer'][$i])) {
                file_put_contents($lastIndexPath, "<?php \n return 0;");
                break;
            }

            Console::output("Property #'$i from '$source'");
            $item = $data['offer'][$i];
            if (!empty($item['url'])) {
//                $updatedAt = strtotime($item['last-update-date']);
                if (empty($item['price']['value']) || $this->_importService->getCountByCondition(['like', 'origin', $item['url']])) {
                    Console::output("... skipping");
                    continue;
                }
            }
            Console::output("... importing");

            $transaction = Yii::$app->db->beginTransaction();
            $item['source'] = $source;
            $propertyData = $propertyConverter->convertObject($item);
            if (!empty($propertyData)) {
                if (!empty($propertyData['GeoForm']['lat']) && !empty($propertyData['GeoForm']['lng']) && !empty($propertyData['buildingData'])) {
                    $building = $this->_buildingService->getOne(['building.lat' => $propertyData['GeoForm']['lat'], 'building.lng' => $propertyData['GeoForm']['lng']]);
                    if (!$building) {
                        $this->_buildingService->create($propertyData['buildingData']);
                    }
                }
                $insertId = $this->_propertyService->create($propertyData);
                if (!empty($insertId)) {
                    $this->_importService->registerImport('property', $insertId, $source, $propertyData['url'] ?? null);
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    return false;
                }
            } else {
                if (!empty($item['url'])) {
                    Console::output("... skipping");
                    $this->_importService->registerImportWithFlags('property', 0, $source, $item['url']);
                    $transaction->commit();
                    continue;
                }
                $transaction->rollBack();
                return false;
            }
        }
        file_put_contents($lastIndexPath, "<?php \n return $i;");

        return true;
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionYandex($limit = 10)
    {
        $dirs = FileHelper::findDirectories(Yii::getAlias('@console') . "/import/yandex", ['recursive' => false]);
        /** @var YandexPropertyConverter $converter */
        $converter = Yii::$container->get(YandexPropertyConverter::class);
        foreach ($dirs as $cityDir) {
            $cityAlias = basename($cityDir);
            $dataFilePath = "{$cityDir}/property-data.php";
            $cityData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            $nextBuildingPath = Yii::getAlias("@console") . "/import/yandex/{$cityAlias}/property-next-building.php";
            $nextSuccessfulBuilding = $nextBuilding = file_exists($nextBuildingPath) ? include($nextBuildingPath) : ['page' => 1, 'building' => 0];
            Console::output("Loading city '$cityAlias'");

            $flags = [
                Flag::TYPE_SOURCE_CITY => $cityAlias,
                Flag::TYPE_SOURCE_PAGE => (string)$nextBuilding['page'],
                Flag::TYPE_SOURCE_BUILDING => (string)$nextBuilding['building']
            ];
            $offset = $this->_importService->getCountByConditionAndFlags(['import.source' => 'yandex', 'import.entity' => 'property'], $flags) * 5;

            for ($i = 0; $i < $limit; $i++) {
                if (empty($cityData[$nextBuilding['page']][$nextBuilding['building']][$offset])) {
                    Console::output("... end of building");
                    $nextBuilding['building']++;
                    $flags[Flag::TYPE_SOURCE_BUILDING] = (string)$nextBuilding['building'];
                    $offset = 0;
                }
                if (!isset($cityData[$nextBuilding['page']][$nextBuilding['building']])) {
                    Console::output("... end of page");
                    $nextBuilding['page']++;
                    $nextBuilding['building'] = 0;
                    $flags[Flag::TYPE_SOURCE_PAGE] = (string)$nextBuilding['page'];
                    $flags[Flag::TYPE_SOURCE_BUILDING] = '0';
                    $offset = 0;
                }
                if (!isset($cityData[$nextBuilding['page']])) {
                    Console::output("... no more data");
                    break;
                }
                if (empty($cityData[$nextBuilding['page']][$nextBuilding['building']][$offset])) {
                    $i--;
                    continue;
                }
                $nextSuccessfulBuilding = $nextBuilding;
                $transaction = Yii::$app->db->beginTransaction();
                Console::output("... page #'{$nextBuilding['page']}' building #'{$nextBuilding['building']}' property #'{$offset}'");
                $item = $cityData[$nextBuilding['page']][$nextBuilding['building']][$offset];
                if (empty($item['price']['value']) || (!empty($item['url']) && $this->_importService->getCountByCondition(['like', 'origin', $item['url']]))) {
                    $this->_importService->registerImportWithFlags('property', 0, 'yandex', $item['url'], $flags);
                    $offset+=5;
                    $transaction->commit();
                    Console::output("...... skipping");
                    continue;
                }
                $item['user_id'] = 15927;
                $data = $converter->convertObject($item);
                $insertId = $this->_propertyService->create($data);
                if ($insertId) {
                    $this->_importService->registerImportWithFlags('property', $insertId, 'yandex', $item['url'] ?? null, $flags);
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                    return false;
                }
                $offset+=5;
            }
            file_put_contents($nextBuildingPath, "<?php \n return " . VarDumper::export($nextSuccessfulBuilding) . ";");
        }

        return true;
    }

    /**
     * @return bool
     */
    public function actionYandex2()
    {
        $dirs = FileHelper::findDirectories(Yii::getAlias('@console') . "/import/yandex", ['recursive' => false]);
        /** @var YandexPropertyConverter $converter */
        $converter = Yii::$container->get(YandexPropertyConverter::class);
        foreach ($dirs as $cityDir) {
            $cityAlias = basename($cityDir);
            $dataFilePath = "{$cityDir}/property-data.php";
            $cityData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            Console::output("Loading city '$cityAlias'");

            $buildingCount = $this->_importService->getCountByConditionAndFlags(['import.source' => 'yandex', 'import.entity' => 'building'], [Flag::TYPE_SOURCE_CITY => $cityAlias]);
            $buildingPage = ((int)($buildingCount / 12)) + 1;
            $buildingOffset = $buildingCount % 12;

            foreach ($cityData as $page => $pageData) {
                if ($page > $buildingPage) {
                    break;
                }
                foreach ($pageData as $building => $buildingData) {
                    if ($building >= $buildingOffset) {
                        break;
                    }
                    $flags = [
                        Flag::TYPE_SOURCE_CITY => $cityAlias,
                        Flag::TYPE_SOURCE_PAGE => (string)$page,
                        Flag::TYPE_SOURCE_BUILDING => (string)$building
                    ];
                    $importCount = $this->_importService->getCountByConditionAndFlags(['import.source' => 'yandex', 'import.entity' => 'property'], $flags) * 5;
                    if (isset($buildingData[$importCount])) {
                        $transaction = Yii::$app->db->beginTransaction();
                        Console::output("... page #'{$page}' building #'{$building}' property #'{$importCount}'");
                        $item = $buildingData[$importCount];
                        if (empty($item['price']['value']) || (!empty($item['url']) && $this->_importService->getCountByCondition(['like', 'origin', $item['url']]))) {
                            $this->_importService->registerImportWithFlags('property', 0, 'yandex', $item['url'], $flags);
                            $transaction->commit();
                            Console::output("...... skipping");
                            continue;
                        }
                        $item['user_id'] = 15927;
                        $data = $converter->convertObject($item);
                        $insertId = $this->_propertyService->create($data);
                        if ($insertId) {
                            $this->_importService->registerImportWithFlags('property', $insertId, 'yandex', $item['url'] ?? null, $flags);
                            $transaction->commit();
                        } else {
                            $transaction->rollBack();
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionFake($limit = 10)
    {
        $converter = Yii::$container->get(FakePropertyConverter::class);

        $transaction = Yii::$app->db->beginTransaction();
        for ($i = 0; $i < $limit; $i++) {
            Console::output("Generating property #{$i}");
            $userId = User::find()->where(['status' => User::STATUS_ACTIVE])->select('id')->orderBy(new Expression("rand()"))->scalar();
            $categoryId = Category::find()->where(['lvl' => 2])->select('id')->orderBy(new Expression("rand()"))->scalar();
            $convertedData = $converter->convertObject(['user_id' => $userId, 'category_id' => $categoryId]);
            if (!empty($convertedData)) {
                $propertyId = $this->_propertyService->create($convertedData);
                if ($propertyId) {
                    $this->_importService->registerImport('property', $propertyId, 'fake');
                }
                else {
                    $transaction->rollBack();
                    return false;
                }
            }
        }
        $transaction->commit();

        return true;
    }
}