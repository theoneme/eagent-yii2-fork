<?php

namespace console\controllers\import;

use common\helpers\FileHelper;
use common\models\Building;
use common\models\Company;
use common\models\elastic\BuildingElastic;
use common\models\elastic\PropertyElastic;
use common\models\Flag;
use common\models\Import;
use common\models\Property;
use common\models\User;
use common\services\ApiClientService;
use common\services\entities\BuildingService;
use common\services\entities\CompanyService;
use common\services\entities\UserService;
use console\services\fake\FakeBuildingConverter;
use console\services\ImportRegisterService;
use console\services\LuximmoPropertyConverter;
use console\services\YandexBuildingConverter;
use Yii;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\Console;

/**
 * Class BuildingController
 * @package console\controllers
 */
class BuildingController extends Controller
{
    /**
     * @var ApiClientService
     */
    private $_apiClientService;
    /**
     * @var ImportRegisterService
     */
    private $_importService;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var CompanyService
     */
    private $_companyService;

    /**
     * PropertyController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param ApiClientService $apiClientService
     * @param ImportRegisterService $importRegisterService
     * @param BuildingService $buildingService
     * @param UserService $userService
     * @param CompanyService $companyService
     * @param array $config
     */
    public function __construct($id, $module,
                                ApiClientService $apiClientService,
                                ImportRegisterService $importRegisterService,
                                BuildingService $buildingService,
                                UserService $userService,
                                CompanyService $companyService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_apiClientService = $apiClientService;
        $this->_importService = $importRegisterService;
        $this->_buildingService = $buildingService;
        $this->_userService = $userService;
        $this->_companyService = $companyService;
    }

    /**
     * @param int $limit
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     * @return bool
     */
    public function actionYandex($limit = 1)
    {
//        if ($clean !== false) {
//            $this->_importService->cleanByCondition(['source' => 'n1', 'entity' => 'user']);
//        }

        $dirs = FileHelper::findDirectories(Yii::getAlias('@console') . "/import/yandex", ['recursive' => false]);
        foreach ($dirs as $cityDir) {
            $cityAlias = basename($cityDir);
            $dataFilePath = "{$cityDir}/building-data.php";
            $cityData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            Console::output("City '$cityAlias'");
            $flags = [Flag::TYPE_SOURCE_CITY => $cityAlias];
            $importCount = $this->_importService->getCountByConditionAndFlags(['import.source' => 'yandex', 'import.entity' => 'building'], $flags);
            $page = ((int)($importCount / 12)) + 1;
            $offset = $importCount % 12;
            /** @var YandexBuildingConverter $converter */
            $converter = Yii::$container->get(YandexBuildingConverter::class);
            for ($i = $offset; $i < $offset + $limit; $i++) {
                if (!isset($cityData[$page][$i])) {
                    break;
                }
                $item = $cityData[$page][$i];
                if (!empty($item['url'])) {
//                    $url = explode('?', $item['url'])[0];
                    if ($this->_importService->getCountByCondition(['like', 'origin', $item['url']])) {
                        Console::output("... skipping building #{$i} from page #{$page}");
                        $this->_importService->registerImportWithFlags('building', 0, 'yandex', $item['url'], $flags);
                        $offset++;
                        continue;
                    }
                }
                Console::output("... importing building #{$i} from page #{$page}");
                $transaction = Yii::$app->db->beginTransaction();

//                $userId = User::find()->where(['or', ['phone' => $item['userData']['phone']], ['email' => $item['userData']['email']]])->select('id')->scalar();
//                if ($userId === false) {
//                    $userFormFields = [
//                        'username' => $item['userData']['name'],
//                        'phone' => $item['userData']['phone'],
//                        'email' => $item['userData']['email'],
//                    ];
//                    $pathInfo = FileHelper::createTempDirectory('@frontend');
//                    if (!empty($item['userData']['avatar'])) {
//                        $newAbsolutePath = $pathInfo['basePath'] . basename($item['userData']['avatar']);
//                        $newRelativePath = $pathInfo['relativePath'] . basename($item['userData']['avatar']);
//                        $headers = get_headers($item['userData']['avatar']);
//                        if (strpos($headers[0], '200') !== false) {
//                            $file = file_get_contents($item['userData']['avatar']);
//                            if ($file) {
//                                file_put_contents($newAbsolutePath, $file);
//                                $userFormFields['avatar'] = $newRelativePath;
//                            }
//                        }
//                    }
//                    $userId = $this->_userService->create([
//                        'UserForm' => $userFormFields
//                    ]);
//                    if ($userId) {
//                        $this->_importService->registerImport('user', $userId, 'luximmo');
//                    }
//                }
//                if ($userId) {
                    $item['user_id'] = 15927;
//                    $item['user_id'] = $userId;
//                    $item['type'] = $actionAlias;
//                    $item['category'] = $categoryAlias;
                    $data = $converter->convertObject($item);
                    $insertId = $this->_buildingService->create($data);

                    if ($insertId) {
                        $this->_importService->registerImportWithFlags('building', $insertId, 'yandex', $item['url'] ?? null, $flags);
                    } else {
                        $transaction->rollBack();
                        return false;
                    }
//                } else {
//                    $transaction->rollBack();
//                    return false;
//                }
                $transaction->commit();
            }
        }
        return true;
    }

    /**
     * @return bool
     */
    public function actionTransferYandexObjects()
    {

        $dirs = FileHelper::findDirectories(Yii::getAlias('@console') . "/import/yandex", ['recursive' => false]);
        foreach ($dirs as $cityDir) {
            $cityAlias = basename($cityDir);
            $dataFilePath = "{$cityDir}/building-data.php";
            $cityData = file_exists($dataFilePath) ? include($dataFilePath) : [];
            $nextTransferPagePath = Yii::getAlias("@console") . "/import/yandex/{$cityAlias}/next-transfer-page.php";
            $nextTransferPage = file_exists($nextTransferPagePath) ? include($nextTransferPagePath) : 1;
            Console::output("City '$cityAlias'");
            $pageData = $cityData[$nextTransferPage] ?? [];
            $pathInfo = FileHelper::createTempDirectory('@frontend');
            if (!empty($pageData)) {
                Console::output("... page #'$nextTransferPage'");
                $success = false;
                foreach ($pageData as $id => $buildingData) {
                    Console::output("...... building #'$id'");
                    if (!empty($buildingData['url'])) {
                        $building = $this->_buildingService->getOne(['exists',
                            Import::find()
                                ->where(['source' => 'yandex', 'entity' => 'building', 'origin' => $buildingData['url']])
                                ->andWhere('import.entity_id = building.id')
                        ]);
                        if ($building !== null) {
                            $company = $this->_companyService->getOne(['exists',
                                Import::find()->where(['source' => 'yandex', 'entity' => 'company', 'origin' => $buildingData['companyData']['id']])->andWhere('import.entity_id = company.id')
                            ]);
                            if ($company) {
                                $userId = $company['user_id'];
                            } else {
                                $companyFormFields = [
                                    'MetaForm' => [
                                        'title' => $buildingData['companyData']['name'] ?? null,
                                        'locale' => 'ru-RU',
                                    ],
                                    'CompanyForm' => [
                                        'type' => Company::TYPE_DEVELOPER
                                    ]
                                ];
                                if (!empty($buildingData['companyData']['logo'])) {
                                    $imagePath = FileHelper::downloadFile($buildingData['companyData']['logo'], $pathInfo);
                                    if ($imagePath) {
                                        $companyFormFields['CompanyForm']['logo'] = $imagePath;
                                    }
                                }
                                $companyId = $this->_companyService->create($companyFormFields);
                                if ($companyId) {
                                    $this->_importService->registerImport('company', $companyId, 'yandex', (string)$buildingData['companyData']['id']);
                                    $userId = Company::find()->where(['id' => $companyId])->select('user_id')->scalar();
                                }
                            }
                            if (!empty($userId)) {
                                $success = true;
                                Console::output("......... transferring building {$building['id']} to user {$userId}");
                                Building::updateAll(['user_id' => $userId], ['id' => $building['id']]);
                                BuildingElastic::updateAll(['user_id' => $userId], ['id' => $building['id']]);
                                Property::updateAll(['user_id' => $userId], ['building_id' => $building['id']]);
                                PropertyElastic::updateAll(['user_id' => $userId], ['building_id' => $building['id']]);
                            }
                        }
                    }
                }
                if ($success) {
                    $nextTransferPage++;
                }
            }
            file_put_contents($nextTransferPagePath, "<?php \n return " . $nextTransferPage . ";");
        }
        return true;
    }


    /**
     * @param int $limit
     * @return bool
     */
    public function actionFake($limit = 10)
    {
        $converter = Yii::$container->get(FakeBuildingConverter::class);

        $transaction = Yii::$app->db->beginTransaction();
        for ($i = 0; $i < $limit; $i++) {
            Console::output("Generating building #{$i}");
            $userId = User::find()->where(['status' => User::STATUS_ACTIVE])->select('id')->orderBy(new Expression("rand()"))->scalar();
            $convertedData = $converter->convertObject(['user_id' => $userId]);
            if (!empty($convertedData)) {
                $buildingId = $this->_buildingService->create($convertedData);
                if ($buildingId) {
                    $this->_importService->registerImport('building', $buildingId, 'fake');
                }
                else {
                    $transaction->rollBack();
                    return false;
                }
            }
        }
        $transaction->commit();

        return true;
    }
}