<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.08.2018
 * Time: 12:00
 */

namespace console\controllers\import;

use common\models\User;
use common\services\ApiClientService;
use common\services\entities\UserService;
use console\services\ImportRegisterService;
use console\services\UjobsUserConverter;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class AgentController
 * @package console\controllers\import
 */
class AgentController extends Controller
{
    /**
     * @var ApiClientService
     */
    private $_apiClientService;
    /**
     * @var ImportRegisterService
     */
    private $_importService;
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * AgentController constructor.
     * @param $id
     * @param $module
     * @param ApiClientService $apiClientService
     * @param ImportRegisterService $importRegisterService
     * @param UserService $userService
     * @param array $config
     */
    public function __construct($id, $module,
                                ApiClientService $apiClientService,
                                ImportRegisterService $importRegisterService,
                                UserService $userService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_apiClientService = $apiClientService;
        $this->_importService = $importRegisterService;
        $this->_userService = $userService;
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionRealtors($limit = 10)
    {
        $userData = $this->_apiClientService->makeRequest('get', "https://ujobs.me/apientry/profiles", [
            'primitive-secure' => 'bratishka-pokushat',
            'expand' => 'profile.translations,userAttributes.attrValueDescriptions',
            'specialty' => 'rieltor',
            'skill' => ['rieltor', 'agent-po-nedvizhimosti'],
            'pageSize' => $limit
        ]);

        if (!empty($userData)) {
            $transaction = Yii::$app->db->beginTransaction();

            $userConverter = new UjobsUserConverter(Yii::$app->s3uJobs);
            foreach ($userData as $item) {
                $emailCondition = !empty($item['email']) ? ['email' => $item['email']] : ['or', ['email' => null], ['email' => '']];
                $phoneCondition = !empty($item['phone']) ? ['phone' => $item['phone']] : ['or', ['phone' => null], ['phone' => '']];
                $user = $this->_userService->getOne(['and', $emailCondition, $phoneCondition]);

                $item['type'] = User::TYPE_REALTOR;
                $userData = $userConverter->convertObject($item);
                if ($user === null) {
                    $userId = $this->_userService->create($userData);

                    if ($userId === null) {
//                        $transaction->rollBack();
                        Console::output("Saving user {$item['email']} failed");
//                        return false;
                    }

                    $this->_importService->registerImport('user', $userId, 'ujobs', (string)$item['id']);
                } else {
                    $this->_userService->update($userData, ['id' => $user['id']]);
                }
            }

            $transaction->commit();
        }

        return true;
    }
}