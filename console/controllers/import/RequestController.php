<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.08.2018
 * Time: 12:00
 */

namespace console\controllers\import;

use common\models\Request;
use common\models\Translation;
use common\services\ApiClientService;
use common\services\entities\RequestService;
use common\services\entities\UserService;
use console\services\ImportRegisterService;
use console\services\UjobsRequestConverter;
use console\services\UjobsUserConverter;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class RequestController
 * @package console\controllers\import
 */
class RequestController extends Controller
{
    /**
     * @var ApiClientService
     */
    private $_apiClientService;
    /**
     * @var ImportRegisterService
     */
    private $_importService;
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * RequestController constructor.
     * @param $id
     * @param $module
     * @param ApiClientService $apiClientService
     * @param ImportRegisterService $importRegisterService
     * @param UserService $userService
     * @param array $config
     */
    public function __construct($id, $module,
                                ApiClientService $apiClientService,
                                ImportRegisterService $importRegisterService,
                                UserService $userService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_apiClientService = $apiClientService;
        $this->_importService = $importRegisterService;
        $this->_userService = $userService;
    }

    /**
     * @param int $limit
     * @param bool $clean
     * @return bool
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionRequests($limit = 5, $clean = false)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $result = true;
        if ($clean !== false) {
            $result = $this->_importService->cleanByCondition(['source' => 'ujobs-requests']);
        }
        $categories = Translation::find()
            ->where(['key' => 'slug', 'entity' => Translation::ENTITY_CATEGORY, 'locale' => 'ru-RU'])
            ->indexBy('value')
            ->select('entity_id')
            ->asArray()
            ->column();
        $catToCat = [
            2478 => $categories['flats'],
            2480 => $categories['houses'],
            2481 => $categories['land'],
            2778 => $categories['commercial-property'],
        ];

        $requestConverter = new UjobsRequestConverter(
            $catToCat,
            Yii::$app->s3uJobs,
            Yii::$container->get(RequestService::class)
        );
        $userConverter = new UjobsUserConverter(Yii::$app->s3uJobs);

        $entityAliases = [];
        $data = $this->_apiClientService->makeRequest('get', 'https://ujobs.me/apientry/products', [
            'category_id' => array_keys($catToCat),
            'type' => 'tender',
            'status' => '-50',
            'expand' => 'translation,attachments',
            'pageSize' => $limit
        ]);

        if ($data === false) {
            $transaction->rollBack();
            Console::output('Request for fetching products failed');
            return false;
        }

        foreach ($data as $item) {
            $entityAliases[] = $item['alias'];
            $request = Request::find()->where(['slug' => $item['alias']])->one();

            if ($request instanceof Request) {
                // Update
            } else {
                $userData = $this->_apiClientService->makeRequest('get', "https://ujobs.me/apientry/profiles/{$item['user_id']}", [
                    'primitive-secure' => 'bratishka-pokushat',
                    'expand' => 'profile',
                ]);

                $user = $this->_userService->getOne(['email' => $userData['email']]);
                $userId = $user['id'];

                if ($user === null) {
                    $userDataConverted = $userConverter->convertObject($userData);
                    $userId = $this->_userService->create($userDataConverted);

                    if ($userId === null) {
                        $transaction->rollBack();
                        Console::output("Saving user {$userData['email']} failed");
                        return false;
                    }

                    $result = $result && $this->_importService->registerImport('user', $userId, 'ujobs-requests', (string)$userData['id']);
                }

                $item['user_id'] = $userId;
                $requestId = $requestConverter->convertObject($item);
                $result = $result && $requestId !== null;
            }

            if ($result === false) {
                Console::output("Property {$item['alias']} went wrong. Rollback. Warns: {$result}");

                $transaction->rollBack();
                return false;
            }
        }

        if ($result === true) {
            $transaction->commit();

        } else {
            $transaction->rollBack();
        }

        return $result;
    }
}