<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.05.2017
 * Time: 18:28
 */

namespace console\controllers\import;

use common\models\Currency;
use common\models\CurrencyExchange;
use Exception;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\httpclient\Client;

/**
 * Class CurrencyController
 * @package console\controllers\import
 */
class CurrencyController extends Controller
{
    /**
     * @return bool
     */
    public function actionUpdateCurrencies()
    {
        $currencies = Currency::find()->select('code')->where(['not', ['code' => 'CTY']])->column();
        $client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);

        try {
            $response = $client->get('http://www.apilayer.net/api/live', [
                'access_key' => Yii::$app->params['currencyApiKey'],
                'currencies' => implode(',', $currencies),
                'format' => 1
            ])->send();
            $responseData = $response->getData();
            $isOk = $response->isOk;
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            return false;
        }

        if ($isOk && $responseData['success'] === true && !empty($responseData['quotes'])) {
            $items = $responseData['quotes'];
            $items["USDUSD"] = 1;
            foreach ($currencies as $from) {
                foreach ($currencies as $to) {
                    if ($from === $to) {
                        continue;
                    }

                    Console::output("Processing {$from} to {$to}");

                    $ratio = round($items['USD' . $to] / $items['USD' . $from], 4);
                    $exchange = CurrencyExchange::find()->where(['code_from' => $from, 'code_to' => $to])->one();
                    if ($exchange === null) {
                        $exchange = new CurrencyExchange(['code_from' => $from, 'code_to' => $to]);
                    }
                    $exchange->ratio = $ratio;
                    $exchange->save();
                }
            }

            return true;
        }

        Yii::error("\n*************************\n Ошибка при обновлении курсов валют \n*************************\n");

        return false;
    }
}