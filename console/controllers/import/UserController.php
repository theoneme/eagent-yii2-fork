<?php

namespace console\controllers\import;

use common\models\User;
use common\services\entities\UserService;
use console\services\fake\FakeUserConverter;
use console\services\ImportRegisterService;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class CompanyController
 * @package console\controllers
 */
class UserController extends Controller
{
    /**
     * @var ImportRegisterService
     */
    private $_importService;
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * UserController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param ImportRegisterService $importRegisterService
     * @param UserService $userService
     * @param array $config
     */
    public function __construct($id, $module,
                                ImportRegisterService $importRegisterService,
                                UserService $userService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_importService = $importRegisterService;
        $this->_userService = $userService;
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionFake($limit = 10)
    {
        $converter = Yii::$container->get(FakeUserConverter::class);

        $transaction = Yii::$app->db->beginTransaction();
        for ($i = 0; $i < $limit; $i++) {
            Console::output("Generating user #{$i}");
            $convertedData = $converter->convertObject(null);
            if (!empty($convertedData)) {
                $userId = User::find()->where(['or', ['phone' => $convertedData['UserForm']['phone']], ['email' => $convertedData['UserForm']['email']]])->select('id')->scalar();
                if ($userId === false) {
                    $userId = $this->_userService->create($convertedData);
                    if ($userId) {
                        $this->_importService->registerImport('user', $userId, 'fake');
                    }
                    else {
                        $transaction->rollBack();
                        return false;
                    }
                }
            }
        }
        $transaction->commit();

        return true;
    }
}