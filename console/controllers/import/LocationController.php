<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.08.2018
 * Time: 12:00
 */

namespace console\controllers\import;

use common\dto\CityDTO;
use common\helpers\UtilityHelper;
use common\models\Translation;
use common\repositories\sql\CityRepository;
use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\RegionService;
use common\services\GoogleMapsService;
use console\services\ImportRegisterService;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\StringHelper;

/**
 * Class LocationController
 * @package console\controllers\import
 */
class LocationController extends Controller
{
    /**
     * @var ImportRegisterService
     */
    private $_importService;
    /**
     * @var CityRepository
     */
    private $_countryService;
    /**
     * @var RegionService
     */
    private $_regionService;
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var GoogleMapsService
     */
    private $_googleMapsService;

    /**
     * LocationController constructor.
     * @param $id
     * @param $module
     * @param ImportRegisterService $importRegisterService
     * @param CityService $cityService
     * @param CountryService $countryService
     * @param RegionService $regionService
     * @param GoogleMapsService $googleMapsService
     * @param array $config
     */
    public function __construct($id, $module,
                                ImportRegisterService $importRegisterService,
                                CityService $cityService,
                                CountryService $countryService,
                                RegionService $regionService,
                                GoogleMapsService $googleMapsService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_importService = $importRegisterService;
        $this->_cityService = $cityService;
        $this->_regionService = $regionService;
        $this->_countryService = $countryService;
        $this->_googleMapsService = $googleMapsService;
    }

    /**
     * @param int $limit
     * @param bool $clean
     * @return bool
     */
    public function actionCities($clean = false)
    {
        $success = true;
        if ($clean !== false) {
            $russia = $this->_countryService->getOne(['code' => 'ru']);

            $success = $this->_importService->cleanByCondition(['and', ['source' => 'location-db'], ['not', ['entity_id' => $russia['id']]]]);
        }

        $data = [];

        $csv = array_map('str_getcsv', file(Yii::getAlias('@console') . '/import/google-city.csv'));
        foreach ($csv as $key => $line) {
            if ($key > 0) {
                if($line[4] === 'RU') {
                    continue;
                }

                $fullName = $line[2];

                $nameParts = explode(',', $fullName);
                if (count($nameParts) !== 3) continue;

                $data[$nameParts[2]]['title'] = $nameParts[2];
                $data[$nameParts[2]]['code'] = $line[4];
                $data[$nameParts[2]]['regions'][$nameParts[1]]['title'] = $nameParts[1];
                $data[$nameParts[2]]['regions'][$nameParts[1]]['cities'][] = $nameParts[0];
            }
        }

        foreach ($data as $country) {
            Yii::$app->db->createCommand()->insert('country', [
                'code' => $country['code']
            ])->execute();
            $countryId = Yii::$app->db->lastInsertID;
            Yii::$app->db->createCommand()->batchInsert('translation', ['entity', 'entity_id', 'value', 'key', 'locale'], [
                [
                    Translation::ENTITY_COUNTRY,
                    $countryId,
                    $country['title'],
                    Translation::KEY_TITLE,
                    'en-GB'
                ],
            ])->execute();
            $this->_importService->registerImport('country', $countryId, 'location-db');
            foreach ($country['regions'] as $region) {
                Yii::$app->db->createCommand()->insert('region', [
                    'country_id' => $countryId,
                    'code' => StringHelper::truncate($region['title'], 2, '')
                ])->execute();
                $regionId = Yii::$app->db->lastInsertID;
                Yii::$app->db->createCommand()->batchInsert('translation', ['entity', 'entity_id', 'value', 'key', 'locale'], [
                    [
                        Translation::ENTITY_REGION,
                        $regionId,
                        $region['title'],
                        Translation::KEY_TITLE,
                        'en-GB'
                    ],
                ])->execute();
                foreach ($region['cities'] as $city) {
                    Console::output("Processing city {$city}");
                    $slug = UtilityHelper::transliterate($city);
                    $existentCity = Yii::$app->db->createCommand("SELECT slug FROM `city` WHERE slug = :slug", [
                        'slug' => $slug,
                    ])->queryOne();
                    if ($existentCity) {
                        $slug = UtilityHelper::transliterate($region['title']) . '-' . $slug;
                    }
                    $existentCity = Yii::$app->db->createCommand("SELECT slug FROM `city` WHERE slug = :slug", [
                        'slug' => $slug,
                    ])->queryOne();
                    if ($existentCity) {
                        $slug = UtilityHelper::transliterate($country['title']) . '-' . $slug;
                    }

                    Yii::$app->db->createCommand()->insert('city', [
                        'region_id' => $regionId,
                        'country_id' => $countryId,
                        'slug' => $slug,
//                        'lat' => $city['latitude'],
//                        'lng' => $city['longitude'],
                    ])->execute();
                    $cityId = Yii::$app->db->lastInsertID;
                    Yii::$app->db->createCommand()->batchInsert('translation', ['entity', 'entity_id', 'value', 'key', 'locale'], [
                        [
                            Translation::ENTITY_CITY,
                            $cityId,
                            $city,
                            Translation::KEY_TITLE,
                            'en-GB'
                        ],
                    ])->execute();
                }
            }
        }

        if ($success === true) {
//            $transaction->commit();
            return true;
        }

//        $transaction->rollBack();
        return false;
    }

    /**
     * @param bool $reverse
     * @return bool
     */
    public function actionNewLargeCities($reverse = false)
    {
        $success = true;

        $csv = array_map('str_getcsv', file(Yii::getAlias('@console') . '/import/world-large-cities.csv'));
        foreach ($csv as $key => $line) {
            if ($key > 0) {
                $cityName = $line[0];

                $city = $this->_cityService->getOne(['city_translations.value' => $cityName]);
                if($city !== null) {
                    Console::output("Processing city {$city['title']}");
                    if($reverse !== false) {
                        $this->_cityService->update(['CityForm' => ['is_big' => false]], ['id' => $city['id']]);
                    } else {
                        $this->_cityService->update(['CityForm' => ['is_big' => true]], ['id' => $city['id']]);
                    }
                }
            }
        }

        if ($success === true) {
//            $transaction->commit();
            return true;
        }

//        $transaction->rollBack();
        return false;
    }

    /**
     * @param int $limit
     * @param bool $clean
     * @return bool
     */
    public function actionLocation($limit = 5, $clean = false)
    {
        $exceptions = [
            'af', 'ag', 'aw', 'az', 'an', 'ai', 'bb', 'bd', 'bf', 'bh', 'bi', 'bj', 'bm', 'bn', 'bt', 'cc', 'cd', 'ck', 'cv', 'cx', 'dj', 'eh', 'er', 'fk', 'fm', 'fo', 'ga', 'gb', 'gg', 'gf', 'gl', 'gp', 'gq', 'gs', 'gw', 'gy', 'iq', 'ir', 'ki', 'km', 'kn', 'lb', 'lc', 'ls', 'mg', 'mh', 'ml', 'mm', 'mo', 'mp', 'mq', 'ms', 'mw', 'mz', 'nc', 'nf', 'ni', 'nr', 'nu', 'om', 'pf', 'pg', 'pk', 'pm', 'pn', 'ps', 'pw', 're', 'ru', 'rw', 'sb', 'sh', 'sj', 'sl', 'sr', 'st', 'sv', 'tc', 'tk', 'tv', 'tz', 'vc', 'wf', 'zr', 'yt', 'ws'
        ];

        $success = true;
        if ($clean !== false) {
            $russia = $this->_countryService->getOne(['code' => 'ru']);

            $success = $this->_importService->cleanByCondition(['and', ['source' => 'location-db'], ['not', ['entity_id' => $russia['id']]]]);
        }

        $count = 0;
        $countries = Yii::$app->db2->createCommand("SELECT * FROM `countries` WHERE code <> 'ru' and name <> '' and name is not null")->queryAll();
        foreach ($countries as $country) {
            if ($count >= $limit) {
                Console::output('Limit done');
                break;
            }

            if (in_array($country['code'], $exceptions)) {
                continue;
            }

            $existentCountry = $this->_countryService->getOne(['country_translations.value' => $country['name'], 'country_translations.key' => Translation::KEY_TITLE]);
            if ($existentCountry !== null) {
                continue;
            }

            $count++;

            Console::output("Processing country {$country['name']}");
            Yii::$app->db->createCommand()->insert('country', [
                'code' => $country['code']
            ])->execute();
            $countryId = Yii::$app->db->lastInsertID;
            Yii::$app->db->createCommand()->batchInsert('translation', ['entity', 'entity_id', 'value', 'key', 'locale'], [
                [
                    Translation::ENTITY_COUNTRY,
                    $countryId,
                    $country['name'],
                    Translation::KEY_TITLE,
                    'en-GB'
                ],
            ])->execute();
            $this->_importService->registerImport('country', $countryId, 'location-db');
            $regions = Yii::$app->db2->createCommand("SELECT * FROM `regions` WHERE name <> '' and name is not null and country_id = {$country['id']}")->queryAll();
            foreach ($regions as $region) {
                Console::output("Processing region {$region['name']}");
                Yii::$app->db->createCommand()->insert('region', [
                    'country_id' => $countryId,
                    'code' => $region['code']
                ])->execute();
                $regionId = Yii::$app->db->lastInsertID;
                Yii::$app->db->createCommand()->batchInsert('translation', ['entity', 'entity_id', 'value', 'key', 'locale'], [
                    [
                        Translation::ENTITY_REGION,
                        $regionId,
                        $region['name'],
                        Translation::KEY_TITLE,
                        'en-GB'
                    ],
                ])->execute();
                $cities = Yii::$app->db2->createCommand("SELECT * FROM `cities` WHERE name <> '' and name is not null and region_id = {$region['id']}")->queryAll();

                foreach ($cities as $city) {
//                    $city['name'] = trim(str_replace(['(', ')'], '', $city['name']));
                    $city['name'] = preg_replace('/[^\w ]/', '', $city['name']);
                    if (mb_strlen($city['name']) < 4) {
                        Console::output('City name is too short');
                    }
                    $existentCity = Yii::$app->db->createCommand("SELECT id FROM `city` WHERE lat = :lat and lng = :lng", [
                        'lat' => $city['latitude'],
                        'lng' => $city['longitude'],
                    ])->queryOne();
                    if ($existentCity) {
                        Console::output("City {$city['name']} found");
                        continue;
                    }

                    $existentCity = Yii::$app->db->createCommand("SELECT value FROM `translation` WHERE entity = :entity and value = :value and `key` = :key", [
                        'entity' => Translation::ENTITY_CITY,
                        'value' => UtilityHelper::transliterate($city['name']),
                        'key' => Translation::KEY_SLUG
                    ])->queryOne();

                    Console::output("Processing city {$city['name']}");
                    $slug = UtilityHelper::transliterate($city['name']);

                    if (empty($slug)) {
                        continue;
                    }

                    if ($existentCity) {
                        continue;
//                        $slug = UtilityHelper::transliterate($region['name']) . '-' . $slug;
                    }

                    Yii::$app->db->createCommand()->insert('city', [
                        'region_id' => $regionId,
                        'country_id' => $countryId,
                        'lat' => $city['latitude'],
                        'lng' => $city['longitude'],
                    ])->execute();
                    $cityId = Yii::$app->db->lastInsertID;
                    Yii::$app->db->createCommand()->batchInsert('translation', ['entity', 'entity_id', 'value', 'key', 'locale'], [
                        [
                            Translation::ENTITY_CITY,
                            $cityId,
                            $city['name'],
                            Translation::KEY_TITLE,
                            'en-GB'
                        ],
                        [
                            Translation::ENTITY_CITY,
                            $cityId,
                            $slug,
                            Translation::KEY_SLUG,
                            'en-GB'
                        ],
                    ])->execute();
                }
            }
        }

        if ($success === true) {
//            $transaction->commit();
            return true;
        }

//        $transaction->rollBack();
        return false;
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     * @throws \Throwable
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionLargeCities()
    {
        $dataFilePath = Yii::getAlias('@console') . '/import/large-cities.csv';
        if (!file_exists($dataFilePath)) {
            throw new InvalidConfigException('File does not exist');
        }

        $data = [];
        if ($file = fopen($dataFilePath, 'r')) {
            while (!feof($file)) {
                $row = fgets($file);
                if (!empty($row)) {
                    $data[] = array_map(function ($val) {
                        return trim(str_replace('"', '', $val));
                    }, explode(',', $row));
                }
            }
            fclose($file);
        }

        if (empty($data)) {
            throw new InvalidConfigException('Nothing to import, fml');
        }

        $country = $this->_countryService->getOne(['code' => 'ru']);
        if ($country === null) {
            throw new InvalidConfigException('Russia not found');
        }

        $success = true;
        $transaction = Yii::$app->db->beginTransaction();

        $this->_importService->cleanByCondition(['source' => 'big-city']);

        foreach ($data as $item) {
            Console::output("Processing {$item[0]}");

            $region = $this->_regionService->getOne(['value' => $item[1], 'key' => Translation::KEY_TITLE, 'country_id' => $country['id']]);
            if ($region !== null) {
                $regionId = $region['id'];
                $city = $this->_cityService->getOne(['value' => $item[0], 'key' => Translation::KEY_TITLE, 'region_id' => $regionId]);
                if ($city === null) {
                    $latLng = $this->_googleMapsService->geocode("{$region['title']} {$item[0]}");
                    if ($latLng) {
                        $insertId = $this->_cityService->create([
                            'country_id' => $country['id'],
                            'lat' => $latLng['lat'],
                            'lng' => $latLng['long'],
                            'region_id' => $regionId,
                            'is_big' => true,
                            'Translation' => [
                                [
                                    'locale' => 'ru-RU',
                                    'key' => Translation::KEY_TITLE,
                                    'value' => $item[0],
                                    'entity' => Translation::ENTITY_CITY
                                ],
                                [
                                    'locale' => 'ru-RU',
                                    'key' => Translation::KEY_SLUG,
                                    'value' => UtilityHelper::transliterate($item[0]),
                                    'entity' => Translation::ENTITY_CITY
                                ]
                            ]
                        ]);
                        $success = $success && $insertId !== null;

                        $this->_importService->registerImport('city', $insertId, 'big-city');
                    }
                } else {
                    if (!$city['is_big']) {
                        $success = $success && $this->_cityService->update(['is_big' => true], ['id' => $city['id']]);
                    }
                }
            }
        }

        if ($success === true) {
            $transaction->commit();
            return true;
        }

        $transaction->rollBack();
        return false;
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionLatLng($limit = 100)
    {
        /** @var CityRepository $cityRepository */
        $cityRepository = Yii::$container->get(CityRepository::class);
        $cities = $cityRepository
            ->with(['translations', 'country.translations', 'region.translations'])
            ->limit($limit)
            ->findManyByCriteria(['lat' => null]);

        foreach ($cities as $city) {
            $dto = new CityDTO($city);
            $data = $dto->getData();

            Console::output("Processing {$data['title']}");

            $address = "{$data['country']['title']} {$data['region']['title']} {$data['title']}";
            $latLng = $this->_googleMapsService->geocode($address);

            Yii::$app->db->createCommand()->update('city', ['lat' => $latLng['lat'], 'lng' => $latLng['long']], ['id' => $city['id']])->execute();
        }

        return true;
    }
}