<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.08.2018
 * Time: 12:00
 */

namespace console\controllers\import;

use common\models\AttributeGroup;
use common\repositories\sql\AttributeRepository;
use common\repositories\sql\CategoryRepository;
use common\services\ApiClientService;
use console\services\ImportRegisterService;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Class AttributeController
 * @package console\controllers\import
 */
class AttributeController extends Controller
{
    /**
     * @var ApiClientService
     */
    private $_categoryRepository;
    /**
     * @var ImportRegisterService
     */
    private $_attributeRepository;

    /**
     * AttributeController constructor.
     * @param $id
     * @param $module
     * @param CategoryRepository $categoryRepository
     * @param AttributeRepository $attributeRepository
     * @param array $config
     */
    public function __construct($id, $module,
                                CategoryRepository $categoryRepository,
                                AttributeRepository $attributeRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_categoryRepository = $categoryRepository;
        $this->_attributeRepository = $attributeRepository;
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionAttributeGroups()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $result = true;

        $dataFilePath = Yii::getAlias('@console') . '/import/attribute-group/attribute-groups.php';
        $data = file_exists($dataFilePath) ? include($dataFilePath) : [];

        AttributeGroup::deleteAll();

        foreach ($data as $entityKey => $entityData) {
            switch ($entityKey) {
                case AttributeGroup::ENTITY_PROPERTY_CATEGORY:
                case AttributeGroup::ENTITY_REQUEST_CATEGORY:
                    foreach ($entityData as $groupAlias => $groupData) {
                        foreach ([AttributeGroup::TYPE_SALE, AttributeGroup::TYPE_RENT] as $type) {
                            // Тут по идее цикл по типу можно добавить
                            $groupData = $this->extractGroupData($entityData, $groupAlias, $type);
                            $this->createGroup($groupData, $groupAlias, $entityKey, $type);
                        }
                    }

                    break;
                case AttributeGroup::ENTITY_BUILDING:
                    foreach ($entityData as $groupType => $groupData) {
                        $this->createGroup($groupData, 'building', $entityKey, $groupType);
                    }

                    break;
            }
        }

        if ($result === true) {
            $transaction->commit();

        } else {
            $transaction->rollBack();
        }

        return $result;
    }

    /**
     * @param $entityData
     * @param $categoryKey
     * @param null $type
     * @return array|mixed|null
     */
    private function extractGroupData($entityData, $categoryKey, $type = null)
    {
        $categoryData = $entityData[$categoryKey];
        $parent = ArrayHelper::remove($categoryData, 'parent');
        $result = ArrayHelper::remove($categoryData, 'common', []);
        if ($parent && !empty($entityData[$parent])) {
            $parentData = $this->extractGroupData($entityData, $parent, $type);
            $result = ArrayHelper::merge($parentData, $result);
        }
        if($type !== null && array_key_exists('type', $categoryData)) {
            $result = array_merge($result, $categoryData[$type]);
        }

        return $result;
    }

    /**
     * @param $data
     * @param $alias
     * @param $entity
     * @param null $type
     * @return bool
     */
    private function createGroup($data, $alias, $entity, $type = null)
    {
        $group = new AttributeGroup(['alias' => $alias, 'entity' => $entity, 'type' => $type]);

        foreach ($data as $attributeKey => $attributeData) {

            $attributeObject = $this->_attributeRepository->findOneByCriteria(['alias' => $attributeKey]);
            if ($attributeObject !== null) {
                $a = $group->bind('groupAttributes');
                $a->attributes = [
                    'attribute_id' => $attributeObject->id,
                    'customDataArray' => $attributeData
                ];
            }
        }

        return $group->save();
    }
}