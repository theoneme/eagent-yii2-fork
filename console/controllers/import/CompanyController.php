<?php

namespace console\controllers\import;

use common\dto\CompanyDTO;
use common\models\CompanyMember;
use common\models\Import;
use common\models\User;
use common\services\ApiClientService;
use common\services\entities\CompanyMemberService;
use common\services\entities\CompanyService;
use common\services\entities\UserService;
use console\services\ImportRegisterService;
use console\services\PrianCompanyConverter;
use Yii;
use yii\console\Controller;
use yii\httpclient\Client;

/**
 * Class CompanyController
 * @package console\controllers
 */
class CompanyController extends Controller
{
    /**
     * @var ApiClientService
     */
    private $_apiClientService;
    /**
     * @var ImportRegisterService
     */
    private $_importService;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var CompanyService
     */
    private $_companyService;
    /**
     * @var CompanyMemberService
     */
    private $_companyMemberService;

    /**
     * CompanyController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param ApiClientService $apiClientService
     * @param ImportRegisterService $importRegisterService
     * @param UserService $userService
     * @param CompanyService $companyService
     * @param CompanyMemberService $companyMemberService
     * @param array $config
     */
    public function __construct($id, $module,
                                ApiClientService $apiClientService,
                                ImportRegisterService $importRegisterService,
                                UserService $userService,
                                CompanyService $companyService,
                                CompanyMemberService $companyMemberService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_apiClientService = $apiClientService;
        $this->_importService = $importRegisterService;
        $this->_userService = $userService;
        $this->_companyService = $companyService;
        $this->_companyMemberService = $companyMemberService;
    }

    /**
     *
     */
    public function actionPrian($limit = 10)
    {
        $client = new Client();
        $lastCompanyRowPath = Yii::getAlias("@console") . "/import/prian/last-company-row.php";
        $lastCompanyRow = file_exists($lastCompanyRowPath) ? include($lastCompanyRowPath) : 2;

        /* @var CompanyService $companyService*/
        $companyService = Yii::$container->get(CompanyService::class);
        $converter = Yii::$container->get(PrianCompanyConverter::class);

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load(Yii::getAlias('@console') . DIRECTORY_SEPARATOR . 'import/prian/companies.xlsx');

        $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();

        $transaction = Yii::$app->db->beginTransaction();
        foreach ($rowIterator as $key => $row) {
            if ($key < $lastCompanyRow) {
                continue;
            }
            if ($key >= ($lastCompanyRow + $limit)) {
                break;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $data = [];
            foreach ($cellIterator as $cell) {
                $column = $cell->getColumn();
                $val = $cell->getCalculatedValue();

                switch ($column) {
                    case 'A':
                        $data['name'] = $val;
                        break;
                    case 'B':
                        $data['url'] = $val;
                        break;
                    case 'C':
                        $data['site'] = $val;
                        break;
                    case 'D':
                        $data['id'] = $val;
                        break;
                    case 'E':
                        $data['emails'] = $val;
                        break;
                }
            }
            if (empty($data['url']) || !preg_match("/^https:\/\/prian\.ru/", $data['url'])) {
                continue;
            }
//            $content = $this->_apiClientService->makeRequest('get', $data['url'], []);
            $content = $client->get($data['url'])->send();
            $convertedData = $converter->convertObject(['docData' => $data, 'siteData' => $content->content]);
            if (!empty($convertedData)) {
                if (!empty($convertedData['userData']['phone']) || !empty($convertedData['userData']['email'])) {
                    $userId = User::find()->where(['or', ['phone' => $convertedData['userData']['phone']], ['email' => $convertedData['userData']['email']]])->select('id')->scalar();
                }
                else {
                    $userId = false;
                }
                if ($userId === false) {
                    $userId = $this->_userService->create(['UserForm' => $convertedData['userData']]);
                    if ($userId) {
                        $this->_importService->registerImport('user', $userId, 'prian');
                    }
                }
                /* @var CompanyDTO $company */
                $company = $companyService->getOne(['exists',
                    Import::find()
                        ->where(['source' => 'prian', 'entity' => 'company', 'origin' => $data['url']])
                        ->andWhere('import.entity_id = company.id')
                ]);
                if ($company !== null) {
                    $result = $companyService->update($convertedData['companyData'], ['id' => $company['id']]);
                    if (!$result) {
                        $transaction->rollBack();
                        return false;
                    }
                    if ($userId && !isset($company['members'][$userId])) {
                        $this->_companyMemberService->create([
                            'user_id' => $userId,
                            'company_id' => $company['id'],
                            'role' => CompanyMember::ROLE_OWNER
                        ]);
                    }
                }
                else {
                    $companyId = $companyService->create($convertedData['companyData']);
                    if ($companyId) {
                        $this->_importService->registerImport('company', $companyId, 'prian', $data['url']);
                        if ($userId) {
                            $this->_companyMemberService->create([
                                'user_id' => $userId,
                                'company_id' => $companyId,
                                'role' => CompanyMember::ROLE_OWNER
                            ]);
                        }
                    }
                    else {
                        $transaction->rollBack();
                        return false;
                    }
                }
            }
        }
        $transaction->commit();

        $lastCompanyRow += $limit;
        file_put_contents($lastCompanyRowPath, "<?php \n return {$lastCompanyRow};");

        return true;
    }
}