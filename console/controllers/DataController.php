<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.05.2017
 * Time: 18:28
 */

namespace console\controllers;

use common\models\Import;
use common\models\Property;
use common\models\Translation;
use common\repositories\sql\PropertyRepository;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class CommandController
 * @package console\controllers
 */
class DataController extends Controller
{
    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionClearProperties()
    {
        $properties = Property::find()->where(['>', 'id', 13872])->all();
        foreach ($properties as $property) {
            $property->delete();
        }
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionClearEmptyImports()
    {
        Import::deleteAll(['entity_id' => 0]);
    }

    /**
     * @return bool
     */
    public function actionFixOldLocales()
    {
        /** @var PropertyRepository $propertyRepository */
        $propertyRepository = Yii::$container->get(PropertyRepository::class);

        $properties = $propertyRepository->joinWith(['translations'])
            ->groupBy('property.id')
            ->select(['property.*', 'localesCount' => 'count(distinct property_translations.locale)', 'translationsCount' => 'count(property_translations.id)'])
            ->having(['and',
                ['=', 'localesCount', 2],
                ['=', 'translationsCount', 3],
            ])
            ->findManyByCriteria([], true);

        Console::startProgress(0, count($properties));

        foreach($properties as $key => $property) {
            Translation::updateAll(['locale' => $property['locale']], ['entity' => Translation::ENTITY_PROPERTY, 'entity_id' => $property['id']]);
            Console::updateProgress($key + 1, count($properties));
        }

        Console::endProgress();

        return true;
    }
}