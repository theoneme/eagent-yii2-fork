<?php

namespace console\controllers\elastic;


use common\models\Building;
use common\models\elastic\BuildingElastic;
use common\models\elastic\PropertyElastic;
use common\models\Property;
use common\models\Translation;
use common\services\elasticsearch\BuildingElasticConverter;
use common\services\elasticsearch\PropertyElasticConverter;
use yii\console\Controller;
use yii\db\ActiveQuery;
use yii\elasticsearch\ActiveRecord;
use yii\elasticsearch\BulkCommand;
use yii\elasticsearch\Command;
use yii\helpers\Console;

/**
 * Class IndexController
 * @package console\controllers\elastic
 */
class IndexController extends Controller
{
    /**
     * @var Command
     */
    private $_command;

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if ($this->_command === null) {
            $connection = ActiveRecord::getDb();
            $this->_command = $connection->createCommand();
        }

        return parent::beforeAction($action);
    }

    /**
     *
     */
    public function actionInitIndex()
    {
        $config = [
            'settings' => [
                'index.refresh_interval' => '30s',
                'index.blocks.read_only_allow_delete' => null,
//                'index.translog.flush_threshold_size' => '4mb',
                'analysis' => [
                    'filter' => [
                        'ru_stop' => [
                            'type' => 'stop',
                            'stopwords' => '_russian_'
                        ],
                        'ru_stemmer' => [
                            'type' => 'stemmer',
                            'language' => 'russian'
                        ],
                        'snowball' => [
                            'type' => 'snowball',
                            'language' => 'russian'
                        ],
                        'worddelimiter' => [
                            'type' => 'word_delimiter',
                            'split_on_numerics' => false,
                            'preserve_original' => true
                        ],
                        'app_ngram' => [
                            'type' => 'nGram',
                            'min_gram' => 4,
                            'max_gram' => 5,
                            'token_chars' => ['letter', 'digit']
                        ],
                        'edgeNGram' => [
                            'type' => 'edgeNGram',
                            'min_gram' => 4,
                            'max_gram' => 5,
                            'token_chars' => ['letter', 'digit']
                        ]
                    ],
                    'analyzer' => [
                        'edgeNGram' => [
                            'char_filter' => [
                                'html_strip'
                            ],
                            'type' => 'custom',
                            'tokenizer' => 'whitespace',
                            'filter' => ['edgeNGram', 'lowercase', 'asciifolding', 'ru_stop', 'ru_stemmer', 'snowball']
                        ],
                        'default' => [
                            'char_filter' => [
                                'html_strip'
                            ],
                            'type' => 'custom',
                            'tokenizer' => 'whitespace',
                            'filter' => ['lowercase', 'asciifolding', 'ru_stop', 'ru_stemmer', 'snowball']
                        ]
                    ],
                    'tokenizer' => [
                        'nGram' => [
                            'type' => 'nGram',
                            'min_gram' => 1,
                            'max_gram' => 2,
                            'token_chars' => ['letter', 'digit']
                        ],
                        'edgeNGram' => [
                            'type' => 'edgeNGram',
                            'min_gram' => 3,
                            'max_gram' => 4,
                            'token_chars' => ['letter', 'digit']
                        ]
                    ],
                ]
            ],
        ];

        $this->_command->deleteIndex('eagent_property');
        $this->_command->deleteIndex('eagent_building');

        $res = $this->_command->createIndex('eagent_property', array_merge($config, [
            'mappings' => PropertyElastic::mapping()
        ]));

        var_dump($res);

        $res = $this->_command->createIndex('eagent_building', array_merge($config, [
            'mappings' => BuildingElastic::mapping()
        ]));

        var_dump($res);
    }

    /**
     *
     */
    public function actionReIndexAll()
    {
        $this->actionProperty();
    }

    /**
     * @throws \yii\elasticsearch\Exception
     */
    public function actionProperty()
    {
        $items = Property::find()->with([
            'propertyAttributes',
            'category',
            'translations' => function (ActiveQuery $query) {
                return $query->andOnCondition(['property_translations.key' => [Translation::KEY_DESCRIPTION]]);
            },
        ])
            ->where(['property.status' => Property::STATUS_ACTIVE])
            ->andWhere(['not', ['lat' => null]])
            ->andWhere(['not', ['lng' => null]])
            ->orderBy(['id' => SORT_ASC])
            ->asArray();
        $total = $items->count();
        Console::output("Total: {$total}");
        Console::startProgress(0, $total);
        PropertyElastic::deleteAll();
        PropertyElastic::updateMapping();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $items->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            $bulkCommand = new BulkCommand([
                'index' => PropertyElastic::index(),
                'type' => PropertyElastic::type(),
                'db' => ActiveRecord::getDb()
            ]);
            foreach ($batch as $k => $item) {
                /** @var array $item */
                $converter = new PropertyElasticConverter($item);
                $converted = $converter->process(false);
                $bulkCommand->addAction(['create' => ['_id' => $converted['id']]], $converted);

                $i++;
                Console::updateProgress($i, $total);
            }

            $bulkCommand->execute();
            $iterator++;
        }

        Console::endProgress();
    }

    /**
     * @throws \yii\elasticsearch\Exception
     */
    public function actionBuilding()
    {
        $items = Building::find()->with([
            'buildingAttributes',
        ])
            ->where(['building.status' => Building::STATUS_ACTIVE])
            ->andWhere(['not', ['lat' => null]])
            ->andWhere(['not', ['lng' => null]])
            ->orderBy(['id' => SORT_ASC])
            ->asArray();
        $total = $items->count();
        Console::output("Total: {$total}");
        Console::startProgress(0, $total);
        BuildingElastic::deleteAll();
        BuildingElastic::updateMapping();

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $items->limit(100)->offset($iterator * 100)->all();
            if (empty($batch)) {
                break;
            }

            $bulkCommand = new BulkCommand([
                'index' => BuildingElastic::index(),
                'type' => BuildingElastic::type(),
                'db' => ActiveRecord::getDb()
            ]);
            foreach ($batch as $k => $item) {
                /** @var array $item */
                $converter = new BuildingElasticConverter($item);
                $converted = $converter->process(false);
                $bulkCommand->addAction(['create' => ['_id' => $converted['id']]], $converted);

                $i++;
                Console::updateProgress($i, $total);
            }

            $bulkCommand->execute();
            $iterator++;
        }

        Console::endProgress();
    }

    /**
     * @param int $prevDays
     */
    public function actionNewProperty($prevDays = 1)
    {
        $items = Property::find()->with([
            'propertyAttributes',
            'translations' => function (ActiveQuery $query) {
                return $query->andOnCondition(['property_translations.key' => [Translation::KEY_DESCRIPTION]]);
            },
            'category'
        ])
            ->andWhere(['not', ['lat' => null]])
            ->andWhere(['not', ['lng' => null]])
            ->andWhere(['or', ['>=', 'created_at', time() - (86400 * $prevDays)], ['>=', 'updated_at', time() - (86400 * $prevDays)]])
            ->orderBy(['id' => SORT_ASC])
            ->asArray();
        $total = $items->count();
        Console::output("Total: {$total}");
        Console::startProgress(0, $total);

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $items->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            $bulkCommand = new BulkCommand([
                'index' => PropertyElastic::index(),
                'type' => PropertyElastic::type(),
                'db' => ActiveRecord::getDb()
            ]);

            foreach ($batch as $k => $item) {
                /** @var array $item */
                $converter = new PropertyElasticConverter($item);
                $converted = $converter->process(false);
                $bulkCommand->addAction(['create' => ['_id' => $converted['id']]], $converted);

                $i++;
                Console::updateProgress($i, $total);
            }

            $bulkCommand->execute();
            $iterator++;
        }

        Console::endProgress();
    }

    /**
     * @param int $prevDays
     */
    public function actionNewBuilding($prevDays = 1)
    {
        $items = Building::find()->with([
            'buildingAttributes',
        ])
            ->andWhere(['not', ['lat' => null]])
            ->andWhere(['not', ['lng' => null]])
            ->andWhere(['or', ['>=', 'created_at', time() - (86400 * $prevDays)], ['>=', 'updated_at', time() - (86400 * $prevDays)]])
            ->orderBy(['id' => SORT_ASC])
            ->asArray();
        $total = $items->count();
        Console::output("Total: {$total}");
        Console::startProgress(0, $total);

        $iterator = 0;
        $i = 0;
        while (true) {
            $batch = $items->limit(100)->offset($iterator * 100)->all();
            $data = [];
            if (empty($batch)) {
                break;
            }

            $bulkCommand = new BulkCommand([
                'index' => BuildingElastic::index(),
                'type' => BuildingElastic::type(),
                'db' => ActiveRecord::getDb()
            ]);

            foreach ($batch as $k => $item) {
                /** @var array $item */
                $converter = new BuildingElasticConverter($item);
                $converted = $converter->process(false);
                $bulkCommand->addAction(['create' => ['_id' => $converted['id']]],  $converted);

                $i++;
                Console::updateProgress($i, $total);
            }

            $bulkCommand->execute();
            $iterator++;
        }

        Console::endProgress();
    }
}