<?php

namespace console\controllers;

use common\models\CompanyMember;
use frontend\components\CompanyDbManager;
use frontend\modules\crm\rules\AuthorRule;
use frontend\modules\crm\rules\ViewRule;
use frontend\rules\CompanyViewRule;
use Yii;
use yii\console\Controller;
use yii\db\Query;

/**
 * Class RbacController
 * @package console\controllers
 */
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        $owner = $auth->createRole('owner');
        $admin = $auth->createRole('admin');
        $editor = $auth->createRole('editor');
        $viewer = $auth->createRole('viewer');

        $auth->add($owner);
        $auth->add($admin);
        $auth->add($editor);
        $auth->add($viewer);

        $viewRule = new ViewRule();
        $authorRule = new AuthorRule();
        $auth->add($viewRule);
        $auth->add($authorRule);

        $viewPermission = $auth->createPermission('view');
        $viewPermission->ruleName = $viewRule->name;
        $viewPermission->description = 'Просмотр';

        $createPermission = $auth->createPermission('create');
        $createPermission->description = 'Создание';

        $editPermission = $auth->createPermission('edit');
        $editPermission->ruleName = $authorRule->name;
        $editPermission->description = 'Редактирование';

        $managePermission = $auth->createPermission('manage');
        $managePermission->description = 'Управление пользователями';

        $manageAdminsPermission = $auth->createPermission('manageAdmins');
        $manageAdminsPermission->description = 'Управление админами';

        $manageCrmPermission = $auth->createPermission('manageCRM');
        $manageCrmPermission->description = 'Управление CRM';

        $auth->add($viewPermission);
        $auth->add($createPermission);
        $auth->add($editPermission);
        $auth->add($managePermission);
        $auth->add($manageAdminsPermission);
        $auth->add($manageCrmPermission);

        $auth->addChild($viewer, $viewPermission);

        $auth->addChild($editor, $viewer);
        $auth->addChild($editor, $createPermission);
        $auth->addChild($editor, $editPermission);

        $auth->addChild($admin, $editor);
        $auth->addChild($admin, $managePermission);

        $auth->addChild($owner, $admin);
        $auth->addChild($owner, $manageAdminsPermission);
        $auth->addChild($owner, $manageCrmPermission);
    }

    public function actionAddModerator(){
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        $editor = $auth->getRole('editor');
        $moderator = $auth->createRole('moderator');

        $auth->add($moderator);

        $editOthersPermission = $auth->createPermission('editOthers');
        $editOthersPermission->description = 'Редактирование чужих элементов';
        $auth->add($editOthersPermission);

        $auth->addChild($moderator, $editor);
        $auth->addChild($moderator, $editOthersPermission);

        $auth->removeChild($admin, $editor);
        $auth->addChild($admin, $moderator);
    }

    public function actionCompanyInit() {
        $auth = Yii::$app->authManager;

        $owner = $auth->createRole('companyOwner');
        $admin = $auth->createRole('companyAdmin');
        $moderator = $auth->createRole('companyModerator');
        $editor = $auth->createRole('companyEditor');
        $viewer = $auth->createRole('companyViewer');

        $auth->add($owner);
        $auth->add($admin);
        $auth->add($moderator);
        $auth->add($editor);
        $auth->add($viewer);

        $viewRule = new CompanyViewRule();
        $auth->add($viewRule);

        $viewPermission = $auth->createPermission('viewCompany');
        $viewPermission->ruleName = $viewRule->name;
        $viewPermission->description = 'Просмотр';

        $createPermission = $auth->createPermission('createAsCompany');
        $createPermission->description = 'Создание';

        $managePermission = $auth->createPermission('manageCompanyMembers');
        $managePermission->description = 'Управление пользователями';

        $manageCompanyPermission = $auth->createPermission('manageCompany');
        $manageCompanyPermission->description = 'Управление компанией';

        $auth->add($viewPermission);
        $auth->add($createPermission);
        $auth->add($managePermission);
        $auth->add($manageCompanyPermission);

        $auth->addChild($viewer, $viewPermission);

        $auth->addChild($editor, $viewer);
        $auth->addChild($editor, $createPermission);

        $auth->addChild($moderator, $editor);

        $auth->addChild($admin, $moderator);
        $auth->addChild($admin, $managePermission);

        $auth->addChild($owner, $admin);
        $auth->addChild($owner, $manageCompanyPermission);
    }

    public function actionAssignCompanyMembers(){
        /** @var CompanyDbManager $authManager */
        $authManager = new CompanyDbManager();
        $members = CompanyMember::find()->where(['not exists',
            (new Query())->from('company_auth_assignment')->where('company_auth_assignment.member_id = company_member.id')
        ])->all();
        foreach ($members as $member) {
            /** @var CompanyMember $member */
            $rbacRoleName = $member->getRbacRoleName();
            if ($rbacRoleName !== null) {
                $rbacRole = $authManager->getRole($rbacRoleName);
                if ($rbacRole !== null) {
                    $authManager->revokeAll($member->id);
                    $authManager->assign($rbacRole, $member->id);
                }
            }
        }
    }
}