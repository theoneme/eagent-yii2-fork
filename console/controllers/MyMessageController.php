<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 08.06.2017
 * Time: 15:36
 */

namespace console\controllers;

use Exception;
use Yii;
use yii\console\controllers\MessageController as BaseMessageController;
use yii\db\Connection;
use yii\di\Instance;
use yii\helpers\FileHelper;

/**
 * Class MessageController
 * @package console\controllers
 */
class MyMessageController extends BaseMessageController
{
    public function actionExtract($configFile = null)
    {
        $configFileContent = [];
        if ($configFile !== null) {
            $configFile = Yii::getAlias($configFile);
            if (!is_file($configFile)) {
                throw new Exception("The configuration file does not exist: $configFile");
            }
            $configFileContent = require($configFile);
        }

        $config = array_merge(
            $this->getOptionValues($this->action->id),
            $configFileContent,
            $this->getPassedOptionValues()
        );
        //$config['sourcePath'] = Yii::getAlias($config['sourcePath']);
        foreach ($config['sourcePath'] as $key => $path) {
            $config[$key] = Yii::getAlias($path);
        }
        $config['messagePath'] = Yii::getAlias($config['messagePath']);

        if (!isset($config['sourcePath'], $config['languages'])) {
            throw new Exception('The configuration file must specify "sourcePath" and "languages".');
        }
//		if (!is_dir($config['sourcePath'])) {
//			throw new Exception("The source path {$config['sourcePath']} is not a valid directory.");
//		}
        if (empty($config['format']) || !in_array($config['format'], ['php', 'po', 'pot', 'db'])) {
            throw new Exception('Format should be either "php", "po", "pot" or "db".');
        }
        if (in_array($config['format'], ['php', 'po', 'pot'])) {
            if (!isset($config['messagePath'])) {
                throw new Exception('The configuration file must specify "messagePath".');
            }
            if (!is_dir($config['messagePath'])) {
                throw new Exception("The message path {$config['messagePath']} is not a valid directory.");
            }
        }
        if (empty($config['languages'])) {
            throw new Exception('Languages cannot be empty.');
        }

        $files = [];

        foreach ($config['sourcePath'] as $path) {
            $files = array_merge($files, FileHelper::findFiles(realpath($path), $config));
        }

        //print_r($files); die;

        $messages = [];
        foreach ($files as $file) {
            $messages = array_merge_recursive($messages, $this->extractMessages($file, $config['translator'], $config['ignoreCategories']));
        }

        $catalog = isset($config['catalog']) ? $config['catalog'] : 'messages';

        if (in_array($config['format'], ['php', 'po'])) {
            foreach ($config['languages'] as $language) {
                $dir = $config['messagePath'] . DIRECTORY_SEPARATOR . $language;
                if (!is_dir($dir) && !@mkdir($dir)) {
                    throw new Exception("Directory '{$dir}' can not be created.");
                }
                if ($config['format'] === 'po') {
                    $this->saveMessagesToPO($messages, $dir, $config['overwrite'], $config['removeUnused'], $config['sort'], $catalog, $config['markUnused']);
                } else {
                    $this->saveMessagesToPHP($messages, $dir, $config['overwrite'], $config['removeUnused'], $config['sort'], $config['markUnused']);
                }
            }
        } elseif ($config['format'] === 'db') {
            /** @var Connection $db */
            $db = Instance::ensure($config['db'], Connection::class);
            $sourceMessageTable = isset($config['sourceMessageTable']) ? $config['sourceMessageTable'] : '{{%source_message}}';
            $messageTable = isset($config['messageTable']) ? $config['messageTable'] : '{{%message}}';
            $this->saveMessagesToDb(
                $messages,
                $db,
                $sourceMessageTable,
                $messageTable,
                $config['removeUnused'],
                $config['languages'],
                $config['markUnused']
            );
        } elseif ($config['format'] === 'pot') {
            $this->saveMessagesToPOT($messages, $config['messagePath'], $catalog);
        }
    }
}