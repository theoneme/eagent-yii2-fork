<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.05.2017
 * Time: 18:28
 */

namespace console\controllers\translation;

use common\exceptions\EntityNotFoundException;
use common\helpers\UtilityHelper;
use common\mappers\TranslationsMapper;
use common\models\City;
use common\models\Country;
use common\models\Region;
use common\models\Translation;
use common\repositories\sql\CityRepository;
use common\repositories\sql\CountryRepository;
use common\repositories\sql\RegionRepository;
use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\RegionService;
use console\services\LocationTranslationService;
use console\services\YandexLocationTranslationService;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\console\Controller;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class LocationController
 * @package console\controllers\translation
 */
class LocationController extends Controller
{
    /**
     * @var CountryService
     */
    private $_countryService;
    /**
     * @var RegionService
     */
    private $_regionService;
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var LocationTranslationService
     */
    private $_locationTranslationService;

    /**
     * LocationController constructor.
     * @param string $id
     * @param Module $module
     * @param CityService $cityService
     * @param CountryService $countryService
     * @param RegionService $regionService
     * @param LocationTranslationService $locationTranslationService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                CityService $cityService,
                                CountryService $countryService,
                                RegionService $regionService,
                                LocationTranslationService $locationTranslationService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_countryService = $countryService;
        $this->_regionService = $regionService;
        $this->_cityService = $cityService;
        $this->_locationTranslationService = $locationTranslationService;
    }

    /**
     * @param $entity
     * @param $alias
     * @param $from
     * @param $to
     * @return bool
     * @throws EntityNotFoundException
     * @throws InvalidConfigException
     * @throws \yii\db\Exception
     * @throws \yii\di\NotInstantiableException
     */
    public function actionLocation($entity, $alias, $from, $to)
    {
        if (!in_array($entity, [
            'country',
            'city',
            'region'
        ])) {
            throw new InvalidConfigException('Wrong entity passed');
        }

        if (!array_key_exists($from, Yii::$app->params['supportedLocales']) || !array_key_exists($to, Yii::$app->params['supportedLocales'])) {
            throw new InvalidConfigException('Wrong languages');
        }

        $transaction = Yii::$app->db->beginTransaction();
        $success = true;

        switch ($entity) {
            case 'country':
                $country = $this->_countryService->getOne(['value' => $alias, 'key' => Translation::KEY_SLUG, 'locale' => $from]);
                if (!$country) {
                    throw new EntityNotFoundException('Entity with that source language does not exist');
                }

                $success = $success && $this->_locationTranslationService->translate($country, Translation::ENTITY_COUNTRY, $from, $to);
                Console::output("Processing country {$country['title']}");
                $regions = $this->_regionService->getMany(['country_id' => $country['id'], 'per-page' => 120]);

                foreach ($regions['items'] as $region) {
                    $region['fullTitle'] = "{$country['title']} {$region['title']}";
                    $success = $success && $this->_locationTranslationService->translate($region, Translation::ENTITY_REGION, $from, $to);
                    Console::output("Processing region {$region['title']}");

                    $cities = $this->_cityService->getMany(['region_id' => $region['id'], 'per-page' => 120]);
                    foreach ($cities['items'] as $city) {
                        $city['fullTitle'] = "{$country['title']} {$region['title']} {$city['title']}";
                        $success = $success && $this->_locationTranslationService->translate($city, Translation::ENTITY_CITY, $from, $to);
                        Console::output("Processing city {$city['title']}");
                    }
                }

                break;
        }

        if ($success === true) {
            $transaction->commit();
            return true;
        }

        $transaction->rollBack();
        return false;
    }


    /**
     * @param $entity
     * @param int $limit
     * @return bool
     * @throws InvalidConfigException
     */
    public function actionLocations($entity, $limit = 10)
    {
        if (!in_array($entity, ['city', 'region', 'country'])) {
            throw new InvalidConfigException('Wrong entity passed');
        }
        $supportedLocales = Yii::$app->params['supportedLocales'];
        $success = true;
        $translationService = Yii::$container->get(YandexLocationTranslationService::class);

        switch ($entity) {
            case 'city':
                $repository = Yii::$container->get(CityRepository::class);
                $translationEntity = Translation::ENTITY_CITY;
                break;
            case 'region':
                $repository = Yii::$container->get(RegionRepository::class);
                $translationEntity = Translation::ENTITY_REGION;
                break;
            case 'country':
                $repository = Yii::$container->get(CountryRepository::class);
                $translationEntity = Translation::ENTITY_COUNTRY;
                break;
        }
        $transaction = Yii::$app->db->beginTransaction();
        /** @var City[]|Region[]|Country[] $items */
        $items = $repository->joinWith(['translations'])
            ->groupBy("{$entity}.id")
            ->select(["{$entity}.*", "translationsCount" => "count(distinct {$entity}_translations.locale)"])
            ->having(['and',
                ['<', 'translationsCount', count($supportedLocales)],
                ['>', 'translationsCount', 0],
            ])
            ->limit($limit)
            ->findManyByCriteria();
        foreach ($items as $item) {
            $translations = TranslationsMapper::getMappedData($item->translations, TranslationsMapper::MODE_FULL);
            $locales = array_diff_key($supportedLocales, $translations);
            $baseLocale = array_key_exists('ru-RU', $translations) ? 'ru-RU' : array_values(array_keys($translations))[0];
            $baseTranslation = $translations[$baseLocale];
            $baseSlug = ArrayHelper::remove($baseTranslation, Translation::KEY_SLUG, (string)$item->id);
            foreach ($locales as $locale => $code) {
                Console::output("Translating {$entity} {$baseTranslation[Translation::KEY_TITLE]} to {$locale}");
                $result = $translationService->translate(
                    ['title' => $baseTranslation[Translation::KEY_TITLE], 'id' => $item->id, 'entity' => $translationEntity, 'baseSlug' => $baseSlug],
                    null,
                    $locale
                );
                if (!$result) {
                    Console::output("Fail");
                    $success = false;
                    break 2;
                };
            }
        }

        if ($success === true) {
            $transaction->commit();
            return true;
        }

        $transaction->rollBack();
        return false;
    }


    /**
     *
     */
    public function actionSlugToModels()
    {
        Yii::$app->language = 'en-GB';

        Region::updateAll(['slug' => null]);
        City::updateAll(['slug' => null]);

        /** @var CountryRepository $countryRepository */
        $countryRepository = Yii::$container->get(CountryRepository::class);

        Console::output('Countries:');
        $countries = $this->_countryService->getMany(['slug' => null], ['pagination' => false]);
        Console::startProgress(0, count($countries['items']));
        foreach($countries['items'] as $key => $country) {
            $countryRepository->updateOneByCriteria(['country.id' => $country['id']], ['slug' => UtilityHelper::transliterate($country['title'])]);
            Console::updateProgress($key + 1, count($countries['items']));
        }

        Console::endProgress();
        Console::output('Counties done');

        /** @var RegionRepository $regionRepository */
        $regionRepository = Yii::$container->get(RegionRepository::class);

        Console::output('Regions:');
        $page = 0;
        do {
            Console::output("Regions page {$page}");
            $page++;
            $regions = $this->_regionService->getMany(['page' => $page], ['pagination' => true]);
            foreach ($regions['items'] as $region) {
                $slug = UtilityHelper::transliterate($region['title']);

                $existent = Yii::$app->db->createCommand("SELECT slug FROM `region` where slug = :slug", [
                    'slug' => $slug,
                ])->queryOne();
                if ($existent) {
                    $country = Yii::$app->db->createCommand("SELECT slug FROM `country` where id = :id", [
                        'id' => $region['country_id'],
                    ])->queryOne();

                    $slug = UtilityHelper::transliterate($country['slug']) . '-' . $slug;
                }

                $regionRepository->updateOneByCriteria(['region.id' => $region['id']], ['slug' => $slug]);
            }
        } while ($page < $regions['pagination']->getPageCount());

        /** @var CityRepository $cityRepository */
        $cityRepository = Yii::$container->get(CityRepository::class);

        Console::output('Cities:');
        $page = 0;
        do {
            Console::output("Cities page {$page}");
            $page++;
            $cities = $this->_cityService->getMany(['page' => $page], ['pagination' => true]);
            foreach ($cities['items'] as $city) {
                $slug = UtilityHelper::transliterate($city['title']);

                $existent = Yii::$app->db->createCommand("SELECT slug FROM `city` where slug = :slug", [
                    'slug' => $slug,
                ])->queryOne();
                if ($existent) {
                    $region = Yii::$app->db->createCommand("SELECT id, slug FROM `region` where id = :id", [
                        'id' => $city['region_id'],
                    ])->queryOne();

                    $slug = UtilityHelper::transliterate($region['slug']) . '-' . $slug;
                }

                try {
                    $cityRepository->updateOneByCriteria(['city.id' => $city['id']], ['slug' => $slug], CityRepository::MODE_LIGHT);
                } catch (Exception $e) {
//                    $slug = $region['id'] . '-' . UtilityHelper::transliterate($city['title']);
//
//                    $cityRepository->updateOneByCriteria(['city.id' => $city['id']], ['slug' => $slug], CityRepository::MODE_LIGHT);
                    $cityRepository->deleteOneByCriteria(['city.id' => $city['id']]);
                }

            }
        } while ($page < $cities['pagination']->getPageCount());
    }
}