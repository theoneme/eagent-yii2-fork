<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.05.2017
 * Time: 18:28
 */

namespace console\controllers\translation;

use common\helpers\UtilityHelper;
use common\mappers\AddressTranslationsMapper;
use common\mappers\TranslationsMapper;
use common\models\Flag;
use common\models\Property;
use common\models\Translation;
use common\repositories\sql\PropertyRepository;
use common\services\entities\CityService;
use common\services\seo\PropertySeoService;
use common\services\YandexTranslationService;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class PropertyController
 * @package console\controllers\translation
 */
class PropertyController extends Controller
{
    /**
     * @var YandexTranslationService
     */
    private $_yandexTranslationService;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var PropertySeoService
     */
    private $_propertySeoService;

    /**
     * PropertyController constructor.
     * @param string $id
     * @param Module $module
     * @param YandexTranslationService $yandexTranslationService
     * @param PropertyRepository $propertyRepository
     * @param PropertySeoService $propertySeoService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                YandexTranslationService $yandexTranslationService,
                                PropertyRepository $propertyRepository,
                                PropertySeoService $propertySeoService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_yandexTranslationService = $yandexTranslationService;
        $this->_propertyRepository = $propertyRepository;
        $this->_propertySeoService = $propertySeoService;
    }

    /**
     * @param int $limit
     * @return bool
     * @throws InvalidConfigException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionProperty($limit = 5)
    {
        /** @var Property[] $properties */
        $properties = $this->_propertyRepository->joinWith(['translations', 'category.translations', 'propertyAttributes'])
            ->limit($limit)
            ->groupBy('property.id')
            ->findManyByCriteria(['and',
//                ['property.id' => 5687],
                ['not exists', Flag::find()->where(['entity' => 'property', 'type' => Flag::TYPE_AUTO_TRANSLATED])->andWhere('flag.entity_id = property.id')],
                ['property.status' => Property::STATUS_ACTIVE],
            ]);

        $count = count($properties);
        if (!$count) {
            Yii::error('Проперти закончились, айайай');
            return false;
        }
        Console::output("Translating {$count} properties");

        $supportedLocales = Yii::$app->params['bigLocales'];
        foreach ($properties as $property) {
            Console::output("Translating property #{$property->id}");
            $locales = array_diff_key($supportedLocales, [$property->locale => 0]);
            $success = true;

            $categoryTranslations = TranslationsMapper::getMappedData($property->category->translations);
            $addressTranslations = AddressTranslationsMapper::getMappedData($property->addressTranslations, AddressTranslationsMapper::MODE_FULL);
            $seoParams = ArrayHelper::map($property->propertyAttributes, 'entity_alias', 'value_alias');
            $seoParams['type'] = $property['type'];
            foreach ($locales as $locale => $code) {
                $currentCategoryTranslation = array_key_exists($locale, $categoryTranslations)
                    ? $categoryTranslations[$locale]
                    : array_pop($categoryTranslations);
                Console::output("...to locale '{$locale}'");

                if(count($addressTranslations) > 0) {
                    $addressTranslation = array_key_exists($locale, $addressTranslations)
                        ? $addressTranslations[$locale]
                        : array_values($addressTranslations)[0];
                } else {
                    $addressTranslation = [];
                }
                $seoParams['address'] = $addressTranslation['title'] ?? '';
                if ($property->locale === $locale) {
                    $seoParams['saddress'] = $addressTranslation['title'] ?? '';
                }
                else {
                    if (!empty($property->city_id)) {
                        /* @var CityService $cityService*/
                        $cityService = Yii::$container->get(CityService::class);
                        $city = $cityService->getOne(['id' => $property->city_id]);
                        if ($city) {
                            $currentCityTranslation = array_key_exists($locale, $city['translations'])
                                ? $city['translations'][$locale]
                                : array_pop($city['translations']);
                            $seoParams['saddress'] = $currentCityTranslation['title'];
                        }
                    }
                }
                $seoParams['city'] = $addressTranslation['data']['city'] ?? '';
                $seoParams['category'] = $currentCategoryTranslation[Translation::KEY_ADDITIONAL_TITLE] ?? $currentCategoryTranslation[Translation::KEY_TITLE] ?? '';

                $translations = TranslationsMapper::getMappedData($property->translations);
                ArrayHelper::remove($translations, 'title');
                ArrayHelper::remove($translations, 'slug');
                foreach ($translations as $key => $value) {
                    $translated = $this->_yandexTranslationService->translate($value, $supportedLocales[$property->locale], $supportedLocales[$locale]);
                    if ($translated) {
                        $translation = new Translation([
                            'locale' => $locale,
                            'key' => $key,
                            'value' => $translated,
                            'entity' => Translation::ENTITY_PROPERTY,
                            'entity_id' => $property->id
                        ]);
                        $success = $success && $translation->save();
                    }
                }

                $seo = $this->_propertySeoService->getSeo($property->category->customDataArray['seoTemplate'] ?? null, $seoParams, $locale);
                if (!empty($seo['heading'])) {
                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_TITLE,
                        'value' => $seo['heading'],
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);
                    $success = $success && $translation->save();

                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_SLUG,
                        'value' => UtilityHelper::generateSlug($seo['heading'], 9, 100, $property->getPrimaryKey()),
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);
                    $success = $success && $translation->save();
                }

                if ($success === true) {
                    Console::output("......success");
                } else {
                    Console::output("......fail");
                }
            }
            if ($success) {
                (new Flag(['entity' => 'property', 'entity_id' => $property->id, 'type' => Flag::TYPE_AUTO_TRANSLATED]))->save();
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function actionSlugToTranslation($limit = 1000)
    {
        $properties = $this->_propertyRepository->joinWith(['translations'])
            ->limit($limit)
            ->groupBy('property.id')
            ->findManyByCriteria(['and',
                ['not exists', Translation::find()->where('translation.entity_id = property.id')->andWhere(['entity' => Translation::ENTITY_PROPERTY, 'key' => Translation::KEY_SLUG])],
            ]);

        Console::startProgress(0, count($properties));

        foreach ($properties as $key => $property) {
            $slugTranslation = new Translation([
                'entity' => Translation::ENTITY_PROPERTY,
                'entity_id' => $property->id,
                'key' => Translation::KEY_SLUG,
                'value' => $property['slug'],
                'locale' => $property['locale']
            ]);
            $slugTranslation->save();

            Console::updateProgress($key + 1, count($properties));
        }

        return true;
    }

    /**
     * @param int $limit
     */
    public function actionDropOldTranslated($limit = 100)
    {
        /** @var Property[] $properties */
        $properties = $this->_propertyRepository->joinWith(['translations', 'category.translations', 'propertyAttributes.attr'])
            ->limit($limit)
            ->groupBy('property.id')
            ->findManyByCriteria(['and',
//                ['property.id' => 5687],
                ['exists', Flag::find()->where(['entity' => 'property', 'type' => Flag::TYPE_AUTO_TRANSLATED])->andWhere('flag.entity_id = property.id')],
                ['property.status' => Property::STATUS_ACTIVE],
                ['not', ['property.locale' => 'ru-RU']]
            ]);

        Console::startProgress(0, count($properties));

        foreach($properties as $key => $property) {
            $property->updateAttributes(['status' => Property::STATUS_DELETED, 'updated_at' => time()]);
            Console::updateProgress($key + 1, count($properties));
        }

        /** @var Property[] $properties */
        $properties = $this->_propertyRepository
            ->limit($limit)
            ->groupBy('property.id')
            ->findManyByCriteria(['and',
                ['exists', Flag::find()->where(['entity' => 'property', 'type' => Flag::TYPE_AUTO_TRANSLATED])->andWhere('flag.entity_id = property.id')],
                ['property.status' => Property::STATUS_ACTIVE],
                ['property.locale' => 'ru-RU']
            ]);

        Console::startProgress(0, count($properties));

        foreach($properties as $key => $property) {
            Flag::deleteAll(['entity' => 'property', 'type' => Flag::TYPE_AUTO_TRANSLATED, 'entity_id' => $property->id]);
            Console::updateProgress($key + 1, count($properties));
        }
    }
}