<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.05.2017
 * Time: 18:28
 */

namespace console\controllers\translation;

use common\helpers\UtilityHelper;
use common\services\YandexTranslationService;
use Exception;
use Yii;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\VarDumper;

/**
 * Class FilesController
 * @package console\controllers\translation
 */
class FilesController extends Controller
{
    /**
     * @var YandexTranslationService
     */
    private $_yandexTranslationService;

    /**
     * FilesController constructor.
     * @param string $id
     * @param Module $module
     * @param YandexTranslationService $yandexTranslationService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                YandexTranslationService $yandexTranslationService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_yandexTranslationService = $yandexTranslationService;
    }

    /**
     * @param null $localeParam
     * @throws Exception
     */
    public function actionMissing($localeParam = null)
    {
        $supportedLocales = array_diff_key(Yii::$app->params['supportedLocales'], ['ru-RU' => 0, 'en-GB' => 0]);
        if ($localeParam !== null && (!array_key_exists($localeParam, $supportedLocales))) {
            throw new Exception('Данная локаль недоступна');
        }
        $locales = $localeParam === null ? $supportedLocales : [$localeParam => $supportedLocales[$localeParam]];
        if (isset($locales['uk-UA'])) {
            $locales['uk-UA'] = 'uk';
        }
        $files = [
            'agent' => '@frontend',
            'catalog' => '@frontend',
            'index' => '@frontend',
            'main' => '@frontend',
            'property' => '@frontend',
            'seo' => '@frontend',
            'tariff' => '@frontend',
            'building' => '@frontend',
            'notification' => '@common',
            'labels' => '@common',
            'wizard' => '@frontend',
            'crm' => '@frontend/modules/crm',
            'model' => '@common',
            'landing' => '@frontend/modules/estet',
            'account' => '@frontend',
            'instance' => '@frontend/modules/instance',
        ];
        foreach ($files as $file => $path) {
            $translations = include(Yii::getAlias($path) . "/messages/ru-RU/{$file}.php");
            foreach ($locales as $locale => $code) {
                $dir = Yii::getAlias($path) . "/messages/{$locale}";
                UtilityHelper::makeDir($dir);
                $localeTranslations = file_exists("$dir/{$file}.php") ? include("$dir/{$file}.php") : [];
                $toTranslate = array_diff_key(array_filter($translations), array_filter($localeTranslations));

                if (count($toTranslate)) {
                    Console::output("Translating file ../" . $locale . '/' . $file);
                    Console::startProgress(0, count($toTranslate));
                    $i = 1;
                    foreach ($toTranslate as $english => $russian) {
                        if (!empty($russian)) {
                            $localeTranslations[$english] = $this->_yandexTranslationService->translateWithVariables($russian, 'ru', $code);
                        }
                        Console::updateProgress($i, count($toTranslate));
                        $i++;
                    }
                    file_put_contents("$dir/" . $file . '.php', "<?php \n return " . VarDumper::export($localeTranslations) . ";");
                    Console::endProgress();
                }
            }
        }

        Console::output("success");
    }
}