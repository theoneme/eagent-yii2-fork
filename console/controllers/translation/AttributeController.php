<?php

namespace console\controllers\translation;

use common\mappers\TranslationsMapper;
use common\models\Attribute;
use common\models\AttributeValue;
use common\models\Translation;
use common\repositories\sql\AttributeRepository;
use common\repositories\sql\AttributeValueRepository;
use common\services\YandexTranslationService;
use Yii;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class AttributeController
 * @package console\controllers\translation
 */
class AttributeController extends Controller
{
    /**
     * @var YandexTranslationService
     */
    private $_yandexTranslationService;
    /**
     * @var AttributeRepository
     */
    private $_attributeRepository;
    /**
     * @var AttributeValueRepository
     */
    private $_attributeValueRepository;

    /**
     * AttributeController constructor.
     * @param string $id
     * @param Module $module
     * @param YandexTranslationService $yandexTranslationService
     * @param AttributeRepository $attributeRepository
     * @param AttributeValueRepository $attributeValueRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                YandexTranslationService $yandexTranslationService,
                                AttributeRepository $attributeRepository,
                                AttributeValueRepository $attributeValueRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_yandexTranslationService = $yandexTranslationService;
        $this->_attributeRepository = $attributeRepository;
        $this->_attributeValueRepository = $attributeValueRepository;
    }

    /**
     * @return bool
     */
    public function actionTitles()
    {
        $transaction = Yii::$app->db->beginTransaction();

        $supportedLocales = Yii::$app->params['supportedLocales'];
        /** @var Attribute[] $attributes */
        $attributes = $this->_attributeRepository->joinWith(['translations'])
            ->select(['attribute.*', 'translationsCount' => 'count(distinct attribute_translations.locale)'])
            ->having(['and',
                ['<', 'translationsCount', count($supportedLocales)],
                ['>', 'translationsCount', 0],
            ])
            ->groupBy('attribute.id')
            ->findManyByCriteria();
        foreach ($attributes as $attribute) {
            $translations = TranslationsMapper::getMappedData($attribute->translations, TranslationsMapper::MODE_FULL);
            $locales = array_diff_key($supportedLocales, $translations);
            $baseLocale = array_key_exists('ru-RU', $translations) ? 'ru-RU' : array_pop(array_keys($translations));
            $baseTranslation = $translations[$baseLocale];
            Console::output("Translating attribute '{$baseTranslation['title']}'");
            foreach ($baseTranslation as $key => $value) {
                Console::output("...field '{$key}'");
                foreach ($locales as $locale => $code) {
                    Console::output("......to locale '{$locale}'");
                    $translated = $this->_yandexTranslationService->translate($value, $supportedLocales[$baseLocale], $code);
                    if ($translated) {
                        $newTranslation = new Translation([
                            'key' => $key,
                            'entity' => Translation::ENTITY_ATTRIBUTE,
                            'entity_id' => $attribute->id,
                            'locale' => $locale,
                            'value' => $translated,
                        ]);
                        if ($newTranslation->save()) {
                            Console::output(".........success");
                        } else {
                            Console::output(".........fail");

                            $transaction->rollBack();
                            return false;
                        }
                    } else {
                        Console::output(".........fail");

                        $transaction->rollBack();
                        return false;
                    }
                }
            }
        }

        $transaction->commit();
        return true;
    }

    /**
     * @param int $limit
     * @return bool
     */
    public function actionValues($limit = 10)
    {
        $transaction = Yii::$app->db->beginTransaction();

        $supportedLocales = Yii::$app->params['supportedLocales'];
        /** @var AttributeValue[] $attributes */
        $attributes = $this->_attributeValueRepository->joinWith(['translations', 'attr'])
            ->select(['attribute_value.*', 'translationsCount' => 'count(distinct attr_value_translations.locale)'])
            ->having(['and',
                ['<', 'translationsCount', count($supportedLocales)],
                ['>', 'translationsCount', 0],
            ])
            ->limit($limit)
            ->groupBy('attribute_value.id')
            ->findManyByCriteria(['attribute.type' => Attribute::TYPE_STRING, 'attribute_value.status' => AttributeValue::STATUS_APPROVED]);
        foreach ($attributes as $attribute) {
            $translations = TranslationsMapper::getMappedData($attribute->translations, TranslationsMapper::MODE_FULL);
            $locales = array_diff_key($supportedLocales, $translations);
            $baseLocale = array_key_exists('ru-RU', $translations) ? 'ru-RU' : array_values(array_keys($translations))[0];
            $baseTranslation = $translations[$baseLocale];
            Console::output("Translating attribute '{$baseTranslation['title']}'");
            foreach ($baseTranslation as $key => $value) {
                Console::output("...field '{$key}'");
                foreach ($locales as $locale => $code) {
                    Console::output("......to locale '{$locale}'");
                    $translated = $this->_yandexTranslationService->translate($value, $supportedLocales[$baseLocale], $code);
                    if ($translated) {
                        $newTranslation = new Translation([
                            'key' => $key,
                            'entity' => Translation::ENTITY_ATTRIBUTE_VALUE,
                            'entity_id' => $attribute->id,
                            'locale' => $locale,
                            'value' => $translated,
                        ]);
                        if ($newTranslation->save()) {
                            Console::output(".........success");
                        } else {
                            Console::output(".........fail");

                            $transaction->rollBack();
                            return false;
                        }
                    } else {
                        Console::output(".........fail");

                        $transaction->rollBack();
                        return false;
                    }
                }
            }
        }

        $transaction->commit();
        return true;
    }
}