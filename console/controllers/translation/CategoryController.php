<?php

namespace console\controllers\translation;

use common\helpers\UtilityHelper;
use common\mappers\TranslationsMapper;
use common\models\Category;
use common\models\Translation;
use common\repositories\sql\CategoryRepository;
use common\services\YandexTranslationService;
use Yii;
use yii\base\Module;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;

/**
 * Class CategoryController
 * @package console\controllers\translation
 */
class CategoryController extends Controller
{
    /**
     * @var YandexTranslationService
     */
    private $_yandexTranslationService;
    /**
     * @var CategoryRepository
     */
    private $_categoryRepository;

    /**
     * CategoryController constructor.
     * @param string $id
     * @param Module $module
     * @param YandexTranslationService $yandexTranslationService
     * @param CategoryRepository $categoryRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                YandexTranslationService $yandexTranslationService,
                                CategoryRepository $categoryRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_yandexTranslationService = $yandexTranslationService;
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     *
     */
    public function actionCategory()
    {
        $supportedLocales = Yii::$app->params['supportedLocales'];
        /** @var Category[] $categories */
        $categories = $this->_categoryRepository->joinWith(['translations'])
            ->groupBy('category.id')
            ->select(['category.*', 'translationsCount' => 'count(distinct category_translations.locale)'])
            ->having(['and',
                ['<', 'translationsCount', count($supportedLocales)],
                ['>', 'translationsCount', 0],
            ])
            ->findManyByCriteria();
        foreach ($categories as $category) {
            $translations = TranslationsMapper::getMappedData($category->translations, TranslationsMapper::MODE_FULL);
            $locales = array_diff_key($supportedLocales, $translations);
            $baseLocale = array_key_exists('ru-RU', $translations) ? 'ru-RU' : array_pop(array_keys($translations));
            $baseTranslation = $translations[$baseLocale];
            $baseSlug = ArrayHelper::remove($baseTranslation, Translation::KEY_SLUG, (string)$category->id);
            /* @var Category $parent */
            $parent = $this->_categoryRepository->findOneByCriteria(['and', ['category.root' => $category['root']], ['<', 'category.lft', $category['lft']], ['>', 'category.rgt', $category['rgt']], ['category.lvl' => $category['lvl'] - 1]]);
            if ($parent) {
                $parentTranslations = TranslationsMapper::getMappedData($parent->translations, TranslationsMapper::MODE_FULL);
            }
            Console::output("Translating category '{$baseTranslation['title']}'");
            foreach ($baseTranslation as $key => $value) {
                Console::output("...field '{$key}'");
                foreach ($locales as $locale => $code) {
                    Console::output("......to locale '{$locale}'");
                    $translated = $this->_yandexTranslationService->translate($value, $supportedLocales[$baseLocale], $code);
                    if ($translated) {
                        $newTranslation = new Translation([
                            'key' => $key,
                            'entity' => Translation::ENTITY_CATEGORY,
                            'entity_id' => $category->id,
                            'locale' => $locale,
                            'value' => $translated,
                        ]);
                        if ($newTranslation->save()) {
                            if ($newTranslation->key === Translation::KEY_TITLE) {
                                $slug = UtilityHelper::transliterate($translated);
                                if (empty($slug)) {
                                    $slug = $baseSlug;
                                } else {
                                    if (!empty($parentTranslations)) {
                                        $parentSlug = $parentTranslations[$locale][Translation::KEY_SLUG] ?? array_reduce($parentTranslations, function ($carry, $value) {
                                                return $carry ?? $value[Translation::KEY_SLUG] ?? null;
                                            }, null);
                                        if (!empty($parentSlug)) {
                                            $slug = "$parentSlug-$slug";
                                        }
                                    }
                                }
                                (new Translation([
                                    'key' => Translation::KEY_SLUG,
                                    'entity' => Translation::ENTITY_CATEGORY,
                                    'entity_id' => $category->id,
                                    'locale' => $locale,
                                    'value' => $slug,
                                ]))->save();
                            }
                            Console::output(".........success");
                        } else {
                            Console::output(".........fail");
                        }
                    } else {
                        Console::output(".........fail");
                    }
                }
            }
        }
    }
}