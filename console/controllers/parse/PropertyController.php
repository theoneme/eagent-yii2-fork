<?php

namespace console\controllers\parse;

use common\helpers\UtilityHelper;
use Exception;
use Yii;
use yii\console\Controller;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class PropertyController
 * @package console\controllers
 */
class PropertyController extends Controller
{
    public function actionFeed()
    {
        $sources = [
            'rtr72' => 'http://www.rtr72.ru/exportcian.php',
            'zagdom' => 'http://zagdom.net/base_for_yandex_eagent.xml',
            '21online' => 'http://buffer.21online.ru/exports/imls/main.xml',
            'ssk' => 'https://api.macrocrm.ru/estate/export/yandex/OzA5_WiGLTOJUuUfZsa-aAnYrqeYWBlO7q5wbzXLcTWdInddefntJn-Gx9oKTmqDosqdi_K_c8t7DgY12UvPknwk5s32xKmP7BLfrIEY6wyBolGBfU6d-dByUSAGeU11Uj98MTU1Mjg4Njk0MXxhNDQwNw/227-yandex.xml',
        ];
        $client = new Client([
            'responseConfig' => ['format' => Client::FORMAT_XML]
        ]);
        foreach ($sources as $source => $url) {
            UtilityHelper::makeDir(Yii::getAlias("@console") . "/import/feed/$source");
            try {
                /* @var Response $response */
                $response = $client->get($url)->send();
                $data = $response->getData();
                $isOk = $response->isOk;
            } catch (Exception $e) {
                $isOk = false;
            }
            if ($isOk && !empty($data)) {
                file_put_contents(Yii::getAlias("@console") . "/import/feed/$source/data.php", "<?php \n return " . VarDumper::export($data) . ";");
            }
        }
    }
}