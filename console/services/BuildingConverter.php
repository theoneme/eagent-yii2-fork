<?php

namespace console\services;

use common\helpers\FileHelper;
use common\models\Building;
use common\models\Property;
use common\repositories\sql\PropertyRepository;
use console\interfaces\ConverterInterface;
use frostealth\yii2\aws\s3\Service;
use Yii;

/**
 * Class BuildingConverter
 * @package console\services
 */
class BuildingConverter implements ConverterInterface
{
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var Service
     */
    private $_s3;

    /**
     * BuildingConverter constructor.
     * @param PropertyRepository $propertyRepository
     * @param Service $s3
     */
    public function __construct(PropertyRepository $propertyRepository, Service $s3)
    {
        $this->_propertyRepository = $propertyRepository;
        $this->_s3 = $s3;
    }

    /**
     * @param $rawData
     * @return array|null
     */
    public function convertObject($rawData)
    {
        if (empty($rawData) || !($rawData instanceof Property)) {
            return null;
        }

        $data = [
            'BuildingForm' => [
                'status' => Building::STATUS_ACTIVE,
                'type' => Building::TYPE_DEFAULT,
                'locale' => $rawData->locale
            ],
            'GeoForm' => [
                'lat' => $rawData->lat,
                'lng' => $rawData->lng,
            ]
        ];
        $attachmentProperties = $this->_propertyRepository->joinWith(['attachments'], true, 'INNER JOIN')
            ->limit(5)
            ->groupBy(['property.id'])
            ->findManyByCriteria([
                'lat' => $rawData->lat,
                'lng' => $rawData->lng,
            ]);
        $tempDir = FileHelper::createTempDirectory('@frontend');
        foreach ($attachmentProperties as $aProperty) {
            /* @var $aProperty Property */
            if (!empty($aProperty->attachments)) {
                $attachment = array_values($aProperty->attachments)[0];
                $pathInfo = pathinfo($attachment->content);
                $newName = hash('crc32b', uniqid($pathInfo['filename'], true)) . '.' . $pathInfo['extension'];
                if (file_exists(Yii::getAlias('@frontend') . '/web' . $attachment->content)) {
                    copy(Yii::getAlias('@frontend') . '/web' . $attachment->content, $tempDir['basePath'] . $newName);
                    $data['BuildingAttachmentForm'][]['content'] = $tempDir['relativePath'] . $newName;
                } else {
                    if ($this->_s3->exist(Yii::$app->mediaLayer->fixMediaPath($attachment->content))) {
                        $fullUrl = Yii::$app->mediaLayer->tryLoadFromAws($attachment->content);
                        $file = file_get_contents($fullUrl);
                        if ($file) {
                            file_put_contents($tempDir['basePath'] . $newName, $file);
                            $data['BuildingAttachmentForm'][]['content'] = $tempDir['relativePath'] . $newName;
                        }
                    }
                }
            }
        }

        return $data;
    }
}
