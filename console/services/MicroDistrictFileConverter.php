<?php

namespace console\services;

use common\helpers\SpatialHelper;
use common\models\Translation;
use common\services\entities\CityService;
use common\services\entities\DistrictService;
use console\interfaces\ConverterInterface;

/**
 * Class MicroDistrictFileConverter
 * @package console\services
 */
class MicroDistrictFileConverter implements ConverterInterface
{
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var DistrictService
     */
    private $_districtService;

    /**
     * MicroDistrictFileConverter constructor.
     * @param CityService $cityService
     * @param DistrictService $districtService
     */
    public function __construct(CityService $cityService, DistrictService $districtService)
    {
        $this->_cityService = $cityService;
        $this->_districtService = $districtService;
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        $city = $this->_cityService->getOne(['slug' => $rawData['city']]);
        if ($city === null) {
            return null;
        }
        $district = $this->_districtService->getOne(['city_id' => $city['id'], 'slug' => $rawData['district']]);
        $polygon = SpatialHelper::simplifyPolygon($rawData['polygon'], 1);
        $data = [
            'MicroDistrictForm' => [
                'city_id' => $city['id'],
                'district_id' => $district['id'] ?? null,
                'slug' => $rawData['slug'],
            ],
            'MetaForm' => [
                'ru-RU' => [
                    Translation::KEY_TITLE => $rawData['title'] ?? null,
                ]
            ],
            'LightGeoForm' => array_map(function ($point) {
                return ['lat' => $point[1], 'lng' => $point[0]];
            }, $polygon),
        ];

        return $data;
    }
}
