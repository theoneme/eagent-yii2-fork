<?php

namespace console\services;

use common\helpers\FileHelper;
use common\helpers\UtilityHelper;
use common\models\Category;
use common\models\Property;
use common\models\Translation;
use common\services\entities\AttributeService;
use common\services\GoogleMapsService;
use console\interfaces\ConverterInterface;
use yii\helpers\ArrayHelper;

/**
 * Class PrianPropertyConverter
 * @package console\services
 */
class PrianPropertyConverter implements ConverterInterface
{
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var
     */
    private $_googleMapsService;

    /**
     * @var array
     */
    private $_fieldToAttribute = [
        'Площадь' => 'property_area',
        'Площадь дома' => 'property_area',
        'Всего комнат' => 'rooms',
        'Количество спален' => 'bedrooms',
        'Количество ванных' => 'bathrooms',
        'Этаж' => 'floor',
        'Этажность' => 'floors',
        'newBuilding' => 'new_building',
        'amenities' => 'amenities',
    ];

    /**
     * @var array
     */
    private $_typeToType = [
        'buy' => Property::TYPE_SALE,
        'rent' => Property::TYPE_RENT,
    ];

    /**
     * @var array
     */
    private $_categoryToCategory = [
        'apartments' => 'flats',
        'houses' => 'houses',
        'commercial_property' => 'commercial-property',
        'land' => 'land',
    ];

    /**
     * @var array
     */
    private $_fieldHandlers;

    /**
     * N1PropertyConverter constructor.
     * @param AttributeService $attributeService
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(AttributeService $attributeService, GoogleMapsService $googleMapsService)
    {
        $this->_attributeService = $attributeService;
        $this->_googleMapsService = $googleMapsService;
        $this->_fieldHandlers = [
            'Площадь' => function ($var) {
                return (float)str_replace(',', '.', $var);
            },
            'Площадь дома' => function ($var) {
                return (float)str_replace(',', '.', $var);
            },
        ];
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        if (!isset($rawData['type'], $rawData['category']) || !isset($this->_typeToType[$rawData['type']], $this->_categoryToCategory[$rawData['category']])) {
            return null;
        }
        $categoryId = Category::find()->joinWith(['translations'])->where(['value' => $this->_categoryToCategory[$rawData['category']], 'key' => Translation::KEY_SLUG])->select('category.id')->scalar();

        $locale = isset($rawData['description']['ru-RU']) ? 'ru-RU' : 'en-GB';
        $data = [
            'PropertyForm' => [
                'user_id' => $rawData['user_id'],
                'status' => Property::STATUS_ACTIVE,
                'type' => $this->_typeToType[$rawData['type']],
                'locale' => $locale,
                'slug' => UtilityHelper::generateSlug($rawData['title'] ?? ''),
            ],
            'CategoryForm' => [
                'category_id' => $categoryId,
            ],
            'GeoForm' => [
                'lat' => $rawData['lat'] ?? null,
                'lng' => $rawData['lng'] ?? null,
            ],
            'PriceForm' => [
                'price' => (int)preg_replace('/[^0-9]/', '', $rawData['price']),
                'currency_code' => 'RUB',
            ]
        ];
        foreach ($rawData['description'] as $locale => $description) {
            $data['MetaForm'][$locale]['description'] = $description;
        }

        if (!empty($rawData['propertyData']['amenities']) && in_array('хорошее состояние', $rawData['propertyData']['amenities'])) {
            ArrayHelper::removeValue($rawData['propertyData']['amenities'], 'хорошее состояние');
            $rawData['propertyData']['condition'] = 'хорошее';
        }
        if (isset($rawData['propertyData'])) {
            foreach ($rawData['propertyData'] as $key => $value) {
                if (isset($this->_fieldToAttribute[$key])) {
                    $attribute = $this->_attributeService->getOne(['alias' => $this->_fieldToAttribute[$key]]);
                    $data['DynamicForm']["at_{$attribute['id']}"] = isset($this->_fieldHandlers[$key]) ? call_user_func($this->_fieldHandlers[$key], $value) : $value;
                }
            }
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        if (isset($rawData['imagesData'])) {
            foreach ($rawData['imagesData'] as $image) {
                $newAbsolutePath = $pathInfo['basePath'] . basename($image);
                $newRelativePath = $pathInfo['relativePath'] . basename($image);
                $headers = get_headers($image);
                if (strpos($headers[0], '200') !== false) {
                    $file = file_get_contents($image);
                    if ($file) {
                        file_put_contents($newAbsolutePath, $file);
                        $data['AttachmentForm'][]['content'] = $newRelativePath;
                    }
                }
            }
        }

        return $data;
    }
}
