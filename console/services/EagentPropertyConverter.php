<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 13:46
 */

namespace console\services;

use Aws\S3\Exception\S3Exception;
use common\exceptions\EntityNotFoundException;
use common\helpers\FileHelper;
use common\helpers\UtilityHelper;
use common\models\Category;
use common\models\Property;
use common\models\Translation;
use common\services\entities\AttributeService;
use console\interfaces\ConverterInterface;
use frostealth\yii2\aws\s3\Service;
use yii\base\Exception;
use yii\helpers\StringHelper;

/**
 * Class EagentPropertyConverter
 * @package console\services
 */
class EagentPropertyConverter implements ConverterInterface
{
    /**
     * @var Service
     */
    private $_s3;
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var array
     */
    private $_typeToType = [
        'property_sale' => Property::TYPE_SALE,
        'residential_rental' => Property::TYPE_RENT
    ];
    /**
     * @var array
     */
    private $_typeToCategory = [
        'Condo' => 'flats',
        'Single family home' => 'houses',
        'Apartment' => 'flats',
        'Townhouse' => 'houses',
        'Multifamily home' => 'houses',
        'Villa' => 'houses',
    ];

    /**
     * EagentPropertyConverter constructor.
     * @param Service $s3
     * @param AttributeService $attributeService
     */
    public function __construct(Service $s3, AttributeService $attributeService)
    {
        $this->_s3 = $s3;
        $this->_attributeService = $attributeService;
    }

    /**
     * @param $rawData
     * @return mixed
     * @throws EntityNotFoundException
     * @throws Exception
     */
    public function convertObject($rawData)
    {
        $category = Category::find()->joinWith(['translations'])->where(['value' => $this->_typeToCategory[$rawData['property']['type']], 'key' => Translation::KEY_SLUG])->one();

        if ($category === null) {
            throw new EntityNotFoundException('Category "flats" was not found');
        }

        $title = StringHelper::truncate(trim(preg_replace('/\-.*$/', '', $rawData['translation']['sluggable'])), 72);

        $data = [
            'PropertyForm' => [
                'status' => Property::STATUS_ACTIVE,
                'locale' => 'en-GB',
                'slug' => UtilityHelper::generateSlug($title),
                'user_id' => $rawData['property']['user_id'],
                'type' => $this->_typeToType[$rawData['property']['entity_type']],
            ],
            'GeoForm' => [
                'lat' => $rawData['property']['lat'],
                'address' => $rawData['property']['address'],
                'lng' => $rawData['property']['lon'],
            ],
            'MetaForm' => [
                'title' => $title,
                'description' => $rawData['property']['remarks'] ?? ''
            ],
            'PriceForm' => [
                'currency_code' => 'USD',
                'price' => $rawData['property']['price'],
            ],
            'CategoryForm' => [
                'category_id' => $category->id,
            ],
        ];

//        if ($rawData['property']['remarks']) {
//            $data['Translation'][] = [
//                'locale' => 'en-GB',
//                'key' => Translation::KEY_DESCRIPTION,
//                'value' => $rawData['property']['remarks'],
//                'entity' => Translation::ENTITY_PROPERTY
//            ];
//        }
//        if ($rawData['description']['bedroomDescription']) {
//            $data['Translation'][] = [
//                'locale' => 'en-GB',
//                'key' => 'bedroomDescription',
//                'value' => $rawData['description']['bedroomDescription'],
//                'entity' => Translation::ENTITY_PROPERTY
//            ];
//        }
//        if ($rawData['description']['designDescription']) {
//            $data['Translation'][] = [
//                'locale' => 'en-GB',
//                'key' => 'designDescription',
//                'value' => $rawData['description']['designDescription'],
//                'entity' => Translation::ENTITY_PROPERTY
//            ];
//        }
//
//        if ($rawData['description']['diningDescription']) {
//            $data['Translation'][] = [
//                'locale' => 'en-GB',
//                'key' => 'diningDescription',
//                'value' => $rawData['description']['diningDescription'],
//                'entity' => Translation::ENTITY_PROPERTY
//            ];
//        }
//
//        if ($rawData['description']['floorDescription']) {
//            $data['Translation'][] = [
//                'locale' => 'en-GB',
//                'key' => 'floorDescription',
//                'value' => $rawData['description']['floorDescription'],
//                'entity' => Translation::ENTITY_PROPERTY
//            ];
//        }

        if (!empty($rawData['property']['yearBuilt'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'year_built']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['yearBuilt'];
        }

        if (!empty($rawData['property']['areaSqFt'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'property_area']);
            $data['DynamicForm']["at_{$attribute['id']}"] = (int)($rawData['property']['areaSqFt'] * 0.092903);
        }

        if (!empty($rawData['property']['furnished'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'is_furnished']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['furnished'] == 'Furnished';
        }

        if (!empty($rawData['property']['garageSpaces'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'garages']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['garageSpaces'];
        }

        if (!empty($rawData['property']['balcony'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'balconies']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['balcony'];
        }

        if (!empty($rawData['property']['bedroom'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'bedrooms']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['bedroom'];

            $attribute = $this->_attributeService->getOne(['alias' => 'rooms']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['bedroom'];
        }

        if (!empty($rawData['property']['petsAllowed'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'are_pets_allowed']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['petsAllowed'];
        }

        if (!empty($rawData['property']['fBath'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'bathrooms']);
            $data['DynamicForm']["at_{$attribute['id']}"] = $rawData['property']['fBath'];
        }

        if (!empty($rawData['amenities'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'amenities']);
            foreach ($rawData['amenities'] as $amenitie) {
                $data['DynamicForm']["at_{$attribute['id']}"][] = $amenitie;
            }
        }

        if (!empty($rawData['interiors'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'interior_features']);
            foreach ($rawData['interiors'] as $interior) {
                $data['DynamicForm']["at_{$attribute['id']}"][] = $interior;
            }
        }

        if (!empty($rawData['exteriors'])) {
            $attribute = $this->_attributeService->getOne(['alias' => 'exterior_features']);
            foreach ($rawData['exteriors'] as $exterior) {
                $data['DynamicForm']["at_{$attribute['id']}"] = $exterior;
            }
        }

        if (!empty($rawData['property']['buildingName'])) {
            $data['BuildingMetaForm']['title'] = $rawData['property']['buildingName'];
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        foreach ($rawData['medias'] as $key => $media) {
            $dir1 = (int)($media['id'] / 100000);
            $dir2 = (int)(($media['id'] - ($dir1 * 100000)) / 1000);
            $dirPath = sprintf('%s/%04s/%02s', 'default', $dir1 + 1, $dir2 + 1);
            $mediaPath = "{$dirPath}/{$media['provider_reference']}";

            $newAbsolutePath = $pathInfo['basePath'] . $media['provider_reference'];
            $newRelativePath = $pathInfo['relativePath'] . $media['provider_reference'];
            try {
                $this->_s3->commands()
                    ->get($mediaPath)
                    ->saveAs($newAbsolutePath)->execute();

                $data['AttachmentForm'][]['content'] = $newRelativePath;

            } catch (S3Exception $e) {

            }

            if($key === 0) {
                $newAbsolutePath = $pathInfo['basePath'] . 'b-' . $media['provider_reference'];
                $newRelativePath = $pathInfo['relativePath'] . 'b-' . $media['provider_reference'];

                try {
                    $this->_s3->commands()
                        ->get($mediaPath)
                        ->saveAs($newAbsolutePath)->execute();

                    $data['BuildingAttachmentForm'][]['content'] = $newRelativePath;

                } catch (S3Exception $e) {

                }
            }
        }

        return $data;
    }
}
