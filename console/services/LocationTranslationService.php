<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 14:42
 */

namespace console\services;

use common\helpers\UtilityHelper;
use common\models\Translation;
use common\services\entities\TranslationService;
use common\services\GoogleMapsService;
use common\services\YandexTranslationService;
use Yii;

/**
 * Class LocationTranslationService
 * @package console\services
 */
class LocationTranslationService
{
    /**
     * @var GoogleMapsService
     */
    private $_gmapsService;
    /**
     * @var TranslationService
     */
    private $_translationService;
    /**
     * @var YandexTranslationService
     */
    private $_yandexTranslationService;
    /**
     * @var array
     */
    private $_entityToGeocode = [
        Translation::ENTITY_COUNTRY => 'country',
        Translation::ENTITY_REGION => 'region',
        Translation::ENTITY_CITY => 'city'
    ];

    /**
     * RegionTranslationService constructor.
     * @param GoogleMapsService $googleMapsService
     * @param TranslationService $translationService
     * @param YandexTranslationService $yandexTranslationService
     */
    public function __construct(GoogleMapsService $googleMapsService, TranslationService $translationService, YandexTranslationService $yandexTranslationService)
    {
        $this->_gmapsService = $googleMapsService;
        $this->_translationService = $translationService;
        $this->_yandexTranslationService = $yandexTranslationService;
    }

    /**
     * @param $locationItem
     * @param $entity
     * @param $from
     * @param $to
     * @return bool
     */
    public function translate($locationItem, $entity, $from, $to)
    {
        $success = true;

        $targetLocale = Yii::$app->params['supportedLocales'][$to];
        if (array_key_exists('lat', $locationItem) && array_key_exists('lng', $locationItem)) {
            $latlng = [
                'lat' => $locationItem['lat'],
                'long' => $locationItem['lng']
            ];
        } else {
            $latlng = $this->_gmapsService->geocode($locationItem['fullTitle'] ?? $locationItem['title'], $targetLocale);
        }
        $addressComponents = $this->_gmapsService->reverseGeocodeAsData($latlng['lat'], $latlng['long'], $targetLocale);
        if ($addressComponents !== false && array_key_exists($this->_entityToGeocode[$entity], $addressComponents)) {
            $newTitle = $addressComponents[$this->_entityToGeocode[$entity]];
        } else {
            $sourceLocale = Yii::$app->params['supportedLocales'][$from];
            $targetLocale = Yii::$app->params['supportedLocales'][$to];
            $newTitle = $this->_yandexTranslationService->translate($locationItem['title'], $sourceLocale, $targetLocale);
        }

        $translationData = [
            'locale' => $to,
            'key' => Translation::KEY_TITLE,
            'value' => $newTitle,
            'entity' => $entity,
            'entity_id' => $locationItem['id']
        ];
        $translationId = $this->_translationService->create($translationData);
        $success = $success && $translationId !== null;

        $translationData = [
            'locale' => $to,
            'key' => Translation::KEY_SLUG,
            'value' => UtilityHelper::transliterate($newTitle),
            'entity' => $entity,
            'entity_id' => $locationItem['id']
        ];
        $translationId = $this->_translationService->create($translationData);
        $success = $success && $translationId !== null;

        return $success;
    }
}
