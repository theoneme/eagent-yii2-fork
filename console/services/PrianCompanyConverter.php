<?php

namespace console\services;

use common\helpers\FileHelper;
use common\helpers\XpathHelper;
use common\models\UserContact;
use common\services\GoogleMapsService;
use console\interfaces\ConverterInterface;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use yii\helpers\StringHelper;

/**
 * Class PrianCompanyConverter
 * @package console\services
 */
class PrianCompanyConverter implements ConverterInterface
{
    /**
     * @var GoogleMapsService
     */
    private $_googleMapsService;

    /**
     * PrianCompanyConverter constructor.
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(GoogleMapsService $googleMapsService)
    {
        $this->_googleMapsService = $googleMapsService;
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        if (empty($rawData['docData']) || empty($rawData['siteData'])) {
            return null;
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        $data = [
            'userData' => [],
            'companyData' => [
                'ContactForm' => [],
                'MetaForm' => ['locale' => 'ru-RU']
            ]
        ];

        if (!empty($rawData['docData']['name'])) {
            $data['companyData']['MetaForm']['title'] = $rawData['docData']['name'];
        }

        if (!empty($rawData['docData']['emails'])) {
            $emails = explode(',', $rawData['docData']['emails']);
            foreach ($emails as $email) {
                $email = trim($email);
                if (!empty($email)) {
                    $data['companyData']['ContactForm'][] = [
                        'type' => UserContact::TYPE_EMAIL,
                        'value' => $email
                    ];
                }
            }
        }

        if (!empty($rawData['docData']['site'])) {
            $data['companyData']['ContactForm'][] = [
                'type' => UserContact::TYPE_WEBSITE,
                'value' => $rawData['docData']['site']
            ];
        }

        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $domData = preg_replace("#<script(.*?)>(.*?)</script>#is", '', $rawData['siteData']);
//        Yii::error($domData);die;
        $dom->loadHTML($domData);

        $xpath = new DomXPath($dom);

        /** @var DOMNodeList $logoNode */
        $logoNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-content/div.logotype/img.logo"));
        if ($logoNode->length) {
            $image = "https:" . $logoNode->item(0)->getAttribute('src');
            $newAbsolutePath = $pathInfo['basePath'] . basename($image);
            $newRelativePath = $pathInfo['relativePath'] . basename($image);
            $headers = get_headers($image);
            if (strpos($headers[0], '200') !== false) {
                $file = file_get_contents($image);
                if ($file) {
                    file_put_contents($newAbsolutePath, $file);
                    $data['companyData']['CompanyForm']['logo'] = $newRelativePath;
                }
            }
        }

        /** @var DOMNodeList $bannerNode */
        $bannerNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-content/div.logotype"));
        if ($bannerNode->length) {
            $image = "https:" . preg_replace("/[\s\S]*background-image:url\(([^()]*)\)[\s\S]*/", "$1", $bannerNode->item(0)->getAttribute('style'));
            $newAbsolutePath = $pathInfo['basePath'] . basename($image);
            $newRelativePath = $pathInfo['relativePath'] . basename($image);
            $headers = get_headers($image);
            if (strpos($headers[0], '200') !== false) {
                $file = file_get_contents($image);
                if ($file) {
                    file_put_contents($newAbsolutePath, $file);
                    $data['companyData']['CompanyForm']['banner'] = $newRelativePath;
                }
            }
        }

        /** @var DOMNodeList $descriptionNode */
        $descriptionNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-content/div.pr-b-company-about/p"));
        if ($descriptionNode->length) {
            $data['companyData']['MetaForm']['description'] = trim(strip_tags(preg_replace("#<table(.*?)>(.*?)</table>#is", '', $dom->saveHTML($descriptionNode->item(0))), '<p><ul><ol><li><br>'));
        }

        /** @var DOMNodeList $phoneNode */
        $phoneNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-office/p/a[contains(@href,\"tel:\")]"));
        if ($phoneNode->length) {
            $data['companyData']['ContactForm'][] = [
                'type' => UserContact::TYPE_PHONE,
                'value' => "+" . preg_replace("/[^0-9]/", "", $phoneNode->item(0)->getAttribute('href'))
            ];
        }

        /** @var DOMNodeList $addressNode */
        $addressNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-office/p[1]/span[1]"));

        if ($addressNode->length) {
            $latLng = $this->_googleMapsService->geocode($addressNode->item(0)->textContent);
            if (!empty($latLng['lat']) && !empty($latLng['long'])) {
                $data['companyData']['GeoForm'] = [
                    'lat' => $latLng['lat'],
                    'lng' => $latLng['long'],
                ];
            }
        }

        /** @var DOMNodeList $messengerNodes */
        $messengerNodes = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-mess/p/a[contains(@href,\"tel:\")]"));
        if ($messengerNodes->length) {
            foreach ($messengerNodes as $messengerNode) {
                /** @var DOMElement $messengerNode*/
                $value = "+" . preg_replace("/[^0-9]/", "", $messengerNode->getAttribute('href'));
                $content = $dom->saveHTML($messengerNode);
                $messengers = [
                    "whatsapp" => UserContact::TYPE_WHATSAPP,
                    "viber" => UserContact::TYPE_VIBER,
                    "telegram" => UserContact::TYPE_TELEGRAM
                ];
                foreach ($messengers as $messenger => $type) {
                    if(preg_match("/title=\"($messenger)\"/", $content)) {
                        $data['companyData']['ContactForm'][] = [
                            'type' => $type,
                            'value' => $value
                        ];
                    }
                }
            }
        }

        /** @var DOMNodeList $phoneNode */
        $socials = [
            "fb" => UserContact::TYPE_FACEBOOK,
            "vk" => UserContact::TYPE_VK,
            "tw" => UserContact::TYPE_TWITTER,
            "gp" => UserContact::TYPE_GOOGLE,
            "yt" => UserContact::TYPE_YOUTUBE,
            "ok" => UserContact::TYPE_OK,
        ];
        foreach ($socials as $class => $type) {
            $socialNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-office/p.socnet/span/a.$class"));
            if ($socialNode->length) {
                $value = StringHelper::truncateWords($socialNode->item(0)->getAttribute('href'), 1, '');
                if (mb_strlen($value) < 150) {
                    $data['companyData']['ContactForm'][] = [
                        'type' => $type,
                        'value' => $value
                    ];
                }
            }
        }

        /** @var DOMNodeList $userAvatarNode */
        $userAvatarNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-face/dl[1]/dt/img"));
        if ($userAvatarNode->length) {
            $image = "https:" . $userAvatarNode->item(0)->getAttribute('src');
            $newAbsolutePath = $pathInfo['basePath'] . basename($image);
            $newRelativePath = $pathInfo['relativePath'] . basename($image);
            $headers = get_headers($image);
            if (strpos($headers[0], '200') !== false) {
                $file = file_get_contents($image);
                if ($file) {
                    file_put_contents($newAbsolutePath, $file);
                    $data['userData']['avatar'] = $newRelativePath;
                }
            }
        }

        /** @var DOMNodeList $userNameNode */
        $userNameNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-face/dl[1]/dd/span.name"));
        if ($userNameNode->length) {
            $data['userData']['username'] = $userNameNode->item(0)->firstChild->textContent;
        }

        /** @var DOMNodeList $userPhoneNode */
        $userPhoneNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-face/dl[1]/dd/div/a.phone"));
        if ($userPhoneNode->length) {
            $data['userData']['phone'] = "+" . preg_replace("/[^0-9]/", "", $userPhoneNode->item(0)->getAttribute('href'));
        }

        /** @var DOMNodeList $userPhoneNode */
        $userEmailNode = $xpath->query(XpathHelper::cssToXpath("//div.pr-b-contact/div.contact-item/div.pr-b-company-face/dl[1]/dd/a.mail"));
        if ($userEmailNode->length) {
            $data['userData']['email'] = preg_replace("/^mailto:/", "", $userEmailNode->item(0)->getAttribute('href'));
        }

        return $data;
    }
}
