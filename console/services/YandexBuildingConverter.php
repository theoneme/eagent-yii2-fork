<?php

namespace console\services;

use common\helpers\FileHelper;
use common\models\Building;
use common\models\BuildingDocument;
use common\models\BuildingPhase;
use common\models\BuildingSite;
use common\models\Translation;
use common\services\entities\AttributeService;
use console\interfaces\ConverterInterface;

/**
 * Class YandexBuildingConverter
 * @package console\services
 */
class YandexBuildingConverter implements ConverterInterface
{
    /**
     * @var AttributeService
     */
    private $_attributeService;

    /**
     * @var array
     */
    private $_fieldToAttribute = [
        'этаж' => 'floors',
        'этажа' => 'floors',
        'этажей' => 'floors',
        'потолки' => 'ceiling_height',
        'тип стен' => 'material',
        'mortgage' => 'new_construction_mortgage',
        'installment' => 'new_construction_installment_plan',
        'discount' => 'new_construction_discount',
    ];

    /**
     * @var array
     */
    private $_fieldHandlers;

    /**
     * YandexBuildingConverter constructor.
     * @param AttributeService $attributeService
     */
    public function __construct(AttributeService $attributeService)
    {
        $this->_attributeService = $attributeService;
        $this->_fieldHandlers = [
            'этаж' => 'floorHandler',
            'этажа' => 'floorHandler',
            'этажей' => 'floorHandler',
            'потолки' => 'floatHandler',
            'mortgage' => function ($var) {
                return $var['minRate'] ?? null;
            },
            'installment' => function ($var) {
                return $var['durationMonths'] ?? null;
            },
            'discount' => function ($var) {
                $pattern = "/^Скидка (\d+)%$/u";
                $text = $var['description'] ?? $var['shortDescription'] ?? '';
                return preg_match($pattern, $text) ? preg_replace($pattern, "$1", $text) : null;
            },
        ];
    }

    /**
     * @param $rawData
     * @return array|null
     */
    public function convertObject($rawData)
    {
        if (empty($rawData['lat']) || empty($rawData['lng'])) {
            return null;
        }

        $data = [
            'BuildingForm' => [
                'status' => Building::STATUS_ACTIVE,
                'type' => Building::TYPE_NEW_CONSTRUCTION,
                'locale' => 'ru-RU',
                'user_id' => $rawData['user_id']
            ],
            'BuildingMetaForm' => [
                'ru-RU' => [
                    Translation::KEY_NAME => $rawData['title'] ?? null,
                    Translation::KEY_DESCRIPTION => $rawData['description'] ?? null,
                ]
            ],
            'GeoForm' => [
                'lat' => $rawData['lat'],
                'lng' => $rawData['lng'],
            ],
            'BuildingDocumentForm' => [],
            'BuildingProgressForm' => [],
            'BuildingPhaseForm' => [],
            'BuildingSiteForm' => [],
        ];

        // Атрибуты
        foreach ($this->_fieldToAttribute as $field => $attribute) {
            if (isset($rawData['attributes'][$field])) {
                $attributeObject = $this->_attributeService->getOne(['alias' => $attribute]);
                $value = $this->handleHandlers($field, $rawData['attributes'][$field]);
                if ($value !== null) {
                    $data['DynamicForm']["at_{$attributeObject['id']}"] = $value;
                }
            }
        }
        if (!empty($rawData['specials'])) {
            foreach ($rawData['specials'] as $special) {
                $specialType = $special['specialProposalType'] ?? null;
                if (isset($this->_fieldToAttribute[$specialType])) {
                    $attributeObject = $this->_attributeService->getOne(['alias' => $this->_fieldToAttribute[$specialType]]);
                    $value = $this->handleHandlers($specialType, $special);
                    if ($value !== null) {
                        if (empty($data['DynamicForm']["at_{$attributeObject['id']}"]) || (!empty($special['mainProposal']) && $special['mainProposal'] === true)) {
                            $data['DynamicForm']["at_{$attributeObject['id']}"] = $value;
                        }
                    }
                }
            }
        }
        if (!empty($rawData['companyData']['name']) && mb_strlen($rawData['companyData']['name']) < 65) {
            $attributeObject = $this->_attributeService->getOne(['alias' => 'new_construction_builder']);
            $data['DynamicForm']["at_{$attributeObject['id']}"] = $rawData['companyData']['name'];
        }

        // Документы
        $pathInfo = FileHelper::createTempDirectory('@frontend');
        if (!empty($rawData['documentsData'])) {
            foreach ($rawData['documentsData'] as $type => $documents) {
                switch ($type) {
                    case 'buildingPermitDocs':
                        $ourType = BuildingDocument::TYPE_PERMISSION;
                        break;
                    case 'projectDeclarationDocs':
                        $ourType = BuildingDocument::TYPE_PROJECT;
                        break;
                    case 'operationActDocs':
                        $ourType = BuildingDocument::TYPE_OPERATION_ACT;
                        break;
                    default:
                        $ourType = null;
                        break;
                }
                if (!empty($documents) && $ourType !== null) {
                    foreach ($documents as $document) {
                        if (!empty($document['downloadUrl'])) {
                            $data['BuildingDocumentForm'][] = [
                                'type' => $ourType,
                                'name' => $document['name'] ?? null,
                                'file' => $document['downloadUrl'],
                            ];
//                            $path = FileHelper::downloadFile($document['downloadUrl'], $pathInfo);
//                            if ($path) {
//                                $data['BuildingDocumentForm'][] = [
//                                    'name' => $document['name'] ?? null,
//                                    'file' => $path,
//                                ];
//                                $imagesUploaded++;
//                            }
//                            if ($imagesUploaded >= 10) {
//                                break;
//                            }
                        }
                    }
                }
            }
        }
        // Прогресс строительства
        if (!empty($rawData['constructionData'])) {
            foreach ($rawData['constructionData'] as $progressItem) {
                $attachments = [];
                if (!empty($progressItem['photos'])) {
                    $imagesUploaded = 0;
                    foreach ($progressItem['photos'] as $image) {
                        $imagePath = FileHelper::downloadFile($image, $pathInfo);
                        if ($imagePath) {
                            $attachments[]['content'] = $imagePath;
                            $imagesUploaded++;
                        }
                        if ($imagesUploaded >= 10) {
                            break;
                        }
                    }
                }
                $data['BuildingProgressForm'][] = [
                    'year' => $progressItem['year'] ?? null,
                    'quarter' => $progressItem['quarter'] ?? null,
                    'BuildingProgressAttachmentForm' => $attachments
                ];
            }
        }

        // Очереди
        if (!empty($rawData['deliveryDates'])) {
            foreach ($rawData['deliveryDates'] as $phase) {
                $commonData = [
                    'year' => $phase['year'] ?? null,
                    'quarter' => $phase['quarter'] ?? null,
                    'status' => !empty($phase['state']) && $phase['state'] === 'HAND_OVER' ? BuildingPhase::STATUS_FINISHED : BuildingPhase::STATUS_UNFINISHED,
                ];
                if (!empty($phase['housesInfo'])) {
                    foreach ($phase['housesInfo'] as $houseInfo) {
                        $data['BuildingPhaseForm'][] = array_merge($commonData, [
                            'lat' => $houseInfo['lat'] ?? null,
                            'lng' => $houseInfo['lng'] ?? null,
                            'floors' => $houseInfo['maxFloor'] ?? null,
                        ]);
                    }
                }
                else {
                    $data['BuildingPhaseForm'][] = $commonData;
                }
            }
        }

        //Места
        if (!empty($rawData['sitesData'])) {
            foreach ($rawData['sitesData'] as $site) {
                switch ($site['type'] ?? null) {
                    case 'metro':
                        $ourType = BuildingSite::TYPE_METRO;
                        break;
                    case 'schools':
                        $ourType = BuildingSite::TYPE_SCHOOL;
                        break;
                    case 'parks':
                        $ourType = BuildingSite::TYPE_PARK;
                        break;
                    case 'ponds':
                        $ourType = BuildingSite::TYPE_POND;
                        break;
                    case 'airports':
                        $ourType = BuildingSite::TYPE_AIRPORT;
                        break;
                    case 'cityCenter':
                        $ourType = BuildingSite::TYPE_CITY_CENTER;
                        break;
                    default:
                        $ourType = null;
                        break;
                }
                if ($ourType !== null) {
                    switch ($site['transport'] ?? null) {
                        case 'ON_FOOT':
                            $ourTransport = BuildingSite::TRANSPORT_FOOT;
                            break;
                        case 'ON_CAR':
                            $ourTransport = BuildingSite::TRANSPORT_CAR;
                            break;
                        case 'ON_TRANSPORT':
                            $ourTransport = BuildingSite::TRANSPORT_TRANSPORT;
                            break;
                        default:
                            $ourTransport = null;
                            break;
                    }
                    $data['BuildingSiteForm'][] = [
                        'type' => $ourType,
                        'transport' => $ourTransport,
                        'name' => $site['name'] ?? null,
                        'time' => $site['time'] ?? null,
                        'distance' => $site['distance'] ?? null,
                        'lat' => $site['lat'] ?? null,
                        'lng' => $site['lng'] ?? null,
                    ];
                }
            }
        }

        // Картинки
        if (isset($rawData['imagesData'])) {
            $imagesUploaded = 0;
            foreach ($rawData['imagesData'] as $image) {
                $imagePath = FileHelper::downloadFile($image, $pathInfo);
                if ($imagePath) {
                    $data['BuildingAttachmentForm'][]['content'] = $imagePath;
                    $imagesUploaded++;
                }
                if ($imagesUploaded >= 10) {
                    break;
                }
            }
        }

        return $data;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function areaHandler($value)
    {
        $pattern = "/^([\d\.]+)([^\d\.].*)?/";
        return preg_match($pattern, $value) ? (float)preg_replace($pattern, '$1', $value) : null;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function floorHandler($value)
    {
        $pattern = "/.*[^\d]([\d]+)$/";
        return preg_match($pattern, $value) ? (int)preg_replace($pattern, '$1', $value) : null;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function floatHandler($value)
    {
        $pattern = "/^([\d\.]+)([^\d\.].*)?/";
        return preg_match($pattern, $value) ? (float)preg_replace($pattern, '$1', str_replace(',', '.', $value)) : null;
    }

    /**
     * @param $field
     * @param $rawValue
     * @return mixed
     */
    protected function handleHandlers($field, $rawValue)
    {
        if(isset($this->_fieldHandlers[$field])) {
            $handler = $this->_fieldHandlers[$field];
            if(is_callable($handler)) {
                $value = call_user_func($this->_fieldHandlers[$field], $rawValue);
            } else {
                $value = $this->{$this->_fieldHandlers[$field]}($rawValue);
            }
        } else {
            $value = $rawValue;
        }

        return $value;
    }
}
