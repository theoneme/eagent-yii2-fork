<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 13:46
 */

namespace console\services;

use common\helpers\FileHelper;
use common\models\Property;
use common\services\ApiClientService;
use common\services\entities\AttributeService;
use console\interfaces\ConverterInterface;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\base\Exception;

/**
 * Class UjobsPropertyConverter
 * @package console\services
 */
class UjobsPropertyConverter implements ConverterInterface
{
    /**
     * @var array
     */
    private $_categoriesMapping;
    /**
     * @var Service
     */
    private $_s3;
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var ApiClientService
     */
    private $_apiClientService;

    /**
     * @var array
     */
    private $_fieldToAttribute = [
        'Площадь, м² (общая)' => 'property_area',
        'Площадь, м² (жилая)' => 'living_area',
        'Площадь кухни, м²' => 'kitchen_area',
        'Планировка' => 'layout',
        'Тип санузла' => 'bathroom_type',
        'Состояние' => 'condition',
        'Тип собственности' => 'property_type',
        'Количество балконов' => 'balconies',
        'Количество лоджий' => 'loggias',
        'Количество комнат' => 'rooms',
        'Тип дома' => 'building_type',
        'Этаж' => 'floor',
        'Этажность' => 'floors',
        'Год постройки' => 'year_built',
    ];

    /**
     * UjobsPropertyConverter constructor.
     * @param array $categoriesMapping
     * @param Service $s3
     * @param AttributeService $attributeService
     * @param ApiClientService $apiClientService
     */
    public function __construct(
        array $categoriesMapping,
        Service $s3,
        AttributeService $attributeService,
        ApiClientService $apiClientService
    )
    {
        $this->_categoriesMapping = $categoriesMapping;
        $this->_s3 = $s3;
        $this->_attributeService = $attributeService;
        $this->_apiClientService = $apiClientService;
    }

    /**
     * @param $rawData
     * @return mixed
     * @throws Exception
     */
    public function convertObject($rawData)
    {
        $data = [
            'PropertyForm' => [
                'locale' => $rawData['locale'],
                'slug' => $rawData['alias'],
                'status' => Property::STATUS_ACTIVE,
                'user_id' => $rawData['user_id'],
                'type' => Property::TYPE_SALE,
            ],
            'PriceForm' => [
                'price' => $rawData['price'],
                'currency_code' => $rawData['currency_code'],
            ],
            'CategoryForm' => [
                'category_id' => $this->_categoriesMapping[$rawData['category_id']],
            ],
            'GeoForm' => [
                'lat' => $rawData['lat'],
                'lng' => $rawData['long'],
            ]
        ];

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        foreach ($rawData['attachments'] as $attachment) {
            if (!empty($attachment['content']) && !preg_match('/^http(s)?:\/\/.*/', $attachment['content'])) {
                $newAbsolutePath = $pathInfo['basePath'] . basename($attachment['content']);
                $newRelativePath = $pathInfo['relativePath'] . basename($attachment['content']);

                try {
                    $this->_s3->commands()
                        ->get(Yii::$app->mediaLayer->fixMediaPath($attachment['content']))
                        ->saveAs($newAbsolutePath)->execute();
                    $data['AttachmentForm'][]['content'] = $newRelativePath;

                } catch (Exception $e) {

                }
            }
        }
        if (!empty($rawData['translation'])) {
            $data['MetaForm'][$rawData['locale']] = [
                'title' => $rawData['translation']['title'],
                'description' => $rawData['translation']['content']
            ];
        }

        foreach ($rawData['extra'] as $attribute) {
            $attributeData = $this->_apiClientService->makeRequest('get', "https://ujobs.me/apientry/attributes/{$attribute['attribute_id']}", ['expand' => "translations"]);
            if ($attributeData && isset($this->_fieldToAttribute[$attributeData['translations']['ru-RU']['title']])) {
                $ourAttribute = $this->_attributeService->getOne(['alias' => $this->_fieldToAttribute[$attributeData['translations']['ru-RU']['title']]]);
                $attributeValueData = $this->_apiClientService->makeRequest('get', "https://ujobs.me/apientry/attribute-values/{$attribute['value']}", ['expand' => "translations"]);
                if ($attributeValueData && isset($attributeValueData['translations']['ru-RU']['title'])) {
                    $data['DynamicForm']["at_{$ourAttribute['id']}"] = $attributeValueData['translations']['ru-RU']['title'];
                }
            }
        }

        return $data;
    }
}
