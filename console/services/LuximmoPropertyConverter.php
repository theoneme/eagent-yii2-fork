<?php

namespace console\services;

use common\helpers\FileHelper;
use common\models\Category;
use common\models\Property;
use common\models\Translation;
use common\services\entities\AttributeService;
use common\services\GoogleMapsService;
use console\interfaces\ConverterInterface;

/**
 * Class LuximmoPropertyConverter
 * @package console\services
 */
class LuximmoPropertyConverter implements ConverterInterface
{
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var
     */
    private $_googleMapsService;

    /**
     * @var array
     */
    private $_fieldToAttribute = [
        'Total area:' => 'property_area',
        'Number of floors:' => 'floors',
        'Floor:' => 'floor',
        'Bedrooms:' => 'bedrooms',
        'Bathrooms' => 'bathrooms',
        'Condition' => 'condition',
        'Heating' => 'amenities',
        'Furnishing' => 'is_furnished',
    ];

    /**
     * @var array
     */
    private $_typeToType = [
        'sell' => Property::TYPE_SALE,
        'rent' => Property::TYPE_RENT,
    ];

    /**
     * @var array
     */
    private $_categoryToCategory = [
        'flats' => 'flats',
        'houses' => 'houses',
        'commercial-property' => 'commercial-property',
        'land' => 'land',
    ];

    /**
     * @var array
     */
    private $_fieldHandlers;

    /**
     * N1PropertyConverter constructor.
     * @param AttributeService $attributeService
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(AttributeService $attributeService, GoogleMapsService $googleMapsService)
    {
        $this->_attributeService = $attributeService;
        $this->_googleMapsService = $googleMapsService;
        $this->_fieldHandlers = [
            'Total area:' => 'areaHandler',
            'Number of floors:' => 'integerHandler',
            'Floor:' => 'integerHandler',
            'Bedrooms:' => 'integerHandler',
            'Bathrooms' => function ($var) {
                return 1;
            },
            'Condition' => function ($var) {
                switch ($var) {
                    case 'excellent':
                        return 'В отличном состоянии';
                        break;
                    case 'unfinished internally':
                        return 'Требует косметического ремонта';
                        break;
//                    case 'key-ready (unfurnished)':
//                        return '';
//                        break;
                    case 'good':
                        return 'Хорошее';
                        break;
                    default:
                        return null;
                }
            },
            'Heating' => function ($var) {
                return explode('/', $var);
            },
            'Furnishing' => function ($var) {
                return $var !== 'Unfurnished';
            },
        ];
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        if (!isset($rawData['type'], $rawData['category']) || !isset($this->_typeToType[$rawData['type']], $this->_categoryToCategory[$rawData['category']])) {
            return null;
        }
        $categoryId = Category::find()->joinWith(['translations'])->where(['value' => $this->_categoryToCategory[$rawData['category']], 'key' => Translation::KEY_SLUG])->select('category.id')->scalar();

        $data = [
            'PropertyForm' => [
                'user_id' => $rawData['user_id'],
                'status' => Property::STATUS_ACTIVE,
                'type' => $this->_typeToType[$rawData['type']],
                'locale' => 'en-GB',
//                'slug' => UtilityHelper::generateSlug($rawData['title'] ?? ''),
            ],
            'CategoryForm' => [
                'category_id' => $categoryId,
            ],
            'MetaForm' => [
                'en-DB' => [
                    'description' => $rawData['description'] ?? null,
                ]
            ],
            'GeoForm' => [
                'lat' => $rawData['lat'] ?? null,
                'lng' => $rawData['lng'] ?? null,
            ],
            'PriceForm' => [
                'price' => (int)preg_replace('/[^0-9]/', '', $rawData['price']),
                'currency_code' => 'EUR',
            ]
        ];

        foreach ($this->_fieldToAttribute as $field => $attribute) {
            if (isset($rawData['propertyData'][$field])) {
                $attributeObject = $this->_attributeService->getOne(['alias' => $attribute]);
                $data['DynamicForm']["at_{$attributeObject['id']}"] = $this->handleHandlers($field, $rawData['propertyData'][$field]);
            }
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        if (isset($rawData['imagesData'])) {
            $imagesUploaded = 0;
            foreach ($rawData['imagesData'] as $image) {
                $newAbsolutePath = $pathInfo['basePath'] . basename($image);
                $newRelativePath = $pathInfo['relativePath'] . basename($image);
                $headers = get_headers($image);
                if (strpos($headers[0], '200') !== false) {
                    $file = file_get_contents($image);
                    if ($file) {
                        file_put_contents($newAbsolutePath, $file);
                        $data['AttachmentForm'][]['content'] = $newRelativePath;
                        $imagesUploaded++;
                    }
                }
                if ($imagesUploaded >= 10) {
                    break;
                }
            }
        }

        return $data;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function areaHandler($value)
    {
        $pattern = "/^([\d\.]+)([^\d\.].*)?/";
        return preg_match($pattern, $value) ? (float)preg_replace($pattern, '$1', $value) : null;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function integerHandler($value)
    {
        $pattern = "/^([\d]+)([^\d].*)?/";
        return preg_match($pattern, $value) ? (int)preg_replace($pattern, '$1', $value) : null;
    }

    /**
     * @param $field
     * @param $rawValue
     * @return mixed
     */
    protected function handleHandlers($field, $rawValue)
    {
        if(isset($this->_fieldHandlers[$field])) {
            $handler = $this->_fieldHandlers[$field];
            if(is_callable($handler)) {
                $value = call_user_func($this->_fieldHandlers[$field], $rawValue);
            } else {
                $value = $this->{$this->_fieldHandlers[$field]}($rawValue);
            }
        } else {
            $value = $rawValue;
        }

        return $value;
    }
}
