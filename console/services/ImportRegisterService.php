<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 14:42
 */

namespace console\services;

use common\exceptions\EntityNotFoundException;
use common\models\Flag;
use common\models\Import;
use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\PropertyService;
use common\services\entities\RegionService;
use common\services\entities\UserService;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class ImportRegisterService
 * @package console\services
 */
class ImportRegisterService
{
    /**
     * @param $entity
     * @param $entityId
     * @param $source
     * @param $origin
     * @return bool
     */
    public function registerImport($entity, $entityId, $source, $origin = null)
    {
        return (new Import([
            'entity' => $entity,
            'entity_id' => $entityId,
            'source' => $source,
            'origin' => $origin,
        ]))->save();
    }

    /**
     * @param $entity
     * @param $entityId
     * @param $source
     * @param null $origin
     * @param array $flags
     * @return bool
     */
    public function registerImportWithFlags($entity, $entityId, $source, $origin = null, $flags = [])
    {
        $import = new Import([
            'entity' => $entity,
            'entity_id' => $entityId,
            'source' => $source,
            'origin' => $origin,
        ]);
        $saved = $import->save();
        if ($saved) {
            foreach ($flags as $type => $value) {
                (new Flag(['entity' => 'import', 'entity_id' => $import->id, 'type' => $type, 'value' => $value]))->save();
            }
        }
        return $saved;
    }

    /**
     * @param $entity
     * @param $entityId
     * @param $source
     * @return false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function unregisterImport($entity, $entityId, $source)
    {
        return (Import::find()->where(['entity' => $entity, 'entity_id' => $entityId, 'source' => $source])->one())->delete();
    }

    /**
     * @param $condition
     * @return int|string
     */
    public function getCountByCondition($condition)
    {
        return Import::find()->where($condition)->count();
    }

    /**
     * @param $condition
     * @param array $flags
     * @return int|string
     */
    public function getCountByConditionAndFlags($condition, $flags = [])
    {
        $query = Import::find()->where($condition);
        foreach ($flags as $type => $value) {
            $query->joinWith(['flags' => function($q) use ($type){
                /* @var $q ActiveQuery*/
                return $q->from(["flag-$type" => 'flag'])->onCondition(["flag-$type.entity" => 'import']);
            }])->andWhere(["flag-$type.value" => $value])->groupBy(['import.id']);
        }
        return $query->count();
    }

    /**
     * @param $condition
     * @return bool
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\di\NotInstantiableException
     */
    public function cleanByCondition($condition)
    {
        $items = Import::find()->where($condition);
        $result = true;

        foreach ($items->batch(30) as $batch) {
            foreach ($batch as $item) {
                /** @var Import $item */
                switch ($item->entity) {
                    case 'property':
                        /** @var PropertyService $propertyService */
                        $propertyService = Yii::$container->get(PropertyService::class);
                        $result = $result && $propertyService->delete(['id' => $item->entity_id]);
                        break;
                    case 'user':
                        /** @var UserService $userService */
                        $userService = Yii::$container->get(UserService::class);
                        try {
                            $result = $result && $userService->delete(['id' => $item->entity_id]);
                        } catch (EntityNotFoundException $e) {
                            Yii::warning('User was not found');
                        }

                        break;
                    case 'country':
                        /** @var CountryService $countryService */
                        $countryService = Yii::$container->get(CountryService::class);
                        $result = $result && $countryService->delete(['id' => $item->entity_id]);
                        break;
                    case 'region':
                        /** @var RegionService $countryService */
                        $regionService = Yii::$container->get(RegionService::class);
                        $result = $result && $regionService->delete(['id' => $item->entity_id]);
                        break;
                    case 'city':
                        /** @var CityService $cityService */
                        $cityService = Yii::$container->get(CityService::class);
                        $result = $result && $cityService->delete(['id' => $item->entity_id]);
                        break;
                }

                $result = $result && $item->delete();
            }
        }

        return $result;
    }
}
