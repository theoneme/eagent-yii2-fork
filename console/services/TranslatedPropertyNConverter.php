<?php

namespace console\services;

use common\helpers\FileHelper;
use common\mappers\TranslationsMapper;
use common\models\Property;
use common\models\PropertyAttribute;
use common\models\Translation;
use common\services\entities\PropertyService;
use common\services\YandexTranslationService;
use console\interfaces\ConverterInterface;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\UnsetArrayValue;
use yii\helpers\VarDumper;

/**
 * Class TranslatedPropertyConverter
 * @package console\services
 */
class TranslatedPropertyNConverter implements ConverterInterface
{
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var YandexTranslationService
     */
    private $_translationService;
    /**
     * @var Service
     */
    private $_s3;

    /**
     * TranslatedPropertyConverter constructor.
     * @param PropertyService $propertyService
     * @param YandexTranslationService $translationService
     * @param Service $s3
     */
    public function __construct(PropertyService $propertyService, YandexTranslationService $translationService, Service $s3)
    {
        $this->_propertyService = $propertyService;
        $this->_translationService = $translationService;
        $this->_s3 = $s3;
    }

    /**
     * @param $rawData
     * @return Property|null
     */
    public function convertObject($rawData)
    {
        if (empty($rawData['locale']) || !array_key_exists($rawData['locale'], Yii::$app->params['supportedLocales']) || empty($rawData['property']) || !($rawData['property'] instanceof Property)) {
            return null;
        }
        /* @var Property $property*/
        $property = $rawData['property'];
        $locale = $rawData['locale'];

        $supportedLocales = Yii::$app->params['supportedLocales'];
        $supportedLocales['ua-UA'] = 'uk';

        // Копируем аттрибуты из старой проперти и связанных моделей
        $input = ArrayHelper::merge($property->attributes, [
            'id' => new UnsetArrayValue,
            'created_at' => new UnsetArrayValue,
            'updated_at' => new UnsetArrayValue,
            'slug' => new UnsetArrayValue,
            'locale' => $locale
        ]);
        $translations = TranslationsMapper::getMappedData($property->translations);
        ArrayHelper::remove($translations, 'title');

        $input['PropertyAttribute'] = ArrayHelper::map($property->propertyAttributes,
            function ($var) use ($locale){
                /* @var PropertyAttribute $var*/
                return $var->attr->id;
            },
            function ($var) use ($locale){
                /* @var PropertyAttribute $var*/
                $attrTranslations = TranslationsMapper::getMappedData($var->attributeValue->translations, TranslationsMapper::MODE_FULL);
                $attrTranslation = array_key_exists($locale, $attrTranslations)
                    ? $attrTranslations[$locale]
                    : array_pop($attrTranslations);
                return $attrTranslation['title'] ?? null;
            }
        );

        // Прогоняем поля, которые нужно перевести, через переводчик
        $translationErrors = 0;
        foreach ($translations as $key => $value) {
            $translated = $this->_translationService->translate($value, $supportedLocales[$property->locale], $supportedLocales[$locale]);
            if ($translated) {
                $data['Translation'][] = [
                    'locale' => $locale,
                    'key' => $key,
                    'value' => $translated,
                    'entity' => Translation::ENTITY_PROPERTY
                ];
            } else {
                $translationErrors++;
            }
        }

        if ($translationErrors > 3) {// Если слишком много ошибок перевода - не сохраняем
            Yii::error($translationErrors . " ошибок при переводе, отмена сохранения \n" . VarDumper::export($input));
        } else {
            // Создаем временную папку для картинок
            $tempDir = FileHelper::createTempDirectory('@frontend');
            $path = $tempDir['relativePath'];
            $dir = $tempDir['basePath'];
            // Создаем копии картинок
            foreach ($property->attachments as $id => $attachment) {
                $pathInfo = pathinfo($attachment->content);
                $newName = hash('crc32b', uniqid($pathInfo['filename'], true)) . '.' . $pathInfo['extension'];
                if (file_exists(Yii::getAlias('@frontend') . '/web' . $attachment->content)) {
                    copy(Yii::getAlias('@frontend') . '/web' . $attachment->content, $dir . $newName);
                    $input['Attachment'][] = $path . $newName;
                } else {
                    if ($this->_s3->exist(Yii::$app->mediaLayer->fixMediaPath($attachment->content))) {
                        $fullUrl = Yii::$app->mediaLayer->tryLoadFromAws($attachment->content);
                        $file = file_get_contents($fullUrl);
                        if ($file) {
                            file_put_contents($dir . $newName, $file);
                            $input['Attachment'][] = $path . $newName;
                        }
                    }
                }
            }

            // Сохраняем проперти
            try {
                return $this->_propertyService->create($input);
            } catch (\Exception $e) {
                Yii::error("Исключение при сохранении проперти \n" . $e->getTraceAsString());
                return null;
            }
        }

        return null;
    }
}
