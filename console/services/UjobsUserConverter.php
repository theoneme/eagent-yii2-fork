<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 13:46
 */

namespace console\services;

use common\forms\ar\UserForm;
use common\helpers\FileHelper;
use common\models\Translation;
use common\models\User;
use console\interfaces\ConverterInterface;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\base\Exception;

/**
 * Class UjobsUserConverter
 * @package console\services
 */
class UjobsUserConverter implements ConverterInterface
{
    /**
     * @var Service
     */
    private $_s3;

    /**
     * UjobsUserConverter constructor.
     * @param Service $s3
     */
    public function __construct(Service $s3)
    {
        $this->_s3 = $s3;
    }

    /**
     * @param $rawData
     * @return mixed|null
     * @throws Exception
     */
    public function convertObject($rawData)
    {
        if (!empty($rawData['profile']['translations'])) {
            $translations = array_intersect_key($rawData['profile']['translations'], Yii::$app->params['supportedLocales']);
            $translation = $translations['ru-RU'] ?? array_pop($translations);
        }
        $locale = $translation['locale'] ?? 'ru-RU';
        if (!empty($translation['title'])) {
            $rawData['UserForm']['username'] = $translation['title'];
        } else if (!empty($rawData['profile']['name'])) {
            $rawData['UserForm']['username'] = $rawData['profile']['name'];
        }
        $data = [
            'UserForm' => [
                'username' => !empty($rawData['UserForm']['username']) ? $rawData['UserForm']['username'] : 'user' . uniqid(),
                'email' => !empty($rawData['email']) ? $rawData['email'] : null,
                'phone' => !empty($rawData['phone']) ? $rawData['phone'] : null,
                'type' => $rawData['type'] ?? User::TYPE_DEFAULT,
                'password_hash' => $rawData['password_hash'],
            ],
            'GeoForm' => [
                'lat' => !empty($rawData['profile']['lat']) ? $rawData['profile']['lat'] : null,
                'lng' => !empty($rawData['profile']['long']) ? $rawData['profile']['long'] : null,
            ]
        ];
        if (!empty($translation['content'])) {
            $data['MetaForm']['description'] = $translation['content'];
        }
        if (!empty($rawData['userAttributes'])) {
            $specialties = array_reduce($rawData['userAttributes'], function ($carry, $var) use ($locale) {
                if ($var['entity_alias'] === 'specialty') {
                    $attrDescription = $var['attrValueDescriptions'][$locale] ?? $var['attrValueDescriptions'][$locale] ?? null;
                    if ($attrDescription) {
                        $carry[] = $attrDescription['title'];
                    }
                }
                return $carry;
            }, []);
            if (!empty($specialties)) {
                $data['MetaForm']['subtitle'] = Yii::t('agent', 'Specialties: {specialties}', ['specialties' => implode(', ', $specialties)], $locale);
            }
        }

        if (!empty($rawData['profile']['gravatar_email']) && !preg_match('/^http(s)?:\/\/.*/', $rawData['profile']['gravatar_email'])) {
            $pathInfo = FileHelper::createTempDirectory('@frontend');
            $newAbsolutePath = $pathInfo['basePath'] . basename($rawData['profile']['gravatar_email']);
            $newRelativePath = $pathInfo['relativePath'] . basename($rawData['profile']['gravatar_email']);
            try {
                $this->_s3->commands()
                    ->get(Yii::$app->mediaLayer->fixMediaPath($rawData['profile']['gravatar_email']))
                    ->saveAs($newAbsolutePath)->execute();
                $data['UserForm']['avatar'] = $newRelativePath;

            } catch (Exception $e) {
                Yii::error('Fetching avatar failed');
                return null;
            }
        }

        return $data;
    }
}
