<?php

namespace console\services;

use common\helpers\FileHelper;
use common\helpers\UtilityHelper;
use common\models\Building;
use common\models\BuildingPhase;
use common\models\Category;
use common\models\Property;
use common\models\Translation;
use common\services\entities\AttributeService;
use common\services\GoogleMapsService;
use console\interfaces\ConverterInterface;

/**
 * Class FeedPropertyConverter
 * @package console\services
 */
class FeedPropertyConverter implements ConverterInterface
{
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var
     */
    private $_googleMapsService;

    /**
     * @var array
     */
    private $_sourceToUser = [
        'rtr72' => 7152,
        'zagdom' => 7067,
        '21online' => 7230,
        'ssk' => 16976,
    ];

    /**
     * @var array
     */
    private $_fieldToAttribute = [
        'area' => 'property_area',
        'living-space' => 'living_area',
        'kitchen-space' => 'kitchen_area',
//        'lot-area' => 'property_area',
        'quality' => 'condition',
        'room-furniture' => 'is_furnished',
        'rooms' => 'rooms',
        'floor' => 'floor',
        'floors-total' => 'floors',
        'bathroom-unit' => 'bathroom_type',
        'building-type' => 'material',
        'built-year' => 'year_built',
        'with-pets' => 'pets_allowed'
    ];

    /**
     * @var array
     */
    private $_attributeToFields = [
        'property_amenitie' => [
            'internet',
            'air-conditioner',
            'television',
            'washing-machine',
            'dishwasher',
            'refrigerator',
            'built-in-tech',
        ],
        'building_amenitie' => [
            'lift',
            'rubbish-chute',
            'electricity-supply',
            'water-supply',
            'gas-supply',
            'sewerage-supply',
            'heating-supply',
            'toilet',
            'shower',
            'pool',
            'billiard',
            'sauna',
            'parking',
            'flat-alarm',
            'security',
        ],
        'sale_type' => [
            'haggle',
            'mortgage',
            'prepayment'
        ]
    ];
    
    private $_amenityDefinitions = [
        'internet' => 'Интернет',
        'air-conditioner' => 'Кондиционер',
        'television' =>  'Телевизор',
        'washing-machine' =>  'Стиральная машина',
        'dishwasher' =>  'Посудомоечная машина',
        'refrigerator' =>  'Холодильник',
        'built-in-tech' =>  'Встроенная техника',
        'lift' =>  'Лифт',
        'rubbish-chute' =>  'Мусоропровод',
        'electricity-supply' =>  'Электричество',
        'water-supply' =>  'Водопровод',
        'gas-supply' =>  'Газ',
        'sewerage-supply' =>  'Канализация',
        'heating-supply' =>  'Отопление',
        'toilet' =>  'Туалет',
        'shower' =>  'Душ',
        'pool' =>  'Бассейн',
        'billiard' =>  'Бильярд',
        'sauna' =>  'Сауна',
        'parking' =>  'Охраняемая парковка',
        'haggle' =>  'Торг',
        'mortgage' =>  'Ипотека',
        'prepayment' =>  'Внесен задаток',
    ];

    /**
     * @var array
     */
    private $_typeToType = [
        'продажа' => Property::TYPE_SALE,
        'аренда' => Property::TYPE_RENT,
    ];

    /**
     * @var array
     */
    private $_categoryToCategory = [
        'дача' => 'houses',
        'коттедж' => 'houses',
        'cottage' => 'houses',
        'дом' => 'houses',
        'house' => 'houses',
        'дом с участком' => 'houses',
        'house with lot' => 'houses',
        'часть дома' => 'houses',
        'дуплекс' => 'houses',
        'duplex' => 'houses',
        'таунхаус' => 'houses',
        'участок' => 'land',
        'lot' => 'land',
        'квартира' => 'flats',
        'гараж' => 'commercial-property',
        'garage' => 'commercial-property',
        'коммерческая' => 'commercial-property',
        'commercial' => 'commercial-property',
    ];

    /**
     * @var array
     */
    private $_fieldHandlers;

    /**
     * N1PropertyConverter constructor.
     * @param AttributeService $attributeService
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(AttributeService $attributeService, GoogleMapsService $googleMapsService)
    {
        $this->_attributeService = $attributeService;
        $this->_googleMapsService = $googleMapsService;
        $this->_fieldHandlers = [
            'area' => 'areaHandler',
            'living-space' => 'areaHandler',
            'kitchen-space' => 'areaHandler',
            'lot-area' => 'areaHandler',
            'haggle' => 'booleanHandler',
            'mortgage' => 'booleanHandler',
            'prepayment' => 'booleanHandler',
            'with-pets' => 'booleanHandler',
            'room-furniture' => 'booleanHandler',
            'internet' => 'booleanHandler',
            'air-conditioner' => 'booleanHandler',
            'television' => 'booleanHandler',
            'washing-machine' => 'booleanHandler',
            'dishwasher' => 'booleanHandler',
            'refrigerator' => 'booleanHandler',
            'built-in-tech' => 'booleanHandler',
            'lift' => 'booleanHandler',
            'rubbish-chute' => 'booleanHandler',
            'electricity-supply' => 'booleanHandler',
            'water-supply' => 'booleanHandler',
            'gas-supply' => 'booleanHandler',
            'sewerage-supply' => 'booleanHandler',
            'heating-supply' => 'booleanHandler',
            'toilet' => 'booleanHandler',
            'shower' => 'booleanHandler',
            'pool' => 'booleanHandler',
            'billiard' => 'booleanHandler',
            'sauna' => 'booleanHandler',
            'parking' => 'booleanHandler',
        ];
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        if (!isset($rawData['type'], $rawData['category'], $rawData['source']) || empty($rawData['type']) || empty($rawData['category']) || empty($rawData['source'])) {
            return null;
        }
        $type = mb_strtolower($rawData['type']);
        $category = mb_strtolower($rawData['category']);
        if (!isset($this->_typeToType[$type], $this->_categoryToCategory[$category], $this->_sourceToUser[$rawData['source']])) {
            return null;
        }
        $categoryId = Category::find()->joinWith(['translations'])->where(['value' => $this->_categoryToCategory[$category], 'key' => Translation::KEY_SLUG])->select('category.id')->scalar();
        $locale = 'ru-RU';

        if (isset($rawData['location']['latitude'], $rawData['location']['longitude'])) {
            $lat = $rawData['location']['latitude'];
            $lng = $rawData['location']['longitude'];
        } else {
            $addressParts = [
                $rawData['location']['address'] ?? '',
                $rawData['location']['locality-name'] ?? '',
                $rawData['location']['region'] ?? '',
                $rawData['location']['country'] ?? '',
            ];

            $latLong = $this->_googleMapsService->geocode(implode(', ', $addressParts));
            $lat = $latLong['lat'] ?? null;
            $lng = $latLong['long'] ?? null;
        }
        $data = [
            'url' => $rawData['url'] ?? $rawData['@attributes']['internal-id'] ?? null,
            'PropertyForm' => [
                'user_id' => $this->_sourceToUser[$rawData['source']],
                'status' => Property::STATUS_ACTIVE,
                'type' => $this->_typeToType[$type],
                'locale' => $locale,
                'slug' => UtilityHelper::generateSlug($rawData['title'] ?? ''),
            ],
            'CategoryForm' => [
                'category_id' => $categoryId,
            ],
            'GeoForm' => [
                'lat' => $lat ?? null,
                'lng' => $lng ?? null,
            ],
            'PriceForm' => [
                'price' => (int)preg_replace('/[^0-9\.]/', '', $rawData['price']['value']),
                'currency_code' => $rawData['price']['currency'] === 'RUR' ? 'RUB' : $rawData['price']['currency'],
            ],
        ];

        if (!empty($rawData['description'])) {
            $data['MetaForm'][$locale]['description'] = $rawData['description'];
        }

        foreach ($this->_fieldToAttribute as $field => $attribute) {
            if (isset($rawData[$field])) {
                $attributeObject = $this->_attributeService->getOne(['alias' => $attribute]);

                $data['DynamicForm']["at_{$attributeObject['id']}"] = $this->handleHandlers($field, $rawData[$field]);
            }
        }

        foreach ($this->_attributeToFields as $key => $group) {
            $attribute = $this->_attributeService->getOne(['alias' => $key]);
            foreach ($group as $groupValue) {
                if (array_key_exists($groupValue, $rawData)) {
                    $result = $this->handleHandlers($groupValue, $rawData[$groupValue]);
                    if($result === true) {
                        $data['DynamicForm']["at_{$attribute['id']}"][] = $this->_amenityDefinitions[$groupValue];
                    }
                }
            }
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        if (isset($rawData['image'])) {
            if (!is_array($rawData['image'])) {
                $rawData['image'] = [$rawData['image']];
            }
            foreach ($rawData['image'] as $image) {
                $newAbsolutePath = $pathInfo['basePath'] . basename($image);
                $newRelativePath = $pathInfo['relativePath'] . basename($image);
                $headers = get_headers($image);
                if (strpos($headers[0], '200') !== false) {
                    $file = file_get_contents($image);
                    if ($file) {
                        file_put_contents($newAbsolutePath, $file);
                        $data['AttachmentForm'][]['content'] = $newRelativePath;
                    }
                }
            }
        }

        // Building
        if (!empty($rawData['building-name'])) {
            $type = !empty($rawData['new-flat']) && $this->booleanHandler($rawData['new-flat']) ? Building::TYPE_NEW_CONSTRUCTION : Building::TYPE_DEFAULT;
            $data['buildingData'] = [
                'BuildingForm' => [
                    'status' => Building::STATUS_ACTIVE,
                    'type' => $type,
                    'locale' => $locale,
                    'user_id' => $this->_sourceToUser[$rawData['source']]
                ],
                'GeoForm' => [
                    'lat' => $lat ?? null,
                    'lng' => $lng ?? null,
                ],
                'BuildingMetaForm' => [
                    $locale => [
                        Translation::KEY_NAME => $rawData['building-name'],
                    ]
                ]
            ];
            if (!empty($rawData['built-year']) && !empty($rawData['ready-quarter']) && !empty($rawData['building-state'])) {
                $data['buildingData']['BuildingPhaseForm'][] = [
                    'year' => (int)$rawData['built-year'],
                    'quarter' => (int)$rawData['ready-quarter'],
                    'status' => $rawData['building-state'] === 'unfinished' ? BuildingPhase::STATUS_UNFINISHED : BuildingPhase::STATUS_FINISHED,
                ];
            }
        }

        return $data;
    }

    /**
     * @param $field
     * @param $rawValue
     * @return mixed
     */
    protected function handleHandlers($field, $rawValue)
    {
        if(isset($this->_fieldHandlers[$field])) {
            $handler = $this->_fieldHandlers[$field];
            if(is_callable($handler)) {
                $value = call_user_func($this->_fieldHandlers[$field], $rawValue);
            } else {
                $value = $this->{$this->_fieldHandlers[$field]}($rawValue);
            }
        } else {
            $value = $rawValue;
        }

        return $value;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function booleanHandler($value)
    {
        return in_array($value, ['+', 'да', 'true', '1']);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function areaHandler($value)
    {
        switch ($value['unit']) {
            case 'cотка':
                $multiplier = 100;
                break;
            case 'гектар':
            case 'hectare':
                $multiplier = 1000;
                break;
            default:
                $multiplier = 1;
                break;
        }
        return (float)$value['value'] * $multiplier;
    }
}
