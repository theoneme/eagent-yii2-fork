<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 14:42
 */

namespace console\services;

use common\helpers\UtilityHelper;
use common\models\Translation;
use common\services\entities\TranslationService;
use common\services\YandexTranslationService;
use Yii;

/**
 * Class LocationTranslationService
 * @package console\services
 */
class YandexLocationTranslationService
{
    /**
     * @var TranslationService
     */
    private $_translationService;
    /**
     * @var YandexTranslationService
     */
    private $_yandexTranslationService;

    /**
     * @param TranslationService $translationService
     * @param YandexTranslationService $yandexTranslationService
     */
    public function __construct(TranslationService $translationService, YandexTranslationService $yandexTranslationService)
    {
        $this->_translationService = $translationService;
        $this->_yandexTranslationService = $yandexTranslationService;
    }

    /**
     * @param $locationItem
     * @param $entity
     * @param $from
     * @param $to
     * @return bool
     */
    public function translate($item, $from, $to)
    {
        $success = true;
        $sourceLocale = Yii::$app->params['supportedLocales'][$from] ?? null;
        $targetLocale = Yii::$app->params['supportedLocales'][$to];

        $newTitle = $this->_yandexTranslationService->translate($item['title'], $sourceLocale, $targetLocale);

        $translationData = [
            'locale' => $to,
            'key' => Translation::KEY_TITLE,
            'value' => $newTitle,
            'entity' => $item['entity'],
            'entity_id' => $item['id']
        ];
        $success = $success && ($this->_translationService->create($translationData) !== null);
//
//        $slug = UtilityHelper::transliterate($newTitle);
//        $translationData = [
//            'locale' => $to,
//            'key' => Translation::KEY_SLUG,
//            'value' => !empty($slug) ? $slug : $item['baseSlug'],
//            'entity' => $item['entity'],
//            'entity_id' => $item['id']
//        ];
//        $success = $success && ($this->_translationService->create($translationData) !== null);

        return $success;
    }
}
