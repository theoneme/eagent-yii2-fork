<?php

namespace console\services;

use common\helpers\FileHelper;
use common\helpers\UtilityHelper;
use common\models\Category;
use common\models\Property;
use common\models\Translation;
use common\services\entities\AttributeService;
use common\services\GoogleMapsService;
use console\interfaces\ConverterInterface;

/**
 * Class N1PropertyConverter
 * @package console\services
 */
class N1PropertyConverter implements ConverterInterface
{
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var
     */
    private $_googleMapsService;

    /**
     * @var array
     */
    private $_fieldToAttribute = [
        'Общая площадь' => 'property_area',
        'Жилая площадь' => 'living_area',
        'Кухня' => 'kitchen_area',
        'Планировка' => 'layout',
        'Санузел' => 'bathroom_type',
        'Состояние' => 'condition',
        'Количество балконов' => 'balconies',
        'Тип собственности' => 'property_type',
        'Количество лоджий' => 'loggias',
        'Интернет' => 'internet',
        'newBuilding' => 'new_building',
    ];

    /**
     * @var array
     */
    private $_typeToType = [
        'kupit' => Property::TYPE_SALE,
        'snyat' => Property::TYPE_RENT,
    ];

    /**
     * @var array
     */
    private $_categoryToCategory = [
        'kvartiry' => 'flats',
        'doma' => 'houses',
        'kommercheskaya' => 'commercial-property',
        'dachi' => 'houses-dachi',
        'zemplya' => 'land',
    ];

    /**
     * @var array
     */
    private $_fieldHandlers;

    /**
     * N1PropertyConverter constructor.
     * @param AttributeService $attributeService
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(AttributeService $attributeService, GoogleMapsService $googleMapsService)
    {
        $this->_attributeService = $attributeService;
        $this->_googleMapsService = $googleMapsService;
        $this->_fieldHandlers = [
            'Общая площадь' => function ($var) {
                return (float)str_replace(',', '.', $var);
            },
            'Жилая площадь' => function ($var) {
                return (float)str_replace(',', '.', $var);
            },
            'Кухня' => function ($var) {
                return (float)str_replace(',', '.', $var);
            },
            'Интернет' => function ($var) {
                return $var === 'есть';
            },
        ];
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        if (!isset($rawData['type'], $rawData['category']) || !isset($this->_typeToType[$rawData['type']], $this->_categoryToCategory[$rawData['category']])) {
            return null;
        }
        $categoryId = Category::find()->joinWith(['translations'])->where(['value' => $this->_categoryToCategory[$rawData['category']], 'key' => Translation::KEY_SLUG])->select('category.id')->scalar();

        $address = $rawData['city'] ?? 'Екатеринбург';
        if (!empty($rawData['title'])) {
            $titleParts = explode(',', $rawData['title'], 2);
            if (count($titleParts) === 2) {
                $address .= ', ' . $titleParts[1];
            }
        }
        if (!empty($address)) {
            $latLong = $this->_googleMapsService->geocode($address);
        }
        $data = [
            'PropertyForm' => [
                'user_id' => $rawData['user_id'],
                'status' => Property::STATUS_ACTIVE,
                'type' => $this->_typeToType[$rawData['type']],
                'locale' => 'ru-RU',
                'slug' => UtilityHelper::generateSlug($rawData['title'] ?? ''),
            ],
            'CategoryForm' => [
                'category_id' => $categoryId,
            ],
            'MetaForm' => [
                'ru-RU' => [
                    'title' => $rawData['title'] ?? null,
                    'description' => $rawData['description'] ?? null,
                ]
            ],
            'GeoForm' => [
                'lat' => $latLong['lat'] ?? null,
                'lng' => $latLong['long'] ?? null,
                'address' => $address ?? '',
            ],
            'PriceForm' => [
                'price' => (int)preg_replace('/[^0-9]/', '', $rawData['price']),
                'currency_code' => 'RUB',
            ]
        ];
        if (empty($data['GeoForm']['lat']) || empty($data['GeoForm']['lng'])) {
            return $data;
        }


        if (preg_match('/^(\d)-к.*/', $rawData['title'] ?? '')) {
            $attribute = $this->_attributeService->getOne(['alias' => 'rooms']);
            $data['DynamicForm']["at_{$attribute['id']}"] = preg_replace('/^(\d)-к.*/', '$1', $rawData['title']);
        }
        if (isset($rawData['flatData'])) {
            foreach ($rawData['flatData'] as $key => $value) {
                if (isset($this->_fieldToAttribute[$key])) {
                    $attribute = $this->_attributeService->getOne(['alias' => $this->_fieldToAttribute[$key]]);
                    $data['DynamicForm']["at_{$attribute['id']}"] = isset($this->_fieldHandlers[$key]) ? call_user_func($this->_fieldHandlers[$key], $value) : $value;
                }
            }
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        if (isset($rawData['imagesData'])) {
            foreach ($rawData['imagesData'] as $image) {
                if (!preg_match("#.*\/static\/img\/Public\/logo\.png$#", $image)) {
                    $newAbsolutePath = $pathInfo['basePath'] . basename($image);
                    $newRelativePath = $pathInfo['relativePath'] . basename($image);
                    $headers = get_headers($image);
                    if (strpos($headers[0], '200') !== false) {
                        $file = file_get_contents($image);
                        if ($file) {
                            file_put_contents($newAbsolutePath, $file);
                            $data['AttachmentForm'][]['content'] = $newRelativePath;
                        }
                    }
                }
            }
        }

        return $data;
    }
}
