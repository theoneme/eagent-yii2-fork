<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 13:46
 */

namespace console\services;

use common\helpers\FileHelper;
use common\models\Property;
use common\models\Request;
use common\models\Translation;
use common\services\entities\RequestService;
use console\interfaces\ConverterInterface;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\base\Exception;

/**
 * Class UjobsRequestConverter
 * @package console\services
 */
class UjobsRequestConverter implements ConverterInterface
{
    /**
     * @var array
     */
    private $_categoriesMapping;
    /**
     * @var Service
     */
    private $_s3;
    /**
     * @var RequestService
     */
    private $_requestService;

    /**
     * UjobsRequestConverter constructor.
     * @param array $categoriesMapping
     * @param Service $s3
     * @param RequestService $requestService
     */
    public function __construct(array $categoriesMapping, Service $s3, RequestService $requestService)
    {
        $this->_categoriesMapping = $categoriesMapping;
        $this->_s3 = $s3;
        $this->_requestService = $requestService;
    }

    /**
     * @param $rawData
     * @return mixed
     * @throws Exception
     */
    public function convertObject($rawData)
    {
        $data = [
            'RequestForm' => [
                'id' => $rawData['id'],
                'locale' => $rawData['locale'],
                'slug' => $rawData['alias'],
                'type' => Request::TYPE_SALE,
                'status' => Request::STATUS_ACTIVE,
                'user_id' => $rawData['user_id'],
                'created_at' => $rawData['created_at'],
                'updated_at' => $rawData['updated_at'],
                'contract_price' => $rawData['contract_price'],
            ],
            'MetaForm' => [

            ],
            'PriceForm' => [
                'price' => $rawData['price'],
                'currency_code' => $rawData['currency_code'],

            ],
            'CategoryForm' => [
                'category_id' => $this->_categoriesMapping[$rawData['category_id']],
            ],
            'GeoForm' => [
                'lat' => !empty($rawData['lat']) ? $rawData['lat'] : '56.83892610',
                'lng' => !empty($rawData['long']) ? $rawData['long'] : '60.60570250',
            ]
        ];

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        foreach ($rawData['attachments'] as $attachment) {
            if (!empty($attachment['content']) && !preg_match('/^http(s)?:\/\/.*/', $attachment['content'])) {
                $newAbsolutePath = $pathInfo['basePath'] . basename($attachment['content']);
                $newRelativePath = $pathInfo['relativePath'] . basename($attachment['content']);

                try {
                    $this->_s3->commands()
                        ->get(Yii::$app->mediaLayer->fixMediaPath($attachment['content']))
                        ->saveAs($newAbsolutePath)->execute();
                    $data['AttachmentForm'][]['content'] = $newRelativePath;

                } catch (Exception $e) {

                }
            }
        }
        if (!empty($rawData['translation'])) {
            $data['MetaForm']['title'] = $rawData['translation']['title'];
            $data['MetaForm']['description'] = $rawData['translation']['content'];
        }

        return $this->_requestService->create($data);
    }
}
