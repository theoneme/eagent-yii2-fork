<?php

namespace console\services;

use common\helpers\FileHelper;
use common\models\Category;
use common\models\Property;
use common\models\Translation;
use common\services\entities\AttributeService;
use common\services\GoogleMapsService;
use console\interfaces\ConverterInterface;

/**
 * Class YandexPropertyConverter
 * @package console\services
 */
class YandexPropertyConverter implements ConverterInterface
{
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var
     */
    private $_googleMapsService;

    /**
     * @var array
     */
    private $_fieldToAttribute = [
        "roomsTotal" => 'rooms',
        "floorsTotal" => 'floors',
        "floorsOffered" => 'floor',
        "area" => 'property_area',
        "livingSpace" => 'living_area',
        "kitchenSpace" => 'kitchen_area',
    ];

    /**
     * @var array
     */
    private $_fieldHandlers;

    /**
     * N1PropertyConverter constructor.
     * @param AttributeService $attributeService
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(AttributeService $attributeService, GoogleMapsService $googleMapsService)
    {
        $this->_attributeService = $attributeService;
        $this->_googleMapsService = $googleMapsService;
        $this->_fieldHandlers = [
            'roomsTotal' => 'integerHandler',
            'floorsTotal' => 'integerHandler',
            'floorsOffered' => function($var) {
                return !empty($var) && is_array(!empty($var)) ? array_values($var)[0] : null;
            },
            'area' => 'areaHandler',
            'livingSpace' => 'areaHandler',
            'kitchenSpace' => 'areaHandler',
        ];
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        $categoryId = Category::find()->joinWith(['translations'])->where(['value' => 'flats', 'key' => Translation::KEY_SLUG])->select('category.id')->scalar();

        $data = [
            'PropertyForm' => [
                'user_id' => $rawData['user_id'],
                'status' => Property::STATUS_ACTIVE,
                'type' => Property::TYPE_SALE,
                'locale' => 'ru-RU',
            ],
            'CategoryForm' => [
                'category_id' => $categoryId,
            ],
            'MetaForm' => [
                'ru-RU' => [
//                    'description' => $rawData['description'] ?? null,
                ]
            ],
            'GeoForm' => [
                'lat' => $rawData['lat'] ?? null,
                'lng' => $rawData['lng'] ?? null,
            ],
            'PriceForm' => [
                'price' => (int)preg_replace('/[^0-9\.]/', '', $rawData['price']['value']),
                'currency_code' => $rawData['price']['currency'] === 'RUR' ? 'RUB' : $rawData['price']['currency'],
            ]
        ];

        foreach ($this->_fieldToAttribute as $field => $attribute) {
            if (isset($rawData[$field])) {
                $attributeObject = $this->_attributeService->getOne(['alias' => $attribute]);
                $value = $this->handleHandlers($field, $rawData[$field]);
                if ($value !== null) {
                    $data['DynamicForm']["at_{$attributeObject['id']}"] = $value;
                }
            }
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        if (isset($rawData['imagesData'])) {
            $imagesUploaded = 0;
            foreach ($rawData['imagesData'] as $image) {
                $imagePath = FileHelper::downloadFile($image, $pathInfo);
                if ($imagePath) {
                    $data['AttachmentForm'][]['content'] = $imagePath;
                    $imagesUploaded++;
                }
                if ($imagesUploaded >= 10) {
                    break;
                }
            }
        }

        return $data;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function areaHandler($value)
    {
        $pattern = "/^([\d\.]+)([^\d\.].*)?/";
        return !empty($value['value']) && preg_match($pattern, $value['value']) ? (float)preg_replace($pattern, '$1', $value['value']) : null;
    }

    /**
     * @param $value
     * @return bool
     */
    protected function integerHandler($value)
    {
        $pattern = "/^([\d]+)([^\d].*)?/";
        return preg_match($pattern, $value) ? (int)preg_replace($pattern, '$1', $value) : null;
    }

    /**
     * @param $field
     * @param $rawValue
     * @return mixed
     */
    protected function handleHandlers($field, $rawValue)
    {
        if(isset($this->_fieldHandlers[$field])) {
            $handler = $this->_fieldHandlers[$field];
            if(is_callable($handler)) {
                $value = call_user_func($this->_fieldHandlers[$field], $rawValue);
            } else {
                $value = $this->{$this->_fieldHandlers[$field]}($rawValue);
            }
        } else {
            $value = $rawValue;
        }

        return $value;
    }
}
