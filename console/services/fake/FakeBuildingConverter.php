<?php

namespace console\services\fake;

use common\helpers\FileHelper;
use common\models\Building;
use common\models\BuildingDocument;
use common\models\BuildingPhase;
use common\models\BuildingSite;
use common\models\Translation;
use common\services\entities\AttributeService;
use console\interfaces\ConverterInterface;
use Faker\Factory;
use Yii;
use yii\base\Exception;

/**
 * Class FakeBuildingConverter
 * @package console\services\fake
 */
class FakeBuildingConverter implements ConverterInterface
{

    /**
     * @var AttributeService
     */
    private $_attributeService;

    /**
     * @var array
     */
    private $_types = [
        Building::TYPE_DEFAULT,
        Building::TYPE_NEW_CONSTRUCTION,
    ];

    /**
     * @var array
     */
    private $_documentTypes = [
        BuildingDocument::TYPE_PERMISSION,
        BuildingDocument::TYPE_PROJECT,
        BuildingDocument::TYPE_OPERATION_ACT,
    ];

    /**
     * @var array
     */
    private $_phaseStatuses = [
        BuildingPhase::STATUS_UNFINISHED,
        BuildingPhase::STATUS_FINISHED,
    ];

    /**
     * @var array
     */
    private $_siteTypes = [
        BuildingSite::TYPE_METRO,
        BuildingSite::TYPE_SCHOOL,
        BuildingSite::TYPE_PARK,
        BuildingSite::TYPE_POND,
        BuildingSite::TYPE_AIRPORT,
        BuildingSite::TYPE_CITY_CENTER,
    ];

    /**
     * @var array
     */
    private $_siteTransport = [
        BuildingSite::TRANSPORT_FOOT,
        BuildingSite::TRANSPORT_CAR,
        BuildingSite::TRANSPORT_TRANSPORT,
    ];

    /**
     * @var array
     */
    private $_attributes = [
        'ceiling_height',
        'floors',
        'new_construction_mortgage',
        'new_construction_installment_plan',
        'new_construction_discount',
    ];

    /**
     * FakePropertyConverter constructor.
     * @param AttributeService $attributeService
     */
    public function __construct(AttributeService $attributeService)
    {
        $this->_attributeService = $attributeService;
    }

    /**
     * @param $rawData
     * @return mixed|null
     * @throws Exception
     */
    public function convertObject($rawData)
    {
        $faker = Factory::create('ru_RU');
        $type = $this->_types[array_rand($this->_types)];
        $data = [
            'BuildingForm' => [
                'status' => Building::STATUS_ACTIVE,
                'type' => $type,
                'locale' => 'ru-RU',
                'user_id' => $rawData['user_id']
            ],
            'BuildingMetaForm' => [
                'ru-RU' => [
                    Translation::KEY_NAME => $faker->text(50),
                    Translation::KEY_DESCRIPTION => $faker->text,
                ]
            ],
            'GeoForm' => [
                'lat' => $faker->latitude,
                'lng' => $faker->longitude,
            ],
            'BuildingDocumentForm' => [],
            'BuildingProgressForm' => [],
            'BuildingPhaseForm' => [],
            'BuildingSiteForm' => [],
        ];

        foreach ($this->_attributes as $attribute) {
            $attributeObject = $this->_attributeService->getOne(['alias' => $attribute]);
            $data['DynamicForm']["at_{$attributeObject['id']}"] = $faker->numberBetween(1, 100);
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');
        $fakeFilesPath = Yii::getAlias('@console') . '/import/fake/';

        // Документы
        $documentsAmount = $faker->numberBetween(0, 3);
        for ($i = 0; $i < $documentsAmount; $i++) {
            $docType = $this->_documentTypes[array_rand($this->_documentTypes)];
            $fileName = $faker->file($fakeFilesPath, $pathInfo['basePath'], false);
            $data['BuildingDocumentForm'][] = [
                'type' => $docType,
                'file' => $pathInfo['relativePath'] . $fileName,
                'name' => $fileName
            ];
        }

        // Прогресс строительства
        $progressImagesAmount = $faker->numberBetween(1, 3);
        $progressAttachments = [];
        for ($i = 0; $i < $progressImagesAmount; $i++) {
            $imageName = $faker->image($pathInfo['basePath'], 320, 320, null, false);
            $progressAttachments[]['content'] = $pathInfo['relativePath'] . $imageName;
        }
        $data['BuildingProgressForm'][] = [
            'year' => $faker->year,
            'quarter' => $faker->numberBetween(1, 4),
            'BuildingProgressAttachmentForm' => $progressAttachments
        ];

        // Очереди
        $phasesAmount = $faker->numberBetween(0, 3);
        for ($i = 0; $i < $phasesAmount; $i++) {
            $phaseStatus = $this->_phaseStatuses[array_rand($this->_phaseStatuses)];
            $data['BuildingPhaseForm'][] = [
                'year' => $faker->year,
                'quarter' => $faker->numberBetween(1, 4),
                'status' => $phaseStatus,
            ];
        }

        //Места
        $sitesAmount = $faker->numberBetween(1, 10);
        for ($i = 0; $i < $sitesAmount; $i++) {
            $siteType = $this->_siteTypes[array_rand($this->_siteTypes)];
            $siteTransport = $this->_siteTransport[array_rand($this->_siteTransport)];

            $data['BuildingSiteForm'][] = [
                'type' => $siteType,
                'transport' => $siteTransport,
                'name' => $faker->streetName,
                'time' => $faker->numberBetween(1, 50),
                'distance' => $siteType === BuildingSite::TRANSPORT_FOOT ? $faker->numberBetween(100, 1000) : $faker->numberBetween(5000, 50000),
            ];
        }

        // Картинки
        $imagesAmount = $faker->numberBetween(1, 8);
        for ($i = 0; $i < $imagesAmount; $i++) {
            $imageName = $faker->image($pathInfo['basePath'], 320, 320, null, false);
            $data['BuildingAttachmentForm'][]['content'] = $pathInfo['relativePath'] . $imageName;
        }

        return $data;
    }
}
