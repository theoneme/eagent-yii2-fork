<?php

namespace console\services\fake;

use common\helpers\FileHelper;
use common\models\Property;
use common\services\entities\AttributeService;
use console\interfaces\ConverterInterface;
use Faker\Factory;
use yii\base\Exception;

/**
 * Class FakePropertyConverter
 * @package console\services\fake
 */
class FakePropertyConverter implements ConverterInterface
{

    /**
     * @var AttributeService
     */
    private $_attributeService;

    /**
     * @var array
     */
    private $_types = [
        Property::TYPE_SALE,
        Property::TYPE_RENT,
    ];

    /**
     * @var array
     */
    private $_attributes = [
        'property_area',
        'living_area',
        'kitchen_area',
        'balconies',
        'floor',
        'floors',
    ];

    /**
     * FakePropertyConverter constructor.
     * @param AttributeService $attributeService
     */
    public function __construct(AttributeService $attributeService)
    {
        $this->_attributeService = $attributeService;
    }

    /**
     * @param $rawData
     * @return mixed|null
     * @throws Exception
     */
    public function convertObject($rawData)
    {
        $faker = Factory::create('ru_RU');
        $type = $this->_types[array_rand($this->_types)];
        $data = [
            'PropertyForm' => [
                'user_id' => $rawData['user_id'],
                'status' => Property::STATUS_ACTIVE,
                'type' => $type,
                'locale' => 'ru-RU',
//                'slug' => UtilityHelper::generateSlug($rawData['title'] ?? ''),
            ],
            'CategoryForm' => [
                'category_id' => $rawData['category_id'],
            ],
            'MetaForm' => [
                'ru-RU' => [
//                    'title' => $rawData['title'] ?? null,
                    'description' => $faker->text,
                ]
            ],
            'GeoForm' => [
                'lat' => $faker->latitude,
                'lng' => $faker->longitude,
            ],
            'PriceForm' => [
                'price' => $type === Property::TYPE_SALE ? $faker->numberBetween(1000000, 100000000) : $faker->numberBetween(5000, 100000),
                'currency_code' => 'RUB',
            ]
        ];

        foreach ($this->_attributes as $attribute) {
            $attributeObject = $this->_attributeService->getOne(['alias' => $attribute]);
            $data['DynamicForm']["at_{$attributeObject['id']}"] = $faker->numberBetween(1, 100);
        }


        $pathInfo = FileHelper::createTempDirectory('@frontend');
        $imagesAmount = $faker->numberBetween(1, 8);
        for ($i = 0; $i < $imagesAmount; $i++) {
            $imageName = $faker->image($pathInfo['basePath'], 320, 320, null, false);
            $data['AttachmentForm'][]['content'] = $pathInfo['relativePath'] . $imageName;
        }

        return $data;
    }
}
