<?php

namespace console\services\fake;

use common\helpers\FileHelper;
use common\models\User;
use console\interfaces\ConverterInterface;
use Faker\Factory;
use Yii;
use yii\base\Exception;

/**
 * Class FakeUserConverter
 * @package console\services
 */
class FakeUserConverter implements ConverterInterface
{
    /**
     * @param $rawData
     * @return mixed|null
     * @throws Exception
     */
    public function convertObject($rawData)
    {
        $faker = Factory::create('ru_RU');
        $data = [
            'UserForm' => [
                'username' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'type' => User::TYPE_DEFAULT,
                'password_hash' => Yii::$app->security->generatePasswordHash($faker->password),
            ],
            'GeoForm' => [
                'lat' => $faker->latitude,
                'lng' => $faker->longitude,
            ],
            'MetaForm' => [
                'description' => $faker->text,
                'subtitle' => $faker->jobTitle,
            ]
        ];
        $pathInfo = FileHelper::createTempDirectory('@frontend');
        $imageName = $faker->image($pathInfo['basePath'], 320, 320, null, false);
        $data['UserForm']['avatar'] = $pathInfo['relativePath'] . $imageName;

        return $data;
    }
}
