<?php

namespace console\services;

use common\helpers\SpatialHelper;
use common\models\Translation;
use common\services\entities\CityService;
use console\interfaces\ConverterInterface;

/**
 * Class DistrictFileConverter
 * @package console\services
 */
class DistrictFileConverter implements ConverterInterface
{
    /**
     * @var CityService
     */
    private $_cityService;

    /**
     * DistrictFileConverter constructor.
     * @param CityService $cityService
     */
    public function __construct(CityService $cityService)
    {
        $this->_cityService = $cityService;
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        $city = $this->_cityService->getOne(['slug' => $rawData['city']]);
        if ($city === null) {
            return null;
        }
        $polygon = SpatialHelper::simplifyPolygon($rawData['polygon'], 1);
        $data = [
            'DistrictForm' => [
                'city_id' => $city['id'],
                'slug' => $rawData['slug'],
            ],
            'MetaForm' => [
                'ru-RU' => [
                    Translation::KEY_TITLE => $rawData['title'] ?? null,
                ]
            ],
            'LightGeoForm' => array_map(function ($point) {
                return ['lat' => $point[1], 'lng' => $point[0]];
            }, $polygon),
        ];

        return $data;
    }
}
