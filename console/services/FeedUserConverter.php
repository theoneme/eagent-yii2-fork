<?php

namespace console\services;

use common\helpers\FileHelper;
use common\helpers\XpathHelper;
use common\models\UserContact;
use common\services\GoogleMapsService;
use console\interfaces\ConverterInterface;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use yii\helpers\StringHelper;

/**
 * Class FeedUserConverter
 * @package console\services
 */
class FeedUserConverter implements ConverterInterface
{
    /**
     * @var GoogleMapsService
     */
    private $_googleMapsService;

    /**
     * PrianCompanyConverter constructor.
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(GoogleMapsService $googleMapsService)
    {
        $this->_googleMapsService = $googleMapsService;
    }

    /**
     * @param $rawData
     * @return array|mixed|null
     * @throws \yii\base\Exception
     */
    public function convertObject($rawData)
    {
        if (empty($rawData['sales-agent'])) {
            return null;
        }

        $pathInfo = FileHelper::createTempDirectory('@frontend');

        $data = [
            'userData' => [],
            'companyData' => [
                'ContactForm' => [],
                'MetaForm' => ['locale' => 'ru-RU']
            ]
        ];

        if (!empty($rawData['sales-agent']['name'])) {
            $data['userData']['username'] = $rawData['sales-agent']['name'];
            if (!empty($rawData['sales-agent']['phone'])) {
                $data['userData']['phone'] = "+" . preg_replace("/[^0-9]/", "", $rawData['sales-agent']['phone']);
            }
            if (!empty($rawData['sales-agent']['email'])) {
                $data['userData']['email'] = $rawData['sales-agent']['email'];
            }
        }


        if (!empty($rawData['sales-agent']['organization'])) {
            $data['companyData']['MetaForm']['title'] = $rawData['sales-agent']['organization'];

            if (!empty($rawData['sales-agent']['url'])) {
                $data['companyData']['url'] = $rawData['sales-agent']['url'];
            }

            if (!empty($rawData['sales-agent']['phone'])) {
                $data['companyData']['ContactForm'][] = [
                    'type' => UserContact::TYPE_PHONE,
                    'value' => "+" . preg_replace("/[^0-9]/", "", $rawData['sales-agent']['phone'])
                ];
            }
            if (!empty($rawData['sales-agent']['email'])) {
                $data['companyData']['ContactForm'][] = [
                    'type' => UserContact::TYPE_EMAIL,
                    'value' => $rawData['sales-agent']['email']
                ];
            }

            if (!empty($rawData['sales-agent']['photo'])) {
                $image = explode('?', $rawData['sales-agent']['photo'])[0];
                $newAbsolutePath = $pathInfo['basePath'] . basename($image);
                $newRelativePath = $pathInfo['relativePath'] . basename($image);
                $headers = get_headers($image);
                if (strpos($headers[0], '200') !== false) {
                    $file = file_get_contents($image);
                    if ($file) {
                        file_put_contents($newAbsolutePath, $file);
                        $data['companyData']['CompanyForm']['logo'] = $newRelativePath;
                    }
                }
            }
        }

        return $data;
    }
}
