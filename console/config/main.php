<?php

use frontend\widgets\IndexTabsWidget;
use yii\log\FileTarget;
use yii\i18n\PhpMessageSource;
use yii\console\controllers\FixtureController;
use frostealth\yii2\aws\s3\Service;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'fixture' => [
            'class' => FixtureController::class,
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'model*' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'agent*' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'model' => 'agent.php',
                    ],
                ],
                'wizard*' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'wizard' => 'wizard.php',
                    ],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'baseUrl' => 'https://eagent.me/',
            'hostInfo' => 'https://eagent.me/',
            'rules' => [
                'sitemap.xml' => 'sitemap/default/index',
                [
                    'pattern' => 'sitemap-<action(list)>-<entity:[\w-]+>',
                    'route' => 'sitemap/default/<action>',
                    'suffix' => '.xml',
                ],

                '/<operation:(' . implode('|', IndexTabsWidget::getOperationList()) . ')>' => 'site/index',
                '/' => 'site/index',

                '/<slug:(privacy-policy)>' => 'page/view',
                '<slug:[\w-]+>-<action:(property|building)>' => 'property/<action>/show',
                '<category:[\w-]+>-for-<operation:(sale|rent)>-catalog' => 'property/catalog/index',

                '/crm' => 'crm/site/index',
                '/crm/<action:[\w-]+>' => 'crm/site/<action>',

                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',

                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        's3uJobs' => [
            'class' => Service::class,
            'credentials' => [
                'key' => 'AKIAJKAMNOAZSPXQ3LMA',
                'secret' => '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',
            ],
            'region' => 'eu-central-1',
            'defaultBucket' => 'ujobs-media-s',
            'defaultAcl' => 'public-read',
        ],
        's3ea' => [
            'class' => Service::class,
            'credentials' => [
                'key' => 'AKIAJKAMNOAZSPXQ3LMA',
                'secret' => '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',
            ],
            'region' => 'us-west-2',
            'defaultBucket' => 'eroom-property-images',
            'defaultAcl' => 'public-read',
        ],
    ],
    'params' => $params,
];
