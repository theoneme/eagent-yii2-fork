# eAgent YII2

DIRECTORY STRUCTURE
-------------------

```
common
    assets/              contains shared assets
    behaviors/           contains custom behaviors for models
    components           contains custom or overrided components
    controllers/         contains base front and back controllers
    config/              contains shared configurations
    data/                contains custom yii data classes (providers)
    decorators/          contains decorators for statuses, labels etc.
    dto/                 contains DTO classes for entities
    exceptions/          contains custom exceptions
    factories/           contains custom class factories
    forms/               contains shared form classes
    helpers/             contains shared helper classes
    interfaces/          contains shared interfaces
    mappers/             contains shared data mapping classes
    messages/            contains shared translation files and configuration
    models/              contains models, also search classes
    morpheus-messages/   contains configuration needed for declensions
    public/              contains shared assets
    repositories/        contains repositoty classes
    services/            contains shared project services
    tests/               contains tests for common classes  
    traits/              contains shared traits
    validators/          contains custom shared validators
    views/               contains shared views
    widgets/             contains shared widgets
     
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    forms/               contains overrided forms for console
    import/              contains data for imports into db
    interfaces/          contains interfaces for console app
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
    services/            contains console services
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    forms/               contains overrided forms for backend
    helpers/             contains some backend helpers
    messages/            contains backend translation files
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    components/          contains custom critical frontend components (language, currency, themes etc..)
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    filters/             contains custom access filters
    forms/               contains frontend form classes
    helpers/             contains frontend helper classes
    mappers/             contains frontend mapper classes
    messages/            contains frontend translation files and config
    models/              contains frontend-specific model classes
    modules/             contains custom modules (crm, landings, site constructor etc..)
    rules/               contains frontend rbac rules
    runtime/             contains files generated during runtime
    services/            contains frontend services
    tests/               contains tests for frontend application
    themes/              contains frontend mobile theme
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
instance
    config/              contains instance configurations
    runtime/             contains files generated during runtime
    tests/               contains tests for instance application
    web/                 contains the entry script and Web resources
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
