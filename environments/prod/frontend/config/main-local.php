<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '2sH6jl_-FHJDkURUDNvndQ8U8oAZatvS',
        ],
        'session' => [
            'cookieParams' => [
                'domain' => '.eagent.me',
            ]
        ],
        'user' => [
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'domain' => '.eagent.me',
            ],
        ],
    ],
];

if ($_SERVER['REMOTE_ADDR'] === '109.254.78.100') {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['109.254.78.100'],
    ];
}

return $config;