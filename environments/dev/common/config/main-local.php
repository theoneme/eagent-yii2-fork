<?php
return [
    'components' => [
        'db' => [
            'class' => \common\components\db\Connection::class,
            'dsn' => 'mysql:host=localhost;dbname=eagent_new',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
        ],
        'mailer' => [
            'class' => 'common\components\Mailer',
            'viewPath' => '@common/views/mail',
            'useFileTransport' => true,
        ],
    ],
];
