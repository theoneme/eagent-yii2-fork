<?php

use frontend\components\themes\ViewMode;

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-instance',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => '\frontend\modules\instance\controllers',
//    'defaultRoute' => '/instance/site/index',
    'bootstrap' => ['log', 'instance'],
    'name' => 'eAgent',
    'modules' => [
        'instance' => [
            'class' => 'frontend\modules\instance\Standalone'
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-instance', 'httpOnly' => true],
            'loginUrl' => ['auth/login'],
        ],
        'request' => [
            'csrfParam' => '_csrf-instance',
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
            ],
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'cookieParams' => [
                'httponly' => true,
                'lifetime' => 3600 * 8,
            ],
            'timeout' => 3600 * 24 * 30,
            'useCookies' => true,
            'name' => 'instanceSession'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'ruleConfig' => [
                'class' => 'frontend\components\CustomUrlRule'
            ],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '',
            'rules' => [
                '/' => 'instance/site/index',

                '<slug:[\w-]+>-<action:(property)>' => 'instance/property/<action>/view',
                '<slug:[\w-]+>-<action:(building)>' => 'instance/building/<action>/view',
                '<category:[\w-]+>-for-<operation:(sale|rent)>-catalog' => 'instance/property/catalog/index',
                '<operation:(secondary|new-construction)>-buildings-catalog' => 'instance/building/catalog/index',

//                '<controller:\w+>/<id:\d+>' => '<controller>/view',
//                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
//                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

//                '<module:\w+>/<controller:\w+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
//                'instance/<module:\w+>/<controller:\w+>/<action:[\w-]+>' => '<module>/<controller>/<action>'

                '<controller:[\w-]+>/<action:[\w-]+>' => 'instance/<controller>/<action>',

                '<entity:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>' => 'instance/<entity>/<controller>/<action>',
            ],
        ],
        'i18n' => [
            'translations' => [
                'main*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'main' => 'main.php',
                    ],
                ],
                'catalog*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'catalog' => 'catalog.php',
                    ],
                ],
                'labels*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'labels' => 'labels.php',
                    ],
                ],
            ],
        ],
        'view' => function (ViewMode $mode) {
            return Yii::createObject([
                'class' => \frontend\components\View::class,
                'enableMinify' => !YII_DEBUG,
                'concatCss' => true,
                'minifyCss' => true,
                'concatJs' => true,
                'minifyJs' => true,
                'webPath' => '@web',
                'basePath' => '@webroot',
                'minifyPath' => '@webroot/minify',
                'jsPosition' => [\yii\web\View::POS_END],
                'forceCharset' => 'UTF-8',
                'expandImports' => true,
                'compressOptions' => ['extra' => true],
                'isMobile' => $mode->isMobile(Yii::$app->request),
                'theme' => $mode->isMobile(Yii::$app->request) ? [
                    'pathMap' => [
                        '@frontend/modules/instance/views' => [
                            '@frontend/modules/instance/views',
                        ],
                        '@frontend/modules/instance/widgets' => [
                            '@frontend/modules/instance/widgets',
                        ]
                    ],
                ] : null,
            ]);
        },
    ],
    'as viewMode' => [
        'class' => \frontend\components\themes\filters\ViewModeFilter::class,
        'expire' => 2592000 // 30 days
    ],
    'params' => $params,
];
