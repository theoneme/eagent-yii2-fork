<?php
return [
    'id' => 'app-eagent-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
    ],
];
