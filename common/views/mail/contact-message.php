<?php


/**
 * @var string $receiverName
 * @var string $locale
 * @var string $senderName
 * @var string $senderEmail
 * @var string $senderMessage
 * @var string $senderPhone
 * @var string $address
 * @var string $floor
 * @var string $area
 * @var string $rooms
 * @var string $link
 */

?>

<table align="center" style="width: 100%">
    <tbody>
    <tr>
        <td style="color:#666;text-align:center;">
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
                        <?= Yii::t('notification', 'Message from contact form', [], $locale) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <?php if (!empty($senderEmail)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b>Email:</b> <?= $senderEmail ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($senderPhone)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('notification', 'Phone') ?>:</b> <?= $senderPhone ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($senderName)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('notification', 'Name') ?>:</b> <?= $senderName ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($address)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('notification', 'Address') ?>:</b> <?= $address ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($floor)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('notification', 'Floor') ?>:</b> <?= $floor ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($rooms)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('notification', 'Rooms') ?>:</b> <?= $rooms ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($area)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('notification', 'Area') ?>:</b> <?= $area ?>
                        </td>
                    </tr>
                <?php } ?>
<!--                <tr>-->
<!--                    <td style="color:#666;font-size:18px;text-align:left">-->
<!--                        <b>--><?//= Yii::t('notification', 'Message') ?><!--:</b> --><?//= $senderMessage ?>
<!--                    </td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <td style="color:#666;font-size:18px;text-align:left">-->
<!--                        --><?//= Yii::t('notification', 'View this property {by-link}', [
//                            'by-link' => Html::a(Yii::t('notification', 'by link'), $link)
//                        ]) ?>
<!--                    </td>-->
<!--                </tr>-->
                <tr>
                    <td style="height: 20px"></td>
                </tr>
                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('notification', 'Sent with {name}', ['name' => Yii::$app->name]) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>