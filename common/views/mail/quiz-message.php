<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.10.2018
 * Time: 11:50
 */

use yii\helpers\Html;

/**
 * @var string $receiverName
 * @var string $locale
 * @var string $senderName
 * @var string $senderEmail
 * @var string $senderMessage
 * @var string $senderPhone
 * @var string $link
 */

?>

<table align="center" style="width: 100%">
    <tbody>
    <tr>
        <td style="color:#666;text-align:center;">
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
                        <?= Yii::t('notification', 'Greetings!') ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= $senderMessage ?>
                    </td>
                </tr>
                <?php if (!empty($senderEmail)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b>Email:</b> <?= $senderEmail ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($senderName)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('notification', 'Name') ?>:</b> <?= $senderName ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php foreach($attributes as $label => $attribute) { ?>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <b><?= $label ?>:</b> <?= $attribute ?>
                    </td>
                </tr>
                <?php } ?>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('notification', 'View this property {by-link}', [
                            'by-link' => Html::a(Yii::t('notification', 'by link'), $link)
                        ]) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                </tr>
                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('notification', 'Sent with {name}', ['name' => Yii::$app->name]) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>