<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.03.2019
 * Time: 12:23
 */

/**
 * @var string $heading
 * @var array $contacts
 * @var string $message
 */

?>

<p>
    <?= $heading ?>
</p>
<?php if (!empty($contacts)) { ?>
    <?php foreach ($contacts as $contact) { ?>
        <?php if ($contact['value']) { ?>
            <p>
                <b><?= $contact['title'] ?></b>: <?= $contact['value'] ?>
            </p>
        <?php } ?>
    <?php } ?>
<?php } ?>
<p>
    <?= $message ?>
</p>
