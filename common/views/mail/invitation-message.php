<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $locale
 * @var string $id
 * @var string $name
 * @var string $login
 * @var string $password
 */

?>

<table align="center" style="width: 100%">
    <tbody>
    <tr>
        <td style="color:#666;text-align:center;">
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
                        <?= Yii::t('notification', 'Hello, {name}', ['name' => $name], $locale) ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>

                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('notification', 'We posted your property on our portal', ['link' => Html::a('https://eagent.me', 'https://eagent.me')], $locale) ?>
                        <br>
                        <?= Yii::t('notification', 'You can check the details', [], $locale) ?>
                        <br>
                        <?= Yii::t('notification', 'Your Login: {login}', ['login' => $login], $locale) ?>
                        <br>
                        <?= Yii::t('notification', 'Your temporary password: {password}', ['password' => $password], $locale) ?>
                    </td>
                </tr>

                <tr>
                    <td style="height: 10px"></td>
                </tr>
                </tbody>
            </table>

            <table style="margin: 0; width: 100%" align="left">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <tr>
                    <td>
                        <?= Html::a(
                            Yii::t('notification', 'Check your profile', [], $locale),
                            Url::to(['/agent/agent/view', 'id' => $id, 'email-action' => 1, 'showLoginModal' => true], true),
                            [
                                'style' => 'background-color:#66afe9;border:1px solid #629ad1;border-radius:3px;padding:16px 20px;display:block;text-decoration:none;color:#fff;font-size:16px;text-align:center;font-family:arial;font-weight:bold;width:200px;',
                                'target' => '_blank'
                            ]
                        ) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                </tbody>
            </table>

            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('notification', 'Our site gives you additional features:', [], $locale) ?>
                        <br>
                        - <?= Yii::t('notification', 'Posting your ads for free', [], $locale) ?>
                        <br>
                        - <?= Yii::t('notification', 'Free CRM', [], $locale) ?>
                        <br>
                        - <?= Yii::t('notification', 'Free translation into 7 languages', [], $locale) ?>
                        <br>
                        - <?= Yii::t('notification', 'Search for customers in various countries, such as China, USA, Brazil, Russia and many others', [], $locale) ?>
                        <br>
                        - <?= Yii::t('notification', 'New customers for the purchase and rental of real estate worldwide', [], $locale) ?>
                    </td>
                </tr>

                <tr>
                    <td style="height: 10px"></td>
                </tr>

                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('notification', 'If you will have any questions please contact us on email {email}', ['email' => 'info@eagent.me'], $locale) ?>
                    </td>
                </tr>

                <tr>
                    <td style="height: 10px"></td>
                </tr>

                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('notification', 'Best regards,', ['name' => Yii::$app->name]) ?>
                        <br>
                        <?= Yii::t('notification', 'Team {name}', ['name' => Yii::$app->name]) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>