<?php

namespace common\dto;

use common\components\CurrencyHelper;
use common\interfaces\DTOInterface;
use common\mappers\WalletHistoryMapper;
use common\models\UserWallet;
use common\models\WalletHistory;

/**
 * Class UserWalletDTO
 * @package common\dto
 */
class UserWalletDTO implements DTOInterface
{
    /**
     * @var UserWallet
     */
    private $_userWallet;

    /**
     * UserWalletDTO constructor.
     * @param $userWallet
     */
    public function __construct($userWallet)
    {
        $this->_userWallet = $userWallet;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $histories = WalletHistoryMapper::getMappedData($this->_userWallet['histories']);
        return [
            'id' => $this->_userWallet['id'],
            'user_id' => $this->_userWallet['user_id'],
            'balance' => $this->_userWallet['balance'],
            'currency_code' => $this->_userWallet['currency_code'],
            'balanceFormatted' => CurrencyHelper::format($this->_userWallet['currency_code'], $this->_userWallet['balance']),
            'histories' => $histories,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $histories = WalletHistoryMapper::getMappedData($this->_userWallet['histories']);
        $totalIncome = CurrencyHelper::format(
            $this->_userWallet['currency_code'],
            array_sum(
                array_column(array_filter($histories, function($var){
                    return ($var['type'] !== WalletHistory::TYPE_INFO) && $var['total'] > 0;
                }), 'total')
            )
        );
        $totalCosts = CurrencyHelper::format(
            $this->_userWallet['currency_code'],
            array_sum(
                array_column(array_filter($histories, function($var){
                    return ($var['type'] !== WalletHistory::TYPE_INFO) && $var['total'] < 0;
                }), 'total')
            )
        );
        return [
            'id' => $this->_userWallet['id'],
            'user_id' => $this->_userWallet['user_id'],
            'balance' => $this->_userWallet['balance'],
            'currency_code' => $this->_userWallet['currency_code'],
            'balanceFormatted' => CurrencyHelper::format($this->_userWallet['currency_code'], $this->_userWallet['balance']),
            'histories' => $histories,
            'totalIncome' => $totalIncome,
            'totalCosts' => $totalCosts,
        ];
    }
}