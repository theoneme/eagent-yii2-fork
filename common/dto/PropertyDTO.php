<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\components\CurrencyHelper;
use common\dto\advanced\UserMiniDTO;
use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AddressTranslationsMapper;
use common\mappers\AttachmentsMapper;
use common\mappers\PropertyAttributesMapper;
use common\mappers\ShortPropertyAttributesMapper;
use common\mappers\TranslationsMapper;
use common\models\Property;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PropertyDTO
 * @package common\dto
 */
class PropertyDTO implements DTOInterface
{
    /**
     * @var Property
     */
    private $_property;
    /**
     * @var array
     */
    private $_attributesData;
    /**
     * @var array
     */
    private $_attributeValuesData;

    /**
     * PropertyDTO constructor.
     * @param array $property
     * @param array $attributesData
     * @param array $attributeValuesData
     */
    public function __construct($property, array $attributesData = [], array $attributeValuesData = [])
    {
        $this->_property = $property;
        $this->_attributesData = $attributesData;
        $this->_attributeValuesData = $attributeValuesData;
    }

    /**
     * @param int $mode
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getFullData()
    {
        $images = AttachmentsMapper::getMappedData($this->_property['attachments']);
        $thumbnails = AttachmentsMapper::getMappedData($this->_property['attachments'], AttachmentsMapper::MODE_THUMB);
        $currentTranslation = TranslationsMapper::getMappedData($this->_property['translations']);
        $translations = TranslationsMapper::getMappedData($this->_property['translations'], TranslationsMapper::MODE_FULL);
        $categoryTranslations = TranslationsMapper::getMappedData($this->_property['category']['translations']);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_property['addressTranslations']);
        $attributes = PropertyAttributesMapper::getMappedData([
            'attributes' => $this->_attributesData,
            'attributeValues' => $this->_attributeValuesData
        ]);
        if (!empty($this->_property['category']['customDataArray'])) {
            $customDataArray = $this->_property['category']['customDataArray'];
        } else if ($this->_property['category']['custom_data'] !== null) {
            $customDataArray = json_decode($this->_property['category']['custom_data'], true);
        }

        $video = !empty($this->_property['videos']) ? [
            'content' => $this->_property['videos'][0]['content'],
            'type' => $this->_property['videos'][0]['type'],
        ] : [];

        return [
            'id' => $this->_property['id'],
            'type' => (int)$this->_property['type'],
            'user_id' => $this->_property['user_id'],
            'region_id' => $this->_property['region_id'],
            'category_id' => $this->_property['category_id'],
            'building_id' => $this->_property['building_id'],
            'city_id' => $this->_property['city_id'],
            'slug' => ArrayHelper::remove($currentTranslation, 'slug'),
            'locale' => $this->_property['locale'],
            'images' => $images,
            'thumbnails' => $thumbnails,
            'image' => $images[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'address' => $addressTranslation['title'] ?? $this->_property['address'],
            'addressData' => $addressTranslation['data'] ?? [],
            'description' => ArrayHelper::remove($currentTranslation, 'description'),
            'translations' => $translations,
            'attributes' => $attributes,
            'lat' => $this->_property['lat'],
            'lng' => $this->_property['lng'],
            'price' => CurrencyHelper::convertAndFormat($this->_property['currency_code'], Yii::$app->params['app_currency'], $this->_property['price']),
            'raw_price' => CurrencyHelper::convert($this->_property['currency_code'], Yii::$app->params['app_currency'], $this->_property['price']),
            'source_price' => $this->_property['price'],
            'currency_code' => $this->_property['currency_code'],
            'created_at' => $this->_property['created_at'],
            'status' => $this->_property['status'],
            'user' => (new UserMiniDTO($this->_property['user']))->getFullData(),
            'category' => $categoryTranslations,
            'categorySeoTemplate' => $customDataArray['seoTemplate'] ?? null,
            'zip' => $this->_property['zip'],
            'views_count' => $this->_property['views_count'],
            'video' => $video,
            'inCompare' => in_array($this->_property['id'], Yii::$app->session->get('compare', []))
        ];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getShortData()
    {
        $images = AttachmentsMapper::getMappedData($this->_property['attachments']);
        $thumbnails = AttachmentsMapper::getMappedData($this->_property['attachments'], AttachmentsMapper::MODE_THUMB);
        $translations = TranslationsMapper::getMappedData($this->_property['translations']);
        $attributes = ShortPropertyAttributesMapper::getMappedData($this->_property['propertyAttributes']);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_property['addressTranslations'] ?? []);
        $categoryTranslations = TranslationsMapper::getMappedData($this->_property['category']['translations'] ?? []);

        return [
            'id' => $this->_property['id'],
            'type' => (int)$this->_property['type'],
            'slug' => ArrayHelper::remove($translations, 'slug'),
            'status' => $this->_property['status'],
            'image' => $thumbnails[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'images' => $images,
            'thumbnails' => $thumbnails,
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'description' => ArrayHelper::remove($translations, 'description'),
            'address' => $addressTranslation['title'] ?? $this->_property['address'],
            'addressData' => $addressTranslation['data'] ?? [],
            'attributes' => $attributes,
            'lat' => $this->_property['lat'],
            'lng' => $this->_property['lng'],
            'created_at' => $this->_property['created_at'],
            'updated_at' => $this->_property['updated_at'],
            'price' => CurrencyHelper::convertAndFormat($this->_property['currency_code'], Yii::$app->params['app_currency'], $this->_property['price']),
            'raw_price' => CurrencyHelper::convert($this->_property['currency_code'], Yii::$app->params['app_currency'], $this->_property['price']),
            'source_price' => $this->_property['price'],
            'user_id' => $this->_property['user_id'],
            'building_id' => $this->_property['building_id'],
            'city_id' => $this->_property['city_id'],
            'category_id' => $this->_property['category_id'],
            'user' => array_key_exists('user', $this->_property) ? (new UserMiniDTO($this->_property['user']))->getShortData() : null,
            'category' => $categoryTranslations,
            'inCompare' => in_array($this->_property['id'], Yii::$app->session->get('compare', []))
        ];
    }
}