<?php

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AddressTranslationsMapper;
use common\mappers\CompanyMembersMapper;
use common\mappers\ContactsMapper;
use common\mappers\TranslationsMapper;
use common\models\Company;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CompanyDTO
 * @package common\dto
 */
class CompanyDTO implements DTOInterface
{
    /**
     * @var Company
     */
    private $_company;

    /**
     * CompanyDTO constructor.
     * @param $company
     */
    public function __construct($company)
    {
        $this->_company = $company;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_company['translations']);
        $translations = TranslationsMapper::getMappedData($this->_company['translations']);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_company['addressTranslations']);
        $contacts = ContactsMapper::getMappedData($this->_company['user']['contacts']);
        $members = CompanyMembersMapper::getMappedData($this->_company['companyMembers']);

        $logo = !empty($this->_company['logo']) ? $this->_company['logo'] : '/images/agent-no-image.png';

        return [
            'id' => $this->_company['id'],
            'user_id' => $this->_company['user_id'],
            'status' => $this->_company['status'],
            'type' => $this->_company['type'],
            'slug' => ArrayHelper::remove($currentTranslation, 'slug'),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'description' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'description')),
            'lat' => $this->_company['lat'],
            'lng' => $this->_company['lng'],
            'logo' => Yii::$app->mediaLayer->getThumb($logo),
            'thumb' => Yii::$app->mediaLayer->getThumb($logo, 'catalog'),
            'banner' => Yii::$app->mediaLayer->getThumb($this->_company['banner'], 'catalog'),
            'translations' => $translations,
            'address' => $addressTranslation['title'] ?? '',
            'addressData' => $addressTranslation['data'] ?? [],
            'contacts' => $contacts,
            'members' => $members,
            'ads_allowed' => $this->_company['user']['ads_allowed'],
            'ads_allowed_partners' => $this->_company['user']['ads_allowed_partners'],
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_company['translations']);
        $logo = !empty($this->_company['logo']) ? $this->_company['logo'] : '/images/agent-no-image.png';
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_company['addressTranslations']);
        $contacts = ContactsMapper::getMappedData($this->_company['user']['contacts']);

        return [
            'id' => $this->_company['id'],
            'user_id' => $this->_company['user_id'],
            'status' => $this->_company['status'],
            'type' => $this->_company['type'],
            'created_at' => $this->_company['created_at'],
            'slug' => ArrayHelper::remove($translations, 'slug'),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'lat' => $this->_company['lat'],
            'lng' => $this->_company['lng'],
            'logo' => Yii::$app->mediaLayer->getThumb($logo, 'catalog'),
            'address' => $addressTranslation['title'] ?? '',
            'addressData' => $addressTranslation['data'] ?? [],
            'translations' => $translations,
            'contacts' => $contacts
        ];
    }
}