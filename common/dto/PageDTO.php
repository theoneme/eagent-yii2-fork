<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use yii\helpers\ArrayHelper;

/**
 * Class PageDTO
 * @package common\dto
 */
class PageDTO implements DTOInterface
{
    /**
     * @var array
     */
    private $_page;

    /**
     * PageDTO constructor.
     * @param $page
     */
    public function __construct($page)
    {
        $this->_page = $page;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_page['translations']);
        $translations = TranslationsMapper::getMappedData($this->_page['translations'], TranslationsMapper::MODE_FULL);

        return [
            'id' => $this->_page['id'],
            'slug' => ArrayHelper::remove($currentTranslation, 'slug'),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'description' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'description')),
            'translations' => $translations,
            'created_at' => $this->_page['created_at']
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_page['translations']);

        return [
            'id' => $this->_page['id'],
            'slug' => ArrayHelper::remove($translations, 'slug'),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
        ];
    }
}