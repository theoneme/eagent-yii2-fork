<?php

namespace common\dto;

use common\interfaces\DTOInterface;
use common\models\Payment;

/**
 * Class PaymentDTO
 * @package common\dto
 */
class PaymentDTO implements DTOInterface
{
    /**
     * @var Payment
     */
    private $_payment;

    /**
     * PaymentDTO constructor.
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->_payment = $payment;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $paymentMethod = (new PaymentMethodDTO($this->_payment['paymentMethod']))->getFullData();
        return [
            'id' => $this->_payment->id,
            'user_id' => $this->_payment->user_id,
            'payment_method_id' => $this->_payment->payment_method_id,
            'type' => $this->_payment->type,
            'total' => $this->_payment->total,
            'currency_code' => $this->_payment->currency_code,
            'code' => $this->_payment->code,
            'created_at' => $this->_payment->created_at,
            'updated_at' => $this->_payment->updated_at,
            'paymentMethod' => $paymentMethod,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_payment->id,
            'user_id' => $this->_payment->user_id,
            'payment_method_id' => $this->_payment->payment_method_id,
            'type' => $this->_payment->type,
            'total' => $this->_payment->total,
            'currency_code' => $this->_payment->currency_code,
            'code' => $this->_payment->code,
        ];
    }
}