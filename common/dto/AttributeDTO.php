<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\Attribute;
use common\models\Translation;
use yii\helpers\ArrayHelper;

/**
 * Class AttributeDTO
 * @package common\dto
 */
class AttributeDTO implements DTOInterface
{
    /**
     * @var Attribute
     */
    private $_attribute;

    /**
     * AttributeDTO constructor.
     * @param Attribute $attribute
     */
    public function __construct(Attribute $attribute)
    {
        $this->_attribute = $attribute;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_attribute['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_attribute['translations']);

        return [
            'id' => $this->_attribute->id,
            'alias' => $this->_attribute->alias,
            'type' => $this->_attribute->type,
            'title' => ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE),
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_attribute['translations']);
        return [
            'id' => $this->_attribute->id,
            'alias' => $this->_attribute->alias,
            'type' => $this->_attribute->type,
            'translations' => $translations,
        ];
    }
}