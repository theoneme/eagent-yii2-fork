<?php

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\WalletHistoryMapper;
use common\models\WalletHistory;

/**
 * Class WalletHistoryDTO
 * @package common\dto
 */
class WalletHistoryDTO implements DTOInterface
{
    /**
     * @var WalletHistory
     */
    private $_walletHistory;

    /**
     * WalletHistoryDTO constructor.
     * @param WalletHistory $walletHistory
     */
    public function __construct(WalletHistory $walletHistory)
    {
        $this->_walletHistory = $walletHistory;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        return [
            'id' => $this->_walletHistory->id,
            'user_id' => $this->_walletHistory->user_id,
            'payment_id' => $this->_walletHistory->payment_id,
            'type' => $this->_walletHistory->type,
            'template' => $this->_walletHistory->template,
            'total' => $this->_walletHistory->total,
            'currency_code' => $this->_walletHistory->currency_code,
            'created_at' => $this->_walletHistory->created_at,
            'customDataArray' => $this->_walletHistory->customDataArray,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_walletHistory->id,
            'type' => $this->_walletHistory->type,
            'template' => $this->_walletHistory->template,
            'total' => $this->_walletHistory->total,
            'currency_code' => $this->_walletHistory->currency_code,
            'created_at' => $this->_walletHistory->created_at,
            'customDataArray' => $this->_walletHistory->customDataArray,
        ];
    }
}