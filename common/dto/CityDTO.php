<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use yii\helpers\ArrayHelper;

/**
 * Class CityDTO
 * @package common\dto
 */
class CityDTO implements DTOInterface
{
    /**
     * @var array
     */
    private $_city;

    /**
     * CityDTO constructor.
     * @param $city
     */
    public function __construct($city)
    {
        $this->_city = $city;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_city['translations']);
        $translations = TranslationsMapper::getMappedData($this->_city['translations'], TranslationsMapper::MODE_FULL);
        $countryTranslations = TranslationsMapper::getMappedData($this->_city['country']['translations']);
        $regionTranslations = TranslationsMapper::getMappedData($this->_city['region']['translations']);

        return [
            'id' => $this->_city['id'],
            'slug' => $this->_city['slug'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'lat' => $this->_city['lat'],
            'lng' => $this->_city['lng'],
            'region_id' => $this->_city['region_id'],
            'country_id' => $this->_city['country_id'],
            'is_big' => $this->_city['is_big'],
            'translations' => $translations,
            'region' => $regionTranslations,
            'country' => $countryTranslations
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_city['translations']);
        $countryTranslations = TranslationsMapper::getMappedData($this->_city['country']['translations'] ?? []);
        $regionTranslations = TranslationsMapper::getMappedData($this->_city['region']['translations'] ?? []);

        return [
            'id' => $this->_city['id'],
            'slug' => $this->_city['slug'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'lat' => $this->_city['lat'],
            'lng' => $this->_city['lng'],
            'region_id' => $this->_city['region_id'],
            'country_id' => $this->_city['country_id'],
            'is_big' => $this->_city['is_big'],
            'region' => $regionTranslations,
            'country' => $countryTranslations
        ];
    }
}