<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\Country;
use yii\helpers\ArrayHelper;

/**
 * Class CountryDTO
 * @package common\dto
 */
class CountryDTO implements DTOInterface
{
    /**
     * @var Country
     */
    private $_country;

    /**
     * CountryDTO constructor.
     * @param Country $country
     */
    public function __construct(Country $country)
    {
        $this->_country = $country;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_country->translations);
        $translations = TranslationsMapper::getMappedData($this->_country->translations, TranslationsMapper::MODE_FULL);

        return [
            'id' => $this->_country->id,
            'code' => $this->_country->code,
            'slug' => $this->_country->slug,
            'title' => ArrayHelper::remove($currentTranslation, 'title'),
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_country->translations);

        return [
            'id' => $this->_country->id,
            'slug' => $this->_country->slug,
            'code' => $this->_country['code'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
        ];
    }
}