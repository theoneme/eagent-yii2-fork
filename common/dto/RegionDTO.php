<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use yii\helpers\ArrayHelper;

/**
 * Class RegionDTO
 * @package common\dto
 */
class RegionDTO implements DTOInterface
{
    /**
     * @var array
     */
    private $_region;

    /**
     * RegionDTO constructor.
     * @param array $region
     */
    public function __construct($region)
    {
        $this->_region = $region;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_region['translations']);
        $translations = TranslationsMapper::getMappedData($this->_region['translations'], TranslationsMapper::MODE_FULL);
        $countryTranslations = TranslationsMapper::getMappedData($this->_region['country']['translations']);

        return [
            'id' => $this->_region['id'],
            'slug' => $this->_region['slug'],
            'title' => ArrayHelper::remove($currentTranslation, 'title'),
            'code' => $this->_region['code'],
            'country_id' => $this->_region['country_id'],
            'country' => $countryTranslations,
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_region['translations']);
        $countryTranslations = TranslationsMapper::getMappedData($this->_region['country']['translations'] ?? []);

        return [
            'id' => $this->_region['id'],
            'slug' => $this->_region['slug'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'country_id' => $this->_region['country_id'],
            'code' => $this->_region['code'],
            'country' => $countryTranslations,
        ];
    }
}