<?php

namespace common\dto;

use common\interfaces\DTOInterface;
use common\models\PaymentMethod;

/**
 * Class PaymentMethodDTO
 * @package common\dto
 */
class PaymentMethodDTO implements DTOInterface
{
    /**
     * @var PaymentMethod
     */
    private $_paymentMethod;

    /**
     * PaymentMethodDTO constructor.
     * @param PaymentMethod $paymentMethod
     */
    public function __construct(PaymentMethod $paymentMethod)
    {
        $this->_paymentMethod = $paymentMethod;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        return [
            'id' => $this->_paymentMethod->id,
            'alias' => $this->_paymentMethod->alias,
            'logo' => $this->_paymentMethod->logo,
            'sort' => $this->_paymentMethod->sort,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_paymentMethod->id,
            'alias' => $this->_paymentMethod->alias,
            'logo' => $this->_paymentMethod->logo,
            'sort' => $this->_paymentMethod->sort,
        ];
    }
}