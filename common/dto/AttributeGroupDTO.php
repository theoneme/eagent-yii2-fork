<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\Attribute;
use common\models\AttributeGroup;
use yii\helpers\ArrayHelper;

/**
 * Class AttributeGroupDTO
 * @package common\dto
 */
class AttributeGroupDTO implements DTOInterface
{
    /**
     * @var Attribute
     */
    private $_attributeGroup;

    /**
     * AttributeGroupDTO constructor.
     * @param AttributeGroup $attributeGroup
     */
    public function __construct(AttributeGroup $attributeGroup)
    {
        $this->_attributeGroup = $attributeGroup;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $attrs = $this->_attributeGroup->attrs;

        $attributes = array_map(function ($attribute) {
            $translations = TranslationsMapper::getMappedData($attribute['translations']);
            return [
                'title' => ArrayHelper::remove($translations, 'title'),
                'type' => $attribute['type'],
                'id' => $attribute['id']
            ];
        }, $attrs);

        return [
            'id' => $this->_attributeGroup->id,
            'attributes' => $attributes
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [

        ];
    }
}