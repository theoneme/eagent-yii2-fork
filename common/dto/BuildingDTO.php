<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AddressTranslationsMapper;
use common\mappers\AttachmentsMapper;
use common\mappers\BuildingDocumentMapper;
use common\mappers\BuildingPhaseMapper;
use common\mappers\BuildingProgressMapper;
use common\mappers\BuildingSiteMapper;
use common\mappers\PropertyAttributesMapper;
use common\mappers\ShortPropertyAttributesMapper;
use common\mappers\TranslationsMapper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingDTO
 * @package common\dto
 */
class BuildingDTO implements DTOInterface
{
    /**
     * @var array
     */
    private $_building;
    /**
     * @var array
     */
    private $_attributesData;
    /**
     * @var array
     */
    private $_attributeValuesData;

    /**
     * BuildingDTO constructor.
     * @param $building
     * @param array $attributesData
     * @param array $attributeValuesData
     */
    public function __construct($building, array $attributesData = [], array $attributeValuesData = [])
    {
        $this->_building = $building;
        $this->_attributesData = $attributesData;
        $this->_attributeValuesData = $attributeValuesData;
    }

    /**
     * @param int $mode
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getFullData()
    {
        $images = AttachmentsMapper::getMappedData($this->_building['attachments']);
        $thumbnails = AttachmentsMapper::getMappedData($this->_building['attachments'], AttachmentsMapper::MODE_THUMB);
        $currentTranslation = TranslationsMapper::getMappedData($this->_building['translations']);
        $translations = TranslationsMapper::getMappedData($this->_building['translations'], TranslationsMapper::MODE_FULL);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_building['addressTranslations']);
        $phases = BuildingPhaseMapper::getMappedData($this->_building['phases']);
        $progress = BuildingProgressMapper::getMappedData($this->_building['progress']);
        $sites = BuildingSiteMapper::getMappedData($this->_building['sites']);
        $documents = BuildingDocumentMapper::getMappedData($this->_building['documents']);

        $attributes = PropertyAttributesMapper::getMappedData([
            'attributes' => $this->_attributesData,
            'attributeValues' => $this->_attributeValuesData
        ]);
//        $advancedAttributes = PropertyAdvancedAttributesMapper::getMappedData([
//            'attributes' => $this->_attributesData,
//            'attributeValues' => $this->_attributeValuesData
//        ], [29]);

        $video = !empty($this->_building['videos']) ? [
            'content' => $this->_building['videos'][0]['content'],
            'type' => $this->_building['videos'][0]['type'],
        ] : [];

        return [
            'id' => $this->_building['id'],
            'slug' => $this->_building['slug'],
            'images' => $images,
            'thumbnails' => $thumbnails,
            'image' => $images[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'name' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'name')),
            'address' => $addressTranslation['title'] ?? '',
            'addressData' => $addressTranslation['data'] ?? [],
            'description' => ArrayHelper::remove($currentTranslation, 'description'),
            'translations' => $translations,
            'attributes' => $attributes,
            'phases' => $phases,
            'progress' => $progress,
            'sites' => $sites,
            'documents' => $documents,
//            'advancedAttributes' => $advancedAttributes,
            'lat' => $this->_building['lat'],
            'lng' => $this->_building['lng'],
            'type' => $this->_building['type'],
            'status' => $this->_building['status'],
            'locale' => $this->_building['locale'],
            'video' => $video
        ];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getShortData()
    {
        $images = AttachmentsMapper::getMappedData($this->_building['attachments']);
        $thumbnails = AttachmentsMapper::getMappedData($this->_building['attachments'], AttachmentsMapper::MODE_THUMB);
        $translations = TranslationsMapper::getMappedData($this->_building['translations']);
        $attributes = ShortPropertyAttributesMapper::getMappedData($this->_building['buildingAttributes']);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_building['addressTranslations'] ?? []);

        return [
            'id' => $this->_building['id'],
            'slug' => $this->_building['slug'],
            'image' => $thumbnails[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'images' => $images,
            'thumbnails' => $thumbnails,
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'description' => ArrayHelper::remove($translations, 'description'),
            'address' => $addressTranslation['title'] ?? null,
            'addressData' => $addressTranslation['data'] ?? [],
            'name' => ArrayHelper::remove($translations, 'name'),
            'attributes' => $attributes,
            'lat' => $this->_building['lat'],
            'lng' => $this->_building['lng'],
            'type' => $this->_building['type'],
            'status' => $this->_building['status'],
        ];
    }
}