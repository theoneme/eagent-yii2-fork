<?php

namespace common\dto\advanced;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\Region;
use yii\helpers\ArrayHelper;

/**
 * Class RegionLightDTO
 * @package common\dto\advanced
 */
class RegionLightDTO implements DTOInterface
{
    /**
     * @var Region
     */
    private $_region;

    /**
     * RegionDTO constructor.
     * @param Region $region
     */
    public function __construct(Region $region)
    {
        $this->_region = $region;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_region->translations);

        return [
            'id' => $this->_region->id,
            'title' => ArrayHelper::remove($currentTranslation, 'title'),
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_region->translations);

        return [
            'id' => $this->_region->id,
            'title' => ArrayHelper::remove($currentTranslation, 'title'),
        ];
    }
}