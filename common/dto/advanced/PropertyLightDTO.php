<?php

namespace common\dto\advanced;

use common\components\CurrencyHelper;
use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AttachmentsMapper;
use common\mappers\PropertyLightAttributesMapper;
use common\mappers\TranslationsMapper;
use common\models\Property;
use common\models\Translation;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PropertyLightDTO
 * @package common\dto\advanced
 */
class PropertyLightDTO implements DTOInterface
{
    /**
     * @var Property
     */
    private $_property;

    /**
     * PropertyLightDTO constructor.
     * @param array $property
     */
    public function __construct($property)
    {
        $this->_property = $property;
    }

    /**
     * @param int $mode
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getFullData()
    {
        $thumbnails = AttachmentsMapper::getMappedData($this->_property['attachments'], AttachmentsMapper::MODE_THUMB);
        $currentTranslation = TranslationsMapper::getMappedData($this->_property['translations']);
        $attributes = PropertyLightAttributesMapper::getMappedData($this->_property['propertyAttributes']);

        return [
            'id' => $this->_property['id'],
            'lat' => $this->_property['lat'],
            'lng' => $this->_property['lng'],
            'price' => CurrencyHelper::convertAndFormat($this->_property['currency_code'], Yii::$app->params['app_currency'], $this->_property['price']),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
            'image' => $thumbnails[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'slug' => ArrayHelper::remove($currentTranslation, Translation::KEY_SLUG),
            'attributes' => $attributes
        ];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getShortData()
    {
//        $thumbnails = AttachmentsMapper::getMappedData($this->_property['attachments'], AttachmentsMapper::MODE_THUMB);
//        $currentTranslation = TranslationsMapper::getMappedData($this->_property['translations']);

        return [
            'id' => $this->_property['id'],
            'lat' => $this->_property['lat'],
            'lng' => $this->_property['lng'],
//            'price' => Yii::$app->currencyLayer->convertAndFormat($this->_property['currency_code'], Yii::$app->params['app_currency'], $this->_property['price']),
//            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
//            'image' => $thumbnails[0] ?? Yii::$app->mediaLayer->getThumb(null),
//            'slug' => $this->_property['slug'],
//            'marker' => Url::to(['/images/marker.png'], true),
        ];
    }
}