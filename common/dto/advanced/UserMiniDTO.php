<?php

namespace common\dto\advanced;

use common\decorators\UserTypeDecorator;
use common\interfaces\DTOInterface;
use common\mappers\ContactsMapper;
use common\models\User;
use common\services\CompanyUserMiniConverter;
use Yii;

/**
 * Class UserMiniDTO
 * @package common\dto\advanced
 */
class UserMiniDTO implements DTOInterface
{
    /**
     * @var User
     */
    private $_user;

    /**
     * UserDTO constructor.
     * @param $user
     */
    public function __construct($user)
    {
        $this->_user = $user;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        if ($this->_user['is_company'] && $this->_user['company']) {
            $companyDTO = new CompanyMiniDTO($this->_user['company']);
            $converter = new CompanyUserMiniConverter();
            return $converter->convertObject([
                'companyData' => $companyDTO->getFullData(),
                'userData' => ['contacts' => ContactsMapper::getMappedData($this->_user['contacts'])]
            ]);
        }
        $avatar = !empty($this->_user['avatar']) ? $this->_user['avatar'] : '/images/agent-no-image.png';
        return [
            'username' => $this->_user['username'],
            'email' => $this->_user['email'],
            'phone' => $this->_user['phone'],
            'type' => UserTypeDecorator::decorate($this->_user['type']),
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar),
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        if ($this->_user['is_company'] && $this->_user['company']) {
            $companyDTO = new CompanyMiniDTO($this->_user['company']);
            $converter = new CompanyUserMiniConverter();
            return $converter->convertObject([
                'companyData' => $companyDTO->getShortData(),
                'userData' => []
            ]);
        }
        $avatar = !empty($this->_user['avatar']) ? $this->_user['avatar'] : '/images/agent-no-image.png';
        return [
            'username' => $this->_user['username'],
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar, 'catalog'),
            'type' => UserTypeDecorator::decorate($this->_user['type']),
        ];
    }
}