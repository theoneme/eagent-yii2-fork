<?php

namespace common\dto\advanced;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\Company;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class CompanyMiniDTO
 * @package common\dto\advanced
 */
class CompanyMiniDTO implements DTOInterface
{
    /**
     * @var Company
     */
    private $_company;

    /**
     * CompanyDTO constructor.
     * @param $company
     */
    public function __construct($company)
    {
        $this->_company = $company;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_company['translations']);

        $logo = !empty($this->_company['logo']) ? $this->_company['logo'] : '/images/agent-no-image.png';

        return [
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'logo' => Yii::$app->mediaLayer->getThumb($logo),
            'type' => $this->_company['type'],
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_company['translations']);

        $logo = !empty($this->_company['logo']) ? $this->_company['logo'] : '/images/agent-no-image.png';

        return [
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'logo' => Yii::$app->mediaLayer->getThumb($logo),
            'type' => $this->_company['type'],
        ];
    }
}