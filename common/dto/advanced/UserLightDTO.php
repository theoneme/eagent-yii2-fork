<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto\advanced;

use common\dto\CompanyDTO;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\User;
use common\services\CompanyUserConverter;
use Yii;

/**
 * Class UserLightDTO
 * @package common\dto\advanced
 */
class UserLightDTO implements DTOInterface
{
    /**
     * @var User
     */
    private $_user;

    /**
     * UserLightDTO constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->_user = $user;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        if ($this->_user->is_company && $this->_user->company) {
            $companyDTO = new CompanyDTO($this->_user->company);
            $converter = new CompanyUserConverter();
            return $converter->convertObject([
                'companyData' => $companyDTO->getFullData(),
                'userData' => [
                    'id' => $this->_user->id,
                    'created_at' => $this->_user->created_at,
                    'status' => $this->_user['status'],
                ]
            ]);
        }
        $translations = TranslationsMapper::getMappedData($this->_user->translations);
        $avatar = !empty($this->_user->avatar) ? $this->_user->avatar : '/images/agent-no-image.png';
        return [
            'id' => $this->_user->id,
            'username' => $this->_user->username,
            'email' => $this->_user->email,
            'phone' => $this->_user->phone,
            'lat' => $this->_user->lat,
            'lng' => $this->_user->lng,
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar),
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_user['id'],
            'lat' => $this->_user['lat'],
            'lng' => $this->_user['lng'],
            'type' => 'user'
        ];
    }
}