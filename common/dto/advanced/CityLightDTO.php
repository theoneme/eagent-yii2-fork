<?php

namespace common\dto\advanced;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\City;
use common\models\Translation;
use yii\helpers\ArrayHelper;

/**
 * Class CityLightDTO
 * @package common\dto\advanced
 */
class CityLightDTO implements DTOInterface
{
    /**
     * @var City
     */
    private $_city;

    /**
     * CityDTO constructor.
     * @param $city
     */
    public function __construct($city)
    {
        $this->_city = $city;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_city['translations']);

        return [
            'id' => $this->_city->id,
            'lat' => $this->_city['lat'],
            'lng' => $this->_city['lng'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_city['translations']);

        return [
            'id' => $this->_city['id'],
            'lat' => $this->_city['lat'],
            'lng' => $this->_city['lng'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
        ];
    }
}