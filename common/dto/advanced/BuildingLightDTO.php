<?php

namespace common\dto\advanced;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AttachmentsMapper;
use common\mappers\PropertyLightAttributesMapper;
use common\mappers\TranslationsMapper;
use common\models\Building;
use common\models\Translation;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingLightDTO
 * @package common\dto\advanced
 */
class BuildingLightDTO implements DTOInterface
{
    /**
     * @var Building
     */
    private $_building;

    /**
     * BuildingLightDTO constructor.
     * @param $building
     */
    public function __construct($building)
    {
        $this->_building = $building;
    }

    /**
     * @param int $mode
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getFullData()
    {
        $thumbnails = AttachmentsMapper::getMappedData($this->_building['attachments'], AttachmentsMapper::MODE_THUMB);
        $currentTranslation = TranslationsMapper::getMappedData($this->_building['translations']);
        $attributes = PropertyLightAttributesMapper::getMappedData($this->_building['buildingAttributes']);

        return [
            'id' => $this->_building['id'],
            'lat' => $this->_building['lat'],
            'lng' => $this->_building['lng'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE)),
            'image' => $thumbnails[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'slug' => $this->_building['slug'],
            'attributes' => $attributes
        ];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getShortData()
    {
        return [
            'id' => $this->_building['id'],
            'lat' => $this->_building['lat'],
            'lng' => $this->_building['lng'],
            'type' => 'building'
        ];
    }
}