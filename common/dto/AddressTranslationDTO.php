<?php

namespace common\dto;

use common\interfaces\DTOInterface;
use common\models\AddressTranslation;

/**
 * Class AddressTranslationDTO
 * @package common\dto
 */
class AddressTranslationDTO implements DTOInterface
{
    /**
     * @var AddressTranslation
     */
    private $_addressTranslation;

    /**
     * AddressTranslationDTO constructor.
     * @param AddressTranslation $addressTranslation
     */
    public function __construct(AddressTranslation $addressTranslation)
    {
        $this->_addressTranslation = $addressTranslation;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        return [
            'lat' => $this->_addressTranslation['lat'],
            'lng' => $this->_addressTranslation['lng'],
            'locale' => $this->_addressTranslation['locale'],
            'title' => $this->_addressTranslation['title'],
            'custodData' => $this->_addressTranslation['customDataArray'],
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'title' => $this->_addressTranslation['title'],
        ];
    }
}