<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\components\CurrencyHelper;
use common\dto\advanced\UserMiniDTO;
use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AddressTranslationsMapper;
use common\mappers\AttachmentsMapper;
use common\mappers\RequestAttributesMapper;
use common\mappers\TranslationsMapper;
use common\models\Request;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class RequestDTO
 * @package common\dto
 */
class RequestDTO implements DTOInterface
{
    /**
     * @var Request
     */
    private $_request;
    /**
     * @var array
     */
    private $_attributesData;
    /**
     * @var array
     */
    private $_attributeValuesData;

    /**
     * RequestDTO constructor.
     * @param Request $request
     * @param array $attributesData
     * @param array $attributeValuesData
     */
    public function __construct(Request $request, array $attributesData = [], array $attributeValuesData = [])
    {
        $this->_request = $request;
        $this->_attributesData = $attributesData;
        $this->_attributeValuesData = $attributeValuesData;
    }

    /**
     * @param int $mode
     * @return array
     * @throws \Throwable
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getFullData()
    {
        $images = AttachmentsMapper::getMappedData($this->_request->attachments);
        $thumbnails = AttachmentsMapper::getMappedData($this->_request->attachments, AttachmentsMapper::MODE_THUMB);
        $translations = TranslationsMapper::getMappedData($this->_request->translations);
        $categoryTranslations = TranslationsMapper::getMappedData($this->_request['category']['translations']);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_request['addressTranslations']);
        $attributes = RequestAttributesMapper::getMappedData([
            'requestAttributes' => $this->_request['requestAttributes'],
            'attributes' => $this->_attributesData,
            'attributeValues' => $this->_attributeValuesData
        ]);

        return [
            'id' => $this->_request->id,
            'user_id' => $this->_request->user_id,
            'category_id' => $this->_request->category_id,
            'slug' => $this->_request->slug,
            'type' => $this->_request->type,
            'locale' => $this->_request->locale,
            'images' => $images,
            'thumbnails' => $thumbnails,
            'image' => $images[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'description' => ArrayHelper::remove($translations, 'description'),
            'translations' => $translations,
            'lat' => $this->_request->lat,
            'lng' => $this->_request->lng,
            'price' => $this->_request->contract_price === 0
                ? CurrencyHelper::convertAndFormat($this->_request->currency_code, Yii::$app->params['app_currency'], $this->_request->price)
                : Yii::t('catalog', 'Contract Price'),
            'raw_price' => CurrencyHelper::convert($this->_request->currency_code, Yii::$app->params['app_currency'], $this->_request->price),
            'source_price' => $this->_request['price'],
            'currency_code' => $this->_request->currency_code,
            'created_at' => $this->_request->created_at,
            'address' => $addressTranslation['title'] ?? null,
            'addressData' => $addressTranslation['data'] ?? [],
            'attributes' => $attributes,
            'status' => $this->_request['status'],
            'user' => (new UserMiniDTO($this->_request['user']))->getFullData(),
            'category' => $categoryTranslations,
        ];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getShortData()
    {
        $images = AttachmentsMapper::getMappedData($this->_request->attachments, AttachmentsMapper::MODE_THUMB);
        $translations = TranslationsMapper::getMappedData($this->_request->translations);

        return [
            'id' => $this->_request->id,
            'slug' => $this->_request->slug,
            'image' => $images[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'description' => ArrayHelper::remove($translations, 'description'),
            'lat' => $this->_request->lat,
            'lng' => $this->_request->lng,
            'created_at' => $this->_request->created_at,
            'updated_at' => $this->_request->updated_at,
            'price' => $this->_request->contract_price === 0
                ? CurrencyHelper::convertAndFormat($this->_request->currency_code, Yii::$app->params['app_currency'], $this->_request->price)
                : Yii::t('catalog', 'Contract Price'),
            'user' => (new UserMiniDTO($this->_request['user']))->getShortData(),
            'user_id' => $this->_request->user_id,
            'status' => $this->_request['status'],
        ];
    }
}