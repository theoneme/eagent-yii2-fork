<?php

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\District;
use yii\helpers\ArrayHelper;

/**
 * Class DistrictDTO
 * @package common\dto
 */
class DistrictDTO implements DTOInterface
{
    /**
     * @var District
     */
    private $_district;

    /**
     * DistrictDTO constructor.
     * @param $district
     */
    public function __construct($district)
    {
        $this->_district = $district;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_district['translations']);
        $translations = TranslationsMapper::getMappedData($this->_district['translations'], TranslationsMapper::MODE_FULL);
        $cityTranslations = TranslationsMapper::getMappedData($this->_district['city']['translations']);

        return [
            'id' => $this->_district['id'],
            'city_id' => $this->_district['city_id'],
            'slug' => $this->_district['slug'],
            'polygonArray' => $this->_district['polygonArray'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'translations' => $translations,
            'city' => $cityTranslations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $cityTranslations = TranslationsMapper::getMappedData($this->_district['city']['translations']);
        $translations = TranslationsMapper::getMappedData($this->_district['translations']);

        return [
            'id' => $this->_district['id'],
            'city_id' => $this->_district['city_id'],
            'slug' => $this->_district['slug'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'translations' => $translations,
            'city' => $cityTranslations,
        ];
    }
}