<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\Category;
use yii\helpers\ArrayHelper;

/**
 * Class CategoryDTO
 * @package common\dto
 */
class CategoryDTO implements DTOInterface
{
    /**
     * @var Category
     */
    private $_category;

    /**
     * CategoryDTO constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->_category = $category;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_category['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_category['translations']);

        return [
            'id' => $this->_category['id'],
            'root' => $this->_category['root'],
            'lft' => $this->_category['lft'],
            'rgt' => $this->_category['rgt'],
            'slug' => ArrayHelper::remove($currentTranslation, 'slug'),
            'title' => ArrayHelper::remove($currentTranslation, 'title'),
            'description' => ArrayHelper::remove($currentTranslation, 'description'),
            'customData' => $this->_category->customDataArray,
            'translations' => $translations
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_category['translations']);

        return [
            'id' => $this->_category['id'],
            'root' => $this->_category['root'],
            'lft' => $this->_category['lft'],
            'rgt' => $this->_category['rgt'],
            'lvl' => $this->_category['lvl'],
            'slug' => ArrayHelper::remove($translations, 'slug'),
            'title' => ArrayHelper::remove($translations, 'title'),
            'customData' => $this->_category->customDataArray,
        ];
    }
}