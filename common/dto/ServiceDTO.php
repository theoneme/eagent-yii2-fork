<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\components\CurrencyHelper;
use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\AttachmentsMapper;
use common\mappers\TranslationsMapper;
use common\models\Service;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class ServiceDTO
 * @package common\dto
 */
class ServiceDTO implements DTOInterface
{
    /**
     * @var Service
     */
    private $_service;

    /**
     * ServiceDTO constructor.
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->_service = $service;
    }

    /**
     * @param int $mode
     * @return array
     * @throws \Throwable
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getFullData()
    {
        $images = AttachmentsMapper::getMappedData($this->_service->attachments);
//        $thumbnails = AttachmentsMapper::getMappedData($this->_service->attachments, AttachmentsMapper::MODE_THUMB);
        $translations = TranslationsMapper::getMappedData($this->_service->translations);

        return [
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'image' => $images[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'description' => ArrayHelper::remove($translations, 'description'),
            'translations' => $translations,
            'price' => CurrencyHelper::convertAndFormat($this->_service->currency_code, Yii::$app->params['app_currency'], $this->_service->price),
            'raw_price' => CurrencyHelper::convert($this->_service->currency_code, Yii::$app->params['app_currency'], $this->_service->price),
            'currency_code' => $this->_service->currency_code,
            'created_at' => $this->_service->created_at,
        ];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function getShortData()
    {
        $images = AttachmentsMapper::getMappedData($this->_service->attachments, AttachmentsMapper::MODE_THUMB);
        $translations = TranslationsMapper::getMappedData($this->_service->translations);

        return [
            'id' => $this->_service->id,
//            'slug' => $this->_service->slug,
            'image' => $images[0] ?? Yii::$app->mediaLayer->getThumb(null),
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'description' => ArrayHelper::remove($translations, 'description'),
//            'lat' => $this->_service->lat,
//            'lng' => $this->_service->lng,
            'created_at' => $this->_service->created_at,
            'updated_at' => $this->_service->updated_at,
            'price' => CurrencyHelper::convertAndFormat($this->_service->currency_code, Yii::$app->params['app_currency'], $this->_service->price),
//            'user' => [
//                'avatar' => Yii::$app->mediaLayer->getThumb($this->_service->user['avatar']),
//                'username' => $this->_service->user['username'],
//            ]
        ];
    }
}