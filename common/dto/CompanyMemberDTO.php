<?php

namespace common\dto;

use common\interfaces\DTOInterface;
use common\models\CompanyMember;
use Yii;

/**
 * Class CompanyMemberDTO
 * @package common\dto
 */
class CompanyMemberDTO implements DTOInterface
{
    /**
     * @var CompanyMember
     */
    private $_companyMember;

    /**
     * CompanyDTO constructor.
     * @param $company
     */
    public function __construct($company)
    {
        $this->_companyMember = $company;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        return [
            'id' => $this->_companyMember['id'],
            'user_id' => $this->_companyMember['user_id'],
            'company_id' => $this->_companyMember['company_id'],
            'role' => $this->_companyMember['role'],
            'status' => $this->_companyMember['status'],
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_companyMember['id'],
            'user_id' => $this->_companyMember['user_id'],
            'company_id' => $this->_companyMember['company_id'],
            'role' => $this->_companyMember['role'],
            'status' => $this->_companyMember['status'],
            'username' => $this->_companyMember['user']['username'],
            'avatar' => Yii::$app->mediaLayer->getThumb($this->_companyMember['user']['avatar'], 'catalog')
        ];
    }
}