<?php

namespace common\dto;

use common\helpers\UtilityHelper;
use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\MicroDistrict;
use yii\helpers\ArrayHelper;

/**
 * Class MicroDistrictDTO
 * @package common\dto
 */
class MicroDistrictDTO implements DTOInterface
{
    /**
     * @var MicroDistrict
     */
    private $_microDistrict;

    /**
     * MicroDistrictDTO constructor.
     * @param $microDistrict
     */
    public function __construct($microDistrict)
    {
        $this->_microDistrict = $microDistrict;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $currentTranslation = TranslationsMapper::getMappedData($this->_microDistrict['translations']);
        $translations = TranslationsMapper::getMappedData($this->_microDistrict['translations'], TranslationsMapper::MODE_FULL);
        $cityTranslations = TranslationsMapper::getMappedData($this->_microDistrict['city']['translations']);
        $districtTranslations = !empty($this->_microDistrict['district']) ? TranslationsMapper::getMappedData($this->_microDistrict['district']['translations']) : [];

        return [
            'id' => $this->_microDistrict['id'],
            'slug' => $this->_microDistrict['slug'],
            'city_id' => $this->_microDistrict['city_id'],
            'district_id' => $this->_microDistrict['district_id'],
            'polygonArray' => $this->_microDistrict['polygonArray'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($currentTranslation, 'title')),
            'translations' => $translations,
            'city' => $cityTranslations,
            'district' => $districtTranslations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_microDistrict['translations']);
        $cityTranslations = TranslationsMapper::getMappedData($this->_microDistrict['city']['translations']);
        $districtTranslations = !empty($this->_microDistrict['district']) ? TranslationsMapper::getMappedData($this->_microDistrict['district']['translations']) : [];

        return [
            'id' => $this->_microDistrict['id'],
            'city_id' => $this->_microDistrict['city_id'],
            'district_id' => $this->_microDistrict['district_id'],
            'title' => UtilityHelper::upperFirstLetter(ArrayHelper::remove($translations, 'title')),
            'translations' => $translations,
            'city' => $cityTranslations,
            'district' => $districtTranslations,
            'slug' => $this->_microDistrict['slug'],
        ];
    }
}