<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\AddressTranslationsMapper;
use common\mappers\ContactsMapper;
use common\mappers\SocialsMapper;
use common\mappers\TranslationsMapper;
use common\models\User;
use Yii;

/**
 * Class UserDTO
 * @package common\dto
 */
class UserDTO implements DTOInterface
{
    /**
     * @var User
     */
    private $_user;

    /**
     * UserDTO constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->_user = $user;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_user->translations);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_user->addressTranslations);
        $avatar = !empty($this->_user->avatar) ? $this->_user->avatar : '/images/agent-no-image.png';
        $contacts = ContactsMapper::getMappedData($this->_user['contacts']);
        $socials = SocialsMapper::getMappedData($this->_user['socials']);

        return [
            'id' => $this->_user->id,
            'username' => $this->_user->username,
            'email' => $this->_user->email,
            'phone' => $this->_user->phone,
            'lat' => $this->_user->lat,
            'lng' => $this->_user->lng,
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar),
            'translations' => $translations,
            'address' => $addressTranslation['title'] ?? '',
            'addressData' => $addressTranslation['data'] ?? [],
            'created_at' => $this->_user->created_at,
            'status' => $this->_user['status'],
            'contacts' => $contacts,
            'socials' => $socials,
            'is_company' => $this->_user['is_company'],
            'ads_allowed' => $this->_user['ads_allowed'],
            'ads_allowed_partners' => $this->_user['ads_allowed_partners'],
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_user->translations);
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->_user->addressTranslations);
        $avatar = !empty($this->_user->avatar) ? $this->_user->avatar : '/images/agent-no-image.png';
        return [
            'id' => $this->_user->id,
            'username' => $this->_user->username,
            'email' => $this->_user->email,
            'phone' => $this->_user->phone,
            'lat' => $this->_user->lat,
            'lng' => $this->_user->lng,
            'avatar' => Yii::$app->mediaLayer->getThumb($avatar, 'catalog'),
            'translations' => $translations,
            'address' => $addressTranslation['title'] ?? '',
            'addressData' => $addressTranslation['data'] ?? [],
            'created_at' => $this->_user->created_at,
            'status' => $this->_user['status'],
        ];
    }
}