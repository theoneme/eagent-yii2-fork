<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\dto\advanced\UserMiniDTO;
use common\interfaces\DTOInterface;
use common\models\Report;

/**
 * Class ReportDTO
 * @package common\dto
 */
class ReportDTO implements DTOInterface
{
    /**
     * @var Report
     */
    private $_report;

    /**
     * RegionDTO constructor.
     * @param Report $report
     */
    public function __construct(Report $report)
    {
        $this->_report = $report;
    }

    /**
     * @param int $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        return [
            'id' => $this->_report->id,
            'from_id' => $this->_report->from_id,
            'created_at' => $this->_report->created_at,
            'from' => (new UserMiniDTO($this->_report['from']))->getFullData(),
            'customData' => $this->_report->customDataArray
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_report->id,
            'created_at' => $this->_report->created_at,
            'customData' => $this->_report->customDataArray
        ];
    }
}