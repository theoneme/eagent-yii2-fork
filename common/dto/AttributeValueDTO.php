<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace common\dto;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use common\models\AttributeValue;
use common\models\Translation;
use yii\helpers\ArrayHelper;

/**
 * Class AttributeValueDTO
 * @package common\dto
 */
class AttributeValueDTO implements DTOInterface
{
    /**
     * @var AttributeValue
     */
    private $_attributeValue;

    /**
     * AttributeValueDTO constructor.
     * @param AttributeValue $attributeValue
     */
    public function __construct(AttributeValue $attributeValue)
    {
        $this->_attributeValue = $attributeValue;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_attributeValue['translations'], TranslationsMapper::MODE_FULL);
        $currentTranslation = TranslationsMapper::getMappedData($this->_attributeValue['translations']);

        return [
            'id' => $this->_attributeValue['id'],
            'alias' => $this->_attributeValue['alias'],
            'status' => $this->_attributeValue['status'],
            'attribute_id' => $this->_attributeValue['attribute_id'],
            'title' => ArrayHelper::remove($currentTranslation, Translation::KEY_TITLE),
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_attributeValue['translations']);
        return [
            'id' => $this->_attributeValue['id'],
            'alias' => $this->_attributeValue['alias'],
            'status' => $this->_attributeValue['status'],
            'attribute_id' => $this->_attributeValue['attribute_id'],
            'translations' => $translations
        ];
    }
}