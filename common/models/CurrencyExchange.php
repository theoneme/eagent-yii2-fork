<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "currency_exchange".
 *
 * @property string $code_from
 * @property string $code_to
 * @property string $ratio
 *
 * @property Currency $codeFrom
 * @property Currency $codeTo
 */
class CurrencyExchange extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency_exchange';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_from', 'code_to'], 'required'],
            [['ratio'], 'number'],
            [['code_from', 'code_to'], 'string', 'max' => 3],
            [['code_from', 'code_to'], 'unique', 'targetAttribute' => ['code_from', 'code_to']],
            [['code_from'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['code_from' => 'code']],
            [['code_to'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['code_to' => 'code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code_from' => Yii::t('model', 'Code From'),
            'code_to' => Yii::t('model', 'Code To'),
            'ratio' => Yii::t('model', 'Ratio'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeFrom()
    {
        return $this->hasOne(Currency::class, ['code' => 'code_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodeTo()
    {
        return $this->hasOne(Currency::class, ['code' => 'code_to']);
    }
}
