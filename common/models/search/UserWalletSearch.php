<?php

namespace common\models\search;

use common\dto\UserWalletDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\UserWallet;
use yii\data\Pagination;

/**
 * Class UserWalletSearch
 * @package common\models\search
 */
class UserWalletSearch extends UserWallet
{
    /**
     * @var RepositoryInterface
     */
    private $_userWalletRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * UserWalletSearch constructor.
     * @param RepositoryInterface $userWalletRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $userWalletRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_userWalletRepository = $userWalletRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->user_id);
            }],
            [['user_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->user_id);
            }],
            [['currency_code'], 'each', 'rule' => ['string'], 'when' => function ($model) {
                return is_array($model->currency_code);
            }],
            [['currency_code'], 'string', 'when' => function ($model) {
                return !is_array($model->currency_code);
            }],
        ];
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_userWalletRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_userWalletRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new UserWalletDTO($value);
            return $dto->getData(UserWalletDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return RepositoryInterface
     */
    protected function initQuery()
    {
        $query = $this->_userWalletRepository->groupBy('user_wallet.id')->joinWith(['histories']);

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }
}
