<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\CountryDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\Country;
use common\models\Translation;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class CountrySearch
 * @package common\models\search
 */
class CountrySearch extends Country
{
    /**
     * @var RepositoryInterface
     */
    private $_countryRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * CountrySearch constructor.
     * @param RepositoryInterface $countryRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $countryRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_countryRepository = $countryRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['code'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_countryRepository);

        /** @var mixed $countriesQuery */
        $countriesQuery = $this->initQuery($params);

        if ($limit !== null) {
            $countriesQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $countriesQuery->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $countriesQuery->countByCriteria($criteria);

            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $countriesQuery->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $countriesQuery->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new CountryDTO($value);
            return $dto->getData(CountryDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return RepositoryInterface
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_countryRepository
            ->groupBy('country.id');

        if (array_key_exists('request', $params)) {
            $query->joinWith(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['country_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        } else {
            $query->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['country_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (array_key_exists('request', $params) && !empty($params['request'])) {
            $criteria = ['and',
                $criteria,
                ['like', 'value', "{$params['request']}%", false],
//                new Expression("MATCH(country_translations.value) AGAINST ('{$params['request']}*' IN BOOLEAN MODE)"),
                ['key' => Translation::KEY_TITLE]
            ];
        }

        return $criteria;
    }
}
