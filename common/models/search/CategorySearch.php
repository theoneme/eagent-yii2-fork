<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 18:30
 */

namespace common\models\search;

use common\dto\CategoryDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\Category;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class CategorySearch
 * @package common\models\search
 */
class CategorySearch extends Category
{
    /**
     * @var RepositoryInterface
     */
    private $_categoryRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * CategorySearch constructor.
     * @param RepositoryInterface $categoryRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $categoryRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_categoryRepository = $categoryRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lvl'], 'integer', 'when' => function ($model) {
                return !is_array($model->lvl);
            }],
            [['lvl'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->lvl);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $this->load($params, '');
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_categoryRepository);

        $parentId = ArrayHelper::remove($params, 'parent_id');
        if ($parentId !== null) {
            $parent = $this->_categoryRepository->findOneByCriteria(['id' => $parentId], true);
            if ($parent) {
                $criteria = ['and', ['category.root' => $parent['root']], ['>', 'category.lft', $parent['lft']], ['<', 'category.rgt', $parent['rgt']], ['category.lvl' => $parent['lvl'] + 1], $criteria];
            }
        }

        /** @var mixed $query */
        $query = $this->_categoryRepository->with(['translations' => function(ActiveQuery $query) {
            return $query->andOnCondition(['category_translations.locale' => [Yii::$app->language, 'en-GB']]);
        }])->groupBy('category.id');

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_categoryRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)
                    ->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new CategoryDTO($value);
            return $dto->getData(CategoryDTO::MODE_SHORT);
        }, $data));

        return $result;
    }
}
