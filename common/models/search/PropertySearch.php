<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\PropertyDTO;
use common\helpers\ConditionHelper;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\Property;
use common\repositories\sql\AttributeRepository;
use frontend\helpers\FilterConditionHelper;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class PropertySearch
 * @package common\models\search
 */
class PropertySearch extends Property
{
    /**
     * @var RepositoryInterface
     */
    protected $_propertyRepository;
    /**
     * @var array
     */
    protected $_config;
    /**
     * @var array
     */
    protected $_propertyOperationToType = [
        self::OPERATION_SALE => self::TYPE_SALE,
        self::OPERATION_RENT => self::TYPE_RENT
    ];

    /**
     * PropertySearch constructor.
     * @param PropertyRepositoryInterface $propertyRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(PropertyRepositoryInterface $propertyRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_propertyRepository = $propertyRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'city_id', 'country_id', 'region_id'], 'integer'],
            ['ads_allowed', 'boolean'],
            ['status', 'in', 'range' => [
                Property::STATUS_PAUSED,
                Property::STATUS_REQUIRES_MODERATION,
                Property::STATUS_REQUIRES_MODIFICATION,
                Property::STATUS_ACTIVE,
                Property::STATUS_SOLD,
                Property::STATUS_DELETED,
            ], 'allowArray' => true],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['building_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->building_id);
            }],
            [['building_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->building_id);
            }],
            [['category_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->category_id);
            }],
            [['category_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->category_id);
            }],
            ['locale', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return [
                'items' => []
            ];
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_propertyRepository);

        $propertiesQuery = $this->initQuery($params);

        if (array_key_exists('operation', $params) && array_key_exists($params['operation'], $this->_propertyOperationToType)) {
            $criteria['type'] = $this->_propertyOperationToType[$params['operation']];
        }

        $params['radius'] = $params['radius'] ?? 100;
        $filterConditionHelper = new FilterConditionHelper();
        $criteria = ['and', $filterConditionHelper->process($params, ['price', 'radius', 'box', 'polygon', 'category']), $criteria];

//        $categoryParams = ArrayHelper::remove($params, 'categories', []);
//        if (!empty($categoryParams)) {
//            /* @var CategoryRepository $categoryRepository */
//            $categoryRepository = Yii::$container->get(CategoryRepositoryInterface::class);
//            $categoriePs = $categoryRepository->select(['lft', 'rgt', 'root'])->joinWith(['translations'])->findManyByCriteria(['category_translations.value' => $categoryParams, 'category_translations.key' => 'slug'], true);
//            if (!empty($categories)) {
//                $categoriesCriteria = ['or'];
//                foreach ($categories as $category) {
//                    $categoriesCriteria[] = ['and', ['category.root' => $category['root']], ['>=', 'category.lft', $category['lft']], ['<=', 'category.rgt', $category['rgt']]];
//                }
//                $criteria = ['and', $categoriesCriteria, $criteria];
//            }
//        }

        /** @var AttributeRepository $attributeRepository */
        $attributeRepository = Yii::$container->get(AttributeRepository::class);
        $availableAttributes = $attributeRepository->select(['id', 'alias'])
            ->indexBy('alias')
            ->findManyByCriteria([
                'alias' => array_keys($params)
            ], true);


        /** @var mixed $propertiesQuery */
        $propertiesQuery = $this->applyQueryParams($propertiesQuery, $availableAttributes, $params);

        if ($limit !== null) {
            $propertiesQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $propertiesQuery->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $propertiesQuery->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['id' => $exclude]]];
        }

        $criteria = $this->improveCriteria($criteria, $params);

        return $this->processSearch($params, $propertiesQuery, $criteria);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $result = [
            'items' => []
        ];

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $query->select(['property.id'], true)->countByCriteria($criteria);
            $query->select(['*'], true);

            unset($params['category_id'], $params['type'], $params['status'], $params['region_id']);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new PropertyDTO($value);
            return $dto->getData(PropertyDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        $query = $this->_propertyRepository
            ->with(['translations' => function ($query) {
                /** @var ActiveQuery $query */
                $query->andOnCondition(['property_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }, 'propertyAttributes', 'attachments', 'addressTranslations' => function ($query) {
                /** @var ActiveQuery $query */
                $query->andOnCondition(['property_address_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }, 'category.translations'])
            ->groupBy('property.id');
        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $query->joinWith(['category']);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }

    /**
     * @param RepositoryInterface $query
     * @param $availableAttributes
     * @param $params
     * @return Query
     */
    protected function applyQueryParams($query, $availableAttributes, $params)
    {
        $paramsToApply = array_intersect_key($params, $availableAttributes);
        foreach ($paramsToApply as $key => $param) {
            if (array_key_exists('min', (array)$param) || array_key_exists('max', (array)$param)) {
                $query = $this->applyRangeAttributeQuery($query, $key, $param);
            } else {
                $query = $this->applyAttributeQuery($query, $key, $param);
            }
        }

        return $query;
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyAttributeQuery($query, $key, $param)
    {
        $relationAlias = ConditionHelper::getRelationName($this->_propertyRepository, 'property_attribute');

        $condition = [
            "{$relationAlias}.entity_alias" => $key,
            "{$relationAlias}.value_alias" => $param
        ];

        return $query->relationExists($relationAlias, 'property_id', $condition);
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyRangeAttributeQuery($query, $key, $param)
    {
        $relationAlias = ConditionHelper::getRelationName($this->_propertyRepository, 'property_attribute');

        $rangeCondition = ['and', [
            "{$relationAlias}.entity_alias" => $key
        ]];

        if (array_key_exists('min', $param)) {
            $rangeCondition[] = ['>=', "{$relationAlias}.value_number", (float)$param['min']];
        }
        if (array_key_exists('max', $param)) {
            $rangeCondition[] = ['<=', "{$relationAlias}.value_number", (float)$param['max']];
        }

        return $query->relationExists($relationAlias, 'property_id', $rangeCondition);
    }
}
