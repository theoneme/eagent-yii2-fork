<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\AttributeValueDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\AttributeValue;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class AttributeValueSearch
 * @package common\models\search
 */
class AttributeValueSearch extends AttributeValue
{
    /**
     * @var RepositoryInterface
     */
    private $_attributeValueRepository;
    /**
     * @var array
     */
    private $_config;
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $except_id;

    /**
     * AttributeValueSearch constructor.
     * @param RepositoryInterface $attributeValueRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $attributeValueRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_attributeValueRepository = $attributeValueRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'alias', 'attribute_id', 'except_id', 'status'], 'safe'],
            ['title', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes, function ($var) {
            return $var !== null && $var !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_attributeValueRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_attributeValueRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)
                    ->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria, false);

        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new AttributeValueDTO($value);
            return $dto->getData(AttributeValueDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return RepositoryInterface
     */
    protected function initQuery()
    {
        $query = $this->_attributeValueRepository
            ->groupBy('attribute_value.id');

        if (!empty($this->title)) {
            $query->joinWith(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['attr_value_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }]);
        } else {
            $query->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['attr_value_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }]);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @return array
     */
    protected function improveCriteria($criteria)
    {
        if (!empty($this->title)) {
            $criteria = ['and',
                ['like', 'attr_value_translations.value', $this->title],
                $criteria
            ];
        }

        if (!empty($this->except_id)) {
            $criteria = ['and',
                ['not', ['attribute_value.id' => $this->except_id]],
                $criteria
            ];
        }

        return $criteria;
    }
}
