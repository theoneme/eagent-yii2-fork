<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\CompanyMemberDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\CompanyMemberRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\CompanyMember;
use yii\base\Model;
use yii\data\Pagination;

/**
 * Class CompanyMemberSearch
 * @package common\models\search
 */
class CompanyMemberSearch extends CompanyMember
{
    /**
     * @var RepositoryInterface
     */
    protected $_companyMemberRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * RegionSearch constructor.
     * @param CompanyMemberRepositoryInterface $companyMemberRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(CompanyMemberRepositoryInterface $companyMemberRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_companyMemberRepository = $companyMemberRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id'], 'integer'],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];
        $this->load($params, '');
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_companyMemberRepository);

        /** @var mixed $companiesQuery */
        $companiesQuery = $this->_companyMemberRepository
            ->with(['company.translations', 'user.translations'])
            ->groupBy('company_member.id');

        if ($limit !== null) {
            $companiesQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $companiesQuery->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $companiesQuery->orderBy($orderBy);
        }
        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_companyMemberRepository->countByCriteria($criteria);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $companiesQuery->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $companiesQuery->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new CompanyMemberDTO($value);
            return $dto->getData(CompanyMemberDTO::MODE_SHORT);
        }, $data));

        return $result;
    }
}
