<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\PageDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\Page;
use common\models\Translation;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class PageSearch
 * @package common\models\search
 */
class PageSearch extends Page
{
    /**
     * @var RepositoryInterface
     */
    protected $_pageRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * RegionSearch constructor.
     * @param RepositoryInterface $pageRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $pageRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_pageRepository = $pageRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];
        $this->load($params, '');
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_pageRepository);

        if (array_key_exists('request', $params) && !empty($params['request'])) {
            $criteria = ['and',
                $criteria,
//              ['like', 'value', "{$params['request']}%", false],
                new Expression("MATCH(page_translations.value) AGAINST ('{$params['request']}*' IN BOOLEAN MODE)"),
                ['key' => Translation::KEY_TITLE]
            ];
        }

        /** @var mixed $citiesQuery */
        $citiesQuery = $this->_pageRepository
            ->joinWith(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['page_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->groupBy('page.id');

        if ($limit !== null) {
            $citiesQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $citiesQuery->indexBy($indexBy);
        }
        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_pageRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $citiesQuery->offset($pages->offset)
                    ->limit($pages->limit);
            }
        }

        $data = $citiesQuery->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new PageDTO($value);
            return $dto->getData(PageDTO::MODE_SHORT);
        }, $data));

        return $result;
    }
}
