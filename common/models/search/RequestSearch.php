<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\RequestDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\Request;
use yii\base\Model;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * Class RequestSearch
 * @package common\models\search
 */
class RequestSearch extends Request
{
    /**
     * @var RepositoryInterface
     */
    private $_requestRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * RequestSearch constructor.
     * @param RepositoryInterface $requestRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $requestRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_requestRepository = $requestRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'category_id'], 'integer'],
            ['status', 'in', 'range' => [
                Request::STATUS_ACTIVE,
                Request::STATUS_REQUIRES_MODERATION,
                Request::STATUS_REQUIRES_MODIFICATION
            ], 'allowArray' => true],
            [['category_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->category_id);
            }],
            [['category_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->category_id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \ReflectionException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_requestRepository);

        /** @var mixed $jobQuery */
        $jobQuery = $this->initQuery($params);

        $root = ArrayHelper::remove($params, 'root');
        $lft = ArrayHelper::remove($params, 'lft');
        $rgt = ArrayHelper::remove($params, 'rgt');
        if ($root !== null && $lft !== null && $rgt !== null) {
            $criteria = ['and', ['category.root' => $root], ['>=', 'category.lft', $lft], ['<=', 'category.rgt', $rgt], $criteria];
        }

        if ($limit !== null) {
            $jobQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $jobQuery->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $jobQuery->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['request.id' => $exclude]]];
        }

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $jobQuery->countByCriteria($criteria);

            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $jobQuery->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $jobQuery->findManyByCriteria($criteria);

        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new RequestDTO($value);
            return $dto->getData(RequestDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_requestRepository
            ->with(['translations', 'user', 'attachments'])
            ->joinWith(['exchange'], false)
            ->groupBy('request.id');
        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $query->joinWith(['category']);
        }

        return $query;
    }
}
