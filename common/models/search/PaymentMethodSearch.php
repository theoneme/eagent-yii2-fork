<?php

namespace common\models\search;

use common\dto\PaymentMethodDTO;
use common\dto\UserWalletDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\UserWallet;
use yii\data\Pagination;

/**
 * Class PaymentMethodSearch
 * @package common\models\search
 */
class PaymentMethodSearch extends UserWallet
{
    /**
     * @var RepositoryInterface
     */
    private $_paymentMethodRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * PaymentMethodSearch constructor.
     * @param RepositoryInterface $paymentMethodRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $paymentMethodRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_paymentMethodRepository = $paymentMethodRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias'], 'each', 'rule' => ['string'], 'when' => function ($model) {
                return is_array($model->alias);
            }],
            [['alias'], 'string', 'when' => function ($model) {
                return !is_array($model->alias);
            }],
            [['enabled'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_paymentMethodRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_paymentMethodRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new PaymentMethodDTO($value);
            return $dto->getData(PaymentMethodDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return RepositoryInterface
     */
    protected function initQuery()
    {
        $query = $this->_paymentMethodRepository->groupBy('payment_method.id')->joinWith(['histories']);

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }
}
