<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search\elastic;

use common\dto\BuildingDTO;
use common\interfaces\repositories\BuildingRepositoryInterface;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class BuildingSearch
 * @package common\models\search\elastic
 */
class BuildingSearch extends \common\models\search\BuildingSearch
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        return $this->_buildingRepository
            ->select(['id'], true);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $orderBy = $this->_config['orderBy'] ?? null;
        $page = (int)($params['page'] ?? 1);
        --$page;
        $result = [
            'items' => []
        ];

        $data = $query->limit($pageSize)->offset($pageSize * $page)->findManyByCriteria($criteria, true);

        $count = $data['hits']['total'] ?? 0;
        $ids = array_map(function ($value) {
            return (int)$value['_source']['id'];
        }, $data['hits']['hits']);

        /** @var BuildingRepositoryInterface $repository */
        $repository = Yii::$container->get(BuildingRepositoryInterface::class);
        $itemsQuery = $repository
            ->with(['translations' => function (ActiveQuery $query) {
                $query->andOnCondition(['building_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }, 'buildingAttributes', 'attachments', 'addressTranslations' => function (ActiveQuery $query) {
                $query->andOnCondition(['building_address_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->groupBy('building.id');

        if ($orderBy !== null) {
            $itemsQuery->orderBy($orderBy);
        }

        $items = $itemsQuery->findManyByCriteria(['id' => $ids], true);

        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new BuildingDTO($value);
            return $dto->getData(BuildingDTO::MODE_SHORT);
        }, $items));

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            unset($params['type'], $params['status'], $params['region_id']);
            $pages = new Pagination([
                'totalCount' => $count,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
        }

        return $result;
    }
}
