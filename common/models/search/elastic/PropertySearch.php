<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search\elastic;

use common\dto\PropertyDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\PropertyRepositoryInterface;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class PropertySearch
 * @package common\models\search\elastic
 */
class PropertySearch extends \common\models\search\PropertySearch
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        return $this->_propertyRepository
            ->select(['id'], true);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $orderBy = $this->_config['orderBy'] ?? null;
        $page = (int)($params['page'] ?? 1);
        --$page;
        $result = [
            'items' => []
        ];

        $data = $query->limit($pageSize)->offset($pageSize * $page)->findManyByCriteria($criteria, true);

        $count = $data['hits']['total'] ?? 0;
        $ids = array_map(function ($value) {
            return (int)$value['_source']['id'];
        }, $data['hits']['hits']);

        /** @var PropertyRepositoryInterface $repository */
        $repository = Yii::$container->get(PropertyRepositoryInterface::class);
        $itemsQuery = $repository
            ->with(['translations' => function (ActiveQuery $query) {
                $query->andOnCondition(['property_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }, 'propertyAttributes', 'attachments', 'addressTranslations' => function (ActiveQuery $query) {
                $query->andOnCondition(['property_address_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }, 'category.translations'])
            ->groupBy('property.id');
        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $query->joinWith(['category']);
        }

        if ($orderBy !== null) {
            $itemsQuery->orderBy($orderBy);
        }

        $items = $itemsQuery->findManyByCriteria(['id' => $ids], true);

        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new PropertyDTO($value);
            return $dto->getData(PropertyDTO::MODE_SHORT);
        }, $items));

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            unset($params['category_id'], $params['type'], $params['status'], $params['region_id']);
            $pages = new Pagination([
                'totalCount' => $count,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
        }

        return $result;
    }
}
