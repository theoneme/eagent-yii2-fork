<?php

namespace common\models\search;

use common\dto\BuildingDTO;
use common\helpers\ConditionHelper;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\BuildingRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\Building;
use common\models\Translation;
use common\repositories\sql\AttributeRepository;
use frontend\helpers\FilterConditionHelper;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class BuildingSearch
 * @package common\models\search
 */
class BuildingSearch extends Building
{
    /**
     * @var RepositoryInterface
     */
    protected $_buildingRepository;
    /**
     * @var array
     */
    protected $_config;
    /**
     * @var array
     */
    protected $_typeToType = [
        self::TYPE_DEFAULT_TEXT => self::TYPE_DEFAULT,
        self::TYPE_NEW_CONSTRUCTION_TEXT => self::TYPE_NEW_CONSTRUCTION
    ];

    /**
     * BuildingSearch constructor.
     * @param BuildingRepositoryInterface $buildingRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(BuildingRepositoryInterface $buildingRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_buildingRepository = $buildingRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'country_id', 'city_id'], 'integer'],
            ['ads_allowed', 'boolean'],
            ['status', 'in', 'range' => [
                Building::STATUS_PAUSED,
                Building::STATUS_REQUIRES_MODERATION,
                Building::STATUS_REQUIRES_MODIFICATION,
                Building::STATUS_ACTIVE,
                Building::STATUS_DELETED,
            ], 'allowArray' => true],
            ['type', 'in', 'range' => [
                Building::TYPE_DEFAULT,
                Building::TYPE_NEW_CONSTRUCTION,
            ]],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        /** @var AttributeRepository $attributeRepository */
        $attributeRepository = Yii::$container->get(AttributeRepository::class);
        $availableAttributes = $attributeRepository->select(['id', 'alias'])
            ->indexBy('alias')
            ->findManyByCriteria([
                'alias' => array_keys($params)
            ], true);

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== false && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_buildingRepository);

        if (array_key_exists('operation', $params) && array_key_exists($params['operation'], $this->_typeToType)) {
            $criteria['type'] = $this->_typeToType[$params['operation']];
        }

        /** @var mixed $buildingsQuery */
        $buildingsQuery = $this->initQuery();
        $buildingsQuery = $this->applyQueryParams($buildingsQuery, $availableAttributes, $params);

        $filterConditionHelper = new FilterConditionHelper();
        $criteria = ['and', $filterConditionHelper->process($params, ['radius', 'box', 'polygon']), $criteria];

        if ($limit !== null) {
            $buildingsQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $buildingsQuery->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $buildingsQuery->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['building.id' => $exclude]]];
        }

        $criteria = $this->improveCriteria($criteria, $params);

        return $this->processSearch($params, $buildingsQuery, $criteria);
    }

    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $result = [
            'items' => []
        ];

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $query->countByCriteria($criteria);

            unset($params['type'], $params['status'], $params['region_id']);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new BuildingDTO($value);
            return $dto->getData(BuildingDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return mixed
     */
    protected function initQuery()
    {
        $query = $this->_buildingRepository
            ->with(['translations' => function ($query) {
                /** @var ActiveQuery|\yii\elasticsearch\ActiveQuery $query */
                return $query->andOnCondition(['building_translations.key' => [Translation::KEY_TITLE, Translation::KEY_NAME, Translation::KEY_DESCRIPTION]]);
            }, 'buildingAttributes', 'attachments', 'addressTranslations' => function ($query) {
                /** @var ActiveQuery|\yii\elasticsearch\ActiveQuery $query */
                return $query->andOnCondition(['building_address_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->groupBy('building.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }

    /**
     * @param RepositoryInterface $query
     * @param $availableAttributes
     * @param $params
     * @return Query
     */
    protected function applyQueryParams($query, $availableAttributes, $params)
    {
        $paramsToApply = array_intersect_key($params, $availableAttributes);
        foreach ($paramsToApply as $key => $param) {
            if (array_key_exists('min', (array)$param) || array_key_exists('max', (array)$param)) {
                $query = $this->applyRangeAttributeQuery($query, $key, $param);
            } else {
                $query = $this->applyAttributeQuery($query, $key, $param);
            }
        }

        return $query;
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyAttributeQuery($query, $key, $param)
    {
        $relationAlias = ConditionHelper::getRelationName($this->_buildingRepository, 'building_attribute');

        $condition = [
            "{$relationAlias}.entity_alias" => $key,
            "{$relationAlias}.value_alias" => $param
        ];

        return $query->relationExists($relationAlias, 'building_id', $condition);
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyRangeAttributeQuery($query, $key, $param)
    {
        $relationAlias = ConditionHelper::getRelationName($this->_buildingRepository, 'building_attribute');

        $rangeCondition = ['and', [
            "{$relationAlias}.entity_alias" => $key
        ]];

        if (array_key_exists('min', $param)) {
            $rangeCondition[] = ['>=', "{$relationAlias}.value_number", (float)$param['min']];
        }
        if (array_key_exists('max', $param)) {
            $rangeCondition[] = ['<=', "{$relationAlias}.value_number", (float)$param['max']];
        }

        return $query->relationExists($relationAlias, 'building_id', $rangeCondition);
    }
}
