<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 18:30
 */

namespace common\models\search;

use common\dto\advanced\UserAdvancedDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\UserRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class UserSearch
 * @package common\models\search
 */
class UserSearch extends User
{
    /**
     * @var integer
     */
    public $minPropertyCount;
    /**
     * @var RepositoryInterface
     */
    protected $_userRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * UserSearch constructor.
     * @param UserRepositoryInterface $userRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(UserRepositoryInterface $userRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_userRepository = $userRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => [self::TYPE_DEFAULT, self::TYPE_REALTOR]],
            ['username', 'string'],
            ['minPropertyCount', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \ReflectionException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes, function ($val) {
            return $val !== null && $val !== '';
        });
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_userRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        $lat = ArrayHelper::remove($params, 'lat');
        $lng = ArrayHelper::remove($params, 'lng');
        $radius = ArrayHelper::remove($params, 'radius') ?? 100;
        if ($lat !== null && $lng !== null) {
            $criteria = ['and', ['radius', $lat, $lng, $radius], $criteria];
        }

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_userRepository->countByCriteria($criteria);

            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new UserAdvancedDTO($value);
            return $dto->getData(UserAdvancedDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return mixed
     */
    protected function initQuery()
    {
        $query = $this->_userRepository
            ->with(['translations', 'addressTranslations' => function (ActiveQuery $query) {
                return $query->andWhere(['user_address_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->groupBy('user.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        $username = ArrayHelper::remove($criteria, 'username');

        if ($username) {
            $criteria = ['and',
                ['like', 'username', $username],
                $criteria
            ];
        }

        return $criteria;
    }
}
