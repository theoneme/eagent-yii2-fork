<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\CompanyDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\CompanyRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\Company;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class CompanySearch
 * @package common\models\search
 */
class CompanySearch extends Company
{
    /**
     * @var RepositoryInterface
     */
    protected $_companyRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * RegionSearch constructor.
     * @param CompanyRepositoryInterface $companyRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(CompanyRepositoryInterface $companyRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_companyRepository = $companyRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            ['status', 'in', 'range' => [
                Company::STATUS_REQUIRES_MODERATION,
                Company::STATUS_REQUIRES_MODIFICATION,
                Company::STATUS_ACTIVE,
                Company::STATUS_DELETED,
            ], 'allowArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_companyRepository);

        /** @var mixed $companiesQuery */
        $companiesQuery = $this->initQuery();

        if ($limit !== null) {
            $companiesQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $companiesQuery->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $companiesQuery->orderBy($orderBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_companyRepository->countByCriteria($criteria);
            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $companiesQuery->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $companiesQuery->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new CompanyDTO($value);
            return $dto->getData(CompanyDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return mixed
     */
    protected function initQuery()
    {
        $query = $this->_companyRepository
            ->joinWith(['companyMembers'])
            ->with(['translations', 'user.contacts', 'addressTranslations' => function (ActiveQuery $query) {
                $query->andOnCondition(['company_address_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }])
            ->groupBy('company.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }
}
