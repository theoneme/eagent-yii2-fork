<?php

namespace common\models\search;

use common\dto\DistrictDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\District;
use common\models\Translation;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class DistrictSearch
 * @package common\models\search
 */
class DistrictSearch extends District
{
    /**
     * @var string
     */
    public $alias;
    /**
     * @var RepositoryInterface
     */
    protected $_districtRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * DistrictSearch constructor.
     * @param RepositoryInterface $districtRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $districtRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_districtRepository = $districtRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->city_id);
            }],
            [['city_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->city_id);
            }],
        ];
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_districtRepository);

        /** @var mixed $query */
        $query = $this->initQuery($params);

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_districtRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new DistrictDTO($value);
            return $dto->getData(DistrictDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return RepositoryInterface
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_districtRepository
            ->select(['district.id', 'district.city_id', 'district.slug', 'polygon' => new Expression('ST_AsGeoJSON(ST_SwapXY(polygon))')], true)
            ->groupBy('district.id');

        if (array_key_exists('request', $params)) {
            $query->joinWith(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['district_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        } else {
            $query->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['district_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        }
        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (array_key_exists('request', $params) && !empty($params['request'])) {
            $criteria = ['and',
                $criteria,
                ['like', 'district_translations.value', "{$params['request']}%", false],
//                new Expression("MATCH(district_translations.value) AGAINST ('{$params['request']}*' IN BOOLEAN MODE)"),
                ['district_translations.key' => Translation::KEY_TITLE]
            ];
        }
        return $criteria;
    }
}
