<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\AttributeDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\Attribute;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;

/**
 * Class AttributeSearch
 * @package common\models\search
 */
class AttributeSearch extends Attribute
{
    /**
     * @var RepositoryInterface
     */
    private $_attributeRepository;
    /**
     * @var array
     */
    private $_config;
    /**
     * @var string
     */
    public $title;

    /**
     * AttributeSearch constructor.
     * @param RepositoryInterface $attributeRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $attributeRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_attributeRepository = $attributeRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'string'],

            [['alias'], 'each', 'rule' => ['string'], 'when' => function ($model) {
                return is_array($model->alias);
            }],
            [['alias'], 'string', 'when' => function ($model) {
                return !is_array($model->alias);
            }],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_attributeRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_attributeRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new AttributeDTO($value);
            return $dto->getData(AttributeDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return RepositoryInterface
     */
    protected function initQuery()
    {
        $query = $this->_attributeRepository
            ->groupBy('attribute.id');

        if (!empty($this->title)) {
            $query->joinWith(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['attribute_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }]);
        } else {
            $query->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['attribute_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }]);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (!empty($this->title)) {
            $criteria = ['and',
                ['like', 'attribute_translations.value', $this->title],
                $criteria
            ];
        }

        if (!empty($this->except_id)) {
            $criteria = ['and',
                ['not', ['attribute.id' => $this->except_id]],
                $criteria
            ];
        }

        return $criteria;
    }
}
