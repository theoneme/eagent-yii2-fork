<?php

namespace common\models\search;

use common\dto\WalletHistoryDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\UserWallet;
use yii\data\Pagination;

/**
 * Class WalletHistorySearch
 * @package common\models\search
 */
class WalletHistorySearch extends UserWallet
{
    /**
     * @var RepositoryInterface
     */
    private $_walletHistoryRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * WalletHistorySearch constructor.
     * @param RepositoryInterface $walletHistoryRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $walletHistoryRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_walletHistoryRepository = $walletHistoryRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->user_id);
            }],
            [['user_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->user_id);
            }],
            [['payment_id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->payment_id);
            }],
            [['payment_id'], 'integer', 'when' => function ($model) {
                return !is_array($model->payment_id);
            }],
            [['type'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->type);
            }],
            [['type'], 'integer', 'when' => function ($model) {
                return !is_array($model->type);
            }],
            [['currency_code'], 'each', 'rule' => ['string'], 'when' => function ($model) {
                return is_array($model->currency_code);
            }],
            [['currency_code'], 'string', 'when' => function ($model) {
                return !is_array($model->currency_code);
            }],
        ];
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_walletHistoryRepository);

        /** @var mixed $query */
        $query = $this->initQuery();

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_walletHistoryRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $query->offset($pages->offset)->limit($pages->limit);
            }
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new WalletHistoryDTO($value);
            return $dto->getData(WalletHistoryDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @return RepositoryInterface
     */
    protected function initQuery()
    {
        $query = $this->_walletHistoryRepository->groupBy('wallet_history.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        return $criteria;
    }
}
