<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\RegionDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\RegionRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\Region;
use common\models\Translation;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class RegionSearch
 * @package common\models\search
 */
class RegionSearch extends Region
{
    /**
     * @var RepositoryInterface
     */
    protected $_regionRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * RegionSearch constructor.
     * @param RegionRepositoryInterface $regionRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RegionRepositoryInterface $regionRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_regionRepository = $regionRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id'], 'integer'],
            [['id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_regionRepository);

        /** @var mixed $regionsQuery */
        $regionsQuery = $this->initQuery($params);

        if ($limit !== null) {
            $regionsQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $regionsQuery->indexBy($indexBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_regionRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $regionsQuery->offset($pages->offset)
                    ->limit($pages->limit);
            }
        }

        $data = $regionsQuery->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new RegionDTO($value);
            return $dto->getData(RegionDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return RegionRepositoryInterface|RepositoryInterface
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_regionRepository
            ->groupBy('region.id');

        if (array_key_exists('request', $params)) {
            $query->joinWith(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['region_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        } else {
            $query->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['region_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (array_key_exists('request', $params) && !empty($params['request'])) {
            $criteria = ['and',
                $criteria,
                ['like', 'value', "{$params['request']}%", false],
//                new Expression("MATCH(region_translations.value) AGAINST ('{$params['request']}*' IN BOOLEAN MODE)"),
                ['key' => Translation::KEY_TITLE]
            ];
        }

        return $criteria;
    }
}
