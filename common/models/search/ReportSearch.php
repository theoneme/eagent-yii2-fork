<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\models\Report;
use yii\base\Model;

/**
 * Class CitySearch
 * @package common\models\search
 */
class ReportSearch extends Report
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return void
     */
    public function search($params)
    {
        // TODO implement this method
    }
}
