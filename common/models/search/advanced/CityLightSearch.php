<?php

namespace common\models\search\advanced;

use common\dto\advanced\CityLightDTO;
use common\helpers\UtilityHelper;
use common\models\search\CitySearch;
use common\models\Translation;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class CityLightSearch
 * @package common\models\search\advanced
 */
class CityLightSearch extends CitySearch
{
    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];
        $this->load($params, '');
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_cityRepository);

        if (array_key_exists('request', $params) && !empty($params['request'])) {
            $criteria = ['and',
                $criteria,
                ['like', 'value', "{$params['request']}%", false],
//                new Expression("MATCH(city_translations.value) AGAINST ('{$params['request']}*' IN BOOLEAN MODE)"),
                ['key' => Translation::KEY_TITLE]
            ];
        }

        /** @var mixed $citiesQuery */
        $citiesQuery = $this->_cityRepository
            ->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['city_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }])
            ->groupBy('city.id');

        $lat = ArrayHelper::remove($params, 'lat');
        $lng = ArrayHelper::remove($params, 'lng');
        $radius = ArrayHelper::remove($params, 'radius') ?? 100;
        if ($lat !== null && $lng !== null) {
            $criteria = ['and', ['radius', $lat, $lng, $radius], $criteria];
        }

        $neLat = ArrayHelper::remove($params, 'neLat');
        $neLng = ArrayHelper::remove($params, 'neLng');
        $swLat = ArrayHelper::remove($params, 'swLat');
        $swLng = ArrayHelper::remove($params, 'swLng');
        if ($neLat !== null && $neLng !== null && $swLat !== null && $swLng !== null) {
            $criteria = ['and', ['between', 'lat', $swLat, $neLat], ['between', 'lng', $swLng, $neLng], $criteria];
        }

        if ($limit !== null) {
            $citiesQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $citiesQuery->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $citiesQuery->orderBy($orderBy);
        }

        $data = $citiesQuery->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new CityLightDTO($value);
            return $dto->getData(CityLightDTO::MODE_SHORT);
        }, $data));

        return $result;
    }
}
