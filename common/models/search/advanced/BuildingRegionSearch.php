<?php

namespace common\models\search\advanced;

use common\interfaces\RepositoryInterface;
use common\models\search\BuildingSearch;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingRegionSearch
 * @package common\models\search\advanced
 */
class BuildingRegionSearch extends BuildingSearch
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        return $this->_buildingRepository;
    }

    /**
     * @param RepositoryInterface $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        if ($query instanceof \common\repositories\sql\BuildingRepository) {
            $data = $query
                ->select(['region_id', 'buildingCount' => 'count(building.id)', 'avgLat' => 'avg(lat)', 'avgLng' => 'avg(lng)'], true)
                ->groupBy('region_id')
                ->findManyByCriteria($criteria, true);

            return [
                'items' => $data
            ];
        }

        $data = $query->aggregate([
            'aggs' => [
                'terms' => [
                    'field' => 'region_id',
                    'size' => 200,
                    'min_doc_count' => 1
                ],
                'aggs' => [
                    'avgLat' => [
                        'avg' => [
                            'field' => 'lat',
                        ]
                    ],
                    'avgLng' => [
                        'avg' => [
                            'field' => 'lng',
                        ]
                    ]
                ]
            ]
        ])->limit(0)->findManyByCriteria($criteria);

        return [
            'items' => ArrayHelper::map($data['aggregations']['aggs']['buckets'] ?? [], 'key', function ($var) {
                return [
                    'region_id' => $var['key'],
                    'buildingCount' => $var['doc_count'],
                    'avgLat' => $var['avgLat']['value'],
                    'avgLng' => $var['avgLng']['value'],
                ];
            })
        ];
    }
}
