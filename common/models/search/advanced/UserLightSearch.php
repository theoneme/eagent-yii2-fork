<?php

namespace common\models\search\advanced;

use common\dto\advanced\UserLightDTO;
use common\helpers\UtilityHelper;
use common\models\search\UserSearch;
use yii\helpers\ArrayHelper;

/**
 * Class UserLightSearch
 * @package common\models\search\advanced
 */
class UserLightSearch extends UserSearch
{
    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_userRepository);

        /** @var mixed $query */
        $query = $this->_userRepository
            ->groupBy('user.id');

        $lat = ArrayHelper::remove($params, 'lat');
        $lng = ArrayHelper::remove($params, 'lng');
        $radius = ArrayHelper::remove($params, 'radius') ?? 100;
        if ($lat !== null && $lng !== null) {
            $criteria = ['and', ['radius', $lat, $lng, $radius], $criteria];
        }

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }

        $data = $query->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new UserLightDTO($value);
            return $dto->getData(UserLightDTO::MODE_SHORT);
        }, $data));

        return $result;
    }
}
