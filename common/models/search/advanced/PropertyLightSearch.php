<?php

namespace common\models\search\advanced;

use common\dto\advanced\PropertyLightDTO;
use common\helpers\ConditionHelper;
use common\models\search\PropertySearch;

/**
 * Class PropertyLightSearch
 * @package common\models\search\advanced
 */
class PropertyLightSearch extends PropertySearch
{
    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $result = [
            'items' => []
        ];

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new PropertyLightDTO($value);
            return $dto->getData(PropertyLightDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        $query = $this->_propertyRepository
            ->select([ConditionHelper::getTabledField($this->_propertyRepository, 'property', 'id'), 'lat', 'lng', 'category_id'], true)
            ->groupBy('property.id');
        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $query->joinWith(['category']);
        }

        return $query;
    }
}
