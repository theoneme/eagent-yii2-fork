<?php

namespace common\models\search\advanced;

use common\dto\advanced\BuildingLightDTO;
use common\helpers\ConditionHelper;
use common\models\search\BuildingSearch;

/**
 * Class BuildingLightSearch
 * @package common\models\search\advanced
 */
class BuildingLightSearch extends BuildingSearch
{
    /**
     * @param $params
     * @param mixed $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        $result = [
            'items' => []
        ];

        $data = $query->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new BuildingLightDTO($value);
            return $dto->getData(BuildingLightDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        $query = $this->_buildingRepository
            ->select([ConditionHelper::getTabledField($this->_buildingRepository, 'building', 'id'), 'lat', 'lng'], true)
            ->groupBy('building.id');

        return $query;
    }
}
