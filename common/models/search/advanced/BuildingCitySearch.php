<?php

namespace common\models\search\advanced;

use common\interfaces\RepositoryInterface;
use common\models\search\BuildingSearch;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingCitySearch
 * @package common\models\search\advanced
 */
class BuildingCitySearch extends BuildingSearch
{
    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        return $this->_buildingRepository;
    }

    /**
     * @param RepositoryInterface $query
     * @param $criteria
     * @return array
     */
    protected function processSearch($params, $query, $criteria)
    {
        if ($query instanceof \common\repositories\sql\BuildingRepository) {
            $data = $query
                ->select(['city_id', 'buildingCount' => 'count(building.id)'], true)
                ->groupBy(['city_id'])
                ->findManyByCriteria($criteria, true);

            return [
                'items' => array_map(function ($value) {
                    return $value['buildingCount'];
                }, $data)
            ];
        }

        $data = $query->aggregate([
            'aggs' => [
                'terms' => [
                    'field' => 'city_id',
                    'size' => 200,
                    'min_doc_count' => 1
                ],
            ]
        ])->limit(0)->findManyByCriteria($criteria);

        return [
            'items' => ArrayHelper::map($data['aggregations']['aggs']['buckets'] ?? [], 'key', 'doc_count')
        ];
    }
}
