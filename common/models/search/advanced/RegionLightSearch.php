<?php

namespace common\models\search\advanced;

use common\dto\advanced\RegionLightDTO;
use common\helpers\UtilityHelper;
use common\models\search\RegionSearch;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class RegionLightSearch
 * @package common\models\search\advanced
 */
class RegionLightSearch extends RegionSearch
{
    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }
        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_regionRepository);

        /** @var mixed $regionsQuery */
        $regionsQuery = $this->_regionRepository
            ->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['region_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }])
            ->groupBy('region.id');

        if ($limit !== null) {
            $regionsQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $regionsQuery->indexBy($indexBy);
        }

        $data = $regionsQuery->findManyByCriteria($criteria);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new RegionLightDTO($value);
            return $dto->getData(RegionLightDTO::MODE_SHORT);
        }, $data));

        return $result;
    }
}
