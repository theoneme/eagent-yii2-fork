<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace common\models\search;

use common\dto\CityDTO;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\CityRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\City;
use common\models\Translation;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class CitySearch
 * @package common\models\search
 */
class CitySearch extends City
{
    /**
     * @var RepositoryInterface
     */
    protected $_cityRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * RegionSearch constructor.
     * @param CityRepositoryInterface $cityRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(CityRepositoryInterface $cityRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_cityRepository = $cityRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'region_id'], 'integer'],
            [['is_big'], 'boolean'],
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     * @throws \yii\base\InvalidArgumentException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_cityRepository);

        /** @var mixed $citiesQuery */
        $citiesQuery = $this->initQuery($params);

        $lat = ArrayHelper::remove($params, 'lat');
        $lng = ArrayHelper::remove($params, 'lng');
        if ($lat !== null && $lng !== null) {
            $citiesQuery->select(['distance' => "3959 * ACOS(COS(RADIANS({$lat})) 
                * COS(RADIANS(city.lat)) 
                * COS(RADIANS(city.lng) - RADIANS({$lng})) 
                + SIN(RADIANS({$lat})) 
                * SIN(RADIANS(city.lat)))"
            ])->having('distance > 0')->orderBy(new Expression('distance ASC'));
        }
        if ($limit !== null) {
            $citiesQuery->limit($limit);
        }
        if ($indexBy !== null) {
            $citiesQuery->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $citiesQuery->orderBy($orderBy);
        }

        $criteria = $this->improveCriteria($criteria, $params);

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $this->_cityRepository->countByCriteria($criteria);
            if ($totalCount > $pageSize) {
                $pages = new Pagination([
                    'totalCount' => $totalCount,
                    'pageSize' => $pageSize,
                    'params' => $params
                ]);
                $result['pagination'] = $pages;
                $citiesQuery->offset($pages->offset)
                    ->limit($pages->limit);
            }
        }

        $data = $citiesQuery->findManyByCriteria($criteria, true);
        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new CityDTO($value);
            return $dto->getData(CityDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return CityRepositoryInterface|RepositoryInterface
     */
    protected function initQuery(array $params = [])
    {
        $query = $this->_cityRepository
            ->groupBy('city.id');

        if (array_key_exists('request', $params)) {
            $query->joinWith(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['city_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        } else {
            $query->with(['translations' => function (ActiveQuery $query) {
                return $query->andWhere(['city_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }]);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (array_key_exists('request', $params) && !empty($params['request'])) {
            $criteria = ['and',
                $criteria,
                ['like', 'value', "{$params['request']}%", false],
//                new Expression("MATCH(city_translations.value) AGAINST ('{$params['request']}*' IN BOOLEAN MODE)"),
                ['key' => Translation::KEY_TITLE]
            ];
        }

        return $criteria;
    }
}
