<?php

namespace common\models\search;

use common\dto\ServiceDTO;
use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use common\models\Service;
use yii\base\Model;
use yii\data\Pagination;

/**
 * Class ServiceSearch
 * @package common\models\search
 */
class ServiceSearch extends Service
{
    /**
     * @var RepositoryInterface
     */
    private $_serviceRepository;
    /**
     * @var array
     */
    private $_config;

    /**
     * ServiceSearch constructor.
     * @param RepositoryInterface $serviceRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(RepositoryInterface $serviceRepository, array $c, array $config = [])
    {
        parent::__construct($config);
        $this->_serviceRepository = $serviceRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return array
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $withPagination = $this->_config['pagination'] ?? false;
        $pageSize = $this->_config['perPage'] ?? 20;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $result = [
            'items' => []
        ];

        $this->load($params, '');
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_serviceRepository);

        /** @var mixed $query */
        $query = $this->_serviceRepository
            ->joinWith(['translations', 'attachments'])
            ->groupBy('service.id');

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $query = ['and', $criteria, ['not', ['service.id' => $exclude]]];
        }

        if ($withPagination === true && ($pageSize < $limit || $limit === null)) {
            $totalCount = $query->countByCriteria($criteria);

            $pages = new Pagination([
                'totalCount' => $totalCount,
                'pageSize' => $pageSize,
                'params' => $params
            ]);
            $result['pagination'] = $pages;
            $query->offset($pages->offset)
                ->limit($pages->limit);
        }

        $data = $query->findManyByCriteria($criteria);

        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new ServiceDTO($value);
            return $dto->getData(ServiceDTO::MODE_SHORT);
        }, $data));

        return $result;
    }
}
