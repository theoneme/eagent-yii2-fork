<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "attribute".
 *
 * @property int $id
 * @property string $alias
 * @property int $type
 *
 * @property AttributeValue[] $attributeValues
 * @property CategoryAttribute[] $categoryAttributes
 * @property Category[] $categories
 * @property Translation[] $translations
 * @property PropertyAttribute[] $propertyAttributes
 *
 * @mixin LinkableBehavior
 */
class Attribute extends ActiveRecord
{
    public const TYPE_NUMBER = 0;
    public const TYPE_DROPDOWN = 10;
    public const TYPE_BOOLEAN = 20;
    public const TYPE_STRING = 30;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'type'], 'required'],
            [['alias'], 'unique'],
            [['type'], 'integer'],
            [['alias'], 'string', 'max' => 65],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'type' => Yii::t('model', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValues()
    {
        return $this->hasMany(AttributeValue::class, ['attribute_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation attribute_translations')
            ->andOnCondition(['attribute_translations.entity' => Translation::ENTITY_ATTRIBUTE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryAttributes()
    {
        return $this->hasMany(CategoryAttribute::class, ['attribute_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->viaTable('category_attribute', ['attribute_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAttributes()
    {
        return $this->hasMany(PropertyAttribute::class, ['attribute_id' => 'id']);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach($this->attributeValues as $value) {
            $value->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_ATTRIBUTE, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
