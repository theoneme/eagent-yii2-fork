<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "property_video".
 *
 * @property int $id
 * @property int $property_id
 * @property int $type
 * @property string $content
 *
 * @property Property $property
 */
class PropertyVideo extends \yii\db\ActiveRecord
{
    public const TYPE_EMBED = 0;
    public const TYPE_FILE = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'content'], 'required'],
            [['property_id', 'type'], 'integer'],
            [['content'], 'string', 'max' => 155],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::class, 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'property_id' => Yii::t('model', 'Property ID'),
            'type' => Yii::t('model', 'Type'),
            'content' => Yii::t('model', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::class, ['id' => 'property_id']);
    }
}
