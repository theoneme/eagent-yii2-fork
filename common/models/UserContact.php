<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_contact".
 *
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property string $value
 *
 * @property User $user
 */
class UserContact extends \yii\db\ActiveRecord
{
    public const TYPE_EMAIL = 0;
    public const TYPE_PHONE = 10;
    public const TYPE_WEBSITE = 30;

    public const TYPE_SKYPE = 100;
    public const TYPE_WHATSAPP = 110;
    public const TYPE_VIBER = 120;
    public const TYPE_TELEGRAM = 130;

    public const TYPE_FACEBOOK = 200;
    public const TYPE_VK = 210;
    public const TYPE_TWITTER = 220;
    public const TYPE_GOOGLE = 230;
    public const TYPE_YOUTUBE = 240;
    public const TYPE_OK = 250;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'value'], 'required'],
            [['user_id', 'type'], 'integer'],
            [['value'], 'string', 'max' => 150],
            ['type', 'in', 'range' => [
                self::TYPE_EMAIL => 0,
                self::TYPE_PHONE => 10,
                self::TYPE_WEBSITE => 30,
                self::TYPE_SKYPE => 100,
                self::TYPE_WHATSAPP => 110,
                self::TYPE_VIBER => 120,
                self::TYPE_TELEGRAM => 130,
                self::TYPE_FACEBOOK => 200,
                self::TYPE_VK => 210,
                self::TYPE_TWITTER => 220,
                self::TYPE_GOOGLE => 230,
                self::TYPE_YOUTUBE => 240,
                self::TYPE_OK => 250,
            ]],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'type' => Yii::t('model', 'Type'),
            'value' => Yii::t('model', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
