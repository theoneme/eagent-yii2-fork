<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use common\behaviors\LinkableBehavior;
use common\models\query\CategoryQuery;
use creocoder\nestedsets\NestedSetsBehavior;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property int $root
 * @property string $custom_data
 * @property string $attribute_group_alias
 * @property string $form_group
 *
 * @property CategoryAttribute[] $categoryAttributes
 * @property Attribute[] $connectedAttributes
 * @property Property[] $properties
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 * @mixin NestedSetsBehavior
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['lft', 'rgt', 'lvl', 'root'], 'required'],
            [['lft', 'rgt', 'lvl', 'root'], 'integer'],
            ['attribute_group_alias', 'string'],
            [['customDataArray'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'tree' => [
                'class' => NestedSetsBehavior::class,
                'depthAttribute' => 'lvl',
                'treeAttribute' => 'root',
            ],
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'lft' => Yii::t('model', 'Lft'),
            'rgt' => Yii::t('model', 'Rgt'),
            'lvl' => Yii::t('model', 'Lvl'),
            'root' => Yii::t('model', 'Root'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryAttributes()
    {
        return $this->hasMany(CategoryAttribute::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConnectedAttributes()
    {
        return $this->hasMany(Attribute::class, ['id' => 'attribute_id'])->viaTable('category_attribute', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::class, ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation category_translations')
            ->andOnCondition(['category_translations.entity' => Translation::ENTITY_CATEGORY]);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        Translation::deleteAll(['entity' => Translation::ENTITY_CATEGORY, 'entity_id' => $this->id]);
    }
}
