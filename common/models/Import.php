<?php

namespace common\models;

/**
 * This is the model class for table "import".
 *
 * @property int $id
 * @property string $source
 * @property string $entity
 * @property int $entity_id
 *
 * @property Flag[] $flags
 */
class Import extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'import';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['source', 'entity', 'origin'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source' => 'Source',
            'entity' => 'Entity',
            'entity_id' => 'Entity ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlags()
    {
        return $this->hasMany(Flag::class, ['entity_id' => 'id'])->onCondition(['flag.entity' => 'import']);
    }

    /**
     * @throws \Throwable
     */
    public function beforeDelete()
    {
        Flag::deleteAll(['entity' => 'import', 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
