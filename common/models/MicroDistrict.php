<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "micro_district".
 *
 * @property int $id
 * @property int $city_id
 * @property int $district_id
 * @property string $slug
 * @property string $polygon
 *
 * @property City $city
 * @property District $district
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class MicroDistrict extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $polygonArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'micro_district';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'json' => [
                'class' => JsonFieldBehavior::class,
                'field' => 'polygon',
                'arrayField' => 'polygonArray',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id', 'district_id'], 'integer'],
            [['polygon'], 'string'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::class, 'targetAttribute' => ['district_id' => 'id']],
            [['slug'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'city_id' => Yii::t('model', 'City ID'),
            'district_id' => Yii::t('model', 'District ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::class, ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation micro_district_translations')
            ->andOnCondition(['micro_district_translations.entity' => Translation::ENTITY_MICRO_DISTRICT]);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);
        if ($result) {
            $this->polygon = new Expression("ST_GeomFromGeoJSON('{$this->polygon}')");
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        Translation::deleteAll(['entity' => Translation::ENTITY_MICRO_DISTRICT, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
