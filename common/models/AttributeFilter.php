<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "attribute_filter".
 *
 * @property int $attribute_id
 * @property int $type
 *
 * @property Attribute $attribute0
 */
class AttributeFilter extends \yii\db\ActiveRecord
{
    public const TYPE_DROPDOWN = 0;
    public const TYPE_RANGE = 10;
    public const TYPE_INPUT = 20;
    public const TYPE_RADIOLIST = 30;
    public const TYPE_CHECKBOXLIST = 40;
    public const TYPE_CHECKBOX = 50;
    public const TYPE_NONE = 60;

    public static $typeToView = [
        self::TYPE_DROPDOWN => 'dropdown',
        self::TYPE_RANGE => 'range',
        self::TYPE_INPUT => 'textbox',
        self::TYPE_RADIOLIST => 'radiolist',
        self::TYPE_CHECKBOXLIST => 'checkboxlist',
        self::TYPE_CHECKBOX => 'checkbox',
        self::TYPE_NONE => 'none'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_filter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'type'], 'required'],
            [['attribute_id', 'type'], 'integer'],
            [['attribute_id'], 'unique'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => Yii::t('model', 'Attribute ID'),
            'type' => Yii::t('model', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }
}
