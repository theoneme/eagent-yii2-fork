<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\CRCBehavior;
use common\behaviors\DefaultPriceBehavior;
use common\behaviors\elastic\PropertyElasticBehavior;
use common\behaviors\LinkableBehavior;
use common\behaviors\PropertyToBuildingImageBehavior;
use common\behaviors\SeoBehavior;
use common\models\query\PropertyQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "property".
 *
 * @property int $id
 * @property int $price
 * @property string $currency_code
 * @property string $locale
 * @property int $category_id
 * @property int $building_id
 * @property int $user_id
 * @property string $slug
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 * @property int $type
 * @property double $lat
 * @property double $lng
 * @property string $zip
 * @property string $address
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $default_price
 * @property boolean $ads_allowed
 * @property boolean $ads_allowed_partners
 * @property integer $views_count
 *
 * @property Category $category
 * @property Currency $currencyCode
 * @property Property $parent
 * @property User $user
 * @property Building $building
 * @property PropertyAttribute[] $propertyAttributes
 * @property Translation[] $translations
 * @property Attachment[] $attachments
 * @property Import[] $import
 * @property AddressTranslation[] $addressTranslations
 * @property PropertyVideo[] $videos
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 */
class Property extends \yii\db\ActiveRecord
{
    public const STATUS_DELETED = -20;
    public const STATUS_PAUSED = -10;
    public const STATUS_DRAFT = 0;
    public const STATUS_REQUIRES_MODERATION = 10;
    public const STATUS_REQUIRES_MODIFICATION = 20;
    public const STATUS_ACTIVE = 30;
    public const STATUS_SOLD = 100;

    public const TYPE_SALE = 0;
    public const TYPE_RENT = 10;

    public const OPERATION_SALE = 'sale';
    public const OPERATION_RENT = 'rent';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'defaultPriceBehavior' => [
                'class' => DefaultPriceBehavior::class
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'propertyAttributes', 'videos'],
            ],
            'seo' => [
                'class' => SeoBehavior::class,
            ],
            'crc' => [
                'class' => CRCBehavior::class,
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
                'folder' => 'property',
                'slugField' => 'slug',
            ],
            PropertyToBuildingImageBehavior::class,
            TimestampBehavior::class,
            PropertyElasticBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'currency_code', 'category_id'], 'required'],
            [['price', 'category_id', 'building_id', 'user_id', 'country_id', 'region_id', 'city_id'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['currency_code'], 'string', 'max' => 3],
            [['locale'], 'string', 'max' => 5],
            [['zip'], 'string'],
            [['slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [
                self::STATUS_DELETED,
                self::STATUS_PAUSED,
                self::STATUS_DRAFT,
                self::STATUS_REQUIRES_MODERATION,
                self::STATUS_REQUIRES_MODIFICATION,
                self::STATUS_ACTIVE,
                self::STATUS_SOLD,
            ]],
            ['type', 'default', 'value' => self::TYPE_SALE],
            [['type'], 'in', 'range' => [
                self::TYPE_SALE,
                self::TYPE_RENT
            ]],
            [['locale'], 'default', 'value' => Yii::$app->language],
            [['address'], 'string', 'max' => 125],
            [['ads_allowed', 'ads_allowed_partners'], 'boolean'],
            [['ads_allowed', 'ads_allowed_partners'], 'default', 'value' => true],
//            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
//            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
//            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::class, 'targetAttribute' => ['building_id' => 'id']],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'price' => Yii::t('model', 'Price'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'locale' => Yii::t('model', 'Locale'),
            'category_id' => Yii::t('model', 'Category ID'),
            'building_id' => Yii::t('model', 'Building ID'),
            'user_id' => Yii::t('model', 'User ID'),
        ];
    }

    /**
     * @return PropertyQuery
     */
    public static function find()
    {
        return new PropertyQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyCode()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::class, ['id' => 'building_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAttributes()
    {
        return $this->hasMany(PropertyAttribute::class, ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation property_translations')
            ->andOnCondition(['property_translations.entity' => Translation::ENTITY_PROPERTY]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressTranslations()
    {
        return $this->hasMany(AddressTranslation::class, ['lat' => 'lat', 'lng' => 'lng'])
            ->from('address_translation property_address_translations')
            ->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_PROPERTY]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchange()
    {
        return $this->hasOne(CurrencyExchange::class, ['code_from' => 'currency_code'])->andOnCondition(['code_to' => Yii::$app->params['app_currency']]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(PropertyVideo::class, ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImport()
    {
        return $this->hasMany(Import::class, ['entity_id' => 'id'])->onCondition(['entity' => 'property']);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }
        foreach ($this->import as $import) {
            $import->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_PROPERTY, 'entity_id' => $this->id]);
        PropertyAttribute::deleteAll(['property_id' => $this->id]);
        Flag::deleteAll(['entity' => 'property', 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
