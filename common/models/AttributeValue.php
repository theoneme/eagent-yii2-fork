<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "attribute_value".
 *
 * @property int $id
 * @property string $alias
 * @property int $status
 * @property int $attribute_id
 *
 * @property Attribute $attr
 * @property PropertyAttribute[] $propertyAttributes
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class AttributeValue extends ActiveRecord
{
    public const STATUS_PENDING = 0;
    public const STATUS_APPROVED = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_value';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'attribute_id'], 'integer'],
            [['alias'], 'string', 'max' => 65],
            [['status'], 'default', 'value' => self::STATUS_PENDING],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'status' => Yii::t('model', 'Status'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation attr_value_translations')
            ->andOnCondition(['attr_value_translations.entity' => Translation::ENTITY_ATTRIBUTE_VALUE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyAttributes()
    {
        return $this->hasMany(PropertyAttribute::class, ['value' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildingAttributes()
    {
        return $this->hasMany(BuildingAttribute::class, ['value' => 'id']);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        Translation::deleteAll(['entity' => Translation::ENTITY_ATTRIBUTE_VALUE, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
