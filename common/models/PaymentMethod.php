<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment_method".
 *
 * @property int $id
 * @property int $alias
 * @property int $logo
 * @property int $sort
 * @property int $enabled
 *
 * @property Payment[] $payments
 */
class PaymentMethod extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_method';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias'], 'required'],
            [['alias', 'logo', 'sort', 'enabled'], 'integer'],
            [['logo'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'logo' => Yii::t('model', 'Logo'),
            'sort' => Yii::t('model', 'Sort'),
            'enabled' => Yii::t('model', 'Enabled'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::class, ['payment_method_id' => 'id']);
    }
}
