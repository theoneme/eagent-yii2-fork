<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "flag".
 *
 * @property int $id
 * @property string $entity
 * @property int $entity_id
 * @property string $type
 * @property string $value
 */
class Flag extends ActiveRecord
{
    public const TYPE_TITLE_GENERATED = 'title-generated';
    public const TYPE_AUTO_TRANSLATED = 'auto-translated';
    public const TYPE_INVITATION_SENT = 'invitation-sent';

    public const TYPE_SOURCE_COUNTRY = 'source-country';
    public const TYPE_SOURCE_CITY = 'source-city';
    public const TYPE_SOURCE_ACTION = 'source-action';
    public const TYPE_SOURCE_CATEGORY = 'source-category';
    public const TYPE_SOURCE_PAGE = 'source-page';
    public const TYPE_SOURCE_BUILDING = 'source-building';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'flag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['entity'], 'string', 'max' => 15],
            [['value'], 'string', 'max' => 40],
            [['type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity' => 'Entity',
            'entity_id' => 'Entity ID',
            'type' => 'Type',
            'value' => 'Value',
        ];
    }
}
