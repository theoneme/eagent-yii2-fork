<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "property_attribute".
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $property_id
 * @property int $value
 * @property string $entity_alias
 * @property string $value_alias
 * @property double $value_number
 *
 * @property Attribute $attr
 * @property Property $property
 * @property AttributeValue $attributeValue
 */
class PropertyAttribute extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'value', 'entity_alias', 'value_alias'], 'required'],
            [['attribute_id', 'property_id', 'value'], 'integer'],
            [['entity_alias', 'value_alias'], 'string', 'max' => 75],
            [['value_number'], 'double'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
//            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::class, 'targetAttribute' => ['property_id' => 'id']],
            [['value'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['value' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
            'property_id' => Yii::t('model', 'Property ID'),
            'value' => Yii::t('model', 'Value'),
            'entity_alias' => Yii::t('model', 'Entity Alias'),
            'value_alias' => Yii::t('model', 'Value Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::class, ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValue()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValueTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'value'])
            ->from('translation property_av_translations')
            ->andOnCondition(['entity' => Translation::ENTITY_ATTRIBUTE_VALUE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'attribute_id'])
            ->from('translation property_a_translations')
            ->andOnCondition(['entity' => Translation::ENTITY_ATTRIBUTE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestAttributes()
    {
        return $this->hasMany(RequestAttribute::class, ['request_id' => 'id']);
    }
}
