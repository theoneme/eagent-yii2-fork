<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "wallet_history".
 *
 * @property int $id
 * @property int $user_id
 * @property int $payment_id
 * @property int $type
 * @property int $total
 * @property string $currency_code
 * @property int $created_at
 * @property resource $custom_data
 * @property int $template
 *
 * @property Currency $currency
 * @property User $user
 * @property Payment $payment
 */
class WalletHistory extends ActiveRecord
{
    const TYPE_REPLENISH = 0;
    const TYPE_PURCHASE = 10;
    const TYPE_INFO = 20;

    const TEMPLATE_REPLENISH = 0;
    const TEMPLATE_TARIFF = 10;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'wallet_history';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_id', 'type', 'total', 'created_at', 'template'], 'integer'],
            [['currency_code'], 'required'],
            [['customDataArray'], 'safe'],
            [['currency_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'payment_id' => Yii::t('model', 'Payment ID'),
            'type' => Yii::t('model', 'Type'),
            'total' => Yii::t('model', 'Total'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'created_at' => Yii::t('model', 'Created At'),
            'custom_data' => Yii::t('model', 'Custom Data'),
            'template' => Yii::t('model', 'Template'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::class, ['id' => 'payment_id']);
    }
}
