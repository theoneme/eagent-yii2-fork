<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use Yii;

/**
 * This is the model class for table "attribute_to_group".
 *
 * @property int $attribute_group_id
 * @property int $attribute_id
 * @property string $custom_data
 *
 * @property Attribute $attribute0
 * @property AttributeGroup $attributeGroup
 */
class AttributeToGroup extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_to_group';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id'], 'required'],
            [['attribute_group_id', 'attribute_id'], 'integer'],
            [['attribute_group_id', 'attribute_id'], 'unique', 'targetAttribute' => ['attribute_group_id', 'attribute_id']],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
            [['attribute_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeGroup::class, 'targetAttribute' => ['attribute_group_id' => 'id']],
            [['customDataArray'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'attribute_group_id' => Yii::t('model', 'Attribute Group ID'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeGroup()
    {
        return $this->hasOne(AttributeGroup::class, ['id' => 'attribute_group_id']);
    }
}
