<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "report".
 *
 * @property int $id
 * @property int $from_id
 * @property int $to_id
 * @property int $type
 * @property int $created_at
 * @property int $updated_at
 * @property resource $custom_data
 *
 * @property User $from
 * @property User $to
 */
class Report extends \yii\db\ActiveRecord
{
    public const REPORT_PROPERTIES = 0;
    public const REPORT_COMPARE_PRICE = 10;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from_id'], 'required'],
            [['from_id', 'to_id', 'type', 'created_at', 'updated_at'], 'integer'],
            [['custom_data'], 'string'],
            [['from_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['from_id' => 'id']],
            [['to_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['to_id' => 'id']],
            [['customDataArray'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'from_id' => Yii::t('model', 'From ID'),
            'to_id' => Yii::t('model', 'To ID'),
            'type' => Yii::t('model', 'Type'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'custom_data' => Yii::t('model', 'Custom Data'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::class, ['id' => 'from_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(User::class, ['id' => 'to_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        Translation::deleteAll(['entity' => Translation::ENTITY_CATEGORY, 'entity_id' => $this->id]);
    }
}
