<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "translation".
 *
 * @property int $id
 * @property int $entity
 * @property int $entity_id
 * @property string $key
 * @property string $locale
 * @property string $value
 */
class Translation extends \yii\db\ActiveRecord
{
    public const ENTITY_CATEGORY = 0;
    public const ENTITY_ATTRIBUTE = 10;
    public const ENTITY_PROPERTY = 20;
    public const ENTITY_ATTRIBUTE_VALUE = 30;
    public const ENTITY_COUNTRY = 40;
    public const ENTITY_REGION = 50;
    public const ENTITY_CITY = 60;
    public const ENTITY_DISTRICT = 65;
    public const ENTITY_MICRO_DISTRICT = 66;
    public const ENTITY_REQUEST = 70;
    public const ENTITY_USER = 80;
    public const ENTITY_BUILDING = 90;
    public const ENTITY_SERVICE = 100;
    public const ENTITY_PAGE = 100;
    public const ENTITY_COMPANY = 110;

    public const KEY_TITLE = 'title';
    public const KEY_ADDITIONAL_TITLE = 'add_title';
    public const KEY_NAME = 'name';
    public const KEY_DESCRIPTION = 'description';
    public const KEY_ADDITIONAL_DESCRIPTION = 'add_description';
    public const KEY_SUBTITLE = 'subtitle';
    public const KEY_SLUG = 'slug';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'key', 'locale', 'value'], 'required'],
            [['entity', 'entity_id'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 55],
            [['locale'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'key' => Yii::t('model', 'Key'),
            'locale' => Yii::t('model', 'Locale'),
            'value' => Yii::t('model', 'Value'),
        ];
    }
}
