<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "building_attribute".
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $building_id
 * @property int $value
 * @property string $entity_alias
 * @property string $value_alias
 * @property double $value_number
 *
 * @property Attribute $attribute0
 * @property Building $building
 * @property AttributeValue $value0
 */
class BuildingAttribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'value', 'entity_alias', 'value_alias'], 'required'],
            [['attribute_id', 'building_id', 'value'], 'integer'],
            [['value_number'], 'number'],
            [['entity_alias', 'value_alias'], 'string', 'max' => 75],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::class, 'targetAttribute' => ['building_id' => 'id']],
            [['value'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['value' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
            'building_id' => Yii::t('model', 'Building ID'),
            'value' => Yii::t('model', 'Value'),
            'entity_alias' => Yii::t('model', 'Entity Alias'),
            'value_alias' => Yii::t('model', 'Value Alias'),
            'value_number' => Yii::t('model', 'Value Number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::class, ['id' => 'building_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue0()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }
}
