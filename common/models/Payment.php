<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property int $user_id
 * @property int $payment_method_id
 * @property int $type
 * @property int $status
 * @property int $total
 * @property string $currency_code
 * @property string $code
 * @property int $created_at
 * @property int $updated_at
 * @property resource $custom_data
 *
 * @property Currency $currency
 * @property PaymentMethod $paymentMethod
 * @property User $user
 * @property WalletHistory[] $walletHistories
 */
class Payment extends ActiveRecord
{
    const STATUS_PENDING = 0;
    const STATUS_REFUSED = 10;
    const STATUS_PAID = 20;

    const TYPE_REPLENISH = 0;
    const TYPE_TARIFF = 10;

    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_method_id', 'type', 'total', 'currency_code'], 'required'],
            [['user_id', 'payment_method_id', 'type', 'total', 'created_at', 'updated_at'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['code'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_PENDING],
            ['status', 'in', 'range' => [self::STATUS_PENDING, self::STATUS_REFUSED, self::STATUS_PAID]],
            ['type', 'in', 'range' => [self::TYPE_REPLENISH, self::TYPE_TARIFF]],
//            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
//            [['payment_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentMethod::class, 'targetAttribute' => ['payment_method_id' => 'id']],
//            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'payment_method_id' => Yii::t('model', 'Payment Method ID'),
            'type' => Yii::t('model', 'Type'),
            'status' => Yii::t('model', 'Status'),
            'total' => Yii::t('model', 'Total'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'code' => Yii::t('model', 'Code'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::class, ['id' => 'payment_method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalletHistories()
    {
        return $this->hasMany(WalletHistory::class, ['payment_id' => 'id']);
    }
}
