<?php

namespace common\models;

use frontend\components\CompanyDbManager;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company_member".
 *
 * @property int $id
 * @property int $user_id
 * @property int $company_id
 * @property int $role
 * @property int $status
 *
 * @property Company $company
 * @property User $user
 */
class CompanyMember extends ActiveRecord
{
    public const ROLE_OWNER = 0;
    public const ROLE_ADMIN = 10;
    public const ROLE_SUPPORT = 20;
    public const ROLE_CONTENT_MANAGER = 30;
    public const ROLE_MEMBER = 40;

    public const STATUS_INVITED = 0;
    public const STATUS_ACTIVE = 10;

    private $_access = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'company_id', 'role', 'status'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['role', 'in', 'range' => [self::ROLE_OWNER, self::ROLE_ADMIN, self::ROLE_SUPPORT, self::ROLE_CONTENT_MANAGER]],
            ['status', 'in', 'range' => [self::STATUS_INVITED, self::STATUS_ACTIVE]],
            ['status', 'default', 'value' => self::STATUS_INVITED],
            ['user_id', 'unique', 'targetAttribute' => ['user_id', 'company_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'company_id' => Yii::t('model', 'Company ID'),
            'role' => Yii::t('model', 'Role'),
            'status' => Yii::t('model', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param $permissionName
     * @param array $params
     * @param bool $allowCaching
     * @return bool
     */
    public function can($permissionName, array $params = [], $allowCaching = true)
    {
        if ($allowCaching && empty($params) && isset($this->_access[$permissionName])) {
            return $this->_access[$permissionName];
        }
        if (($accessChecker = $this->getAccessChecker()) === null) {
            return false;
        }
        $access = $accessChecker->checkAccess($this->id, $permissionName, $params);
        if ($allowCaching && empty($params)) {
            $this->_access[$permissionName] = $access;
        }

        return $access;
    }

    /**
     * @return CompanyDbManager|null
     */
    protected function getAccessChecker()
    {
        return Yii::$app->companyAuthManager ?? null;
    }

    /**
     * @return string
     */
    public function getRbacRoleName()
    {
        switch ($this->role) {
            case self::ROLE_OWNER :
                return 'companyOwner';
                break;
            case self::ROLE_ADMIN :
                return 'companyAdmin';
                break;
            case self::ROLE_SUPPORT :
                return 'companyModerator';
                break;
            case self::ROLE_CONTENT_MANAGER :
                return 'companyEditor';
                break;
            case self::ROLE_MEMBER :
                return 'companyViewer';
                break;
            default :
                return null;
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert || array_key_exists('role', $changedAttributes)) {
            $rbacRoleName = $this->getRbacRoleName();
            if ($rbacRoleName !== null) {
                /** @var CompanyDbManager $authManager */
                $authManager = Yii::$app->companyAuthManager;
                $rbacRole = $authManager->getRole($rbacRoleName);
                if ($rbacRole !== null) {
                    $authManager->revokeAll($this->id);
                    $authManager->assign($rbacRole, $this->id);
                }
            }
        }
    }
}
