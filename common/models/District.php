<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "district".
 *
 * @property int $id
 * @property int $city_id
 * @property string $slug
 * @property string $polygon
 *
 * @property City $city
 * @property MicroDistrict[] $microDistricts
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class District extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $polygonArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'district';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'json' => [
                'class' => JsonFieldBehavior::class,
                'field' => 'polygon',
                'arrayField' => 'polygonArray',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id'], 'integer'],
            [['polygon'], 'string'],
            [['slug'], 'string', 'max' => 55],
//            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'city_id' => Yii::t('model', 'City ID'),
            'polygon' => Yii::t('model', 'Polygon'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMicroDistricts()
    {
        return $this->hasMany(MicroDistrict::class, ['district_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation district_translations')
            ->andOnCondition(['district_translations.entity' => Translation::ENTITY_DISTRICT]);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);
        if ($result) {
            $this->polygon = new Expression("ST_GeomFromGeoJSON('{$this->polygon}')");
        }
        return $result;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        Translation::deleteAll(['entity' => Translation::ENTITY_DISTRICT, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
