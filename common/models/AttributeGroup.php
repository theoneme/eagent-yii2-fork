<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;

/**
 * This is the model class for table "attribute_group".
 *
 * @property int $id
 * @property string $alias
 * @property integer $type
 * @property integer $entity
 *
 * @property AttributeToGroup[] $groupAttributes
 * @property Attribute[] $attrs
 *
 * @mixin LinkableBehavior
 */
class AttributeGroup extends \yii\db\ActiveRecord
{
    public const ENTITY_PROPERTY_CATEGORY = 0;
    public const ENTITY_BUILDING = 10;
    public const ENTITY_REQUEST_CATEGORY = 20;

    public const TYPE_RENT = 0;
    public const TYPE_SALE = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_group';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['groupAttributes'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'entity'], 'integer'],
            ['alias', 'string', 'max' => 35],
            [['alias'], 'required'],
            [['alias', 'type', 'entity'], 'unique', 'targetAttribute' => ['alias', 'type', 'entity']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'alias' => Yii::t('model', 'Alias'),
            'entity' => Yii::t('model', 'Entity'),
            'type' => Yii::t('model', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupAttributes()
    {
        return $this->hasMany(AttributeToGroup::class, ['attribute_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttrs()
    {
        return $this->hasMany(Attribute::class, ['id' => 'attribute_id'])->viaTable('attribute_to_group', ['attribute_group_id' => 'id']);
    }
}
