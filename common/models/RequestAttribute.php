<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use Yii;

/**
 * This is the model class for table "request_attribute".
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $request_id
 * @property resource $value
 *
 * @property Attribute $attribute0
 * @property Request $request
 */
class RequestAttribute extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request_attribute';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'json' => [
                'class' => JsonFieldBehavior::class,
                'field' => 'value'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id'], 'required'],
            [['attribute_id', 'request_id'], 'integer'],
            [['value'], 'string'],
//            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
//            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'id']],
            [['customDataArray'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
            'request_id' => Yii::t('model', 'Request ID'),
            'value' => Yii::t('model', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['id' => 'request_id']);
    }
}
