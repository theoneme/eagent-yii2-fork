<?php

namespace common\models\auth;

use common\interfaces\repositories\UserRepositoryInterface;
use common\models\User;
use common\repositories\sql\UserRepository;
use Yii;
use yii\base\Model;

/**
 * Class LoginForm
 * @package common\models\auth
 */
class LoginForm extends Model
{
    /**
     * @var string
     */
    public $login;
    /**
     * @var string
     */
    public $password;
    /**
     * @var bool
     */
    public $rememberMe = false;
    /**
     * @var bool
     */
    public $withRoles = false;
    /**
     * @var User
     */
    private $_user;
    /**
     * @var UserRepository
     */
    private $_userRepository;

    /**
     * LoginForm constructor.
     * @param UserRepositoryInterface $userRepository
     * @param array $config
     */
    public function __construct(UserRepositoryInterface $userRepository, $config = [])
    {
        $this->_userRepository = $userRepository;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'login' => Yii::t('labels', 'Login'),
            'password' => Yii::t('labels', 'Password'),
            'rememberMe' => Yii::t('labels', 'Remember me next time'),
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'loginTrim' => ['login', 'trim'],
            'requiredFields' => [['login'], 'required'],
            'rememberMe' => ['rememberMe', 'boolean'],
            ['password', 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @throws \common\exceptions\RepositoryException
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * @return bool
     * @throws \common\exceptions\RepositoryException
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 7200);
        }

        return false;
    }

    /**
     * @return array|User|null|\yii\db\ActiveRecord
     * @throws \common\exceptions\RepositoryException
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $criteria = ['email' => $this->login];
            if ($this->withRoles === true) {
                $criteria = ['and', ['>', 'role', User::ROLE_USER], $criteria];
            }
            $user = $this->_userRepository->findOneByCriteria($criteria);

            if ($user === null) {
                $criteria = ['phone' => $this->login];
                if ($this->withRoles === true) {
                    $criteria = ['and', ['>', 'role', User::ROLE_USER], $criteria];
                }
                $user = $this->_userRepository->findOneByCriteria($criteria);
            }

            if ($user !== null) {
                $this->_user = $user;
            }
        }

        return $this->_user;
    }
}
