<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "building_document".
 *
 * @property int $id
 * @property int $building_id
 * @property string $name
 * @property int $type
 * @property string $file
 *
 * @property Building $building
 */
class BuildingDocument extends ActiveRecord
{
    public const TYPE_PERMISSION = 0;
    public const TYPE_PROJECT = 10;
    public const TYPE_OPERATION_ACT = 20;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_document';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'building-document',
                'imageField' => 'file',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'type'], 'integer'],
            [['name', 'file'], 'string', 'max' => 255],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::class, 'targetAttribute' => ['building_id' => 'id']],
            [['type'], 'in', 'range' => [
                self::TYPE_PERMISSION,
                self::TYPE_PROJECT,
                self::TYPE_OPERATION_ACT,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'building_id' => Yii::t('model', 'Building ID'),
            'name' => Yii::t('model', 'Name'),
            'type' => Yii::t('model', 'Type'),
            'file' => Yii::t('model', 'File'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::class, ['id' => 'building_id']);
    }
}
