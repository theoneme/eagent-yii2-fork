<?php

namespace common\models;

use common\behaviors\JsonFieldBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "address_translation".
 *
 * @property string $lat
 * @property string $lng
 * @property string $locale
 * @property string $title
 * @property string $custom_data
 */
class AddressTranslation extends ActiveRecord
{
    /**
     * @var array
     */
    public $customDataArray = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_translation';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'json' => [
                'class' => JsonFieldBehavior::class,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lat', 'lng', 'locale'], 'required'],
            [['lat', 'lng'], 'number'],
            [['locale'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 255],
            [['lat', 'lng', 'locale'], 'unique', 'targetAttribute' => ['lat', 'lng', 'locale']],
            [['customDataArray'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'lng'),
            'locale' => Yii::t('model', 'Locale'),
            'title' => Yii::t('model', 'Title'),
        ];
    }
}
