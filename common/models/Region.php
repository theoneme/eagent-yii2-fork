<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;

/**
 * This is the model class for table "region".
 *
 * @property int $id
 * @property string $code
 * @property int $country_id
 * @property string $slug
 *
 * @property City[] $cities
 * @property Country $country
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'country_id'], 'required'],
            [['country_id'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['slug'], 'string', 'max' => 55],
//            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'code' => Yii::t('model', 'Code'),
            'country_id' => Yii::t('model', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['region_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation region_translations')
            ->andOnCondition(['region_translations.entity' => Translation::ENTITY_REGION]);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        foreach ($this->cities as $city) {
            $city->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_REGION, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
