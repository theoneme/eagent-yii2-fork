<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:36
 */

namespace common\models\query;

use common\models\Property;
use yii\db\ActiveQuery;

/**
 * Class PropertyQuery
 * @package common\models\query
 */
class PropertyQuery extends ActiveQuery
{
    /**
     * @param bool $state
     * @return $this
     */
    public function active($state = true)
    {
        if ($state === true) {
            return $this->andWhere(['property.status' => Property::STATUS_ACTIVE]);
        }

        return $this->andWhere(['not', ['property.status' => [Property::STATUS_DELETED, Property::STATUS_DRAFT]]]);
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function byId(int $id)
    {
        return $this->andWhere(['property.id' => $id]);
    }

    /**
     * @param string $slug
     * @return $this
     */
    public function bySlug(string $slug)
    {
        return $this->andWhere(['property.slug' => $slug]);
    }
}
