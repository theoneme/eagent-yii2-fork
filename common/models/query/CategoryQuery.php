<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:36
 */

namespace common\models\query;

use yii\db\ActiveQuery;

/**
 * Class CategoryQuery
 * @package common\models\query
 */
class CategoryQuery extends ActiveQuery
{
    /**
     * @param $levels
     * @return $this
     */
    public function levels($levels)
    {
        return $this->andWhere(['lvl' => $levels]);
    }
}
