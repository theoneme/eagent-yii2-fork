<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property int $country_id
 * @property int $region_id
 * @property string $lat
 * @property string $lng
 * @property boolean $is_big
 * @property string $slug
 *
 * @property Country $country
 * @property Region $region
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'region_id'], 'required'],
            [['country_id', 'region_id'], 'integer'],
            [['is_big'], 'boolean'],
            [['is_big'], 'default', 'value' => false],
            [['lat', 'lng'], 'number'],
            [['slug'], 'string', 'max' => 55],
//            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
//            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'country_id' => Yii::t('model', 'Country ID'),
            'region_id' => Yii::t('model', 'Region ID'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation city_translations')
            ->andOnCondition(['city_translations.entity' => Translation::ENTITY_CITY]);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        Translation::deleteAll(['entity' => Translation::ENTITY_CITY, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
