<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 *
 * @property int $id
 * @property int $price
 * @property string $currency_code
 * @property string $locale
 * @property string $slug
 * @property int $category_id
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property string $lat
 * @property string $lng
 * @property integer $status
 * @property integer $type
 * @property boolean $contract_price
 *
 * @property Category $category
 * @property Currency $currencyCode
 * @property User $user
 * @property Attachment[] $attachments
 * @property Import[] $import
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 */
class Request extends \yii\db\ActiveRecord
{
    public const STATUS_DELETED = -20;
    public const STATUS_PAUSED = -10;
    public const STATUS_DRAFT = 0;
    public const STATUS_REQUIRES_MODERATION = 10;
    public const STATUS_REQUIRES_MODIFICATION = 20;
    public const STATUS_ACTIVE = 30;

    public const TYPE_SALE = 0;
    public const TYPE_RENT = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'requestAttributes'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
                'folder' => 'job',
                'slugField' => 'slug',
            ],
            [
                'class' => TimestampBehavior::class,
                'preserveNonEmptyValues' => true
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'category_id', 'user_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['currency_code', 'locale', 'slug', 'category_id', 'user_id'], 'required'],
            [['lat', 'lng'], 'number'],
            [['status'], 'in', 'range' => [
                self::STATUS_DELETED,
                self::STATUS_PAUSED,
                self::STATUS_DRAFT,
                self::STATUS_REQUIRES_MODERATION,
                self::STATUS_REQUIRES_MODIFICATION,
                self::STATUS_ACTIVE,
            ]],
            ['type', 'default', 'value' => self::TYPE_SALE],
            [['type'], 'in', 'range' => [self::TYPE_SALE, self::TYPE_RENT]],
            [['contract_price'], 'boolean'],
            [['contract_price'], 'default', 'value' => false],
            [['currency_code'], 'string', 'max' => 3],
            [['locale'], 'string', 'max' => 5],
            [['slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'price' => Yii::t('model', 'Price'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'locale' => Yii::t('model', 'Locale'),
            'slug' => Yii::t('model', 'Slug'),
            'category_id' => Yii::t('model', 'Category ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyCode()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation request_translations')
            ->andOnCondition(['request_translations.entity' => Translation::ENTITY_REQUEST]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_REQUEST]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchange()
    {
        return $this->hasOne(CurrencyExchange::class, ['code_from' => 'currency_code'])->andOnCondition(['code_to' => Yii::$app->params['app_currency']]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressTranslations()
    {
        return $this->hasMany(AddressTranslation::class, ['lat' => 'lat', 'lng' => 'lng'])
            ->from('address_translation request_address_translations')
            ->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestAttributes()
    {
        return $this->hasMany(RequestAttribute::class, ['request_id' => 'id']);
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }
        foreach ($this->import as $import) {
            $import->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_REQUEST, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
