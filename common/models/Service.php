<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property int $price
 * @property string $currency_code
 * @property string $locale
 * @property int $created_at
 * @property int $updated_at
 * @property string $url
 *
 * @property Currency $currencyCode
 * @property Translation[] $translations
 * @property Attachment[] $attachments
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 */
class Service extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
                'folder' => 'service',
                'slugField' => 'id',
            ],
            TimestampBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'created_at', 'updated_at'], 'integer'],
            [['currency_code', 'locale'], 'required'],
            [['currency_code'], 'string', 'max' => 3],
            [['locale'], 'string', 'max' => 5],
            [['url'], 'string', 'max' => 255],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'price' => Yii::t('model', 'Price'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'locale' => Yii::t('model', 'Locale'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
            'url' => Yii::t('model', 'Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyCode()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation service_translations')
            ->andOnCondition(['service_translations.entity' => Translation::ENTITY_SERVICE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_REQUEST]);
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_SERVICE, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
