<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "building_site".
 *
 * @property int $id
 * @property int $building_id
 * @property int $type
 * @property string $name
 * @property int $transport
 * @property int $distance
 * @property int $time
 * @property string $lat
 * @property string $lng
 *
 * @property Building $building
 */
class BuildingSite extends ActiveRecord
{
    public const TYPE_METRO = 0;
    public const TYPE_SCHOOL = 10;
    public const TYPE_PARK = 20;
    public const TYPE_POND = 30;
    public const TYPE_AIRPORT = 40;
    public const TYPE_CITY_CENTER = 50;

    public const TRANSPORT_FOOT = 0;
    public const TRANSPORT_CAR = 10;
    public const TRANSPORT_TRANSPORT = 20;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_site';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'type', 'transport', 'distance', 'time'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::class, 'targetAttribute' => ['building_id' => 'id']],
            [['transport'], 'in', 'range' => [
                self::TRANSPORT_FOOT,
                self::TRANSPORT_CAR,
                self::TRANSPORT_TRANSPORT,
            ]],
            [['type'], 'in', 'range' => [
                self::TYPE_METRO,
                self::TYPE_SCHOOL,
                self::TYPE_PARK,
                self::TYPE_POND,
                self::TYPE_AIRPORT,
                self::TYPE_CITY_CENTER,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'building_id' => Yii::t('model', 'Building ID'),
            'type' => Yii::t('model', 'Type'),
            'name' => Yii::t('model', 'Name'),
            'transport' => Yii::t('model', 'Transport'),
            'distance' => Yii::t('model', 'Distance'),
            'time' => Yii::t('model', 'Time'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::class, ['id' => 'building_id']);
    }
}
