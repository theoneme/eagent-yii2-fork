<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "attachment".
 *
 * @property int $id
 * @property int $entity
 * @property int $entity_id
 * @property string $content
 * @property resource $metadata
 * @property int $is_primary
 */
class Attachment extends ActiveRecord
{
    public const ENTITY_PROPERTY = 0;
    public const ENTITY_REQUEST = 10;
    public const ENTITY_BUILDING = 20;
    public const ENTITY_SERVICE = 30;
    public const ENTITY_COMPANY = 40;
    public const ENTITY_BUILDING_PROGRESS = 50;

    public const ENTITY_CRM_CUSTOMER_DOCUMENT = 200;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'content'], 'required'],
            [['entity', 'entity_id', 'is_primary'], 'integer'],
            [['metadata'], 'string'],
            [['content'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'entity' => Yii::t('model', 'Entity'),
            'entity_id' => Yii::t('model', 'Entity ID'),
            'content' => Yii::t('model', 'Content'),
            'metadata' => Yii::t('model', 'Metadata'),
            'is_primary' => Yii::t('model', 'Is Primary'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            Yii::$app->mediaLayer->saveToAws($this->content);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        Yii::$app->mediaLayer->removeMedia($this->content);
    }
}
