<?php

namespace common\models;

use common\behaviors\LinkableBehavior;
use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $code
 * @property string $slug
 *
 * @property City[] $cities
 * @property Region[] $regions
 * @property Translation[] $translations
 *
 * @mixin LinkableBehavior
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string', 'max' => 2],
            [['slug'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'code' => Yii::t('model', 'Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::class, ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation country_translations')
            ->andOnCondition(['country_translations.entity' => Translation::ENTITY_COUNTRY]);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        foreach ($this->regions as $region) {
            $region->delete();
        }
        foreach ($this->cities as $city) {
            $city->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_COUNTRY, 'entity_id' => $this->id]);
        return parent::beforeDelete();
    }
}
