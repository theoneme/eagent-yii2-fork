<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "building_phase".
 *
 * @property int $id
 * @property int $building_id
 * @property int $year
 * @property int $quarter
 * @property int $status
 * @property string $lat
 * @property string $lng
 * @property int $floors
 *
 * @property Building $building
 */
class BuildingPhase extends ActiveRecord
{
    public const STATUS_UNFINISHED = 0;
    public const STATUS_FINISHED = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_phase';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'year', 'quarter', 'status', 'floors'], 'integer'],
            [['year'], 'number', 'max' => 9999],
            [['quarter'], 'number', 'max' => 4],
            [['lat', 'lng'], 'number'],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::class, 'targetAttribute' => ['building_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_UNFINISHED],
            [['status'], 'in', 'range' => [
                self::STATUS_UNFINISHED,
                self::STATUS_FINISHED,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'building_id' => Yii::t('model', 'Building ID'),
            'year' => Yii::t('model', 'Year'),
            'quarter' => Yii::t('model', 'Quarter'),
            'status' => Yii::t('model', 'Status'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
            'floors' => Yii::t('model', 'Floors'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::class, ['id' => 'building_id']);
    }
}
