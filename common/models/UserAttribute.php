<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_attribute".
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $user_id
 * @property int $value
 * @property string $entity_alias
 * @property string $value_alias
 *
 * @property Attribute $attribute0
 * @property User $user
 * @property AttributeValue $value0
 */
class UserAttribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'user_id', 'value', 'entity_alias', 'value_alias'], 'required'],
            [['attribute_id', 'user_id', 'value'], 'integer'],
            [['entity_alias', 'value_alias'], 'string', 'max' => 75],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['value'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['value' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'attribute_id' => Yii::t('model', 'Attribute ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'value' => Yii::t('model', 'Value'),
            'entity_alias' => Yii::t('model', 'Entity Alias'),
            'value_alias' => Yii::t('model', 'Value Alias'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue0()
    {
        return $this->hasOne(AttributeValue::class, ['id' => 'value']);
    }
}
