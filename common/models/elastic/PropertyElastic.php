<?php

namespace common\models\elastic;

use common\components\elasticsearch\ActiveRecord;

/**
 * This is the model class for table "property".
 *
 * @property int $id
 * @property int $building_id
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 * @property int $type
 * @property double $lat
 * @property double $lng
 * @property array $latlng
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $default_price
 *
 * @property array $category
 * @property array $property_attribute
 */
class PropertyElastic extends ActiveRecord
{
    public const STATUS_DELETED = -20;
    public const STATUS_PAUSED = -10;
    public const STATUS_DRAFT = 0;
    public const STATUS_REQUIRES_MODERATION = 10;
    public const STATUS_REQUIRES_MODIFICATION = 20;
    public const STATUS_ACTIVE = 30;
    public const STATUS_SOLD = 100;

    public const TYPE_SALE = 0;
    public const TYPE_RENT = 10;

    public const OPERATION_SALE = 'sale';
    public const OPERATION_RENT = 'rent';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * @return string
     */
    public static function index()
    {
        return 'eagent_property';
    }

    /**
     * @return string
     */
    public static function type()
    {
        return 'property';
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'id' => ['type' => 'integer'],
                    'user_id' => ['type' => 'integer'],
                    'created_at' => ['type' => 'integer'],
                    'updated_at' => ['type' => 'integer'],
                    'status' => ['type' => 'integer'],
                    'default_price' => ['type' => 'integer'],
                    'country_id' => ['type' => 'integer'],
                    'region_id' => ['type' => 'integer'],
                    'city_id' => ['type' => 'integer'],
                    'building_id' => ['type' => 'integer'],
                    'lat' => ['type' => 'float'],
                    'lng' => ['type' => 'float'],
                    'latlng' => ['type' => 'geo_point'],
                    'type' => ['type' => 'integer'],
                    'ads_allowed' => ['type' => 'boolean'],
                    'ads_allowed_partners' => ['type' => 'boolean'],
                    'category' => [
                        'type' => 'object',
                        'properties' => [
                            'root' => ['type' => 'integer'],
                            'lft' => ['type' => 'integer'],
                            'rgt' => ['type' => 'integer'],
                        ]
                    ],
                    'property_attribute' => [
                        'type' => 'nested',
                        'properties' => [
                            'attribute_id' => ['type' => 'integer'],
                            'entity_alias' => ['type' => 'keyword'],
                            'value_alias' => ['type' => 'keyword'],
                            'value_number' => ['type' => 'float'],
                        ]
                    ],
                    'translation' => [
                        'type' => 'nested',
                        'properties' => [
                            'key' => ['type' => 'keyword'],
                            'value' => ['type' => 'text'],
                            'locale' => ['type' => 'keyword']
                        ]
                    ],
                ]
            ],
        ];
    }

    /**
     * Delete this model's type
     */
    public static function deleteMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteMapping(static::index(), static::type());
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'user_id',
            'created_at',
            'updated_at',
            'status',
            'default_price',
            'country_id',
            'region_id',
            'city_id',
            'building_id',
            'type',
            'latlng',
            'lat',
            'lng',
            'ads_allowed',
            'ads_allowed_partners',

            'category',
            'property_attribute',
            'translation'
        ];
    }

    /**
     * @return array
     */
    public static function getRelations()
    {
        return [
            'property_attribute' => 'property_attribute'
        ];
    }

    /**
     * @return array
     */
    public static function getRelationDefinitions()
    {
        return [
            'category' => 'object',
            'property_attribute' => 'nested',
            'translation' => 'nested',
        ];
    }
}
