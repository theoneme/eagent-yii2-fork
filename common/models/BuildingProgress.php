<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "building_progress".
 *
 * @property int $id
 * @property int $building_id
 * @property int $year
 * @property int $quarter
 *
 * @property Building $building
 * @property Attachment[] $attachments
 *
 * @mixin AttachmentBehavior
 */
class BuildingProgress extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_progress';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'folder' => 'building-progress',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id', 'year', 'quarter'], 'integer'],
            [['year'], 'number', 'max' => 9999],
            [['quarter'], 'number', 'max' => 4],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::class, 'targetAttribute' => ['building_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'building_id' => Yii::t('model', 'Building ID'),
            'year' => Yii::t('model', 'Year'),
            'quarter' => Yii::t('model', 'Quarter'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::class, ['id' => 'building_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_BUILDING_PROGRESS]);
    }
}
