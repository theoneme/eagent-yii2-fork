<?php

namespace common\models;

use common\behaviors\AttachmentBehavior;
use common\behaviors\BuildingSeoBehavior;
use common\behaviors\CRCBehavior;
use common\behaviors\elastic\BuildingElasticBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "building".
 *
 * @property int $id
 * @property string $slug
 * @property string $lat
 * @property string $lng
 * @property int $status
 * @property int $type
 * @property int $user_id
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $locale
 * @property boolean $ads_allowed
 * @property boolean $ads_allowed_partners
 *
 * @property BuildingAttribute[] $buildingAttributes
 * @property Translation[] $translations
 * @property Attachment[] $attachments
 * @property User $user
 * @property AddressTranslation[] $addressTranslations
 * @property BuildingPhase[] $phases
 * @property BuildingProgress[] $progress
 * @property BuildingSite[] $sites
 * @property BuildingDocument[] $documents
 * @property BuildingVideo[] $videos
 * @property Property[] $properties
 *
 * @mixin LinkableBehavior
 * @mixin AttachmentBehavior
 */
class Building extends ActiveRecord
{
    public const STATUS_DELETED = -20;
    public const STATUS_PAUSED = -10;
    public const STATUS_DRAFT = 0;
    public const STATUS_REQUIRES_MODERATION = 10;
    public const STATUS_REQUIRES_MODIFICATION = 20;
    public const STATUS_ACTIVE = 30;

    public const TYPE_DEFAULT = 0;
    public const TYPE_NEW_CONSTRUCTION = 10;

    public const TYPE_DEFAULT_TEXT = 'secondary';
    public const TYPE_NEW_CONSTRUCTION_TEXT = 'new-construction';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'buildingAttributes', 'phases', 'progress', 'sites', 'documents', 'videos'],
            ],
            'seo' => [
                'class' => BuildingSeoBehavior::class,
            ],
            'crc' => [
                'class' => CRCBehavior::class,
            ],
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'attachmentRelation' => 'attachments',
                'folder' => 'building',
                'slugField' => 'slug',
            ],
            TimestampBehavior::class,
            BuildingElasticBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lat', 'lng'], 'number'],
            [['status', 'user_id', 'country_id', 'region_id', 'city_id', 'user_id'], 'integer'],
            [['slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
            [['type'], 'default', 'value' => self::TYPE_DEFAULT],
            [['type'], 'in', 'range' => [
                self::TYPE_DEFAULT,
                self::TYPE_NEW_CONSTRUCTION,
            ]],
            [['status'], 'in', 'range' => [
                self::STATUS_DELETED,
                self::STATUS_PAUSED,
                self::STATUS_DRAFT,
                self::STATUS_REQUIRES_MODERATION,
                self::STATUS_REQUIRES_MODIFICATION,
                self::STATUS_ACTIVE,
            ]],
            [['locale'], 'default', 'value' => Yii::$app->language],
            [['status'], 'default', 'value' => self::STATUS_REQUIRES_MODERATION],
            [['locale'], 'string', 'max' => 5],
            [['ads_allowed', 'ads_allowed_partners'], 'boolean'],
            [['ads_allowed', 'ads_allowed_partners'], 'default', 'value' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'slug' => Yii::t('model', 'Slug'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
            'status' => Yii::t('model', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuildingAttributes()
    {
        return $this->hasMany(BuildingAttribute::class, ['building_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation building_translations')
            ->andOnCondition(['building_translations.entity' => Translation::ENTITY_BUILDING]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressTranslations()
    {
        return $this->hasMany(AddressTranslation::class, ['lat' => 'lat', 'lng' => 'lng'])
            ->from('address_translation building_address_translations')
            ->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_BUILDING]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhases()
    {
        return $this->hasMany(BuildingPhase::class, ['building_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgress()
    {
        return $this->hasMany(BuildingProgress::class, ['building_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(BuildingSite::class, ['building_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(BuildingDocument::class, ['building_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(BuildingVideo::class, ['building_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::class, ['building_id' => 'id']);
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        foreach ($this->attachments as $attachment) {
            $attachment->delete();
        }
        Translation::deleteAll(['entity' => Translation::ENTITY_BUILDING, 'entity_id' => $this->id]);
        BuildingAttribute::deleteAll(['building_id' => $this->id]);
        return parent::beforeDelete();
    }
}
