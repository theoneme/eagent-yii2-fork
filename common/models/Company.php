<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\helpers\Auth;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property int $user_id
 * @property string $logo
 * @property string $banner
 * @property int $status
 * @property int $type
 * @property int $created_at
 * @property int $updated_at
 * @property string $lat
 * @property string $lng
 *
 * @property User $user
 * @property CompanyMember[] $companyMembers
 * @property Translation[] $translations
 * @property UserAttribute[] $userAttributes
 * @property UserContact[] $contacts
 *
 * @mixin LinkableBehavior
 */
class Company extends ActiveRecord
{
    public const STATUS_DELETED = -50;
    public const STATUS_REQUIRES_MODIFICATION = -40;
    public const STATUS_REQUIRES_MODERATION = -30;
    public const STATUS_ACTIVE = 10;

    public const TYPE_AGENCY = 0;
    public const TYPE_DEVELOPER = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
            ],
            'logo' => [
                'class' => ImageBehavior::class,
                'folder' => 'company',
                'imageField' => 'logo'
            ],
            'banner' => [
                'class' => ImageBehavior::class,
                'folder' => 'company',
                'imageField' => 'banner'
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'integer'],
            [['logo', 'banner'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['lat', 'lng'], 'number'],
            ['status', 'default', 'value' => self::STATUS_REQUIRES_MODERATION],
            ['type', 'default', 'value' => self::TYPE_AGENCY],
            ['type', 'in', 'range' => [self::TYPE_AGENCY, self::TYPE_DEVELOPER]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'logo' => Yii::t('model', 'Logo'),
            'banner' => Yii::t('model', 'Banner'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMembers()
    {
        return $this->hasMany(CompanyMember::class, ['company_id' => 'id'])->indexBy('user_id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation company_translations')
            ->andOnCondition(['company_translations.entity' => Translation::ENTITY_COMPANY]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAttributes()
    {
        return $this->hasMany(UserAttribute::class, ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressTranslations()
    {
        return $this->hasMany(AddressTranslation::class, ['lat' => 'lat', 'lng' => 'lng'])
            ->from('address_translation company_address_translations')
            ->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(UserContact::class, ['user_id' => 'user_id']);
    }

    /**
     * @param $companyId
     * @return bool
     */
    public static function hasAccess($companyId)
    {
        return Yii::$app->user->isGuest
            ? false
            : CompanyMember::find()->where(['company_id' => $companyId, 'user_id' => Auth::user()->id, 'role' => CompanyMember::ROLE_OWNER])->exists();
    }
}
