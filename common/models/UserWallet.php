<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_wallet".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $currency_code
 * @property integer $balance
 *
 * @property Currency $currency
 * @property User $user
 * @property WalletHistory[] $histories
 */
class UserWallet extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'balance'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'user_id' => Yii::t('model', 'User ID'),
            'currency_code' => Yii::t('model', 'Currency Code'),
            'balance' => Yii::t('model', 'Balance'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(WalletHistory::class, ['currency_code' => 'currency_code', 'user_id' => 'user_id'])->orderBy('created_at desc');
    }
}
