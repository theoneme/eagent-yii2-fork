<?php

namespace common\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\models\elastic\BuildingElastic;
use common\models\elastic\PropertyElastic;
use frontend\modules\crm\models\CrmMember;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $avatar
 * @property string $phone
 * @property double $lat
 * @property double $lng
 * @property integer $role
 * @property integer $crm_member_id
 * @property integer $company_user_id
 * @property boolean $is_company
 * @property boolean $ads_allowed
 * @property boolean $ads_allowed_partners
 *
 * @property CrmMember $crmMember
 * @property Property[] $properties
 * @property Translation[] $translations
 * @property AddressTranslation[] $addressTranslations
 * @property User $companyUser
 * @property Company $company
 * @property CompanyMember[] $companyMembers
 * @property Company $currentCompany
 * @property UserContact[] $contacts
 * @property UserWallet[] $wallets
 *
 * @mixin LinkableBehavior
 */
class User extends ActiveRecord implements IdentityInterface
{
    public const STATUS_DELETED = -50;
    public const STATUS_REQUIRES_MODIFICATION = -40;
    public const STATUS_REQUIRES_MODERATION = -30;
    public const STATUS_ACTIVE = 10;

    public const TYPE_DEFAULT = 0;
    public const TYPE_REALTOR = 10;

    public const ROLE_USER = 0;
    public const ROLE_MODERATOR = 10;
    public const ROLE_ADMIN = 20;

    /**
     * @var string
     */
    public $password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'image' => [
                'class' => ImageBehavior::class,
                'folder' => 'user',
                'imageField' => 'avatar',
                'labelField' => 'username'
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations', 'contacts'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['type', 'default', 'value' => self::TYPE_DEFAULT],
            ['type', 'in', 'range' => [self::TYPE_DEFAULT, self::TYPE_REALTOR]],
            ['avatar', 'string', 'max' => 155],
            [['lat', 'lng'], 'number'],
            [['username', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email', 'phone'], 'unique'],
//            [['email'], 'phoneOrEmailRequired'],
            [['password', 'password_reset_token'], 'string', 'min' => 6, 'max' => 72],
            [['is_company', 'ads_allowed', 'ads_allowed_partners'], 'boolean'],
            ['is_company', 'default', 'value' => false],
            [['ads_allowed', 'ads_allowed_partners'], 'default', 'value' => true],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::find()->where(['and', ['id' => $id], ['not', ['status' => self::STATUS_DELETED]]])->one();
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['and', ['username' => $username], ['not', ['status' => self::STATUS_DELETED]]]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     * @throws \yii\base\InvalidArgumentException
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function generatePasswordResetToken()
    {
        return Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['entity_id' => 'id'])
            ->from('translation user_translations')
            ->andOnCondition(['user_translations.entity' => Translation::ENTITY_USER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressTranslations()
    {
        return $this->hasMany(AddressTranslation::class, ['lat' => 'lat', 'lng' => 'lng'])
            ->from('address_translation user_address_translations')
            ->indexBy('locale');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmMember()
    {
        return $this->hasOne(CrmMember::class, ['id' => 'crm_member_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImport()
    {
        return $this->hasOne(Import::class, ['entity_id' => 'id'])->andOnCondition(['import.entity' => 'user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(__CLASS__, ['id' => 'company_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyMembers()
    {
        return $this->hasMany(CompanyMember::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentCompany()
    {
        return $this->hasOne(Company::class, ['user_id' => 'company_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(UserContact::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocials()
    {
        return $this->hasMany(UserSocial::class, ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getWallets()
    {
        return $this->hasMany(UserWallet::class, ['user_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getCurrentId()
    {
        return $this->company_user_id ?? $this->id;
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        Yii::$app->mediaLayer->removeMedia($this->avatar);
        foreach ($this->properties as $property) {
            $property->delete();
        }
        return parent::beforeDelete();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!$insert && (array_key_exists('ads_allowed', $changedAttributes) || array_key_exists('ads_allowed_partners', $changedAttributes))) {
            Property::updateAll(['ads_allowed' => $this->ads_allowed, 'ads_allowed_partners' => $this->ads_allowed_partners], ['user_id' => $this->id]);
            PropertyElastic::updateAll(['ads_allowed' => $this->ads_allowed, 'ads_allowed_partners' => $this->ads_allowed_partners], ['user_id' => $this->id]);
            Building::updateAll(['ads_allowed' => $this->ads_allowed, 'ads_allowed_partners' => $this->ads_allowed_partners], ['user_id' => $this->id]);
            BuildingElastic::updateAll(['ads_allowed' => $this->ads_allowed, 'ads_allowed_partners' => $this->ads_allowed_partners], ['user_id' => $this->id]);
        }
    }
}
