<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "building_video".
 *
 * @property int $id
 * @property int $building_id
 * @property int $type
 * @property string $content
 *
 * @property Building $building
 */
class BuildingVideo extends \yii\db\ActiveRecord
{
    public const TYPE_EMBED = 0;
    public const TYPE_FILE = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_video';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'content'], 'required'],
            [['building_id', 'type'], 'integer'],
            [['content'], 'string', 'max' => 155],
            [['building_id'], 'exist', 'skipOnError' => true, 'targetClass' => Building::class, 'targetAttribute' => ['building_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'building_id' => Yii::t('model', 'Building ID'),
            'type' => Yii::t('model', 'Type'),
            'content' => Yii::t('model', 'Content'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Building::class, ['id' => 'building_id']);
    }
}
