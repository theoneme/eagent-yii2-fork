<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property string $title
 * @property string $code
 * @property string $symbol_left
 * @property string $symbol_right
 *
 * @property Property[] $properties
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'code'], 'required'],
            [['title'], 'string', 'max' => 32],
            [['code'], 'string', 'max' => 3],
            [['symbol_left', 'symbol_right'], 'string', 'max' => 12],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('model', 'Title'),
            'code' => Yii::t('model', 'Code'),
            'symbol_left' => Yii::t('model', 'Symbol Left'),
            'symbol_right' => Yii::t('model', 'Symbol Right'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::class, ['currency_code' => 'code']);
    }
}
