<?php

namespace common\behaviors;

use common\helpers\UtilityHelper;
use common\mappers\AddressTranslationsMapper;
use common\mappers\TranslationsMapper;
use common\models\Building;
use common\models\Flag;
use common\models\Translation;
use common\services\seo\BuildingSeoService;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingSeoBehavior
 * @property ActiveRecord $owner
 * @package commmon\behaviors
 */
class BuildingSeoBehavior extends Behavior
{
    /**
     * @var BuildingSeoService
     */
    private $_buildingSeoService;

    /**
     * BuildingSeoBehavior constructor.
     * @param BuildingSeoService $buildingSeoService
     * @param array $config
     */
    public function __construct(BuildingSeoService $buildingSeoService, array $config = [])
    {
        parent::__construct($config);
        $this->_buildingSeoService = $buildingSeoService;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
//            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function afterSave()
    {
        /* @var Building $building */
        $building = $this->owner;
        $translations = TranslationsMapper::getMappedData($building->translations);
        $addressTranslation = AddressTranslationsMapper::getMappedData($building->addressTranslations);
        $seoParams = ArrayHelper::map($building->buildingAttributes, 'entity_alias', 'value_alias');
        $seoParams['address'] = $addressTranslation['title'] ?? '';
        $seoParams['title'] = $translations[Translation::KEY_NAME] ?? null;
        $seo = $this->_buildingSeoService->getSeo(BuildingSeoService::TEMPLATE_BUILDING, $seoParams, $building->locale);

        if (!empty($seo['heading'])) {
            $titleTranslation = array_reduce(
                $building->translations,
                function ($carry, $var) {
                    /* @var Translation $var */
                    return $carry === null && $var->key === Translation::KEY_TITLE ? $var : $carry;
                },
                null
            );
            if ($titleTranslation === null) {
                $titleTranslation = new Translation([
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_BUILDING,
                    'entity_id' => $building->id,
                    'locale' => $building->locale
                ]);
            }
            $titleTranslation->value = $seo['heading'];
            if ($titleTranslation->save()) {
                $building->populateRelation('translations', array_merge($building->translations, [$titleTranslation]));
                $building->updateAttributes([
                    'slug' => UtilityHelper::generateSlug($seo['heading'], 7, 75, $building->getPrimaryKey()),
                    'updated_at' => time()
                ]);
                (new Flag(['entity' => 'building', 'entity_id' => $building->id, 'type' => Flag::TYPE_TITLE_GENERATED]))->save();
            }
        }
    }
}