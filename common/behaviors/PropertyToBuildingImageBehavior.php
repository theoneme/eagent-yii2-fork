<?php

namespace common\behaviors;

use common\models\Attachment;
use common\models\Property;
use common\repositories\sql\BuildingRepository;
use common\repositories\sql\PropertyRepository;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\BaseFileHelper;

/**
 * Class PropertyToBuildingImageBehavior
 * @property ActiveRecord $owner
 * @package commmon\behaviors
 */
class PropertyToBuildingImageBehavior extends Behavior
{
    /**
     * @var PropertyRepository
     */
    private $_buildingRepository;
    /**
     * @var Service
     */
    private $_s3;

    /**
     * PropertyToBuildingImageBehavior constructor.
     * @param BuildingRepository $buildingRepository
     * @param Service $s3
     * @param array $config
     */
    public function __construct(BuildingRepository $buildingRepository, Service $s3, array $config = [])
    {
        $this->_buildingRepository = $buildingRepository;
        $this->_s3 = $s3;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function afterSave()
    {
        /** @var Property $property */
        $property = $this->owner;

        if ($property->building_id !== null) {
            $building = $this->_buildingRepository->with(['attachments'])->findOneByCriteria([
                'id' => $property->building_id
            ], true);

            if (!empty($property->attachments) && empty($building['attachments'])) {
                $propertyAttachment = $property->attachments[0];

                $pathInfo = pathinfo($propertyAttachment['content']);
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $dir = Yii::getAlias('@frontend') . "/web/uploads/building/{$year}/{$month}/{$day}/";
                BaseFileHelper::createDirectory($dir);

                $this->_s3->commands()
                    ->get(Yii::$app->mediaLayer->fixMediaPath($propertyAttachment['content']))
                    ->saveAs("{$dir}{$building['slug']}.{$pathInfo['extension']}")->execute();

                $buildingAttachment = new Attachment([
                    'entity' => Attachment::ENTITY_BUILDING,
                    'entity_id' => $building['id'],
                    'content' => "/uploads/building/{$year}/{$month}/{$day}/{$building['slug']}.{$pathInfo['extension']}"
                ]);
                Yii::$app->mediaLayer->saveToAws("/uploads/building/{$year}/{$month}/{$day}/{$building['slug']}.{$pathInfo['extension']}");

                $buildingAttachment->save();
            }
        }
    }
}