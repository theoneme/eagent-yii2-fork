<?php

namespace common\behaviors;

use common\models\Attachment;
use common\models\Building;
use common\models\Property;
use common\repositories\sql\PropertyRepository;
use frostealth\yii2\aws\s3\Service;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\BaseFileHelper;

/**
 * Class BuildingAttachmentBehavior
 * @property ActiveRecord $owner
 * @package commmon\behaviors
 */
class BuildingAttachmentBehavior extends Behavior
{
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var Service
     */
    private $_s3;

    /**
     * BuildingAttachmentBehavior constructor.
     * @param PropertyRepository $propertyRepository
     * @param Service $s3
     * @param array $config
     */
    public function __construct(PropertyRepository $propertyRepository, Service $s3, array $config = [])
    {
        $this->_propertyRepository = $propertyRepository;
        $this->_s3 = $s3;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function afterSave()
    {
        /** @var Building $building */
        $building = $this->owner;

        $attachments = $building->attachments;
        if (empty($attachments)) {
            /** @var Property $property */
            $property = $this->_propertyRepository->joinWith(['attachments'], true, 'INNER JOIN')->findOneByCriteria([
                'building_id' => $building->id
            ]);

            if ($property !== null) {
                $attachments = $property->attachments;
                if (!empty($attachments)) {
                    $pathInfo = pathinfo($attachments[0]['content']);
                    $year = date('Y');
                    $month = date('m');
                    $day = date('d');
                    $dir = Yii::getAlias('@frontend') . "/web/uploads/building/{$year}/{$month}/{$day}/";
                    BaseFileHelper::createDirectory($dir);

                    $this->_s3->commands()
                        ->get(Yii::$app->mediaLayer->fixMediaPath($attachments[0]['content']))
                        ->saveAs("{$dir}{$building['slug']}.{$pathInfo['extension']}")->execute();

                    $buildingAttachment = new Attachment([
                        'entity' => Attachment::ENTITY_BUILDING,
                        'entity_id' => $building['id'],
                        'content' => "/uploads/building/{$year}/{$month}/{$day}/{$building['slug']}.{$pathInfo['extension']}"
                    ]);
                    Yii::$app->mediaLayer->saveToAws("/uploads/building/{$year}/{$month}/{$day}/{$building['slug']}.{$pathInfo['extension']}");
                    $buildingAttachment->save();
                }
            }
        }
    }
}