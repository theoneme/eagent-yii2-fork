<?php

namespace common\behaviors\elastic;

use common\services\elasticsearch\BuildingElasticConverter;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class BuildingElasticBehavior
 * @package common\behaviors\elastic
 */
class BuildingElasticBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @return bool|\common\models\elastic\PropertyElastic
     */
    public function afterSave()
    {
        /** @var mixed $object */
        $object = $this->owner;

        $converter = new BuildingElasticConverter($object);
        return $converter->process(true);
    }
}