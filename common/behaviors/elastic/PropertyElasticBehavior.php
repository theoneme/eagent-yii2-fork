<?php

namespace common\behaviors\elastic;

use common\services\elasticsearch\PropertyElasticConverter;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Class PropertyElasticBehavior
 * @package common\behaviors
 */
class PropertyElasticBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @return bool|\common\models\elastic\PropertyElastic
     */
    public function afterSave()
    {
        /** @var mixed $object */
        $object = $this->owner;

        $converter = new PropertyElasticConverter($object);
        return $converter->process(true);
    }
}