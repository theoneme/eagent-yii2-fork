<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.04.2017
 * Time: 14:58
 */

namespace common\behaviors;

use common\helpers\UtilityHelper;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\BaseStringHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * Class ImageBehavior
 *
 * @property ActiveRecord $owner
 *
 * @package common\behaviors
 */
class ImageBehavior extends Behavior
{
    /**
     * @var string
     */
    public $imageField = 'image';
    /**
     * @var bool
     */
    public $saveToAws = true;
    /**
     * @var string
     */
    public $folder;
    /**
     * @var string
     */
    public $labelField;
    /**
     * @var string
     */
    public $label;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @return void
     * @throws \RuntimeException
     * @throws \yii\base\InvalidArgumentException
     */
    public function beforeSave()
    {
        if ($this->folder === null) {
            $this->folder = Inflector::camel2id(StringHelper::basename($this->owner::className()));
        }

        $this->label = $this->labelField && !empty($this->owner->{$this->labelField})
            ? UtilityHelper::transliterate(BaseStringHelper::truncateWords(preg_replace('/\s+/', ' ', trim($this->owner->{$this->labelField})), 5, ''))
            : Inflector::camel2id(StringHelper::basename($this->owner::className()));

        if (!empty($this->owner->{$this->imageField}) && !preg_match('/^http(s)?:\/\/.*/', $this->owner->{$this->imageField})) {
            $oldPath = $this->owner->getOldAttribute($this->imageField);
            $currentPath = $this->owner->{$this->imageField};
            $webRoot = Yii::getAlias('@frontend') . '/web';
            if (file_exists($webRoot . $currentPath) && preg_match('/^\/uploads\/temp\/.*/', $currentPath)) {
                $year = date('Y');
                $month = date('m');
                $basePath = "/uploads/{$this->folder}/{$year}/{$month}/";
                $dir = $webRoot . $basePath;
                if (!is_dir($dir) && !mkdir($dir, 0777, true) && !is_dir($dir)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
                }

                $pathInfo = pathinfo($currentPath);
                if (empty($pathInfo['extension'])) {
                    switch (exif_imagetype($webRoot . $currentPath)) {
                        case IMAGETYPE_JPEG:
                        case IMAGETYPE_PNG:
                            $pathInfo['extension'] = 'jpg';
                            break;
                        case IMAGETYPE_GIF:
                            $pathInfo['extension'] = 'gif';
                            break;
                    }
                }
                $extension = strtolower(!empty($pathInfo['extension']) ? '.' . $pathInfo['extension'] : '');
                $newPath = $basePath . $this->label . '-' . uniqid() . $extension;

                rename($webRoot . $this->owner->{$this->imageField}, $webRoot . $newPath);
                $this->owner->{$this->imageField} = $newPath;

                if ($this->saveToAws === true) {
                    Yii::$app->mediaLayer->saveToAws($newPath);
                }

                if ($oldPath && $newPath !== $oldPath) {
                    Yii::$app->mediaLayer->removeMedia($oldPath);
                }
            } else if (!empty($oldPath)) {
                $this->owner->{$this->imageField} = $oldPath;
            }
        }
    }
}