<?php

namespace common\behaviors;

use common\repositories\sql\CityRepository;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\AfterSaveEvent;

/**
 * Class CRCBehavior КАНТРИ РИГИОН СИТИ ДУ НОТ ФОРГЕТ
 * @property ActiveRecord $owner
 * @package commmon\behaviors
 */
class CRCBehavior extends Behavior
{
    /**
     * @var CityRepository
     */
    private $_cityRepository;

    /**
     * CRCBehavior constructor.
     * @param CityRepository $cityRepository
     * @param array $config
     */
    public function __construct(CityRepository $cityRepository, array $config = [])
    {
        parent::__construct($config);
        $this->_cityRepository = $cityRepository;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @param AfterSaveEvent $afterSaveEvent
     */
    public function afterSave(AfterSaveEvent $afterSaveEvent)
    {
        /** @var mixed $object */
        $object = $this->owner;

        if (array_key_exists('lat', $afterSaveEvent->changedAttributes) && $object->lat !== $afterSaveEvent->changedAttributes['lat']) {
            $city = $this->_cityRepository->select(['distance' => "3959 * ACOS(COS(RADIANS({$object->lat})) 
                * COS(RADIANS(city.lat)) 
                * COS(RADIANS(city.lng) - RADIANS({$object->lng})) 
                + SIN(RADIANS({$object->lat})) 
                * SIN(RADIANS(city.lat)))"])
                ->orderBy(['distance' => SORT_ASC])
                ->limit(1)
                ->findOneByCriteria(['and',
                    ['not', ['lat' => null]],
                    ['between', 'city.lat', $object->lat - 5, $object->lat + 5],
                    ['between', 'city.lng', $object->lng - 5, $object->lng + 5],
                ], true);

            if ($city !== null) {
                $object->updateAttributes(['city_id' => $city['id'], 'region_id' => $city['region_id'], 'country_id' => $city['country_id'], 'updated_at' => time()]);
            }
        }
    }
}