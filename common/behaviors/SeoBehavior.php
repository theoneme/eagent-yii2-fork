<?php

namespace common\behaviors;


use common\helpers\UtilityHelper;
use common\mappers\AddressTranslationsMapper;
use common\mappers\TranslationsMapper;
use common\models\Flag;
use common\models\Property;
use common\models\Translation;
use common\services\entities\CityService;
use common\services\seo\PropertySeoService;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class SeoBehavior
 * @property ActiveRecord $owner
 * @package commmon\behaviors
 */
class SeoBehavior extends Behavior
{
    /**
     * @var PropertySeoService
     */
    private $_propertySeoService;
    /**
     * @var CityService
     */
    private $_cityService;

    /**
     * SeoBehavior constructor.
     * @param PropertySeoService $propertySeoService
     * @param CityService $cityService
     * @param array $config
     */
    public function __construct(PropertySeoService $propertySeoService, CityService $cityService, array $config = [])
    {
        parent::__construct($config);
        $this->_propertySeoService = $propertySeoService;
        $this->_cityService = $cityService;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
//            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @return void
     * @throws \Throwable
     */
    public function afterSave()
    {
        /* @var Property $property */
        $property = $this->owner;
        $categoryTranslations = TranslationsMapper::getMappedData($property->category->translations);
        $addressTranslations = AddressTranslationsMapper::getMappedData($property->addressTranslations, AddressTranslationsMapper::MODE_FULL);
        $addressTranslation = array_key_exists($property->locale, $addressTranslations) ? $addressTranslations[$property->locale] : array_pop($addressTranslations);
        $seoParams = ArrayHelper::map($property->propertyAttributes, 'entity_alias', 'value_alias');
        $seoParams['type'] = $property['type'];
        $seoParams['address'] = $addressTranslation['title'] ?? '';
        if ($property->locale === Yii::$app->language) {
            $seoParams['saddress'] = $addressTranslation['title'] ?? '';
        } else if (!empty($property->city_id)) {
            $city = $this->_cityService->getOne(['id' => $property->city_id]);
            if ($city) {
                $seoParams['saddress'] = $city['title'];
            }
        }
        $seoParams['city'] = $addressTranslation['data']['city'] ?? '';
        $seoParams['category'] = $categoryTranslations[Translation::KEY_ADDITIONAL_TITLE] ?? $categoryTranslations[Translation::KEY_TITLE] ?? '';
        $seo = $this->_propertySeoService->getSeo($property->category->customDataArray['seoTemplate'] ?? null, $seoParams, $property->locale);
        if (!empty($seo['heading'])) {
            $titleTranslation = array_reduce(
                $property->translations,
                function ($carry, $var) {
                    /* @var Translation $var */
                    return $carry === null && $var->key === Translation::KEY_TITLE ? $var : $carry;
                },
                null
            );
            if ($titleTranslation === null) {
                $titleTranslation = new Translation([
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_PROPERTY,
                    'entity_id' => $property->id,
                    'locale' => $property->locale
                ]);
            }
            $titleTranslation->value = $seo['heading'];
            if ($titleTranslation->save()) {
                $property->populateRelation('translations', array_merge($property->translations, [$titleTranslation]));
                (new Flag(['entity' => 'property', 'entity_id' => $property->id, 'type' => Flag::TYPE_TITLE_GENERATED]))->save();
            }

            $slugTranslation = array_reduce(
                $property->translations,
                function ($carry, $var) {
                    /* @var Translation $var */
                    return $carry === null && $var->key === Translation::KEY_SLUG ? $var : $carry;
                },
                null
            );
            if ($slugTranslation === null) {
                $slugTranslation = new Translation([
                    'key' => Translation::KEY_SLUG,
                    'entity' => Translation::ENTITY_PROPERTY,
                    'entity_id' => $property->id,
                    'locale' => $property->locale,
                    'value' => UtilityHelper::generateSlug($seo['heading'], 9, 100, $property->getPrimaryKey())
                ]);
                $slugTranslation->save();
                $property->populateRelation('translations', array_merge($property->translations, [$slugTranslation]));

                $property->updateAttributes([
                    'slug' => UtilityHelper::generateSlug($seo['heading'], 9, 100, $property->getPrimaryKey()),
                    'updated_at' => time()
                ]);
            }
        }
    }
}