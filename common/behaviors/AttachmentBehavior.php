<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 27.04.2017
 * Time: 14:58
 */

namespace common\behaviors;

use common\helpers\UtilityHelper;
use Yii;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\BaseStringHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * Class AttachmentBehavior
 * @property ActiveRecord $owner
 * @package common\behaviors
 */
class AttachmentBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attachmentRelation = 'attachments';

    /**
     * @var array
     */
    private $changes = [];

    /**
     * @var string
     */
    public $folder = 'job';

    /**
     * @var array
     */
    private $relationsBuffer = [];

    /**
     * @var string
     */
    public $slugField;

    /**
     * @var string
     */
    public $labelField;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        if ($this->attachmentRelation === null) {
            throw new InvalidConfigException('The "attachmentRelation" property must be set.');
        }
    }

    /**
     * @param null $id
     * @return ActiveRecord
     * @throws \yii\base\InvalidArgumentException
     */
    public function bindAttachment($id = null)
    {
        if ($id !== null) {
            $this->changes[] = $id;
        }

        /* @var ActiveRecord $class */
        $class = $this->owner->getRelation($this->attachmentRelation)->modelClass;

        /* @var ActiveRecord[] $records */
        $records = $this->owner->{$this->attachmentRelation};
        foreach ($records as $record) {
            if ($id !== null && $record->getAttribute('content') === $id) {
                $this->relationsBuffer[] = $record;

                return $record;
            }
        }

        $record = new $class;
        $records[] = $record;

        $this->relationsBuffer = $records;
        $this->owner->populateRelation($this->attachmentRelation, $records);

        return $record;
    }

    /**
     * @return void
     */
    public function afterValidate()
    {
        if (!Model::validateMultiple($this->relationsBuffer)) {
            foreach ($this->relationsBuffer as $item) {
                $this->owner->addError($this->attachmentRelation, $item->errors);
            }
        }
    }

    /**
     * @return void
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave()
    {
        $ids = !empty($this->changes) ? $this->changes : [];

        /* @var ActiveRecord $relationClass */
        $relationClass = $this->owner->getRelation($this->attachmentRelation)->modelClass;
        $relationQuery = $this->owner->getRelation($this->attachmentRelation);
        $foreignKeyName = array_keys($relationQuery->link)[0];

        $whereCondition = $relationQuery->where ?? [];
        $onCondition = $relationQuery->on ?? [];

        /* @var ActiveQuery $queryCondition */
        $queryCondition = $relationClass::find()
            ->andWhere($whereCondition)
            ->andWhere([$foreignKeyName => $this->owner->getPrimaryKey()])
            ->andFilterWhere($onCondition);

        if (!empty($ids)) {
            $queryCondition->andWhere(['not', ['content' => $ids]]);
        }
        $items = $queryCondition->all();

        /* @var ActiveRecord $relationClass */
        if ($items !== null) {
            foreach ($items as $item) {
                $item->delete();
            }
        }

        /* @var ActiveRecord $record */
        $relations = array_filter($this->relationsBuffer, function ($value) use ($ids) {
            return in_array($value->content, $ids);
        });

        $this->relationsBuffer = $relations;
        $this->owner->populateRelation($this->attachmentRelation, $relations);
        $baseName = $this->getBaseName();
        $webRoot = Yii::getAlias('@frontend') . '/web';
        foreach ($relations as $key => $record) {
            if (preg_match('/^\/uploads\/temp\/.*/', $record->content) && file_exists($webRoot . $record->content)) {
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $dir = $webRoot . "/uploads/{$this->folder}/{$year}/{$month}/{$day}/";
                if (!is_dir($dir)) {
                    if (!mkdir($dir, 0777, true) && !is_dir($dir)) {
                        throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
                    }
                    if (Yii::$app instanceof \yii\console\Application) {
                        chown($dir, 'www-data');
                        chgrp($dir, 'www-data');
                    }
                }
                $pathInfo = pathinfo($record->content);
                if (empty($pathInfo['extension'])) {
                    switch (exif_imagetype($webRoot . $record->content)) {
                        case IMAGETYPE_JPEG:
                        case IMAGETYPE_PNG:
                            $pathInfo['extension'] = 'jpg';
                            break;
                        case IMAGETYPE_GIF:
                            $pathInfo['extension'] = 'gif';
                            break;
                    }
                }
                $extension = strtolower(!empty($pathInfo['extension']) ? '.' . $pathInfo['extension'] : '');
                $newPath = "/uploads/{$this->folder}/{$year}/{$month}/{$day}/" . $baseName . '-' . uniqid() . $extension;

                rename($webRoot . $record->content, $webRoot . $newPath);
                $record->content = $newPath;

                $record->{$foreignKeyName} = $this->owner->getPrimaryKey();
                $record->save();
            }
        }
    }

    /**
     * @return string
     */
    private function getBaseName()
    {
        if ($this->slugField && !empty($this->owner->{$this->slugField})) {
            $slug = $this->owner->{$this->slugField};
        } else if ($this->labelField && !empty($this->owner->{$this->labelField})) {
            $slug = UtilityHelper::transliterate(
                    BaseStringHelper::truncateWords(
                        preg_replace('/\s+/', ' ', trim($this->owner->{$this->labelField})),
                        5,
                        ''
                    )
                ) . '-' . $this->owner->getPrimaryKey();
        } else {
            $slug = Inflector::camel2id(StringHelper::basename(get_class($this->owner)));
        }
        return $slug;
    }
}