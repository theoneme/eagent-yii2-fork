<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2018
 * Time: 10:19
 */

namespace common\exceptions;
use yii\base\Exception;

/**
 * Class EntityNotCreatedException
 * @package common\exceptions
 */
class EntityNotCreatedException extends Exception
{

}