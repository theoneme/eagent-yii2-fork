<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2018
 * Time: 10:19
 */

namespace common\exceptions;

use yii\base\Exception;

/**
 * Class FileSystemException
 * @package common\exceptions
 */
class FileSystemException extends Exception
{

}