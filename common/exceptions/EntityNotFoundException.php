<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2018
 * Time: 10:19
 */

namespace common\exceptions;
use yii\base\Exception;

/**
 * Class EntityNotFoundException
 * @package common\exceptions
 */
class EntityNotFoundException extends Exception
{

}