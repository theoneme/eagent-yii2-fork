<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 14:18
 */

namespace common\exceptions;

use yii\db\Exception;

/**
 * Class RepositoryException
 * @package common\exceptions
 */
class RepositoryException extends Exception
{

}