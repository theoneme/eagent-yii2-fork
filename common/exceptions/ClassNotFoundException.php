<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2018
 * Time: 19:41
 */

namespace common\exceptions;

use yii\base\InvalidConfigException;

/**
 * Class ClassNotFoundException
 * @package common\exceptions
 */
class ClassNotFoundException extends InvalidConfigException
{

}