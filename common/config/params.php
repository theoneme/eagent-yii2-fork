<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'languages' => [
        'en-GB' => 'English',
        'zh-CN' => 'Chinese',
        'es-ES' => 'Spanish',
        'pt-PT' => 'Portuguese',
        'fr-FR' => 'French',
        'de-DE' => 'German',
        'it-IT' => 'Italian',
        'ru-RU' => 'Russian',
        'fi-FI' => 'Finnish',
        'sv-SV' => 'Swedish',
//        'da-DA' => 'Danish',
//        'hr-HR' => 'Croatian',
//        'bg-BG' => 'Bulgarian',
        'pl-PL' => 'Polish',
        'el-EL' => 'Greek',
        'be-BE' => 'Belarusian',
        'ua-UA' => 'Ukrainian',
        'tr-TR' => 'Turkish',
        'ka-GE' => 'Georgian',
//        'az-AZ' => 'Azerbaijani',
        'ja-JA' => 'Japanese',
        'ko-KO' => 'Korean',
        'vi-VI' => 'Vietnamese',
        'th-TH' => 'Thai',
        'ar-AR' => 'Arabic',
        'hi-HI' => 'Hindi',
        'he-IL' => 'Hebrew',
    ],
    'supportedLocales' => [
        'en-GB' => 'en',
        'zh-CN' => 'zh',
        'es-ES' => 'es',
        'pt-PT' => 'pt',
        'fr-FR' => 'fr',
        'de-DE' => 'de',
        'it-IT' => 'it',
        'ru-RU' => 'ru',
        'fi-FI' => 'fi',
        'sv-SV' => 'sv',
//        'da-DA' => 'da',
//        'hr-HR' => 'hr',
//        'bg-BG' => 'bg',
        'pl-PL' => 'pl',
        'el-EL' => 'el',
        'be-BE' => 'be',
        'ua-UA' => 'uk',
        'tr-TR' => 'tr',
        'ka-GE' => 'ka',
//        'az-AZ' => 'az',
        'ja-JA' => 'ja',
        'ko-KO' => 'ko',
        'vi-VI' => 'vi',
        'th-TH' => 'th',
        'ar-AR' => 'ar',
        'hi-HI' => 'hi',
        'he-IL' => 'he',
    ],
    'imperaviLocales' => [
        'en-GB' => 'en',
        'zh-CN' => 'zh_cn',
        'es-ES' => 'es',
        'pt-PT' => 'pt',
        'fr-FR' => 'fr',
        'de-DE' => 'de',
        'it-IT' => 'it',
        'ru-RU' => 'ru',
    ],
    'currencies' => [
        'USD' => ['title' => 'Dollar', 'sign' => '$'],
        'EUR' => ['title' => 'Euro', 'sign' => '€'],
        'RUB' => ['title' => 'Ruble', 'sign' => 'Р'],
        'CNY' => ['title' => 'Yuan', 'sign' => '¥'],
    ],
    'bigLocales' => [
        'en-GB' => 'en',
        'zh-CN' => 'zh',
        'es-ES' => 'es',
        'ru-RU' => 'ru',
    ],

    'awsRegion' => 'eu-central-1',
    'awsCloudfrontId' => 'dwmh9kmdoyiua',
    'awsSource' => 's3', // cloudfront or s3

    'currencyApiKey' => 'b3a1fa961cac829d2dd42bfee8a88040',

//    'currency' => 'RUB',
//    'app_currency' => 'RUB',

//    'googleAPIKey' => 'AIzaSyB25LjYwHLOIcqg9Dff-2qn453CZWfOZQw',
    'yandexAPIKey' => 'trnsl.1.1.20170811T073735Z.bb4b895fb8c879e1.aeb27f9a01b569d58e85564abdf380761fe8124f',
    'yandexAPIKeys' => [
        'trnsl.1.1.20170811T073735Z.bb4b895fb8c879e1.aeb27f9a01b569d58e85564abdf380761fe8124f',
        'trnsl.1.1.20181210T143902Z.e1c3c91f9dfd5d7e.2690606047ed44a8a7838c69fc0961dcc1061052',
        'trnsl.1.1.20181210T144152Z.b429032800cd89a8.68c92839fc67ab94c0c89b6f8d7453979b88a7d3',
        'trnsl.1.1.20181211T100417Z.061fafa2e912ee18.9d28705f369870d495a0f17e571e42a0e7bf4f79',
        'trnsl.1.1.20181211T100535Z.24839a73b7124677.7f255c66eb1c200d1f09f6f48878326f4d0ebc70',
    ],

    'enabledSocials' => [
        'facebook',
        'google'
    ],

    'emailSender' => ['info@eagent.me' => 'Команда eAgent'],
    'ourEmails' => ['netproject7@gmail.com'],
    'landingEmails' => ['info@eagent.me', 'save@eagent.me', 'tav.ekb@yandex.ru'],

    'runtime' => [
        'currency' => [
            'ratios' => [],
            'currencies' => []
        ]
    ],

    'images' => [
        'catalog' => [
            'width' => 320,
            'height' => 230
        ],
    ],
    'mapImages' => [
        'catalog' => [
            'width' => 265,
            'height' => 110
        ],
    ],

    'polygonCoef' => 20
];
