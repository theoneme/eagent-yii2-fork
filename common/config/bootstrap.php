<?php

use common\interfaces\repositories\CompanyMemberRepositoryInterface;
use common\interfaces\repositories\CompanyRepositoryInterface;
use common\models\Company;
use common\models\CompanyMember;
use common\repositories\sql\CompanyMemberRepository;
use common\repositories\sql\CompanyRepository;
use frostealth\yii2\aws\s3\Service;

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(__DIR__, 2) . '/frontend');
Yii::setAlias('@backend', dirname(__DIR__, 2) . '/backend');
Yii::setAlias('@console', dirname(__DIR__, 2) . '/console');
Yii::setAlias('@instance', dirname(__DIR__, 2) . '/instance');

Yii::$container->set(Service::class, [
    'credentials' => [
        'key' => 'AKIAJKAMNOAZSPXQ3LMA',
        'secret' => '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',
    ],
    'region' => 'eu-central-1',
    'defaultBucket' => 'eagent-media',
    'defaultAcl' => 'public-read',
]);

Yii::$container->set(\common\interfaces\repositories\PropertyRepositoryInterface::class, [
    'class' => \common\repositories\sql\PropertyRepository::class
]);
Yii::$container->set(\common\repositories\sql\PropertyRepository::class, function () {
    return new \common\repositories\sql\PropertyRepository(new \common\models\Property());
});
Yii::$container->set(\common\repositories\elastic\PropertyRepository::class, function () {
    return new \common\repositories\elastic\PropertyRepository(new \common\models\elastic\PropertyElastic());
});

Yii::$container->set(\common\interfaces\repositories\BuildingRepositoryInterface::class, [
    'class' => \common\repositories\sql\BuildingRepository::class
]);
Yii::$container->set(\common\repositories\sql\BuildingRepository::class, function () {
    return new \common\repositories\sql\BuildingRepository(new \common\models\Building());
});
Yii::$container->set(\common\repositories\elastic\BuildingRepository::class, function () {
    return new \common\repositories\elastic\BuildingRepository(new \common\models\elastic\BuildingElastic());
});

Yii::$container->set(\common\interfaces\repositories\AttributeValueRepositoryInterface::class, [
    'class' => \common\repositories\sql\AttributeValueRepository::class
]);
Yii::$container->set(\common\repositories\sql\AttributeValueRepository::class, function () {
    return new \common\repositories\sql\AttributeValueRepository(new \common\models\AttributeValue());
});

Yii::$container->set(\common\interfaces\repositories\AttributeRepositoryInterface::class, [
    'class' => \common\repositories\sql\AttributeRepository::class
]);
Yii::$container->set(\common\repositories\sql\AttributeRepository::class, function () {
    return new \common\repositories\sql\AttributeRepository(new \common\models\Attribute());
});

Yii::$container->set(\common\interfaces\repositories\UserRepositoryInterface::class, [
    'class' => \common\repositories\sql\UserRepository::class
]);
Yii::$container->set(\common\repositories\sql\UserRepository::class, function () {
    return new \common\repositories\sql\UserRepository(new \common\models\User());
});

Yii::$container->set(\common\interfaces\repositories\CountryRepositoryInterface::class, [
    'class' => \common\repositories\sql\CountryRepository::class
]);
Yii::$container->set(\common\repositories\sql\CountryRepository::class, function () {
    return new \common\repositories\sql\CountryRepository(new \common\models\Country());
});

Yii::$container->set(\common\interfaces\repositories\RegionRepositoryInterface::class, [
    'class' => \common\repositories\sql\RegionRepository::class
]);
Yii::$container->set(\common\repositories\sql\RegionRepository::class, function () {
    return new \common\repositories\sql\RegionRepository(new \common\models\Region());
});

Yii::$container->set(\common\interfaces\repositories\CityRepositoryInterface::class, [
    'class' => \common\repositories\sql\CityRepository::class
]);
Yii::$container->set(\common\repositories\sql\CityRepository::class, function () {
    return new \common\repositories\sql\CityRepository(new \common\models\City());
});

Yii::$container->set(\common\interfaces\repositories\TranslationRepositoryInterface::class, [
    'class' => \common\repositories\sql\TranslationRepository::class
]);
Yii::$container->set(\common\repositories\sql\TranslationRepository::class, function () {
    return new \common\repositories\sql\TranslationRepository(new \common\models\Translation());
});

Yii::$container->set(\common\interfaces\repositories\CategoryRepositoryInterface::class, [
    'class' => \common\repositories\sql\CategoryRepository::class
]);
Yii::$container->set(\common\repositories\sql\CategoryRepository::class, function () {
    return new \common\repositories\sql\CategoryRepository(new \common\models\Category());
});

Yii::$container->set(\common\interfaces\repositories\PropertyAttributeRepositoryInterface::class, [
    'class' => \common\repositories\sql\PropertyAttributeRepository::class
]);
Yii::$container->set(\common\repositories\sql\PropertyAttributeRepository::class, function () {
    return new \common\repositories\sql\PropertyAttributeRepository(new \common\models\PropertyAttribute());
});

Yii::$container->set(\common\interfaces\repositories\BuildingAttributeRepositoryInterface::class, [
    'class' => \common\repositories\sql\BuildingAttributeRepository::class
]);
Yii::$container->set(\common\repositories\sql\BuildingAttributeRepository::class, function () {
    return new \common\repositories\sql\BuildingAttributeRepository(new \common\models\BuildingAttribute());
});

Yii::$container->set(\common\interfaces\repositories\AttributeFilterRepositoryInterface::class, [
    'class' => \common\repositories\sql\AttributeFilterRepository::class
]);
Yii::$container->set(\common\repositories\sql\AttributeFilterRepository::class, function () {
    return new \common\repositories\sql\AttributeFilterRepository(new \common\models\AttributeFilter());
});

Yii::$container->set(\common\interfaces\repositories\AddressTranslationRepositoryInterface::class, [
    'class' => \common\repositories\sql\AddressTranslationRepository::class
]);
Yii::$container->set(\common\repositories\sql\AddressTranslationRepository::class, function () {
    return new \common\repositories\sql\AddressTranslationRepository(new \common\models\AddressTranslation());
});

Yii::$container->set(\common\interfaces\repositories\RequestRepositoryInterface::class, [
    'class' => \common\repositories\sql\RequestRepository::class
]);
Yii::$container->set(\common\repositories\sql\RequestRepository::class, function () {
    return new \common\repositories\sql\RequestRepository(new \common\models\Request());
});

Yii::$container->set(\common\interfaces\repositories\PageRepositoryInterface::class, [
    'class' => \common\repositories\sql\PageRepository::class
]);
Yii::$container->set(\common\repositories\sql\PageRepository::class, function () {
    return new \common\repositories\sql\PageRepository(new \common\models\Page());
});

Yii::$container->set(\common\interfaces\repositories\ServiceRepositoryInterface::class, [
    'class' => \common\repositories\sql\ServiceRepository::class
]);
Yii::$container->set(\common\repositories\sql\ServiceRepository::class, function () {
    return new \common\repositories\sql\ServiceRepository(new \common\models\Service());
});

Yii::$container->set(\common\repositories\sql\ReportRepository::class, function () {
    return new \common\repositories\sql\ReportRepository(new \common\models\Report());
});

Yii::$container->set(\common\repositories\sql\UserSocialRepository::class, function () {
    return new \common\repositories\sql\UserSocialRepository(new \common\models\UserSocial());
});

Yii::$container->set(\common\repositories\sql\AttributeGroupRepository::class, function () {
    return new \common\repositories\sql\AttributeGroupRepository(new \common\models\AttributeGroup());
});
Yii::$container->set(\common\repositories\sql\AttributeToGroupRepository::class, function () {
    return new \common\repositories\sql\AttributeToGroupRepository(new \common\models\AttributeToGroup());
});

Yii::$container->set(CompanyRepositoryInterface::class, [
    'class' => CompanyRepository::class
]);
Yii::$container->set(CompanyRepository::class, function () {
    return new CompanyRepository(new Company());
});

Yii::$container->set(CompanyMemberRepositoryInterface::class, [
    'class' => CompanyMemberRepository::class
]);
Yii::$container->set(CompanyMemberRepository::class, function () {
    return new CompanyMemberRepository(new CompanyMember());
});

Yii::$container->set(\common\interfaces\filters\PropertyFilterInterface::class, [
    'class' => \frontend\services\elasticsearch\PropertyFilterService::class
]);
Yii::$container->set(\common\interfaces\filters\BuildingFilterInterface::class, [
    'class' => \frontend\services\elasticsearch\BuildingFilterService::class
]);

Yii::$container->set(\common\interfaces\repositories\UserWalletRepositoryInterface::class, [
    'class' => \common\repositories\sql\UserWalletRepository::class
]);
Yii::$container->set(\common\repositories\sql\UserWalletRepository::class, function () {
    return new \common\repositories\sql\UserWalletRepository(new \common\models\UserWallet());
});

Yii::$container->set(\common\interfaces\repositories\WalletHistoryRepositoryInterface::class, [
    'class' => \common\repositories\sql\WalletHistoryRepository::class
]);
Yii::$container->set(\common\repositories\sql\WalletHistoryRepository::class, function () {
    return new \common\repositories\sql\WalletHistoryRepository(new \common\models\WalletHistory());
});

Yii::$container->set(\common\interfaces\repositories\PaymentRepositoryInterface::class, [
    'class' => \common\repositories\sql\PaymentRepository::class
]);
Yii::$container->set(\common\repositories\sql\PaymentRepository::class, function () {
    return new \common\repositories\sql\PaymentRepository(new \common\models\Payment());
});

Yii::$container->set(\common\interfaces\repositories\PaymentMethodRepositoryInterface::class, [
    'class' => \common\repositories\sql\PaymentMethodRepository::class
]);
Yii::$container->set(\common\repositories\sql\PaymentMethodRepository::class, function () {
    return new \common\repositories\sql\PaymentMethodRepository(new \common\models\PaymentMethod());
});


Yii::$container->set(\common\interfaces\repositories\DistrictRepositoryInterface::class, [
    'class' => \common\repositories\sql\DistrictRepository::class
]);
Yii::$container->set(\common\repositories\sql\DistrictRepository::class, function () {
    return new \common\repositories\sql\DistrictRepository(new \common\models\District());
});


Yii::$container->set(\common\interfaces\repositories\MicroDistrictRepositoryInterface::class, [
    'class' => \common\repositories\sql\MicroDistrictRepository::class
]);
Yii::$container->set(\common\repositories\sql\MicroDistrictRepository::class, function () {
    return new \common\repositories\sql\MicroDistrictRepository(new \common\models\MicroDistrict());
});
