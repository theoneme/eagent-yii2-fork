<?php

use common\components\CacheLayer;
use common\components\MediaLayer;
use yii\caching\FileCache;
use yii\i18n\PhpMessageSource;
use yii\rbac\DbManager;

return [
    'name' => 'eAgent',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'language' => 'ru-RU',
    'bootstrap' => [
        frontend\components\CurrencySelector::class,
    ],
    'components' => [
        'elasticsearch' => [
            'class' => \common\components\elasticsearch\Connection::class,
            'connectionTimeout' => 200,
            'nodes' => [
                ['http_address' => '127.0.0.1:9200'],
                // configure more hosts if you have a cluster
            ],
        ],
        'cache' => [
            'class' => FileCache::class,
        ],
        'cacheLayer' => [
            'class' => CacheLayer::class
        ],
        'mediaLayer' => [
            'class' => MediaLayer::class
        ],
        'i18n' => [
            'translations' => [
                'seo*' => [
                    'class' => \yii\i18n\PhpMessageSource::class,
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'seo' => 'seo.php',
                    ],
                ],
                'notification*' => [
                    'class' => PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'notification' => 'notification.php',
                    ],
                ],
            ],
        ],
        'authManager' => [
            'class' => DbManager::class,
        ],
    ],
];
