<?php

return [
    'request' => [
        'default' => [
            1 => [
                'title' => Yii::t('wizard', 'Primary Information'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ],
                ]
            ],
            2 => [
                'title' => Yii::t('wizard', 'Address'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'address-step'
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('wizard', 'Price and terms of deal'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'price-step',
                    ],
                ]
            ],
        ],
        'flats' => [
            1 => [
                'title' => Yii::t('wizard', 'Primary Information'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ]
                ]
            ],
            2 => [
                'title' => Yii::t('wizard', 'Address'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'address-step'
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('wizard', 'Request Parameters'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'area',
                            'rooms',
                            'bathrooms',
                            'floor',
                            'additional',
                            'balconies',
                            'building',
                        ]
                    ],
                ]
            ],
            4 => [
                'title' => Yii::t('wizard', 'Price and terms of deal'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'price-step',
                    ],
                ]
            ],
        ],
    ]
];
