<?php

return [
    'building' => [
        \common\models\Building::TYPE_NEW_CONSTRUCTION => [
            1 => [
                'title' => Yii::t('wizard', 'Primary Information'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ]
                ]
            ],
            2 => [
                'title' => Yii::t('wizard', 'Address'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'address-step'
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('wizard', 'Building Parameters'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'building_common',
                            'building_new'
                        ]
                    ]
                ]
            ],
            4 => [
                'title' => Yii::t('wizard', 'Photo and Video'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'photo-step'
                    ]
                ]
            ],
            5 => [
                'title' => Yii::t('wizard', 'Construction Phases'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'phase-step'
                    ]
                ]
            ],
            6 => [
                'title' => Yii::t('wizard', 'Construction Progress'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'progress-step'
                    ]
                ]
            ],
            7 => [
                'title' => Yii::t('wizard', 'Nearby Sites'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'site-step'
                    ]
                ]
            ],
            8 => [
                'title' => Yii::t('wizard', 'Building Documents'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'document-step'
                    ]
                ]
            ],
        ],
        'default' => [
            1 => [
                'title' => Yii::t('wizard', 'Primary Information'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ]
                ]
            ],
            2 => [
                'title' => Yii::t('wizard', 'Address'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'address-step'
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('wizard', 'Building Parameters'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'building_common'
                        ]
                    ]
                ]
            ],
            4 => [
                'title' => Yii::t('wizard', 'Photo and Video'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'photo-step'
                    ]
                ]
            ],
        ],
    ],
];
