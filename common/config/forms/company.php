<?php

return [
    'company' => [
        1 => [
            'title' => Yii::t('wizard', 'Company Info'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'info-step'
                ],
            ]
        ],
        2 => [
            'title' => Yii::t('wizard', 'Address'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'address-step'
                ],
            ]
        ],
        3 => [
            'title' => Yii::t('wizard', 'Additional Contact Info'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'dynamic-contact-step',
                ],
            ]
        ],
        4 => [
            'title' => Yii::t('wizard', 'Company banner and logo'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'photo-step'
                ],
            ]
        ],
    ],
];
