<?php

return [
    'property' => [
        'default' => [
            1 => [
                'title' => Yii::t('wizard', 'Address'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'address-step'
                    ],
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'address_additional',
                        ]
                    ]
                ]
            ],
            2 => [
                'title' => Yii::t('wizard', 'Property Parameters'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'attributes-step'
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('wizard', 'Price and terms of deal'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'price-step',
                    ],
                ]
            ],
            4 => [
                'title' => Yii::t('wizard', 'Photo and Video'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'photo-step'
                    ]
                ]
            ],
            5 => [
                'title' => Yii::t('wizard', 'Additional Information'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ]
                ]
            ],
        ],
        'flats' => [
            1 => [
                'title' => Yii::t('wizard', 'Address'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'address-step'
                    ],
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'address_additional',
                        ]
                    ]
                ]
            ],
            2 => [
                'title' => Yii::t('wizard', 'Property Parameters'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'area',
                            'ceiling',
                            'rooms',
                            'bathrooms',
                            'floor',
                            'additional',
                            'balconies',
                        ]
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('wizard', 'Price and terms of deal'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'price-step',
                    ],
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'price'
                        ]
                    ]
                ]
            ],
            4 => [
                'title' => Yii::t('wizard', 'Photo and Description'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'photo-step'
                    ],
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ]
                ]
            ],
            5 => [
                'title' => Yii::t('wizard', 'Building Details'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'building',
                        ]
                    ],
                    [
                        'type' => 'view',
                        'value' => 'building-step'
                    ]
                ]
            ],
        ],
        'houses' => [
            1 => [
                'title' => Yii::t('wizard', 'Address'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'address-step'
                    ],
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'home_address_additional',
                        ]
                    ]
                ]
            ],
            2 => [
                'title' => Yii::t('wizard', 'House Parameters'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'primary',
                            'years',
                            'area',
                            'rooms',
                            'bathrooms',
                            'heatcool',
                            'furniture',
                            'layout',
                            'condition',
                            'amenities',
                            'balconies',
                            'garages',
                        ]
                    ]
                ]
            ],
            3 => [
                'title' => Yii::t('wizard', 'Price and terms of deal'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'price-step',
                    ],
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'price'
                        ]
                    ]
                ]
            ],
            4 => [
                'title' => Yii::t('wizard', 'Photo and Description'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'photo-step'
                    ],
                    [
                        'type' => 'view',
                        'value' => 'info-step'
                    ]
                ]
            ],
            5 => [
                'title' => Yii::t('wizard', 'Residential complex parameters and description'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'village-step'
                    ],
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'village'
                        ]
                    ]
                ]
            ],
            6 => [
                'title' => Yii::t('wizard', 'Stead'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'stead'
                        ]
                    ]
                ]
            ],
            7 => [
                'title' => Yii::t('wizard', 'Seller Status'),
                'config' => [
                    [
                        'type' => 'constructor',
                        'groups' => [
                            'status'
                        ]
                    ]
                ]
            ],
        ],
    ],
];
