<?php

return [
    'user' => [
        1 => [
            'title' => Yii::t('wizard', 'Contact Info'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'contact-step'
                ],
            ]
        ],
        2 => [
            'title' => Yii::t('wizard', 'Additional Contact Info'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'dynamic-contact-step',
                ],
            ]
        ],
        3 => [
            'title' => Yii::t('wizard', 'Main Info'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'photo-step'
                ],
                [
                    'type' => 'view',
                    'value' => 'info-step'
                ]
            ]
        ],
        4 => [
            'title' => Yii::t('wizard', 'Location Settings'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'address-step'
                ]
            ]
        ],
        5 => [
            'title' => Yii::t('wizard', 'Password Settings'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'password-step'
                ]
            ]
        ],
        6 => [
            'title' => Yii::t('wizard', 'Social Settings'),
            'config' => [
                [
                    'type' => 'view',
                    'value' => 'social-step'
                ]
            ]
        ],
    ],
];
