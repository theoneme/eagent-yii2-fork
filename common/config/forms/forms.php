<?php

return yii\helpers\ArrayHelper::merge(
    require __DIR__ . '/property.php',
    require __DIR__ . '/building.php',
    require __DIR__ . '/request.php',
    require __DIR__ . '/user.php',
    require __DIR__ . '/company.php'
);