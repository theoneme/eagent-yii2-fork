<?php

namespace common\helpers;

/**
 * Class SpatialHelper
 * @package common\helpers
 */
class SpatialHelper
{
    /**
     * @param $point
     * @return string
     */
    public static function stringToPoint($point)
    {
        return implode(' ', $point);
    }

    /**
     * @param $points
     * @return string
     */
    public static function stringToPoints($points)
    {
        return implode(',', array_map(function ($v) {
            return static::stringToPoint($v);
        }, $points));
    }

    /**
     * @param $lines
     * @return string
     */
    public static function stringToLines($lines)
    {
        return implode(',', array_map(function ($v) {
            return '(' . static::stringToPoints($v) . ')';
        }, $lines));
    }

    /**
     * @param $point
     * @return array
     */
    public static function pointToString($point)
    {
        return array_map('floatval', explode(' ', $point));
    }

    /**
     * @param $points
     * @return array
     */
    public static function pointsToString($points)
    {
        return array_map(function ($v) {
            return static::pointToString($v);
        }, $points);
    }

    /**
     * @param $lines
     * @return array
     */
    public static function linesToString($lines)
    {
        return array_map(function ($v) {
            $matches = [];
            return preg_match_all('/([-\d. ]+)/', $v, $matches) ? static::pointsToString($matches[1]) : [];
        }, $lines);
    }

    /**
     * @param $pointList
     * @param $distance
     * @return array
     */
    public static function simplifyPolygon($pointList, $distance)
    {
        if ($distance <= 0 || count($pointList) < 2) {
            return $pointList;
        }
        // Find the point with the maximum distance
        $dmax = 0;
        $index = 0;
        $totalPoints = count($pointList);
        for ($i = 1; $i < ($totalPoints - 1); $i++) {
            $d = self::perpendicularDistance(
                $pointList[$i][0], $pointList[$i][1],
                $pointList[0][0], $pointList[0][1],
                $pointList[$totalPoints - 1][0],
                $pointList[$totalPoints - 1][1]);
            if ($d > $dmax) {
                $index = $i;
                $dmax = $d;
            }
        }
        // If max distance is greater than epsilon, recursively simplify
        if ($dmax * 6371 >= $distance) {
            // Recursive call on each 'half' of the polyline
            $results1 = self::simplifyPolygon(array_slice($pointList, 0, $index + 1), $distance);
            $results2 = self::simplifyPolygon(array_slice($pointList, $index, $totalPoints - $index), $distance);
            // Build the result list
            $result = array_merge(
                array_slice($results1, 0, count($results1) - 1),
                array_slice($results2, 0, count($results2))
            );
        } else {
            $result = array($pointList[0], $pointList[$totalPoints - 1]);
        }
        // Return the result
        return $result;
    }

    /**
     * @param $ptX
     * @param $ptY
     * @param $l1x
     * @param $l1y
     * @param $l2x
     * @param $l2y
     * @return float|int|number
     */
    public static function perpendicularDistance($ptX, $ptY, $l1x, $l1y, $l2x, $l2y)
    {
        if ($l2x === $l1x) {
            //vertical lines - treat this case specially to avoid dividing
            //by zero
            $result = abs($ptX - $l2x);
        } else {
            $slope = (($l2y - $l1y) / ($l2x - $l1x));
            $passThroughY = (0 - $l1x) * $slope + $l1y;
            $result = (abs(($slope * $ptX) - $ptY + $passThroughY)) / (sqrt($slope * $slope + 1));
        }
        return $result;
    }
}