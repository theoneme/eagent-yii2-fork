<?php

namespace common\helpers;

use common\mappers\CategoryTreeMapper;
use common\models\UserContact;
use common\services\entities\CategoryService;
use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\RegionService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DataHelper
 * @package common\helpers
 */
class DataHelper
{
    /**
     * @param string $indexBy
     * @return array
     */
    public static function getCategoryTree($indexBy = 'id')
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = $categoryService->getMany(['lvl' => [1, 2, 3]], ['orderBy' => 'lft']);

        return CategoryTreeMapper::getMappedData($categories['items'], $indexBy);
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function getCategories()
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = $categoryService->getMany(['lvl' => 1], ['orderBy' => 'lft']);

        return ArrayHelper::map($categories['items'], 'id', 'title');
    }

    /**
     * @param $parentId
     * @return array
     */
    public static function getCategoryChildren($parentId)
    {
        /** @var CategoryService $categoryService */
        $categoryService = Yii::$container->get(CategoryService::class);
        $categories = $categoryService->getMany(['parent_id' => $parentId], ['orderBy' => 'lft']);

        return ArrayHelper::map($categories['items'], 'id', 'title');
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function getCountries()
    {
        /** @var CategoryService $categoryService */
        $countryService = Yii::$container->get(CountryService::class);
        $countries = $countryService->getMany([]);

        return ArrayHelper::map($countries['items'], 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getLocationTree()
    {
        /** @var CountryService $countryService */
        $countryService = Yii::$container->get(CountryService::class);

        /** @var RegionService $regionService */
        $regionService = Yii::$container->get(RegionService::class);

        /** @var CityService $cityService */
        $cityService = Yii::$container->get(CityService::class);

        $data = [];

        $countries = $countryService->getMany([]);
        foreach ($countries['items'] as $country) {
            $data[$country['title']] = [
                'title' => $country['title'],
                'slug' => $country['slug']
            ];
            $regions = $regionService->getMany(['country_id' => $country['id']]);

            if (empty($regions['items'])) {
                unset($data[$country['title']]);
                continue;
            }
            foreach ($regions['items'] as $region) {
                $data[$country['title']]['regions'][$region['id']] = [
                    'title' => $region['title'],
                    'slug' => $region['slug']
                ];
                $cities = $cityService->getMany(['region_id' => $region['id']]);
                if (empty($cities['items'])) {
                    unset($data[$country['title']]['regions'][$region['id']]);
                    continue;
                }

                foreach ($cities['items'] as $city) {
                    $data[$country['title']]['regions'][$region['id']]['cities'][$city['id']] = [
                        'title' => $city['title'],
                        'slug' => $city['slug']
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public static function getCurrencies()
    {
        return array_map(function ($currency) {
            return $currency['title'];
        }, Yii::$app->params['currencies']);
    }

    /**
     * @param bool $sorted
     * @return mixed
     */
    public static function getLocales($sorted = false)
    {
        if ($sorted === false) {
            return Yii::$app->params['languages'];
        }

        $locales = Yii::$app->params['languages'];

        uksort($locales, function ($a) {
            return $a === Yii::$app->language ? -1 : 1;
        });

        return $locales;
    }

    /**
     * @return array
     */
    public static function getMonths()
    {
        return [
            1 => Yii::t('main', 'January'),
            Yii::t('main', 'February'),
            Yii::t('main', 'March'),
            Yii::t('main', 'April'),
            Yii::t('main', 'May'),
            Yii::t('main', 'June'),
            Yii::t('main', 'July'),
            Yii::t('main', 'August'),
            Yii::t('main', 'September'),
            Yii::t('main', 'October'),
            Yii::t('main', 'November'),
            Yii::t('main', 'December'),
        ];
    }

    /**
     * @return array
     */
    public static function getContacts()
    {
        return [
            UserContact::TYPE_EMAIL => 'Email',
            UserContact::TYPE_PHONE => Yii::t('labels', 'Phone'),
            UserContact::TYPE_WEBSITE => Yii::t('labels', 'Website'),
            UserContact::TYPE_SKYPE => 'Skype',
            UserContact::TYPE_WHATSAPP => 'WhatsApp',
            UserContact::TYPE_VIBER => 'Viber',
            UserContact::TYPE_TELEGRAM => 'Telegram',
            UserContact::TYPE_FACEBOOK => 'Facebook',
            UserContact::TYPE_VK => 'Vk',
            UserContact::TYPE_TWITTER => 'Twitter',
            UserContact::TYPE_GOOGLE => 'Google',
            UserContact::TYPE_YOUTUBE => 'Youtube',
            UserContact::TYPE_OK => Yii::t('labels', 'Odnoklassniki'),
        ];
    }

    /**
     * @return array
     */
    public static function getQuizIntervals()
    {
        return [
            Yii::t('main', '{months} months', ['months' => '0-3']),
            Yii::t('main', '{months} months', ['months' => '3-6']),
            Yii::t('main', '{months} months', ['months' => '6-12']),
            Yii::t('main', '{months} months', ['months' => '12+']),
            Yii::t('main', 'I`m not sure'),
        ];
    }

    /**
     * @param $data
     * @return array
     */
    public static function getBuildingProgressData($data)
    {
        $progressPeriods = ArrayHelper::map($data, 'quarter', 'id', 'year');
        $years = [];
        foreach ($progressPeriods as $year => $quarters) {
            $years[$year] = Yii::t('building', '{year} year', ['year' => $year]);
        }
        $quarters = array_map(function ($var) {
            return Yii::t('building', '{quarter} quarter', ['quarter' => $var]);
        }, [1 => 1, 2 => 2, 3 => 3, 4 => 4]);
        $firstYear = array_values(array_keys($progressPeriods))[0];
        $firstQuarter = array_values(array_keys($progressPeriods[$firstYear]))[0];

        return [
            'periods' => $progressPeriods,
            'years' => $years,
            'quarters' => $quarters,
            'firstYear' => $firstYear,
            'firstQuarter' => $firstQuarter,
        ];
    }

    /**
     * @return array
     */
    public static function getCatalogItemAttributes()
    {
        return [
            'rooms' => [
                'template' => '{rooms, plural, one{# room} other{# rooms}}',
                'param' => 'rooms',
            ],
            'bedrooms' => [
                'template' => '{bedrooms, plural, one{# bedroom} other{# bedrooms}}',
                'param' => 'bedrooms',
            ],
            'year_built' => [
                'template' => '{year}',
                'param' => 'year',
            ],
            'property_area' => [
                'template' => 'Area {area} m²',
                'param' => 'area',
            ],
            'kitchen_area' => [
                'template' => 'Kitchen {area} m²',
                'param' => 'area',
            ],
        ];
    }
}