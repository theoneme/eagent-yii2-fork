<?php

namespace common\helpers;
use Yii;

/**
 * Class Morpheus
 * @package common\helpers
 */
class Morpheus
{
    public const GENDER_MALE = 0;
    public const GENDER_FEMALE = 10;
    public const GENDER_MIDDLE = 20;
    public const GENDER_PLURAL = 30;

    public const CASE_NOMINATIVE = 0;      // Именительный
    public const CASE_GENITIVE = 10;       // Родительный
    public const CASE_ACCUSATIVE = 20;     // Винительный
    public const CASE_DATIVE = 30;         // Дательный
    public const CASE_INSTRUMENTAL = 40;   // Творительный
    public const CASE_PREPOSITIONAL = 50;  // Предложный

    /**
     * @param $category
     * @param $message
     * @param $gender
     * @param $case
     * @param array $params
     * @param null $language
     * @return mixed
     */
    public static function t($category, $message, $gender, $case, array $params = [], $language = null){
        $language = $language ?? Yii::$app->language;
        $messageSourcePath = Yii::getAlias('@common') . "/morpheus-messages/$language/$category.php";
        if (file_exists($messageSourcePath)) {
            $messageSource = include $messageSourcePath;
            if (!empty($messageSource[$message][$gender][$case])) {
                $result = $messageSource[$message][$gender][$case];
                $placeholders = [];
                foreach ($params as $name => $value) {
                    $placeholders['{' . $name . '}'] = $value;
                }
                return preg_replace('/{.*?}/', '', ($placeholders === []) ? $result : strtr($result, $placeholders));
            }
        }
        return Yii::t($category, $message, $params, $language);
    }
}