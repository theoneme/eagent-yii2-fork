<?php

namespace common\helpers;

use Yii;

/**
 * Class GoogleStaticMapHelper
 * @package common\helpers
 */
class GoogleStaticMapHelper
{
    /**
     * @param $lat
     * @param $lng
     * @param $width
     * @param $height
     * @param null $locale
     * @param bool $naeb
     * @return string
     */
    public static function getMapImage($lat, $lng, $width, $height, $locale = null, $naeb = true)
    {
        if ($naeb === true) {
            $centerLng = (float)$lng - 0.02;
        } else {
            $centerLng = $lng;
        }

        $language = Yii::$app->params['supportedLocales'][$locale ?? Yii::$app->language];
        return 'https://maps.googleapis.com/maps/api/staticmap?' . http_build_query([
                'center' => "{$lat},{$centerLng}",
                'markers' => "{$lat},{$lng}",
                'size' => "{$width}x{$height}",
                'key' => Yii::$app->params['googleAPIKey'],
                'language' => $language,
                'zoom' => 12
            ]);
    }
}