<?php

namespace common\helpers;

use common\repositories\sql\CategoryRepository;
use Yii;

/**
 * Class FormHelper
 * @package common\helpers
 */
class FormHelper
{
    public const ENTITY_PROPERTY = 'property';
    public const ENTITY_USER = 'user';
    public const ENTITY_COMPANY = 'company';
    public const ENTITY_BUILDING = 'building';
    public const ENTITY_REQUEST = 'request';

    /**
     * @param $entity
     * @param $category
     * @return mixed
     */
    public static function getFormConfig($entity, $category = null)
    {
        $formConfig = require dirname(__DIR__, 1) . "/config/forms/{$entity}.php";

        switch ($entity) {
            case self::ENTITY_COMPANY:
            case self::ENTITY_USER:
                $config = $formConfig[$entity];

                break;
            case self::ENTITY_PROPERTY:
            case self::ENTITY_REQUEST:
                /** @var CategoryRepository $categoryRepository */
                $categoryRepository = Yii::$container->get(CategoryRepository::class);

                $category = $categoryRepository->findOneByCriteria(['id' => $category]);
                $config = array_key_exists($category['form_group'], $formConfig[$entity])
                    ? $formConfig[$entity][$category['form_group']]
                    : $formConfig[$entity]['default'];

                break;
            case self::ENTITY_BUILDING:
            default:
                $config = array_key_exists($category, $formConfig[$entity])
                    ? $formConfig[$entity][$category]
                    : $formConfig[$entity]['default'];

                break;

        }

        return $config;
    }
}