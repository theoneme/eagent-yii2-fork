<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2018
 * Time: 13:52
 */

namespace common\helpers;

use Yii;
use yii\helpers\BaseFileHelper;

/**
 * Class FileHelper
 * @package common\helpers
 */
class FileHelper extends BaseFileHelper
{
    /**
     * @param string $context
     * @return array
     * @throws \yii\base\Exception
     */
    public static function createTempDirectory($context = '@app')
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $rootDirectory = Yii::getAlias($context);
        $relativePath = "/uploads/temp/{$year}/{$month}/{$day}/";
        $basePath = "{$rootDirectory}/web{$relativePath}";

        self::createDirectory($basePath);

        return [
            'relativePath' => $relativePath,
            'basePath' => $basePath
        ];
    }

    /**
     * @param string $context
     * @return bool|string
     */
    public static function getContextPath($context = '@app')
    {
        return Yii::getAlias($context);
    }

    /**
     * @param string $context
     * @return string
     */
    public static function getPublicContextPath($context = '@app')
    {
        return Yii::getAlias($context) . '/web';
    }

    /**
     * @param $from
     * @param $to
     */
    public static function webpToJpg($from, $to)
    {
        $im = imagecreatefromwebp($from);
        imagejpeg($im, $to, 100);
        imagedestroy($im);
        unlink($from);
    }

    /**
     * @param $from
     * @param null $dir
     * @return null|string
     */
    public static function downloadFile($from, $dir = null)
    {
        $dir = $dir ?? self::createTempDirectory('@frontend');
        $headers = get_headers($from);
        if (strpos($headers[0], '200') !== false) {
            $file = file_get_contents($from);
            if ($file) {
                $pathInfo = pathinfo($from);
                $hash = hash('crc32b', uniqid($pathInfo['filename'], true));
                $extension = (!empty($pathInfo['extension']) ? '.' . $pathInfo['extension'] : '');
                $basename = $hash . $extension;
                $newAbsolutePath = $dir['basePath'] . $basename;
                $newRelativePath = $dir['relativePath'] . $basename;
                file_put_contents($newAbsolutePath, $file);
                if (exif_imagetype($newAbsolutePath) === IMAGETYPE_WEBP) {
                    $jpgName = "$hash.jpg";
                    self::webpToJpg($newAbsolutePath, $dir['basePath'] . $jpgName);
                    $newRelativePath = $dir['relativePath'] . $jpgName;
                }
                return $newRelativePath;
            }
        }
        return null;
    }
}