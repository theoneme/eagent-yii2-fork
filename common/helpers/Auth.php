<?php

namespace common\helpers;

use common\models\User;
use Yii;

/**
 * Class Auth
 * @package common\helpers
 */
class Auth
{
    /**
     * @return User
     */
    public static function user()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        return $user;
    }
}