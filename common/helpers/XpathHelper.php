<?php

namespace common\helpers;

class XpathHelper
{
    /**
     * @param $string
     * @return mixed
     */
    public static function cssToXpath($string)
    {
        $string = preg_replace("/\#([\w-]+)/i", '[@id="$1"]', $string);
        $string = preg_replace("/\.([\w-]+)/i", "[contains(concat(' ',normalize-space(@class),' '),' $1 ')]", $string);
        return $string;
    }
}