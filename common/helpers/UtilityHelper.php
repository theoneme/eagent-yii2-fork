<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.08.2018
 * Time: 13:22
 */

namespace common\helpers;

use Yii;
use yii\console\Application;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\AssetBundle;

class UtilityHelper
{
    /**
     * @param $inputString
     * @return string
     */
    public static function transliterate($inputString)
    {
        $text = preg_replace("%(www\.)|(http(s)?:\/\/)|[^.,єіїáéíóúüña-zа-яა-ჸ0-9-\s]%ui", '', mb_strtolower($inputString));

        $text = trim($text);
        $transliterations = array_merge(
            self::$russianTransliterations,
            self::$georgianTransliterations,
            self::$ukrainianTransliterations,
            self::$spanishTransliterations,
            [
                ' ' => '-',
                '_' => '-',
                '.' => '-',
                ',' => '-'
            ]
        );
        if (preg_match("/[.,єіїáéíóúüñа-яა-ჸ\s+]/ui", $text)) {
            $text = strtr($text, $transliterations);
        }
        $text = preg_replace("/[\s]/", '-', $text);
        $text = preg_replace("/\-{2,}/", '-', $text);

        return trim($text);
    }

    /**
     * @param $dir
     * @return bool
     * @throws \RuntimeException
     */
    public static function makeDir($dir)
    {
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }

            if (Yii::$app instanceof Application) {
                chown($dir, 'www-data');
                chgrp($dir, 'www-data');
            }

            return true;
        }

        return false;
    }

    /**
     * @param $input
     * @return string
     */
    public static function lowerFirstLetter($input)
    {
        return strlen($input) > 0 ? (mb_strtolower(mb_substr($input, 0, 1)) . mb_substr($input, 1)) : null;
    }

    /**
     * @param $input
     * @return string
     */
    public static function upperFirstLetter($input)
    {
        return strlen($input) > 0 ? (mb_strtoupper(mb_substr($input, 0, 1)) . mb_substr($input, 1)) : null;
    }

    /**
     * Generate slug of specified length
     *
     * @param $text
     * @param int $wordsLimit
     * @param int $charactersLimit
     * @param string $uniqueId
     * @return string
     */
    public static function generateSlug($text, $wordsLimit = 5, $charactersLimit = 55, $uniqueId = null)
    {
        $uniqueId = $uniqueId ?? uniqid();
        $transliterated = self::transliterate(preg_replace('/\s+/', ' ', StringHelper::truncateWords($text, $wordsLimit, '')));
        $slug = preg_replace("/\-{2,}/", '-', StringHelper::truncate($transliterated, $charactersLimit - 14, '') . '-' . $uniqueId);

        return $slug;
    }

    /**
     * @param $assetClass
     * @return string
     */
    public static function getPublishedAssetPath($assetClass)
    {
        /** @var AssetBundle $assetClass */
        $assetClass = new $assetClass;

        return Yii::$app->assetManager->getPublishedUrl($assetClass->sourcePath);
    }

    /**
     * @param $array
     * @return mixed
     */
    public static function hasUpperCase($array)
    {
        return array_reduce($array, function ($carry, $value) {
            return $carry || (is_array($value) ? self::hasUpperCase($value) : preg_match('/[A-Z]/u', $value));
        }, false);
    }

    /**
     * @param $what
     * @param bool $die
     */
    public static function debug($what, $die = true)
    {
        echo '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title></title></head><body>';
        VarDumper::dump($what, 115, true);
        if ($die) {
            die();
        }
        echo '</body></html>';
    }

    /**
     * @param $condition
     * @param $tableName
     * @param $repository
     * @return mixed
     */
    public static function fixAmbiguousCondition($condition, $tableName, $repository)
    {
        $result = $condition;

        if (strpos(get_class($repository), 'sql')) {
            foreach (['id', 'status', 'type', 'locale', 'country_id', 'user_id'] as $field) {
                if (array_key_exists($field, $result)) {
                    $result["{$tableName}.{$field}"] = $result[$field];
                    unset($result[$field]);
                }
            }
        }

        return $result;
    }

    /**
     * @param $code
     * @return null|string|string[]
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function localeCurrentUrl($code)
    {
        $locale = array_search($code, Yii::$app->params['supportedLocales'], true);
        if ($locale === false || $locale === Yii::$app->language) {
            return Url::current();
        }
        $params = ['app_language' => $code];
        $alias = Yii::$app->request->get('category');
        if ($alias !== null) {
            $categoryAliases = Yii::$app->cacheLayer->getCategoryAliasCache(Yii::$app->language);

            if (isset($categoryAliases[$alias][$locale])) {
                $params['category'] = $categoryAliases[$alias][$locale];
            } else {
                return ($code === 'ru' ? '/ru' : Url::to(['/site/index', 'app_language' => $code]));
            }
        }

        $url = Url::current($params);
        $url = $url === '/' ? '' : $url;
        if ($code === 'ru') {
            $url = preg_replace("#^(\/\/[\w-]+\." . preg_quote(Yii::$app->params['domainName']) . ')?(.*)#', '$1/ru$2', $url);
        }
        return $url;
    }

    /**
     * @return string
     */
    public static function debugMemory()
    {
        return memory_get_peak_usage() / 1024 / 1024 . 'MB.';
    }

    /**
     * @param null $email
     * @param null $phone
     * @return null
     */
    public static function generateUsername($email = null, $phone = null)
    {
        return !empty($email) ? explode('@', $email)[0] : $phone;
    }

    public static $russianTransliterations = [
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'e',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'y',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'kh',
        'ц' => 'ts',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'shch',
        'ы' => 'y',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        'ь' => '',
        'ъ' => '',
    ];

    public static $georgianTransliterations = [
        'ა' => 'a',
        'ბ' => 'b',
        'გ' => 'g',
        'დ' => 'd',
        'ე' => 'e',
        'ვ' => 'v',
        'ზ' => 'z',
        'ჱ' => '',
        'თ' => 't',
        'ი' => 'i',
        'კ' => 'k',
        'ლ' => 'l',
        'მ' => 'm',
        'ნ' => 'n',
        'ჲ' => '',
        'ო' => 'o',
        'პ' => 'p',
        'ჟ' => 'zh',
        'რ' => 'r',
        'ს' => 's',
        'ტ' => 't',
        'ჳ' => '',
        'უ' => 'u',
        'ჷ' => '',
        'ფ' => 'p',
        'ქ' => 'k',
        'ღ' => 'gh',
        'ყ' => 'q',
        'ჸ' => '',
        'შ' => 'sh',
        'ჩ' => 'ch',
        'ც' => 'ts',
        'ძ' => 'dz',
        'წ' => 'ts',
        'ჭ' => 'ch',
        'ხ' => 'kh',
        'ჴ' => '',
        'ჯ' => 'j',
        'ჰ' => 'h',
        'ჵ' => '',
        'ჶ' => '',
    ];

    public static $ukrainianTransliterations = [
        'є' => 'e',
        'і' => 'i',
        'ї' => 'yi',
    ];

    public static $spanishTransliterations = [
        'á' => 'a',
        'é' => 'e',
        'í' => 'i',
        'ó' => 'o',
        'ú' => 'u',
        'ü' => 'u',
        'ñ' => 'n',
        '¿' => ' ',
        '¡' => ' ',
    ];
}