<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.11.2017
 * Time: 16:35
 */

namespace common\helpers;

use yii\helpers\Url;

/**
 * Class UrlAdvanced
 * @package common\helpers
 */
class UrlAdvanced extends Url
{
    /**
     * @param string $url
     * @param bool $scheme
     * @return bool|string
     */
    public static function to($url = '', $scheme = false)
    {
        $url = parent::to($url, $scheme);

        if (strpos($url, '//.') !== false) {
            $url = str_replace('//.', '//', $url);
        }

        return $url;
    }
}