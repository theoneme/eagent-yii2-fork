<?php

namespace common\helpers;

/**
 * Class ImageHelper
 * @package common\helpers
 */
class ImageHelper
{
    /**
     * @param int $width
     * @param int $height
     * @param string $filename
     * @param int $pointSize
     */
    public static function createRandomImage($width, $height, $filename, $pointSize = 1)
    {
        $image = imagecreate($width, $height);
        for($row = 0; $row <= $height / $pointSize; $row++) {
            for($column = 0; $column <= $width / $pointSize; $column++) {
                $colour = imagecolorallocate($image, rand(0,255) , rand(0,255), rand(0,255));
                imagerectangle($image,$column * $pointSize, $row * $pointSize,($column + 1) * $pointSize,($row + 1) * $pointSize, $colour);
            }
        }

        imagejpeg($image, $filename);
    }
}