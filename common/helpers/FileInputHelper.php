<?php

namespace common\helpers;

use Yii;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class FileInputHelper
 * @package common\helpers
 */
class FileInputHelper
{
    /**
     * @return array
     */
    public static function getDefaultConfig()
    {
        return [
            'name' => 'uploaded_images[]',
            'options' => ['accept' => 'image/*', 'class' => 'file-upload-input'],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/image/upload']),
                'allowedFileExtensions' => [
                    'jpg', 'gif', 'png', 'jpeg'
                ],
                'minImageHeight' => 100,
                'minImageWidth' => 100,
                'maxFileCount' => 25,
                'showPreview' => true,
                'dropZoneEnabled' => true,
                'initialPreviewAsData' => true,
                'showCancel' => false,
                'showRemove' => false,
                'showUpload' => false,
                'fileActionSettings' => [
                    'showZoom' => false,
                    'showDrag' => false,
                    'showUpload' => false
                ],
                'previewThumbTags' => [
                    '{actions}' => '{actions}',
                ],
                'uploadExtraData' => new JsExpression("
                    function (previewId, index) {
                        if (previewId !== undefined) {
                            var obj = $('[data-fileid=\"' + previewId + '\"]').data();
                            return obj;
                        }
                    }
                "),
                'otherActionButtons' =>
                    '<button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('main', 'Crop image') . '">
                        <i class="icon-crop" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('main', 'Rotate image') . '">
                        <i class="icon-rotate-left" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-default crop-button" title="' . Yii::t('main', 'Rotate image') . '">
                        <i class="icon-rotate-right" aria-hidden="true"></i>
                    </button>',
            ]
        ];
    }

    /**
     * @param array $attachments
     * @return array
     */
    public static function buildPreviews(array $attachments)
    {
        $result = [];

        foreach ($attachments as $attachment) {
            $result[] = Yii::$app->mediaLayer->getThumb($attachment['content']);
        }

        return $result;
    }

    /**
     * @param array $attachments
     * @return array
     */
    public static function buildPreviewsConfig(array $attachments)
    {
        $output = [];

        foreach ($attachments as $key => $attachment) {
            $output[] = [
                'caption' => basename($attachment['content']),
                'url' => Url::toRoute('/image/delete'),
                'key' => "image_init_{$key}"
            ];

        }
        return $output;
    }

    /**
     * @param $path
     * @return array
     */
    public static function buildOriginImagePath($path)
    {
        $pos = strpos($path, '/uploads');

        $path = substr($path, $pos);
        return $path;
    }
}