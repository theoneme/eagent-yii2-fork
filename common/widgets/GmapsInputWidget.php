<?php

namespace common\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;

/**
 * Class GmapsInputWidget
 * @package common\widgets
 */
class GmapsInputWidget extends Widget
{
    /**
     * @var array
     */
    public $inputOptions = [];
    /**
     * @var string
     */
    public $lat;
    /**
     * @var string
     */
    public $lng;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    private $_apiKey;
    /**
     * @var string
     */
    private $_language;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!array_key_exists('googleAPIKey', Yii::$app->params)) {
            throw new InvalidConfigException('Google API key is not set');
        }

        $this->_apiKey = Yii::$app->params['googleAPIKey'];

        $this->_language = Yii::$app->params['supportedLocales'][Yii::$app->language];
        if ($this->_language === 'ua') {
            $this->_language = 'uk';
        }

        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('gmaps-input-widget', [
            'inputOptions' => $this->inputOptions,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'address' => $this->address,
            'apiKey' => $this->_apiKey,
            'language' => $this->_language
        ]);
    }
}