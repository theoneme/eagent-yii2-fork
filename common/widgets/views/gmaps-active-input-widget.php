<?php
/**
 * @var $this View
 * @var $model \yii\base\Model
 * @var $inputOptions array
 * @var $fieldOptions array
 * @var $modal boolean
 */

use frontend\assets\plugins\GoogleAddressAsset;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var Model $model
 * @var string $address
 * @var string $apiKey
 * @var ActiveForm|null $form
 * @var boolean $withMap
 * @var string $addressAttribute
 * @var string $latAttribute
 * @var string $lngAttribute
 * @var string $widgetId
 */

GoogleAddressAsset::register($this);

$inputOptions = array_merge(
    ['id' => 'gmaps-input-address' . $widgetId, 'placeholder' => Yii::t('main', 'Enter Address')],
    $inputOptions
);
?>

<?php if ($form === null) { ?>
    <?= Html::activeHiddenInput($model, $latAttribute, ['id' => 'gmaps-input-lat' . $widgetId]) ?>
    <?= Html::activeHiddenInput($model, $lngAttribute, ['id' => 'gmaps-input-lng' . $widgetId]) ?>
    <?= Html::activeTextInput($model, $addressAttribute, $inputOptions); ?>
<?php } else { ?>
    <?= $form->field($model, $addressAttribute, $fieldOptions)->textInput($inputOptions)->label($inputOptions['label'] ?? null); ?>
    <?= $form->field($model, $latAttribute, ['template' => '{input}{error}'])->hiddenInput(['id' => 'gmaps-input-lat' . $widgetId])->label(false); ?>
    <?= $form->field($model, $lngAttribute, ['template' => '{input}{error}'])->hiddenInput(['id' => 'gmaps-input-lng' . $widgetId])->label(false); ?>
<?php } ?>

<?php
$lat = $model['lat'] ?? 56.83200970;
$lng = $model['lng'] ?? 60.59915200;
$script = <<<JS
    var widgetId = '$widgetId';
    var options = {
        withMap: Boolean({$withMap}),
        startLat: {$lat},
        startLng: {$lng},
        inputSelector: '#gmaps-input-address' + widgetId,
        latSelector: '#gmaps-input-lat' + widgetId,
        longSelector: '#gmaps-input-lng' + widgetId,
        addressSelector: '#gmaps-input-address' + widgetId,
        mapSelector: '#google-map' + widgetId,
    };
    
    $('#gmaps-input-address' + widgetId).GoogleAddressInput(options);
JS;

$this->registerJs($script);