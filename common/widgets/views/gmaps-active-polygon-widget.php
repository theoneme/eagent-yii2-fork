<?php

use common\assets\plugins\GooglePolygonAsset;
use yii\base\Model;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var Model[] $models
 * @var string $apiKey
 * @var ActiveForm|null $form
 * @var string $latAttribute
 * @var string $lngAttribute
 * @var string $widgetId
 * @var string $formName
 */

GooglePolygonAsset::register($this);

?>

<div id="gmaps-polygon-map-container" style='height: 360px; margin-bottom: 15px;'></div>
<div id="gmaps-polygon-actions-container">
    <div class="row">
        <div class="col-md-6">
            <?= Html::button(Yii::t('labels', 'Cancel Last Point'), ['class' => 'btn btn-block btn-warning', 'data-action' => 'gmaps-polygon-cancel']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::button(Yii::t('labels', 'Clear'), ['class' => 'btn btn-block btn-danger', 'data-action' => 'gmaps-polygon-clear']) ?>
        </div>
    </div>
</div>
<div id="gmaps-polygon-input-container" style="display: none">
    <?php
    $startPoints = [];
    foreach ($models as $index => $model) {
        $startPoints[] = ['lat' => $model[$latAttribute], 'lng' => $model[$lngAttribute]];
        echo Html::activeHiddenInput($model, "[$index]$latAttribute");
        echo Html::activeHiddenInput($model, "[$index]$lngAttribute");
    }?>
</div>

<?php
$startPoints = json_encode($startPoints);
$script = <<<JS
    var options = {
        containerSelector: '#gmaps-polygon-input-container',
        mapSelector: '#gmaps-polygon-map-container',
        actionsSelection: '#gmaps-polygon-actions-container',
        formName: '$formName',
        startPoints: $startPoints,
        markerIcon: '/images/simple-marker.png'
    };
    
    var polygonInput = new PolygonInput(options);
    polygonInput.attachListeners();
    // $('#gmaps-polygon-input-container').GooglePolygonInput(options);
JS;

$this->registerJs($script);