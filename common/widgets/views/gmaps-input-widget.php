<?php
/**
 * @var $this View
 * @var $model \yii\base\Model
 * @var $inputOptions array
 * @var $modal boolean
 */

use yii\helpers\Html;
use yii\web\View;

/**
 * @var string $lat
 * @var string $lng
 * @var string $address
 * @var string $apiKey
 */

\common\assets\GoogleAsset::register($this);

?>

<?= Html::hiddenInput('lat', $lat, ['id' => 'gmaps-input-lat']) ?>
<?= Html::hiddenInput('long', $lng, ['id' => 'gmaps-input-long']) ?>
<?= Html::textInput('address', $address,
    array_merge([
        'id' => 'gmaps-input-address',
        'placeholder' => 'Введите адрес',
    ], $inputOptions)
);

$script = <<<JS
    $('#gmaps-input-address').GoogleAddressInput();
JS;

$this->registerJs($script);