<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets;

use yii\base\Model;
use yii\helpers\Html;
use yii\widgets\ActiveForm as BaseActiveForm;

/**
 * ActiveForm is a widget that builds an interactive HTML form for one or multiple data models.
 *
 * For more details and usage information on ActiveForm, see the [guide article on forms](guide:input-forms).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CompositeActiveForm extends BaseActiveForm
{
    /**
     * @param Model $model
     * @param null $attributes
     * @return array
     */
    public static function validate($model, $attributes = null)
    {
        $result = [];
        if ($attributes instanceof Model) {
            // validating multiple models
            $models = func_get_args();
            $attributes = null;
        } else {
            $models = [$model];
        }

        /** @var $modelItem Model */
        foreach ($models as $modelItem) {
            $modelItem->validate($attributes);
            foreach ($modelItem->getErrors() as $attribute => $errors) {
                $inputId = Html::getInputId($modelItem, $attribute);
//                $inputId = str_replace('propertyform-', '', $inputId);
                $result[$inputId] = $errors;
            }
        }

        return $result;
    }
}
