<?php

namespace common\widgets;

use yii\base\Model;
use yii\base\Widget;
use yii\widgets\ActiveForm;

/**
 * Class GmapsActivePolygonWidget
 * @package common\widgets
 */
class GmapsActivePolygonWidget extends Widget
{
    /**
     * @var Model[]
     */
    public $models;
    /**
     * @var ActiveForm
     */
    public $form;
    /**
     * @var string
     */
    public $formName;
    /**
     * @var string
     */
    public $latAttribute = 'lat';
    /**
     * @var string
     */
    public $lngAttribute = 'lng';
    /**
     * @var string
     */
    public $widgetId;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('gmaps-active-polygon-widget', [
            'models' => $this->models,
            'form' => $this->form,
            'latAttribute' => $this->latAttribute,
            'lngAttribute' => $this->lngAttribute,
            'widgetId' => $this->widgetId,
            'formName' => $this->formName,
        ]);
    }
}