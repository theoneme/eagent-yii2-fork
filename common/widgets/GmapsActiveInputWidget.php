<?php

namespace common\widgets;

use yii\base\Model;
use yii\base\Widget;
use yii\widgets\ActiveForm;

/**
 * Class GmapsActiveInputWidget
 * @package frontend\widgets
 */
class GmapsActiveInputWidget extends Widget
{
    /**
     * @var Model
     */
    public $model;
    /**
     * @var ActiveForm
     */
    public $form;
    /**
     * @var array
     */
    public $inputOptions = [];
    /**
     * @var array
     */
    public $fieldOptions = [];
    /**
     * @var bool
     */
    public $withMap = false;
    /**
     * @var string
     */
    public $addressAttribute = 'address';
    /**
     * @var string
     */
    public $latAttribute = 'lat';
    /**
     * @var string
     */
    public $lngAttribute = 'lng';
    /**
     * @var string
     */
    public $widgetId;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('gmaps-active-input-widget', [
            'model' => $this->model,
            'inputOptions' => $this->inputOptions,
            'fieldOptions' => $this->fieldOptions,
            'form' => $this->form,
            'withMap' => $this->withMap,
            'addressAttribute' => $this->addressAttribute,
            'latAttribute' => $this->latAttribute,
            'lngAttribute' => $this->lngAttribute,
            'widgetId' => $this->widgetId,
        ]);
    }
}