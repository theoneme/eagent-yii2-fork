<?php 
 return [
    'Address' => 'მისამართი',
    'Advertising on Partner Sites Allowed' => 'დასაშვებია რეკლამის პარტნიორი საიტები',
    'Advertising on Site Allowed' => 'არის სარეკლამო დაშვებული ვერსია',
    'Alias' => '',
    'Attribute Group ID' => '',
    'Attribute ID' => '',
    'Avatar' => 'ავატარი',
    'Banner' => '',
    'Building ID' => '',
    'Category' => '',
    'Category ID' => '',
    'City Code' => '',
    'Code' => '',
    'Code From' => '',
    'Code To' => '',
    'Company' => '',
    'Company ID' => '',
    'Contact Type' => 'საკონტაქტო ტიპი',
    'Content' => '',
    'Contract Price' => 'ფასი შეთანხმებით',
    'Country' => '',
    'Country Code' => '',
    'Country ID' => '',
    'Created At' => '',
    'Currency Code' => 'ვალუტა',
    'Custom Data' => '',
    'Data' => '',
    'Description' => 'აღწერა',
    'Distance' => '',
    'Email' => '',
    'Entity' => '',
    'Entity Alias' => '',
    'Entity ID' => '',
    'File' => '',
    'Floors' => '',
    'From ID' => '',
    'ID' => '',
    'Image' => '',
    'Is Primary' => '',
    'Key' => '',
    'Lat' => '',
    'Lft' => '',
    'Lng' => '',
    'Locale' => '',
    'Logo' => '',
    'Lvl' => '',
    'Metadata' => '',
    'Name' => '',
    'New Password' => 'ახალი პაროლი',
    'Password Confirmation' => 'დაადასტურეთ პაროლი',
    'Phone' => 'ტელეფონი',
    'Price' => 'ფასი',
    'Property ID' => '',
    'Quarter' => 'კვარტალში',
    'Ratio' => '',
    'Region' => '',
    'Region Code' => '',
    'Region ID' => '',
    'Request ID' => '',
    'Rgt' => '',
    'Role' => 'როლი',
    'Root' => '',
    'Short Description' => 'მოკლე აღწერა',
    'Slug' => '',
    'Source' => '',
    'Source ID' => '',
    'Status' => 'სტატუსი',
    'Symbol Left' => '',
    'Symbol Right' => '',
    'Time' => '',
    'Title' => 'დასახელება',
    'To ID' => '',
    'Transport' => '',
    'Type' => '',
    'Updated  At' => '',
    'Updated At' => '',
    'Url' => '',
    'User' => 'მომხმარებელი',
    'User ID' => '',
    'Username' => 'მომხმარებლის სახელი',
    'Value' => 'ღირებულება',
    'Value Alias' => '',
    'Value Number' => '',
    'Year' => 'წელი',
    'Zip Code' => 'საფოსტო',
    'lng' => '',
    'City' => 'ქალაქი',
    'Company Type' => 'ტიპის კომპანია',
];