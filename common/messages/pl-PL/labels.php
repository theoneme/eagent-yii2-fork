<?php 
 return [
    'Active' => 'Aktywny',
    'Address' => 'Adres',
    'Address of your property' => 'Adres nieruchomości',
    'Administrator' => 'Administrator',
    'Airport' => 'Lotnisko',
    'Approved' => 'Zatwierdzony',
    'Area' => 'Powierzchnia',
    'Available' => 'Dostępne',
    'Building permit' => 'Pozwolenie na budowę',
    'Checkbox' => 'Чекбокс',
    'City' => 'Miasto',
    'City Center' => 'Centrum miasta',
    'Company Owner' => 'Właściciel firmy',
    'Content Manager' => 'Content manager',
    'Country' => 'Kraj',
    'Credit Amount' => 'Kredyt',
    'Currency' => 'Waluta',
    'Date' => 'Data',
    'Disabled' => 'Wyłączone',
    'Do you need mortgage?' => 'Potrzebujesz kredytu hipotecznego?',
    'Do you plan to buy condo?' => 'Planujesz kupić mieszkanie?',
    'Do you plan to sell secondary property?' => 'Planujesz sprzedać na rynku wtórnym?',
    'Dropdown' => 'Rozwijana lista',
    'Enter client`s email address' => 'Wprowadź adres E-mail klienta',
    'Enter client`s first name' => 'Wpisz nazwę klienta',
    'Enter client`s last name' => 'Wprowadź nazwisko klienta',
    'Enter client`s middle name' => 'Wprowadź imię klienta',
    'Enter client`s phone' => 'Wprowadź numer telefonu klienta',
    'Enter offer price' => 'Wpisz proponowaną cenę',
    'Enter the area of your property' => 'Wprowadź powierzchnia nieruchomości',
    'Enter the number of rooms' => 'Wpisz ilość pokoi',
    'Enter your address' => 'Wpisz swój adres',
    'Enter your age' => 'Wpisz swój wiek',
    'Enter your email address' => 'Wpisz swój adres e-mail',
    'Enter your name' => 'Wpisz swoje imię i nazwisko',
    'Enter your phone' => 'Wpisz swój telefon',
    'Finished' => 'Skończona',
    'First Name' => 'Nazwa',
    'How many years do you plan to repay the loan' => 'Na ile lat zamierzasz wziąć kredyt',
    'I want to get approval from the bank to the property' => 'Chcę otrzymać zgodę od banku na nieruchomości',
    'Invited' => 'Zaproszony',
    'Last Name' => 'Nazwisko',
    'Loan Term (years)' => 'Data заема (lat)',
    'Login' => 'Login',
    'Metro' => 'Metra',
    'Middle Name' => 'Imię',
    'Name' => 'Nazwa',
    'Not specified' => 'Nie podano',
    'Number' => 'Liczba',
    'Odnoklassniki' => 'Koledzy',
    'Operation act' => 'Akt o operacji',
    'Park' => 'Park',
    'Password' => 'Hasło',
    'Pending' => 'W oczekiwaniu na',
    'Phone' => 'Telefon',
    'Pond' => 'Staw',
    'Project declaration' => 'Projektowa deklaracja',
    'Published' => 'Opublikowany',
    'Region' => 'Region',
    'Remember me next time' => 'Zapamiętaj mnie',
    'Requires moderation' => 'Wymaga moderacji',
    'Requires modification' => 'Wymaga zmiany',
    'Rooms' => 'Liczba pokoi',
    'School' => 'Szkoła',
    'Set the date for showcase' => 'Wprowadź datę pokazu',
    'Set the time when you wil be able to come to showcase' => 'Wprowadź czas, kiedy można przyjść na pokaz',
    'Sold' => 'Sprzedane',
    'Start typing name' => 'Zacznij wpisywać nazwę',
    'String' => 'Wiersz',
    'Support' => 'Wsparcie',
    'Time' => 'Czas',
    'Unavailable' => 'Niedostępne',
    'Unfinished' => 'Неокончен',
    'Unknown role' => 'Nieznana rola',
    'Unknown status' => 'Stan nieznany',
    'Unknown transport' => 'Nieznany transport',
    'Unknown type' => 'Nieznany typ',
    'Website' => 'Strona www',
    'When do you plan on buying a property' => 'Gdy planujesz zakup nieruchomości?',
    'Your Age' => 'Twój wiek',
    'Your Email' => 'Twój adres e-mail',
    'Your Family Status' => 'Twój status rodzinny',
    'Your Message' => 'Wiadomość',
    'Your Name' => 'Twoje imię',
    'Your Phone' => 'Telefon',
    'Your Price' => 'Twoja cena',
    'on car' => 'samochodem',
    'on city transport' => 'w transporcie miejskim',
    'on foot' => 'pieszo',
    'Refused' => 'Odrzucony',
    'Paid' => 'Opłacony',
    'Account replenishment' => 'Dodawanie konta',
    'Payment for tariff' => 'Płatność taryfy',
    'Cancel Last Point' => 'Anulować ostatni punkt',
    'Clear' => 'Wyczyścić',
    'Import' => 'Import',
    'Agency' => 'Agencja',
    'Developer' => 'Deweloper',
    'Owner' => 'Właściciel',
    'Realtor' => 'Nieruchomości',
];