<?php 
 return [
    'Address' => '地址',
    'Advertising on Partner Sites Allowed' => '允许合作伙伴网站上广告',
    'Advertising on Site Allowed' => '是广告允许的门户网站上的',
    'Alias' => '',
    'Attribute Group ID' => '',
    'Attribute ID' => '',
    'Avatar' => '头像',
    'Banner' => '',
    'Building ID' => '',
    'Category' => '',
    'Category ID' => '',
    'City Code' => '',
    'Code' => '',
    'Code From' => '',
    'Code To' => '',
    'Company' => '',
    'Company ID' => '',
    'Contact Type' => '联系的类型',
    'Content' => '',
    'Contract Price' => '可转让价格',
    'Country' => '',
    'Country Code' => '',
    'Country ID' => '',
    'Created At' => '',
    'Currency Code' => '货币',
    'Custom Data' => '',
    'Data' => '',
    'Description' => '描述',
    'Distance' => '',
    'Email' => '',
    'Entity' => '',
    'Entity Alias' => '',
    'Entity ID' => '',
    'File' => '',
    'Floors' => '',
    'From ID' => '',
    'ID' => '',
    'Image' => '',
    'Is Primary' => '',
    'Key' => '',
    'Lat' => '',
    'Lft' => '',
    'Lng' => '',
    'Locale' => '',
    'Logo' => '',
    'Lvl' => '',
    'Metadata' => '',
    'Name' => '',
    'New Password' => '新密码',
    'Password Confirmation' => '确认密码',
    'Phone' => '电话',
    'Price' => '价格',
    'Property ID' => '',
    'Quarter' => '季度',
    'Ratio' => '',
    'Region' => '',
    'Region Code' => '',
    'Region ID' => '',
    'Request ID' => '',
    'Rgt' => '',
    'Role' => '的作用',
    'Root' => '',
    'Short Description' => '简短的说明',
    'Slug' => '',
    'Source' => '',
    'Source ID' => '',
    'Status' => '状态',
    'Symbol Left' => '',
    'Symbol Right' => '',
    'Time' => '',
    'Title' => '名称',
    'To ID' => '',
    'Transport' => '',
    'Type' => '',
    'Updated  At' => '',
    'Updated At' => '',
    'Url' => '',
    'User' => '用户',
    'User ID' => '',
    'Username' => '用户名',
    'Value' => '值',
    'Value Alias' => '',
    'Value Number' => '',
    'Year' => '年',
    'Zip Code' => '邮政编码',
    'lng' => '',
    'City' => '城市',
    'Company Type' => '公司的类型',
];