<?php 
 return [
    '' => '',
    'A report with properties by your request has been prepared. You can view it {by-link}' => 'Le preparó una recopilación de los objetos de los inmuebles de su petición. Usted puede ver la {by-link}',
    'Address' => 'La dirección de',
    'Area' => 'La plaza de',
    'Best regards,' => 'Saludos cordiales,',
    'Check your profile' => 'Revise su perfil',
    'Consultation request' => 'Solicitud de consultas',
    'Dear {name}!' => 'Estimado / a) {name}!',
    'Email' => '',
    'Floor' => 'Piso',
    'Follow the link below to reset your password' => 'Haga clic en el enlace a continuación para restablecer la contraseña',
    'Free CRM' => 'Gratuito CRM',
    'Free listing of your property on new portal eAgent.me' => 'Alojamiento gratuito de su propiedad en el nuevo portal eAgent.me',
    'Free translation into 7 languages' => 'Traducción gratuita en 7 idiomas',
    'Greetings!' => 'Hola!',
    'Hello, {name}' => 'Hola {name}',
    'If you will have any questions please contact us on email {email}' => 'Si tiene alguna pregunta, póngase en contacto con nosotros por correo electrónico a {email}',
    'Message' => 'El mensaje',
    'Message for agent {agent}' => 'Mensaje para el agente de {agent}',
    'Message for property owner' => 'Mensaje para el propietario de un inmueble',
    'Message from contact form' => 'Un mensaje desde el formulario de contacto',
    'Name' => 'El nombre de',
    'New customers for the purchase and rental of real estate worldwide' => 'Nuevos clientes en la compra y alquiler de bienes inmuebles de todo el mundo',
    'Our site gives you additional features:' => 'Nuestro sitio web le proporciona características adicionales:',
    'Password reset for {site}' => 'Restablecimiento de la contraseña de la {site}',
    'Phone' => 'Teléfono',
    'Posting your ads for free' => 'Publicación gratuita de sus anuncios',
    'Presentation request' => 'La presentación de la solicitud',
    'Property Price Offer' => 'Propuesta de precio en un objeto de inmu',
    'Property Request Mortgage' => 'La solicitud de la hipoteca de un inmueble',
    'Property Request Quiz' => 'Adicional de la encuesta después de contactar a un agente',
    'Property Request Showcase' => 'La solicitud de presentación del objeto de los inmuebles',
    'Property report from {agent}' => 'Una recopilación de los objetos de los inmuebles de {agent}',
    'Reset Password' => 'Restablecimiento de la contraseña',
    'Rooms' => 'La estaca-en de las habitaciones',
    'Search for customers in various countries, such as China, USA, Brazil, Russia and many others' => 'Búsqueda de clientes en diferentes países, tales como china, estados unidos, brasil, rusia y muchos otros.',
    'Sent with {name}' => 'Enviado a través de {name}',
    'Sincerely, {name}' => 'Atentamente, {name}',
    'Special offer request' => 'Consulta la oferta especial',
    'Team {name}' => 'El comando {name}',
    'View this property {by-link}' => 'Ver el objeto {by-link}',
    'We posted your property on our portal' => 'Hemos colocado a su propiedad en nuestro portal',
    'You can check the details' => 'Puede verificar la información',
    'You got a message from user with contacts:' => 'Usted ha recibido un mensaje de un usuario con los datos de contacto:',
    'You got a message from user with following contacts' => 'Usted ha recibido un mensaje de un usuario con los siguientes datos de contacto',
    'You got a request for consultation on buying property from user with contacts:' => 'Recibes una solicitud de consulta sobre la compra de bienes de usuario con los datos de contacto:',
    'You got a request for new constructions presentation from user with contacts:' => 'Usted recibe una solicitud de presentación de las casas nuevas de usuario con los datos de contacto:',
    'You got a request presentation for building {name} from user with contacts:' => 'Usted ha recibido la solicitud de la presentación de la estructura {name} del usuario con los datos de contacto:',
    'You got a special offer request from user with contacts:' => 'Usted ha recibido una solicitud de una oferta de usuario con los datos de contacto:',
    'Your Login: {login}' => 'Su nombre de usuario: {login}',
    'Your temporary password: {password}' => 'Su contraseña temporal: {password}',
    'by link' => 'enlace',
    'eAgent Message' => 'El mensaje con eAgent',
];