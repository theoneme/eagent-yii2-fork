<?php 
 return [
    'Address' => 'Адреса',
    'Advertising on Partner Sites Allowed' => 'Дозволена реклама на сайтах партнерів',
    'Advertising on Site Allowed' => 'Дозволена реклама на порталі',
    'Alias' => '',
    'Attribute Group ID' => '',
    'Attribute ID' => '',
    'Avatar' => 'Аватар',
    'Banner' => '',
    'Building ID' => '',
    'Category' => '',
    'Category ID' => '',
    'City Code' => '',
    'Code' => '',
    'Code From' => '',
    'Code To' => '',
    'Company' => '',
    'Company ID' => '',
    'Contact Type' => 'Тип контакту',
    'Content' => '',
    'Contract Price' => 'Договірна ціна',
    'Country' => '',
    'Country Code' => '',
    'Country ID' => '',
    'Created At' => '',
    'Currency Code' => 'Валюта',
    'Custom Data' => '',
    'Data' => '',
    'Description' => 'Опис',
    'Distance' => '',
    'Email' => '',
    'Entity' => '',
    'Entity Alias' => '',
    'Entity ID' => '',
    'File' => '',
    'Floors' => '',
    'From ID' => '',
    'ID' => '',
    'Image' => '',
    'Is Primary' => '',
    'Key' => '',
    'Lat' => '',
    'Lft' => '',
    'Lng' => '',
    'Locale' => '',
    'Logo' => '',
    'Lvl' => '',
    'Metadata' => '',
    'Name' => '',
    'New Password' => 'Новий пароль',
    'Password Confirmation' => 'Підтвердження пароля',
    'Phone' => 'Телефон',
    'Price' => 'Ціна',
    'Property ID' => '',
    'Quarter' => 'Квартал',
    'Ratio' => '',
    'Region' => '',
    'Region Code' => '',
    'Region ID' => '',
    'Request ID' => '',
    'Rgt' => '',
    'Role' => 'Роль',
    'Root' => '',
    'Short Description' => 'Короткий опис',
    'Slug' => '',
    'Source' => '',
    'Source ID' => '',
    'Status' => 'Статус',
    'Symbol Left' => '',
    'Symbol Right' => '',
    'Time' => '',
    'Title' => 'Назва',
    'To ID' => '',
    'Transport' => '',
    'Type' => '',
    'Updated  At' => '',
    'Updated At' => '',
    'Url' => '',
    'User' => 'Користувач',
    'User ID' => '',
    'Username' => 'Ім\'я користувача',
    'Value' => 'Значення',
    'Value Alias' => '',
    'Value Number' => '',
    'Year' => 'Рік',
    'Zip Code' => 'Поштовий індекс',
    'lng' => '',
    'City' => 'Місто',
    'Company Type' => 'Тип компанії',
];