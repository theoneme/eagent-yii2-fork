<?php 
 return [
    'Address' => 'Διεύθυνση',
    'Advertising on Partner Sites Allowed' => 'Επιτρέπεται η προβολή διαφημίσεων σε ιστότοπους συνεργατών',
    'Advertising on Site Allowed' => 'Επιτρέπεται η διαφήμιση στο portal',
    'Alias' => '',
    'Attribute Group ID' => '',
    'Attribute ID' => '',
    'Avatar' => 'Avatar',
    'Banner' => '',
    'Building ID' => '',
    'Category' => '',
    'Category ID' => '',
    'City Code' => '',
    'Code' => '',
    'Code From' => '',
    'Code To' => '',
    'Company' => '',
    'Company ID' => '',
    'Contact Type' => 'Τύπος επαφών',
    'Content' => '',
    'Contract Price' => 'Διαπραγματεύσιμη τιμή',
    'Country' => '',
    'Country Code' => '',
    'Country ID' => '',
    'Created At' => '',
    'Currency Code' => 'Νόμισμα',
    'Custom Data' => '',
    'Data' => '',
    'Description' => 'Περιγραφή',
    'Distance' => '',
    'Email' => '',
    'Entity' => '',
    'Entity Alias' => '',
    'Entity ID' => '',
    'File' => '',
    'Floors' => '',
    'From ID' => '',
    'ID' => '',
    'Image' => '',
    'Is Primary' => '',
    'Key' => '',
    'Lat' => '',
    'Lft' => '',
    'Lng' => '',
    'Locale' => '',
    'Logo' => '',
    'Lvl' => '',
    'Metadata' => '',
    'Name' => '',
    'New Password' => 'Ένα νέο κωδικό πρόσβασης',
    'Password Confirmation' => 'Επιβεβαίωση κωδικού πρόσβασης',
    'Phone' => 'Τηλέφωνο',
    'Price' => 'Τιμή',
    'Property ID' => '',
    'Quarter' => 'Τρίμηνο',
    'Ratio' => '',
    'Region' => '',
    'Region Code' => '',
    'Region ID' => '',
    'Request ID' => '',
    'Rgt' => '',
    'Role' => 'Ρόλο',
    'Root' => '',
    'Short Description' => 'Σύντομη περιγραφή',
    'Slug' => '',
    'Source' => '',
    'Source ID' => '',
    'Status' => 'Το καθεστώς',
    'Symbol Left' => '',
    'Symbol Right' => '',
    'Time' => '',
    'Title' => 'Όνομα',
    'To ID' => '',
    'Transport' => '',
    'Type' => '',
    'Updated  At' => '',
    'Updated At' => '',
    'Url' => '',
    'User' => 'Ο χρήστης',
    'User ID' => '',
    'Username' => 'Όνομα χρήστη',
    'Value' => 'Τιμή',
    'Value Alias' => '',
    'Value Number' => '',
    'Year' => 'Έτος',
    'Zip Code' => 'Ταχυδρομικός κώδικας',
    'lng' => '',
    'City' => 'Πόλη',
    'Company Type' => 'Τύπος επιχείρησης',
];