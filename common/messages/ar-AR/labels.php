<?php 
 return [
    'Active' => 'نشط',
    'Address' => 'عنوان',
    'Address of your property' => 'عنوان العقار',
    'Administrator' => 'مسؤول',
    'Airport' => 'مطار',
    'Approved' => 'وافق',
    'Area' => 'المنطقة',
    'Available' => 'المتاحة',
    'Building permit' => 'رخصة البناء',
    'Checkbox' => 'مربع',
    'City' => 'المدينة',
    'City Center' => 'مركز المدينة',
    'Company Owner' => 'صاحب الشركة',
    'Content Manager' => 'مدير المحتوى',
    'Country' => 'البلد',
    'Credit Amount' => 'الائتمان',
    'Currency' => 'العملة',
    'Date' => 'تاريخ',
    'Disabled' => 'تعطيل',
    'Do you need mortgage?' => 'كنت في حاجة الى الرهن العقاري ؟ ',
    'Do you plan to buy condo?' => 'كنت تخطط لشراء شقة ؟ ',
    'Do you plan to sell secondary property?' => 'كنت تخطط لبيع المنازل الثانية ؟ ',
    'Dropdown' => 'القائمة المنسدلة',
    'Enter client`s email address' => 'أدخل عنوان البريد الإلكتروني العميل',
    'Enter client`s first name' => 'أدخل اسم العميل',
    'Enter client`s last name' => 'أدخل اسم العميل',
    'Enter client`s middle name' => 'أدخل الاسم الأول من العملاء',
    'Enter client`s phone' => 'يدخل الزبون الهاتف',
    'Enter offer price' => 'أدخل السعر المقترح',
    'Enter the area of your property' => 'أدخل المنطقة من الممتلكات الخاصة بك',
    'Enter the number of rooms' => 'أدخل عدد الغرف',
    'Enter your address' => 'أدخل عنوانك',
    'Enter your age' => 'أدخل عمرك',
    'Enter your email address' => 'أدخل عنوان البريد الإلكتروني الخاص بك',
    'Enter your name' => 'أدخل اسمك',
    'Enter your phone' => 'أدخل رقم الهاتف الخاص بك',
    'Finished' => 'أكثر',
    'First Name' => 'اسم',
    'How many years do you plan to repay the loan' => 'كم سنة كنت تخطط للحصول على قرض',
    'I want to get approval from the bank to the property' => 'أريد الحصول على موافقة من بنك العقار',
    'Invited' => 'دعا',
    'Last Name' => 'اللقب',
    'Loan Term (years)' => 'تاريخ القرض (سنة)',
    'Login' => 'تسجيل الدخول',
    'Metro' => 'مترو',
    'Middle Name' => 'العائلي',
    'Name' => 'اسم',
    'Not specified' => 'غير محدد',
    'Number' => 'عدد',
    'Odnoklassniki' => 'زملاء',
    'Operation act' => 'قانون العملية',
    'Park' => 'بارك',
    'Password' => 'كلمة المرور',
    'Pending' => 'في انتظار',
    'Phone' => 'الهاتف',
    'Pond' => 'البركة',
    'Project declaration' => 'إعلان المشروع',
    'Published' => 'نشرت',
    'Region' => 'المنطقة',
    'Remember me next time' => 'تذكر لي',
    'Requires moderation' => 'يتطلب الاعتدال',
    'Requires modification' => 'يتطلب تغييرات',
    'Rooms' => 'عدد الغرف',
    'School' => 'المدرسة',
    'Set the date for showcase' => 'تحديد موعد عرض',
    'Set the time when you wil be able to come to showcase' => 'تحديد الوقت عندما يمكنك أن تأتي إلى المعرض',
    'Sold' => 'تباع',
    'Start typing name' => 'ابدأ كتابة اسم',
    'String' => 'خط',
    'Support' => 'الدعم',
    'Time' => 'الوقت',
    'Unavailable' => 'غير متوفر',
    'Unfinished' => 'لم تنته',
    'Unknown role' => 'معروف دور',
    'Unknown status' => 'غير معروف الحالة',
    'Unknown transport' => 'غير معروف النقل',
    'Unknown type' => 'غير معروف',
    'Website' => 'الموقع',
    'When do you plan on buying a property' => 'عندما كنت تفكر في شراء العقارات ؟ ',
    'Your Age' => 'عمرك',
    'Your Email' => 'البريد الإلكتروني الخاص بك',
    'Your Family Status' => 'وضعك العائلي',
    'Your Message' => 'رسالتك',
    'Your Name' => 'اسمك',
    'Your Phone' => 'الهاتف',
    'Your Price' => 'السعر الخاص بك',
    'on car' => 'بالسيارة',
    'on city transport' => 'بواسطة وسائل النقل العام',
    'on foot' => 'المشي',
    'Refused' => 'رفض',
    'Paid' => 'دفعت',
    'Account replenishment' => 'عبوة',
    'Payment for tariff' => 'معدل الدفع',
    'Cancel Last Point' => 'إلى التراجع عن آخر نقطة',
    'Clear' => 'نظيفة',
    'Import' => 'استيراد',
    'Agency' => 'وكالة',
    'Developer' => 'المطور',
    'Owner' => 'مالك',
    'Realtor' => 'سمسار عقارات',
];