<?php 
 return [
    'Active' => 'Актыўны',
    'Address' => 'Адрас',
    'Address of your property' => 'Адрас вашай нерухомасці',
    'Administrator' => 'Адміністратар',
    'Airport' => 'Аэрапорт',
    'Approved' => 'Ухвалены',
    'Area' => 'Плошча',
    'Available' => 'Даступна',
    'Building permit' => 'Дазвол на будаўніцтва',
    'Checkbox' => 'Чекбокс',
    'City' => 'Горад',
    'City Center' => 'Цэнтр горада',
    'Company Owner' => 'Уладальнік кампаніі',
    'Content Manager' => 'Кантэнт-менеджэр',
    'Country' => 'Краіна',
    'Credit Amount' => 'Крэдыт',
    'Currency' => 'Валюта',
    'Date' => 'Дата',
    'Disabled' => 'Адключана',
    'Do you need mortgage?' => 'Вам патрэбна іпатэка?',
    'Do you plan to buy condo?' => 'Вы плануеце купіць кватэру?',
    'Do you plan to sell secondary property?' => 'Вы плануеце прадаваць другаснае жыллё?',
    'Dropdown' => 'Выпадальны спіс',
    'Enter client`s email address' => 'Калі ласка, увядзіце Email-адрас кліента',
    'Enter client`s first name' => 'Калі ласка, увядзіце імя кліента',
    'Enter client`s last name' => 'Калі ласка, увядзіце прозвішча кліента',
    'Enter client`s middle name' => 'Калі ласка, увядзіце імя па бацьку кліента',
    'Enter client`s phone' => 'Калі ласка, увядзіце тэлефон кліента',
    'Enter offer price' => 'Калі ласка, увядзіце прапанаваную цану',
    'Enter the area of your property' => 'Калі ласка, увядзіце плошчу вашай нерухомасці',
    'Enter the number of rooms' => 'Калі ласка, увядзіце колькасць пакояў',
    'Enter your address' => 'Калі ласка, увядзіце ваш адрас',
    'Enter your age' => 'Калі ласка, увядзіце ваш узрост',
    'Enter your email address' => 'Калі ласка, увядзіце ваш адрас электроннай пошты',
    'Enter your name' => 'Калі ласка, увядзіце ваша імя',
    'Enter your phone' => 'Калі ласка, увядзіце ваш тэлефон',
    'Finished' => 'Скончаны',
    'First Name' => 'Імя',
    'How many years do you plan to repay the loan' => 'На колькі гадоў вы плануеце браць крэдыт',
    'I want to get approval from the bank to the property' => 'Я хачу атрымаць адабрэнне ад банка на аб\'ект нерухомасці',
    'Invited' => 'Запрошаны',
    'Last Name' => 'Прозвішча',
    'Loan Term (years)' => 'Дата пазыку (гадоў)',
    'Login' => 'Лагін',
    'Metro' => 'Метро',
    'Middle Name' => 'Імя па бацьку',
    'Name' => 'Імя',
    'Not specified' => 'Не паказана',
    'Number' => 'Лік',
    'Odnoklassniki' => 'Аднакласнікі',
    'Operation act' => 'Акт аб аперацыі',
    'Park' => 'Парк',
    'Password' => 'Пароль',
    'Pending' => 'У чаканні',
    'Phone' => 'Тэлефон',
    'Pond' => 'Вадаём',
    'Project declaration' => 'Праектная дэкларацыя',
    'Published' => 'Апублікавана',
    'Region' => 'Рэгіён',
    'Remember me next time' => 'Запомніць мяне',
    'Requires moderation' => 'Патрабуе мадэрацыі',
    'Requires modification' => 'Патрабуе змены',
    'Rooms' => 'Колькасць пакояў',
    'School' => 'Школа',
    'Set the date for showcase' => 'Пакажыце дату паказу',
    'Set the time when you wil be able to come to showcase' => 'Пакажыце час, калі вы зможаце прыйсці на паказ',
    'Sold' => 'Прададзена',
    'Start typing name' => 'Пачніце ўводзіць імя',
    'String' => 'Радок',
    'Support' => 'Падтрымка',
    'Time' => 'Час',
    'Unavailable' => 'Недаступна',
    'Unfinished' => 'Неокончен',
    'Unknown role' => 'Невядомая ролю',
    'Unknown status' => 'Статус невядомы',
    'Unknown transport' => 'Невядомы транспарт',
    'Unknown type' => 'Невядомы тып',
    'Website' => 'Вэб-сайт',
    'When do you plan on buying a property' => 'Калі вы плануеце куплю нерухомасці?',
    'Your Age' => 'Ваш узрост',
    'Your Email' => 'Ваш email',
    'Your Family Status' => 'Ваш сямейны статус',
    'Your Message' => 'Ваша паведамленне',
    'Your Name' => 'Ваша імя',
    'Your Phone' => 'Ваш тэлефон',
    'Your Price' => 'Ваша цана',
    'on car' => 'на машыне',
    'on city transport' => 'на гарадскім транспарце',
    'on foot' => 'пешшу',
    'Refused' => 'Адхілены',
    'Paid' => 'Аплочаны',
    'Account replenishment' => 'Папаўненне рахунку',
    'Payment for tariff' => 'Аплата тарыфу',
    'Cancel Last Point' => 'Адмяніць апошнюю кропку',
    'Clear' => 'Ачысціць',
    'Import' => 'Імпарт',
    'Agency' => 'Агенцтва',
    'Developer' => 'Забудоўшчык',
    'Owner' => 'Уладальнік',
    'Realtor' => 'Рыэлтар',
];