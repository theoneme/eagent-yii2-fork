<?php 
 return [
    'Active' => 'ที่ทำงานอยู่',
    'Address' => 'ที่อยู่',
    'Address of your property' => 'ที่อยู่ของทรัพย์สินของคุณ',
    'Administrator' => 'ผู้ดูแลระบบ',
    'Airport' => 'สนามบิน',
    'Approved' => 'ได้รับอนุญาต',
    'Area' => 'พื้นที่',
    'Available' => 'ที่มีอยู่',
    'Building permit' => 'ตึกนี้ได้รับอนุญาต',
    'Checkbox' => 'ตัวเลือก',
    'City' => 'เมืองนี้',
    'City Center' => 'ศูนย์กลางเมือง',
    'Company Owner' => 'เจ้าของบริษัท',
    'Content Manager' => 'เนื้อหาผู้จัดการ',
    'Country' => 'ประเทศ',
    'Credit Amount' => 'บัตรเครดิต',
    'Currency' => 'สัญลักษณ์ของเงินตรา',
    'Date' => 'เดท',
    'Disabled' => 'ปิดการใช้งาน',
    'Do you need mortgage?' => 'คุณต้องการจำนองสินะ?',
    'Do you plan to buy condo?' => 'คุณกำลังวางแผนเพื่อซื้ออพาร์ทเม้นสินะ?',
    'Do you plan to sell secondary property?' => 'คุณวางแผนไปขายที่สองบ้านเหรอครับ?',
    'Dropdown' => 'ที่ลองรายการ',
    'Enter client`s email address' => 'ป้อนที่อยู่อีเมลของลูกค้า',
    'Enter client`s first name' => 'ป้อนชื่อของลูกค้า',
    'Enter client`s last name' => 'ป้อนชื่อของลูกค้า',
    'Enter client`s middle name' => 'ป้อนชื่อแรกของลูกค้า',
    'Enter client`s phone' => 'ป้อนลูกค้าโทรศัพท์ของ',
    'Enter offer price' => 'ป้อนเสนอราคา',
    'Enter the area of your property' => 'ป้อนพื้นที่ของทรัพย์สินของคุณ',
    'Enter the number of rooms' => 'ป้อนจำนวนของห้อง',
    'Enter your address' => 'ป้อนที่อยู่ของคุณ',
    'Enter your age' => 'ป้อนอายุของเธอ',
    'Enter your email address' => 'ป้อนที่อยู่อีเมลของคุณ',
    'Enter your name' => 'ป้อนชื่อของคุณ',
    'Enter your phone' => 'ป้อนชื่อของคุณหมายเลขโทรศัพท์',
    'Finished' => 'ต',
    'First Name' => 'ชื่อ',
    'How many years do you plan to repay the loan' => 'กี่ปี่แล้วที่คุณวางแผนจะไปกู้เงิน',
    'I want to get approval from the bank to the property' => 'ฉันอยากจะอนุมัติจากธนาคารเพื่อที่บ้าน',
    'Invited' => 'ได้รับเชิญ',
    'Last Name' => 'นามสกุล',
    'Loan Term (years)' => 'เดทของกู้เงิน(ปี)',
    'Login' => 'ล็อกอิน',
    'Metro' => 'เมโทร',
    'Middle Name' => 'Patronymic',
    'Name' => 'ชื่อ',
    'Not specified' => 'ยังไม่ได้ระบุ',
    'Number' => 'จำนวน',
    'Odnoklassniki' => 'รวจสอบเพื่อนร่วมชั้น',
    'Operation act' => 'การกระทำของปฏิบัติการ',
    'Park' => 'วนสาธารณะ',
    'Password' => 'รหัสผ่าน',
    'Pending' => 'รออยู่',
    'Phone' => 'โทรศัพท์',
    'Pond' => 'บ่อน้ำ',
    'Project declaration' => 'โครงการปล่อยให้เธอได้หายใจอยู่อี',
    'Published' => 'ตีพิมพ์ออก',
    'Region' => 'ขอบเขต',
    'Remember me next time' => 'จำฉันได้ไหม',
    'Requires moderation' => 'ต้องการเนื่อง',
    'Requires modification' => 'ต้องการเปลี่ยนแปลง',
    'Rooms' => 'จำนวนของห้อง',
    'School' => 'โรงเรียน',
    'Set the date for showcase' => 'สำหรับกำหนดวันของการแสดง',
    'Set the time when you wil be able to come to showcase' => 'สำหรับกำหนดช่วงเวลาที่เธอออกมาได้ต้องการแสดง',
    'Sold' => 'ขาย',
    'Start typing name' => 'เริ่มพิมพ์ชื่อ',
    'String' => 'เส้น',
    'Support' => 'สนับสนุน',
    'Time' => 'เวลา',
    'Unavailable' => 'ไม่ว่า',
    'Unfinished' => 'ที่ยังทำไม่เสร็จที่อ',
    'Unknown role' => 'ไม่รู้จักบทบาท',
    'Unknown status' => 'ไม่ทราบสถานะ',
    'Unknown transport' => 'ไม่ทราบขนส่ง',
    'Unknown type' => 'ไม่ทราบ',
    'Website' => 'บนเว็บไซต์',
    'When do you plan on buying a property' => 'ตอนที่คุณพิจารณาการซื้ออสังหาริมทรัพย์?',
    'Your Age' => 'อายุของเธอ',
    'Your Email' => 'อีเมลของคุณ',
    'Your Family Status' => 'สถานภาพการสมรสของคุณ',
    'Your Message' => 'ข้อความของคุณ',
    'Your Name' => 'ชื่อของคุณ',
    'Your Phone' => 'โทรศัพท์ของคุณ',
    'Your Price' => 'คุณบอกราคาอีก',
    'on car' => 'โดยรถ',
    'on city transport' => 'โดยการขนส่ง',
    'on foot' => 'เดิน',
    'Refused' => 'ถูกปฏิเสธ',
    'Paid' => 'จ่ายเงิน',
    'Account replenishment' => 'เติมหน่อย',
    'Payment for tariff' => 'ค่าจ้างราคา',
    'Cancel Last Point' => 'ที่จะมีโอกาสแก้ไขโดยการทำลาถึงจุดสุดท้ายที่',
    'Clear' => 'สะอาด',
    'Import' => 'นำเข้า',
    'Agency' => 'องกรณ์',
    'Developer' => 'ที่ผู้พัฒนา',
    'Owner' => 'เจ้าของ',
    'Realtor' => 'นายหน้า',
];