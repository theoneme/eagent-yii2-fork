<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\base\InvalidConfigException;

/**
 * Class RequestAttributesMapper
 * @package common\mappers
 */
class RequestAttributesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        if (!array_key_exists('attributes', $rawData)) {
            throw new InvalidConfigException('Array must contain `attributes` key');
        }

        if (!array_key_exists('attributeValues', $rawData)) {
            throw new InvalidConfigException('Array must contain `attributeValues` key');
        }

        $data = [];

        foreach ($rawData['attributes'] as $value) {
            $data[$value['alias']]['title'] = $value['translations']['title'];
            $data[$value['alias']]['id'] = $value['id'];
        }

        foreach ($rawData['attributeValues'] as $value) {
            $data[$rawData['attributes'][$value['attribute_id']]['alias']]['value'][] = $value['translations']['title'];
            $data[$rawData['attributes'][$value['attribute_id']]['alias']]['value_id'][] = $value['id'];
        }

        foreach($rawData['requestAttributes'] as $value) {
            if(array_key_exists('min', $value['customDataArray'])) {
                $data[$rawData['attributes'][$value['attribute_id']]['alias']]['value'] = [
                    'min' => $value['customDataArray']['min'],
                    'max' => $value['customDataArray']['max']
                ];
                $data[$rawData['attributes'][$value['attribute_id']]['alias']]['value_id'] = null;
            }
        }

        $data = array_map(function ($value) {
            return [
                'title' => $value['title'],
                'attribute_id' => $value['id'],
                'value' => !array_key_exists('min', $value['value']) ? implode(', ', $value['value']) : $value['value'],
                'value_id' => $value['value_id']
            ];
        }, $data);

        return $data;
    }
}