<?php
namespace common\mappers;

use common\interfaces\DataMapperInterface;
use common\models\Property;
use yii\helpers\ArrayHelper;

/**
 * Class UserPropertyMapper
 * @package common\mappers
 */
class UserPropertyMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return ArrayHelper::map($rawData, 'id', function ($var) {
            /* @var Property $var*/
            return [
                'id' => $var->id,
                'slug' => $var->slug,
            ];
        });
    }
}