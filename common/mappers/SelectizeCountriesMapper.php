<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class SelectizeCountriesMapper
 * @package common\mappers
 */
class SelectizeCountriesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $result = array_map(function($value) {
            return [
                'label' => $value['title'],
                'id' => $value['id']
            ];
        }, $rawData);

        return $result;
    }
}