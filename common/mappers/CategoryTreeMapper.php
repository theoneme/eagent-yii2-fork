<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class CategoryTreeMapper
 * @package common\mappers
 */
class CategoryTreeMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param string $indexBy
     * @return array
     */
    public static function getMappedData($rawData, $indexBy = 'id')
    {
        return ArrayHelper::map($rawData, $indexBy, function ($category) {
            return str_repeat('---', $category['lvl']) . ' ' . $category['title'];
        });
    }
}