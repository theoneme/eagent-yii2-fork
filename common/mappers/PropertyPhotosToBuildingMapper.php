<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class PropertyPhotosToBuildingMapper
 * @package common\mappers
 */
class PropertyPhotosToBuildingMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $images = [];
        $thumbnails = [];

        foreach ($rawData as $item) {
            if (!empty($item['images'])) {
                $images[] = $item['images'][0];
                $thumbnails[] = $item['thumbnails'][0];
            }
        }

        return [
            'images' => $images,
            'thumbnails' => $thumbnails
        ];
    }
}