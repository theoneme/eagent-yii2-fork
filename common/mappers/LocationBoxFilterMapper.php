<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class LocationBoxFilterMapper
 * @package common\mappers
 */
class LocationBoxFilterMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        $params = [];

        $points = explode(';', $rawData);
        if (count($points) === 2) {
            $ne = explode(',', $points[0]);
            $sw = explode(',', $points[1]);

            if (count($ne) === 2 && count($sw) === 2) {
                $params['neLat'] = (float)$ne[0];
                $params['neLng'] = (float)$ne[1];
                $params['swLat'] = (float)$sw[0];
                $params['swLng'] = (float)$sw[1];
            }
        }

        return $params;
    }
}