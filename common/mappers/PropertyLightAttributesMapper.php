<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class PropertyLightAttributesMapper
 * @package common\mappers
 */
class PropertyLightAttributesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param array $exceptions
     * @return array
     */
    public static function getMappedData($rawData, array $exceptions = [])
    {
        $data = [];

        foreach ($rawData as $item) {
            $data[$item['entity_alias']][] = $item['value_number'] ?? $item['value_alias'];
        }

        $data = array_map(function ($value) {
            return implode(', ', $value);
        }, $data);

        return $data;
    }
}