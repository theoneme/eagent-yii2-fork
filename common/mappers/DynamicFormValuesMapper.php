<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class DynamicFormValuesMapper
 * @package common\mappers
 */
class DynamicFormValuesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param array $config
     * @return array
     */
    public static function getMappedData($rawData, $config = [])
    {
        $result = [];

        foreach ($rawData as $attribute) {
            $result["at_{$attribute['attribute_id']}"] = self::getValueByType($attribute, $config);
        }

        return $result;
    }

    /**
     * @param $value
     * @param $config
     * @return null
     */
    private static function getValueByType($value, $config)
    {
        $result = null;

        if (!empty($config['types']) && array_key_exists("at_{$value['attribute_id']}", $config['types'])) {
            switch ($config['types']["at_{$value['attribute_id']}"]) {
                case 'radiolist':
                case 'radio':
                case 'checkboxlist':
                case 'dropdownlist':
                case 'list':
                    $result = $value['value_id'];
                    break;
                case 'textbox':
                case 'textarea':
                case 'checkbox':
                case 'multitextbox':
                case 'numberbox':
                case 'range':
                    $result = $value['value'];
                    break;
            }
        }

        return $result;
    }
}