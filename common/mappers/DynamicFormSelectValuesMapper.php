<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;

/**
 * Class DynamicFormSelectValuesMapper
 * @package common\mappers
 */
class DynamicFormSelectValuesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $result = [];

        foreach ($rawData as $attribute) {
            $result["at_{$attribute['attribute_id']}"][$attribute['id']] = $attribute['translations']['title']
                ?? UtilityHelper::upperFirstLetter(str_replace('-', ' ', $attribute['alias']));
        }

        return $result;
    }
}