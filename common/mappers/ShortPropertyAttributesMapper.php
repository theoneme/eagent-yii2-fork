<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class ShortPropertyAttributesMapper
 * @package common\mappers
 */
class ShortPropertyAttributesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $data = [];

        if (empty($rawData)) {
            return $data;
        }

        $data = ArrayHelper::map($rawData, 'entity_alias', function ($value) {
            return str_replace('-', ',', $value['value_alias']);
        });

        return $data;
    }
}