<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use common\models\AttributeValue;
use yii\helpers\ArrayHelper;

/**
 * Class AttributeTranslationsMapper
 * @package common\mappers
 */
class AttributeTranslationMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $translationsArray = array_filter(
            ArrayHelper::map($rawData, function ($var) {
                /* @var $var AttributeValue */
                return $var->attr->alias;
            }, function ($var) {
                /* @var $var AttributeValue */
                return TranslationsMapper::getMappedData($var->translations)['title'] ?? null;
            })
        );
        return $translationsArray;
    }
}