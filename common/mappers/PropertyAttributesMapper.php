<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use common\models\Attribute;
use yii\base\InvalidConfigException;

/**
 * Class PropertyAttributesMapper
 * @package common\mappers
 */
class PropertyAttributesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param array $exceptions
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData, array $exceptions = [])
    {
        if (!array_key_exists('attributes', $rawData)) {
            throw new InvalidConfigException('Array must contain `attributes` key');
        }

        if (!array_key_exists('attributeValues', $rawData)) {
            throw new InvalidConfigException('Array must contain `attributeValues` key');
        }

        $data = [];

        if (empty($rawData['attributes']) || empty($rawData['attributeValues'])) {
            return $data;
        }

        foreach ($rawData['attributes'] as $value) {
            if (empty($exceptions) || !in_array($value['id'], $exceptions)) {
                $item = ['title' => $value['translations']['title'], 'id' => $value['id']];
                if ($value['alias'] === 'flat_number') {
                    $data = array_merge([$value['alias'] => $item], $data);
                } else {
                    $data[$value['alias']] = $item;
                }
            }
        }

        foreach ($rawData['attributeValues'] as $value) {
            if (empty($exceptions) || !in_array($value['attribute_id'], $exceptions)) {
                $title = null;
                if (array_key_exists('title', $value['translations'])) {
                    $title = $value['translations']['title'];
                } else if ($rawData['attributes'][$value['attribute_id']]['type'] === Attribute::TYPE_NUMBER) {
                    $title = (float)str_replace('-', '.', $value['alias']);
                }
                $data[$rawData['attributes'][$value['attribute_id']]['alias']]['value'][] = $title;
                $data[$rawData['attributes'][$value['attribute_id']]['alias']]['value_id'][] = $value['id'];
            }
        }

        $data = array_filter(array_map(function ($value) {
            if (array_key_exists('value', $value)) {
                return [
                    'title' => $value['title'],
                    'attribute_id' => $value['id'],
                    'value' => implode(', ', $value['value']),
                    'value_id' => $value['value_id']
                ];
            }

            return null;
        }, $data));

        return $data;
    }
}