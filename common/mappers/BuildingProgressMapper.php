<?php
namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class BuildingProgressMapper
 * @package common\mappers
 */
class BuildingProgressMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function($var){
            return [
                'id' => $var['id'],
                'year' => $var['year'],
                'quarter' => $var['quarter'],
                'images' => AttachmentsMapper::getMappedData($var['attachments']),
                'thumbnails' => AttachmentsMapper::getMappedData($var['attachments'], AttachmentsMapper::MODE_THUMB),
            ];
        }, $rawData);
    }
}