<?php

namespace common\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class SeoAttributesMapper
 * @package common\mappers
 */
class SeoAttributesMapper implements DataMapperInterface
{
    public const MODE_DEFAULT = 0;
    public const MODE_TITLE = 10;

    /**
     * @param $rawData
     * @param int $mode
     * @return string
     */
    public static function getMappedData($rawData, $mode = self::MODE_DEFAULT)
    {
        $exclude = ['category' => 0, 'price' => 0, 'operation' => 0];
        $data = array_diff_key($rawData, $exclude);
        $data = array_filter($data, function ($var) {
            if (is_array($var['checked'])) {
                return (isset($var['checked']['min']) && $var['checked']['min'] !== false) || (isset($var['checked']['max']) && $var['checked']['max'] !== false);
            }

            return $var['checked'] !== false;
        });
        $result = implode($mode === self::MODE_DEFAULT ? '; ' : ' | ',
            array_filter(
                array_map(function ($var) use ($mode) {
                    $title = $var['title'];
                    if (is_array($var['checked'])) {
                        $min = isset($var['checked']['min']) ? Yii::t('seo', ' from {value}', ['value' => $var['checked']['min']]) : null;
                        $max = isset($var['checked']['max']) ? Yii::t('seo', ' to {value}', ['value' => $var['checked']['max']]) : null;
                        $value = trim($min . $max);
                    } else if (isset($var['values'][$var['checked']]['title'])) {
                        $value = $var['values'][$var['checked']]['title'];
                    } else {
                        return null;
                    }
                    return $mode === self::MODE_DEFAULT ? "$title: $value" : UtilityHelper::upperFirstLetter("$title: $value");
                }, $data)
            )
        );

        return $result;
    }
}