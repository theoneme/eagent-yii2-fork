<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class SelectizeAttributeValuesMapper
 * @package common\mappers
 */
class SelectizeAttributeValuesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $result = array_map(function($value) {
            return [
                'id' => $value['id'],
                'value' => $value['translations']['title'],
            ];
        }, $rawData);

        return $result;
    }
}