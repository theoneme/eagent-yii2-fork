<?php

namespace common\mappers\forms;

use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class UserInvitationMessageMapper
 * @package common\mappers\forms
 */
class UserInvitationMessageMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return [
            'to' => [$rawData['email']],
            'id' => $rawData['id'],
            'login' => $rawData['email'],
            'password' => $rawData['password'],
            'name' => $rawData['username'],
            'subject' => Yii::t('notification', 'Free listing of your property on new portal eAgent.me'),
            'view' => 'invitation-message',
            'locale' => Yii::$app->language
        ];
    }
}