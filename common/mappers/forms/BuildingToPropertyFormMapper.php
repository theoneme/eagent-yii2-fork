<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers\forms;

use common\helpers\FileInputHelper;
use common\interfaces\DataMapperInterface;

/**
 * Class BuildingToPropertyFormMapper
 * @package common\mappers
 */
class BuildingToPropertyFormMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param array $config
     * @return array
     */
    public static function getMappedData($rawData, $config = [])
    {
        $result = [];

        $result['description'] = $rawData['description'];
        $result['title'] = $rawData['title'];
        $result['name'] = $rawData['name'];
        $result['attributes'] = [];
        $result['images'] = [];
        foreach ($rawData['attributes'] as $attribute) {
            $result['attributes']["at_{$attribute['attribute_id']}"] = $attribute['value'];
        }

        foreach ($rawData['images'] as $image) {
            $result['images'][] = FileInputHelper::buildOriginImagePath($image);
        }

        return $result;
    }
}