<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.10.2018
 * Time: 12:29
 */

namespace common\mappers\forms;

use common\interfaces\DataMapperInterface;
use Yii;
use yii\helpers\Url;

/**
 * Class ReportFormToMessageMapper
 * @package common\mappers
 */
class ReportFormToMessageMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return [
            'to' => $rawData['attributes']['email'],
            'receiverName' => "{$rawData['attributes']['firstName']} {$rawData['attributes']['middleName']} {$rawData['attributes']['lastName']}",
            'agentName' => $rawData['agent'],
            'subject' => Yii::t('notification', 'Property report from {agent}', ['agent' => $rawData['agent']]),
            'link' => Url::to(['/report/report/report', 'id' => $rawData['insertId']], true),
            'view' => 'report-message',
            'locale' => Yii::$app->language
        ];
    }
}