<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.10.2018
 * Time: 12:29
 */

namespace common\mappers\forms;

use common\interfaces\DataMapperInterface;
use Yii;
use yii\helpers\Url;

/**
 * Class RequestResetFormMapper
 * @package common\mappers\forms
 */
class RequestResetFormMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return [
            'to' => $rawData['attributes']['email'],
            'subject' => Yii::t('notification', 'Password reset for {site}', ['site' => Yii::$app->name]),
            'message' => Yii::t('notification', 'Follow the link below to reset your password'),
            'receiverName' => $rawData['user']->username,
            'link' => Url::to(['/auth/reset-password', 'token' => $rawData['user']->password_reset_token], true),
            'linkLabel' => Yii::t('notification', 'Reset Password'),
            'view' => 'simple-message',
            'locale' => Yii::$app->language
        ];
    }
}