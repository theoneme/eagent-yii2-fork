<?php
namespace common\mappers\forms;

use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class ContactFormToMessageMapper
 * @package common\mappers\forms
 */
class ContactFormToMessageMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return [
            'to' => ['netproject7@gmail.com', 'info@ujobs.me', 'tav.ekb@yandex.ru'],
            'receiverName' => 'eAgent',
            'senderName' => $rawData['attributes']['name'],
            'senderEmail' => $rawData['attributes']['email'],
            'senderPhone' => $rawData['attributes']['phone'],
            'address' => $rawData['attributes']['address'],
            'floor' => $rawData['attributes']['floor'],
            'rooms' => $rawData['attributes']['rooms'],
            'area' => $rawData['attributes']['area'],
            'subject' => Yii::t('notification', 'Message from contact form'),
            'view' => 'contact-message',
            'locale' => Yii::$app->language
        ];
    }
}