<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.10.2018
 * Time: 12:29
 */

namespace common\mappers\forms;

use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class AgentFormToMessageMapper
 * @package common\mappers\forms
 */
class AgentFormToMessageMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        $ourEmails = ['netproject7@gmail.com', 'info@ujobs.me', 'tav.ekb@yandex.ru'];
        $to = !empty($rawData['agent']) ? array_merge([$rawData['agent']['email']], $ourEmails) : $ourEmails;

        return [
            'to' => $to,
            'receiverName' => !empty($rawData['agent']) ? $rawData['agent']['username'] : Yii::$app->name,
            'senderName' => $rawData['attributes']['name'],
            'senderEmail' => $rawData['attributes']['email'],
            'senderPhone' => $rawData['attributes']['phone'],
            'senderMessage' => $rawData['attributes']['message'],
            'subject' => Yii::t('notification', 'Message for agent {agent}', ['agent' => $rawData['agent']['username']]),
            'link' => $rawData['attributes']['url'],
            'view' => 'agent-message',
            'locale' => Yii::$app->language
        ];
    }
}