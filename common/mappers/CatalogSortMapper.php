<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class CatalogSortMapper
 * @package common\mappers
 */
class CatalogSortMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $replaces = [
            'asc' => SORT_ASC,
            'desc' => SORT_DESC
        ];
        $parts = explode(':', $rawData);

        if (count($parts) < 2) {
            return null;
        }

        $order = strtolower($parts[1]);
        if (!in_array($order, ['asc', 'desc'])) {
            $order = 'desc';
        }

        if(array_key_exists($order, $replaces)) {
            $order = $replaces[$order];
        }

        return [
            'field' => $parts[0],
            'order' => $order
        ];
    }
}