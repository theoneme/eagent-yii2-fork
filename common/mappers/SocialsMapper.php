<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class SocialsMapper
 * @package common\mappers
 */
class SocialsMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return ArrayHelper::map($rawData, 'source', function ($socialItem) {
            return [
                'id' => $socialItem['id'],
                'url' => $socialItem['customDataArray']['url'],
            ];
        });
    }
}