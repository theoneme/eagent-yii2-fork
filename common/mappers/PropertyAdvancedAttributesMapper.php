<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\base\InvalidConfigException;

/**
 * Class PropertyAdvancedAttributesMapper
 * @package common\mappers
 */
class PropertyAdvancedAttributesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param array $advanced
     * @return array
     */
    public static function getMappedData($rawData, array $advanced = [])
    {
        if (!array_key_exists('attributes', $rawData)) {
            throw new InvalidConfigException('Array must contain `attributes` key');
        }

        if (!array_key_exists('attributeValues', $rawData)) {
            throw new InvalidConfigException('Array must contain `attributeValues` key');
        }

        $data = [];

        if (empty($advanced) || empty($rawData['attributes']) || empty($rawData['attributeValues'])) {
            return $data;
        }

        foreach ($rawData['attributes'] as $value) {
            if (in_array($value['id'], $advanced)) {
                $data[$value['alias']]['title'] = $value['translations']['title'];
                $data[$value['alias']]['id'] = $value['id'];
            }
        }
        foreach ($rawData['attributeValues'] as $value) {
            if (in_array($value['attribute_id'], $advanced)) {
                $data[$rawData['attributes'][$value['attribute_id']]['alias']]['values'][] = $value['translations']['title'];
            }
        }

        return $data;
    }
}