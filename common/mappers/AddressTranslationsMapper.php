<?php
namespace common\mappers;

use common\interfaces\DataMapperInterface;
use common\models\AddressTranslation;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class AddressTranslationsMapper
 * @package common\mappers
 */
class AddressTranslationsMapper implements DataMapperInterface
{
    public const MODE_FULL = 0;
    public const MODE_SHORT = 10;

    /**
     * @param $rawData
     * @param int $mode
     * @return array|null
     */
    public static function getMappedData($rawData, $mode = self::MODE_SHORT)
    {
        $translationsArray = ArrayHelper::map($rawData, 'locale', function ($translation) {
            $customData = json_decode($translation['custom_data'], true);
            /** @var AddressTranslation $translation*/
            return [
                'title' => $translation['title'],
                'data' => $customData,
            ];
        });

        if($mode === self::MODE_FULL) {
            return $translationsArray;
        }

        $languageTranslation = array_key_exists(Yii::$app->language, $translationsArray)
            ? $translationsArray[Yii::$app->language]
            : array_pop($translationsArray);

        return $languageTranslation;
    }
}