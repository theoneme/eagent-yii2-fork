<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class CompanyMembersMapper
 * @package common\mappers
 */
class CompanyMembersMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return ArrayHelper::map($rawData, 'user_id', function ($var) {
            return [
                'role' => $var['role'],
                'status' => $var['status']
            ];
        });
    }
}