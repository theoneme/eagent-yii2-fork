<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.10.2018
 * Time: 17:08
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class SimilarPropertiesParamsMapper
 * @package common\mappers
 */
class SimilarPropertiesParamsMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $similarParams = [
            'category_id' => $rawData['category_id'],
            'type' => $rawData['type'],
            'lat' => $rawData['lat'],
            'lng' => $rawData['lng'],
            'status' => $rawData['status'],
            'region_id' => $rawData['region_id'],
            'radius' => 4,
//            'locale' => $rawData['locale'],
            'price' => [
                'min' => (int)($rawData['raw_price'] * 0.8),
                'max' => (int)($rawData['raw_price'] * 1.2)
            ],
            'ads_allowed' => true,
        ];
        if (array_key_exists('rooms', $rawData['attributes'])) {
            $similarParams['rooms'] = $rawData['attributes']['rooms']['value'];
        }

        if (array_key_exists('property_area', $rawData['attributes'])) {
            $similarParams['property_area']['min'] = (int)($rawData['attributes']['property_area']['value'] * 0.9);
            $similarParams['property_area']['max'] = (int)($rawData['attributes']['property_area']['value'] * 1.1);
        }

        return $similarParams;
    }
}