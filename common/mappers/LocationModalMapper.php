<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class LocationModalMapper
 * @package common\mappers
 */
class LocationModalMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param string $entity
     * @return array
     */
    public static function getMappedData($rawData, $entity = 'city')
    {
        $output = [];

        foreach ($rawData as $item) {
            $firstLetter = mb_substr($item['title'], 0, 1);
            $item['entity'] = $entity;
            $output[$firstLetter][] = $item;
        }

        ksort($output);

        return $output;
    }
}