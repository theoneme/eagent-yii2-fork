<?php
namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class BuildingSiteMapper
 * @package common\mappers
 */
class BuildingSiteMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function($var){
            return [
                'id' => $var['id'],
                'type' => $var['type'],
                'name' => $var['name'],
                'transport' => $var['transport'],
                'distance' => $var['distance'],
                'time' => $var['time'],
                'lat' => $var['lat'],
                'lng' => $var['lng'],
            ];
        }, $rawData);
    }
}