<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class SimilarBuildingsParamsMapper
 * @package common\mappers
 */
class SimilarBuildingsParamsMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        $similarParams = [
            'lat' => $rawData['lat'],
            'lng' => $rawData['lng'],
            'radius' => 8,
            'ads_allowed' => true,
        ];
//        if (array_key_exists('rooms', $rawData['attributes'])) {
//            $similarParams['rooms'] = $rawData['attributes']['rooms'];
//        }
//        if (array_key_exists('property_area', $rawData['attributes'])) {
//            $similarParams['property_area']['min'] = (int)($rawData['attributes']['property_area']['value'] * 0.8);
//            $similarParams['property_area']['max'] = (int)($rawData['attributes']['property_area']['value'] * 1.2);
//        }

        return $similarParams;
    }
}