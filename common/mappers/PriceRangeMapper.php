<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class PriceRangeMapper
 * @package common\mappers
 */
class PriceRangeMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array|mixed
     */
    public static function getMappedData($rawData)
    {
        $prices = array_column($rawData, 'price');
        if (empty($prices)) {
            return [];
        }
        return [
            'min' => min($prices),
            'max' => max($prices)
        ];
    }
}