<?php
namespace common\mappers;

use common\components\CurrencyHelper;
use common\interfaces\DataMapperInterface;

/**
 * Class WalletHistoryMapper
 * @package common\mappers
 */
class WalletHistoryMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function ($var) {
            return [
                'id' => $var['id'],
                'type' => $var['type'],
                'total' => $var['total'],
                'totalFormatted' => CurrencyHelper::format($var['currency_code'], $var['total']),
                'template' => $var['template'],
                'created_at' => $var['created_at'],
                'customDataArray' => $var['customDataArray'],
            ];
        }, $rawData);
    }
}