<?php
namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class BuildingDocumentMapper
 * @package common\mappers
 */
class BuildingDocumentMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function($var){
            return [
                'id' => $var['id'],
                'name' => $var['name'],
                'type' => $var['type'],
                'file' => $var['file'],
            ];
        }, $rawData);
    }
}