<?php
namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class UserWalletMapper
 * @package common\mappers
 */
class UserWalletMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return ArrayHelper::map($rawData, 'currency_code', function ($var) {
            return [
                'balance' => $var['balance'],
                'currency_code' => $var['currency_code'],
            ];
        });
    }
}