<?php

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class PropertyRoomMapper
 * @package common\mappers
 */
class PropertyRoomMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array|mixed
     */
    public static function getMappedData($rawData)
    {
        $roomGroups = ArrayHelper::map($rawData, 'id', function($var){ return $var;},
            function($var){
                return $var['attributes']['rooms'] ?? $var['attributes']['bedrooms'] ?? 1;
            }
        );
        $roomGroups = array_map(function($var){
            return [
                'items' => $var,
                'priceRange' => PriceRangeMapper::getMappedData($var)
            ];
        }, $roomGroups);
        ksort($roomGroups);
        return $roomGroups;
    }
}