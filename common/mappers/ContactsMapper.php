<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class ContactsMapper
 * @package common\mappers
 */
class ContactsMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function ($var) {
            return [
                'type' => $var['type'],
                'value' => $var['value']
            ];
        }, $rawData);
    }
}