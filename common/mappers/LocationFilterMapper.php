<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class LocationFilterMapper
 * @package common\mappers
 */
class LocationFilterMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        $params = [];
        switch ($rawData['entity']) {
            case 'city':
                $params = [
                    'lat' => $rawData['lat'],
                    'lng' => $rawData['lng'],
                    'region_id' => $rawData['region_id'],
                    'radius' => 10
                ];

                break;
            case 'country':
                $params = [
                    'country_id' => $rawData['country_id']
                ];

                break;
            case 'region':
                $params = [
                    'region_id' => $rawData['region_id']
                ];

                break;
        }

        return $params;
    }
}