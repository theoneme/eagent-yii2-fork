<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace common\mappers;

use common\interfaces\DataMapperInterface;
use Yii;

/**
 * Class SelectizeUserMapper
 * @package common\mappers
 */
class SelectizeUserMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function ($value) {
            return [
                'id' => $value['id'],
                'value' => Yii::t('main', '{username} ({id} {email} {phone})', [
                    'username' => $value['username'],
                    'id' => "ID: {$value['id']}",
                    'email' => $value['email'] ? "Email: {$value['email']})" : null,
                    'phone' => $value['phone'] ?? null,
                ]),

                'img' => !empty($value['avatar']) ? $value['avatar'] : '/images/agent-no-image.png'
            ];
        }, $rawData);
    }
}