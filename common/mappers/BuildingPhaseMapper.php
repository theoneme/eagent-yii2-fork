<?php
namespace common\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class BuildingPhaseMapper
 * @package common\mappers
 */
class BuildingPhaseMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function($var){
            return [
                'id' => $var['id'],
                'year' => $var['year'],
                'quarter' => $var['quarter'],
                'status' => $var['status'],
                'lat' => $var['lat'],
                'lng' => $var['lng'],
                'floors' => $var['floors'],
            ];
        }, $rawData);
    }
}