<?php


use common\helpers\Morpheus;

return [
    '{value} Room ' => [
        Morpheus::GENDER_MALE => [
            Morpheus::CASE_NOMINATIVE => '{value} Комнатный ',
            Morpheus::CASE_GENITIVE => '{value} Комнатного ',
            Morpheus::CASE_ACCUSATIVE => '{value} Комнатный ',
        ],
        Morpheus::GENDER_FEMALE => [
            Morpheus::CASE_NOMINATIVE => '{value} Комнатная ',
            Morpheus::CASE_GENITIVE => '{value} Комнатной ',
            Morpheus::CASE_ACCUSATIVE => '{value} Комнатную ',
        ],
        Morpheus::GENDER_MIDDLE => [
            Morpheus::CASE_NOMINATIVE => '{value} Комнатное ',
            Morpheus::CASE_GENITIVE => '{value} Комнатного ',
            Morpheus::CASE_ACCUSATIVE => '{value} Комнатное ',
        ],
        Morpheus::GENDER_PLURAL => [
            Morpheus::CASE_NOMINATIVE => '{value} Комнатные ',
            Morpheus::CASE_GENITIVE => '{value} Комнатных ',
            Morpheus::CASE_ACCUSATIVE => '{value} Комнатные ',
        ]
    ],
    '{value} room ' => [
        Morpheus::GENDER_MALE => [
            Morpheus::CASE_NOMINATIVE => '{value}-комнатный ',
            Morpheus::CASE_GENITIVE => '{value}-комнатного ',
            Morpheus::CASE_ACCUSATIVE => '{value}-комнатный ',
        ],
        Morpheus::GENDER_FEMALE => [
            Morpheus::CASE_NOMINATIVE => '{value}-комнатная ',
            Morpheus::CASE_GENITIVE => '{value}-комнатной ',
            Morpheus::CASE_ACCUSATIVE => '{value}-комнатную ',
        ],
        Morpheus::GENDER_MIDDLE => [
            Morpheus::CASE_NOMINATIVE => '{value} комнатное ',
            Morpheus::CASE_GENITIVE => '{value} комнатного ',
            Morpheus::CASE_ACCUSATIVE => '{value} комнатное ',
        ],
        Morpheus::GENDER_PLURAL => [
            Morpheus::CASE_NOMINATIVE => '{value} комнатные ',
            Morpheus::CASE_GENITIVE => '{value} комнатных ',
            Morpheus::CASE_ACCUSATIVE => '{value} комнатные ',
        ]
    ],
    ' {value} bedroom ' => [
        Morpheus::GENDER_MALE => [
            Morpheus::CASE_NOMINATIVE => ' {value}-спальный ',
        ],
        Morpheus::GENDER_FEMALE => [
            Morpheus::CASE_NOMINATIVE => ' {value}-спальная ',
        ],
        Morpheus::GENDER_MIDDLE => [
            Morpheus::CASE_NOMINATIVE => ' {value}-спальное ',
        ],
        Morpheus::GENDER_PLURAL => [
            Morpheus::CASE_NOMINATIVE => ' {value}-спальные ',
        ]
    ],
];
