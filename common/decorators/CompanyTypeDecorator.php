<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Company;
use Yii;

/**
 * Class CompanyTypeDecorator
 * @package common\decorators
 */
class CompanyTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            Company::TYPE_AGENCY => Yii::t('labels', 'Agency'),
            Company::TYPE_DEVELOPER => Yii::t('labels', 'Developer'),
        ];
    }
}
