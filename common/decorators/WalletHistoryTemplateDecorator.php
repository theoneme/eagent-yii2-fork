<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\WalletHistory;
use Yii;

/**
 * Class WalletHistoryTemplateDecorator
 * @package common\decorators
 */
class WalletHistoryTemplateDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @param array $params
     * @return mixed|string
     */
    public static function decorate($rawData, $params = [])
    {
        $labels = static::getTemplateLabels();
        return array_key_exists($rawData, $labels) ? Yii::t('labels', $labels[$rawData], $params) : '';
    }

    /**
     * @return array
     */
    public static function getTemplateLabels()
    {
        return [
            WalletHistory::TEMPLATE_REPLENISH => 'Account replenishment',
            WalletHistory::TEMPLATE_TARIFF => 'Payment for tariff',
        ];
    }
}
