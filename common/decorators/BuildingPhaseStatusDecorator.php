<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\BuildingPhase;
use Yii;
use yii\helpers\Html;

/**
 * Class BuildingPhaseStatusDecorator
 * @package common\decorators
 */
class BuildingPhaseStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = true)
    {
        return [
            BuildingPhase::STATUS_UNFINISHED => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Unfinished'), ['style' => 'color: #ac4137'])
                : Yii::t('labels', 'Unfinished'),
            BuildingPhase::STATUS_FINISHED => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Finished'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'Finished')
        ];
    }
}
