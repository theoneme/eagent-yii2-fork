<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Attribute;
use Yii;

/**
 * Class AttributeTypeDecorator
 * @package common\decorators
 */
class AttributeTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            Attribute::TYPE_NUMBER => Yii::t('labels', 'Number'),
            Attribute::TYPE_DROPDOWN => Yii::t('labels', 'Dropdown'),
            Attribute::TYPE_BOOLEAN => Yii::t('labels', 'Checkbox'),
            Attribute::TYPE_STRING => Yii::t('labels', 'String'),
        ];
    }
}
