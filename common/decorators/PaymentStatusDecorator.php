<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Payment;
use yii\helpers\Html;
use Yii;

/**
 * Class PaymentStatusDecorator
 * @package common\decorators
 */
class PaymentStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = true)
    {
        return [
            Payment::STATUS_PENDING => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Pending'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'Pending'),
            Payment::STATUS_REFUSED => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Refused'), ['style' => 'color: #ac4137'])
                : Yii::t('labels', 'Refused'),
            Payment::STATUS_PAID => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Paid'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Paid'),
        ];
    }
}
