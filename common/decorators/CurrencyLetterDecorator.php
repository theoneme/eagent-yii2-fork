<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use Yii;

/**
 * Class CurrencyLetterDecorator
 * @package common\decorators
 */
class CurrencyLetterDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $letters = static::getLetters();
        return array_key_exists($rawData, $letters) ? $letters[$rawData] : '';
    }

    /**
     * @return array
     */
    public static function getLetters()
    {
        return [
            'RUB' => 'R',
            'USD' => 'D',
            'EUR' => 'E',
            'UAH' => 'H',
            'GEL' => 'G',
            'CNY' => 'Y',
        ];
    }
}
