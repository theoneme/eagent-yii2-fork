<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\CompanyMember;
use Yii;
use yii\helpers\Html;

/**
 * Class CompanyMemberRoleDecorator
 * @package common\decorators
 */
class CompanyMemberStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getRoleLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getRoleLabels($colored = true)
    {
        return [
            CompanyMember::STATUS_INVITED => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Invited'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Invited'),
            CompanyMember::STATUS_ACTIVE => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Active'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Active'),
        ];
    }
}
