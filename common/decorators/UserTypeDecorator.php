<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\User;
use Yii;

/**
 * Class UserTypeDecorator
 * @package common\decorators
 */
class UserTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            User::TYPE_DEFAULT => Yii::t('labels', 'Owner'),
            User::TYPE_REALTOR => Yii::t('labels', 'Realtor'),
        ];
    }
}
