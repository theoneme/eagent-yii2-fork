<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\CompanyMember;
use Yii;
use yii\helpers\Html;

/**
 * Class CompanyMemberRoleDecorator
 * @package common\decorators
 */
class CompanyMemberRoleDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getRoleLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown role');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getRoleLabels($colored = true)
    {
        return [
            CompanyMember::ROLE_OWNER => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Company Owner'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Company Owner'),
            CompanyMember::ROLE_ADMIN => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Administrator'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Administrator'),
            CompanyMember::ROLE_SUPPORT => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Support'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Support'),
            CompanyMember::ROLE_CONTENT_MANAGER => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Content Manager'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Content Manager')
        ];
    }
}
