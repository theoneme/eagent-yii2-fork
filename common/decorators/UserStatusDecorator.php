<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.10.2018
 * Time: 17:42
 */

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\User;
use yii\helpers\Html;
use Yii;

/**
 * Class ProfileStatusDecorator
 * @package common\decorators
 */
class UserStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = true)
    {
        return [
            User::STATUS_ACTIVE => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Published'), ['style' => 'color: #3ab845'])
                : Yii::t('labels', 'Published'),
            User::STATUS_REQUIRES_MODERATION => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Requires moderation'), ['style' => 'color: #2d618c'])
                : Yii::t('labels', 'Requires moderation'),
            User::STATUS_REQUIRES_MODIFICATION => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Requires modification'), ['style' => 'color: #ffd288'])
                : Yii::t('labels', 'Requires modification'),
            User::STATUS_DELETED => $colored === true
                ? Html::tag('span', Yii::t('labels', 'Disabled'), ['style' => 'color: #ac4137'])
                : Yii::t('labels', 'Disabled')];
    }
}
