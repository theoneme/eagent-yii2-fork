<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\BuildingSite;
use Yii;

/**
 * Class BuildingSiteTypeDecorator
 * @package common\decorators
 */
class BuildingSiteTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getLabels()
    {
        return [
            BuildingSite::TYPE_METRO => Yii::t('labels', 'Metro'),
            BuildingSite::TYPE_SCHOOL => Yii::t('labels', 'School'),
            BuildingSite::TYPE_PARK => Yii::t('labels', 'Park'),
            BuildingSite::TYPE_POND => Yii::t('labels', 'Pond'),
            BuildingSite::TYPE_AIRPORT => Yii::t('labels', 'Airport'),
            BuildingSite::TYPE_CITY_CENTER => Yii::t('labels', 'City Center'),
        ];
    }

    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function getIcon($rawData)
    {
        switch ($rawData) {
            case BuildingSite::TYPE_METRO:
                return 'metro.png';
                break;
            case BuildingSite::TYPE_SCHOOL:
                return 'school.png';
                break;
            case BuildingSite::TYPE_PARK:
                return 'park.png';
                break;
            case BuildingSite::TYPE_POND:
                return 'pond.png';
                break;
            case BuildingSite::TYPE_AIRPORT:
                return 'airport.png';
                break;
//            case BuildingSite::TYPE_CITY_CENTER:
            default:
                return 'city-center.png';
                break;
        }
    }
}
