<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\BuildingSite;
use Yii;

/**
 * Class BuildingSiteTransportDecorator
 * @package common\decorators
 */
class BuildingSiteTransportDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown transport');
    }

    /**
     * @return array
     */
    public static function getLabels()
    {
        return [
            BuildingSite::TRANSPORT_FOOT => Yii::t('labels', 'on foot'),
            BuildingSite::TRANSPORT_CAR => Yii::t('labels', 'on car'),
            BuildingSite::TRANSPORT_TRANSPORT => Yii::t('labels', 'on city transport'),
        ];
    }
}
