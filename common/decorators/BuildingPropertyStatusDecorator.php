<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.10.2018
 * Time: 17:42
 */

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\Property;
use yii\helpers\Html;
use Yii;

/**
 * Class BuildingPropertyStatusDecorator
 * @package common\decorators
 */
class BuildingPropertyStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @return array
     */
    private static function getStatusLabels()
    {
        return [
            Property::STATUS_ACTIVE => Html::tag('div', Yii::t('labels', 'Available'), ['class' => 'flat-marker green-mark']),
            Property::STATUS_REQUIRES_MODERATION => Html::tag('div', Yii::t('labels', 'Pending'), ['class' => 'flat-marker yellow-mark']),
            Property::STATUS_REQUIRES_MODIFICATION => Html::tag('div', Yii::t('labels', 'Pending'), ['class' => 'flat-marker yellow-mark']),
            Property::STATUS_PAUSED => Html::tag('div', Yii::t('labels', 'Unavailable'), ['class' => 'flat-marker red-mark']),
            Property::STATUS_SOLD => Html::tag('div', Yii::t('labels', 'Sold'), ['class' => 'flat-marker blue-mark']),
        ];
    }
}
