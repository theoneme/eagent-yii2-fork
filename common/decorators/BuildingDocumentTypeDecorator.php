<?php

namespace common\decorators;

use common\interfaces\DecoratorInterface;
use common\models\BuildingDocument;
use Yii;

/**
 * Class BuildingDocumentTypeDecorator
 * @package common\decorators
 */
class BuildingDocumentTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getLabels()
    {
        return [
            BuildingDocument::TYPE_PERMISSION => Yii::t('labels', 'Building permit'),
            BuildingDocument::TYPE_PROJECT => Yii::t('labels', 'Project declaration'),
            BuildingDocument::TYPE_OPERATION_ACT => Yii::t('labels', 'Operation act'),
        ];
    }
}
