<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:27
 */

namespace common\interfaces;

/**
 * Interface DTOInterface
 * @package common\interfaces
 */
interface DTOInterface
{
    public const MODE_FULL = 0;
    public const MODE_SHORT = 10;

    public function getData($mode);
//    public function getShortData();
//    public function getFullData();
}