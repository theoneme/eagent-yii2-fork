<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:27
 */

namespace common\interfaces;

/**
 * Interface FilterInterface
 * @package common\interfaces
 */
interface FilterInterface
{
    public function buildFilterArray($params);
}