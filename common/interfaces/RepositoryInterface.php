<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:55
 */

namespace common\interfaces;

/**
 * Interface RepositoryInterface
 * @package common\interfaces
 */
interface RepositoryInterface
{
    public const desc = 'DESC';
    public const asc = 'ASC';

    /**
     * @param array $with
     * @return $this
     */
    public function with(array $with = []);

    /**
     * @param array $joinWith
     * @param bool $eagerLoading
     * @param string $joinType
     * @return $this
     */
    public function joinWith(array $joinWith = [], $eagerLoading = true, $joinType = 'LEFT JOIN');

    /**
     * @param array $select
     * @param bool $rewrite
     * @return $this
     */
    public function select(array $select = ['*'], $rewrite = false);

    /**
     * @param int $limit
     * @return $this
     */
    public function limit($limit = 10);

    /**
     * @param $having
     * @return $this
     */
    public function having($having);

    /**
     * @param $indexBy
     * @return $this
     */
    public function indexBy($indexBy);

    /**
     * @param $orderBy
     * @return mixed
     */
    public function orderBy($orderBy);

    /**
     * @param $groupBy
     * @return $this
     */
    public function groupBy($groupBy);

    /**
     * @param $aggregations
     * @return $this
     */
    public function aggregate($aggregations);

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param array $criteria
     * @param bool $returnArray
     * @return mixed
     */
    public function findOneByCriteria(array $criteria = [], $returnArray = false);

    /**
     * @param array $criteria
     * @return mixed
     */
    public function existsByCriteria(array $criteria = []);

    /**
     * @param $relation
     * @param $relationKey
     * @param $condition
     * @return mixed
     */
    public function relationExists($relation, $relationKey, $condition);

    /**
     * @param array $criteria
     * @param bool $returnArray
     * @return mixed
     */
    public function findManyByCriteria(array $criteria = [], $returnArray = false);

    /**
     * @param array $criteria
     * @param string $column
     * @return mixed
     */
    public function findColumnByCriteria(array $criteria = [], $column = 'id');

    /**
     * @param array $criteria
     * @param array $data
     * @return boolean
     */
    public function updateOneByCriteria(array $criteria = [], array $data = []);

    /**
     * @param array $criteria
     * @param array $data
     * @return boolean
     */
    public function updateManyByCriteria(array $criteria = [], array $data = []);

    /**
     * @param array $criteria
     * @return boolean
     */
    public function deleteOneByCriteria(array $criteria = []);

    /**
     * @param array $criteria
     * @return boolean
     */
    public function deleteManyByCriteria(array $criteria = []);

    /**
     * @param array $criteria
     * @return boolean
     */
    public function countByCriteria(array $criteria = []);

    /**
     * @param $id
     * @param $field
     * @param int $count
     * @return boolean
     */
    public function inc($id, $field, $count = 1);

    /**
     * @param $id
     * @param $field
     * @param int $count
     * @return boolean
     */
    public function dec($id, $field, $count = 1);
}