<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.10.2018
 * Time: 17:43
 */

namespace common\interfaces;

/**
 * Interface DecoratorInterface
 * @package common\interfaces
 */
interface DecoratorInterface
{
    public static function decorate($rawData);
}