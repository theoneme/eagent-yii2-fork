<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:27
 */

namespace common\interfaces\filters;

use common\interfaces\FilterInterface;

/**
 * Interface PropertyFilterInterface
 * @package common\interfaces\filters
 */
interface PropertyFilterInterface extends FilterInterface
{
    public function buildFilterArray($params);
}