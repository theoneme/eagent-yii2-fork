<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:27
 */

namespace common\interfaces\filters;

use common\interfaces\FilterInterface;

/**
 * Interface BuildingFilterInterface
 * @package common\interfaces\filters
 */
interface BuildingFilterInterface extends FilterInterface
{
    public function buildFilterArray($params);
}