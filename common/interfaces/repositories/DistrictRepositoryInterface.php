<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface DistrictRepositoryInterface
 * @package common\interfaces\repositories
 */
interface DistrictRepositoryInterface extends RepositoryInterface
{

}