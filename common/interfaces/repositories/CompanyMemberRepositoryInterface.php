<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface CompanyMemberRepositoryInterface
 * @package common\interfaces\repositories
 */
interface CompanyMemberRepositoryInterface extends RepositoryInterface
{

}