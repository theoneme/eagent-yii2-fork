<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface WalletHistoryRepositoryInterface
 * @package common\interfaces\repositories
 */
interface WalletHistoryRepositoryInterface extends RepositoryInterface
{

}