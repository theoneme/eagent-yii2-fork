<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface MicroDistrictRepositoryInterface
 * @package common\interfaces\repositories
 */
interface MicroDistrictRepositoryInterface extends RepositoryInterface
{

}