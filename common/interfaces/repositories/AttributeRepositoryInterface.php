<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 14:37
 */

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface AttributeRepositoryInterface
 * @package common\interfaces\repositories
 */
interface AttributeRepositoryInterface extends RepositoryInterface
{

}