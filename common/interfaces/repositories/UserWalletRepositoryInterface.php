<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface UserWalletRepositoryInterface
 * @package common\interfaces\repositories
 */
interface UserWalletRepositoryInterface extends RepositoryInterface
{

}