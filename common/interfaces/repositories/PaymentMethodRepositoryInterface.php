<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface PaymentMethodRepositoryInterface
 * @package common\interfaces\repositories
 */
interface PaymentMethodRepositoryInterface extends RepositoryInterface
{

}