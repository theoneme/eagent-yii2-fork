<?php

namespace common\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface CompanyRepositoryInterface
 * @package common\interfaces\repositories
 */
interface CompanyRepositoryInterface extends RepositoryInterface
{

}