<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 10:52
 */

namespace common\services;

use Yii;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Class ApiClientService
 * @package common\services
 */
class ApiClientService
{
    /**
     * @var Client
     */
    private $_client;

    /**
     * ApiClientService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->_client = $client;
    }

    /**
     * @param $method
     * @param $url
     * @param $params
     * @return array|bool
     */
    public function makeRequest($method, $url, $params)
    {
        try {
            /** @var Response $response */
            $response = $this->_client->{$method}($url, $params)->send();
            $responseData = $response->getData();
        } catch (\Exception $e) {
            Yii::warning($e->getMessage());
            return false;
        }

        if (!$response->isOk || empty($responseData)) {
            Yii::warning("Request {$url} failed");
            return false;
        }

        return $responseData;
    }
}