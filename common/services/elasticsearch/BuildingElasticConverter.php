<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 10:52
 */

namespace common\services\elasticsearch;

use common\models\Building;
use common\models\elastic\BuildingElastic;

/**
 * Class BuildingElasticConverter
 * @package common\services\elasticsearch
 */
class BuildingElasticConverter
{
    /**
     * @var Building|array
     */
    private $_building;

    /**
     * BuildingElasticConverter constructor.
     * @param array $building
     */
    public function __construct($building)
    {
        $this->_building = $building;
    }

    /**
     * @param bool $save
     * @return bool|BuildingElastic
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        }

        return $object;
    }

    /**
     * @return BuildingElastic
     */
    protected function makeObject()
    {
        $elasticObject = BuildingElastic::find()->where(['id' => $this->_building['id']])->one();

        if ($elasticObject === null) {
            /** @var BuildingElastic $elasticObject */
            $elasticObject = new BuildingElastic();
            $elasticObject->setPrimaryKey((int)$this->_building['id']);
        }

        $sourceAttributes = $this->_building instanceof Building ? $this->_building->attributes : $this->_building;
        $sourceAttributes['ads_allowed'] = (boolean)$sourceAttributes['ads_allowed'];
        $sourceAttributes['ads_allowed_partners'] = (boolean)$sourceAttributes['ads_allowed_partners'];
        $elasticObject->attributes = array_intersect_key($sourceAttributes, $elasticObject->attributes);
        $elasticObject->latlng = ['lat' => $this->_building['lat'], 'lon' => $this->_building['lng']];

        $elasticObject->building_attribute = array_values(array_map(function ($value) {
            return [
                'attribute_id' => $value['attribute_id'],
                'entity_alias' => $value['entity_alias'],
                'value_alias' => $value['value_alias'],
                'value_number' => $value['value_number'],
            ];
        }, $this->_building['buildingAttributes']));

        return $elasticObject;
    }
}