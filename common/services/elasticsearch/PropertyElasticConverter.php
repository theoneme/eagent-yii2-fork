<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 10:52
 */

namespace common\services\elasticsearch;

use common\models\elastic\PropertyElastic;
use common\models\Property;
use common\models\Translation;
use yii\helpers\StringHelper;

/**
 * Class PropertyElasticConverter
 * @package common\services\elasticsearch
 */
class PropertyElasticConverter
{
    /**
     * @var Property|array
     */
    private $_property;

    /**
     * PropertyElasticConverter constructor.
     * @param array $property
     */
    public function __construct($property)
    {
        $this->_property = $property;
    }

    /**
     * @param bool $save
     * @return bool|PropertyElastic
     */
    public function process($save = true)
    {
        $object = $this->makeObject();

        if ($save === true) {
            return $object->save();
        }

        return $object;
    }

    /**
     * @return PropertyElastic
     */
    protected function makeObject()
    {
        $elasticObject = PropertyElastic::find()->where(['id' => $this->_property['id']])->one();

        if ($elasticObject === null) {
            /** @var PropertyElastic $elasticObject */
            $elasticObject = new PropertyElastic();
            $elasticObject->setPrimaryKey((int)$this->_property['id']);
        }

        $sourceAttributes = $this->_property instanceof Property ? $this->_property->attributes : $this->_property;
        $sourceAttributes['ads_allowed'] = (boolean)$sourceAttributes['ads_allowed'];
        $sourceAttributes['ads_allowed_partners'] = (boolean)$sourceAttributes['ads_allowed_partners'];

        $elasticObject->attributes = array_intersect_key($sourceAttributes, $elasticObject->attributes);
        $elasticObject->latlng = ['lat' => $this->_property['lat'], 'lon' => $this->_property['lng']];
        $elasticObject->category = [
            'root' => $this->_property['category']['root'],
            'lft' => $this->_property['category']['lft'],
            'rgt' => $this->_property['category']['rgt'],
        ];

        $elasticObject->property_attribute = array_values(array_map(function ($value) {
            return [
                'attribute_id' => $value['attribute_id'],
                'entity_alias' => $value['entity_alias'],
                'value_alias' => $value['value_alias'],
                'value_number' => $value['value_number'],
            ];
        }, $this->_property['propertyAttributes']));

        $elasticObject->translation = array_values(array_filter(array_map(function ($value) {
            if ($value['key'] === Translation::KEY_DESCRIPTION) {
                return [
                    'key' => $value['key'],
                    'value' => StringHelper::truncate($value['value'], 20),
                    'locale' => $value['locale'],
                ];
            }

            return null;
        }, $this->_property['translations'])));

        return $elasticObject;
    }
}