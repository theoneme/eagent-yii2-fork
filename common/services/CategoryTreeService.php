<?php

namespace common\services;

use common\services\entities\CategoryService;
use Yii;

/**
 * Class CategoryTreeService
 * @package common\services\entities
 */
class CategoryTreeService
{
    /**
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * CategoryTreeService constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->_categoryService = $categoryService;
    }

    /**
     * @return array
     */
    public function getTree()
    {
        $cache = Yii::$app->cache;
        $locale = Yii::$app->language;
        $categoriesCache = $cache->get("categoryTree_{$locale}");
        if ($categoriesCache) {
            return $categoriesCache;
        }

        $categories = $this->_categoryService->getMany(['lvl' => 1]);
        foreach ($categories['items'] as $key => $category) {
            $categories['items'][$key]['children'] = ($category['rgt'] - $category['lft']) === 1 ? [] : $this->getChildrenRecursive($category['id']);
        }
        $cache->set("categoryTree_{$locale}", $categories['items'], 60 * 60 * 24 * 10);

        return $categories['items'];
    }

    /**
     * @param $parentId
     * @return array
     */
    public function getChildrenRecursive($parentId)
    {
        $categories = $this->_categoryService->getMany(['parent_id' => $parentId]);
        foreach ($categories['items'] as $key => $category) {
            $categories['items'][$key]['children'] = ($category['rgt'] - $category['lft']) === 1 ? [] : $this->getChildrenRecursive($category['id']);
        }

        return $categories['items'];
    }
}