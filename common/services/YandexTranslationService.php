<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.10.2017
 * Time: 13:57
 */

namespace common\services;

use Yii;

class YandexTranslationService
{
    /**
     * @var ApiClientService
     */
    private $_apiClientService;

    /**
     * YandexTranslationService constructor.
     * @param ApiClientService $apiClientService
     */
    public function __construct(ApiClientService $apiClientService)
    {
        $this->_apiClientService = $apiClientService;
    }

    /**
     * @param $text
     * @param string $from
     * @param string $to
     * @return array|bool
     */
    public function translate($text, $from = 'ru', $to = 'en')
    {
        $apiKey = Yii::$app->params['yandexAPIKeys'][array_rand(Yii::$app->params['yandexAPIKeys'])];
        $data = $this->_apiClientService->makeRequest('get', 'https://translate.yandex.net/api/v1.5/tr.json/translate', [
            'key' => $apiKey,
            'text' => $text,
            'lang' => $from ? "{$from}-{$to}" : $to,
            'format' => 'html'
        ]);

        if ($data !== false) {
            return $data['text'][0];
        }

        return false;
    }

    /**
     * @param $text
     * @param string $from
     * @param string $to
     * @return bool
     */
    public function translateWithVariables($text, $from = 'ru', $to = 'en')
    {
        $hasVariables = preg_match_all('/{.+?(?:{.*?})*}/u', $text, $matches);
        if ($hasVariables) {
            $variables = $matches[0];
            $replaces = array_map(
                function ($var) {
                    return '{' . $var . '}';
                },
                array_flip($variables)
            );
            $text = strtr($text, $replaces);

            foreach ($replaces as $key => $value) {
                if (strpos($key, 'plural') !== false) {
                    preg_match_all('/(?:one|few|many|other|=0|=1|=2|=3)({.*?})/u', $key, $plurals);
                    $newKey = strtr($key,
                        array_map(
                            function ($v) use ($from, $to) {
                                return $this->translate($v, $from, $to);
                            },
                            array_combine($plurals[1], $plurals[1])
                        )
                    );
                    $replaces[$newKey] = $value;
                    unset($replaces[$key]);
                }
            }
        }
        $translated = $this->translate($text, $from, $to);

        if ($hasVariables) {
            $translated = strtr($translated, array_flip($replaces));
        }

        return $translated;
    }
}