<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.09.2018
 * Time: 14:43
 */

namespace common\services\seo;

use common\components\CurrencyHelper;
use common\helpers\Morpheus;
use common\helpers\UtilityHelper;
use common\models\Property;
use Yii;

/**
 * Class PropertySeoService
 * @package common\services
 */
class PropertySeoService extends SeoService
{
    public const TEMPLATE_APARTMENT = 0;
    public const TEMPLATE_APARTMENT_MALE = 1;
    public const TEMPLATE_APARTMENT_FEMALE = 2;
    public const TEMPLATE_APARTMENT_MIDDLE = 3;
    public const TEMPLATE_APARTMENT_PLURAL = 4;

    public const TEMPLATE_HOUSE = 10;
    public const TEMPLATE_HOUSE_MALE = 11;
    public const TEMPLATE_HOUSE_FEMALE = 12;
    public const TEMPLATE_HOUSE_MIDDLE = 13;
    public const TEMPLATE_HOUSE_PLURAL = 14;

    public const TEMPLATE_COMMERCIAL = 20;
    public const TEMPLATE_COMMERCIAL_MALE = 21;
    public const TEMPLATE_COMMERCIAL_FEMALE = 22;
    public const TEMPLATE_COMMERCIAL_MIDDLE = 23;
    public const TEMPLATE_COMMERCIAL_PLURAL = 24;

    public const TEMPLATE_LAND = 30;
    public const TEMPLATE_LAND_MALE = 31;
    public const TEMPLATE_LAND_FEMALE = 32;
    public const TEMPLATE_LAND_MIDDLE = 33;
    public const TEMPLATE_LAND_PLURAL = 34;

    /**
     * @param $templateCategory
     * @param array $params
     * @param null $locale
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getSeo($templateCategory, $params = [], $locale = null)
    {
        if (isset($params['property_area']) && $params['property_area'] < 15) {
            unset($params['property_area']);
        }
        $processedParams = $this->getAttributeParams($templateCategory, $params, $locale);
        if (isset($params['type'])) {
            switch ($params['type']) {
                case Property::TYPE_SALE:
                    $processedParams['action'] = Yii::t('seo', 'for sale', [], $locale);
                    $processedParams['Action'] = ' | ' . Yii::t('seo', 'Sale', [], $locale);
                    break;
                case Property::TYPE_RENT:
                    $processedParams['action'] = Yii::t('seo', 'for rent', [], $locale);
                    $processedParams['Action'] = ' | ' . Yii::t('seo', 'Rent', [], $locale);
                    break;
            }
        }
        if (!empty($params['city'])) {
            $processedParams['city'] = Yii::t('seo', ' in city {value}', ['value' => $params['city']], $locale);
        }
        if (!empty($params['address'])) {
            $processedParams['Address'] = ' | ' . UtilityHelper::upperFirstLetter($params['address']);
            $processedParams['address'] = Yii::t('seo', ' at {address}', ['address' => $params['address']], $locale);
        }
        if (!empty($params['saddress'])) {
            $processedParams['saddress'] = ' ' . $params['saddress'];
        }
        if (!empty($params['price'])) {
            $processedParams['price'] = ' ' . CurrencyHelper::format(Yii::$app->params['app_currency'], $params['price']);
        }
        if (!empty($params['buildingName'])) {
            $processedParams['BuildingName'] = ' | ' . UtilityHelper::upperFirstLetter($params['buildingName']);
            $processedParams['buildingName'] = '. ' . $params['buildingName'];
        }
        if (!empty($params['category'])) {
            $processedParams['Category'] = UtilityHelper::upperFirstLetter($params['category']);
            $processedParams['category'] = UtilityHelper::lowerFirstLetter($params['category']);
        }

        return parent::getSeo($templateCategory, $processedParams, $locale);
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getTemplates($templateCategory)
    {
        $heading = '';
        $title = '';
        $description = '';
        $keywords = '';
        $textView = '';
        switch ($templateCategory) {
            case self::TEMPLATE_APARTMENT:
            case self::TEMPLATE_APARTMENT_MALE:
            case self::TEMPLATE_APARTMENT_FEMALE:
            case self::TEMPLATE_APARTMENT_MIDDLE:
            case self::TEMPLATE_APARTMENT_PLURAL:
                $heading = '{Category} {action}{buildingName}.{flatNumber}{saddress}{aaarooms}{aaabedrooms}';
                $title = '{Rooms}{Category}{Bedrooms}{Action}{price}{BuildingName}{Address}';
                $description = '{rooms}{category} {action}{bedrooms}{Area}{kitchenArea}';
                $keywords = '{rooms}{nbedrooms}{category} {action}{buildingName}.{flatNumber}{saddress}';
//                $textView = 'universal';
                break;
            case self::TEMPLATE_HOUSE:
            case self::TEMPLATE_HOUSE_MALE:
            case self::TEMPLATE_HOUSE_FEMALE:
            case self::TEMPLATE_HOUSE_MIDDLE:
            case self::TEMPLATE_HOUSE_PLURAL:
                $heading = '{Category} {action}{buildingName}.{flatNumber}{saddress}{aaarooms}{aaabedrooms}{aaafloors}';
                $title = '{Floors}{Rooms}{Category}{Bedrooms}{Action}{price}{BuildingName}{Address}';
                $description = '{floors}{rooms}{category} {action}{bedrooms}{Area}{kitchenArea}';
                $keywords = '{floors}{rooms}{nbedrooms}{category} {action}{buildingName}.{flatNumber}{saddress}';
                break;
            case self::TEMPLATE_COMMERCIAL:
            case self::TEMPLATE_COMMERCIAL_MALE:
            case self::TEMPLATE_COMMERCIAL_FEMALE:
            case self::TEMPLATE_COMMERCIAL_MIDDLE:
            case self::TEMPLATE_COMMERCIAL_PLURAL:
                $heading = '{Category} {action}{saddress}';
                $title = '{Category}{Action}{price}{Address}';
                $description = '{Category} {action}{Area}';
                $keywords = '{Category} {action}{saddress}';
//                $textView = 'universal';
                break;
            case self::TEMPLATE_LAND:
            case self::TEMPLATE_LAND_MALE:
            case self::TEMPLATE_LAND_FEMALE:
            case self::TEMPLATE_LAND_MIDDLE:
            case self::TEMPLATE_LAND_PLURAL:
                $heading = '{Category}{landCategory}{Area} {action}{saddress}';
                $title = '{Category} |{Area}{Action}{price}{Address}';
                $description = '{Category} {action}{Area}';
                $keywords = '{Category}{landCategory}{Area} {action}{saddress}';
//                $textView = 'universal';
                break;
        }
        return [
            'heading' => $heading,
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'textView' => $textView
        ];
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getAttributeTemplates($templateCategory)
    {
        $templates = [];
        $gender = $this->getGender($templateCategory);
        switch ($templateCategory) {
            case self::TEMPLATE_APARTMENT:
            case self::TEMPLATE_APARTMENT_MALE:
            case self::TEMPLATE_APARTMENT_FEMALE:
            case self::TEMPLATE_APARTMENT_MIDDLE:
            case self::TEMPLATE_APARTMENT_PLURAL:
                $templates = [
                    'rooms' => [
                        'Rooms' => [
                            'gender' => $gender,
                            'case' => Morpheus::CASE_NOMINATIVE,
                            'template' => '{value} Room '
                        ],
                        'rooms' => [
                            'gender' => $gender,
                            'case' => Morpheus::CASE_NOMINATIVE,
                            'template' => '{value} room '
                        ],
                        'aaarooms' => '. {value, plural, one{# Room} other{# Rooms}}',
                    ],
                    'bedrooms' => [
                        'Bedrooms' => ' | {value, plural, one{# Bedroom} other{# Bedrooms}}',
                        'bedrooms' => ' {value, plural, one{# bedroom} other{# bedrooms}}',
                        'nbedrooms' => [
                            'gender' => $gender,
                            'case' => Morpheus::CASE_NOMINATIVE,
                            'template' => ' {value} bedroom '
                        ],
                        'aaabedrooms' => '. {value, plural, one{# Bedroom} other{# Bedrooms}}',
                    ],
                    'kitchen_area' => [
                        'kitchenArea' => ' kitchen {value} m²',
                    ],
//                    'flat_number' => [
//                        'flatNumber' => ' apt. {value}'
//                    ]
                ];
                break;
            case self::TEMPLATE_HOUSE:
            case self::TEMPLATE_HOUSE_MALE:
            case self::TEMPLATE_HOUSE_FEMALE:
            case self::TEMPLATE_HOUSE_MIDDLE:
            case self::TEMPLATE_HOUSE_PLURAL:
                $templates = [
                    'rooms' => [
                        'Rooms' => [
                            'gender' => $gender,
                            'case' => Morpheus::CASE_NOMINATIVE,
                            'template' => '{value} Room '
                        ],
                        'rooms' => [
                            'gender' => $gender,
                            'case' => Morpheus::CASE_NOMINATIVE,
                            'template' => '{value} room '
                        ],
                        'aaarooms' => '. {value, plural, one{# Room} other{# Rooms}}',
                    ],
                    'material' => [
                        'Material' => ' Material - {value}',
                        'material' => ' made of «{value}»',
                    ],
                    'floors' => [
                        'Floors' => '{value} Storey ',
                        'floors' => '{value}-storey ',
                        'aaafloors' => '. {value, plural, one{# Floor} other{# Floors}}',
                    ],
                    'bedrooms' => [
                        'Bedrooms' => ' | {value, plural, one{# Bedroom} other{# Bedrooms}}',
                        'bedrooms' => ' {value, plural, one{# bedroom} other{# bedrooms}}',
                        'nbedrooms' => [
                            'gender' => $gender,
                            'case' => Morpheus::CASE_NOMINATIVE,
                            'template' => ' {value} bedroom '
                        ],
                        'aaabedrooms' => '. {value, plural, one{# Bedroom} other{# Bedrooms}}',
                    ],
//                    'flat_number' => [
//                        'flatNumber' => ' apt. {value}'
//                    ],
                ];
                break;
            case self::TEMPLATE_LAND:
            case self::TEMPLATE_LAND_MALE:
            case self::TEMPLATE_LAND_FEMALE:
            case self::TEMPLATE_LAND_MIDDLE:
            case self::TEMPLATE_LAND_PLURAL:
                $templates = [
                    'land_category' => [
                        'landCategory' => ' ({value})',
                    ],
                ];
                break;
        }
        return $templates;
    }

    /**
     * @param $slug
     * @return int|string
     */
    public static function getTemplateByCategory($slug)
    {
        if (Yii::$app->language !== 'ru-RU') {
            $categoryAliases = Yii::$app->cacheLayer->getCategoryAliasCache(Yii::$app->language);
            $slug = $categoryAliases[$slug]['ru-RU'] ?? $slug;
        }
        switch ($slug) {
            case 'flats':
                return self::TEMPLATE_APARTMENT;
                break;
            case 'houses':
                return self::TEMPLATE_HOUSE;
                break;
            case 'commercial-property':
                return self::TEMPLATE_COMMERCIAL;
                break;
            case 'land':
                return self::TEMPLATE_LAND;
                break;
            default:
                return 'KOSTYL';
        }
    }

    /**
     * @param $templateCategory
     * @return int
     */
    public function getGender($templateCategory)
    {
        switch ($templateCategory) {
            case self::TEMPLATE_HOUSE:
            case self::TEMPLATE_APARTMENT_MALE:
            case self::TEMPLATE_HOUSE_MALE:
            case self::TEMPLATE_COMMERCIAL_MALE:
            case self::TEMPLATE_LAND_MALE:
                return Morpheus::GENDER_MALE;
            case self::TEMPLATE_APARTMENT:
            case self::TEMPLATE_COMMERCIAL:
            case self::TEMPLATE_LAND:
            case self::TEMPLATE_APARTMENT_FEMALE:
            case self::TEMPLATE_HOUSE_FEMALE:
            case self::TEMPLATE_COMMERCIAL_FEMALE:
            case self::TEMPLATE_LAND_FEMALE:
                return Morpheus::GENDER_FEMALE;
            case self::TEMPLATE_APARTMENT_MIDDLE:
            case self::TEMPLATE_HOUSE_MIDDLE:
            case self::TEMPLATE_COMMERCIAL_MIDDLE:
            case self::TEMPLATE_LAND_MIDDLE:
                return Morpheus::GENDER_MIDDLE;
            case self::TEMPLATE_APARTMENT_PLURAL:
            case self::TEMPLATE_HOUSE_PLURAL:
            case self::TEMPLATE_COMMERCIAL_PLURAL:
            case self::TEMPLATE_LAND_PLURAL:
                return Morpheus::GENDER_PLURAL;
            default:
                return Morpheus::GENDER_MIDDLE;
        }
    }
}