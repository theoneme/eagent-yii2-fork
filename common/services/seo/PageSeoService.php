<?php

namespace common\services\seo;

use common\components\CurrencyHelper;
use frontend\widgets\IndexTabsWidget;
use Yii;

/**
 * Class PageSeoService
 * @package common\services
 */
class PageSeoService extends SeoService
{
    public const TEMPLATE_INDEX_SELL = 0;
    public const TEMPLATE_INDEX_BUY = 10;
    public const TEMPLATE_INDEX_RENT = 20;
    public const TEMPLATE_INDEX_APPRAISAL = 30;

    /**
     * @param $templateCategory
     * @param array $params
     * @param null $locale
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getSeo($templateCategory, $params = [], $locale = null)
    {
        $processedParams = $this->getAttributeParams($templateCategory, $params, $locale);

        $location = Yii::$app->params['runtime']['location']['object'];
        switch ($params['location_entity']) {
            case 'country':
                $processedParams['City'] = " | {$location['title']}";
                $processedParams['city'] = Yii::t('seo', ' in {value}', ['value' => $location['title']], $locale);
                break;
            case 'region':
                $processedParams['City'] = " | {$location['title']} ({$location['country']['title']})";
                $processedParams['city'] = Yii::t('seo', ' in {value}', ['value' => $location['title']], $locale);
                break;
            case 'city':
            default:
                $processedParams['City'] = " | {$location['title']} ({$location['region']['title']} {$location['country']['title']})";
                $processedParams['city'] = Yii::t('seo', ' in city {value}', ['value' => $location['title']], $locale);
        }
        if (!empty($params['minPrice'])) {
            $processedParams['minPrice'] = ' | ' . Yii::t('seo', 'Prices from {value}', [
                    'value' => CurrencyHelper::format(Yii::$app->params['app_currency'], $params['minPrice'])
                ], $locale);
        }

        return parent::getSeo($templateCategory, $processedParams, $locale);
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getTemplates($templateCategory)
    {
        $heading = '';
        $title = '';
        $description = '';
        $keywords = '';
        $textView = '';
        switch ($templateCategory) {
            case self::TEMPLATE_INDEX_SELL:
//                $heading = 'Buy an apartment{city}';
                $title = 'Sell Property{City} | List Your Property For Free';
                $description = 'We will help to sell a condo or a house{city} and if you need to provide all the necessary services from preparing a condo for sale to a contract of sale. To sell without intermediaries simply with the site eAgent';
                $keywords = 'Sell a condo or a house{city} without intermediaries';
//                $textView = 'universal';
                break;
            case self::TEMPLATE_INDEX_BUY:
//                $heading = 'Buy an apartment{city}';
                $title = 'Buy Property{City} | Convenient Search{minPrice}';
                $description = 'We will help you buy a condo or a house{city} and if you need to provide all the necessary services from the selection of a condo to obtain a mortgage loan and a contract of sale. Buy without intermediaries just with the site eAgent';
                $keywords = 'Buy a condo or a house{city} without intermediaries';
//                $textView = 'universal';
                break;
            case self::TEMPLATE_INDEX_RENT:
//                $heading = 'Buy an apartment{city}';
                $title = 'Rent Property{City} | Convenient Search{minPrice}';
                $description = 'We will help you rent a condo or a house{city} and if you need we will provide all the necessary services from the selection of a condo rental option to the conclusion of a rental agreement. Rent without intermediaries with eAgent site is safe and easy';
                $keywords = 'Rent a condo or house{city} without intermediaries';
//                $textView = 'universal';
                break;
            case self::TEMPLATE_INDEX_APPRAISAL:
//                $heading = 'Buy an apartment{city}';
                $title = 'Free Property Appraisal{City} | We Will Help to Appraise for Free';
                $description = 'We will help you evaluate your condo or house for free{city} and also provide your assessment of the real estate market{city}';
                $keywords = 'Free appraisal of a condo or a house{city}';
//                $textView = 'universal';
                break;
        }
        return [
            'heading' => $heading,
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'textView' => $textView
        ];
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getAttributeTemplates($templateCategory)
    {
        $templates = [];
        switch ($templateCategory) {
            case self::TEMPLATE_INDEX_SELL:
                break;
        }
        return $templates;
    }

    /**
     * @param $operation
     * @return int|string
     */
    public static function getTemplateByOperation($operation)
    {
        $operation = !empty($operation) && in_array($operation, IndexTabsWidget::getOperationList()) ? $operation : IndexTabsWidget::OPERATION_BUY;
        switch ($operation) {
            case IndexTabsWidget::OPERATION_SELL:
                return self::TEMPLATE_INDEX_SELL;
                break;
            case IndexTabsWidget::OPERATION_BUY:
                return self::TEMPLATE_INDEX_BUY;
                break;
            case IndexTabsWidget::OPERATION_RENT:
                return self::TEMPLATE_INDEX_RENT;
                break;
            case IndexTabsWidget::OPERATION_APPRAISAL:
                return self::TEMPLATE_INDEX_APPRAISAL;
                break;
            default:
                return 'KOSTYL';
        }
    }

    /**
     * @param $operation
     * @return float|int|null
     */
    public static function getMinPriceByOperation($operation)
    {
        $operation = !empty($operation) && in_array($operation, IndexTabsWidget::getOperationList()) ? $operation : IndexTabsWidget::OPERATION_BUY;
        switch ($operation) {
            case IndexTabsWidget::OPERATION_BUY:
                return round(CurrencyHelper::convert('USD', Yii::$app->params['app_currency'], 15000) / 1000) * 1000;
                break;
            case IndexTabsWidget::OPERATION_RENT:
                return round(CurrencyHelper::convert('USD', Yii::$app->params['app_currency'], 150) / 50) * 50;
                break;
            default:
                return null;
        }
    }
}