<?php

namespace common\services\seo;

use common\helpers\UtilityHelper;
use Yii;

/**
 * Class BuildingSeoService
 * @package common\services
 */
class BuildingSeoService extends SeoService
{
    public const TEMPLATE_BUILDING = 0;

    /**
     * @param $templateCategory
     * @param array $params
     * @param null $locale
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getSeo($templateCategory, $params = [], $locale = null)
    {
        $processedParams = $this->getAttributeParams($templateCategory, $params, $locale);

        if (!empty($params['address'])) {
            $processedParams['Address'] = ' | (' . UtilityHelper::upperFirstLetter($params['address']) . ')';
            $processedParams['address'] = Yii::t('seo', ' at {address}', ['address' => $params['address']], $locale);
        }
        if (isset($params['count'])) {
            $processedParams['count'] = $params['count'];
        }
        if (isset($params['countSale'])) {
            $processedParams['countSale'] = $params['countSale'];
        }
        if (isset($params['countRent'])) {
            $processedParams['countRent'] = $params['countRent'];
        }
        if (!empty($params['title'])) {
            $processedParams['name'] = Yii::t('seo', ' «{title}»', ['title' => $params['title']]);
        }

        return parent::getSeo($templateCategory, $processedParams, $locale);
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getTemplates($templateCategory)
    {
        $heading = '';
        $title = '';
        $description = '';
        $keywords = '';
        $textView = '';
        switch ($templateCategory) {
            case self::TEMPLATE_BUILDING:
                $heading = 'House{name}{address}';
                $title = '{count, plural, =0{Condos} one{# condo} other{# condos}} for sale(for rent) in House{name}{Address}';
                $description = '{countSale, plural, =0{Condos} one{# condo} other{# condos}} for sale, {countRent, plural, =0{condos} one{# condo} other{# condos}} for rent in a residential house{name}{address}. You can view detailed information about the house and compare prices on eAgent.me portal';
                $keywords = 'Condos for sale and rent in house{name}{address}';
//                $textView = 'universal';
                break;
        }
        return [
            'heading' => $heading,
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'textView' => $textView
        ];
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getAttributeTemplates($templateCategory)
    {
        $templates = [];
        switch ($templateCategory) {
            case self::TEMPLATE_BUILDING:
                break;
        }
        return $templates;
    }
}