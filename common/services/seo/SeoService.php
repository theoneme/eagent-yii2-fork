<?php

namespace common\services\seo;

use common\helpers\Morpheus;
use common\interfaces\SeoServiceInterface;
use common\mappers\AttributeTranslationMapper;
use common\repositories\sql\AttributeValueRepository;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class SeoService
 * @package common\services\seo
 */
class SeoService implements SeoServiceInterface
{
    /**
     * @param $templateCategory
     * @param array $params
     * @param null $locale
     * @return array
     */
    public function getSeo($templateCategory, $params = [], $locale = null)
    {
//        $params['keyword'] = $params['title'] ?? $params['category'] ?? '';
//        foreach (['price', 'minPrice', 'maxPrice'] as $priceParam) {
//            if (isset($params[$priceParam])) {
//                $params[$priceParam] = CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $params[$priceParam]);
//            }
//        }
        $templates = $this->getTemplates($templateCategory);
        foreach ($templates as $k => $v) {
            $templates[$k] = Yii::t('seo', $v, $params, $locale);
            $templates[$k] = preg_replace('/{([^{}]+?({[^{}]*?})?)*}/', '', $templates[$k]);
            $templates[$k] = preg_replace("/\s{2,}/", ' ', $templates[$k]);
        }
        $viewDir = Yii::getAlias('@frontend') . '/views/seo/';
        $contentTemplate = ArrayHelper::remove($templates, 'textView');
        if ($contentTemplate && file_exists($viewDir . $contentTemplate . '.php')) {
            $templates['content'] = preg_replace('/\s\s+/', ' ', Yii::t('seo', Yii::$app->controller->renderPartial('@frontend/views/seo/' . $contentTemplate), $params, $locale));
        }

        return $templates;
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getTemplates($templateCategory)
    {
        return [];
    }

    /**
     * @return array
     */
    public function getCommonAttributeTemplates()
    {
        return [
//            'city' => [
//                'city' => ' in city {value}'
//            ],
            'property_area' => [
                'area' => ' area {value} m²',
                'Area' => ' {value} m²',
            ],
        ];
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getAttributeTemplates($templateCategory)
    {
        return [];
    }

    /**
     * @param $templateCategory
     * @param $attributes
     * @param null $locale
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getAttributeParams($templateCategory, $attributes, $locale = null)
    {
        $params = [];
        $templates = ArrayHelper::merge($this->getCommonAttributeTemplates(), $this->getAttributeTemplates($templateCategory));
        $attributeCustomTemplates = array_intersect_key($templates, $attributes);
        $attributes = array_intersect_key($attributes, $templates);
//        UtilityHelper::debug($attributes);
        if (!empty($attributes)) {
//            $baseCondition = ['and',
////            'attr_value_translations.locale' => 'ru-RU',
////                ['attr_value_translations.key' => 'title'],
//            ];
            $baseCondition = ['and'];
            $attributeConditions = array_map(function ($key, $value) {
                return [
                    'attribute.alias' => $key,
                    'attribute_value.alias' => $value,
                ];
            }, array_keys($attributes), $attributes);
            if (!empty($attributeConditions)) {
                $attributeConditions = array_merge(['or'], $attributeConditions);
                $baseCondition[] = $attributeConditions;
            }
            $attributeTranslations = AttributeTranslationMapper::getMappedData(
                Yii::$container->get(AttributeValueRepository::class)
                    ->with(['translations'])
                    ->joinWith(['attr'])
                    ->findManyByCriteria($baseCondition)
            );

            foreach ($attributeCustomTemplates as $attribute => $attributeParams) {
                $valueAlias = ArrayHelper::remove($attributes, $attribute);
                if ($valueAlias !== null) {
                    if (isset($valueAlias['min']) || isset($valueAlias['max'])) {
                        $attributeParamParams = [
                            'minValue' => isset($valueAlias['min']) ? Yii::t('seo', ' from {value}', ['value' => $valueAlias['min']], $locale) : null,
                            'maxValue' => isset($valueAlias['max']) ? Yii::t('seo', ' to {value}', ['value' => $valueAlias['max']], $locale) : null,
                        ];
                    } else {
                        $value = $attributeTranslations[$attribute] ?? null;
                        if (!empty($value)) {
                            $attributeParamParams = [
                                'value' => $value,
                            ];
                        }
                    }
                    if (!empty($attributeParamParams)) {
                        foreach ($attributeParams as $paramAlias => $paramTemplate) {
                            if (is_array($paramTemplate)) {
                                if (isset($paramTemplate['template'], $paramTemplate['gender'], $paramTemplate['case'])) {
                                    $params[$paramAlias] = Morpheus::t('seo', $paramTemplate['template'], $paramTemplate['gender'], $paramTemplate['case'], $attributeParamParams, $locale);
                                }
                            } else {
                                $params[$paramAlias] = Yii::t('seo', $paramTemplate, $attributeParamParams, $locale);
                            }
                        }
                    }
                }
            }
        }
        return $params;
    }
}