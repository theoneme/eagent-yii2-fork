<?php

namespace common\services\seo;

use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Property;
use common\services\entities\CityService;
use common\services\entities\PropertyService;
use common\services\entities\RegionService;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * Class IndexCrosslinkService
 * @package common\services\seo
 */
class IndexCrosslinkService
{
    /**
     * @var RegionService
     */
    private $_regionService;
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;

    /**
     * IndexCrosslinkService constructor.
     * @param RegionService $regionService
     * @param CityService $cityService
     * @param PropertyService $propertyService
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(RegionService $regionService, CityService $cityService, PropertyService $propertyService, CategoryRepositoryInterface $categoryRepository)
    {
        $this->_cityService = $cityService;
        $this->_propertyService = $propertyService;
        $this->_regionService = $regionService;
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * @param array $locationData
     * @return array
     */
    public function getData(array $locationData)
    {
        $locale = Yii::$app->language;
        $data = Yii::$app->cache->get("index-crosslink-{$locale}-{$locationData['slug']}");

        if (!$data) {
            $data = [
                0 => [],
                1 => [],
                2 => [],
                3 => []
            ];
            switch ($locationData['entity']) {
                case 'city':
                    $cities = $this->_cityService->getMany(['region_id' => $locationData['region_id']], ['limit' => 10]);
                    $column = 0;
                    foreach ($cities['items'] as $city) {
                        $data[$column++ % 4][] = [
                            'title' => Yii::t('seo', 'Real estate in {city}', ['city' => $city['title']]),
                            'link' => Url::to(['/site/index', 'app_city' => $city['slug']])
                        ];
                        $data[$column++ % 4][] = [
                            'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Flats'), 'city' => $city['title']]),
                            'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                        ];
                        $data[$column++ % 4][] = [
                            'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Houses'), 'city' => $city['title']]),
                            'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                        ];
                        $data[$column++ % 4][] = [
                            'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Land'), 'city' => $city['title']]),
                            'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['land'] ?? 'land', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                        ];
                        $data[$column++ % 4][] = [
                            'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Commercial Property'), 'city' => $city['title']]),
                            'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['commercial-property'] ?? 'commercial-property', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                        ];
                    }

                    break;
                case 'region':
                    $category = $this->_categoryRepository->select(['id', 'root', 'lft', 'rgt'], true)->findOneByCriteria(['id' => 2], true);
                    $properties = $this->_propertyService->getMany([
                        'status' => Property::STATUS_ACTIVE,
//                        'category_id' => 2,
                        'root' => $category['root'],
                        'lft' => $category['lft'],
                        'rgt' => $category['rgt'],
                        'ads_allowed' => true
                    ], ['limit' => 10]);
                    foreach ($properties['items'] as $property) {
                        $data[0][] = [
                            'title' => $property['title'],
                            'link' => Url::to(['/property/property/show', 'slug' => $property['slug']])
                        ];
                    }

                    $cities = $this->_cityService->getMany(['region_id' => $locationData['region_id']], [
                        'limit' => 30,
                        'orderBy' => new Expression('rand()')
                    ]);

                    $column = 0;
                    foreach ($cities['items'] as $key => $city) {
                        switch ($column % 3) {
                            case 0:
                                $data[1][] = [
                                    'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Flats'), 'city' => $city['title']]),
                                    'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                                ];

                                break;
                            case 1:
                                $data[2][] = [
                                    'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Houses'), 'city' => $city['title']]),
                                    'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                                ];

                                break;
                            case 2:
                                $data[3][] = [
                                    'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Commercial Property'), 'city' => $city['title']]),
                                    'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['commercial-property'] ?? 'commercial-property', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                                ];
                        }

                        $column++;
                    }

                    break;

                case 'country':
                    $cities = $this->_cityService->getMany(['country_id' => $locationData['country_id']], [
                        'limit' => 30,
                        'orderBy' => new Expression('rand()')
                    ]);
                    $regions = $this->_regionService->getMany(['country_id' => $locationData['country_id']], [
                        'limit' => 10,
                        'orderBy' => new Expression('rand()')
                    ]);

                    foreach ($regions['items'] as $key => $region) {
                        $data[0][] = [
                            'title' => Yii::t('seo', 'Real Estate {region}', ['region' => $region['title']]),
                            'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => Property::OPERATION_SALE, 'app_city' => "region-{$region['slug']}"])
                        ];
                    }

                    $column = 0;
                    foreach ($cities['items'] as $key => $city) {
                        switch ($column % 3) {
                            case 0:
                                $data[1][] = [
                                    'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Flats'), 'city' => $city['title']]),
                                    'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                                ];

                                break;
                            case 1:
                                $data[2][] = [
                                    'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Houses'), 'city' => $city['title']]),
                                    'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                                ];

                                break;
                            case 2:
                                $data[3][] = [
                                    'title' => Yii::t('seo', '{entity} for sale in {city}', ['entity' => Yii::t('main', 'Commercial Property'), 'city' => $city['title']]),
                                    'link' => Url::to(['/property/catalog/index', 'category' => $categorySlugs['commercial-property'] ?? 'commercial-property', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']])
                                ];
                        }

                        $column++;
                    }
            }

            Yii::$app->cache->set("index-crosslink-{$locale}-{$locationData['slug']}", $data, 26200);
        }

        return $data;
    }
}