<?php

namespace common\services\seo;

/**
 * Class UserSeoService
 * @package common\services
 */
class UserSeoService extends SeoService
{
    public const TEMPLATE_USER = 0;
    public const TEMPLATE_AGENT = 10;

    /**
     * @param $templateCategory
     * @param array $params
     * @param null $locale
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getSeo($templateCategory, $params = [], $locale = null)
    {
        $processedParams = $this->getAttributeParams($templateCategory, $params, $locale);

        if (!empty($params['name'])) {
            $processedParams['name'] = $params['name'];
        }
        if (!empty($params['description'])) {
            $processedParams['description'] = $params['description'];
        }
        return parent::getSeo($templateCategory, $processedParams, $locale);
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getTemplates($templateCategory)
    {
        $heading = '';
        $title = '';
        $description = '';
        $keywords = '';
        $textView = '';
        switch ($templateCategory) {
            case self::TEMPLATE_USER:
                break;
            case self::TEMPLATE_AGENT:
                $heading = 'Real estate agent {name}';
                $title = 'Real estate agent {name}';
                $description = '{description}';
                $keywords = 'Real estate agent {name}';
                break;
        }
        return [
            'heading' => $heading,
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'textView' => $textView
        ];
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getAttributeTemplates($templateCategory)
    {
        $templates = [];
        switch ($templateCategory) {
            case self::TEMPLATE_USER:
                break;
            case self::TEMPLATE_AGENT:
                break;
        }
        return $templates;
    }
}