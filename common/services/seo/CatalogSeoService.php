<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.09.2018
 * Time: 14:43
 */

namespace common\services\seo;

use common\components\CurrencyHelper;
use common\helpers\UtilityHelper;
use common\mappers\SeoAttributesMapper;
use common\models\Building;
use common\models\Property;
use Yii;

/**
 * Class CatalogSeoService
 * @package common\services
 */
class CatalogSeoService extends SeoService
{
    public const TEMPLATE_CATEGORY = 0;
    public const TEMPLATE_AGENT = 20;
    public const TEMPLATE_BUILDING = 30;

    /**
     * @param $templateCategory
     * @param array $params
     * @param null $locale
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getSeo($templateCategory, $params = [], $locale = null)
    {
        $processedParams = $this->getAttributeParams($templateCategory, $params, $locale);
        if (isset($params['operation'])) {
            switch ($params['operation']) {
                case Property::OPERATION_SALE:
                    $processedParams['action'] = Yii::t('seo', 'Sale', [], $locale) . ' ';
                    $processedParams['Action'] = '(' . Yii::t('seo', 'Sale', [], $locale) . ')';
                    $processedParams['forAction'] = Yii::t('seo', 'for sale', [], $locale);
                    $processedParams['aboutAction'] = Yii::t('seo', 'about sale', [], $locale);
                    break;
                case Property::OPERATION_RENT:
                    $processedParams['action'] = Yii::t('seo', 'Rent', [], $locale) . ' ';
                    $processedParams['Action'] = '(' . Yii::t('seo', 'Rent', [], $locale) . ')';
                    $processedParams['forAction'] = Yii::t('seo', 'for rent', [], $locale);
                    $processedParams['aboutAction'] = Yii::t('seo', 'about rent', [], $locale);
                    break;
            }
        }

        if (!array_key_exists('zoom', $params) && array_key_exists('location_entity', $params)) {
            $location = Yii::$app->params['runtime']['location']['object'];

            switch ($params['location_entity']) {
                case 'country':
                    $processedParams['City'] = " | {$location['title']}";
                    $processedParams['city'] = Yii::t('seo', ' in {value}', ['value' => $location['title']], $locale);
                    break;
                case 'region':
                    $processedParams['City'] = " | {$location['title']} ({$location['country']['title']})";
                    $processedParams['city'] = Yii::t('seo', ' in {value}', ['value' => $location['title']], $locale);
                    break;
                case 'city':
                default:
                    $processedParams['City'] = " | {$location['title']}";
                    if (!empty($params['district_title'])) {
                        $processedParams['City'] .= " " . Yii::t('seo', '(district {district})', ['district' => $params['district_title']]);
                    }
                    else if (!empty($params['micro_district_title'])) {
                        $processedParams['City'] .= " " . Yii::t('seo', '(microdistrict {microdistrict})', ['microdistrict' => $params['micro_district_title']]);
                    }
                    $processedParams['city'] = Yii::t('seo', ' in city {value}', ['value' => $location['title']], $locale);
            }
        }

        if (isset($params['count'])) {
            $processedParams['count'] = $params['count'];
        }
        if (!empty($params['attributes'])) {
            $attributesString = SeoAttributesMapper::getMappedData($params['attributes']);
            $attributesTitleString = SeoAttributesMapper::getMappedData($params['attributes'], SeoAttributesMapper::MODE_TITLE);
            if (!empty($attributesString)) {
                $processedParams['Attributes'] = " | {$attributesTitleString}";
                $processedParams['attributes'] = " | {$attributesString}";
            }
        }
        if (!empty($params['category'])) {
            $processedParams['Category'] = UtilityHelper::upperFirstLetter($params['category']);
            $processedParams['category'] = UtilityHelper::lowerFirstLetter($params['category']);
        }
        if (!empty($params['minPrice'])) {
            $processedParams['minPrice'] = ' | ' . Yii::t('seo', 'Prices from {value}', [
                    'value' => CurrencyHelper::format(Yii::$app->params['app_currency'], $params['minPrice'])
                ], $locale);
        }
        if (!empty($params['maxPrice'])) {
            $processedParams['maxPrice'] = Yii::t('seo', ' to {value}', [
                'value' => CurrencyHelper::format(Yii::$app->params['app_currency'], $params['maxPrice'])
            ], $locale);
        }
        if (isset($params['buildingType'])) {
            switch ($params['buildingType']) {
                case Building::TYPE_DEFAULT:
                    $processedParams['buildingType'] = Yii::t('seo', 'Buildings', [], $locale) . ' ';
                    $processedParams['BuildingType'] = '(' . Yii::t('seo', 'Buildings', [], $locale) . ')';
                    break;
                case Building::TYPE_NEW_CONSTRUCTION:
                    $processedParams['buildingType'] = Yii::t('seo', 'New constructions', [], $locale) . ' ';
                    $processedParams['BuildingType'] = '(' . Yii::t('seo', 'New constructions', [], $locale) . ')';
                    break;
            }
        }
        return parent::getSeo($templateCategory, $processedParams, $locale);
    }

    /**
     * @param $templateCategory
     * @return array
     */
    public function getTemplates($templateCategory)
    {
        $heading = '';
        $title = '';
        $description = '';
        $keywords = '';
        $emptyText = '';
        $textView = '';
        switch ($templateCategory) {
            case self::TEMPLATE_CATEGORY:
                $heading = '{count, plural, =0{} other{#}} {Category} {Action} {City} {attributes}';
                $title = '{count, plural, =0{} other{#}} {Category} {forAction}{Attributes}{City}{minPrice}{maxPrice} | List your property for free on eAgent.me website';
                $description = 'On our site you can find {category} {forAction}, as well as place your ads {aboutAction} of {category} for free. eAgent.me is a convenient real estate portal. Free placement of ads about sale and rent of real estate{city}';
                $keywords = '{category} {action}{city}';
                $emptyText = 'There is no {category} {forAction} in this city. You can be the first to place an announcement {aboutAction} or request for what interests you.';
//                $textView = 'universal';
                break;
            case self::TEMPLATE_AGENT:
                $heading = 'Real estate agencies and agents{city}';
                $title = '{count, plural, =0{Agencies and Agents} one{# Agency or Agent} other{# Agencies and Agents}} (Real estate){City}';
                $description = 'Our real estate agents will help you not only to find the best options for real estate, but also help you get mortgage loans and issue a deal';
                $keywords = 'Real estate agencies and agents{city}';
//                $textView = 'universal';
                break;
            case self::TEMPLATE_BUILDING:
                $heading = '{buildingType}{attributes}{City}';
                $title = '{count, plural, =0{} other{#}} {BuildingType}{Attributes}{City}';
                $description = 'We will help you not only to choose the best option, but also to process documents for free';
                $keywords = '{buildingType}{city}';
                $emptyText = 'There is no buildings found for your request';
                break;
        }
        return [
            'heading' => $heading,
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'emptyText' => $emptyText,
            'textView' => $textView,
        ];
    }


    /**
     * @param $templateCategory
     * @return array
     */
    public function getAttributeTemplates($templateCategory)
    {
        $templates = [];
        switch ($templateCategory) {
            case self::TEMPLATE_CATEGORY:
                $templates = [
//                    'app_city' => [
//                        'city' => ' in city {value}'
//                    ],
                ];
                break;
        }
        return $templates;
    }
}