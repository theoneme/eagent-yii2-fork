<?php

namespace common\services;

use common\interfaces\repositories\BuildingRepositoryInterface;
use common\models\search\advanced\BuildingCitySearch;
use common\models\search\advanced\BuildingLightSearch;
use common\models\search\advanced\BuildingRegionSearch;
use common\models\search\advanced\CityLightSearch;
use common\models\search\advanced\RegionLightSearch;
use Yii;

/**
 * Class BuildingMarkerService
 * @package common\services
 */
class BuildingMarkerService
{
    /**
     * @var BuildingRepositoryInterface
     */
    private $_buildingRepository;

    /**
     * BuildingMarkerService constructor.
     * @param BuildingRepositoryInterface $buildingRepository
     */
    public function __construct(BuildingRepositoryInterface $buildingRepository)
    {
        $this->_buildingRepository = $buildingRepository;
    }

    /**
     * @param $params
     * @return array
     */
    public function getMarkers($params)
    {
        $params['zoom'] = $params['zoom'] ?? 11;
        $params['ads_allowed'] = true;
        if ($params['zoom'] > 10) {
            /** @var BuildingLightSearch $markerSearch */
            $markerSearch = Yii::createObject(BuildingLightSearch::class);
//            $markerSearch = new BuildingLightSearch($this->_buildingRepository);
            $markers = $markerSearch->search($params);
        } else
            if ($params['zoom'] > 6) {
                /** @var BuildingCitySearch $buildingSearch */
                $buildingSearch = new BuildingCitySearch($this->_buildingRepository, ['indexBy' => 'city_id']);
                $buildingCounts = $buildingSearch->search($params);
                $params['id'] = array_keys($buildingCounts['items']);

                /** @var CityLightSearch $markerSearch */
                $markerSearch = Yii::$container->get(CityLightSearch::class);
                $markers = $markerSearch->search($params);

                if (count($markers['items'])) {
                    $markers['items'] = array_reduce($markers['items'], function ($carry, $value) use ($buildingCounts) {
                        if (isset($buildingCounts['items'][$value['id']])) {
                            $value['content'] = Yii::t('catalog', '{count, plural, one{# object} other{# objects}}', [
                                'count' => $buildingCounts['items'][$value['id']]
                            ]);
                            $value['type'] = 'city';
                            $value['count'] = $buildingCounts['items'][$value['id']];
                            $carry[] = $value;
                        }
                        return $carry;
                    }, []);
                }
            } else {
                /** @var BuildingRegionSearch $buildingSearch */
                $buildingSearch = new BuildingRegionSearch($this->_buildingRepository, ['indexBy' => 'region_id']);
                $buildingCounts = $buildingSearch->search($params);

                $regionParams['id'] = array_map(function ($value) {
                    return $value['region_id'];
                }, $buildingCounts['items']);

                /** @var RegionLightSearch $markerSearch */
                $markerSearch = Yii::$container->get(RegionLightSearch::class);
                $markers = $markerSearch->search($regionParams);

                if (count($buildingCounts['items'])) {
                    $markers['items'] = array_reduce($markers['items'], function ($carry, $value) use ($buildingCounts) {
                        if (isset($buildingCounts['items'][$value['id']])) {
                            $value['content'] = Yii::t('catalog', '{count, plural, one{# object} other{# objects}}', [
                                'count' => $buildingCounts['items'][$value['id']]['buildingCount']
                            ]);
                            $value['type'] = 'city';
                            $value['count'] = $buildingCounts['items'][$value['id']]['buildingCount'];
                            $value['lat'] = $buildingCounts['items'][$value['id']]['avgLat'];
                            $value['lng'] = $buildingCounts['items'][$value['id']]['avgLng'];
                            $carry[] = $value;
                        }
                        return $carry;
                    }, []);
                }
            }

        return $markers;
    }
}