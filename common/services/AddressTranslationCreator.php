<?php

namespace common\services;

use common\repositories\sql\AddressTranslationRepository;
use Yii;

/**
 * Class AddressTranslationCreator
 * @package common\services
 */
class AddressTranslationCreator
{
    /**
     * @var AddressTranslationRepository
     */
    private $_addressTranslationRepository;

    /**
     * @var GoogleMapsService
     */
    private $_googleMapsService;

    /**
     * AddressTranslationCreator constructor.
     * @param AddressTranslationRepository $addressTranslationRepository
     * @param GoogleMapsService $googleMapsService
     */
    public function __construct(AddressTranslationRepository $addressTranslationRepository, GoogleMapsService $googleMapsService)
    {
        $this->_addressTranslationRepository = $addressTranslationRepository;
        $this->_googleMapsService = $googleMapsService;
    }

    /**
     * @param $data
     */
    public function create($data)
    {
        if (!empty($data['lat']) && !empty($data['lng'])) {
            $data['lat'] = round($data['lat'], 8);
            $data['lng'] = round($data['lng'], 8);
            $addressData = ['lat' => $data['lat'], 'lng' => $data['lng'], 'locale' => $data['locale']];
            if (!$this->_addressTranslationRepository->existsByCriteria($addressData)) {
                $locationData = $this->_googleMapsService->reverseGeocodeAsData($data['lat'], $data['lng'], Yii::$app->params['supportedLocales'][$data['locale']]);
                if ($locationData !== false) {
                    $addressData['title'] = $this->_googleMapsService->getAddressFromData($locationData);
                    $addressData['customDataArray'] = $locationData;
                    $this->_addressTranslationRepository->create($addressData);
                }
            }
        }
    }
}