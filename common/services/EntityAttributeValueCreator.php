<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2018
 * Time: 16:41
 */

namespace common\services;

use common\exceptions\ClassNotFoundException;
use common\services\entities\AttributeService;

/**
 * Class EntityAttributeValueCreator
 * @package common\services
 */
class EntityAttributeValueCreator
{
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var AttributeValueCreator
     */
    private $_attributeValueCreator;

    /**
     * EntityAttributeValueCreator constructor.
     * @param AttributeValueCreator $attributeValueCreator
     * @param AttributeService $attributeService
     */
    public function __construct(AttributeValueCreator $attributeValueCreator, AttributeService $attributeService)
    {
        $this->_attributeValueCreator = $attributeValueCreator;
        $this->_attributeService = $attributeService;
    }

    /**
     * @param $className
     * @param $attributeId
     * @param $value
     * @param array $params
     * @return mixed
     * @throws ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     */
    public function makeObject($className, $attributeId, $value, array $params = [])
    {
        if (!class_exists($className)) {
            throw new ClassNotFoundException("Johnny, I can`t feel class {$className}! It`s because you don`t have class! And legs.");
        }

        $attributeValue = $this->_attributeValueCreator->makeObject($attributeId, $value, $params);

        if ($attributeValue !== null) {
            $attribute = $this->_attributeService->getOne(['attribute.id' => $attributeId]);

            $attributeObject = new $className([
                'attribute_id' => $attributeId,
                'entity_alias' => $attribute['alias'],
                'value_alias' => $attributeValue['alias'],
                'value' => $attributeValue['id'],
                'value_number' => is_numeric($attributeValue['title']) ? $attributeValue['title'] : null
            ]);

            return $attributeObject;
        }

        return null;
    }
}