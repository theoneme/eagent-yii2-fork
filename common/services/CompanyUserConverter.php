<?php

namespace common\services;

use common\decorators\CompanyTypeDecorator;
use common\interfaces\ConverterInterface;
use common\models\UserContact;

/**
 * Class CompanyUserConverter
 * @package console\services
 */
class CompanyUserConverter implements ConverterInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public function convertObject($rawData)
    {
        if (empty($rawData['companyData']) || empty($rawData['userData'])) {
            return [];
        }
        $data = $rawData['userData'];
        $data['username'] = $rawData['companyData']['title'];
        $data['email'] = array_reduce(
            $rawData['companyData']['contacts'] ?? [],
            function ($carry, $var) {
                return !$carry || $var['type'] === UserContact::TYPE_EMAIL ? $var['value'] : $carry;
            }
        );
        $data['phone'] = array_reduce(
            $rawData['companyData']['contacts'] ?? [],
            function ($carry, $var) {
                return !$carry || $var['type'] === UserContact::TYPE_PHONE ? $var['value'] : $carry;
            }
        );
        $data['lat'] = $rawData['companyData']['lat'];
        $data['lng'] = $rawData['companyData']['lng'];
        $data['avatar'] = $rawData['companyData']['logo'];
        $data['thumb'] = $rawData['companyData']['thumb'] ?? $rawData['companyData']['logo'];
        $data['translations'] = $rawData['companyData']['translations'];
        $data['address'] = $rawData['companyData']['address'];
        $data['addressData'] = $rawData['companyData']['addressData'];
        $data['contacts'] = $rawData['companyData']['contacts'] ?? [];
        $data['type'] = CompanyTypeDecorator::decorate($rawData['companyData']['type']);

        return $data;
    }
}
