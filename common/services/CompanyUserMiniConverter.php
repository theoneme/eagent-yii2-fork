<?php

namespace common\services;

use common\decorators\CompanyTypeDecorator;
use common\interfaces\ConverterInterface;
use common\models\UserContact;

/**
 * Class CompanyUserConverter
 * @package console\services
 */
class CompanyUserMiniConverter implements ConverterInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public function convertObject($rawData)
    {
        if (empty($rawData['companyData'])) {
            return [];
        }
        $data = $rawData['userData'] ?? [];
        $data['username'] = $rawData['companyData']['title'];
        $data['email'] = array_reduce(
            $rawData['userData']['contacts'] ?? [],
            function($carry, $var){
                return !$carry || $var['type'] === UserContact::TYPE_EMAIL ? $var['value'] : $carry;
            }
        );
        $data['phone'] = array_reduce(
            $rawData['userData']['contacts'] ?? [],
            function($carry, $var){
                return !$carry || $var['type'] === UserContact::TYPE_PHONE ? $var['value'] : $carry;
            }
        );
        $data['avatar'] = $rawData['companyData']['logo'];
        $data['type'] = CompanyTypeDecorator::decorate($rawData['companyData']['type']);

        return $data;
    }
}
