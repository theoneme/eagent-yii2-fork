<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2018
 * Time: 16:41
 */

namespace common\services;

use common\exceptions\EntityNotCreatedException;
use common\helpers\UtilityHelper;
use common\models\AttributeValue;
use common\models\Translation;
use common\services\entities\AttributeValueService;
use yii\helpers\StringHelper;

/**
 * Class AttributeValueService
 * @package common\services
 */
class AttributeValueCreator
{
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;

    /**
     * AttributeValueCreator constructor.
     * @param AttributeValueService $attributeValueService
     */
    public function __construct(AttributeValueService $attributeValueService)
    {
        $this->_attributeValueService = $attributeValueService;
    }

    /**
     * @param $attributeId
     * @param $value
     * @param array $params
     * @return array
     * @throws EntityNotCreatedException
     */
    public function makeObject($attributeId, $value, array $params = [])
    {
        $attributeObject = null;
        $locale = $params['locale'] ?? 'ru-RU';
        $status = $params['status'] ?? AttributeValue::STATUS_PENDING;
        $isId = $params['isId'] ?? null;
        $value = trim($value);

        $condition = ['or',
            ['attr_value_translations.value' => $value, 'attr_value_translations.key' => Translation::KEY_TITLE],
            ['attribute_value.id' => $value]
        ];

        if ($isId === true && is_numeric($value)) {
            $condition = ['attribute_value.id' => $value];
        }
        if ($isId === false) {
            $condition = ['attr_value_translations.value' => $value, 'attr_value_translations.key' => Translation::KEY_TITLE];
        }

        $attributeValue = $this->_attributeValueService->getOne([
            'and',
            ['attribute_id' => $attributeId],
            $condition
        ]);

        if ($attributeValue === null) {
            $insertId = $this->_attributeValueService->create([
                'AttributeValueForm' => [
                    'attribute_id' => $attributeId,
                    'status' => $status,
                    'alias' => StringHelper::truncate(UtilityHelper::transliterate($value), 55, null),
                ],
                'MetaForm' => [
                    $locale => [
                        'title' => $value,
                    ]
                ]
            ]);

            if ($insertId !== null) {
                $attributeValue = $this->_attributeValueService->getOne(['attribute_value.id' => $insertId]);
            } else {
                throw new EntityNotCreatedException('Entity "AttributeValue" failed to create for some reason');
            }
        }

        return $attributeValue;
    }
}