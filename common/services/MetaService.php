<?php

namespace common\services;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Class MetaService
 * @package common\services
 */
class MetaService
{
    /**
     * @var Controller
     */
    private $_controller;

    /**
     * @param Controller $controller
     */
    public function __construct(Controller $controller)
    {
        $this->_controller = $controller;
    }

    /**
     * @param $data
     */
    public function registerMeta($data)
    {
        $title = $data['title'] ?? Yii::$app->name;
        $description = $data['description'] ?? '';
        $keywords = $data['keywords'] ?? '';

        $this->_controller->view->title = $title;
        $this->_controller->view->registerMetaTag(['name' => 'description', 'content' => Html::encode($description)], 'description');
        $this->_controller->view->registerMetaTag(['name' => 'keywords', 'content' => Html::encode($keywords)], 'keywords');
        $this->registerOpenGraphMeta($data);
    }

    /**
     * @param $data
     */
    public function registerOpenGraphMeta($data)
    {
        $title = $data['title'] ?? Yii::$app->name;
        $description = $data['description'] ?? null;
        $siteName = Yii::$app->name;
        $url = Yii::$app->request->absoluteUrl;
        $type = 'article';
        $locale = str_replace('-', '_', Yii::$app->language);
        $image = $data['image'] ?? null;

        $this->_controller->view->registerMetaTag(['property' => 'og:title', 'content' => $title], 'og:title');
        if ($description !== null) {
            $this->_controller->view->registerMetaTag(['property' => 'og:description', 'content' => $description], 'og:description');
        }

        $this->_controller->view->registerMetaTag(['property' => 'og:site_name', 'content' => $siteName], 'og:site_name');
        $this->_controller->view->registerMetaTag(['property' => 'og:url', 'content' => $url], 'og:url');
        $this->_controller->view->registerMetaTag(['property' => 'og:type', 'content' => $type], 'og:type');

        $this->_controller->view->registerMetaTag(['property' => 'og:locale', 'content' => $locale], 'og:locale');
//        $this->_controller->view->registerMetaTag(['property' => 'fb:app_id', 'content' => Yii::$app->params['fb_appid']], 'fb:app_id');

        if (!empty($image) && strpos($image, '/images/locale/' . Yii::$app->language . '/no-image.png') === false) {
            $plain = str_replace('https://', 'http://', $image);

            $this->_controller->view->registerMetaTag(['property' => 'og:image', 'content' => $plain], 'og:image');
            $this->_controller->view->registerMetaTag(['property' => 'og:image:secure_url', 'content' => $image], 'og:image:secure_url');
        } else {
            $this->_controller->view->registerMetaTag(['property' => 'og:image', 'content' => Url::to('/images/micro.jpg', true)], 'og:image');
        }

//        $this->_controller->view->registerLinkTag(['rel' => 'search', 'type' => 'application/opensearchdescription+xml', 'title' => 'Search uJobs', 'href' => '/search.xml']);
    }
}