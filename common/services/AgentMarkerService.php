<?php

namespace common\services;

use common\models\search\advanced\UserLightSearch;
use Yii;

/**
 * Class AgentMarkerService
 * @package common\services
 */
class AgentMarkerService
{
    /**
     * @param $params
     * @return array
     */
    public function getMarkers($params)
    {
        $params['zoom'] = $params['zoom'] ?? 11;
        /** @var UserLightSearch $markerSearch */
        $markerSearch = Yii::createObject(UserLightSearch::class);

        return $markerSearch->search($params);
    }
}