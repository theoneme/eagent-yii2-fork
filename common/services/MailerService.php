<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.11.2017
 * Time: 15:02
 */

namespace common\services;

use common\components\Mailer;
use common\interfaces\NotificationProviderInterface;
use Yii;
use yii\base\InvalidConfigException;
use yii\validators\EmailValidator;

/**
 * Class NewMailerService
 * @package common\services
 */
class MailerService implements NotificationProviderInterface
{
    /**
     * @param $params
     * @return bool
     * @throws InvalidConfigException
     */
    public function send($params)
    {
        if (!array_key_exists('to', $params)) {
            throw new InvalidConfigException('Missing receiver email in params');
        }

        if (!array_key_exists('emailSender', Yii::$app->params)) {
            throw new InvalidConfigException('Missing from email in params');
        }

        $emailValidator = new EmailValidator();
        if (!is_array($params['to'])) {
            $params['to'] = [$params['to']];
        }
        foreach ($params['to'] as $to) {
            if ($emailValidator->validate($to) !== true) {
                return false;
            }
        }

//        $unSubUrl = Url::to(['/site/unsubscribe', 'id' => $this->receiver->id, 'created_at' => $this->receiver->created_at, 'updated_at' => $this->receiver->updated_at], false);
        $view = $params['view'] ?? 'custom-message';

        /**
         * @var Mailer $mailer
         */
        $mailer = Yii::$app->mailer;
        if (!empty($params['viewPath'])) {
            $mailer->setViewPath($params['viewPath']);
        }
        if (!empty($params['viewParams'])) {
            $mailer->view->params = $params['viewParams'];
        }

        return $mailer->compose([
            'html' => $view
        ], $params)
            ->setTo($params['to'])
            ->setFrom(Yii::$app->params['emailSender'])
            ->setSubject($params['subject'])
//            ->addHeader('List-Unsubscribe', "<{$unSubUrl}>")
            ->send();
    }
}