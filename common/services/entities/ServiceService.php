<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\ServiceDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\ServiceRepositoryInterface;
use common\models\search\ServiceSearch;

/**
 * Class ServiceService
 * @package common\services\entities
 */
class ServiceService implements EntityServiceInterface
{
    /**
     * @var ServiceRepositoryInterface
     */
    private $_serviceRepository;

    /**
     * ServiceService constructor.
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->_serviceRepository = $serviceRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     */
    public function getOne($criteria)
    {
        $service = $this->_serviceRepository
            ->joinWith(['translations', 'attachments'])
            ->groupBy('service.id')
            ->findOneByCriteria($criteria);

        if ($service === null) {
            return null;
        }

        $serviceDTO = new ServiceDTO($service);
        return $serviceDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $serviceSearch = new ServiceSearch($this->_serviceRepository, $config);
        return $serviceSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_serviceRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_serviceRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}