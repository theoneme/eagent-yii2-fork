<?php

namespace common\services\entities;

use common\dto\PaymentDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\PaymentRepositoryInterface;
use common\models\search\PaymentSearch;

/**
 * Class PaymentService
 * @package common\services\entities
 */
class PaymentService implements EntityServiceInterface
{
    /**
     * @var PaymentRepositoryInterface
     */
    private $_paymentRepository;

    /**
     * PaymentService constructor.
     * @param PaymentRepositoryInterface $paymentRepository
     */
    public function __construct(PaymentRepositoryInterface $paymentRepository)
    {
        $this->_paymentRepository = $paymentRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $payment = $this->_paymentRepository
            ->joinWith(['paymentMethod'])
            ->groupBy('payment.id')
            ->findOneByCriteria($criteria);

        if ($payment === null) {
            return null;
        }

        $paymentDTO = new PaymentDTO($payment);
        return $paymentDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $paymentSearch = new PaymentSearch($this->_paymentRepository, $config);
        return $paymentSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_paymentRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_paymentRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_paymentRepository->updateOneByCriteria($criteria, $data);
    }
}