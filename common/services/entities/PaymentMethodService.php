<?php

namespace common\services\entities;

use common\dto\PaymentMethodDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\PaymentMethodRepositoryInterface;
use common\models\search\PaymentMethodSearch;

/**
 * Class PaymentMethodService
 * @package common\services\entities
 */
class PaymentMethodService implements EntityServiceInterface
{
    /**
     * @var PaymentMethodRepositoryInterface
     */
    private $_paymentMethodRepository;

    /**
     * UserWalletService constructor.
     * @param PaymentMethodRepositoryInterface $paymentMethodRepository
     */
    public function __construct(PaymentMethodRepositoryInterface $paymentMethodRepository)
    {
        $this->_paymentMethodRepository = $paymentMethodRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $paymentMethod = $this->_paymentMethodRepository
            ->groupBy('payment_method.id')
            ->findOneByCriteria($criteria);

        if ($paymentMethod === null) {
            return null;
        }

        $paymentMethodDTO = new PaymentMethodDTO($paymentMethod);
        return $paymentMethodDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $paymentMethodSearch = new PaymentMethodSearch($this->_paymentMethodRepository, $config);
        return $paymentMethodSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_paymentMethodRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_paymentMethodRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_paymentMethodRepository->updateOneByCriteria($criteria, $data);
    }
}