<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\RegionDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CountryRepositoryInterface;
use common\interfaces\repositories\RegionRepositoryInterface;
use common\models\search\RegionSearch;

/**
 * Class RegionService
 * @package common\services\entities
 */
class RegionService implements EntityServiceInterface
{
    /**
     * @var CountryRepositoryInterface
     */
    private $_regionRepository;

    /**
     * CountryService constructor.
     * @param RegionRepositoryInterface $regionRepository
     */
    public function __construct(RegionRepositoryInterface $regionRepository)
    {
        $this->_regionRepository = $regionRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $region = $this->_regionRepository
            ->with(['translations'])
            ->groupBy('region.id')
            ->findOneByCriteria($criteria);

        if ($region === null) {
            return null;
        }

        $regionDTO = new RegionDTO($region);
        return $regionDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $regionSearch = new RegionSearch($this->_regionRepository, $config);
        return $regionSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_regionRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_regionRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}