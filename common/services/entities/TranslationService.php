<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CityRepositoryInterface;
use common\interfaces\repositories\TranslationRepositoryInterface;

/**
 * Class TranslationService
 * @package common\services\entities
 */
class TranslationService implements EntityServiceInterface
{
    /**
     * @var CityRepositoryInterface
     */
    private $_translationRepository;

    /**
     * TranslationService constructor.
     * @param TranslationRepositoryInterface $translationRepository
     */
    public function __construct(TranslationRepositoryInterface $translationRepository)
    {
        $this->_translationRepository = $translationRepository;
    }

    /**
     * @param $criteria
     * @return void
     */
    public function getOne($criteria)
    {
        // TODO implement this shit
    }

    /**
     * @param $params
     * @param array $config
     * @return void
     */
    public function getMany($params, $config = [])
    {
//        // TODO implement this shit
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_translationRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_translationRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}