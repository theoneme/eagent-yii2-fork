<?php

namespace common\services\entities;

use common\dto\UserWalletDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\UserWalletRepositoryInterface;
use common\models\search\UserWalletSearch;

/**
 * Class UserWalletService
 * @package common\services\entities
 */
class UserWalletService implements EntityServiceInterface
{
    /**
     * @var UserWalletRepositoryInterface
     */
    private $_userWalletRepository;

    /**
     * UserWalletService constructor.
     * @param UserWalletRepositoryInterface $userWalletRepository
     */
    public function __construct(UserWalletRepositoryInterface $userWalletRepository)
    {
        $this->_userWalletRepository = $userWalletRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $userWallet = $this->_userWalletRepository
            ->joinWith(['histories'])
            ->groupBy('user_wallet.id')
            ->findOneByCriteria($criteria);

        if ($userWallet === null) {
            return null;
        }

        $userWalletDTO = new UserWalletDTO($userWallet);
        return $userWalletDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $userWalletSearch = new UserWalletSearch($this->_userWalletRepository, $config);
        return $userWalletSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_userWalletRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_userWalletRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_userWalletRepository->updateOneByCriteria($criteria, $data);
    }
}