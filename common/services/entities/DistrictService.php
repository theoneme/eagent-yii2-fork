<?php

namespace common\services\entities;

use common\dto\DistrictDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\DistrictRepositoryInterface;
use common\models\search\DistrictSearch;
use yii\db\Expression;

/**
 * Class DistrictService
 * @package common\services\entities
 */
class DistrictService implements EntityServiceInterface
{
    /**
     * @var DistrictRepositoryInterface
     */
    private $_districtRepository;

    /**
     * DistrictService constructor.
     * @param DistrictRepositoryInterface $districtRepository
     */
    public function __construct(DistrictRepositoryInterface $districtRepository)
    {
        $this->_districtRepository = $districtRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $district = $this->_districtRepository
            ->select(['id', 'city_id', 'slug', 'polygon' => new Expression('ST_AsGeoJSON(ST_SwapXY(polygon))')], true)
            ->with(['translations'])
            ->groupBy('district.id')
            ->findOneByCriteria($criteria);

        if ($district === null) {
            return null;
        }

        $districtDTO = new DistrictDTO($district);
        return $districtDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $districtSearch = new DistrictSearch($this->_districtRepository, $config);
        return $districtSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_districtRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_districtRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_districtRepository->updateOneByCriteria($criteria, $data);
    }
}