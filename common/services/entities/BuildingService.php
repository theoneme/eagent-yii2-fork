<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\BuildingDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\BuildingRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\BuildingSearch;

/**
 * Class BuildingService
 * @package common\services\entities
 */
class BuildingService implements EntityServiceInterface
{
    /**
     * @var BuildingRepositoryInterface
     */
    private $_buildingRepository;
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;

    /**
     * BuildingService constructor.
     * @param BuildingRepositoryInterface $buildingRepository
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     */
    public function __construct(
        BuildingRepositoryInterface $buildingRepository,
        AttributeService $attributeService,
        AttributeValueService $attributeValueService
    )
    {
        $this->_buildingRepository = $buildingRepository;
        $this->_attributeService = $attributeService;
        $this->_attributeValueService = $attributeValueService;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $building = $this->_buildingRepository
            ->with(['translations', 'attachments', 'buildingAttributes', 'addressTranslations', 'videos'])
            ->groupBy('building.id')
            ->findOneByCriteria($criteria);

        if ($building === null) {
            return null;
        }

        $attributes = $attributeValues = ['items' => []];

        if (!empty($building['buildingAttributes'])) {
            $attributeIDs = array_map(function ($attribute) {
                return $attribute['attribute_id'];
            }, $building['buildingAttributes']);

            $attributeValueIDs = array_map(function ($attribute) {
                return $attribute['value'];
            }, $building['buildingAttributes']);

            $attributes = $this->_attributeService->getMany(['id' => $attributeIDs], ['indexBy' => 'id']);
            $attributeValues = $this->_attributeValueService->getMany(['id' => $attributeValueIDs]);
        }

        $buildingDTO = new BuildingDTO($building, $attributes['items'], $attributeValues['items']);
        return $buildingDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getMany($params, $config = [])
    {
        $repository = $this->_buildingRepository;
        $repositoryClassName = get_class($repository);
        if (strpos($repositoryClassName, 'elastic')) {
            $search = new \common\models\search\elastic\BuildingSearch($repository, $config);
        } else {
            $search = new BuildingSearch($repository, $config);
        }

        return $search->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_buildingRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_buildingRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_buildingRepository = $repository;
    }

    /**
     *
     */
    public function getRepository()
    {
        return $this->_buildingRepository;
    }
}