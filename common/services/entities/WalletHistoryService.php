<?php

namespace common\services\entities;

use common\dto\WalletHistoryDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\WalletHistoryRepositoryInterface;
use common\models\search\WalletHistorySearch;

/**
 * Class WalletHistoryService
 * @package common\services\entities
 */
class WalletHistoryService implements EntityServiceInterface
{
    /**
     * @var WalletHistoryRepositoryInterface
     */
    private $_walletHistoryRepository;

    /**
     * WalletHistoryService constructor.
     * @param WalletHistoryRepositoryInterface $walletHistoryRepository
     */
    public function __construct(WalletHistoryRepositoryInterface $walletHistoryRepository)
    {
        $this->_walletHistoryRepository = $walletHistoryRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $walletHistory = $this->_walletHistoryRepository
            ->groupBy('wallet_history.id')
            ->findOneByCriteria($criteria);

        if ($walletHistory === null) {
            return null;
        }

        $walletHistoryDTO = new WalletHistoryDTO($walletHistory);
        return $walletHistoryDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $walletHistorySearch = new WalletHistorySearch($this->_walletHistoryRepository, $config);
        return $walletHistorySearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_walletHistoryRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_walletHistoryRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_walletHistoryRepository->updateOneByCriteria($criteria, $data);
    }
}