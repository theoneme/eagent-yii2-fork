<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\AttributeGroupDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\AttributeRepositoryInterface;
use common\repositories\sql\AttributeGroupRepository;

/**
 * Class AttributeGroupService
 * @package common\services\entities
 */
class AttributeGroupService implements EntityServiceInterface
{
    /**
     * @var AttributeRepositoryInterface
     */
    private $_attributeGroupRepository;

    /**
     * AttributeService constructor.
     * @param AttributeGroupRepository $attributeGroupRepository
     */
    public function __construct(AttributeGroupRepository $attributeGroupRepository)
    {
        $this->_attributeGroupRepository = $attributeGroupRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \common\exceptions\RepositoryException
     */
    public function getOne($criteria)
    {
        $attributeGroup = $this->_attributeGroupRepository
            ->joinWith(['attrs.translations'])
            ->groupBy('attribute_group.id')
            ->findOneByCriteria($criteria);

        if ($attributeGroup === null) {
            return null;
        }

        $attributeGroupDTO = new AttributeGroupDTO($attributeGroup);
        return $attributeGroupDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        return [];
    }

    /**
     * @param $data
     */
    public function create($data)
    {
        // TODO: Implement exists() method.
    }

    /**
     * @param $criteria
     */
    public function delete($criteria)
    {
        // TODO Implement this shit
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}