<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\PropertyDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\PropertySearch;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class PropertyService
 * @package common\services
 */
class PropertyService implements EntityServiceInterface
{
    /**
     * @var PropertyRepositoryInterface
     */
    private $_propertyRepository;
    /**
     * @var PropertyRepositoryInterface
     */
    private $_attributeService;
    /**
     * @var PropertyRepositoryInterface
     */
    private $_attributeValueService;

    /**
     * PropertyService constructor.
     * @param PropertyRepositoryInterface $propertyRepository
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     */
    public function __construct(
        PropertyRepositoryInterface $propertyRepository,
        AttributeService $attributeService,
        AttributeValueService $attributeValueService
    )
    {
        $this->_propertyRepository = $propertyRepository;
        $this->_attributeService = $attributeService;
        $this->_attributeValueService = $attributeValueService;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $property = $this->_propertyRepository
            ->joinWith(['translations'])
            ->with(['attachments', 'user.contacts', 'propertyAttributes', 'category.translations' => function (ActiveQuery $query) {
                return $query->andWhere(['category_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }, 'addressTranslations', 'user.company.translations', 'videos'])
            ->findOneByCriteria($criteria, true);

        if ($property === null) {
            return null;
        }

        $attributes = $attributeValues = ['items' => []];

        if (!empty($property['propertyAttributes'])) {
            $attributeIDs = array_map(function ($attribute) {
                return $attribute['attribute_id'];
            }, $property['propertyAttributes']);

            $attributeValueIDs = array_map(function ($attribute) {
                return $attribute['value'];
            }, $property['propertyAttributes']);

            $attributes = $this->_attributeService->getMany(['id' => $attributeIDs], ['indexBy' => 'id']);
            $attributeValues = $this->_attributeValueService->getMany(['id' => $attributeValueIDs]);
        }

        $propertyDTO = new PropertyDTO($property, $attributes['items'], $attributeValues['items']);
        return $propertyDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getMany($params, $config = [])
    {
        $propertyRepository = $this->_propertyRepository;
        $repositoryClassName = get_class($propertyRepository);
        if (strpos($repositoryClassName, 'elastic')) {
            $propertySearch = new \common\models\search\elastic\PropertySearch($propertyRepository, $config);
        } else {
            $propertySearch = new PropertySearch($propertyRepository, $config);
        }

        return $propertySearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_propertyRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_propertyRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_propertyRepository = $repository;
    }

    /**
     *
     */
    public function getRepository()
    {
        return $this->_propertyRepository;
    }
}