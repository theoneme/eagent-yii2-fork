<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\AttributeDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\AttributeRepositoryInterface;
use common\models\search\AttributeSearch;

/**
 * Class AttributeService
 * @package common\services
 */
class AttributeService implements EntityServiceInterface
{
    /**
     * @var AttributeRepositoryInterface
     */
    private $_attributeRepository;

    /**
     * AttributeService constructor.
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(AttributeRepositoryInterface $attributeRepository)
    {
        $this->_attributeRepository = $attributeRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $attribute = $this->_attributeRepository
            ->joinWith(['translations'])
            ->groupBy('attribute.id')
            ->findOneByCriteria($criteria);

        if ($attribute === null) {
            return null;
        }

        $attributeDTO = new AttributeDTO($attribute);
        return $attributeDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $attributeSearch = new AttributeSearch($this->_attributeRepository, $config);
        return $attributeSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_attributeRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_attributeRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_attributeRepository->updateOneByCriteria($criteria, $data);
    }
}