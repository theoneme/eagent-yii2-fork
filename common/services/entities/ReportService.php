<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\ReportDTO;
use common\interfaces\EntityServiceInterface;
use common\models\Report;
use common\repositories\sql\ReportRepository;

/**
 * Class ReportService
 * @package common\services\entities
 */
class ReportService implements EntityServiceInterface
{
    /**
     * @var ReportRepository
     */
    private $_reportRepository;

    /**
     * ReportService constructor.
     * @param ReportRepository $reportRepository
     */
    public function __construct(ReportRepository $reportRepository)
    {
        $this->_reportRepository = $reportRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \common\exceptions\RepositoryException
     */
    public function getOne($criteria)
    {
        /** @var Report $report */
        $report = $this->_reportRepository
            ->findOneByCriteria($criteria);

        if ($report === null) {
            return null;
        }

        $reportDTO = new ReportDTO($report);
        return $reportDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        //TODO IMPLEMENT
        return [];
    }

    /**
     * @param $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create($data)
    {
        return $this->_reportRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     * @throws \Throwable
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\StaleObjectException
     */
    public function delete($criteria)
    {
        return $this->_reportRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return mixed
     */
    public function update($data, $criteria)
    {
        return $this->_reportRepository->updateOneByCriteria($criteria, $data);
    }
}