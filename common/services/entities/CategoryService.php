<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\CategoryDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\search\CategorySearch;

/**
 * Class CategoryService
 * @package common\services\entities
 */
class CategoryService implements EntityServiceInterface
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;

    /**
     * CategoryService constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $category = $this->_categoryRepository
            ->joinWith(['translations'])
            ->groupBy('category.id')
            ->findOneByCriteria($criteria);

        if ($category === null) {
            return null;
        }

        $categoryDTO = new CategoryDTO($category);
        return $categoryDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $categorySearch = new CategorySearch($this->_categoryRepository, $config);
        return $categorySearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_categoryRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_categoryRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}