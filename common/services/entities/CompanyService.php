<?php

namespace common\services\entities;

use common\dto\CompanyDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CompanyRepositoryInterface;
use common\models\search\CompanySearch;

/**
 * Class CompanyService
 * @package common\services\entities
 */
class CompanyService implements EntityServiceInterface
{
    /**
     * @var CompanyRepositoryInterface
     */
    private $_companyRepository;

    /**
     * CompanyService constructor.
     * @param CompanyRepositoryInterface $repository
     */
    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->_companyRepository = $repository;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $model = $this->_companyRepository
            ->joinWith(['translations'])
            ->groupBy('company.id')
            ->findOneByCriteria($criteria);

        if ($model === null) {
            return null;
        }

        $dto = new CompanyDTO($model);
        return $dto->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $companySearch = new CompanySearch($this->_companyRepository, $config);
        return $companySearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_companyRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_companyRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_companyRepository->updateOneByCriteria($criteria, $data);
    }
}