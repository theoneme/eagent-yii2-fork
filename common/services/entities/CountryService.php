<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\CountryDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CountryRepositoryInterface;
use common\models\search\CountrySearch;

/**
 * Class CountryService
 * @package common\services\entities
 */
class CountryService implements EntityServiceInterface
{
    /**
     * @var CountryRepositoryInterface
     */
    private $_countryRepository;

    /**
     * CountryService constructor.
     * @param CountryRepositoryInterface $countryRepository
     */
    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->_countryRepository = $countryRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $country = $this->_countryRepository
            ->with(['translations'])
            ->groupBy('country.id')
            ->findOneByCriteria($criteria);

        if ($country === null) {
            return null;
        }

        $countryDTO = new CountryDTO($country);
        return $countryDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $countrySearch = new CountrySearch($this->_countryRepository, $config);
        return $countrySearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_countryRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_countryRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}