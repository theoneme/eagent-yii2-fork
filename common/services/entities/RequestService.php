<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\RequestDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\interfaces\repositories\RequestRepositoryInterface;
use common\models\search\RequestSearch;

/**
 * Class RequestService
 * @package common\services\entities
 */
class RequestService implements EntityServiceInterface
{
    /**
     * @var RequestRepositoryInterface
     */
    private $_requestRepository;
    /**
     * @var PropertyRepositoryInterface
     */
    private $_attributeService;
    /**
     * @var PropertyRepositoryInterface
     */
    private $_attributeValueService;

    /**
     * RequestService constructor.
     * @param RequestRepositoryInterface $requestRepository
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     */
    public function __construct(RequestRepositoryInterface $requestRepository, AttributeService $attributeService, AttributeValueService $attributeValueService)
    {
        $this->_requestRepository = $requestRepository;
        $this->_attributeService = $attributeService;
        $this->_attributeValueService = $attributeValueService;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     */
    public function getOne($criteria)
    {
        $request = $this->_requestRepository
            ->with(['translations', 'attachments', 'user', 'category.translations', 'addressTranslations', 'requestAttributes'])
            ->groupBy('request.id')
            ->findOneByCriteria($criteria);

        if ($request === null) {
            return null;
        }

        $attributes = $attributeValues = ['items' => []];

        if (!empty($request['requestAttributes'])) {
            $attributeIDs = array_map(function ($attribute) {
                return $attribute['attribute_id'];
            }, $request['requestAttributes']);

            $attributeValueIDs = array_filter(array_map(function ($attribute) {
                return !array_key_exists('min', $attribute['customDataArray']) ? $attribute['customDataArray']['value'] : null;
            }, $request['requestAttributes']));

            $attributes = $this->_attributeService->getMany(['id' => $attributeIDs], ['indexBy' => 'id']);
            if(!empty($attributeValueIDs)) {
                $attributeValues = $this->_attributeValueService->getMany(['id' => $attributeValueIDs]);
            }
        }

        $requestDTO = new RequestDTO($request, $attributes['items'], $attributeValues['items']);
        return $requestDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $requestSearch = new RequestSearch($this->_requestRepository, $config);
        return $requestSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_requestRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_requestRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}