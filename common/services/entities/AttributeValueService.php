<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\AttributeValueDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\AttributeValueRepositoryInterface;
use common\models\search\AttributeValueSearch;

/**
 * Class AttributeValueService
 * @package common\services
 */
class AttributeValueService implements EntityServiceInterface
{
    /**
     * @var AttributeValueRepositoryInterface
     */
    private $_attributeValueRepository;

    /**
     * AttributeValueService constructor.
     * @param AttributeValueRepositoryInterface $attributeValueRepository
     */
    public function __construct(AttributeValueRepositoryInterface $attributeValueRepository)
    {
        $this->_attributeValueRepository = $attributeValueRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $attributeValue = $this->_attributeValueRepository
            ->joinWith(['translations'])
            ->groupBy('attribute_value.id')
            ->findOneByCriteria($criteria);

        if ($attributeValue === null) {
            return null;
        }

        $attributeValueDTO = new AttributeValueDTO($attributeValue);
        return $attributeValueDTO->getData();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_attributeValueRepository->create($data);
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $attributeValueSearch = new AttributeValueSearch($this->_attributeValueRepository, $config);
        return $attributeValueSearch->search($params);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_attributeValueRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_attributeValueRepository->updateOneByCriteria($criteria, $data);
    }
}