<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\PageDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\PageRepositoryInterface;
use common\models\search\PageSearch;

/**
 * Class PageService
 * @package common\services\entities
 */
class PageService implements EntityServiceInterface
{
    /**
     * @var PageRepositoryInterface
     */
    private $_pageRepository;

    /**
     * PageService constructor.
     * @param PageRepositoryInterface $pageRepository
     */
    public function __construct(PageRepositoryInterface $pageRepository)
    {
        $this->_pageRepository = $pageRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $page = $this->_pageRepository
            ->joinWith(['translations'])
            ->with(['user'])
            ->groupBy('page.id')
            ->findOneByCriteria($criteria, true);

        if ($page === null) {
            return null;
        }

        $pageDTO = new PageDTO($page);
        return $pageDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getMany($params, $config = [])
    {
        $pageSearch = new PageSearch($this->_pageRepository, $config);
        return $pageSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_pageRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_pageRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}