<?php

namespace common\services\entities;

use common\dto\MicroDistrictDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\MicroDistrictRepositoryInterface;
use common\models\search\MicroDistrictSearch;
use yii\db\Expression;

/**
 * Class MicroDistrictService
 * @package common\services\entities
 */
class MicroDistrictService implements EntityServiceInterface
{
    /**
     * @var MicroDistrictRepositoryInterface
     */
    private $_microDistrictRepository;

    /**
     * MicroDistrictService constructor.
     * @param MicroDistrictRepositoryInterface $microDistrictRepository
     */
    public function __construct(MicroDistrictRepositoryInterface $microDistrictRepository)
    {
        $this->_microDistrictRepository = $microDistrictRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $microDistrict = $this->_microDistrictRepository
            ->select(['id', 'city_id', 'district_id', 'slug', 'polygon' => new Expression('ST_AsGeoJSON(ST_SwapXY(polygon))')], true)
            ->with(['translations'])
            ->groupBy('micro_district.id')
            ->findOneByCriteria($criteria);

        if ($microDistrict === null) {
            return null;
        }

        $microDistrictDTO = new MicroDistrictDTO($microDistrict);
        return $microDistrictDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $microDistrictSearch = new MicroDistrictSearch($this->_microDistrictRepository, $config);
        return $microDistrictSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_microDistrictRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_microDistrictRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_microDistrictRepository->updateOneByCriteria($criteria, $data);
    }
}