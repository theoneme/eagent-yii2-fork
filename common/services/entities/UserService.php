<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\advanced\UserAdvancedDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\UserRepositoryInterface;
use common\models\search\UserSearch;

/**
 * Class UserService
 * @package common\services\entities
 */
class UserService implements EntityServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private $_userRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->_userRepository = $userRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $user = $this->_userRepository
            ->findOneByCriteria($criteria);

        if ($user === null) {
            return null;
        }

        $userDTO = new UserAdvancedDTO($user);
        return $userDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     * @throws \ReflectionException
     */
    public function getMany($params, $config = [])
    {
        $search = new UserSearch($this->_userRepository, $config);
        return $search->search($params);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_userRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_userRepository->create($data);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_userRepository->updateOneByCriteria($criteria, $data);
    }
}