<?php

namespace common\services\entities;

use common\dto\CompanyMemberDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CompanyMemberRepositoryInterface;
use common\models\search\CompanyMemberSearch;

/**
 * Class CompanyMemberService
 * @package common\services\entities
 */
class CompanyMemberService implements EntityServiceInterface
{
    /**
     * @var CompanyMemberRepositoryInterface
     */
    private $companyMemberRepository;

    /**
     * CompanyMemberService constructor.
     * @param CompanyMemberRepositoryInterface $repository
     */
    public function __construct(CompanyMemberRepositoryInterface $repository)
    {
        $this->companyMemberRepository = $repository;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $model = $this->companyMemberRepository
            ->groupBy('company_member.id')
            ->findOneByCriteria($criteria);

        if ($model === null) {
            return null;
        }

        $dto = new CompanyMemberDTO($model);
        return $dto->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $companyMemberSearch = new CompanyMemberSearch($this->companyMemberRepository, $config);
        return $companyMemberSearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->companyMemberRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->companyMemberRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->companyMemberRepository->updateOneByCriteria($criteria, $data);
    }
}