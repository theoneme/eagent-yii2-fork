<?php

namespace common\services\entities;

use common\dto\AddressTranslationDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\AddressTranslationRepositoryInterface;

/**
 * Class AddressTranslationService
 * @package common\services
 */
class AddressTranslationService implements EntityServiceInterface
{
    /**
     * @var AddressTranslationRepositoryInterface
     */
    private $_addressTranslationRepository;

    /**
     * AddressTranslationService constructor.
     * @param AddressTranslationRepositoryInterface $addressTranslationRepository
     */
    public function __construct(AddressTranslationRepositoryInterface $addressTranslationRepository)
    {
        $this->_addressTranslationRepository = $addressTranslationRepository;
    }

    /**
     * @param $criteria
     * @return array
     */
    public function getOne($criteria)
    {
        $addressTranslation = $this->_addressTranslationRepository->findOneByCriteria($criteria);

        if ($addressTranslation === null) {
            return null;
        }

        $addressTranslationDTO = new AddressTranslationDTO($addressTranslation);
        return $addressTranslationDTO->getData();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_addressTranslationRepository->create($data);
    }

    /**
     * @param $params
     * @param array $config
     * @return void
     */
    public function getMany($params, $config = [])
    {
        // TODO Implement this shit
    }

    /**
     * @param $criteria
     */
    public function delete($criteria)
    {
        // TODO Implement this shit
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }
}