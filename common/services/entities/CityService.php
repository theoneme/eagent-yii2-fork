<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace common\services\entities;

use common\dto\CityDTO;
use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\CityRepositoryInterface;
use common\models\search\CitySearch;

/**
 * Class CityService
 * @package common\services\entities
 */
class CityService implements EntityServiceInterface
{
    /**
     * @var CityRepositoryInterface
     */
    private $_cityRepository;

    /**
     * CityService constructor.
     * @param CityRepositoryInterface $cityRepository
     */
    public function __construct(CityRepositoryInterface $cityRepository)
    {
        $this->_cityRepository = $cityRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     */
    public function getOne($criteria)
    {
        $city = $this->_cityRepository
            ->with(['translations'])
            ->groupBy('city.id')
            ->findOneByCriteria($criteria);

        if ($city === null) {
            return null;
        }

        $cityDTO = new CityDTO($city);
        return $cityDTO->getData();
    }

    /**
     * @param $params
     * @param $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $citySearch = new CitySearch($this->_cityRepository, $config);
        return $citySearch->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_cityRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_cityRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     * @return bool
     */
    public function update($data, $criteria)
    {
        return $this->_cityRepository->updateOneByCriteria($criteria, $data);
    }
}