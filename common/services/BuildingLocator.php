<?php

namespace common\services;

use common\models\Building;
use common\repositories\sql\AddressTranslationRepository;
use common\repositories\sql\BuildingRepository;

/**
 * Class AddressTranslationCreator
 * @package common\services
 */
class BuildingLocator
{
    /**
     * @var AddressTranslationRepository
     */
    private $_buildingRepository;

    /**
     * BuildingLocator constructor.
     * @param BuildingRepository $buildingRepository
     */
    public function __construct(BuildingRepository $buildingRepository)
    {
        $this->_buildingRepository = $buildingRepository;
    }

    /**
     * @param $data
     * @return array|Building|null|\yii\db\ActiveRecord
     * @throws \common\exceptions\RepositoryException
     */
    public function findOrCreate($data)
    {
        if (!empty($data['lat']) && !empty($data['lng'])) {
            $lat = round($data['lat'], 8);
            $lng = round($data['lng'], 8);
            $building = $this->_buildingRepository->findOneByCriteria(['lat' => $lat, 'lng' => $lng]);

            if ($building instanceof Building) {
                return $building;
            }
        }

        return new Building();
    }
}