<?php

namespace common\services;

use common\interfaces\repositories\PropertyRepositoryInterface;
use common\models\search\advanced\CityLightSearch;
use common\models\search\advanced\PropertyCitySearch;
use common\models\search\advanced\PropertyLightSearch;
use common\models\search\advanced\PropertyRegionSearch;
use common\models\search\advanced\RegionLightSearch;
use Yii;

/**
 * Class PropertyMarkerService
 * @package common\services
 */
class PropertyMarkerService
{
    /**
     * @var PropertyRepositoryInterface
     */
    private $_propertyRepository;

    /**
     * MarkerService constructor.
     * @param PropertyRepositoryInterface $propertyRepository
     */
    public function __construct(PropertyRepositoryInterface $propertyRepository)
    {
        $this->_propertyRepository = $propertyRepository;
    }

    /**
     * @param $params
     * @return array
     */
    public function getMarkers($params)
    {
        $params['zoom'] = $params['zoom'] ?? 11;
        $params['ads_allowed'] = true;
        if ($params['zoom'] > 10) {
            /** @var PropertyLightSearch $markerSearch */
            $markerSearch = Yii::createObject(PropertyLightSearch::class);
//            $markerSearch = new PropertyLightSearch($this->_propertyRepository);
            $markers = $markerSearch->search($params);
        } else
            if ($params['zoom'] > 6) {
                /** @var PropertyCitySearch $propertySearch */
                $propertySearch = new PropertyCitySearch($this->_propertyRepository, ['indexBy' => 'city_id']);
                $propertyCounts = $propertySearch->search($params);
                $params['id'] = array_keys($propertyCounts['items']);

                /** @var CityLightSearch $markerSearch */
                $markerSearch = Yii::$container->get(CityLightSearch::class);
                $markers = $markerSearch->search($params);

                if (count($markers['items'])) {
                    $markers['items'] = array_reduce($markers['items'], function ($carry, $value) use ($propertyCounts) {
                        if (isset($propertyCounts['items'][$value['id']])) {
                            $value['content'] = Yii::t('catalog', '{count, plural, one{# object} other{# objects}}', [
                                'count' => $propertyCounts['items'][$value['id']]
                            ]);
                            $value['type'] = 'city';
                            $value['count'] = $propertyCounts['items'][$value['id']];
                            $carry[] = $value;
                        }
                        return $carry;
                    }, []);
                }
            } else {
                /** @var PropertyRegionSearch $propertySearch */
                $propertySearch = new PropertyRegionSearch($this->_propertyRepository, ['indexBy' => 'region_id']);
                $propertyCounts = $propertySearch->search($params);

                $regionParams['id'] = array_map(function ($value) {
                    return $value['region_id'];
                }, $propertyCounts['items']);

                /** @var RegionLightSearch $markerSearch */
                $markerSearch = Yii::$container->get(RegionLightSearch::class);
                $markers = $markerSearch->search($regionParams);

                if (count($propertyCounts['items'])) {
                    $markers['items'] = array_reduce($markers['items'], function ($carry, $value) use ($propertyCounts) {
                        if (isset($propertyCounts['items'][$value['id']])) {
                            $value['content'] = Yii::t('catalog', '{count, plural, one{# object} other{# objects}}', [
                                'count' => $propertyCounts['items'][$value['id']]['propertyCount']
                            ]);
                            $value['type'] = 'city';
                            $value['count'] = $propertyCounts['items'][$value['id']]['propertyCount'];
                            $value['lat'] = $propertyCounts['items'][$value['id']]['avgLat'];
                            $value['lng'] = $propertyCounts['items'][$value['id']]['avgLng'];
                            $carry[] = $value;
                        }
                        return $carry;
                    }, []);
                }
            }

        return $markers;
    }
}