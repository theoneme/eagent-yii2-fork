<?php

namespace common\services;

use common\forms\ar\BuildingForm;
use common\forms\ar\PropertyForm;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\services\entities\BuildingService;

/**
 * Class BuildingForPropertyService
 * @package common\services
 */
class BuildingForPropertyService
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var BuildingLocator
     */
    private $_buildingLocator;

    /**
     * BuildingForPropertyService constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     * @param BuildingService $buildingService
     * @param BuildingLocator $buildingLocator
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository, BuildingService $buildingService, BuildingLocator $buildingLocator)
    {
        $this->_categoryRepository = $categoryRepository;
        $this->_buildingService = $buildingService;
        $this->_buildingLocator = $buildingLocator;
    }

    /**
     * @param $categoryId
     * @return bool
     */
    public function hasToManipulateBuilding($categoryId)
    {
        $category = $this->_categoryRepository->findOneByCriteria(['id' => $categoryId]);

        if ($category !== null && array_key_exists('createBuilding', $category['customDataArray'])) {
            return (boolean)$category['customDataArray']['createBuilding'];
        }

        return false;
    }

    /**
     * @param BuildingForm $buildingForm
     * @param PropertyForm $propertyForm
     * @return mixed
     */
    public function initForm($buildingForm, $propertyForm)
    {
        $building = $this->_buildingLocator->findOrCreate($propertyForm->geo->attributes);
        $buildingForm->_building = $building;
        if ($building->isNewRecord === false) {
            $buildingDTO = $this->_buildingService->getOne(['id' => $building['id']]);
            $buildingForm->prepareUpdate($buildingDTO);
        }

        return $buildingForm;
    }
}