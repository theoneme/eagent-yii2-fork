<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 09.11.2017
 * Time: 14:05
 */

namespace common\services;

use Yii;

/**
 * Class NotificationService
 * @package common\services
 */
class NotificationService
{
    /**
     * @param $provider
     * @param $params
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function sendNotification($provider, $params)
    {
        switch ($provider) {
            case 'email':
                /** @var MailerService $mailerService */
                $mailerService = Yii::$container->get(MailerService::class);

                return $mailerService->send($params);
        }

        return false;
    }
}