<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\elastic;

use common\interfaces\repositories\BuildingRepositoryInterface;

/**
 * Class BuildingRepository
 * @package common\repositories\elastic
 */
class BuildingRepository extends AbstractElasticArRepository implements BuildingRepositoryInterface
{

}