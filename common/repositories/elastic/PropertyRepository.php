<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\elastic;

use common\interfaces\repositories\PropertyRepositoryInterface;

/**
 * Class PropertyRepository
 * @package common\repositories\elastic
 */
class PropertyRepository extends AbstractElasticArRepository implements PropertyRepositoryInterface
{

}