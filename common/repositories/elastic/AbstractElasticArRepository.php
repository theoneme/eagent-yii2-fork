<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 14:00
 */

namespace common\repositories\elastic;

use common\components\elasticsearch\ActiveQuery;
use common\exceptions\EntityNotFoundException;
use common\exceptions\RepositoryException;
use common\interfaces\RepositoryInterface;
use Yii;
use yii\elasticsearch\ActiveRecord;
use yii\elasticsearch\Connection;

/**
 * Class AbstractElasticArRepository
 * @package common\repositories\sql
 */
class AbstractElasticArRepository implements RepositoryInterface
{
    public const PRIMARY_KEY = 'id';
    public const APPLICATION_KEY = 'id';

    /**
     * @var Connection
     */
    protected $connection;
    /**
     * @var string
     */
    public $modelClass;
    /**
     * @var ActiveRecord
     */
    protected $model;
    /**
     * @var ActiveQuery
     */
    protected $query;
    /**
     * @var array $data
     * query parameters (sort, filters, pagination)
     */
    protected $data;
    /**
     * @var array
     */
    protected $select = ['*'];
    /**
     * @var array|string
     */
    protected $orderBy = [];
    /**
     * @var int
     */
    protected $limit;
    /**
     * @var array
     */
    protected $having;
    /**
     * @var int
     */
    protected $offset;
    /**
     * @var string
     */
    protected $indexBy;

    /**
     * AbstractElasticArRepository constructor.
     * @param ActiveRecord $model
     */
    public function __construct(ActiveRecord $model)
    {
        $this->modelClass = get_class($model);
        $this->model = $model;
        $this->initRepositoryParams();
    }

    /**
     * @throws RepositoryException
     */
    public function init()
    {
        if ($this->model) {
            return;
        }

        if (empty($this->modelClass)) {
            throw new RepositoryException('what the f ...');
        }

        $this->model = new $this->modelClass;
        $this->select();
        $this->orderBy([]);
        $this->connection = Yii::$app->db;
        $this->query = $this->model::find();
    }

    /**
     * @return $this
     * @throws \common\exceptions\RepositoryException
     */
    protected function initRepositoryParams()
    {
        $this->select();
        $this->orderBy([]);
        $this->connection = Yii::$app->elasticsearch;
        $this->query = $this->model::find();
        $this->limit = null;
        $this->offset = null;
        $this->indexBy = null;

        return $this;
    }

    /**
     * @param array $with
     * @return $this
     * @throws RepositoryException
     */
    public function with(array $with = [])
    {
        return $this;
    }

    /**
     * @param array $joinWith
     * @param bool $eagerLoading
     * @param string $joinType
     * @return $this
     * @throws RepositoryException
     */
    public function joinWith(array $joinWith = [], $eagerLoading = true, $joinType = 'LEFT JOIN')
    {
        return $this;
    }

    /**
     * @param array $select
     * @param bool $rewrite
     * @return $this
     */
    public function select(array $select = ['*'], $rewrite = false)
    {
        if ($rewrite === true) {
            $this->query->source($select);
        }

        return $this;
    }

    /**
     * @param int $offset
     * @return $this
     * @throws RepositoryException
     */
    public function offset($offset = 0)
    {
        if (!is_numeric($offset) || $offset < 0) {
            throw new RepositoryException('Param "offset" smells bad.');
        }
        $this->offset = $offset;

        return $this;
    }

    /**
     * @param $indexBy
     * @return $this
     * @throws RepositoryException
     */
    public function indexBy($indexBy)
    {
        if ($indexBy === null) {
            throw new RepositoryException('Param "indexBy" smells bad.');
        }
        $this->indexBy = $indexBy;

        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     * @throws RepositoryException
     */
    public function limit($limit = 10)
    {
        if (!is_numeric($limit)) {
            throw new RepositoryException('Param "limit" smells bad.');
        }
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param $having
     * @return $this
     * @throws RepositoryException
     */
    public function having($having)
    {
        return $this;
    }

    /**
     * @param $orderBy
     * @return $this
     */
    public function orderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * @param $groupBy
     * @return $this
     */
    public function groupBy($groupBy)
    {
        return $this;
    }

    /**
     * @param $aggregations
     * @return $this
     */
    public function aggregate($aggregations)
    {
        $this->query->aggregations($aggregations);

        return $this;
    }

    /**
     * @param $relation
     * @param $relationKey
     * @param array $condition
     * @return $this
     * @throws RepositoryException
     */
    public function relationExists($relation, $relationKey, $condition = [])
    {
        $this->query->andWhere($condition);

        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $this->model->setAttributes($data);
        $this->model->save();

        return $this->model;
    }

    /**
     * @param array $criteria
     * @param bool $returnArray
     * @return array|null|ActiveRecord
     * @throws RepositoryException
     */
    public function findOneByCriteria(array $criteria = [], $returnArray = false)
    {
        $this->initFetch($returnArray, $this->select);
        $this->query = $this->query->where($criteria)->orderBy($this->orderBy);
        $result = $this->query->one();
        $this->initRepositoryParams();

        return $result;
    }

    /**
     * @param array $criteria
     * @return bool
     */
    public function existsByCriteria(array $criteria = [])
    {
        $this->query = $this->query->where($criteria);
        return $this->query->exists();
    }

    /**
     * @param $returnArray
     * @param $select
     */
    protected function initFetch($returnArray, $select)
    {
        if ($select !== ['*']) {
            $this->query->source($select);
        }

        if ($returnArray) {
            $this->query->asArray();
        }
    }

    /**
     * @param array $criteria
     * @param bool $returnArray
     * @return array|ActiveRecord[]
     * @throws RepositoryException
     */
    public function findManyByCriteria(array $criteria = [], $returnArray = false)
    {
        $this->initFetch($returnArray, $this->select);
        $this->query = $this->query->andWhere($criteria)->orderBy($this->orderBy);

        if ($this->limit !== null) {
            $this->query->limit = $this->limit;
        }

        if ($this->offset !== null) {
            $this->query->offset = $this->offset;
        }

        if ($this->indexBy !== null) {
            $this->query->indexBy = $this->indexBy;
        }

        $result = $this->query->search();
        $this->initRepositoryParams();

        return $result;
    }

    /**
     * @param array $criteria
     * @param string $column
     * @return array
     */
    public function findColumnByCriteria(array $criteria = [], $column = 'id')
    {
        $this->query->source([$column])->asArray();
        $this->query = $this->query->andWhere($criteria)->orderBy($this->orderBy);

        if ($this->limit !== null) {
            $this->query->limit = $this->limit;
        }

        if ($this->offset !== null) {
            $this->query->offset = $this->offset;
        }

        if ($this->indexBy !== null) {
            $this->query->indexBy = $this->indexBy;
        }

        if ($this->having !== null) {
            $this->query->having = $this->having;
        }

        $result = $this->query->column($column);
        $this->initRepositoryParams();

        return $result;
    }

    /**
     * @param array $criteria
     * @param array $with
     * @return int|string
     */
    public function countByCriteria(array $criteria = [], array $with = [])
    {
        $oldQuery = clone $this->query;

        $this->initFetch(true, $this->select);
        $this->query = $this->query->andWhere($criteria);

        if ($this->limit !== null) {
            $this->query->limit = $this->limit;
        }

        if ($this->offset !== null) {
            $this->query->offset = $this->offset;
        }

        if ($this->indexBy !== null) {
            $this->query->indexBy = $this->indexBy;
        }

        $result = $this->query->count();
        $this->query = $oldQuery;

        return $result;
    }

    /**
     * @param $id
     * @param $field
     * @param int $count
     * @return bool
     */
    public function inc($id, $field, $count = 1)
    {
        $entity = $this->query->where([self::PRIMARY_KEY => $id])->one();
        return $entity->updateCounters([$field => $count]);
    }

    /**
     * @param $id
     * @param $field
     * @param int $count
     * @return bool
     */
    public function dec($id, $field, $count = -1)
    {
        $entity = $this->query->where([self::PRIMARY_KEY => $id])->one();
        return $entity->updateCounters([$field => $count]);
    }

    /**
     * @param ActiveRecord $entity
     * @param array $data
     * @return ActiveRecord
     */
    protected function updateEntity($entity, array $data)
    {
        $entity->updateAttributes($data);
//        $entity->save();

        return $entity;
    }

    /**
     * @param array $criteria
     * @param array $data
     * @return mixed
     */
    public function updateOneByCriteria(array $criteria = [], array $data = [])
    {
        $entity = $this->query->where($criteria)->one();

        return $this->updateEntity($entity, $data);
    }

    /**
     * @param array $criteria
     * @param array $data
     * @return int number of records updated
     */
    public function updateManyByCriteria(array $criteria = [], array $data = [])
    {
        return $this->model::updateAll($data, $criteria);
    }

    /**
     * @param array $criteria
     * @return bool
     * @throws RepositoryException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteOneByCriteria(array $criteria = [])
    {
        $entity = $this->model::findOne($criteria);

        if ($entity !== null) {
            if ($entity->delete()) {
                return true;
            }

            throw new RepositoryException("Entity {$this->model::type()} cannot be removed");
        }

        throw new EntityNotFoundException("Entity {$this->model::type()} with criteria was not found");
    }

    /**
     * @param array $criteria
     * @return boolean|integer number of rows deleted
     */
    public function deleteManyByCriteria(array $criteria = [])
    {
        return $this->model::deleteAll($criteria);
    }
}