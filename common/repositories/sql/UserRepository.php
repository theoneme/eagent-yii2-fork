<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\dto\UserDTO;
use common\forms\ar\UserForm;
use common\interfaces\repositories\UserRepositoryInterface;
use common\models\User;
use Yii;

/**
 * Class PropertyRepository
 * @package common\repositories\sql
 */
class UserRepository extends AbstractSqlArRepository implements UserRepositoryInterface
{
    public const MODE_DEFAULT = 0;
    public const MODE_LIGHT = 1;

    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     */
    public function create(array $data)
    {
        if (Yii::$app instanceof \yii\console\Application) {
            $form = new \console\forms\ar\UserForm();
        } else {
            $form = new UserForm();
        }
        $form->_user = new User();
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* User has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
            Yii::error($form->_user->errors);
        }

        return $form->_user->id;
    }

    /**
     * @param array $criteria
     * @param array $data
     * @param int $mode
     * @return bool|mixed
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function updateOneByCriteria(array $criteria = [], array $data = [], $mode = self::MODE_DEFAULT)
    {
        /** @var User $user */
        $user = $this->findOneByCriteria($criteria);

        if ($user === null) {
            return false;
        }

        if ($mode === self::MODE_DEFAULT) {
            $form = new UserForm();

            $form->_user = $user;
            $userDTO = new UserDTO($user);
            $userData = $userDTO->getData();
            $form->prepareUpdate($userData);

            $form->load($data);
            $form->bindData();

            if (!$form->save()) {
                Yii::error('******* User has failed to update: ' . PHP_EOL);
                Yii::error($form->_user->errors);

                return false;
            }
        }

        if ($mode === self::MODE_LIGHT) {
            $result = $user->updateAttributes($data);

            if (!$result) {
                Yii::error('******* User has failed to update: ' . PHP_EOL);

                return false;
            }
        }

        return true;
    }
}