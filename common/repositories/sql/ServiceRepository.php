<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\ServiceForm;
use common\interfaces\repositories\ServiceRepositoryInterface;
use Yii;

/**
 * Class ServiceRepository
 * @package common\repositories\sql
 */
class ServiceRepository extends AbstractSqlArRepository implements ServiceRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $serviceForm = new ServiceForm();
        $serviceForm->load($data);

        if (array_key_exists('id', $data)) {
            $serviceForm->id = $data['id'];
        }

        if (!$serviceForm->save()) {
            Yii::error('******* Service has failed to save: ' . PHP_EOL);
            Yii::error($serviceForm->errors);
        }
        return $serviceForm->id;
    }
}