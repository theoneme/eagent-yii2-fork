<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\ReportForm;
use common\interfaces\repositories\ReportRepositoryInterface;
use Yii;

/**
 * Class ReportRepository
 * @package common\repositories\sql
 */
class ReportRepository extends AbstractSqlArRepository implements ReportRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $reportForm = new ReportForm();
        $reportForm->load($data);

        $result = $reportForm->save();
        if (!$result) {
            Yii::error('******* Report has failed to save: ' . PHP_EOL);
            Yii::error($reportForm->errors);
        }
        return $reportForm->id;
    }
}