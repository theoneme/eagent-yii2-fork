<?php

namespace common\repositories\sql;

use common\dto\CompanyMemberDTO;
use common\forms\ar\CompanyMemberForm;
use common\interfaces\repositories\CompanyMemberRepositoryInterface;
use common\models\CompanyMember;
use Yii;

/**
 * Class CompanyMemberRepository
 * @package common\repositories\sql
 */
class CompanyMemberRepository extends AbstractSqlArRepository implements CompanyMemberRepositoryInterface
{
    public const MODE_DEFAULT = 0;
    public const MODE_LIGHT = 1;

    /**
     * @param array $data
     * @return mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new CompanyMemberForm(['_companyMember' => new CompanyMember()]);
        $form->load($data);

        if (!$form->save()) {
            Yii::error('******* Company member has failed to save: ' . PHP_EOL);
            Yii::error($form->_companyMember->errors);
        }

        return $form->_companyMember->id;
    }

    /**
     * @param array $criteria
     * @param array $data
     * @param int $mode
     * @return bool|mixed
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function updateOneByCriteria(array $criteria = [], array $data = [], $mode = self::MODE_DEFAULT)
    {
        /** @var CompanyMember $model */
        $model = $this->findOneByCriteria($criteria);

        if ($model === null) {
            return false;
        }

        if ($mode === self::MODE_DEFAULT) {
            $form = new CompanyMemberForm(['_companyMember' => $model]);

            $dto = new CompanyMemberDTO($model);
            $form->prepareUpdate($dto->getData());

            $form->load($data);

            if (!$form->save()) {
                Yii::error('******* Company member has failed to update: ' . PHP_EOL);
                Yii::error($form->_companyMember->errors);

                return false;
            }
        }

        if ($mode === self::MODE_LIGHT) {
            $result = $model->updateAttributes($data);

            if (!$result) {
                Yii::error('******* Company member has failed to update: ' . PHP_EOL);

                return false;
            }
        }

        return true;
    }
}