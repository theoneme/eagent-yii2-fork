<?php

namespace common\repositories\sql;

use common\forms\ar\PaymentMethodForm;
use common\forms\ar\UserWalletForm;
use common\interfaces\repositories\PaymentMethodRepositoryInterface;
use Yii;

/**
 * Class PaymentMethodRepository
 * @package common\repositories\sql
 */
class PaymentMethodRepository extends AbstractSqlArRepository implements PaymentMethodRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new PaymentMethodForm();
        $form->load($data);

        if (!$form->save()) {
            Yii::error('******* Payment method has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->id;
    }
}