<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\BuildingForm;
use common\forms\ar\PropertyForm;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\models\Building;
use common\models\Property;
use common\services\BuildingForPropertyService;
use Yii;

/**
 * Class PropertyRepository
 * @package common\repositories\sql
 */
class PropertyRepository extends AbstractSqlArRepository implements PropertyRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {
        /** @var BuildingForPropertyService $buildingForPropertyService */
        $buildingForPropertyService = Yii::$container->get(BuildingForPropertyService::class);

        $propertyForm = new PropertyForm(['_property' => new Property()]);
        $propertyForm->load($data);
        $hasToManipulateBuilding = $buildingForPropertyService->hasToManipulateBuilding($propertyForm->category->category_id);

        if ($hasToManipulateBuilding === true) {
            $buildingForm = new BuildingForm(['_building' => new Building()]);
            $buildingForm = $buildingForPropertyService->initForm($buildingForm, $propertyForm);
            if ($buildingForm->_building->isNewRecord === true) {
                $buildingForm->type = Building::TYPE_DEFAULT;
                $buildingForm->locale = $propertyForm->locale;
                $buildingForm->load($data);
                $buildingForm->bindData();
                if ($buildingForm->save()) {
                    $propertyForm->_property->building_id = $buildingForm->_building->id;
                }
            } else {
                $propertyForm->_property->building_id = $buildingForm->_building->id;
            }
        }

        $propertyForm->bindData();
        if (!$propertyForm->save()) {
            Yii::error('******* Property has failed to save: ' . PHP_EOL);
            Yii::error($propertyForm->_property->errors);
        }

        return $propertyForm->_property->id;
    }
}