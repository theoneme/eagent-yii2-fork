<?php

namespace common\repositories\sql;

use common\forms\ar\WalletHistoryForm;
use common\interfaces\repositories\UserWalletRepositoryInterface;
use Yii;

/**
 * Class WalletHistoryRepository
 * @package common\repositories\sql
 */
class WalletHistoryRepository extends AbstractSqlArRepository implements UserWalletRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new WalletHistoryForm();
        $form->load($data);

        if (!$form->save()) {
            Yii::error('******* History has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->id;
    }
}