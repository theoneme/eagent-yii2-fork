<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\interfaces\repositories\AttributeFilterRepositoryInterface;

/**
 * Class AttributeFilterRepository
 * @package common\repositories\sql
 */
class AttributeFilterRepository extends AbstractSqlArRepository implements AttributeFilterRepositoryInterface
{

}