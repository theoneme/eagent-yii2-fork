<?php

namespace common\repositories\sql;

use common\forms\ar\MicroDistrictForm;
use common\interfaces\repositories\MicroDistrictRepositoryInterface;
use common\models\MicroDistrict;
use Yii;

/**
 * Class MicroDistrictRepository
 * @package common\repositories\sql
 */
class MicroDistrictRepository extends AbstractSqlArRepository implements MicroDistrictRepositoryInterface
{
    /**
     * @param array $data
     * @return mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new MicroDistrictForm(['_microDistrict' => new MicroDistrict()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Micro District has failed to save: ' . PHP_EOL);
            Yii::error($form->_microDistrict->errors);
        }

        return $form->_microDistrict->id;
    }
}