<?php

namespace common\repositories\sql;

use common\forms\ar\DistrictForm;
use common\interfaces\repositories\DistrictRepositoryInterface;
use common\models\District;
use Yii;

/**
 * Class DistrictRepository
 * @package common\repositories\sql
 */
class DistrictRepository extends AbstractSqlArRepository implements DistrictRepositoryInterface
{
    /**
     * @param array $data
     * @return mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new DistrictForm(['_district' => new District()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* District has failed to save: ' . PHP_EOL);
            Yii::error($form->_district->errors);
        }

        return $form->_district->id;
    }
}