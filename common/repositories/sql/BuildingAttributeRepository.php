<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\interfaces\repositories\BuildingAttributeRepositoryInterface;

/**
 * Class BuildingAttributeRepository
 * @package common\repositories\sql
 */
class BuildingAttributeRepository extends AbstractSqlArRepository implements BuildingAttributeRepositoryInterface
{

}