<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\interfaces\repositories\PropertyAttributeRepositoryInterface;

/**
 * Class PropertyAttributeRepository
 * @package common\repositories\sql
 */
class PropertyAttributeRepository extends AbstractSqlArRepository implements PropertyAttributeRepositoryInterface
{

}