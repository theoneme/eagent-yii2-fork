<?php

namespace common\repositories\sql;

use common\dto\CompanyDTO;
use common\forms\ar\CompanyForm;
use common\interfaces\repositories\CompanyRepositoryInterface;
use common\models\Company;
use Yii;

/**
 * Class CompanyRepository
 * @package common\repositories\sql
 */
class CompanyRepository extends AbstractSqlArRepository implements CompanyRepositoryInterface
{
    public const MODE_DEFAULT = 0;
    public const MODE_LIGHT = 1;

    /**
     * @param array $data
     * @return mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new CompanyForm(['_company' => new Company()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Company has failed to save: ' . PHP_EOL);
            Yii::error($form->_company->errors);
        }

        return $form->_company->id;
    }

    /**
     * @param array $criteria
     * @param array $data
     * @param int $mode
     * @return bool|mixed
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function updateOneByCriteria(array $criteria = [], array $data = [], $mode = self::MODE_DEFAULT)
    {
        /** @var Company $model */
        $model = $this->findOneByCriteria($criteria);

        if($model === null) {
            return false;
        }

        if($mode === self::MODE_DEFAULT) {
            $form = new CompanyForm();

            $form->_company = $model;
            $dto = new CompanyDTO($model);
            $form->prepareUpdate($dto->getData());

            $form->load($data);
            $form->bindData();

            if (!$form->save()) {
                Yii::error('******* Company has failed to update: ' . PHP_EOL);
                Yii::error($form->_company->errors);

                return false;
            }
        }

        if($mode === self::MODE_LIGHT) {
            $result = $model->updateAttributes($data);

            if(!$result) {
                Yii::error('******* Company has failed to update: ' . PHP_EOL);

                return false;
            }
        }

        return true;
    }
}