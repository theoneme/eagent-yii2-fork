<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\BuildingForm;
use common\interfaces\repositories\BuildingRepositoryInterface;
use common\models\Building;
use Yii;

/**
 * Class BuildingRepository
 * @package common\repositories\sql
 */
class BuildingRepository extends AbstractSqlArRepository implements BuildingRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\web\NotFoundHttpException
     */
    public function create(array $data)
    {
        $form = new BuildingForm(['_building' => new Building()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Building has failed to save: ' . PHP_EOL);
            Yii::error($form->_building->errors);
        }

        return $form->_building->id;
    }
}