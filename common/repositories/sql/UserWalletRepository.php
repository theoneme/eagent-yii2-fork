<?php

namespace common\repositories\sql;

use common\forms\ar\UserWalletForm;
use common\interfaces\repositories\UserWalletRepositoryInterface;
use Yii;

/**
 * Class UserWalletRepository
 * @package common\repositories\sql
 */
class UserWalletRepository extends AbstractSqlArRepository implements UserWalletRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new UserWalletForm();
        $form->load($data);

        if (!$form->save()) {
            Yii::error('******* Wallet has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->id;
    }
}