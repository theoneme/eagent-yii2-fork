<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\PageForm;
use common\interfaces\repositories\PageRepositoryInterface;
use common\models\Page;
use Yii;

/**
 * Class PageRepository
 * @package common\repositories\sql
 */
class PageRepository extends AbstractSqlArRepository implements PageRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {
        $form = new PageForm(['_page' => new Page()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Page has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->_page->id;
    }
}