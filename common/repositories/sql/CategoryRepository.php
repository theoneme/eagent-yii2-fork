<?php
/**
 * Created by PhpStorm.
 * Category: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\dto\CategoryDTO;
use common\forms\ar\CategoryForm;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Category;
use Yii;

/**
 * Class CategoryRepository
 * @package common\repositories\sql
 */
class CategoryRepository extends AbstractSqlArRepository implements CategoryRepositoryInterface
{
    public const MODE_DEFAULT = 0;
    public const MODE_LIGHT = 1;

    /**
     * @param array $data
     * @return int|mixed
     */
    public function create(array $data)
    {
        //TODO implement this method
        return 0;
    }

    /**
     * @param array $criteria
     * @param array $data
     * @param int $mode
     * @return bool|mixed
     */
    public function updateOneByCriteria(array $criteria = [], array $data = [], $mode = self::MODE_DEFAULT)
    {
        /** @var Category $category */
        $category = $this->findOneByCriteria($criteria);

        if ($category === null) {
            return false;
        }

        if ($mode === self::MODE_DEFAULT) {
            $form = new CategoryForm();

            $form->_category = $category;
            $categoryDTO = new CategoryDTO($category);
            $categoryData = $categoryDTO->getData();
            $form->prepareUpdate($categoryData);

            $form->load($data);
            $form->bindData();

            if (!$form->save()) {
                Yii::error('******* Category has failed to update: ' . PHP_EOL);
                Yii::error($form->_category->errors);

                return false;
            }
        }

        if ($mode === self::MODE_LIGHT) {
            $result = $category->updateAttributes($data);

            if (!$result) {
                Yii::error('******* Category has failed to update: ' . PHP_EOL);

                return false;
            }
        }

        return true;
    }
}