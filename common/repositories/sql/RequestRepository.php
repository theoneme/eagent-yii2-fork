<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\RequestForm;
use common\interfaces\repositories\RequestRepositoryInterface;
use common\models\Request;
use Yii;

/**
 * Class RequestRepository
 * @package common\repositories\sql
 */
class RequestRepository extends AbstractSqlArRepository implements RequestRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function create(array $data)
    {
        if(Yii::$app instanceof \yii\console\Application) {
            $form = new \console\forms\ar\RequestForm();
        } else {
            $form = new RequestForm();
        }

        $form->_request = new Request();
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Request has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
            Yii::error($form->_request->errors);
        }

        return $form->_request->id;
    }
}