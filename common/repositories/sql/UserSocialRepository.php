<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\interfaces\repositories\UserSocialRepositoryInterface;

/**
 * Class UserSocialRepository
 * @package common\repositories\sql
 */
class UserSocialRepository extends AbstractSqlArRepository implements UserSocialRepositoryInterface
{

}