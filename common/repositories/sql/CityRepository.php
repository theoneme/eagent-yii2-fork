<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\dto\CityDTO;
use common\forms\ar\CityForm;
use common\interfaces\repositories\CityRepositoryInterface;
use common\models\City;
use Yii;

/**
 * Class CityRepository
 * @package common\repositories\sql
 */
class CityRepository extends AbstractSqlArRepository implements CityRepositoryInterface
{
    public const MODE_DEFAULT = 0;
    public const MODE_LIGHT = 1;

    /**
     * @param array $data
     * @return mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new CityForm(['_city' => new City()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* City has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->_city->id;
    }

    /**
     * @param array $criteria
     * @param array $data
     * @param int $mode
     * @return bool|mixed
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function updateOneByCriteria(array $criteria = [], array $data = [], $mode = self::MODE_DEFAULT)
    {
        /** @var City $model */
        $model = $this->findOneByCriteria($criteria);

        if($model === null) {
            return false;
        }

        if($mode === self::MODE_DEFAULT) {
            $form = new CityForm();

            $form->_city = $model;
            $dto = new CityDTO($model);
            $cityData = $dto->getData();
            $form->prepareUpdate($cityData);

            $form->load($data);
            $form->bindData();

            if (!$form->save()) {
                Yii::error('******* City has failed to update: ' . PHP_EOL);
                Yii::error($form->errors);

                return false;
            }
        }

        if($mode === 1) {
            $result = $model->updateAttributes($data);

            if(!$result) {
                Yii::error('******* City has failed to update: ' . PHP_EOL);

                return false;
            }
        }

        return true;
    }
}