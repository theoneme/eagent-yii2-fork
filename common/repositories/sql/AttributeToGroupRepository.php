<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\interfaces\repositories\AttributeToGroupRepositoryInterface;

/**
 * Class AttributeToGroupRepository
 * @package common\repositories\sql
 */
class AttributeToGroupRepository extends AbstractSqlArRepository implements AttributeToGroupRepositoryInterface
{

}