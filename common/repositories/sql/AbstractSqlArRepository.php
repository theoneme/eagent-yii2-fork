<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 14:00
 */

namespace common\repositories\sql;

use common\exceptions\EntityNotFoundException;
use common\exceptions\RepositoryException;
use common\interfaces\RepositoryInterface;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;

/**
 * Class AbstractSqlArRepository
 * @package common\repositories\sql
 */
class AbstractSqlArRepository implements RepositoryInterface
{
    public const PRIMARY_KEY = 'id';
    public const APPLICATION_KEY = 'id';

    /**
     * @var Connection
     */
    protected $connection;
    /**
     * @var string
     */
    public $modelClass;
    /**
     * @var ActiveRecord
     */
    protected $model;
    /**
     * @var ActiveQuery
     */
    protected $query;
    /**
     * @var array $data
     * query parameters (sort, filters, pagination)
     */
    protected $data;
    /**
     * @var array
     */
    protected $with = [];
    /**
     * @var array
     */
    protected $joinWith = [];
    /**
     * @var array
     */
    protected $select = ['*'];
    /**
     * @var array|string
     */
    protected $orderBy = [];
    /**
     * @var int
     */
    protected $limit;
    /**
     * @var array
     */
    protected $having;
    /**
     * @var int
     */
    protected $offset;
    /**
     * @var string
     */
    protected $indexBy;
    /**
     * @var int
     */
    protected $groupBy = [];

    /**
     * AbstractSqlArRepository constructor.
     * @param ActiveRecord $model
     * @throws \common\exceptions\RepositoryException
     */
    public function __construct(ActiveRecord $model)
    {
        $this->modelClass = get_class($model);
        $this->model = $model;
        $this->initRepositoryParams();
    }

    /**
     * @throws RepositoryException
     */
    public function init()
    {
        if ($this->model) {
            return;
        }

        if (empty($this->modelClass)) {
            throw new RepositoryException('what the f ...');
        }

        $this->model = new $this->modelClass;
        $this->select();
        $this->orderBy([]);
        $this->connection = Yii::$app->db;
        $this->query = $this->model::find();
    }

    /**
     * @return $this
     * @throws \common\exceptions\RepositoryException
     */
    protected function initRepositoryParams()
    {
        $this->select(['*'], true);
        $this->orderBy([]);
        $this->connection = Yii::$app->db;
        $this->query = $this->model::find();
        $this->with = [];
        $this->joinWith = [];
        $this->limit = null;
        $this->offset = null;
        $this->having = null;
        $this->indexBy = null;

        return $this;
    }

    /**
     * @param array $with
     * @return $this
     * @throws RepositoryException
     */
    public function with(array $with = [])
    {
        if (is_array($with) === false) {
            throw new RepositoryException('Param "with" smells bad.');
        }

        $this->with[] = [$with];

        return $this;
    }

    /**
     * @param array $joinWith
     * @param bool $eagerLoading
     * @param string $joinType
     * @return $this
     * @throws RepositoryException
     */
    public function joinWith(array $joinWith = [], $eagerLoading = true, $joinType = 'LEFT JOIN')
    {
        if (is_array($joinWith) === false) {
            throw new RepositoryException('Param "joinWith" smells bad.');
        }

        $this->joinWith[] = [$joinWith, $eagerLoading, $joinType];

        return $this;
    }

    /**
     * @param array $select
     * @param bool $rewrite
     * @return $this
     * @throws RepositoryException
     */
    public function select(array $select = ['*'], $rewrite = false)
    {
        if (is_array($select) === false) {
            throw new RepositoryException('Param "select" smells bad.');
        }

        if ($select === ['*']) {
            $select = ["{$this->model::tableName()}.*"];
        }

        if ($this->select !== $select && $this->select !== ['*']) {
            if ($rewrite === false) {
                $this->select = array_merge($this->select, $select);
            } else {
                $this->select = $select;
            }
        } else {
            $this->select = $select;
        }

        return $this;
    }

    /**
     * @param int $offset
     * @return $this
     * @throws RepositoryException
     */
    public function offset($offset = 0)
    {
        if (!is_numeric($offset) || $offset < 0) {
            throw new RepositoryException('Param "offset" smells bad.');
        }
        $this->offset = $offset;

        return $this;
    }

    /**
     * @param $indexBy
     * @return $this
     * @throws RepositoryException
     */
    public function indexBy($indexBy)
    {
        if ($indexBy === null) {
            throw new RepositoryException('Param "indexBy" smells bad.');
        }
        $this->indexBy = $indexBy;

        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     * @throws RepositoryException
     */
    public function limit($limit = 10)
    {
        if (!is_numeric($limit) || $limit < 1) {
            throw new RepositoryException('Param "limit" smells bad.');
        }
        $this->limit = $limit;

        return $this;
    }

    /**
     * @param $having
     * @return $this
     * @throws RepositoryException
     */
    public function having($having)
    {
        if (is_string($having) && strlen($having) < 2) {
            throw new RepositoryException('Param "having" smells bad.');
        }
        if ($this->having === null) {
            $this->having = $having;
        } else {
            $this->having = ['and', $this->having, $having];
        }

        return $this;
    }

    /**
     * @param array $orderBy
     * @return $this
     */
    public function orderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    /**
     * @param $groupBy
     * @return $this
     */
    public function groupBy($groupBy)
    {
        $this->groupBy = $groupBy;

        return $this;
    }

    /**
     * @param $aggregations
     * @return $this
     */
    public function aggregate($aggregations)
    {
        return $this;
    }

    /**
     * @param $relation
     * @param $relationKey
     * @param array $condition
     * @return $this
     * @throws RepositoryException
     */
    public function relationExists($relation, $relationKey, $condition = [])
    {
        if (!$relation || !$relationKey || !is_array($condition)) {
            throw new RepositoryException('Something wrong with params for relation checking');
        }
        $propertyName = $this->model::tableName();
        $this->query->andFilterWhere(['exists', (new Query)
            ->select("{$relation}.id")
            ->from($relation)
            ->where("{$relation}.{$relationKey} = {$propertyName}.id")
            ->andWhere($condition)
        ]);

        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $this->model->setAttributes($data);
        $this->model->save();

        return $this->model;
    }

    /**
     * @param array $criteria
     * @param bool $returnArray
     * @return array|null|ActiveRecord
     * @throws RepositoryException
     */
    public function findOneByCriteria(array $criteria = [], $returnArray = false)
    {
        foreach ($this->joinWith as $relation) {
            $this->query = $this->query->joinWith($relation[0], $relation[1], $relation[2]);
        }
        foreach ($this->with as $relation) {
            $this->query = $this->query->with($relation[0]);
        }
//        $this->query->with = $this->with;
        $this->initFetch($returnArray, $this->select);
        $this->query = $this->query->where($criteria);

        if (!empty($this->groupBy)) {
            $this->query->groupBy($this->groupBy);
        }
        if (!empty($this->orderBy)) {
            $this->query->orderBy($this->orderBy);
        }

        $result = $this->query->limit(1)->one();
        $this->initRepositoryParams();

        return $result;
    }

    /**
     * @param array $criteria
     * @return bool
     */
    public function existsByCriteria(array $criteria = [])
    {
        foreach ($this->joinWith as $relation) {
            $this->query = $this->query->joinWith($relation[0], $relation[1], $relation[2]);
        }

        $this->query = $this->query->andWhere($criteria);
        $result = $this->query->exists();
        $this->initRepositoryParams();

        return $result;
    }

    /**
     * @param $returnArray
     * @param $select
     */
    protected function initFetch($returnArray, $select)
    {
        if ($select !== ['*']) {
            $this->query->select($select);
        }

        if ($returnArray) {
            $this->query->asArray();
        }
    }

    /**
     * @param array $criteria
     * @param bool $returnArray
     * @return array|ActiveRecord[]
     * @throws RepositoryException
     */
    public function findManyByCriteria(array $criteria = [], $returnArray = false)
    {
        foreach ($this->joinWith as $relation) {
            $this->query = $this->query->joinWith($relation[0], $relation[1], $relation[2]);
        }
        foreach ($this->with as $relation) {
            $this->query = $this->query->with($relation[0]);
        }

//        $this->query->with = $this->with;

        $this->initFetch($returnArray, $this->select);
        $this->query = $this->query->andWhere($criteria)->orderBy($this->orderBy)->groupBy($this->groupBy);

        if ($this->limit !== null) {
            $this->query->limit = $this->limit;
        }

        if ($this->offset !== null) {
            $this->query->offset = $this->offset;
        }

        if ($this->indexBy !== null) {
            $this->query->indexBy = $this->indexBy;
        }

        if ($this->having !== null) {
            $this->query->having = $this->having;
        }

        $result = $this->query->all();
        $this->initRepositoryParams();

        return $result;
    }

    /**
     * @param array $criteria
     * @param string $column
     * @return array
     */
    public function findColumnByCriteria(array $criteria = [], $column = 'id')
    {
        foreach ($this->joinWith as $relation) {
            $this->query = $this->query->joinWith($relation[0], $relation[1], $relation[2]);
        }
        foreach ($this->with as $relation) {
            $this->query = $this->query->with($relation[0]);
        }
//        $this->query->with = $this->with;

        $this->query->select($column)->asArray();
        $this->query = $this->query->andWhere($criteria);

        if (!empty($this->groupBy)) {
            $this->query->groupBy($this->groupBy);
        }

        if (!empty($this->orderBy)) {
            $this->query->orderBy($this->orderBy);
        }

        if ($this->limit !== null) {
            $this->query->limit = $this->limit;
        }

        if ($this->offset !== null) {
            $this->query->offset = $this->offset;
        }

        if ($this->indexBy !== null) {
            $this->query->indexBy = $this->indexBy;
        }

        if ($this->having !== null) {
            $this->query->having = $this->having;
        }

        $result = $this->query->column();
        $this->initRepositoryParams();

        return $result;
    }

    /**
     * @param array $criteria
     * @param array $with
     * @param bool $reset
     * @return int|string
     */
    public function countByCriteria(array $criteria = [], array $with = [], $reset = false)
    {
        $oldQuery = clone $this->query;

        foreach ($this->joinWith as $relation) {
            if (!empty($with)) {
                $relation[0] = array_intersect($relation[0], $with);
            }

            $this->query = $this->query->joinWith($relation[0], $relation[1], $relation[2]);
        }

        $this->initFetch(true, $this->select);
        $this->query = $this->query->andWhere($criteria);

        if (!empty($this->groupBy)) {
            $this->query->groupBy($this->groupBy);
        }
        if (!empty($this->orderBy)) {
            $this->query->orderBy($this->orderBy);
        }

        if ($this->limit !== null) {
            $this->query->limit = $this->limit;
        }

        if ($this->offset !== null) {
            $this->query->offset = $this->offset;
        }

        if ($this->indexBy !== null) {
            $this->query->indexBy = $this->indexBy;
        }

        if ($this->having !== null) {
            $this->query->having = $this->having;
        }

        $result = $this->query->count();
        if ($reset === false) {
            $this->query = $oldQuery;
        } else {
            $this->initRepositoryParams();
        }

        return $result;
    }

    /**
     * @param $id
     * @param $field
     * @param int $count
     * @return bool
     */
    public function inc($id, $field, $count = 1)
    {
        $entity = $this->query->where([self::PRIMARY_KEY => $id])->one();
        return $entity->updateCounters([$field => $count]);
    }

    /**
     * @param $id
     * @param $field
     * @param int $count
     * @return bool
     */
    public function dec($id, $field, $count = -1)
    {
        $entity = $this->query->where([self::PRIMARY_KEY => $id])->one();
        return $entity->updateCounters([$field => $count]);
    }

    /**
     * @param ActiveRecord $entity
     * @param array $data
     * @return ActiveRecord
     */
    protected function updateEntity(ActiveRecord $entity, array $data)
    {
        $entity->updateAttributes($data);
//        $entity->save();

        return $entity;
    }

    /**
     * @param array $criteria
     * @param array $data
     * @return mixed
     */
    public function updateOneByCriteria(array $criteria = [], array $data = [])
    {
        $entity = $this->query->where($criteria)->one();

        return $this->updateEntity($entity, $data);
    }

    /**
     * @param array $criteria
     * @param array $data
     * @return int number of records updated
     */
    public function updateManyByCriteria(array $criteria = [], array $data = [])
    {
        return $this->model::updateAll($data, $criteria);
    }

    /**
     * @param array $criteria
     * @return bool
     * @throws RepositoryException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteOneByCriteria(array $criteria = [])
    {
        $entity = $this->model::findOne($criteria);

        if ($entity !== null) {
            if ($entity->delete()) {
                return true;
            }

            throw new RepositoryException("Entity {$this->model::tableName()} cannot be removed");
        }

        throw new EntityNotFoundException("Entity {$this->model::tableName()} with criteria was not found");
    }

    /**
     * @param array $criteria
     * @return boolean|integer number of rows deleted
     */
    public function deleteManyByCriteria(array $criteria = [])
    {
        return $this->model::deleteAll($criteria);
    }
}