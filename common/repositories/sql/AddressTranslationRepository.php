<?php

namespace common\repositories\sql;

use common\interfaces\repositories\AddressTranslationRepositoryInterface;

/**
 * Class AddressTranslationRepository
 * @package common\repositories\sql
 */
class AddressTranslationRepository extends AbstractSqlArRepository implements AddressTranslationRepositoryInterface
{

}