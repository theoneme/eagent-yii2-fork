<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\TranslationForm;
use common\interfaces\repositories\TranslationRepositoryInterface;

/**
 * Class CityRepository
 * @package common\repositories\sql
 */
class TranslationRepository extends AbstractSqlArRepository implements TranslationRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $translationForm = new TranslationForm();
        $translationForm->load($data);

        $translationForm->save();
        return $translationForm->id;
    }
}