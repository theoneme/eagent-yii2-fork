<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\RegionForm;
use common\interfaces\repositories\RegionRepositoryInterface;
use common\models\Region;
use Yii;

/**
 * Class RegionRepository
 * @package common\repositories\sql
 */
class RegionRepository extends AbstractSqlArRepository implements RegionRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new RegionForm(['_region' => new Region()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Region has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->_region->id;
    }
}