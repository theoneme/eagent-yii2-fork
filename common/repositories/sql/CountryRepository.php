<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\forms\ar\CountryForm;
use common\interfaces\repositories\CountryRepositoryInterface;
use common\models\Country;
use Yii;

/**
 * Class CountryRepository
 * @package common\repositories\sql
 */
class CountryRepository extends AbstractSqlArRepository implements CountryRepositoryInterface
{
    /**
     * @param array $data
     * @return int|mixed
     * @throws \ReflectionException
     */
    public function create(array $data)
    {
        $form = new CountryForm(['_country' => new Country()]);
        $form->load($data);
        $form->bindData();

        if (!$form->save()) {
            Yii::error('******* Country has failed to save: ' . PHP_EOL);
            Yii::error($form->errors);
        }

        return $form->_country->id;
    }
}