<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace common\repositories\sql;

use common\interfaces\repositories\AttributeGroupRepositoryInterface;

/**
 * Class AttributeGroupRepository
 * @package common\repositories\sql
 */
class AttributeGroupRepository extends AbstractSqlArRepository implements AttributeGroupRepositoryInterface
{

}