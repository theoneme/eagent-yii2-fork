<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 10.11.2016
 * Time: 16:43
 */

namespace common\components;

use common\helpers\UtilityHelper;
use frostealth\yii2\aws\s3\Service;
use Imagine\Image\Box;
use Yii;
use yii\base\Component;
use yii\imagine\Image;

/**
 * Class MediaLayer
 * @package common\components
 */
class MediaLayer extends Component
{
    /**
     * @var Service
     */
    private $_s3;

    /**
     * MediaLayer constructor.
     * @param array $config
     * @param Service $s3
     */
    public function __construct(array $config = [], Service $s3)
    {
        parent::__construct($config);
        $this->_s3 = $s3;
    }

    /**
     * @param $media
     * @param int $width
     * @param int $height
     * @return string
     */
    public function tryLoadFromAws($media, $width = null, $height = null)
    {
        if (preg_match('/^http(s)?:\/\/.*/', $media)) {
            return $media;
        }

        $cachePath = null;
        if (is_int($width) && is_int($height)) {
            $cachePath = "/cache/{$width}x{$height}";
        }

        $mode = Yii::$app->params['awsSource'];
        switch ($mode) {
            case 'cloudfront':
                $cloudfrontId = Yii::$app->params['awsCloudfrontId'];
                $completeUrl = "https://{$cloudfrontId}.cloudfront.net{$cachePath}{$media}";
                break;
            case 's3':
            default:
                $awsBucketName = $this->_s3->defaultBucket;
                $awsRegion = Yii::$app->params['awsRegion'];
                $completeUrl = "https://s3.{$awsRegion}.amazonaws.com/{$awsBucketName}{$cachePath}{$media}";
        }

        return $completeUrl;
    }

    /**
     * @param $media
     * @param bool $makeThumbnails
     * @param bool $cropThumbnails
     * @return bool
     * @throws \yii\base\InvalidArgumentException
     * @throws \LogicException
     */
    public function saveToAws($media, $makeThumbnails = true, $cropThumbnails = true)
    {
        if ($media !== null && file_exists(Yii::getAlias('@frontend') . '/web' . $media)) {
            if ($makeThumbnails) {
                $this->makeThumbnails($media, $cropThumbnails);
            }

//            $this->applyWatermark(Yii::getAlias("@frontend") . "/web" . $media);

            // Отключаем AWS
//            $promise = $this->_s3->commands()
//                ->upload($this->fixMediaPath($media), Yii::getAlias('@frontend') . '/web' . $media)
//                ->withParam('CacheControl', 'max-age=640000')
//                ->async()
//                ->execute();
//            $promise->wait();
        }

        return true;
    }

    /**
     * @param string $media
     * @return bool
     */
    public function removeMedia($media)
    {
        if ($media !== null) {
            if (file_exists(Yii::getAlias('@frontend') . '/web' . $media)) {
                unlink(Yii::getAlias('@frontend') . '/web' . $media);
            }

            // Отключаем AWS
//            $fixedMediaPath = $this->fixMediaPath($media);
//            if ($this->_s3->exist($fixedMediaPath)) {
//                $this->_s3->delete($fixedMediaPath);
//            }

            $sizes = Yii::$app->params['images'];
            if (!empty($sizes)) {
                foreach ($sizes as $key => $size) {
                    $cachePath = "cache/{$size['width']}x{$size['height']}";

                    if (file_exists(Yii::getAlias('@frontend') . '/web/' . $cachePath . $media)) {
                        unlink(Yii::getAlias('@frontend') . '/web/' . $cachePath . $media);
                    }

                    // Отключаем AWS
//                    if ($this->_s3->exist("{$cachePath}{$media}")) {
//                        $this->_s3->delete("{$cachePath}{$media}");
//                    }
                }
            }
        }

        return true;
    }

    /**
     * @param $media
     * @param bool $crop
     * @return bool
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\base\InvalidParamException
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     * @throws \LogicException
     */
    public function makeThumbnails($media, $crop = true)
    {
        if ($media !== null && in_array(pathinfo($media, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif', 'png'])) {
            $pathinfo = pathinfo($media);
            $cachePath = '/cache/%size%' . $pathinfo['dirname'];

            foreach (Yii::$app->params['images'] as $key => $value) {
                $path = str_replace('%size%', "{$value['width']}x{$value['height']}", $cachePath);

                UtilityHelper::makeDir(Yii::getAlias('@frontend') . '/web' . $path);

                $originPath = Yii::getAlias('@frontend') . '/web' . $media;
                $newPath = Yii::getAlias('@frontend') . '/web' . $path . '/' . $pathinfo['basename'];

                $size = getimagesize($originPath);
                if ($crop) {
                    $imageRatio = $size[0] / $size[1];
                    $ourRatio = $value['width'] / $value['height'];
                    if ($imageRatio > $ourRatio) {
                        $width = $size[1] * $ourRatio;
                        $height = $size[1];
                    } else {
                        $width = $size[0];
                        $height = $size[0] / $ourRatio;
                    }

                    $width = max($width, 1);
                    $height = max($height, 1);
                    $x = floor(($size[0] - $width) / 2);
                    $y = floor(($size[1] - $height) / 2);

                    Image::crop($originPath, $width, $height, [$x, $y])->resize(new Box($value['width'], $value['height']))->save($newPath, ['quality' => 80]);
//                    Image::thumbnail($newPath, $value['width'], $value['height'])->save($newPath, ['quality' => 80]);
                } else {
                    Image::thumbnail($originPath, $value['width'], null)->save($newPath, ['quality' => 80]);
                }
//                $this->applyWatermark($newPath);

                // Отключаем AWS
//                $promise = $this->_s3
//                    ->commands()
//                    ->upload($this->fixMediaPath($path) . '/' . $pathinfo['basename'], $newPath)
//                    ->withParam('CacheControl', 'max-age=640000')
//                    ->async()
//                    ->execute();
//                $promise->wait();
            }

            return true;
        }

        return false;
    }

    /**
     * @param $link
     * @param null $target
     * @param bool $fromAws
     * @return string
     */
    public function getThumb($link, $target = null, $fromAws = true)
    {
        if (!in_array(pathinfo($link, PATHINFO_EXTENSION), ['jpg', 'jpeg', 'gif', 'png', 'bmp'])) {
            return '/images/attachment-icon.png';
        }
        $image = '/images/locale/' . Yii::$app->language . '/no-image.png';
        if (!empty($link) && file_exists(Yii::getAlias("@frontend") . "/web" . $link)) {
            $image = $link;
        }

        $fromAws = false; // Отключаем AWS
        if ($fromAws === false) {
            return $image;
        }

        $width = $height = null;

        if ($target !== null && isset(Yii::$app->params['images'][$target])) {
            $width = Yii::$app->params['images'][$target]['width'];
            $height = Yii::$app->params['images'][$target]['height'];
        }

        return $this->tryLoadFromAws($image, $width, $height);
    }

    /**
     * @param $path
     * @return null|string
     */
    public function fixMediaPath($path)
    {
        return strlen($path) > 0 ? $fixedPath = ltrim($path, '/') : null;
    }
}