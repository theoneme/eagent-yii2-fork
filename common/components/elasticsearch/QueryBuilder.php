<?php

namespace common\components\elasticsearch;

use frontend\mappers\map\GooglePolygonMapper;
use ReflectionClass;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\elasticsearch\Query;
use yii\elasticsearch\QueryBuilder as BaseQueryBuilder;
use yii\helpers\ArrayHelper;

/**
 * Class QueryBuilder
 * @package common\components\elasticsearch
 */
class QueryBuilder extends BaseQueryBuilder
{
    /**
     * @var array
     */
    private $_relationDefinitions = [];

    /**
     * @param Query $query
     * @return array
     */
    public function build($query)
    {
        $modelClass = $query->modelClass;
        $class = new ReflectionClass($modelClass);
        $method = $class->getMethod('getRelationDefinitions');

        if ($method !== null) {
            $this->_relationDefinitions = $modelClass::getRelationDefinitions();
        }

        $parts = [];

        if ($query->storedFields !== null) {
            $parts['stored_fields'] = $query->storedFields;
        }
        if ($query->scriptFields !== null) {
            $parts['script_fields'] = $query->scriptFields;
        }

        if ($query->source !== null) {
            $parts['_source'] = $query->source;
        }
        if ($query->limit !== null && $query->limit >= 0) {
            $parts['size'] = $query->limit;
        }
        if ($query->offset > 0) {
            $parts['from'] = (int)$query->offset;
        }
        if (null !== $query->minScore) {
            $parts['min_score'] = (float)$query->minScore;
        }
        if (null !== $query->explain) {
            $parts['explain'] = $query->explain;
        }

        $whereQuery = $this->buildQueryFromWhere($query->where);
//        UtilityHelper::debug($whereQuery);
        if ($whereQuery) {
            $parts['query'] = $whereQuery;
        } else if ($query->query) {
            $parts['query'] = $query->query;
        }

        if (!empty($query->highlight)) {
            $parts['highlight'] = $query->highlight;
        }
        if (!empty($query->aggregations)) {
            $parts['aggregations'] = $query->aggregations;
        }
        if (!empty($query->stats)) {
            $parts['stats'] = $query->stats;
        }
        if (!empty($query->suggest)) {
            $parts['suggest'] = $query->suggest;
        }
        if (!empty($query->postFilter)) {
            $parts['post_filter'] = $query->postFilter;
        }
        if (!empty($query->collapse)) {
            $parts['collapse'] = $query->collapse;
        }

        $sort = $this->buildOrderBy($query->orderBy);
        if (!empty($sort)) {
            $parts['sort'] = $sort;
        }

        $options = $query->options;
        if ($query->timeout !== null) {
            $options['timeout'] = $query->timeout;
        }

        return [
            'queryParts' => $parts,
            'index' => $query->index,
            'type' => $query->type,
            'options' => $options,
        ];
    }

    /**
     * @param array|string $condition
     * @return array|string
     */
    public function buildCondition($condition)
    {
        static $builders = [
            'radius' => 'buildRadiusCondition',
            'polygon' => 'buildPolygonCondition',
            'not' => 'buildNotCondition',
            'and' => 'buildBoolCondition',
            'or' => 'buildBoolCondition',
            'between' => 'buildBetweenCondition',
            'not between' => 'buildBetweenCondition',
            'in' => 'buildInCondition',
            'not in' => 'buildInCondition',
            'like' => 'buildLikeCondition',
            'not like' => 'buildLikeCondition',
            'or like' => 'buildLikeCondition',
            'or not like' => 'buildLikeCondition',
            'lt' => 'buildHalfBoundedRangeCondition',
            '<' => 'buildHalfBoundedRangeCondition',
            'lte' => 'buildHalfBoundedRangeCondition',
            '<=' => 'buildHalfBoundedRangeCondition',
            'gt' => 'buildHalfBoundedRangeCondition',
            '>' => 'buildHalfBoundedRangeCondition',
            'gte' => 'buildHalfBoundedRangeCondition',
            '>=' => 'buildHalfBoundedRangeCondition',
        ];

        if (empty($condition)) {
            return [];
        }
        if (!is_array($condition)) {
            throw new NotSupportedException('String conditions in where() are not supported by elasticsearch.');
        }
        if (isset($condition[0])) { // operator format: operator, operand 1, operand 2, ...
            $operator = strtolower($condition[0]);
            if (isset($builders[$operator])) {
                $method = $builders[$operator];
                array_shift($condition);

                return $this->$method($operator, $condition);
            }

            throw new InvalidConfigException('Found unknown operator in query: ' . $operator);
        }

        // hash format: 'column1' => 'value1', 'column2' => 'value2', ...
        return $this->buildHashCondition($condition);
    }

    /**
     * @param $condition
     * @return array
     */
    private function buildHashCondition($condition)
    {
        $parts = $emptyFields = [];
        foreach ($condition as $attribute => $value) {
            if ($attribute === '_id') {
                if ($value === null) { // there is no null pk
                    $parts[] = ['terms' => ['_uid' => []]]; // this condition is equal to WHERE false
                } else {
                    $parts[] = ['ids' => ['values' => is_array($value) ? $value : [$value]]];
                }
            } else if (is_array($value)) { // IN condition
                $parts[] = ['terms' => [$attribute => $value]];
            } else if ($value === null) {
                $emptyFields[] = ['exists' => ['field' => $attribute]];
            } else {
                $query = [
                    'term' => [
                        $attribute => $value
                    ]
                ];

                $containsPoint = strpos($attribute, '.');
                if ($containsPoint) {
                    $attributeParts = explode('.', $attribute);
                    $relationName = $attributeParts[0];

                    if (array_key_exists($relationName, $this->_relationDefinitions)
                        && $this->_relationDefinitions[$relationName] === 'nested') {
                        $query = [
                            'nested' => [
                                'path' => $relationName,
                                'query' => [
                                    'bool' => [
                                        'must' => [
                                            $query
                                        ]
                                    ]
                                ]
                            ]
                        ];
                    }
                }

                $parts[] = $query;
            }
        }

        if (count($parts) === 1) {
            return $parts[0];
        }

        $query = ['must' => $parts];
        if ($emptyFields) {
            $query['must_not'] = $emptyFields;
        }

        return ['bool' => $query];
    }

    /**
     * @param $operator
     * @param $operands
     * @return array
     */
    private function buildNotCondition($operator, $operands)
    {
        if (count($operands) !== 1) {
            throw new InvalidConfigException("Operator '$operator' requires exactly one operand.");
        }

        $operand = reset($operands);
        if (is_array($operand)) {
            $operand = $this->buildCondition($operand);
        }

        return [
            'bool' => [
                'must_not' => $operand,
            ],
        ];
    }

    /**
     * @param $operator
     * @param $operands
     * @return array|null
     */
    private function buildBoolCondition($operator, $operands)
    {
        $parts = [];
        if ($operator === 'and') {
            $clause = 'must';
        } else if ($operator === 'or') {
            $clause = 'should';
        } else {
            throw new InvalidConfigException("Operator should be 'or' or 'and'");
        }
//UtilityHelper::debug($operands);
        foreach ($operands as $operand) {
            if (is_array($operand)) {
                $operand = $this->buildCondition($operand);
            }
//            UtilityHelper::debug($operand);
            if (!empty($operand)) {
                $parts[] = $operand;
                $parts = array_reduce($parts, function ($carry, $value) {
                    if (array_key_exists('nested', $value)) {
                        foreach ($carry as $k => $v) {
                            if (array_key_exists('nested', $v) && $v['nested']['path'] === $value['nested']['path']) {
                                $carry[$k]['nested'] = ArrayHelper::merge($carry[$k]['nested'], $value['nested']);
                                return $carry;
                            }
                        }
                    }
                    $carry[] = $value;
                    return $carry;
                }, []);
            }
        }
        if ($parts) {
            return [
                'bool' => [
                    $clause => $parts,
                ]
            ];
        }

        return null;
    }

    /**
     * @param $operator
     * @param $operands
     * @return array
     */
    private function buildBetweenCondition($operator, $operands)
    {
        if (!isset($operands[0], $operands[1], $operands[2])) {
            throw new InvalidConfigException("Operator '$operator' requires three operands.");
        }

        [$column, $value1, $value2] = $operands;
        if ($column === '_id') {
            throw new NotSupportedException('Between condition is not supported for the _id field.');
        }
        $filter = ['range' => [$column => ['gte' => $value1, 'lte' => $value2]]];
        if ($operator === 'not between') {
            $filter = ['bool' => ['must_not' => $filter]];
        }

        return $filter;
    }

    /**
     * @param $operator
     * @param $operands
     * @return array
     */
    private function buildInCondition($operator, $operands)
    {
        if (!isset($operands[0], $operands[1]) || !is_array($operands)) {
            throw new InvalidConfigException("Operator '$operator' requires array of two operands: column and values");
        }

        [$column, $values] = $operands;

        $values = (array)$values;

        if (empty($values) || $column === []) {
            return $operator === 'in' ? ['terms' => ['_uid' => []]] : []; // this condition is equal to WHERE false
        }

        if (is_array($column)) {
            if (count($column) > 1) {
                return $this->buildCompositeInCondition($operator, $column, $values);
            }
            $column = reset($column);
        }
        $canBeNull = false;
        foreach ($values as $i => $value) {
            if (is_array($value)) {
                $values[$i] = $value = $value[$column] ?? null;
            }
            if ($value === null) {
                $canBeNull = true;
                unset($values[$i]);
            }
        }
        if ($column === '_id') {
            if (empty($values) && $canBeNull) { // there is no null pk
                $filter = ['terms' => ['_uid' => []]]; // this condition is equal to WHERE false
            } else {
                $filter = ['ids' => ['values' => array_values($values)]];
                if ($canBeNull) {
                    $filter = [
                        'bool' => [
                            'should' => [
                                $filter,
                                'bool' => ['must_not' => ['exists' => ['field' => $column]]],
                            ],
                        ],
                    ];
                }
            }
        } else if (empty($values) && $canBeNull) {
            $filter = [
                'bool' => [
                    'must_not' => [
                        'exists' => ['field' => $column],
                    ]
                ]
            ];
        } else {
            $filter = ['terms' => [$column => array_values($values)]];
            if ($canBeNull) {
                $filter = [
                    'bool' => [
                        'should' => [
                            $filter,
                            'bool' => ['must_not' => ['exists' => ['field' => $column]]],
                        ],
                    ],
                ];
            }
        }

        if ($operator === 'not in') {
            $filter = [
                'bool' => [
                    'must_not' => $filter,
                ],
            ];
        }

        return $filter;
    }

    /**
     * Builds a half-bounded range condition
     * (for "gt", ">", "gte", ">=", "lt", "<", "lte", "<=" operators)
     * @param string $operator
     * @param array $operands
     * @return array Filter expression
     */
    private function buildHalfBoundedRangeCondition($operator, $operands)
    {
        if (!isset($operands[0], $operands[1])) {
            throw new InvalidConfigException("Operator '$operator' requires two operands.");
        }

        [$column, $value] = $operands;
        if ($column === '_id') {
            $column = '_uid';
        }

        $range_operator = null;

        if (in_array($operator, ['gte', '>='])) {
            $range_operator = 'gte';
        } elseif (in_array($operator, ['lte', '<='])) {
            $range_operator = 'lte';
        } elseif (in_array($operator, ['gt', '>'])) {
            $range_operator = 'gt';
        } elseif (in_array($operator, ['lt', '<'])) {
            $range_operator = 'lt';
        }

        if ($range_operator === null) {
            throw new InvalidConfigException("Operator '$operator' is not implemented.");
        }

        $filter = [
            'range' => [
                $column => [
                    $range_operator => $value
                ]
            ]
        ];

        $containsPoint = strpos($column, '.');
        if ($containsPoint) {
            $attributeParts = explode('.', $column);
            $relationName = $attributeParts[0];

            if (array_key_exists($relationName, $this->_relationDefinitions)
                && $this->_relationDefinitions[$relationName] === 'nested') {
                $filter = [
                    'nested' => [
                        'path' => $relationName,
                        'query' => [
                            'bool' => [
                                'must' => [
                                    $filter
                                ]
                            ]
                        ]
                    ]
                ];
            }
        }

        return $filter;
    }

    /**
     * @param $operator
     * @param $operands
     * @return array
     * @throws InvalidConfigException
     */
    private function buildRadiusCondition($operator, $operands)
    {
        if (!isset($operands[0], $operands[1], $operands[2])) {
            throw new InvalidConfigException("Operator '$operator' requires three operands.");
        }

        return [
            'geo_distance' => [
                'distance' => "{$operands[2]}km",
                'latlng' => [
                    'lat' => $operands[0],
                    'lon' => $operands[1]
                ]
            ]
        ];
    }

    /**
     * @param $operator
     * @param $operands
     * @return array
     * @throws InvalidConfigException
     */
    private function buildPolygonCondition($operator, $operands)
    {
        if (!isset($operands[0], $operands[1], $operands[2])) {
            throw new InvalidConfigException("Operator '$operator' requires three operands.");
        }

        return [
            'geo_polygon' => [
                'latlng' => [
                    'points' => GooglePolygonMapper::getMappedData($operands[0], 'lon')
                ]
            ]
        ];
    }
}
