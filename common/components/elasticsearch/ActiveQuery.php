<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.01.2018
 * Time: 14:42
 */

namespace common\components\elasticsearch;

use common\traits\ElasticAggregationTrait;

/**
 * Class ActiveQuery
 * @package common\components\elasticsearch
 */
class ActiveQuery extends \yii\elasticsearch\ActiveQuery
{
    use ElasticAggregationTrait;
}