<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components\elasticsearch;

use yii\elasticsearch\Connection as BaseConnection;

/**
 * Class Connection
 * @package common\components\elasticsearch
 */
class Connection extends BaseConnection
{
    /**
     * Creates new query builder instance
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return new QueryBuilder($this);
    }
}