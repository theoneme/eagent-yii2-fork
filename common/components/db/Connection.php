<?php

namespace common\components\db;

use common\components\db\mysql\Schema;

/**
 * Class Connection
 * @package common\components\db
 */
class Connection extends \yii\db\Connection
{
    public $schemaMap = [
        'pgsql' => \yii\db\pgsql\Schema::class, // PostgreSQL
        'mysqli' => \yii\db\mysql\Schema::class, // MySQL
        'mysql' => Schema::class, // MySQL
        'sqlite' => \yii\db\sqlite\Schema::class, // sqlite 3
        'sqlite2' => \yii\db\sqlite\Schema::class, // sqlite 2
        'sqlsrv' => \yii\db\mssql\Schema::class, // newer MSSQL driver on MS Windows hosts
        'oci' => \yii\db\oci\Schema::class, // Oracle driver
        'mssql' => \yii\db\mssql\Schema::class, // older MSSQL driver on MS Windows hosts
        'dblib' => \yii\db\mssql\Schema::class, // dblib drivers on GNU/Linux (and maybe other OSes) hosts
        'cubrid' => \yii\db\cubrid\Schema::class, // CUBRID
    ];
}
