<?php

namespace common\components\db\mysql;

use common\components\db\conditions\PolygonCondition;
use yii\base\InvalidArgumentException;
use yii\db\conditions\BetweenColumnsConditionBuilder;
use yii\db\conditions\BetweenColumnsCondition;
use yii\db\conditions\HashConditionBuilder;
use yii\db\conditions\HashCondition;
use yii\db\conditions\SimpleConditionBuilder;
use yii\db\conditions\SimpleCondition;
use yii\db\conditions\ExistsConditionBuilder;
use yii\db\conditions\ExistsCondition;
use yii\db\conditions\LikeConditionBuilder;
use yii\db\conditions\LikeCondition;
use yii\db\conditions\InConditionBuilder;
use yii\db\conditions\InCondition;
use yii\db\conditions\BetweenConditionBuilder;
use yii\db\conditions\BetweenCondition;
use yii\db\conditions\ConjunctionConditionBuilder;
use yii\db\conditions\OrCondition;
use yii\db\conditions\AndCondition;
use yii\db\conditions\NotConditionBuilder;
use yii\db\conditions\NotCondition;
use yii\db\conditions\ConjunctionCondition;
use yii\db\ExpressionBuilder;
use yii\db\Expression;
use yii\db\PdoValueBuilder;
use yii\db\PdoValue;
use yii\db\QueryExpressionBuilder;
use yii\db\Query;
use common\components\db\conditions\RadiusCondition;

/**
 * Class QueryBuilder
 * @package common\components\db
 */
class QueryBuilder extends \yii\db\mysql\QueryBuilder
{
    /**
     * @return array
     */
    protected function defaultConditionClasses()
    {
        return [
            'NOT' => NotCondition::class,
            'AND' => AndCondition::class,
            'OR' => OrCondition::class,
            'BETWEEN' => BetweenCondition::class,
            'RADIUS' => RadiusCondition::class,
            'POLYGON' => PolygonCondition::class,
            'NOT BETWEEN' => BetweenCondition::class,
            'IN' => InCondition::class,
            'NOT IN' => InCondition::class,
            'LIKE' => LikeCondition::class,
            'NOT LIKE' => LikeCondition::class,
            'OR LIKE' => LikeCondition::class,
            'OR NOT LIKE' => LikeCondition::class,
            'EXISTS' => ExistsCondition::class,
            'NOT EXISTS' => ExistsCondition::class,
        ];
    }

    /**
     * @return array
     */
    protected function defaultExpressionBuilders()
    {
        return [
            Query::class => QueryExpressionBuilder::class,
            PdoValue::class => PdoValueBuilder::class,
            Expression::class => ExpressionBuilder::class,
            ConjunctionCondition::class => ConjunctionConditionBuilder::class,
            NotCondition::class => NotConditionBuilder::class,
            AndCondition::class => ConjunctionConditionBuilder::class,
            OrCondition::class => ConjunctionConditionBuilder::class,
            BetweenCondition::class => BetweenConditionBuilder::class,
            InCondition::class => InConditionBuilder::class,
            LikeCondition::class => LikeConditionBuilder::class,
            ExistsCondition::class => ExistsConditionBuilder::class,
            SimpleCondition::class => SimpleConditionBuilder::class,
            HashCondition::class => HashConditionBuilder::class,
            BetweenColumnsCondition::class => BetweenColumnsConditionBuilder::class,
        ];
    }

    /**
     * Creates an SQL expressions like `"column" operator value`.
     * @param string $operator the operator to use. Anything could be used e.g. `>`, `<=`, etc.
     * @param array $operands contains two column names.
     * @param array $params the binding parameters to be populated
     * @return string the generated SQL expression
     * @throws InvalidArgumentException if wrong number of operands have been given.
     * @deprecated since 2.0.14. Use `buildCondition()` instead.
     */
    public function buildSimpleCondition($operator, $operands, &$params)
    {
        array_unshift($operands, $operator);
        return $this->buildCondition($operands, $params);
    }
}
