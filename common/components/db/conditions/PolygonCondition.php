<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 15.01.2019
 * Time: 15:37
 */

namespace common\components\db\conditions;

use yii\base\InvalidArgumentException;
use yii\db\conditions\ConditionInterface;

/**
 * Class PolygonCondition
 * @package common\components\db\conditions
 */
class PolygonCondition implements ConditionInterface
{
    /**
     * @var mixed the condition to be negated
     */
    private $condition;

    /**
     * RadiusCondition constructor.
     * @param $condition
     */
    public function __construct($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException if wrong number of operands have been given.
     */
    public static function fromArrayDefinition($operator, $operands)
    {
        if (count($operands) !== 3) {
            throw new InvalidArgumentException("Operator '$operator' requires exactly three operands.");
        }

        $points = implode(',', array_map(function($value) {
            return implode(' ', $value);
        }, $operands[0]));

        return "ST_INTERSECTS(ST_GEOMFROMTEXT('POLYGON(({$points}))'), POINT({$operands[1]}, {$operands[2]}))";
    }
}
