<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 15.01.2019
 * Time: 15:37
 */

namespace common\components\db\conditions;

use yii\base\InvalidArgumentException;
use yii\db\conditions\ConditionInterface;

/**
 * Class RadiusCondition
 * @package common\components\db\conditions
 */
class RadiusCondition implements ConditionInterface
{
    /**
     * @var mixed the condition to be negated
     */
    private $condition;

    /**
     * RadiusCondition constructor.
     * @param $condition
     */
    public function __construct($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * {@inheritdoc}
     * @throws InvalidArgumentException if wrong number of operands have been given.
     */
    public static function fromArrayDefinition($operator, $operands)
    {
        if (count($operands) !== 3) {
            throw new InvalidArgumentException("Operator '$operator' requires exactly three operands.");
        }

        return "6371 * acos (
          cos ( radians({$operands[0]}) )
          * cos( radians( lat ) )
          * cos( radians( `lng` ) - radians({$operands[1]}) )
          + sin ( radians({$operands[0]}) )
          * sin( radians( lat ) )
        ) < {$operands[2]}";
    }
}
