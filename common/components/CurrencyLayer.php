<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:34
 */

namespace common\components;

use common\models\Currency;
use common\models\CurrencyExchange;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class CurrencyHelper
 * @package common\components
 */
class CurrencyLayer extends Component
{
    public const MODE_GREEDY = 0;
    public const MODE_SPIDERMAN = 10;

    /**
     * @var string
     */
    public $template;
    /**
     * @var int
     */
    public $digits;
    /**
     * @var
     */
    public $toCurrency;
    /**
     * @var int
     */
    public $mode;
    /**
     * @var Currency
     */
    private $_currency;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->_currency = Currency::find()->where(['code' => $this->toCurrency])->one();
        if($this->_currency === null) {
            throw new InvalidConfigException('Some params in Currency Service are configured incorrectly');
        }
    }

    /**
     * @param $from
     * @param $amount
     * @return float|int
     * @throws \Throwable
     */
    public function convert($from, $amount)
    {
        if ($from === $this->toCurrency) {
            return $amount;
        }

        $ratio = CurrencyExchange::getDb()->cache(function () use ($from) {
            return CurrencyExchange::find()->where(['code_from' => $from, 'code_to' => $this->toCurrency])->select('ratio')->scalar();
        });

        $result = ceil($amount * $ratio);

        return ($this->mode === self::MODE_GREEDY && $result > 15) ? ceil($result / 5) * 5 : $result;
    }

    /**
     * @param $amount
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function format($amount)
    {
        return strtr($this->template, [
            '{sl}' => $this->_currency->symbol_left,
            '{amount}' => Yii::$app->formatter->asDecimal($amount, $this->digits),
            '{sr}' => $this->_currency->symbol_right
        ]);
    }

    /**
     * @param $from
     * @param $amount
     * @return string
     * @throws \Throwable
     */
    public function convertAndFormat($from, $amount)
    {
        return $this->format($this->convert($from, $amount));
    }
}