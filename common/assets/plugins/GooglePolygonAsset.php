<?php

namespace common\assets\plugins;

use common\assets\GoogleAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class GooglePolygonAsset
 * @package common\assets\plugins
 */
class GooglePolygonAsset extends AssetBundle
{
    public $sourcePath = '@common/public';

    public $js = [
        'js/classes/google-polygon.js',
    ];
    public $depends = [
        JqueryAsset::class,
        GoogleAsset::class
    ];
}
