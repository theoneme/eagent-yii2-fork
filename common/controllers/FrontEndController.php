<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:27
 */

namespace common\controllers;

use frontend\assets\CommonAsset;
use frontend\forms\LocationForm;
use frontend\services\LocationService;
use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\captcha\CaptchaAction;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\ErrorAction;
use yii\web\JqueryAsset;
use yii\web\Response;

/**
 * Class FrontEndController
 * @package common\controllers
 */
class FrontEndController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
                'view' => '@frontend/views/site/error.php'
            ],
            'captcha' => [
                'class' => CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @param $action
     * @return bool|\yii\web\Response
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            $this->disableAjaxAssets();
            /** @var LocationService $locationService */
            $locationService = Yii::$container->get(LocationService::class);
            $locationData = $locationService->getLocationData(Yii::$app->request->cookies->getValue('location'));
            Yii::$app->params['runtime']['location'] = $locationData;
        } else {
            $isSitemap = Yii::$app->controller->module->id === 'sitemap';
            $subdomainRedirectParam = Yii::$app->params['forceSubdomainRedirects'] ?? true;
            $subdomainRedirect = $isSitemap === false && $subdomainRedirectParam;

            $result = $this->verifyLanguage();
            if ($result['redirect'] === true) {
                $this->redirect($result['url']);
                return false;
            }

            $result = $this->verifySubdomain();
            if ($subdomainRedirect === true && $result['redirect'] === true) {
                $this->redirect($result['url']);
                return false;
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * @return bool
     */
    protected function disableAjaxAssets()
    {
        $this->layout = false;

        Yii::$app->assetManager->bundles = [
            BootstrapPluginAsset::class => false,
            BootstrapAsset::class => false,
            JqueryAsset::class => false,
            CommonAsset::class => false,
        ];

        return true;
    }

    /**
     * @param mixed $object
     * @return mixed
     */
    protected function validateAjax($object)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $object->getAjaxErrors();
    }

    /**
     * @return array
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function verifySubdomain()
    {
        if (Yii::$app->controller->id === 'auth') {
            return [
                'redirect' => false
            ];
        }
        $cookies = Yii::$app->request->cookies;

        /** @var LocationService $locationService */
        $locationService = Yii::$container->get(LocationService::class);
        $locationData = $locationService->getLocationData($cookies->getValue('location'), false);
        Yii::$app->params['runtime']['location'] = $locationData;

        $appLocation = Yii::$app->request->get('app_city');

        $entity = 'city';
        $slug = $appLocation;

        if (preg_match('/^country-[\w-]+/i', $appLocation)) {
            $entity = 'country';
            $slug = preg_replace('/^country-/i', '', $appLocation);
        }
        if (preg_match('/^region-[\w-]+/i', $appLocation)) {
            $entity = 'region';
            $slug = preg_replace('/^region-/i', '', $appLocation);
        }

        if ($slug !== null) {
            if (!empty($locationData) && $slug === $locationData['slug'] && $entity === $locationData['entity']) {
                return [
                    'redirect' => false
                ];
            }

            $locationObject = $locationService->getLocationObject($entity, $slug);
            $locationForm = new LocationForm(['object' => $locationObject]);
            $result = $locationForm->saveLocation(['slug' => $slug, 'entity' => $entity], Yii::$app->response->cookies);

            if ($result === false) {
                $url = preg_replace("/^(http(?:s)?:\/\/)[\w-]+\.(" . preg_quote(Yii::$app->params['domainName']) . '.*)/', '$1$2', Url::current([], true));

                return [
                    'redirect' => true,
                    'url' => $url
                ];
            }

            return [
                'redirect' => false
            ];
        }

        if ($slug === null && !empty($locationData)) {
            $slug = $locationData['slug'];
            if ($locationData['entity'] === 'country') {
                $slug = "country-{$slug}";
            }
            if ($locationData['entity'] === 'region') {
                $slug = "region-{$slug}";
            }
            return [
                'redirect' => true,
                'url' => Url::current(['app_city' => $slug], true)
            ];
        }

        if (empty($locationData)) {
            $locationData = $locationService->getLocationData($cookies->getValue('location'));

            Yii::$app->params['runtime']['location'] = $locationData;

            if (empty($locationData)) {
                throw new InvalidConfigException('Default city is missing');
            }

            $locationObject = $locationService->getLocationObject($entity, $locationData['slug']);
            $locationForm = new LocationForm(['object' => $locationObject]);
            $locationForm->saveLocation(['slug' => $locationData['slug'], 'entity' => $entity], Yii::$app->response->cookies);

            if (Yii::$app->request->cookies['location'] !== null) {
                return [
                    'redirect' => true,
                    'url' => Url::current()
                ];
            }
        }

        return [
            'redirect' => false
        ];
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    protected function verifyLanguage()
    {
        $languageParam = Yii::$app->request->get('app_language');

        if (!Yii::$app->request->isAjax && !Yii::$app->request->isPost && in_array($languageParam, Yii::$app->params['supportedLocales'])) {
            $locale = array_search($languageParam, Yii::$app->params['supportedLocales'], true);
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'language',
                'value' => $locale,
                'expire' => time() + 60 * 60 * 24 * 30,
                'domain' => '.' . Yii::$app->params['domainName'],
            ]));
            Yii::$app->language = $locale;

            $requestUrl = explode('?', Yii::$app->request->getUrl())[0];
            $currentUrl = explode('?', Url::current())[0];
            $currentUrl = preg_replace("#^\/\/[\w-]+\." . preg_quote(Yii::$app->params['domainName']) . '(.*)#', '$1', $currentUrl);
            $currentUrl = (strpos($currentUrl, '/') === 0 ? '' : '/') . $currentUrl;
            if ($requestUrl !== $currentUrl && Yii::$app->request->cookies['language'] !== null) {
                return [
                    'redirect' => true,
                    'url' => Url::current()
                ];
            }
        }

        return [
            'redirect' => false
        ];
    }
}