<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.08.2016
 * Time: 19:27
 */

namespace common\controllers;

use common\models\User;
use Yii;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\JqueryAsset;
use yii\web\Response;

/**
 * Class BackEndController
 * @package common\controllers
 */
class BackEndController extends Controller
{
    /*
     * @var array
     */
    private $allowedModeratorsControllers = [
        'admin' => [
            'update' => true,
            'index' => true,
            'flush-cache' => true,
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return $this->isAllowed();
                        }
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * @param null $controller
     * @param null $action
     * @return bool
     */
    public function isAllowed($controller = null, $action = null)
    {
        $isGranted = false;
        if (!Yii::$app->user->isGuest) {
            /* @var User $identity */
            $identity = Yii::$app->user->identity;

            switch ($identity->role) {
                case User::ROLE_ADMIN:
                    $isGranted = true;
                    break;
                case User::ROLE_MODERATOR:
                    $controller = $controller ?? Yii::$app->controller->id;
                    $action = $action ?? Yii::$app->controller->action->id;
                    if (isset($this->allowedModeratorsControllers[$controller][$action])) {
                        $isGranted = true;
                    }
                    break;
                default:
                    break;
            }
        }

        return $isGranted;
    }


    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            $this->disableAjaxAssets();
        }

        return parent::beforeAction($action);
    }

    /**
     * @param mixed $object
     * @return mixed
     */
    protected function validateAjax($object)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $object->getAjaxErrors();
    }

    /**
     * @return bool
     */
    protected function disableAjaxAssets()
    {
        $this->layout = false;

        Yii::$app->assetManager->bundles = [
            BootstrapPluginAsset::class => false,
            BootstrapAsset::class => false,
            JqueryAsset::class => false,
        ];

        return true;
    }
}