<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.10.2018
 * Time: 15:47
 */

namespace common\factories;

use common\forms\ar\DynamicForm;
use common\mappers\DynamicFormSelectValuesMapper;
use common\mappers\GroupAttributesMapper;
use common\repositories\sql\AttributeToGroupRepository;
use common\repositories\sql\CategoryRepository;
use common\services\entities\AttributeValueService;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class DynamicFormFactory
 * @package common\factories
 */
class DynamicFormFactory
{
    /**
     * @param array $attributes
     * @param array $values
     * @return DynamicForm
     */
    public static function initByAttributes(array $attributes, array $values = [])
    {
        $dynamicForm = new DynamicForm();

        if (!empty($attributes)) {
            foreach ($attributes as $attribute) {
                $dynamicForm->defineAttribute("at_{$attribute['id']}");
                $dynamicForm->defineLabel("at_{$attribute['id']}", $attribute['title']);

                $dynamicForm->config['groups'][$attribute['customDataArray']['group']][] = "at_{$attribute['id']}";
                $dynamicForm->config['notes']["at_{$attribute['id']}"]['title'] = isset($attribute['customDataArray']['note']['title'])
                    ? Yii::t('wizard', $attribute['customDataArray']['note']['title'])
                    : Yii::t('wizard', 'Set {attribute}', [
                        'attribute' => $attribute['title']
                    ]);
                $dynamicForm->config['notes']["at_{$attribute['id']}"]['body'] = isset($attribute['customDataArray']['note']['body'])
                    ? Yii::t('wizard', $attribute['customDataArray']['note']['body'])
                    : Yii::t('wizard', 'Set {attribute}', [
                        'attribute' => $attribute['title']
                    ]);

                $dynamicForm->config['types']["at_{$attribute['id']}"] = $attribute['type'];
                $dynamicForm->config['values']["at_{$attribute['id']}"] = [];
                if (array_key_exists("at_{$attribute['id']}", $values)) {
                    $dynamicForm->config['values']["at_{$attribute['id']}"] = $values["at_{$attribute['id']}"];
                }

                if (Yii::$app->id === 'app-backend') {
                    $dynamicForm->addRule("at_{$attribute['id']}", 'safe');
                } else if (array_key_exists('rules', $attribute['customDataArray'])) {
                    foreach ($attribute['customDataArray']['rules'] as $rule) {
                        if (is_array($rule)) {
                            $dynamicForm->addRule("at_{$attribute['id']}", $rule['validator'], $rule['options']);
                        } else {
                            $dynamicForm->addRule("at_{$attribute['id']}", $rule);
                        }
                    }
                }
            }
        }

        return $dynamicForm;
    }

    /**
     * @param $entity
     * @param $category_id
     * @param $type
     * @return DynamicForm
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public static function initByCategory($entity, $category_id, $type)
    {
        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = Yii::$container->get(CategoryRepository::class);
        $category = $categoryRepository->select(['attribute_group_alias'], true)->findOneByCriteria(['id' => $category_id]);

        $attributes = [];
        if ($category['attribute_group_alias']) {
            $attributes = self::getAttributeList($entity, $category['attribute_group_alias'], $type);
        }

        $mappedAttributes = GroupAttributesMapper::getMappedData($attributes);
        $values = self::initValuesForAttributes($mappedAttributes);

        return self::initByAttributes($mappedAttributes, $values);
    }

    /**
     * @param $entity
     * @param $alias
     * @param null $type
     * @return DynamicForm
     */
    public static function initByAlias($entity, $alias, $type = null)
    {
        $attributes = self::getAttributeList($entity, $alias, $type);

        $mappedAttributes = GroupAttributesMapper::getMappedData($attributes);
        $values = self::initValuesForAttributes($mappedAttributes);

        return self::initByAttributes($mappedAttributes, $values);
    }

    /**
     * @param $entity
     * @param $alias
     * @param $type
     * @return array|\yii\db\ActiveRecord[]
     */
    private static function getAttributeList($entity, $alias, $type)
    {
        /** @var AttributeToGroupRepository $attributeToGroupRepository */
        $attributeToGroupRepository = Yii::$container->get(AttributeToGroupRepository::class);

        $attributes = $attributeToGroupRepository
            ->with(['attr.translations' => function (ActiveQuery $query) {
                return $query->andOnCondition(['locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->joinWith(['attributeGroup'], false)
            ->findManyByCriteria(['and',
                ['attribute_group.entity' => $entity],
                ['attribute_group.alias' => $alias],
                ['attribute_group.type' => $type]
            ], true);

        return $attributes;
    }

    /**
     * @param array $attributes
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    private static function initValuesForAttributes(array $attributes = [])
    {
        $values = [];

        $attributesWithValues = array_filter(array_map(function ($mappedAttribute) {
            if (in_array($mappedAttribute['type'], ['dropdownlist', 'checkbox', 'checkboxlist', 'radiolist'])) {
                return $mappedAttribute['id'];
            }
            return null;
        }, $attributes));

        if (!empty($attributesWithValues)) {
            /** @var AttributeValueService $attributeValueService */
            $attributeValueService = Yii::$container->get(AttributeValueService::class);

            $values = $attributeValueService->getMany([
                'attribute_id' => $attributesWithValues
            ]);

            $values = DynamicFormSelectValuesMapper::getMappedData($values['items']);
        }

        return $values;
    }
}