function PolygonInput(options) {
    this.options = $.extend({
        containerSelector: '#gmaps-polygon-input-container',
        formName: null,
        language: 'ru',
        apiKey: null,
        mapSelector: '#google-map',
        actionsSelection: '#gmaps-polygon-actions-container',
        startLat: 56.83200970,
        startLng: 60.59915200,
        startPoints: [],
        markerIcon: '/images/marker.png',
    }, options);

    this.map = null;
    this.markers = [];
    this.points = [];
    this.bounds = [];
    this.geocoder = null;
    this.polygon = null;

    this.attachListeners = function () {
        let that = this;
        let uluru = {lat: that.options.startLat, lng: that.options.startLng};
        that.map = new google.maps.Map($(that.options.mapSelector)[0], {
            zoom: 13,
            center: uluru,
            disableDefaultUI: true
        });
        google.maps.event.addListener(that.map, 'click', function (event) {
            let result = {lat: event.latLng.lat(), lng: event.latLng.lng()};
            let marker = new google.maps.Marker({position: result, map: that.map, icon: that.options.markerIcon});
            that.markers.push(marker);

            let count = that.points.length;
            that.points.push(result);
            if (count >= 3) {
                that.processPolygon(that.points);
            }
            that.appendPointInputs(count, result);
        });
        google.maps.event.addListenerOnce(that.map, 'tilesloaded', function(event) {
            that.initPolygon();
        });
        $(that.options.actionsSelection + " [data-action=gmaps-polygon-cancel]").on('click', function () {
            that.cancelLastPoint();
            return false;
        });

        $(that.options.actionsSelection + " [data-action=gmaps-polygon-clear]").on('click', function () {
            that.clearPolygon();
            return false;
        });
    };
    this.processPolygon = function(points) {
        if (this.polygon !== null) {
            this.polygon.setMap(null);
        }
        this.polygon = new google.maps.Polygon({
            paths: points,
            strokeColor: '#FF0000',
            strokeOpacity: 0.6,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.15,
            clickable: false
        });
        this.polygon.setMap(this.map);
    };
    this.clearPolygon = function () {
        if (this.polygon) {
            this.polygon.setMap(null);
        }
        $.each(this.markers, function () {
            this.setMap(null);
        });
        this.markers = [];
        this.points = [];
        $(this.options.containerSelector).html('');
    };
    this.cancelLastPoint = function () {
        this.points.pop();
        this.processPolygon(this.points);

        let lastMarker = this.markers.pop();
        if (lastMarker) {
            lastMarker.setMap(null);
        }

        let index = this.points.length;
        $(this.options.containerSelector + ' input[name="' + this.options.formName + '[' + index + '][lat]"]').remove();
        $(this.options.containerSelector + ' input[name="' + this.options.formName + '[' + index + '][lng]"]').remove();
    };
    this.initPolygon = function () {
        let that = this;
        this.points = $.extend(this.points, this.options.startPoints);
        let count = this.points.length;
        if (count) {
            that.bounds = new google.maps.LatLngBounds();
            $.each(this.points, function () {
                let marker = new google.maps.Marker({position: this, map: that.map, icon: that.options.markerIcon});
                that.bounds.extend(marker.getPosition());
                that.markers.push(marker);
            });
            if (count > 3) {
                this.processPolygon(this.points);
            }
            that.map.fitBounds(that.bounds);
        }
    };
    this.appendPointInputs = function (index, point) {
        $(this.options.containerSelector).append('<input name="' + this.options.formName + '[' + index + '][lat]" value="' + point.lat + '">');
        $(this.options.containerSelector).append('<input name="' + this.options.formName + '[' + index + '][lng]" value="' + point.lng + '">');
    };
    this.appendPolygonInputs = function (polygon) {
        let that = this;
        $.each(polygon, function(index, point){
            that.appendPointInputs(index, point);
        });
    };
}