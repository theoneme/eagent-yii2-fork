<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\factories\DynamicFormFactory;
use common\forms\ar\composite\AttachmentForm;
use common\forms\ar\composite\CategoryForm;
use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\composite\PriceForm;
use common\forms\ar\composite\VideoForm;
use common\forms\CompositeForm;
use common\mappers\DynamicFormValuesMapper;
use common\models\Attachment;
use common\models\AttributeGroup;
use common\models\Property;
use common\models\PropertyAttribute;
use common\models\Translation;
use common\models\User;
use common\services\AddressTranslationCreator;
use common\services\EntityAttributeValueCreator;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class PropertyForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 * @property PriceForm $price
 * @property AttachmentForm[] $attachment
 * @property GeoForm $geo
 * @property CategoryForm $category
 * @property VideoForm $video
 */
class PropertyForm extends CompositeForm
{
    /**
     * @var Property
     */
    public $_property;
    /**
     * @var DynamicForm
     */
    public $dynamicForm;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $zip;
    /**
     * @var string
     */
    public $locale;
    /**
     * @var integer
     */
    public $user_id;
    /**
     * @var integer
     */
    public $status;

    /**
     * PropertyForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->geo = new GeoForm();
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->price = new PriceForm();
        $this->category = new CategoryForm();
        $this->attachment = [];
        $this->video = new VideoForm();

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['zip'], 'string', 'max' => 10],
            [['type'], 'required'],
            ['type', 'in', 'range' => [
                Property::TYPE_RENT,
                Property::TYPE_SALE
            ]],
            [['locale'], 'in', 'range' =>
                array_keys(Yii::$app->params['supportedLocales'])
            ],
            'defaultStatus' => ['status', 'default', 'value' => Property::STATUS_ACTIVE],

//            [['user_id'], 'required'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'geo' => GeoForm::class,
            'meta' => MetaForm::class,
            'price' => PriceForm::class,
            'category' => CategoryForm::class,
            'attachment' => AttachmentForm::class,
            'video' => VideoForm::class
//            'dynamic' => DynamicForm::class
        );
    }

    /**
     * @param array $data
     * @param null $formName
     * @param array $exceptions
     * @return bool
     */
    public function load($data, $formName = null, $exceptions = ['meta'])
    {
        $success = parent::load($data, $formName, $exceptions);

        $this->dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_PROPERTY_CATEGORY, $this->category->category_id, $this->type);
        $this->dynamicForm->load($data);

        return $success;
    }

    /**
     * @return array
     */
    public function getAjaxErrors()
    {
        return array_merge(parent::getAjaxErrors(), ActiveForm::validate($this->dynamicForm));
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_property->save($withValidation);
    }

    /**
     * @param $propertyDTO
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function prepareUpdate($propertyDTO)
    {
        $this->category->load($propertyDTO, '');
        $this->price->load($propertyDTO, '');
        $this->price->price = $propertyDTO['source_price'];
        $this->geo->load($propertyDTO, '');
        $this->video->load($propertyDTO['video'], '');
        $this->load($propertyDTO, '');

        foreach ($propertyDTO['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }

        $attachmentForms = [];
        foreach ($propertyDTO['images'] as $image) {
            $attachmentForms[] = new AttachmentForm(['content' => $image]);
        }
        $this->attachment = $attachmentForms;

        $this->dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_PROPERTY_CATEGORY, $this->category->category_id, $this->type);
        $this->dynamicForm->load(DynamicFormValuesMapper::getMappedData($propertyDTO['attributes'], $this->dynamicForm->config), '');
    }

    /**
     * @return bool
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_property->load($this->attributes, '');
        $this->_property->load($this->price->attributes, '');
        $this->_property->load($this->geo->attributes, '');
        $this->_property->load($this->category->attributes, '');

        if ($this->video->content) {
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->video->content, $match);
            $videoId = $match[1];
            $video = $this->_property->bind('videos');
            $video->attributes = [
                'type' => VideoForm::TYPE_EMBED,
                'content' => "https://www.youtube.com/embed/{$videoId}"
            ];
        }

        foreach ($this->meta as $locale => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_property->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $locale,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_PROPERTY
                ];
            }

            if (!empty($meta->add_title)) {
                $title = $this->_property->bind('translations');
                $title->attributes = [
                    'value' => $meta->add_title,
                    'locale' => $locale,
                    'key' => Translation::KEY_ADDITIONAL_TITLE,
                    'entity' => Translation::ENTITY_PROPERTY
                ];
            }

            if (!empty($meta->add_description)) {
                $title = $this->_property->bind('translations');
                $title->attributes = [
                    'value' => $meta->add_description,
                    'locale' => $locale,
                    'key' => Translation::KEY_ADDITIONAL_DESCRIPTION,
                    'entity' => Translation::ENTITY_PROPERTY
                ];
            }

            if (!empty($meta->description)) {
                $description = $this->_property->bind('translations');
                $description->attributes = [
                    'value' => $meta->description,
                    'locale' => $locale,
                    'key' => Translation::KEY_DESCRIPTION,
                    'entity' => Translation::ENTITY_PROPERTY
                ];
            }

            if (!empty($meta->slug)) {
                $description = $this->_property->bind('translations');
                $description->attributes = [
                    'value' => $meta->slug,
                    'locale' => $locale,
                    'key' => Translation::KEY_SLUG,
                    'entity' => Translation::ENTITY_PROPERTY
                ];
            }
        }

        foreach ($this->attachment as $attachment) {
            $this->_property->bindAttachment($attachment->content)->attributes = ['entity' => Attachment::ENTITY_PROPERTY, 'content' => $attachment->content];
        }

        /** @var AddressTranslationCreator $addressCreator */
        $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
        $addressCreator->create(array_merge($this->geo->attributes, ['locale' => $this->locale]));

        /** @var EntityAttributeValueCreator $creator */
        $creator = Yii::$container->get(EntityAttributeValueCreator::class);

        foreach ($this->dynamicForm->attributes as $key => $propertyAttribute) {
            if (empty($propertyAttribute)) {
                continue;
            }

            $params = [
                'isId' => in_array($this->dynamicForm['config']['types'][$key], [
                    'dropdownlist',
                    'radiolist',
                    'checkboxlist',
                ]),
                'locale' => $this->locale
            ];
            $key = explode('_', $key);

            if (is_array($propertyAttribute)) {
                $values = $propertyAttribute;
            } else {
                $valueParts = array_filter(explode(',', $propertyAttribute));
                $values = $valueParts;
            }

            foreach ($values as $value) {
                $object = $creator->makeObject(PropertyAttribute::class, $key[1], $value, $params);

                $attr = $this->_property->bind('propertyAttributes');
                $attr->attributes = $object->attributes;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'zip' => Yii::t('model', 'Zip Code')
        ];
    }
}