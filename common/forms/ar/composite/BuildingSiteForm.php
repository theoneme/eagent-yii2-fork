<?php

namespace common\forms\ar\composite;

use common\models\BuildingSite;
use Yii;
use yii\base\Model;

/**
 * Class BuildingSiteForm
 * @package common\forms\ar\composite
 */
class BuildingSiteForm extends Model
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $name;
    /**
     * @var integer
     */
    public $transport;
    /**
     * @var integer
     */
    public $distance;
    /**
     * @var integer
     */
    public $time;
    /**
     * @var float
     */
    public $lat;
    /**
     * @var float
     */
    public $lng;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'transport', 'distance', 'time', 'id'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['transport'], 'in', 'range' => [
                BuildingSite::TRANSPORT_FOOT,
                BuildingSite::TRANSPORT_CAR,
                BuildingSite::TRANSPORT_TRANSPORT,
            ]],
            [['type'], 'in', 'range' => [
                BuildingSite::TYPE_METRO,
                BuildingSite::TYPE_SCHOOL,
                BuildingSite::TYPE_PARK,
                BuildingSite::TYPE_POND,
                BuildingSite::TYPE_AIRPORT,
                BuildingSite::TYPE_CITY_CENTER,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'type' => Yii::t('model', 'Type'),
            'name' => Yii::t('model', 'Name'),
            'transport' => Yii::t('model', 'Transport'),
            'distance' => Yii::t('model', 'Distance'),
            'time' => Yii::t('model', 'Time'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!empty($this->lat)) {
            $this->lat = round($this->lat, 8);
        }
        if (!empty($this->lng)) {
            $this->lng = round($this->lng, 8);
        }

        return parent::beforeValidate();
    }
}