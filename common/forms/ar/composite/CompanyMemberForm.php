<?php

namespace common\forms\ar\composite;

use Yii;
use yii\base\Model;

/**
 * Class CompanyMemberForm
 * @package common\forms\ar
 */
class CompanyMemberForm extends Model
{
    /**
     * @var integer
     */
    public $user_id;
    /**
     * @var integer
     */
    public $role;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'role'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('model', 'User'),
            'role' => Yii::t('model', 'Role'),
        ];
    }
}