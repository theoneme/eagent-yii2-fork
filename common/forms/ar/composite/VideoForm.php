<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use common\validators\RegexValidator;
use Yii;
use yii\base\Model;

/**
 * Class VideoForm
 * @package common\forms\ar\composite
 */
class VideoForm extends Model
{
    public const TYPE_EMBED = 0;
    public const TYPE_FILE = 10;

    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $content;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['content', 'string', 'max' => 155],
            ['content', RegexValidator::class,
                'regex' => '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',
                'message' => Yii::t('model', '{attribute} contains invalid youtube link')],
            ['type', 'in', 'range' => [
                self::TYPE_EMBED,
//                self::TYPE_FILE
            ]]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'content' => Yii::t('model', 'Tour'),
        ];
    }
}