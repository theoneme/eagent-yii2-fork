<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use common\models\Currency;
use Yii;
use yii\base\Model;

/**
 * Class PriceForm
 * @package common\forms\ar
 */
class PriceForm extends Model
{
    /**
     * @var integer
     */
    public $price;
    /**
     * @var string
     */
    public $currency_code;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'currency_code'], 'required'],
            [['price'], 'integer'],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'price' => Yii::t('model', 'Price'),
            'currency_code' => Yii::t('model', 'Currency Code'),
        ];
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->price = preg_replace('/\D/', '', $this->price);

        return parent::beforeValidate();
    }
}