<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use Yii;
use yii\base\Model;

/**
 * Class AttachmentForm
 * @package common\forms\ar\composite
 */
class AttachmentForm extends Model
{
    /**
     * @var string
     */
    public $content;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'content' => Yii::t('model', 'Content')
        ];
    }
}