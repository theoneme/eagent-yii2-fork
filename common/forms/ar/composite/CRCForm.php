<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use common\models\City;
use common\models\Country;
use common\models\Region;
use yii\base\Model;

/**
 * Class CRCForm
 * @package common\forms\ar\composite
 */
class CRCForm extends Model
{
    /**
     * @var float
     */
    public $country_id;
    /**
     * @var float
     */
    public $region_id;
    /**
     * @var string
     */
    public $city_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'region_id', 'city_id'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['region_id' => 'id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
        ];
    }
}