<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use Yii;
use yii\base\Model;

/**
 * Class PasswordForm
 * @package common\forms\ar\composite
 */
class PasswordForm extends Model
{
    /**
     * @var integer
     */
    public $password;
    /**
     * @var string
     */
    public $confirm;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password', 'confirm'], 'string', 'min' => 6, 'max' => 72],
            ['confirm', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('model', 'New Password'),
            'confirm' => Yii::t('model', 'Password Confirmation')
        ];
    }
}