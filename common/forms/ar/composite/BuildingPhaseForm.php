<?php

namespace common\forms\ar\composite;

use common\models\BuildingPhase;
use Yii;
use yii\base\Model;

/**
 * Class BuildingPhaseForm
 * @package common\forms\ar\composite
 */
class BuildingPhaseForm extends Model
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $year;
    /**
     * @var integer
     */
    public $quarter;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var integer
     */
    public $floors;
    /**
     * @var float
     */
    public $lat;
    /**
     * @var float
     */
    public $lng;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year', 'quarter'], 'required'],
            [['year', 'quarter', 'status', 'floors', 'id'], 'integer'],
            [['year'], 'number', 'max' => 9999],
            [['quarter'], 'number', 'max' => 4],
            [['lat', 'lng'], 'number'],
            ['status', 'default', 'value' => BuildingPhase::STATUS_UNFINISHED],
            [['status'], 'in', 'range' => [
                BuildingPhase::STATUS_UNFINISHED,
                BuildingPhase::STATUS_FINISHED,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'year' => Yii::t('model', 'Year'),
            'quarter' => Yii::t('model', 'Quarter'),
            'status' => Yii::t('model', 'Status'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
            'floors' => Yii::t('model', 'Floors'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->lat = round($this->lat, 8);
        $this->lng = round($this->lng, 8);

        return parent::beforeValidate();
    }
}