<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

/**
 * Class LightGeoForm
 * @package common\forms\ar\composite
 */
class LightGeoForm extends GeoForm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lat', 'lng'], 'number'],
        ];
    }
}