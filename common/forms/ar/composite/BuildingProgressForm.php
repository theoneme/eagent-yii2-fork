<?php

namespace common\forms\ar\composite;

use common\forms\CompositeForm;
use Yii;

/**
 * Class BuildingProgressForm
 * @package common\forms\ar\composite
 *
 * @property BuildingProgressAttachmentForm[] $attachments
 */
class BuildingProgressForm extends CompositeForm
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $year;
    /**
     * @var integer
     */
    public $quarter;

    /**
     * BuildingProgressForm constructor.
     * @param array $config
     * @param array $attachments
     */
    public function __construct($config = [], $attachments = [])
    {
        $this->attachments = array_map(function ($image) {
            return new BuildingProgressAttachmentForm(['content' => $image]);
        }, $attachments);

        parent::__construct($config);
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return [
            'attachments' => BuildingProgressAttachmentForm::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year', 'quarter'], 'required'],
            [['year', 'quarter', 'id'], 'integer'],
            [['year'], 'number', 'max' => 9999],
            [['quarter'], 'number', 'max' => 4],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'year' => Yii::t('model', 'Year'),
            'quarter' => Yii::t('model', 'Quarter'),
            'status' => Yii::t('model', 'Status'),
            'lat' => Yii::t('model', 'Lat'),
            'lng' => Yii::t('model', 'Lng'),
            'floors' => Yii::t('model', 'Floors'),
        ];
    }
}