<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use Yii;
use yii\base\Model;

/**
 * Class MetaForm
 * @package common\forms\ar
 */
class MetaForm extends Model
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $add_title;
    /**
     * @var string
     */
    public $subtitle;
    /**
     * @var string
     */
    public $description;
    /**
     * @var string
     */
    public $add_description;
    /**
     * @var string
     */
    public $locale;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var array
     */
    protected $_config = [];

    /**
     * MetaForm constructor.
     * @param array $rules
     * @param array $config
     */
    public function __construct(array $rules = [], array $config = [])
    {
        parent::__construct($config);
        $this->_config = $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
//            [['description'], 'required'],
            [['title', 'subtitle', 'add_title'], 'string', 'max' => 150],
            [['slug'], 'string', 'max' => 100],
            [['add_description'], 'string'],
            [['description'], 'string', 'min' => 3],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['languages'])]
        ];

        if (!empty($this->_config) && array_key_exists('rules', $this->_config)) {
            $rules = array_merge($rules, $this->_config['rules']);
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('model', 'Title'),
            'description' => Yii::t('model', 'Description'),
            'subtitle' => Yii::t('model', 'Short Description'),
            'locale' => Yii::t('model', 'Locale'),
        ];
    }
}