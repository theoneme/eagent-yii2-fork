<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use common\models\UserContact;
use Yii;
use yii\base\Model;
use yii\helpers\StringHelper;

/**
 * Class GeoForm
 * @package common\forms\ar
 */
class ContactForm extends Model
{
    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $value;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'value'], 'required'],
            [['value'], 'string', 'max' => 150],
            ['type', 'in', 'range' => [
                UserContact::TYPE_EMAIL => 0,
                UserContact::TYPE_PHONE => 10,
                UserContact::TYPE_WEBSITE => 30,
                UserContact::TYPE_SKYPE => 100,
                UserContact::TYPE_WHATSAPP => 110,
                UserContact::TYPE_VIBER => 120,
                UserContact::TYPE_TELEGRAM => 130,
                UserContact::TYPE_FACEBOOK => 200,
                UserContact::TYPE_VK => 210,
                UserContact::TYPE_TWITTER => 220,
                UserContact::TYPE_GOOGLE => 230,
                UserContact::TYPE_YOUTUBE => 240,
                UserContact::TYPE_OK => 250,
            ]]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('model', 'Contact Type'),
            'value' => Yii::t('model', 'Value')
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ((int)$this->type !== UserContact::TYPE_PHONE) {
            $this->value = StringHelper::truncateWords($this->value, 1, '');
        }

        return parent::beforeValidate();
    }
}