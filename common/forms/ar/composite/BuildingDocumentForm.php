<?php

namespace common\forms\ar\composite;

use common\models\BuildingDocument;
use Yii;
use yii\base\Model;

/**
 * Class BuildingDocumentForm
 * @package common\forms\ar\composite
 */
class BuildingDocumentForm extends Model
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var integer
     */
    public $name;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $file;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'id'], 'integer'],
            [['name', 'file'], 'string', 'max' => 255],
            [['type'], 'in', 'range' => [
                BuildingDocument::TYPE_PERMISSION,
                BuildingDocument::TYPE_PROJECT,
                BuildingDocument::TYPE_OPERATION_ACT,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'name' => Yii::t('model', 'Name'),
            'type' => Yii::t('model', 'Type'),
            'file' => Yii::t('model', 'File'),
        ];
    }
}