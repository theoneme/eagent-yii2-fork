<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar\composite;

use common\models\Category;
use Yii;
use yii\base\Model;

/**
 * Class CategoryForm
 * @package common\forms\ar\composite
 */
class CategoryForm extends Model
{
    /**
     * @var integer
     */
    public $category_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['category_id'], 'required'],

            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'category_id' => Yii::t('model', 'Category')
        ];
    }
}