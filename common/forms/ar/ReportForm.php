<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\models\Report;

/**
 * Class ReportForm
 * @package common\forms\ar
 */
class ReportForm extends Report
{
    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        return parent::load($data, $formName);
    }
}