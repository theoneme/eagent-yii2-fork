<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Attribute;
use common\models\AttributeValue;
use common\models\Translation;
use Yii;

/**
 * Class AttributeValueForm
 *
 * @property MetaForm $meta
 *
 * @package frontend\models
 */
class AttributeValueForm extends CompositeForm
{
    /**
     * @var AttributeValue
     */
    public $_attributeValue;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var string
     */
    public $status;
    /**
     * @var integer
     */
    public $attribute_id;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'alias'], 'required'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::class, 'targetAttribute' => ['attribute_id' => 'id']],
            [['alias'], 'string', 'max' => 65],
            [['alias'], 'unique', 'targetAttribute' => ['alias', 'attribute_id'], 'targetClass' => AttributeValue::class, 'when' => function ($model) {
                return $model->alias !== $this->_attributeValue->alias;
            }],
            ['status', 'in', 'range' => [
                AttributeValue::STATUS_PENDING,
                AttributeValue::STATUS_APPROVED
            ]],
            [['status'], 'default', 'value' => AttributeValue::STATUS_PENDING],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'alias' => Yii::t('model', 'Alias'),
            'status' => Yii::t('model', 'Status'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_attributeValue->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_attributeValue->load($this->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_attributeValue->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_ATTRIBUTE_VALUE
                ];
            }
        }

        return true;
    }
}