<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Page;
use common\models\Translation;
use Yii;

/**
 * Class PageForm
 * @package common\forms\ar
 *
 * @property MetaForm $meta
 */
class PageForm extends CompositeForm
{
    /**
     * @var Page
     */
    public $_page;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);

        parent::__construct($config);
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
        );
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_page->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_page->load($this->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_page->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_PAGE
                ];
            }

            if (!empty($meta->slug)) {
                $title = $this->_page->bind('translations');
                $title->attributes = [
                    'value' => $meta->slug,
                    'locale' => $key,
                    'key' => Translation::KEY_SLUG,
                    'entity' => Translation::ENTITY_PAGE
                ];
            }

            if (!empty($meta->description)) {
                $title = $this->_page->bind('translations');
                $title->attributes = [
                    'value' => $meta->description,
                    'locale' => $key,
                    'key' => Translation::KEY_DESCRIPTION,
                    'entity' => Translation::ENTITY_PAGE
                ];
            }
        }

        return true;
    }
}