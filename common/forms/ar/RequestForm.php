<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\factories\DynamicFormFactory;
use common\forms\ar\composite\AttachmentForm;
use common\forms\ar\composite\CategoryForm;
use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\composite\PriceForm;
use common\forms\CompositeForm;
use common\helpers\UtilityHelper;
use common\mappers\DynamicFormValuesMapper;
use common\models\Attachment;
use common\models\AttributeGroup;
use common\models\Property;
use common\models\Request;
use common\models\Translation;
use common\models\User;
use common\services\AddressTranslationCreator;
use common\services\AttributeValueCreator;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class RequestForm
 *
 * @property MetaForm $meta
 * @property PriceForm $price
 * @property GeoForm $geo
 * @property CategoryForm $category
 * @property AttachmentForm $attachment
 *
 * @package common\forms\ar
 */
class RequestForm extends CompositeForm
{
    /**
     * @var Request
     */
    public $_request;
    /**
     * @var DynamicForm
     */
    public $dynamicForm;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $locale;
    /**
     * @var integer
     */
    public $user_id;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var boolean
     */
    public $contract_price;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->geo = new GeoForm();
        $this->meta = new MetaForm(['rules' => [
            [['title'], 'required']
        ]]);
        $this->price = new PriceForm();
        $this->category = new CategoryForm();
        $this->attachment = [];

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['type'], 'required'],
            ['type', 'in', 'range' => [Request::TYPE_RENT, Request::TYPE_SALE]],
            [['contract_price'], 'boolean'],
            [['locale'], 'in', 'range' =>
                array_keys(Yii::$app->params['supportedLocales'])
            ],
            'defaultStatus' => ['status', 'default', 'value' => Property::STATUS_ACTIVE],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'geo' => GeoForm::class,
            'meta' => MetaForm::class,
            'price' => PriceForm::class,
            'category' => CategoryForm::class,
            'attachment' => AttachmentForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'contract_price' => Yii::t('model', 'Contract Price')
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @param array $exceptions
     * @return bool
     */
    public function load($data, $formName = null, $exceptions = [])
    {
        $success = parent::load($data, $formName, $exceptions);

        $this->dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_REQUEST_CATEGORY, $this->category->category_id, $this->type);
        $this->dynamicForm->load($data);

        return $success;
    }

    /**
     * @return array
     */
    public function getAjaxErrors()
    {
        return array_merge(parent::getAjaxErrors(), ActiveForm::validate($this->dynamicForm));
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_request->save($withValidation);
    }

    /**
     * @param $requestDTO
     */
    public function prepareUpdate($requestDTO)
    {
        $this->category->load($requestDTO, '');
        $this->price->load($requestDTO, '');
        $this->price->price = $requestDTO['source_price'];
        $this->geo->load($requestDTO, '');
        $this->meta->load($requestDTO, '');
        $this->load($requestDTO, '');

        $attachmentForms = [];
        foreach ($requestDTO['images'] as $image) {
            $attachmentForms[] = new AttachmentForm(['content' => $image]);
        }
        $this->attachment = $attachmentForms;

        $this->dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_REQUEST_CATEGORY, $this->category->category_id, $this->type);
        $this->dynamicForm->load(DynamicFormValuesMapper::getMappedData($requestDTO['attributes'], $this->dynamicForm->config), '');
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_request->load($this->attributes, '');
        $this->_request->load($this->price->attributes, '');
        $this->_request->load($this->geo->attributes, '');
        $this->_request->load($this->category->attributes, '');
        $this->_request->slug = UtilityHelper::generateSlug($this->meta->title);

        if (!empty($this->meta->title)) {
            $title = $this->_request->bind('translations');
            $title->attributes = [
                'value' => $this->meta->title,
                'locale' => $this->locale,
                'key' => Translation::KEY_TITLE,
                'entity' => Translation::ENTITY_REQUEST
            ];
        }

        if (!empty($this->meta->description)) {
            $description = $this->_request->bind('translations');
            $description->attributes = [
                'value' => $this->meta->description,
                'locale' => $this->locale,
                'key' => Translation::KEY_DESCRIPTION,
                'entity' => Translation::ENTITY_REQUEST
            ];
        }

        foreach ($this->attachment as $attachment) {
            $this->_request->bindAttachment($attachment->content)->attributes = ['entity' => Attachment::ENTITY_REQUEST, 'content' => $attachment->content];
        }

        /** @var AddressTranslationCreator $addressCreator */
        $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
        $addressCreator->create(array_merge($this->geo->attributes, ['locale' => $this->locale]));

        /** @var AttributeValueCreator $avCreator */
        $avCreator = Yii::$container->get(AttributeValueCreator::class);

        foreach ($this->dynamicForm->attributes as $key => $requestAttribute) {
            if (empty($requestAttribute)) {
                continue;
            }

            $isId = in_array($this->dynamicForm['config']['types'][$key], [
                'dropdownlist',
                'radiolist',
                'checkboxlist',
            ]);

            $key = explode('_', $key);

            if (is_array($requestAttribute)) {
                $values = array_filter($requestAttribute);
            } else {
                $valueParts = array_filter(explode(',', $requestAttribute));
                $values = $valueParts;
            }

            if (array_key_exists('min', $values) || array_key_exists('max', $values)) {
                $requestAttribute = $this->_request->bind('requestAttributes');
                $requestAttribute->load([
                    'attribute_id' => $key[1],
                    'customDataArray' => [
                        'min' => $values['min'] ?? null,
                        'max' => $values['max'] ?? null
                    ]
                ], '');
            } else {
                foreach ($values as $value) {
                    $requestAttribute = $this->_request->bind('requestAttributes');

                    if ($isId === false) {
                        $attributeValue = $avCreator->makeObject($key[1], $value, ['isId' => $isId, 'locale' => $this->locale]);
                        $value = $attributeValue['id'];
                    }

                    $requestAttribute->load([
                        'attribute_id' => $key[1],
                        'customDataArray' => [
                            'value' => $value
                        ]
                    ], '');
                }
            }
        }

        return true;
    }
}