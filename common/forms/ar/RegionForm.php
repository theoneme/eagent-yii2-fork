<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Country;
use common\models\Region;
use common\models\Translation;
use Yii;

/**
 * Class RegionForm
 * @package common\forms\ar
 *
 * @property MetaForm $meta
 */
class RegionForm extends CompositeForm
{
    /**
     * @var Region
     */
    public $_region;
    /**
     * @var string
     */
    public $code;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var integer
     */
    public $country_id;

    /**
     * RegionForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'string', 'max' => 2],
            [['slug'], 'unique', 'targetClass' => Region::class, 'when' => function ($model) {
                return $model->slug !== $this->_region->slug;
            }],
            [['slug'], 'required'],
            [['slug'], 'string', 'max' => 55],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            [['country_id'], 'required'],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('model', 'Country'),
            'code' => Yii::t('model', 'Region Code')
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_region->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_region->load($this->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_region->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_REGION
                ];
            }
        }

        return true;
    }
}