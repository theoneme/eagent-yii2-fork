<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\forms\ar\composite\ContactForm;
use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\composite\PasswordForm;
use common\forms\CompositeForm;
use common\models\Translation;
use common\models\User;
use common\services\AddressTranslationCreator;
use Yii;

/**
 * Class UserForm
 *
 * @property GeoForm $geo
 * @property MetaForm $meta
 * @property PasswordForm $password
 * @property ContactForm[] $contact
 *
 * @package common\forms\ar
 */
class UserForm extends CompositeForm
{
    /**
     * @var User
     */
    public $_user;
    /**
     * @var DynamicForm
     */
    public $dynamicForm;
    /**
     * @var string
     */
    public $avatar;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $username;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var boolean
     */
    public $is_company;
    /**
     * @var boolean
     */
    public $ads_allowed;
    /**
     * @var boolean
     */
    public $ads_allowed_partners;

    /**
     * UserForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->contact = [];
        $this->password = new PasswordForm();
        $this->meta = new MetaForm();
        $this->geo = new GeoForm();

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => User::class, 'when' => function ($model) {
                return $model->email !== $this->_user->email;
            }],
            [['phone'], 'unique', 'targetClass' => User::class, 'when' => function ($model) {
                return $model->phone !== $this->_user->phone;
            }],
            [['email', 'phone'], 'default', 'value' => null],
            [['email'], 'phoneOrEmailRequired'],
            ['avatar', 'string', 'max' => 155],
            ['username', 'required'],
            ['username', 'string', 'max' => 35],
            ['type', 'in', 'range' => [
                User::TYPE_DEFAULT,
                User::TYPE_REALTOR
            ]],
            [['is_company', 'ads_allowed', 'ads_allowed_partners'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'avatar' => Yii::t('model', 'Avatar'),
            'email' => Yii::t('model', 'Email'),
            'phone' => Yii::t('model', 'Phone'),
            'username' => Yii::t('model', 'Username'),
            'ads_allowed' => Yii::t('model', 'Advertising on Site Allowed'),
            'ads_allowed_partners' => Yii::t('model', 'Advertising on Partner Sites Allowed'),
        ];
    }

    /**
     * @param $attribute
     */
    public function phoneOrEmailRequired($attribute)
    {
        if (empty($this->email) && empty($this->phone)) {
            $this->addError($attribute, Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => 'Email or phone']));
        }
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'contact' => ContactForm::class,
            'password' => PasswordForm::class,
            'meta' => MetaForm::class,
            'geo' => GeoForm::class
        );
    }

    /**
     * @return array
     */
//    public function getAjaxErrors()
//    {
//        return array_merge(parent::getAjaxErrors(), ActiveForm::validate($this->dynamicForm));
//    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_user->save($withValidation);
    }

    /**
     * @param $userDTO
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function prepareUpdate($userDTO)
    {
        $this->load($userDTO, '');
        $this->geo->load($userDTO, '');
        $this->meta->load($userDTO['translations'], '');

        $this->contact = array_map(function ($var) {
            return new ContactForm($var);
        }, $userDTO['contacts']);
//
//        $this->dynamicForm = DynamicFormFactory::initByGroup(AttributeGroup::ENTITY_PROPERTY_CATEGORY, $this->category->category_id, $this->type);
//        $this->dynamicForm->load(DynamicFormValuesMapper::getMappedData($userDTO['attributes'], $this->dynamicForm->config), '');
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_user->load($this->attributes, '');
        $this->_user->load($this->geo->attributes, '');

        /** @var AddressTranslationCreator $addressCreator */
        $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
        $addressCreator->create(array_merge($this->geo->attributes, ['locale' => Yii::$app->language]));

        if (!empty($this->meta->subtitle)) {
            $title = $this->_user->bind('translations');
            $title->attributes = [
                'value' => $this->meta->subtitle,
                'locale' => Yii::$app->language,
                'key' => Translation::KEY_SUBTITLE,
                'entity' => Translation::ENTITY_USER
            ];
        }
        if (!empty($this->meta->description)) {
            $description = $this->_user->bind('translations');
            $description->attributes = [
                'value' => $this->meta->description,
                'locale' => Yii::$app->language,
                'key' => Translation::KEY_DESCRIPTION,
                'entity' => Translation::ENTITY_USER
            ];
        }

        foreach ($this->contact as $contact) {
            $contactRelation = $this->_user->bind('contacts');
            $contactRelation->attributes = [
                'value' => $contact['value'],
                'type' => $contact['type'],
            ];
        }

        if (!empty($this->password->password)) {
            $this->_user->setPassword($this->password->password);
        }

        return true;
    }
}