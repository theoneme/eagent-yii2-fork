<?php

namespace common\forms\ar;

use common\models\PaymentMethod;

/**
 * Class PaymentMethodForm
 * @package common\forms\ar
 */
class PaymentMethodForm extends PaymentMethod
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [

        ]);
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        return parent::load($data, $formName);
    }
}