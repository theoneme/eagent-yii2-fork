<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Country;
use common\models\Translation;
use Yii;

/**
 * Class CountryForm
 * @package common\forms\ar
 *
 * @property MetaForm $meta
 */
class CountryForm extends CompositeForm
{
    /**
     * @var Country
     */
    public $_country;
    /**
     * @var string
     */
    public $code;
    /**
     * @var string
     */
    public $slug;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'string', 'max' => 2],
            [['slug'], 'unique', 'targetClass' => Country::class, 'when' => function ($model) {
                return $model->slug !== $this->_country->slug;
            }],
            [['slug'], 'required'],
            [['slug'], 'string', 'max' => 55],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('model', 'Country Code')
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_country->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_country->load($this->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_country->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_COUNTRY
                ];
            }
        }

        return true;
    }
}