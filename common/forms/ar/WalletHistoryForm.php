<?php

namespace common\forms\ar;

use common\models\Currency;
use common\models\Payment;
use common\models\User;
use common\models\WalletHistory;

/**
 * Class WalletHistoryForm
 * @package common\forms\ar
 */
class WalletHistoryForm extends WalletHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::class, 'targetAttribute' => ['payment_id' => 'id']],
        ]);
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        return parent::load($data, $formName);
    }
}