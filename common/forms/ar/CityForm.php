<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\LightGeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Country;
use common\models\City;
use common\models\Region;
use common\models\Translation;
use Yii;

/**
 * Class CityForm
 * @package common\forms\ar
 *
 * @property MetaForm $meta
 * @property GeoForm $geo
 */
class CityForm extends CompositeForm
{
    /**
     * @var City
     */
    public $_city;
    /**
     * @var boolean
     */
    public $is_big;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var integer
     */
    public $country_id;
    /**
     * @var integer
     */
    public $region_id;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->geo = new LightGeoForm();

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'region_id'], 'required'],
            [['is_big'], 'boolean'],
            [['slug'], 'unique', 'targetClass' => City::class, 'when' => function ($model) {
                return $model->slug !== $this->_city->slug;
            }],
            [['slug'], 'required'],
            [['slug'], 'string', 'max' => 55],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
            'geo' => LightGeoForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('model', 'Country'),
            'region_id' => Yii::t('model', 'Region'),
            'code' => Yii::t('model', 'City Code')
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_city->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');
        $this->geo->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_city->load($this->attributes, '');
        $this->_city->load($this->geo->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_city->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_CITY
                ];
            }
        }

        return true;
    }
}