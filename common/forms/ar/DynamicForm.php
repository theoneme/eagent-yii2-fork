<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2017
 * Time: 12:20
 */

namespace common\forms\ar;

use Yii;
use yii\base\DynamicModel;
use yii\validators\StringValidator;

/**
 * Class DynamicForm
 * @package common\forms\ar
 */
class DynamicForm extends DynamicModel
{
    /**
     * @var array
     */
    private $_labels = [];
    /**
     * @var array
     */
    public $config;

    /**
     * @param $attribute
     */
    public function validateSelectizeString($attribute)
    {
        $stringValidator = new StringValidator(['max' => 25, 'min' => 2]);
        $values = explode(',', $this->{$attribute});
        foreach($values as $value) {
            if(!$stringValidator->validate($value)) {
                $this->addError($attribute, Yii::t('wizard', 'Value {value} is too short or too long', ['value' => $value]));
            }
        }
    }

    /**
     * @param $name
     * @param null $value
     */
    public function defineLabel($name, $value = null)
    {
        $this->_labels[$name] = $value;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return $this->_labels;
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasAttribute($name)
    {
        return isset($this->attributes[$name]);
    }

    /**
     * @param $group
     * @return array
     */
    public function getAttributesByGroup($group)
    {
        return $this->config['groups'][$group] ?? [];
    }
}