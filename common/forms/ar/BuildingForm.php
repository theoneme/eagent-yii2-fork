<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace common\forms\ar;

use common\factories\DynamicFormFactory;
use common\forms\ar\composite\BuildingAttachmentForm;
use common\forms\ar\composite\BuildingDocumentForm;
use common\forms\ar\composite\BuildingMetaForm;
use common\forms\ar\composite\BuildingPhaseForm;
use common\forms\ar\composite\BuildingProgressForm;
use common\forms\ar\composite\BuildingSiteForm;
use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\composite\VideoForm;
use common\forms\CompositeForm;
use common\mappers\DynamicFormValuesMapper;
use common\models\Attachment;
use common\models\AttributeGroup;
use common\models\Building;
use common\models\BuildingAttribute;
use common\models\Translation;
use common\models\User;
use common\services\AddressTranslationCreator;
use common\services\EntityAttributeValueCreator;
use Yii;
use yii\widgets\ActiveForm;

/**
 * Class BuildingForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 * @property BuildingAttachmentForm[] $attachment
 * @property GeoForm $geo
 * @property BuildingPhaseForm[] $phases
 * @property BuildingProgressForm[] $progress
 * @property BuildingSiteForm[] $sites
 * @property BuildingDocumentForm[] $documents
 * @property VideoForm $video
 */
class BuildingForm extends CompositeForm
{
    /**
     * @var Building
     */
    public $_building;
    /**
     * @var DynamicForm
     */
    public $dynamicForm;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $locale;
    /**
     * @var integer
     */
    public $user_id;

    /**
     * BuildingForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->geo = new GeoForm(['rules' => [
            [['lat', 'lng'], 'unique', 'targetClass' => Building::class, 'when' => function ($model) {
                return $model->lat !== (float)$this->_building->lat && $model->lat !== (float)$this->_building->lng;
            }],
            ['address', 'required']
        ]]);
        $this->meta = array_map(function () {
            return new BuildingMetaForm();
        }, Yii::$app->params['languages']);
        $this->attachment = [];
        $this->phases = [];
        $this->progress = [];
        $this->sites = [];
        $this->documents = [];
        $this->video = new VideoForm();

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'in', 'range' => [
                Building::TYPE_DEFAULT,
                Building::TYPE_NEW_CONSTRUCTION,
            ]],
            [['status'], 'in', 'range' => [
                Building::STATUS_DELETED,
                Building::STATUS_PAUSED,
                Building::STATUS_DRAFT,
                Building::STATUS_REQUIRES_MODERATION,
                Building::STATUS_REQUIRES_MODIFICATION,
                Building::STATUS_ACTIVE,
            ]],
            [['locale'], 'default', 'value' => Yii::$app->language],
            [['locale'], 'string', 'max' => 5],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'geo' => GeoForm::class,
            'meta' => BuildingMetaForm::class,
            'attachment' => BuildingAttachmentForm::class,
            'phases' => BuildingPhaseForm::class,
            'progress' => BuildingProgressForm::class,
            'sites' => BuildingSiteForm::class,
            'documents' => BuildingDocumentForm::class,
            'video' => VideoForm::class
        );
    }

    /**
     * @param array $data
     * @param null $formName
     * @param array $exceptions
     * @return bool
     */
    public function load($data, $formName = null, $exceptions = ['meta'])
    {
        $success = parent::load($data, $formName, $exceptions);

        $this->dynamicForm = DynamicFormFactory::initByAlias(AttributeGroup::ENTITY_BUILDING, 'building', $this->type);
        $this->dynamicForm->load($data);

        return $success;
    }

    /**
     * @return array
     */
    public function getAjaxErrors()
    {
        return array_merge(parent::getAjaxErrors(), ActiveForm::validate($this->dynamicForm));
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_building->save($withValidation);
    }

    /**
     * @param $buildingDTO
     */
    public function prepareUpdate($buildingDTO)
    {
        $this->geo->load($buildingDTO, '');
        $this->video->load($buildingDTO['video'], '');
        foreach ($buildingDTO['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
        $this->load($buildingDTO, '');

//        $attachmentForms = [];
//        foreach ($buildingDTO['images'] as $image) {
//            $attachmentForms[] = new AttachmentForm(['content' => $image]);
//        }
        $this->attachment = array_map(function ($image) {
            return new BuildingAttachmentForm(['content' => $image]);
        }, $buildingDTO['images']);

        $this->phases = array_map(function ($phase) {
            $model = new BuildingPhaseForm();
            $model->load($phase, '');
            return $model;
        }, $buildingDTO['phases']);

        $this->progress = array_map(function ($progress) {
            return new BuildingProgressForm(['id' => $progress['id'], 'year' => $progress['year'], 'quarter' => $progress['quarter']], $progress['images']);
        }, $buildingDTO['progress']);

        $this->sites = array_map(function ($site) {
            $model = new BuildingSiteForm();
            $model->load($site, '');
            return $model;
        }, $buildingDTO['sites']);

        $this->documents = array_map(function ($document) {
            $model = new BuildingDocumentForm();
            $model->load($document, '');
            return $model;
        }, $buildingDTO['documents']);

        $this->dynamicForm = DynamicFormFactory::initByAlias(AttributeGroup::ENTITY_BUILDING, 'building', $this->type);
        $this->dynamicForm->load(DynamicFormValuesMapper::getMappedData($buildingDTO['attributes'], $this->dynamicForm->config), '');
    }

    /**
     * @return bool
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_building->load($this->attributes, '');
        $this->_building->load($this->geo->attributes, '');

        if ($this->video->content) {
            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $this->video->content, $match);
            $videoId = $match[1];
            $video = $this->_building->bind('videos');
            $video->attributes = [
                'type' => VideoForm::TYPE_EMBED,
                'content' => "https://www.youtube.com/embed/{$videoId}"
            ];
        }

        foreach ($this->meta as $locale => $meta) {
            if (!empty($meta->name)) {
                $name = $this->_building->bind('translations');
                $name->attributes = [
                    'value' => $meta->name,
                    'locale' => $locale,
                    'key' => Translation::KEY_NAME,
                    'entity' => Translation::ENTITY_BUILDING
                ];
            }
            if (!empty($meta->description)) {
                $description = $this->_building->bind('translations');
                $description->attributes = [
                    'value' => $meta->description,
                    'locale' => $locale,
                    'key' => Translation::KEY_DESCRIPTION,
                    'entity' => Translation::ENTITY_BUILDING
                ];
            }
            if (!empty($meta->title)) {
                $description = $this->_building->bind('translations');
                $description->attributes = [
                    'value' => $meta->title,
                    'locale' => $locale,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_BUILDING
                ];
            }
        }

        foreach ($this->attachment as $attachment) {
            $this->_building->bindAttachment($attachment->content)->attributes = ['entity' => Attachment::ENTITY_BUILDING, 'content' => $attachment->content];
        }

        foreach ($this->phases as $phase) {
            $this->_building->bind('phases', (int)$phase->id)->attributes = $phase->attributes;
        }

        foreach ($this->progress as $progress) {
            $progressRelation = $this->_building->bind('progress', (int)$progress->id);
            $progressRelation->attributes = $progress->attributes;
            foreach ($progress->attachments as $progressAttachment) {
                $progressRelation->bindAttachment($progressAttachment->content)->attributes = ['entity' => Attachment::ENTITY_BUILDING_PROGRESS, 'content' => $progressAttachment->content];
            }
        }

        foreach ($this->sites as $site) {
            $this->_building->bind('sites', (int)$site->id)->attributes = $site->attributes;
        }

        foreach ($this->documents as $document) {
            $this->_building->bind('documents', (int)$document->id)->attributes = $document->attributes;
        }

        /** @var AddressTranslationCreator $addressCreator */
        $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
        $addressCreator->create(array_merge($this->geo->attributes, ['locale' => $this->locale]));

        foreach ($this->dynamicForm->attributes as $key => $buildingAttribute) {
            if (empty($buildingAttribute)) {
                continue;
            }

            $params = [
                'isId' => in_array($this->dynamicForm['config']['types'][$key], [
                    'dropdownlist',
                    'radiolist',
                    'checkboxlist',
                ]),
                'locale' => $this->locale
            ];
            $key = explode('_', $key);

            if (is_array($buildingAttribute)) {
                $values = $buildingAttribute;
            } else {
                $valueParts = array_filter(explode(',', $buildingAttribute));
                $values = $valueParts;
            }

            /** @var EntityAttributeValueCreator $creator */
            $creator = Yii::$container->get(EntityAttributeValueCreator::class);

            foreach ($values as $value) {
                $object = $creator->makeObject(BuildingAttribute::class, $key[1], $value, $params);

                $attr = $this->_building->bind('buildingAttributes');
                $attr->attributes = $object->attributes;
            }
        }

        return true;
    }
}