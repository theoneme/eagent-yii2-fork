<?php

namespace common\forms\ar;

use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\Category;
use common\models\Translation;
use Yii;

/**
 * Class CategoryForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 */
class CategoryForm extends CompositeForm
{
    /**
     * @var Category
     */
    public $_category;
    /**
     * @var integer
     */
    public $parent_id;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);

        parent::__construct($config);
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return [
            'meta' => MetaForm::class,
        ];
    }

    /**
     * @param bool $runValidation
     * @return bool
     */
    public function save($runValidation = true)
    {
        if (!$this->_category->isNewRecord) {
            $saved = $this->_category->save($runValidation);
        } else if ($this->parent_id !== null && $parent = Category::findOne($this->parent_id)) {
            $saved = $this->_category->appendTo($parent, $runValidation);
        } else {
            $saved = $this->_category->makeRoot($runValidation);
        }
        return $saved;
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_category->load($this->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_category->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_CATEGORY
                ];
            }

            if (!empty($meta->slug)) {
                $title = $this->_category->bind('translations');
                $title->attributes = [
                    'value' => $meta->slug,
                    'locale' => $key,
                    'key' => Translation::KEY_SLUG,
                    'entity' => Translation::ENTITY_CATEGORY
                ];
            }

            if (!empty($meta->add_title)) {
                $title = $this->_category->bind('translations');
                $title->attributes = [
                    'value' => $meta->add_title,
                    'locale' => $key,
                    'key' => Translation::KEY_ADDITIONAL_TITLE,
                    'entity' => Translation::ENTITY_CATEGORY
                ];
            }
        }

        return true;
    }
}