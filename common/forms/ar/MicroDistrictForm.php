<?php

namespace common\forms\ar;

use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\LightGeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\models\City;
use common\models\District;
use common\models\MicroDistrict;
use common\models\Translation;
use Yii;

/**
 * Class MicroDistrictForm
 * @package common\forms\ar
 *
 * @property MetaForm[] $meta
 * @property GeoForm[] $geo
 */
class MicroDistrictForm extends CompositeForm
{
    /**
     * @var MicroDistrict
     */
    public $_microDistrict;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var boolean
     */
    public $city_id;
    /**
     * @var boolean
     */
    public $district_id;

    /**
     * RequestForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->geo = [];

        parent::__construct($config);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required'],
            [['city_id', 'district_id'], 'integer'],
//            [['polygon'], 'string'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['city_id' => 'id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => District::class, 'targetAttribute' => ['district_id' => 'id']],
            [['slug'], 'required'],
            [['slug'], 'string', 'max' => 55],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
            'geo' => LightGeoForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'city_id' => Yii::t('model', 'City ID'),
            'polygon' => Yii::t('model', 'Polygon'),
        ];
    }


    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_microDistrict->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');

        foreach ($dto['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }
        $geoForms = [];
        foreach ($dto['polygonArray']['coordinates'][0] as $i => $point) {
            $geoForm = new LightGeoForm();
            $geoForm->load(['lat' => $point[0], 'lng' => $point[1]], '');
            $geoForms[] = $geoForm;
        }
        $this->geo = $geoForms;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_microDistrict->load($this->attributes, '');

        foreach ($this->meta as $key => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_microDistrict->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $key,
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_MICRO_DISTRICT
                ];
            }
        }
        $polygon = [];
        foreach ($this->geo as $geoForm) {
            $polygon[] = [$geoForm->lng, $geoForm->lat];
        }
        if (count($polygon) >= 3) {
            $last = end($polygon);
            reset($polygon);
            if ($polygon[0][0] !== $last[0] || $polygon[0][1] !== $last[1]) {
                $polygon[] = $polygon[0];
            }
            $this->_microDistrict->polygonArray = ['type' => 'Polygon', 'coordinates' => [$polygon]];
        }

        return true;
    }
}