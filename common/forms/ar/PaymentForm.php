<?php

namespace common\forms\ar;

use common\models\Currency;
use common\models\Payment;
use common\models\PaymentMethod;
use common\models\User;
/**
 * Class PaymentForm
 * @package common\forms\ar
 */
class PaymentForm extends Payment
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['payment_method_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentMethod::class, 'targetAttribute' => ['payment_method_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ]);
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \ReflectionException
     */
    public function load($data, $formName = null)
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        return parent::load($data, $formName);
    }
}