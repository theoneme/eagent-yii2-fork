<?php

namespace common\forms\ar;

use common\models\Company;
use common\models\CompanyMember;
use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Class CompanyMemberForm
 *
 * @package common\forms\ar
 */
class CompanyMemberForm extends Model
{
    /**
     * @var CompanyMember
     */
    public $_companyMember;
    /**
     * @var integer
     */
    public $user_id;
    /**
     * @var integer
     */
    public $company_id;
    /**
     * @var integer
     */
    public $role;
    /**
     * @var integer
     */
    public $status;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'role', 'status'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['role', 'in', 'range' => [
                CompanyMember::ROLE_CONTENT_MANAGER,
                CompanyMember::ROLE_SUPPORT,
                CompanyMember::ROLE_ADMIN,
                CompanyMember::ROLE_OWNER
            ]],
            ['status', 'in', 'range' => [
                CompanyMember::STATUS_ACTIVE,
                CompanyMember::STATUS_INVITED
            ]]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('model', 'User'),
            'company_id' => Yii::t('model', 'Company'),
            'role' => Yii::t('model', 'Role'),
            'status' => Yii::t('model', 'Status'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_companyMember->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        $success = parent::load($data, $formName);
        $success = $success && $this->_companyMember->load($this->attributes, '');

        return $success;
    }
}