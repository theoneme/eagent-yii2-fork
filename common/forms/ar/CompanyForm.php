<?php

namespace common\forms\ar;

use common\forms\ar\composite\ContactForm;
use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use common\mappers\ContactsMapper;
use common\models\Company;
use common\models\Translation;
use common\services\AddressTranslationCreator;
use common\services\entities\UserService;
use Yii;

/**
 * Class CompanyForm
 *
 * @property GeoForm $geo
 * @property MetaForm $meta
 * @property ContactForm[] $contact
 *
 * @package common\forms\ar
 */
class CompanyForm extends CompositeForm
{
    /**
     * @var Company
     */
    public $_company;
    /**
     * @var string
     */
    public $logo;
    /**
     * @var string
     */
    public $banner;
    /**
     * @var integer
     */
    public $status;
    /**
     * @var boolean
     */
    public $ads_allowed;
    /**
     * @var boolean
     */
    public $ads_allowed_partners;
    /**
     * @var integer
     */
    public $type = Company::TYPE_AGENCY;

    /**
     * CompanyForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->contact = [];
        $this->meta = new MetaForm(['rules' => [
            [['title'], 'required']
        ]]);
        $this->geo = new GeoForm(['rules' => [
            [['address'], 'required']
        ]]);

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['logo', 'banner'], 'string', 'max' => 255],
            [['ads_allowed', 'ads_allowed_partners'], 'boolean'],
            ['type', 'in', 'range' => [Company::TYPE_AGENCY, Company::TYPE_DEVELOPER]],
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'contact' => ContactForm::class,
            'meta' => MetaForm::class,
            'geo' => GeoForm::class
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'logo' => Yii::t('model', 'Logo'),
            'banner' => Yii::t('model', 'Banner'),
            'ads_allowed' => Yii::t('model', 'Advertising on Site Allowed'),
            'ads_allowed_partners' => Yii::t('model', 'Advertising on Partner Sites Allowed'),
            'type' => Yii::t('model', 'Company Type'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        /* @var UserService $userService */
        $userService = Yii::$container->get(UserService::class);
        $userFormFields = [
            'UserForm' => ['ads_allowed' => $this->ads_allowed, 'ads_allowed_partners' => $this->ads_allowed_partners],
            'ContactForm' => ContactsMapper::getMappedData($this->contact)
        ];
        if ($this->_company->user_id) {
            $userService->update($userFormFields, ['id' => $this->_company->user_id]);
            $userId = $this->_company->user_id;
        } else {
            $password = crypt(time() . '@fake.cake', time() . '@fake.cake');
            $email = time() . uniqid() . '@fake.cake';
            $userFormFields['UserForm']['email'] = $email;
            $userFormFields['UserForm']['is_company'] = true;
            $userFormFields['UserForm']['username'] = $email;
            $userFormFields['PasswordForm'] = [
                'password' => $password,
                'confirm' => $password
            ];
            $userId = $userService->create($userFormFields);
            $this->_company->user_id = $userId;
        }

        return $userId && $this->_company->save($withValidation);
    }

    /**
     * @param $dto
     */
    public function prepareUpdate($dto)
    {
        $this->load($dto, '');
        $this->meta->load($dto, '');
        $this->geo->load($dto, '');

        $this->contact = array_map(function ($var) {
            return new ContactForm($var);
        }, $dto['contacts']);
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_company->load($this->attributes, '');
        $this->_company->load($this->geo->attributes, '');

        if (!empty($this->meta->title)) {
            $title = $this->_company->bind('translations');
            $title->attributes = [
                'value' => $this->meta->title,
                'locale' => Yii::$app->language,
                'key' => Translation::KEY_TITLE,
                'entity' => Translation::ENTITY_COMPANY
            ];
        }
        if (!empty($this->meta->description)) {
            $description = $this->_company->bind('translations');
            $description->attributes = [
                'value' => $this->meta->description,
                'locale' => Yii::$app->language,
                'key' => Translation::KEY_DESCRIPTION,
                'entity' => Translation::ENTITY_COMPANY
            ];
        }

        /** @var AddressTranslationCreator $addressCreator */
        $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
        $addressCreator->create(array_merge($this->geo->attributes, ['locale' => Yii::$app->language]));

        return true;
    }
}