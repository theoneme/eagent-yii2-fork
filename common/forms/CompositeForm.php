<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.10.2018
 * Time: 16:59
 */

namespace common\forms;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;

/**
 * Class CompositeForm
 * @package common\forms
 */
abstract class CompositeForm extends Model
{
    /**
     * @var Model[]|array[]
     */
    protected $_forms = [];

    /**
     * @return array of internal forms like ['meta', 'values']
     */
    abstract protected function internalForms();

    /**
     * @param array $data
     * @param null $formName
     * @param array $exceptions
     * @return bool
     */
    public function load($data, $formName = null, $exceptions = [])
    {
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $data)) {
            $formName = '';
        }

        $success = parent::load($data, $formName);

        foreach ($this->internalForms() as $formKey => $formClass) {
            $form = $this->_forms[$formKey];
            $shortClassName = StringHelper::basename($this->getFormClassByName($formKey));

            if (is_array($form)) {
                if (empty($form) || (array_key_exists($shortClassName, $data) && count($form) !== count($data[$shortClassName]))) {
                    if (!in_array($formKey, $exceptions)) {
                        $forms = $this->initFormsByPost($form, $data, $formName ?: $formKey);
                        if ($forms) {
                            $this->_forms[$formKey] = $forms;
                        }
                    }
                }

                $success = Model::loadMultiple($this->_forms[$formKey], $data, $formName !== '' ? null : $shortClassName) || $success;
            } else {
                $success = $form->load($data, $formName !== '' ? null : $shortClassName) || $success;
            }
        }

        return $success;
    }

    /**
     * @param null $attributeNames
     * @param bool $clearErrors
     * @return bool
     */
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if ($attributeNames !== null) {
            $parentNames = array_filter($attributeNames, 'is_string');
            $success = $parentNames ? parent::validate($parentNames, $clearErrors) : true;
        } else {
            $success = parent::validate(null, $clearErrors);
        }

        foreach ($this->_forms as $name => $form) {
            if ($attributeNames === null || array_key_exists($name, $attributeNames) || in_array($name, $attributeNames, true)) {
                $innerNames = ArrayHelper::getValue($attributeNames, $name);
                if (is_array($form)) {
                    $success = Model::validateMultiple($form, $innerNames) && $success;
                } else {
                    $success = $form->validate($innerNames, $clearErrors) && $success;
                }
            }
        }

        return $success;
    }

    /**
     * @param array $form
     * @param $data
     * @param $formName
     * @return array
     */
    protected function initFormsByPost(array $form, $data, $formName)
    {
        if (empty($data) || $formName === '') {
            return $form;
        }

        try {
            $formClass = $this->getFormClassByName($formName);
            /** @var Model $blankForm */
            $blankForm = new $formClass();
            $postFormName = $blankForm->formName();
            if (!empty($data[$postFormName])) {
                $newForms = [];
                for ($i = 0, $max = count($data[$postFormName]); $i < $max; $i++) {
                    $newForms[] = clone $blankForm;
                }

                $form = array_combine(array_keys($data[$postFormName]), $newForms);
            }
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage());
        }

        return $form;
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function getFormClassByName($name)
    {
        $forms = $this->internalForms();
        if (!array_key_exists($name, $forms)) {
            throw new \RuntimeException("Form class name for \"$name\" form does not found in the Internal array.");
        }
        return $forms[$name];
    }

    /**
     * @return array
     */
    public function getAjaxErrors()
    {
        $errors = array_reduce($this->_forms, function ($carry, $value) {
            if (is_array($value)) {
                $carry = array_merge($carry, ActiveForm::validateMultiple($value));
            } else {
                $carry = array_merge($carry, ActiveForm::validate($value));
            }

            return $carry;
        }, ActiveForm::validate($this));

        return $errors;
    }

    /**
     * @param string $name
     * @return array|mixed|Model
     * @throws \yii\base\UnknownPropertyException
     */
    public function __get($name)
    {
        if (isset($this->_forms[$name])) {
            return $this->_forms[$name];
        }
        return parent::__get($name);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @throws \yii\base\UnknownPropertyException
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->internalForms())) {
            $this->_forms[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->_forms[$name]) || parent::__isset($name);
    }
}