<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 14:15
 */

namespace common\traits;

/**
 * Trait ElasticAggregationTrait
 * @package common\traits
 */
trait ElasticAggregationTrait
{
    /**
     * @param $aggregations
     * @return $this
     */
    public function aggregations($aggregations)
    {
        $this->aggregations = $aggregations;
        return $this;
    }
}