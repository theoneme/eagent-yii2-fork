/**
 * Created by Алёнка on 10.02.2017.
 */

/*******VARIABLES*******/
end_price_input = $('#end-price-input');
end_price = $('#end-price');
begin_price_input = $('#begin-price-input');
begin_price = $('#begin-price');
/*****END VARIABLES*****/


/*******FUNCTIONS*******/
function rightPrice() {
    var rightPrice = begin_price_input.val().replace(/[^0-9]/gim, '');
    if (rightPrice == '') {
        rightPrice = 0;
    }
    rightPrice = parseInt(rightPrice) + 50000;
    $('.filter-prices .filter-column-right .filter-choose').each(function () {
        $(this).html('$' + addCommas(rightPrice));
        rightPrice += 50000;
    });
    $('.filter-prices .filter-column-right .filter-choose:last-child').html('Any Price');
    $('.filter-prices .filter-column-left').hide();
    $('.filter-prices .filter-column-right').show();
}

function priceDelimeters() {
    if (end_price_input.val() == '' && begin_price_input.val() == '') {
        $('#up-price').html('');
        $('#delimiter-price').html('Any Price');
    } else {
        if (begin_price_input.val() == '') {
            $('#up-price').html('Up To ');
            $('#delimiter-price').html('');
        } else {
            if (end_price_input.val() == '') {
                $('#up-price').html('');
                $('#delimiter-price').html('+');
            } else {
                $('#up-price').html('');
                $('#delimiter-price').html(' - ');
            }
        }
    }
}

function activeRight() {
    var thisChoose = $('.filter-prices .filter-column-right .filter-choose');
    thisChoose.removeClass('active');
    thisChoose.each(function () {
        if ($(this).html().replace(/[^0-9]/gim, '') == $('#end-price-input').val().replace(/[^0-9]/gim, '')) {
            $(this).addClass('active');
        }
    });
}

function activeLeft() {
    var thisChoose = $('.filter-prices .filter-column-left .filter-choose');
    thisChoose.removeClass('active');
    thisChoose.each(function () {
        if ($(this).html().replace(/[^0-9]/gim, '') == begin_price_input.val().replace(/[^0-9]/gim, '')) {
            $(this).addClass('active');
        }
    });
}

function changeLT() {
    var countF = $('.filter-types .filter-type-group-name input[type="checkbox"]:checked');
    if (countF.length == 1) {
        $('.filter-types .filter-type-name').html(countF.parents('.filter-type-group-name').find('label .ftgn-title').html());
    } else {
        $('.filter-types .filter-type-name').html('Listing Type');
    }
}

function filterMore() {
    var more = 0;
    $('.filter-more .more-row').each(function () {
        var moreInp = 0;
        $(this).find('input[type="text"]').each(function () {
            if ($(this).val() != '' && moreInp < 1) {
                more++;
                moreInp++;
            }
        });
    });
    $('.filter-more .more-row select').each(function () {
        if ($(this).val() != '0') {
            more++;
        }
    });
    if ($('.filter-more textarea').val() != '') {
        more++;
    }
    if ($('.filter-more input[type="checkbox"]').is(':checked')) {
        more++;
    }
    $('#more-par').html(more);
}
/*****END FUNCTIONS*****/


$(document).ready(function () {
    $body.on('click', '.filter-name', function () {
        if ($(this).parents('.filter-item').hasClass('open')) {
            $(this).parents('.filter-item').removeClass('open');
        } else {
            $('.filter-item').removeClass('open');
            $(this).parents('.filter-item').addClass('open')
        }
    });

    $body.on('click', '.filter-prices .filter-name', function () {
        $('.filter-prices .filter-column-left').show();
        $('.filter-prices .filter-column-right').hide();
    });

    $body.on('keyup', '.number-format', function () {
        var input_value = $(this).val().replace(/[^0-9]/gim, '');
        $(this).val(addCommas(input_value));
    });

    $body.on('keyup', '#begin-price-input', function () {
        activeLeft();
        var input_value = $(this).val().replace(/[^0-9]/gim, '');
        if (input_value.length > 9) {
            input_value = input_value.substring(0, input_value.length - 1);
        }
        $(this).val(addCommas(input_value));
        begin_price.html('$' + addSymbols(input_value));
        priceDelimeters();
        if (input_value != '') {
            begin_price.html('$' + addSymbols(input_value));
        } else {
            begin_price.html('');
        }
    });

    $body.on('keyup', '#end-price-input', function () {
        var input_value = $(this).val().replace(/[^0-9]/gim, '');
        $(this).val(addCommas(input_value));
        if (input_value.length > 9) {
            input_value = input_value.substring(0, input_value.length - 1);
        }
        $(this).val(addCommas(input_value));
        end_price.html('$' + addSymbols(input_value));
        priceDelimeters();
        if (input_value != '') {
            end_price.html('$' + addSymbols(input_value));
        } else {
            end_price.html('');
        }
        activeRight();
    });

    $body.on('focus', '#end-price-input', function () {
        activeRight();
        rightPrice();
    });

    $body.on('focus', '#begin-price-input', function () {
        $('.filter-prices .filter-column-left').show();
        $('.filter-prices .filter-column-right').hide();
    });

    $body.on('click', '.filter-beds .filter-choose', function () {
        $('.filter-beds .filter-choose').removeClass('active');
        $(this).addClass('active');
        $('#beds').html($(this).html());
        $(this).parents('.filter-item').removeClass('open');
    });

    $body.on('change', '.filter-home input', function () {
        $('#homeType').html($('.filter-home input:checked').size());
    });

    $body.on('click', '.filter-prices .filter-column-left .filter-choose', function () {
        $('.filter-prices .filter-column-left .filter-choose').removeClass('active');
        $(this).addClass('active');
        var currentPrice = $(this).html().replace(/[^0-9]/gim, '');
        begin_price_input.val(addCommas(currentPrice));
        end_price_input.focus();
        rightPrice();
        if (currentPrice != 0) {
            begin_price.html('$' + addSymbols(currentPrice));
        } else {
            begin_price_input.val('');
            begin_price.html('');
        }
        activeRight();
        priceDelimeters();
    });

    $body.on('click', '.filter-prices .filter-column-right .filter-choose', function () {
        $('.filter-prices .filter-column-right .filter-choose').removeClass('active');
        $(this).addClass('active');
        var currentPrice = $(this).html().replace(/[^0-9]/gim, '');
        end_price_input.val(addCommas(currentPrice));
        end_price.html('$' + addSymbols(currentPrice));
        if (currentPrice != 0) {
            end_price.html('$' + addSymbols(currentPrice));
        } else {
            end_price_input.val('');
            end_price.html('');
        }
        priceDelimeters();
        $(this).parents('.filter-item').removeClass('open');
    });

    $('.filter-types .filter-type-group-name input[type="checkbox"]').each(function () {
        if (!$(this).parents('.filter-type-group').parent().hasClass('filter-footer')) {
            var circleClass = $(this).parents('.filter-type-group-name').find('label .filter-circle').attr('class');
            circleClass = circleClass.replace('filter-circle ', '');
            if (!$(this).is(":checked")) {
                $('.filter-types-icons .' + circleClass).hide();
            }
        }
        changeLT();
    });

    $body.on('change', '.filter-types .filter-type-group-name input[type="checkbox"]', function () {
        if (!$(this).parents('.filter-type-group').parent().hasClass('filter-footer')) {
            var circleClass = $(this).parents('.filter-type-group-name').find('label .filter-circle').attr('class');
            circleClass = circleClass.replace('filter-circle ', '');
            if ($(this).is(':checked')) {
                $(this).parents('.filter-type-group').find('.filter-sub-type input[type="checkbox"]').prop('checked', true);
                $('.filter-types-icons .' + circleClass).show();
            } else {
                $(this).parents('.filter-type-group').find('.filter-sub-type input[type="checkbox"]').prop('checked', false);
                $('.filter-types-icons .' + circleClass).hide();
            }
        }
        changeLT();
    });

    $body.on('change', '.filter-types .filter-sub-type input[type="checkbox"]', function () {
        if (!$(this).parents('.filter-type-group').parent().hasClass('filter-footer')) {
            var circleClass = $(this).parents('.filter-type-group').find('.filter-type-group-name').find('label .filter-circle').attr('class');
            circleClass = circleClass.replace('filter-circle ', '');
            $(this).parents('.filter-type-group').find('.filter-type-group-name input[type="checkbox"]').prop('checked', false);
            $('.filter-types-icons .' + circleClass).hide();
            $(this).parents('.filter-sub-types').find('.filter-sub-type').each(function () {
                if ($(this).find('input[type="checkbox"]').is(':checked')) {
                    $('.filter-types-icons .' + circleClass).show();
                    $(this).parents('.filter-type-group').find('.filter-type-group-name input[type="checkbox"]').prop('checked', true);
                }
            });
        }
        changeLT();
    });

    $body.on('change', '.filter-more input', function () {
        filterMore();
    });

    $body.on('change', '.filter-more textarea', function () {
        filterMore();
    });

    $body.on('change', '.filter-more select', function () {
        filterMore();
    });

    $body.on('click', '.filter-item', function (e) {
        e.stopPropagation();
    });

});