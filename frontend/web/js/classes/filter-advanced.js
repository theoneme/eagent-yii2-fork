function Filter(options) {
    this.options = $.extend({
        submitOnChange: true,
        filterSelector: '#filter-form',
        topFilterItem: '[data-action=expand-filter]',
        moreFilterItem: '[data-action=show-more-filters]',
        leftPriceItem: '[data-action=set-left-price]',
        rightPriceItem: '[data-action=set-right-price]',
        changeViewItem: '[data-action=change-catalog-view]',
        cityPartsItem: '[data-action=load-city-parts]',
        submitCityPartsItem: '[data-action=submit-city-parts]',
        clearCityPartsItem: '[data-action=clear-city-parts]',
        leftPriceInput: '#min-price-input',
        rightPriceInput: '#max-price-input',
        topFilterOption: '[data-action=set-top-filter]',
        mapSelector: '#map_catalog',
        districtFilter: '#filter-district',
        microDistrictFilter: '#filter-microdistrict',
        locationInput: '#filter-city-helper',

        catalogSortSelector: '#catalog-sort',
        filterSortSelector: '#filter-sort',
        filterBoxSelector: '#filter-box',
        filterZoomSelector: '#filter-zoom',

        locationUrl: null,
        canonicalUrl: null,
        changeViewUrl: null,
        cityPartsUrl: null,
    }, options);

    this.process = function () {
        $(this.options.filterSelector).submit();
    };

    this.registerHandlers = function () {
        let that = this;

        new AutoComplete({
            selector: that.options.locationInput,
            autoFocus: false,
            customContainer: true,
            containerClass: 'autocomplete-loc-suggestions',
            minChars: 3,
            source: function (request, response) {
                try {
                    xhr.abort();
                } catch (e) {
                }
                xhr = $.get(that.options.locationUrl, {request: request}, function (data) {
                    response(data);
                });
            },
            renderItem: function (item, search) {
                search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                return '<div class="autocomplete-suggestion" data-title="' + item.title + '" data-slug="' + item.slug + '">'
                    + item.title.replace(re, "<b>$1</b>")
                    + '</div>';
            },
            onSelect: function (e, term, item) {
                $("#filter-city").val(item.getAttribute('data-slug'));
                $("#filter-city-helper").val(item.getAttribute('data-title')).prop('disabled', true);
                $(that.options.filterSelector).submit();
            }
        });

        $(that.options.locationInput).on('click', function () {
            $('.district-form').removeClass('hidden').removeClass('d-none');
        });

        $(this.options.topFilterItem).on('click', function () {
            $('.filter-item').removeClass('open');
            $(this).parents('.filter-item').toggleClass('open');
            $(this).parents('.filter-item').find('.filter-box').toggleClass('d-none');
        });
        $(this.options.moreFilterItem).on('click', function () {
            $('.filter-item').removeClass('open');
            $('.params-filter').slideToggle(300);
            $('html,body').scrollTop(0);
        });
        $(this.options.cityPartsItem).on('click', function () {
            let districtInput = $(that.options.districtFilter),
                microDistrictInput = $(that.options.microDistrictFilter);

            $(this).closest('.district-form').addClass('hidden').addClass('d-none'); //TODO переделать этот кусок говна для мобильной версии

            that.sendRequest(that.options.cityPartsUrl, {
                district: districtInput.val(),
                microdistrict: microDistrictInput.val()
            }, function (result) {
                if (result.success === true) {
                    $('[data-role=district-container]').html(result.html.districts);
                    $('[data-role=microdistrict-container]').html(result.html.microdistricts);
                }
            });
        });
        $(this.options.submitCityPartsItem).on('click', function () {
            let checked = $(this).closest('.modal-content').find('input:checked'),
                entity = checked.data('entity'),
                slug = checked.val(),
                districtInput = $(that.options.districtFilter),
                microDistrictInput = $(that.options.microDistrictFilter);

            switch (entity) {
                case 'microdistrict':
                    microDistrictInput.val(slug);
                    districtInput.val('');
                    break;

                case 'district':
                    districtInput.val(slug);
                    microDistrictInput.val('');
            }

            that.process();
        });
        $(this.options.clearCityPartsItem).on('click', function () {
            let districtInput = $(that.options.districtFilter),
                microDistrictInput = $(that.options.microDistrictFilter);
            districtInput.val('');
            microDistrictInput.val('');

            $(this).closest('.modal-content').find('input:checked').prop('checked', false);
        });
        $(this.options.catalogSortSelector).on('change', function () {
            $(that.options.filterSortSelector).val($(this).val()).trigger('change');
        });
        $(this.options.leftPriceItem).on('click', function () {
            $(this).closest('.filter-box').find(that.options.leftPriceInput).val($(this).data('value')).trigger('change');
        });
        $(this.options.rightPriceItem).on('click', function () {
            $(this).closest('.filter-box').find(that.options.rightPriceInput).val($(this).data('value')).trigger('change');
        });
        $(this.options.topFilterOption).on('click', function () {
            $(this).closest('.filter-item').find('input').val($(this).data('value')).trigger('change');
        });
        $(this.options.changeViewItem).on('click', function () {
            let self = $(this);
            that.sendRequest(that.options.changeViewUrl, {view: $(this).data('view')}, function (result) {
                if (result.success === true) {
                    let newUrl = self.data('url');
                    if (newUrl) {
                        window.location.href = newUrl;
                    } else {
                        window.location.reload();
                    }
                }
            })
        });
        $(this.options.leftPriceInput).on('focus', function () {
            $('.filter-column-left').show().removeClass('d-none');
            $('.filter-column-right').hide().addClass('d-none');
        });
        $(this.options.rightPriceInput).on('focus', function () {
            $('.filter-column-left').hide().addClass('d-none');
            $('.filter-column-right').show().removeClass('d-none');
        });
        if (that.options.submitOnChange === true) {
            $(that.options.filterSelector + ' input:not([name=city]):not([name=city-helper]), ' + that.options.filterSelector + ' select').on('change', function () {
                $(that.options.filterSelector).submit();
            });
        }
    };

    this.setBox = function (box) {
        $(this.options.filterBoxSelector).val(box);
    };
    this.setZoom = function (zoom) {
        $(this.options.filterZoomSelector).val(zoom);
    };

    this.buildQuery = function (specials = []) {
        let inputs = $(this.options.filterSelector + ' input:not(.price-label):not(#filter-city-helper):not([name=_csrf-frontend]):not([value=""])'),
            dropdowns = $(this.options.filterSelector + ' select'),
            query;

        $.each(dropdowns, function () {
            if ($(this).find('option:selected') && $(this).find('option:selected').val()) {
                inputs.push($(this)[0]);
            }
        }).promise().done(function () {
            query = inputs.filter(function (index, element) {
                if (specials.length > 0) {
                    return $(element).val() !== "" && specials.indexOf(element.getAttribute('name')) !== -1;
                }

                return $(element).val() !== "";
            }).serialize();
        });

        return query;
    };

    this.getUrl = function (query) {
        let baseUrl = this.options.canonicalUrl;

        if (query.length > 0) {
            if (baseUrl.indexOf('?') > -1) {
                baseUrl += '&' + query;
            } else {
                baseUrl += '?' + query;
            }
        }

        return baseUrl;
    };

    this.sendRequest = function (address, data, callback) {
        $.ajax({
            url: address,
            type: 'get',
            data: data,
            success: function (result) {
                if (callback) {
                    callback(result);
                } else {

                }
            }
        });
    };
}