function CatalogMap(options) {
    const DEFAULT_ZOOM = 11;

    this.options = $.extend({
        lat: 33.347316,
        lon: -115.729235,
        zoom: DEFAULT_ZOOM,
        box: null,
        zoomControl: true,
        zoomSatteliteValue: 20,
        streetViewControl: true,
        isManualMapType: false,
        scaleControl: true,
        mode: 'default',
        mapCanvasId: 'map_catalog',
        searchInputId: 'google-map-search',
        polygon: [],
        markerViewRoutes: {
            property: null
        },
        markerIcons: {
            property: null
        },
        markerEvents: {
            property: {
                hover: null
            }
        },

        markerListSelector: '[data-role=marker-data]'
    }, options);

    this.polygon = null;
    this.markerCache = {};
    this.markersFetched = false;
    this.map = null;
    this.markerClusterer = null;
    this.markers = [];
    this.infoWindow = null;
    this.bounds = [];
    this.center = null;
    this.timer = null;
    this.searchBox = null;
    this.events = {
        isZoomChanging: false,
        isManualMapType: this.options.isManualMapType
    };

    this.init = function () {
        if (typeof google === 'object') {
            this.show();
        } else {
            let that = this;
            $(document).on('google-loaded', function () {
                that.show();
            });
        }
    };

    this.show = function () {
        let latlng = new google.maps.LatLng(this.options.lat, this.options.lon);
        let mapOptions = {
            'zoom': this.options.zoom,
            'maxZoom': 21,
            'minZoom': 3,
            'center': latlng,
            gestureHandling: 'greedy',
            zoomControl: this.options.zoomControl,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.LEFT_TOP
            },
            streetViewControl: this.options.streetViewControl,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            scaleControl: this.options.scaleControl,
            mapTypeControlOptions: {
                mapTypeIds: [
                    google.maps.MapTypeId.ROADMAP,
                    google.maps.MapTypeId.SATELLITE
                ]
            },
            styles: []
        };

        this.gmap = new google.maps.Map(
            document.getElementById(this.options.mapCanvasId),
            mapOptions
        );

        let searchInput = document.getElementById(this.options.searchInputId);
        if (searchInput) {
            this.searchBox = new google.maps.places.SearchBox(searchInput);
            this.searchBox.addListener('places_changed', $.proxy(this.placesChanged, this));
            this.gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(searchInput);
            setTimeout(function () {
                searchInput.classList.remove("hidden");
                searchInput.classList.remove("d-none");
            }, 2000);
        }

        this.infoWindow = new google.maps.InfoWindow({
            maxWidth: 243
        });

        if (this.options.box) {
            let points = this.options.box.split(";");
            if (points.length === 2) {
                let ne = points[0].split(',');
                let sw = points[1].split(',');
                this.bounds = new google.maps.LatLngBounds(new google.maps.LatLng(sw[0], sw[1]), new google.maps.LatLng(ne[0], ne[1]));
                this.gmap.fitBounds(this.bounds);
            } else {
                this.bounds = new google.maps.LatLngBounds();
            }
        } else {
            this.bounds = new google.maps.LatLngBounds();
        }

        if (this.options.polygon) {
            this.processPolygon(this.options.polygon);
        }

        let that = this;
        $(document).on('mouseenter', '[data-hover=show-marker]', function () {
            let id = $(this).data('id'),
                type = $(this).data('type');
            if (id && type) {
                let marker = that.markers.find(function (value) {
                    return parseInt(value.id) === parseInt(id);
                });
                if (marker) {
                    that.test({id: id, type: type}, marker.position);
                }
            }
        });

        this.gmap.addListener('zoom_changed', $.proxy(this.zoomChange, this));
        this.gmap.addListener('maptypeid_changed', $.proxy(this.maptypeChanged, this));
        this.gmap.addListener('dragend', $.proxy(this.dragEnd, this));
    };

    this.dragEnd = function () {
        if (this.options.mode === 'advanced') {
            let box = this.getBox();
            let map = $('#' + this.options.mapCanvasId);
            map.data('box', box.neLat + ',' + box.neLng + ';' + box.swLat + ',' + box.swLng);
            map.data('zoom', this.gmap.getZoom());
            this.triggerEvent('mapDragEnd');
        }
    };

    this.placesChanged = function () {
        if (this.options.mode === 'advanced' && this.searchBox) {
            let places = this.searchBox.getPlaces();
            this.gmap.setCenter(places[0].geometry.location);
            this.gmap.setZoom(15);
        }
    };

    this.zoomChange = function () {
        if (this.events.isManualMapType) {
            return;
        }
        let zoom = this.gmap.getZoom(),
            zoomSatteliteValue = this.options.zoomSatteliteValue,
            mapType = '';
        this.events.isZoomChanging = true;
        if (zoom >= zoomSatteliteValue) {
            mapType = google.maps.MapTypeId.SATELLITE;
        } else {
            mapType = google.maps.MapTypeId.ROADMAP;
        }
        if (mapType !== this.gmap.getMapTypeId()) {
            this.gmap.setMapTypeId(mapType);
        }
        this.events.isZoomChanging = false;

        if (this.options.mode === 'advanced') {
            if (this.markersFetched === true) {
                let box = this.getBox();
                let map = $('#' + this.options.mapCanvasId);
                map.data('box', box.neLat + ',' + box.neLng + ';' + box.swLat + ',' + box.swLng);
                map.data('zoom', zoom);
                this.triggerEvent('mapZoomChanged');
            }
        }
    };

    this.maptypeChanged = function () {
        if (!this.events.isZoomChanging) {
            this.events.isManualMapType = true;
        }
    };

    this.parseMarkers = function () {
        let markers = [];

        $(this.options.markerListSelector).each(function () {
            let data = $(this).data('property');
            data = window.atob(data);
            data = JSON.parse(data);
            markers.push({
                lat: data.lat,
                lng: data.lng,
                id: data.id,
                type: data.type
            });
        });

        this.processMarkers(markers);
    };

    this.fetchMarkers = function (url, params) {
        let that = this;
        $.get(url, params, function (result) {
            if (result.success === true) {
                that.processMarkers(result.markers);
                window.setTimeout(function () {
                    that.markersFetched = true;
                }, 1000);
            }
        });
    };

    this.setMarkers = function (markers) {
        this.processMarkers(markers, false);
    };

    this.setPolygon = function (points) {
        this.processPolygon(points);
    };

    this.processMarkers = function (markers, fitBounds = true) {
        let that = this;

        for (i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers = [];

        $(markers).each(function (key, element) {
            if (element.lat && element.lng) {
                let latLng = new google.maps.LatLng(element.lat, element.lng),
                    marker = null;

                element.type = element.type ? element.type : 'property';
                switch (element.type) {
                    case 'city':
                    case 'region':
                        let markerScaleCoef = 1,
                            markerFontScaleCoef = 1;

                        if (element.count < 30) {
                            markerScaleCoef = 0.6;
                            markerFontScaleCoef = 0.7;
                        }
                        if (element.count < 100 && element.count > 30) {
                            markerScaleCoef = 0.8;
                            markerFontScaleCoef = 0.8;
                        }

                        marker = new CustomMarker(
                            latLng,
                            that.gmap,
                            {
                                icon: '/images/gmap-marker.svg',
                                label: {
                                    title: element.title,
                                    content: element.content,
                                    color: '#fff',
                                    fontSize: 14,
                                    fontSizeCoef: markerFontScaleCoef
                                }
                            }
                        );

                        break;
                    default:
                        marker = new google.maps.Marker({
                            'position': latLng,
                            'icon': element.marker ? element.marker : that.options.markerIcons[element.type],
                            'id': element.id
                        });

                        fn = $.proxy(that.showMarker(element, latLng), that);
                        google.maps.event.addListener(marker, 'click', fn);
                }

                if (fitBounds === true) {
                    that.bounds.extend(marker.position);
                }
                that.markers.push(marker);
            }
        });

        if (fitBounds === true && markers.length > 0) {
            let that = this;
            this.gmap.fitBounds(this.bounds);

            let listener = google.maps.event.addListener(this.gmap, "idle", function () {
                if (that.gmap.getZoom() !== that.options.zoom && that.options.zoom !== DEFAULT_ZOOM) that.gmap.setZoom(that.options.zoom);
                google.maps.event.removeListener(listener);
            });
        }

        window.setTimeout($.proxy(this.showMarkers(), this), 0);
    };

    this.processPolygon = function (points) {
        if (this.polygon !== null) {
            this.polygon.setMap(null);
        }

        if (points && points.length > 0) {
            this.polygon = new google.maps.Polygon({
                paths: points,
                strokeColor: '#FF0000',
                strokeOpacity: 0.6,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.15
            });

            this.polygon.setMap(this.gmap);
        }
    };

    this.test = function (element, latLng) {
        let that = this;
        that.displayMarker(element, latLng);
    };

    this.showMarker = function (element, latLng) {
        let that = this;
        return function (e) {
            e.cancelBubble = false;
            e.returnValue = false;
            if (e.stopPropagation) {
                e.stopPropagation();
                e.preventDefault();
            }

            that.displayMarker(element, latLng);
        };
    };

    this.displayMarker = function (element, latLng) {
        let that = this;

        if (this.markerCache[element.id]) {
            that.preRenderInfoWindow(element, this.markerCache[element.id], latLng);
        } else {
            if (that.options.markerViewRoutes[element.type]) {
                $.get(that.options.markerViewRoutes[element.type], {id: element.id}, function (result) {
                    if (result.success === true) {
                        that.preRenderInfoWindow(element, result, latLng);
                        that.markerCache[element.id] = result;
                    }
                });
            }
        }
    };

    this.preRenderInfoWindow = function (element, markerInfo, latLng) {
        let infoHtml = null;

        switch (element.type) {
            case 'user':
                infoHtml = this.renderUserInfo(markerInfo);

                break;
            case 'building':
                infoHtml = this.renderBuildingInfo(markerInfo);

                break;
            default:
                infoHtml = this.renderPropertyInfo(markerInfo);
        }

        this.infoWindow.setContent(infoHtml);
        this.infoWindow.setPosition(latLng);
        this.infoWindow.open(this.gmap);
    };

    this.showMarkers = function () {
        if (this.markerClusterer) {
            this.markerClusterer.clearMarkers();
        }
        this.markerClusterer = new MarkerClusterer(this.gmap, this.markers, {
            gridSize: 35
        });
    };

    this.getBox = function () {
        let bounds = this.gmap.getBounds();
        let ne = bounds.getNorthEast();
        let sw = bounds.getSouthWest();
        return {
            neLat: ne.lat(),
            neLng: ne.lng(),
            swLat: sw.lat(),
            swLng: sw.lng()
        };
    };

    this.triggerEvent = function (event) {
        let map = $('#' + this.options.mapCanvasId);
        if (this.timer) {
            clearTimeout(this.timer); //cancel the previous timer.
            this.timer = null;
        }
        this.timer = setTimeout(function () {
            map.trigger(event);
        }, 500);
    };

    this.clearMarkerCache = function () {
        this.markerCache = {};
    };

    this.renderPropertyInfo = function (element) {
        return '<div>' +
            '<a class="flex d-flex" href="' + element.marker.url + '" data-action="load-modal-property">' +
            '<div class="map-marker-left">' +
            '<img src="' + element.marker.image + '" />' +
            '</div>' +
            '<div class="map-marker-right">' +
            '<div class="map-marker-data font-weight-bold">' + element.marker.price + '</div>' +
            (element.marker.propertyArea ? '<div class="map-marker-data font-weight-bold">' + element.marker.propertyArea + '</div>' : '') +
            (element.marker.rooms ? '<div class="map-marker-data font-weight-bold">' + element.marker.rooms + '</div>' : '') +
            (element.marker.bathrooms ? '<div class="map-marker-data font-weight-bold">' + element.marker.bathrooms + '</div>' : '') +
            '</div>' +
            '</a>' +
            '</div>';
    };

    this.renderBuildingInfo = function (element) {
        return '<div>' +
            '<a class="flex d-flex" href="' + element.marker.url + '" data-action="load-modal-property">' +
            '<div class="map-marker-left">' +
            '<img src="' + element.marker.image + '" />' +
            '</div>' +
            '<div class="map-marker-right">' +
            (element.marker.flatsForSale ? '<div class="map-marker-data font-weight-bold">' + element.marker.flatsForSale + '</div>' : '') +
            (element.marker.flatsForRent ? '<div class="map-marker-data font-weight-bold">' + element.marker.flatsForRent + '</div>' : '') +
            '</div>' +
            '</a>' +
            '</div>';
    };

    this.renderUserInfo = function (element) {
        return '<div>' +
            '<a class="flex" href="' + element.marker.url + '">' +
            '<div class="map-marker-left">' +
            '<img src="' + element.marker.image + '" />' +
            '</div>' +
            '<div class="map-marker-right">' +
            '<div class="map-marker-data">' + element.marker.name + '</div>' +
            '</div>' +
            '</a>' +
            '</div>';
    };
}