/**
 * Created by Devour on 28.08.2018.
 */

(function ($) {
    $.fn.PropertyModal = function (options) {
        var ModalLoader = function (options, modalId) {
            this.options = $.extend({
                selector: '[data-action=load-modal-property]',
                mainSlider: '[data-role=main-slider]',
                similarSlider: '[data-role=similar-slider]',
                sideContactFormSelector: '#side-contact-agent-form',
                bottomContactFormSelector: '#bottom-contact-agent-form',
                fancyCaptionContainer: '#fancy-caption-container',
                fancyTitle: '#fancy-title',

                additionalModalSelector: '#dynamic-modal',
                sendMessageSelector: '[data-action=send-message]',
                showContactsSelector: '[data-action=show-contacts]',
                offerPriceSelector: '[data-action=offer-price]',
                requestShowcaseSelector: '[data-action=request-showcase]',
                requestMortgageSelector: '[data-action=request-mortgage]'
            }, options);

            this.modal = $('#' + modalId);
            this.additionalModal = $(this.options.additionalModalSelector);
            this.meta = {
                location: window.location.href,
                title: $('title'),
                description: $('meta[name=description]'),
                keywords: $('meta[name=keywords]')
            };
            this.state = {state: true};

            this.attachHandlers();
        };

        ModalLoader.prototype = {
            attachHandlers: function () {
                let that = this;

                $(document).on('click', this.options.selector, function () {
                    that.modal.find('.modal-content').html('');

                    let isModalShown = that.modal.hasClass('in');
                    let url = $(this).attr('href');
                    that.sendRequest(url, 'get', {}, function (result) {
                        if (result.success === true) {
                            that.modal.find('.modal-content').html(result.html);
                            if (isModalShown === false) {
                                that.modal.modal('show');
                            } else {
                                that.modal.trigger('modal-data-updated');
                            }

                            history.pushState(that.state, result.seo.title, url);
                            that.updateMeta(result.seo);
                        }
                    });

                    return false;
                });

                $(document).on('submit', this.options.sideContactFormSelector + ',' + this.options.bottomContactFormSelector, function () {
                    let form = $(this);
                    that.sendRequest($(this).attr('action'), 'post', $(this).serialize(), function (result) {
                        if (result.success === true) {
                            // that.modal.modal('hide');
                            let name = form.find('[data-role=name-field]').val();
                            let email = form.find('[data-role=email-field]').val();
                            let url = form.data('addon');
                            if(url) {
                                that.sendRequest(form.data('addon'), 'get', {name: name, email: email}, function (result) {
                                    if (result.success === true) {
                                        that.additionalModal.find('.modal-content').html(result.html);
                                        that.additionalModal.modal('show');
                                    }
                                });
                            }
                        }
                    });

                    return false;
                });

                $(document).on('click', this.options.sendMessageSelector
                    + ',' + this.options.showContactsSelector
                    + ',' + this.options.offerPriceSelector
                    + ',' + this.options.requestMortgageSelector
                    + ',' + this.options.requestShowcaseSelector, function () {
                    if($(this).attr('data-action')==='show-contacts'){
                        that.additionalModal.find('.modal-dialog').addClass('modal-sm');
                    } else if (that.additionalModal.find('.modal-dialog').hasClass('modal-sm')){
                        that.additionalModal.find('.modal-dialog').removeClass('modal-sm');
                    }
                    //if(e.target.id==this.options.showContactsSelector) {alert('!');}
                    that.sendRequest($(this).attr('href'), 'get', {}, function (result) {
                        if (result.success === true) {
                            that.additionalModal.find('.modal-content').html(result.html);
                        }
                    });

                    return false;
                });

                this.modal.on('hide.bs.modal', function () {
                    history.pushState(that.state, that.meta.title, that.meta.location);
                    $(this).find('.modal-content').html('');
                }).on('shown.bs.modal', function () {
                    that.initPlugins();
                    $(this).css('z-index', 1050);
                    setTimeout(function () {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', 1049).addClass('modal-stack');
                    }, 0);
                }).on('modal-data-updated', function () {
                    that.initPlugins();
                });
            },
            initPlugins: function () {
                let that = this;
                var rtlSlider = false;
                if($('body').hasClass('rtl')) {
                    rtlSlider = true;
                }

                $(this.options.mainSlider).slick({
                    dots: false,
                    arrows: true,
                    autoplay: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    variableWidth: true,
                    rtl: rtlSlider
                });

                $(this.options.similarSlider).slick({
                    dots: false,
                    arrows: true,
                    autoplay: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    rtl: rtlSlider,
                    responsive: [{
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }]
                });

                $('[data-fancybox="gallery"]').fancybox({
                    baseClass: 'fancybox-advanced',
                    hash: false,
                    buttons: [
                        'title',
                        'close'
                    ],
                    infobar: true,
                    // caption : function(instance, item) {
                    //     return that.fancyCaption;
                    // },
                    beforeShow: function (instance, current) {
                        $('.fancybox-caption').append($(that.options.fancyCaptionContainer + '> div'));
                    },
                    beforeClose: function () {
                        $(that.options.fancyCaptionContainer).append($('.fancybox-caption > div'));
                    },
                    thumbs: {
                        autoStart: true,
                        axis: 'x'
                    },
                    mobile: {
                        infobar: false,
                        caption: false,
                        thumbs: false
                    },
                    btnTpl: {
                        title: '<p class="fancybox-button--title">' +
                        $(that.options.fancyTitle).text() +
                        '</p>',
                    },
                });

                $('.more-less').trigger('show');
            },

            sendRequest: function (address, method, data, callback) {
                $.ajax({
                    url: address,
                    type: method,
                    data: data,
                    success: function (result) {
                        if (callback) {
                            callback(result);
                        } else {

                        }
                    }
                });
            },

            updateMeta: function (meta) {
                if (meta.title) {
                    $(this.meta.title).html(meta.title);
                }

                if (meta.description) {
                    $(this.meta.description).attr('content', meta.description);
                }

                if (meta.keywords) {
                    $(this.meta.keywords).attr('content', meta.keywords);
                }
            }
        };

        return new ModalLoader(options, this.attr('id'));
    }
})(jQuery);