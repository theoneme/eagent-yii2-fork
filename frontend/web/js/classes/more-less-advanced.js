(function ($) {
    $.fn.MoreLessAdvanced = function (options) {
        var ExpanderAdvanced = function (options) {
            this.options = $.extend({
                containerSelector: '.more-less-container',
                itemSelector: '.item',
                arrowUp: '<i class="icon-up"></i>',
                arrowDown: '<i class="icon-down"></i>',
                withArrows: true,
                moreText: 'More&nbsp;',
                lessText: 'Less&nbsp;',
                itemsToShow: 1
            }, options);

            if (this.options.withArrows === true) {
                this.options.moreText += this.options.arrowDown;
                this.options.lessText += this.options.arrowUp;
            }

            this.attachHandlers();
        };

        ExpanderAdvanced.prototype = {
            attachHandlers: function () {
                let container = $(this.options.containerSelector),
                    items = container.find($(this.options.itemSelector));

                this.init(container, items);
            },

            init: function (container, items) {
                let that = this;

                if (items.length > this.options.itemsToShow) {
                    container.append('<a href="#" data-action="trigger-ml">' + that.options.moreText + '</a>');

                    container.find('[data-action=trigger-ml]').on('click', function () {
                        that.trigger(container, items);

                        return false;
                    });

                    this.hide(container, items);
                }
            },

            trigger: function (container, items) {
                if (container.data('expanded') === 0) {
                    this.show(container, items);
                    container.find('[data-action=trigger-ml]').text(this.options.lessText);
                } else {
                    this.hide(container, items);
                    container.find('[data-action=trigger-ml]').text(this.options.moreText);
                }
            },

            hide: function (container, items) {
                let that = this;

                $.each(items, function (index) {
                    if (index >= that.options.itemsToShow) {
                        $(this).addClass('hidden');
                    }
                });

                container.data('expanded', 0);
            },

            show: function (container, items) {
                let that = this;

                $.each(items, function (index) {
                    if (index >= that.options.itemsToShow) {
                        $(this).removeClass('hidden');
                    }
                });

                container.data('expanded', 1);
            }
        };

        return new ExpanderAdvanced(options);
    }
})(jQuery);