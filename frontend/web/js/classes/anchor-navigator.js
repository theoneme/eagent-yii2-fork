/**
 * Created by Devour on 28.08.2018.
 */

(function ($) {
    $.fn.AnchorNavigator = function (options) {
        var AnchorNavigatorLoader = function (options, containerId) {
            this.options = $.extend({
                selector: '[data-action=load-modal-report]',
                reportItemSelector: '[data-role=report-item]',
                reportFormSelector: '#report-form',
            }, options);

            this.container = $('#' + containerId);

            this.attachHandlers();
        };

        ReportModalLoader.prototype = {
            attachHandlers: function () {
                let that = this;

                $(document).on('click', this.options.selector, function () {
                    that.container.html('');

                    let reportItems = $(that.options.reportItemSelector).serialize();
                    let url = $(this).attr('href');
                    that.sendRequest(url, 'get', reportItems, function (result) {
                        if (result.success === true) {
                            that.container.html(result.html);
                            that.container.find('.modal').modal('show');
                        }
                    });

                    return false;
                });

                $(document).on('submit', this.options.reportFormSelector, function() {
                    that.sendRequest($(this).attr('action'), 'post', $(this).serialize(), function(result) {
                        if(result.success === true) {
                            that.container.find('.modal').modal('hide');
                        }
                    });

                    return false;
                });

                $(this.container).on('hide.bs.modal', function () {
                    $(this).find('.modal-content').html('');
                });
            },

            sendRequest: function (address, method, data, callback) {
                $.ajax({
                    url: address,
                    type: method,
                    data: data,
                    success: function (result) {
                        if (callback) {
                            callback(result);
                        } else {

                        }
                    }
                });
            },
        };

        return new AnchorNavigatorLoader(options, this.attr('id'));
    }
})(jQuery);