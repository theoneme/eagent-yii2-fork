function CustomMarker(latlng, map, args) {
    this.position = latlng;
    this.setMap(map);
    this.args = args;
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.onAdd = function () {
    let div = document.createElement('DIV'),
        panes = this.getPanes(),
        self = this;

    div.style.position = 'absolute';
    div.className = "google-map-marker";
    div.innerHTML = "<div class='google-map-marker-sub'><img src='" + this.args.icon + "'>" +
        "<div style='color:" + this.args.label.color + "; font-size:" + this.args.label.fontSize * this.args.label.fontSizeCoef + "px;padding-top: 5px;'>" + this.args.label.title + "</div>" +
        "<div style='color:" + this.args.label.color + "; font-size:" + Math.floor(this.args.label.fontSize * this.args.label.fontSizeCoef * 0.8) + "px; padding-bottom: 15px;'>" + this.args.label.content + "</div></div>";

    google.maps.event.addDomListener(div, "click", function () {
        self.getMap().setCenter(self.position);
        self.getMap().setZoom(11);
    });

    panes.overlayImage.appendChild(div);
    this.div = div;
};

CustomMarker.prototype.draw = function () {
    let overlayProjection = this.getProjection(),
        position = overlayProjection.fromLatLngToDivPixel(this.position),
        panes = this.getPanes();
    this.div.style.left = position.x + 'px';
    this.div.style.top = position.y + 'px';
};

CustomMarker.prototype.remove = function () {
    if (this.div) {
        this.div.parentNode.removeChild(this.div);
        this.div = null;
    }
};

CustomMarker.prototype.getPosition = function () {
    return this.position;
};