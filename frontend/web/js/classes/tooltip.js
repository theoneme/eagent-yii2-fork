/**
 * Created by Алёнка on 24.08.2018.
 */

'use strict';

function Tooltip(element){
        this.element = element;
        this.tModal = '.tooltip-modal';
        this.tdir = element.attr('data-position');
        this.twidth = element.attr('data-width');
}

Tooltip.prototype.open = function(){
    this.element.find(this.tModal).css('width',this.twidth);
    this.element.find(this.tModal).addClass('tooltip-'+this.tdir);
    this.element.find(this.tModal).toggleClass('open');
};

Tooltip.prototype.close = function(){
    this.element.find(this.tModal).css('width',this.twidth);
    this.element.find(this.tModal).addClass('tooltip-'+this.tdir);
    this.element.find(this.tModal).removeClass('open');
};

$(document.body).on('click', '.tooltip-ea', function () {
    var tooltip = new Tooltip($(this));
    tooltip.open();
});

$(document.body).on('click', '.tooltip-close', function () {
    var tooltip = new Tooltip($(this).closest('.tooltip-ea'));
    tooltip.close();
    return false;
});
