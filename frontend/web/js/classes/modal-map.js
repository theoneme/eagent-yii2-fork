function ModalMap(options) {
    const DEFAULT_ZOOM = 16;

    this.options = $.extend({
        lat: 33.347316,
        lon: -115.729235,
        zoom: DEFAULT_ZOOM,
        zoomControl: true,
        zoomSatteliteValue: 20,
        streetViewControl: true,
        isManualMapType: false,
        scaleControl: true,
        mapCanvasId: 'modal-map',

        markerIcons: {
            property: null
        },
    }, options);

    this.map = null;
    this.center = null;

    this.events = {
        isZoomChanging: false,
        isManualMapType: this.options.isManualMapType
    };

    this.init = function () {
        if (typeof google === 'object') {
            this.show();
        } else {
            let that = this;
            $(document).on('google-loaded', function () {
                that.show();
            });
        }
    };

    this.show = function () {
        let latlng = new google.maps.LatLng(this.options.lat, this.options.lon);
        let mapOptions = {
            'zoom': this.options.zoom,
            'maxZoom': 21,
            'minZoom': 3,
            'center': latlng,
            gestureHandling: 'greedy',
            zoomControl: this.options.zoomControl,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.LEFT_TOP
            },
            streetViewControl: this.options.streetViewControl,
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            scaleControl: this.options.scaleControl,
            mapTypeControlOptions: {
                mapTypeIds: [
                    google.maps.MapTypeId.ROADMAP,
                    google.maps.MapTypeId.SATELLITE
                ]
            },
            styles: []
        };

        this.gmap = new google.maps.Map(
            document.getElementById(this.options.mapCanvasId),
            mapOptions
        );
        this.gmap.addListener('maptypeid_changed', $.proxy(this.maptypeChanged, this));
    };

    this.setMarker = function () {
        let container = $('#' + this.options.mapCanvasId),
            lat = container.data('lat'),
            lng = container.data('lng'),
            type = container.data('type'),
            that = this,
            marker = new google.maps.Marker({
                'position': new google.maps.LatLng(lat, lng),
                'icon': this.options.markerIcons[type],
                'map': this.gmap
            });

        this.gmap.setCenter(new google.maps.LatLng(lat, lng));
    };
}