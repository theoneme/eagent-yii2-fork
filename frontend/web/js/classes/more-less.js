/**
 * Created by Devour on 30.08.2018.
 */

(function($) {
    $.fn.MoreLess = function(options) {
        var Expander = function(options) {
            this.options = $.extend({
                selector: '.more-less',
                arrowUp: '<i class="icon-up"></i>',
                arrowDown: '<i class="icon-down"></i>',
                withArrows: true,
                moreText: 'More&nbsp;',
                lessText: 'Less&nbsp;',
                heightAttribute: '[data-height]'
            }, options);

            if(this.options.withArrows === true) {
                this.options.moreText += this.options.arrowDown;
                this.options.lessText += this.options.arrowUp;
            }

            this.attachHandlers();
        };

        Expander.prototype = {
            attachHandlers: function() {
                let that = this;

                $(this.options.selector).on('show', function() {
                    let totalHeight = 0;
                    let border = $(this).attr('data-height') ? parseInt($(this).attr('data-height')) : 190;

                    totalHeight = $(this).siblings(".hide-content").children(':first').innerHeight();
                    if(totalHeight < border) {
                        $(this).hide();
                    } else {
                        $(this).siblings(".hide-content").css({height: border});
                        $(this).siblings(".hide-content").append('<div class="more-less-shadow"></div>');
                    }
                });

                $(this.options.selector).on('click', function() {
                    if ($(this).siblings(".hide-content").hasClass('hide-text')) {
                        $(this).html(that.options.lessText);
                        $(this).siblings(".hide-content").removeClass('hide-text').addClass('show-text');
                    } else {
                        $(this).html(that.options.moreText);
                        $(this).siblings(".hide-content").removeClass('show-text').addClass('hide-text');
                    }

                    return false;
                });
            },
        };

        return new Expander(options);
    }

})(jQuery);