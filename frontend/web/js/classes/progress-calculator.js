function ProgressCalculator(options) {
    this.options = $.extend({
        formSelector: null,
        okClass: 'good-mark',
        errorClass: 'progress-error',
        percentSelector: '.progress-percent',
        successMark: '✔',
        language: 'ru',
        texts: {
            en: {
                one: 'error',
                few: 'errors',
                many: 'errors'
            },
            ru: {
                one: 'ошибка',
                few: 'ошибки',
                many: 'ошибок'
            }
        }
    }, options);

    this.updateProgress = function () {
        let that = this;

        let sections = $(this.options.formSelector).find('section');
        let totalInputs = $(this.options.formSelector).find('.form-group:not(.form-group-disabled)').length;
        let totalSuccess = 0;

        if (sections.length > 0) {
            $.each(sections, function (key, value) {
                let sectionId = $(this).attr('id');

                let inputs = $(this).find('.form-group:not(.form-group-disabled)');
                let errors = 0;
                $.each(inputs, function (k, v) {
                    if ($(this).hasClass('has-error')) {
                        errors++;
                    } else if ($(this).hasClass('has-success')) {
                        totalSuccess++;
                    }
                });
                let progressBlock = $('a[href="#' + sectionId + '"]');
                let progressMessageBlock = progressBlock.find('div');

                if (errors > 0) {
                    progressMessageBlock.attr('class', that.options.errorClass);
                    progressMessageBlock.text(that.getPluralMessage(errors));
                } else {
                    progressMessageBlock.attr('class', that.options.okClass);
                    progressMessageBlock.text(that.options.successMark);
                }
                // console.log(sectionId, errors);
            });

            let progress = parseInt((totalSuccess / totalInputs) * 100);
            $(this.options.percentSelector).text(progress);
        }
    };

    this.getPluralMessage = function (count) {
        let texts = this.options.texts[this.options.language];

        if (count === 1) {
            return count + ' ' + texts.one;
        }

        if (count > 1 && count < 5) {
            return count + ' ' + texts.few;
        }

        if (count >= 5) {
            return count + ' ' + texts.many;
        }
    };
}