/**
 * Created by Devour on 27.08.2018.
 */

function MortgageCalculator(options) {
    this.options = $.extend({
        selector: '#mortgages',
        currency: 'USD',
        locale: 'en-GB',
        fields: {
            price: '[data-role=mortgage-price]',
            downPayment: '[data-role=mortgage-down-payment]',
            downPaymentPercent: '[data-role=mortgage-down-payment-percent]',
            loanTerm: '[data-role=mortgage-loan-term]',
            interestRate: '[data-role=mortgage-interest-rate]',
            insurance: '[data-role=mortgage-property-insurance]',
        },
        labels: {
            result: '[data-role=mortgage-payment]'
        }
    }, options);

    this.registerHandlers = function() {
        let that = this;
        let fields = that.options.fields;

        $(this.options.selector + ' input, ' + this.options.selector + ' select').on('change', function() {
            let role = $(this).data('role'),
                downPaymentPercent = that.getFieldValue(that.getSelector(fields.downPaymentPercent)),
                price = that.getFieldValue(that.getSelector(fields.price)),
                downPayment = that.getFieldValue(that.getSelector(fields.downPayment));

            switch(role) {
                case 'mortgage-price':
                    downPayment = price * (downPaymentPercent / 100);
                    $(that.getSelector(fields.downPayment)).val(downPayment.toFixed());
                    break;

                case 'mortgage-down-payment-percent':
                    downPaymentPercent = that.getFieldValue(that.getSelector(fields.downPaymentPercent));
                    price = that.getFieldValue(that.getSelector(fields.price));

                    downPayment = price * (downPaymentPercent / 100);
                    $(that.getSelector(fields.downPayment)).val(downPayment.toFixed());
                    break;
            }

            that.calculate();
        });
    };

    this.calculate = function () {
        let fields = this.options.fields;
        let price = this.getFieldValue(this.getSelector(fields.price)),
            downPayment = this.getFieldValue(this.getSelector(fields.downPayment)),
            loanTerm = this.getFieldValue(this.getSelector(fields.loanTerm)),
            insurance = $(this.getSelector(fields.insurance)).is(':checked'),
            interestRate = this.getFieldValue(this.getSelector(fields.interestRate));

        if(insurance === true) {
            interestRate += 1.5;
        }
        let totalForMortgage = price - downPayment;
        let monthlyRate = (interestRate / 12) / 100;
        let multiplier = (monthlyRate * (Math.pow(1 + monthlyRate, loanTerm * 12))) / (Math.pow(1 + monthlyRate, loanTerm * 12) - 1);
        let payment = (multiplier * totalForMortgage);
        if(typeof Intl !== 'undefined') {
            let formatter = new Intl.NumberFormat(this.options.locale, {
                style: 'currency',
                currency: this.options.currency,
                maximumFractionDigits: 0,
                minimumFractionDigits: 0
            });

            $(this.getSelector(this.options.labels.result)).html(formatter.format(payment));
        } else {
            $(this.getSelector(this.options.labels.result)).html(parseInt(payment) + ' ' + this.options.currency);
        }
    };

    this.getFieldValue = function(selector) {
        return parseInt($(selector).val().replace(/\D+/g, ""))
    };

    this.getSelector = function(selector) {
        return this.options.selector + ' ' + selector;
    };
}
