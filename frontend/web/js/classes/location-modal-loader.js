/**
 * Created by Devour on 28.08.2018.
 */

(function ($) {
    $.fn.LocationModal = function (options) {
        var LocationModalLoader = function (options, modalId) {
            this.options = $.extend({
                countryContainerSelector: '[data-role=country-container]',
                regionContainerSelector: '[data-role=region-container]',
                bigCityContainerSelector: '[data-role=big-city-container]',
                cityContainerSelector: '[data-role=city-container]',
                countryItemSelector: '[data-role=country-item]',
                countrySpecialItemSelector: '[data-role=country-special-item]',
                regionItemSelector: '[data-role=region-item]',
                cityItemSelector: '[data-role=city-item]',
                countryUrl: null,
                regionUrl: null,
                bigCityUrl: null,
                cityUrl: null,
                setLocationUrl: null
            }, options);

            this.modal = $('#' + modalId);

            this.attachHandlers();
        };

        LocationModalLoader.prototype = {
            attachHandlers: function () {
                let that = this;

                $(that.modal).on('shown.bs.modal', function () {
                    that.sendRequest(that.options.countryUrl, 'get', {}, function (result) {
                        if (result.success === true) {
                            $(that.options.countryContainerSelector).html(result.html);

                            that.scrollBlocksToActiveElement($(that.options.countryContainerSelector));
                        }
                    });

                    that.sendRequest(that.options.regionUrl, 'get', {}, function (result) {
                        if (result.success === true) {
                            $(that.options.regionContainerSelector).html(result.html);

                            that.scrollBlocksToActiveElement($(that.options.regionContainerSelector));
                        }
                    });

                    that.sendRequest(that.options.bigCityUrl, 'get', {}, function (result) {
                        if (result.success === true) {
                            $(that.options.bigCityContainerSelector).html(result.html);

                            that.scrollBlocksToActiveElement($(that.options.bigCityContainerSelector));
                        }
                    });

                    that.sendRequest(that.options.cityUrl, 'get', {}, function (result) {
                        if (result.success === true) {
                            $(that.options.cityContainerSelector).html(result.html);

                            that.scrollBlocksToActiveElement($(that.options.cityContainerSelector));
                        }
                    });
                });

                $(document).on('click', this.options.countryItemSelector, function () {
                    let clicked = $(this);
                    clicked.closest(that.options.countryContainerSelector).find('a').removeClass('active');
                    clicked.closest('.tab-pane').find(that.options.cityContainerSelector).html('');
                    clicked.addClass('active');
                    that.sendRequest(that.options.regionUrl, 'get', {country_id: clicked.attr('data-id')}, function (result) {
                        if (result.success === true) {
                            let container = clicked.closest('.tab-pane').find(that.options.regionContainerSelector);
                            container.html(result.html);

                            if (container.find('a.active').length > 0) {
                                container.find('a.active').click()
                            } else {
                                if (container.find('a').length > 0) {
                                    container.find('a')[0].click();
                                }
                            }

                            that.scrollBlockToActiveElement(container);
                        }
                    });

                    return false;
                });

                $(document).on('click', this.options.regionItemSelector, function () {
                    let clicked = $(this);
                    clicked.closest(that.options.regionContainerSelector).find('a').removeClass('active');
                    clicked.addClass('active');
                    that.sendRequest(that.options.cityUrl, 'get', {region_id: clicked.attr('data-id')}, function (result) {
                        if (result.success === true) {
                            let container = clicked.closest('.tab-pane').find(that.options.cityContainerSelector);
                            container.html(result.html);

                            that.scrollBlockToActiveElement(container);
                        }
                    });

                    return false;
                });

                $(document).on('click', this.options.countrySpecialItemSelector, function () {
                    let clicked = $(this);
                    clicked.closest(that.options.cityContainerSelector).find('a').removeClass('active');
                    clicked.addClass('active');
                    that.sendRequest(that.options.setLocationUrl, 'post', {location: clicked.attr('data-slug')});

                    return false;
                });

                $(document).on('click', this.options.cityItemSelector, function () {
                    let clicked = $(this);
                    clicked.closest(that.options.cityContainerSelector).find('a').removeClass('active');
                    clicked.addClass('active');
                    that.sendRequest(that.options.setLocationUrl, 'post', {location: clicked.attr('data-slug')});

                    return false;
                });
            },

            sendRequest: function (address, method, data, callback) {
                $.ajax({
                    url: address,
                    type: method,
                    data: data,
                    success: function (result) {
                        if (callback) {
                            callback(result);
                        } else {

                        }
                    }
                });
            },

            scrollBlocksToActiveElement: function(blocks) {
                let that = this;

                $.each(blocks, function() {
                    that.scrollBlockToActiveElement($(this));
                });
            },

            scrollBlockToActiveElement: function (block) {
                console.log(block);
                let activeElement = block.find('a.active');
                if (activeElement.length > 0) {
                    let blockOffset = block.offset().top;
                    let offset = activeElement.offset().top;

                    block.scrollTop(offset - blockOffset - 50);
                }
            }
        };

        return new LocationModalLoader(options, this.attr('id'));
    }
})(jQuery);