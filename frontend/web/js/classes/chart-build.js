/**
 * Created by Алёнка on 17.09.2018.
 */
function ChartCircle(options) {
    this.chartColors = {
        PandI: 'rgb(0, 116, 228)',
        Insurance: 'rgb(98, 174, 247)',
        Taxes: 'rgb(51, 144, 233)',
        PMI: 'rgb(27, 80, 152)'
    };
    this.dataSum = {
        sumPandI: 0,
        sumInsurance: 0,
        sumTaxes: 0,
        sumPMI: 0,
    };
    this.dataPercent = {
        PandI: 0,
        Insurance: 0,
        Taxes: 0,
        PMI: 0,
    };
    this.options =  $.extend({
        currency: 'р.',
        labels: {

        },
        selectors: {
            price: '#mortgage-property-price',
            includeTaxes: '#mortgage-include-taxes',
        }
    }, options);
    this.sum = 0;
    this.config = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    this.dataPercent.PandI,
                    this.dataPercent.Insurance,
                    this.dataPercent.Taxes,
                    this.dataPercent.PMI,
                ],
                backgroundColor: [
                    this.chartColors.PandI,
                    this.chartColors.Insurance,
                    this.chartColors.Taxes,
                    this.chartColors.PMI,
                ]
            }],
            labels: [
                'P&I: ' + Math.round(this.dataSum.PandI) + this.options.currency,
                'Insurance' + Math.round(this.dataSum.Insurance) + this.options.currency,
                'Taxes' + Math.round(this.dataSum.Taxes) + this.options.currency,
                'PMI' + Math.round(this.dataSum.PMI) + this.options.currency,
            ]
        },
        options: {
            responsive: false,
            legend: {
                position: 'left',
                fontColor: '#3e414e',
                fontSize: '16',
                labels: {
                    usePointStyle: true
                }
            },
            padding: {
                left: 50,
                right: 0,
                top: 0,
                bottom: 0
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                enabled: false
            },
            cutoutPercentage: 80
        }
    };
}

ChartCircle.prototype.configCircle = function (chart) {
    let priceHome = parseInt($(this.options.selectors.price).val().replace(/\D+/g, ""));
    this.dataSum.PandI = priceHome * 0.005; // не поняла откуда, но близко к полпроценту от суммы
    if ($(this.options.selectors.includeTaxes).prop("checked")) {
        this.dataSum.Insurance = parseInt($('#homeInsurance').val().replace(/\D+/g, "")) / 12;
        this.dataSum.Taxes = parseInt($('#tax').val().replace(/\D+/g, "")) / 12;
    } else {
        this.dataSum.Insurance = 0;
        this.dataSum.Taxes = 0;
    }
    if ($('#includePMI').prop("checked")) {
        this.dataSum.PMI = 4000; // я хз откуда оно берётся, поэтому пока рандомное число
    } else {
        this.dataSum.PMI = 0;
    }

    this.sum = Math.round(this.dataSum.PandI + this.dataSum.Insurance + this.dataSum.Taxes + this.dataSum.PMI);
    console.log(this.dataSum);
    $('#paySum').html(this.sum + this.options.currency);
    this.dataPercent.HOA = Math.round(this.dataSum.HOA / (this.sum / 100));
    this.dataPercent.Insurance = Math.round(this.dataSum.Insurance / (this.sum / 100));
    this.dataPercent.Taxes = Math.round(this.dataSum.Taxes / (this.sum / 100));
    this.dataPercent.PMI = Math.round(this.dataSum.PMI / (this.sum / 100));
    this.dataPercent.PandI = 100 - this.dataPercent.HOA - this.dataPercent.Insurance - this.dataPercent.Taxes - this.dataPercent.PMI;
    chart.data.datasets[0].data[0] = this.dataPercent.PandI;
    chart.data.datasets[0].data[1] = this.dataPercent.Insurance;
    chart.data.datasets[0].data[2] = this.dataPercent.Taxes;
    chart.data.datasets[0].data[3] = this.dataPercent.PMI;
    chart.data.labels[0] = 'P&I: ' + Math.round(this.dataSum.PandI) + this.options.currency;
    chart.data.labels[1] = 'Insurance: ' + Math.round(this.dataSum.Insurance) + this.options.currency;
    chart.data.labels[2] = 'Taxes: ' + Math.round(this.dataSum.Taxes) + this.options.currency;
    chart.data.labels[3] = 'PMI: ' + Math.round(this.dataSum.PMI) + this.options.currency;
    chart.update();
};

ChartCircle.prototype.getConfig = function () {
    return this.config;
};