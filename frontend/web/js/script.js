const
    body = $('body'),
    footer = $('footer'),
    header = $('header'),
    mobile_menu = $('.mobile-menu'),
    all_content = $('.all-content');

$(document.body).on('click', '.trigger-menu, #menu', function () {
    if (!mobile_menu.hasClass('open')) {
        mobile_menu.addClass('open');
        $('#menu').addClass('open');
        header.addClass('content-open');
        footer.addClass('content-open');
        all_content.addClass('content-open');
        $('html,body').css('overflow-x', 'hidden');
    }
}).on('click', '.content-open, #menu.open', function () {
    mobile_menu.removeClass('open');
    $('#menu').removeClass('open');
    header.removeClass('content-open');
    footer.removeClass('content-open');
    all_content.removeClass('content-open');
    body.css('overflow-y', 'auto');
    $('html,body').css('overflow-x', 'auto');
}).on('click', '.close-mobile', function () {
    mobile_menu.removeClass('open');
    header.removeClass('content-open');
    all_content.removeClass('content-open');
    body.css('overflow-y', 'auto');
    $('html,body').css('overflow-x', 'auto');
}).on('click', '.form-submit', function () {
    $(this).closest('form').submit();
    return false;
}).on('submit', 'form', function () {
    $(this).submit(function () {
        return false;
    });

    return true;
});

function googleLoad() {
    $(document).trigger('google-loaded');
}