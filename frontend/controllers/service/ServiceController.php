<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\services\entities\ServiceService;
use frontend\services\LocationService;
use frontend\widgets\ServiceFilter;
use Yii;
use yii\base\Module;

/**
 * Class ServiceController
 * @package frontend\controllers
 */
class ServiceController extends FrontEndController
{
    /**
     * @var ServiceService
     */
    private $_serviceService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * ServiceController constructor.
     * @param $id
     * @param Module $module
     * @param ServiceService $serviceService
     * @param LocationService $locationService
     * @param array $config
     */
    public function __construct($id, Module $module, ServiceService $serviceService, LocationService $locationService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_serviceService = $serviceService;
        $this->_locationService = $locationService;

        $this->layout = 'catalog';
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCatalog()
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        $params = Yii::$app->request->queryParams;

        $services = $this->_serviceService->getMany($params, [
            'pagination' => true,
            'perPage' => 36,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $filter = new ServiceFilter(['catalogView' => 'grid', 'queryParams' => $params, 'locationData' => $locationData]);

        return $this->render('catalog', [
            'services' => $services,
            'filter' => $filter
        ]);
    }
}
