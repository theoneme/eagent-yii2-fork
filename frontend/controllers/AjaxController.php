<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\mappers\SelectizeAttributeValuesMapper;
use common\services\entities\AttributeValueService;
use common\services\entities\CategoryService;
use frontend\forms\StepZeroForm;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers
 */
class AjaxController extends FrontEndController
{
    /**
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param CategoryService $categoryService
     * @param AttributeValueService $attributeValueService
     * @param array $config
     */
    public function __construct(string $id, Module $module, CategoryService $categoryService, AttributeValueService $attributeValueService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_categoryService = $categoryService;
        $this->_attributeValueService = $attributeValueService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['attribute-values', 'render-category-input'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['attribute-values', 'render-category-input'],
            ]
        ];
    }


    /**
     * @param $attribute
     * @param $query
     * @return array
     */
    public function actionAttributeValues($attribute, $query)
    {
        $values = $this->_attributeValueService->getMany(['title' => $query, 'attribute_id' => $attribute]);
        if (!empty($values['items'])) {
            return SelectizeAttributeValuesMapper::getMappedData($values['items']);
        }

        return [];
    }

    /**
     * @param $index
     * @param $parent
     * @return array
     */
    public function actionRenderCategoryInput($index, $parent)
    {
        $categories = $this->_categoryService->getMany(['parent_id' => $parent], ['orderBy' => 'lft']);
        return [
            'html' => $this->renderAjax('@frontend/views/common/category-input', [
                'model' => new StepZeroForm(),
                'index' => $index,
                'items' => ArrayHelper::map($categories['items'], 'id', 'title'),
                'field' => 'category_id',
                'createForm' => true
            ]),
            'success' => true,
        ];
    }
}
