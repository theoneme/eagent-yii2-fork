<?php
namespace frontend\controllers\user;

use common\controllers\FrontEndController;
use common\helpers\Auth;
use common\models\CompanyMember;
use common\models\User;
use common\services\entities\PaymentMethodService;
use common\services\entities\UserWalletService;
use frontend\forms\CheckoutForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class CheckoutController
 * @package frontend\controllers\user
 */
class CheckoutController extends FrontEndController
{
    /**
     * @var UserWalletService
     */
    private $_userWalletService;
    /**
     * @var PaymentMethodService
     */
    private $_paymentMethodService;

    /**
     * WalletController constructor.
     * @param string $id
     * @param Module $module
     * @param UserWalletService $userWalletService
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserWalletService $userWalletService, PaymentMethodService $paymentMethodService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userWalletService = $userWalletService;
        $this->_paymentMethodService = $paymentMethodService;
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // Пускаем простых юзеров
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return empty(Auth::user()->company_user_id);
                        },
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    //Правила для операций с кошельками
                    [
                        'allow' => false,
                        'actions' => ['checkout', 'process'],
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('createAsCompany');
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionCheckout()
    {
        $form = new CheckoutForm();
        $form->load(Yii::$app->request->post());
        if ($form->validate()) {
            $paymentMethods = $this->_paymentMethodService->getMany(['enabled' => 1], ['orderBy' => ['sort' => SORT_ASC]]);
            return $this->render('replenish', [
                'type' => $form->type,
                'currency_code' => $form->currency_code,
                'total' => $form->getTotal(),
                'paymentMethods' => $paymentMethods['items'],
            ]);
        }
        else {
            Yii::$app->session->setFlash('error', Yii::t('main', 'Please set the amount of replenishment'));
            return $this->redirect(['/user/wallet/index']);
        }
    }
}
