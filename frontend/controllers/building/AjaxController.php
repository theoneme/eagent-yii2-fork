<?php

namespace frontend\controllers\building;

use common\controllers\FrontEndController;
use common\mappers\forms\BuildingToPropertyFormMapper;
use common\services\entities\BuildingService;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers\building
 */
class AjaxController extends FrontEndController
{
    /**
     * @var BuildingService
     */
    private $_buildingService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param BuildingService $buildingService
     * @param array $config
     */
    public function __construct(string $id, Module $module, BuildingService $buildingService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingService = $buildingService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['building'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['building'],
            ]
        ];
    }

    /**
     * @param $lat
     * @param $lng
     * @return array
     */
    public function actionBuilding($lat, $lng)
    {
        $lat = round($lat, 8);
        $lng = round($lng, 8);
        $building = $this->_buildingService->getOne(['building.lat' => $lat, 'building.lng' => $lng]);
        if ($building !== null) {
            $buildingData = BuildingToPropertyFormMapper::getMappedData($building);
            return [
                'success' => true,
                'data' => $buildingData
            ];
        }

        return ['success' => false];
    }
}
