<?php

namespace frontend\controllers\building;

use common\assets\GoogleAsset;
use common\controllers\FrontEndController;
use common\services\MetaService;
use frontend\assets\CatalogAsset;
use frontend\assets\CommonAsset;
use frontend\assets\FilterAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use frontend\helpers\ViewToTemplateHelper;
use frontend\services\catalog\BuildingCatalogDataService;
use frontend\services\catalog\BuildingCatalogUrlService;
use frontend\widgets\BuildingFilter;
use Yii;
use yii\base\Module;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;
use yii\web\JqueryAsset;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\controllers\building
 */
class CatalogController extends FrontEndController
{
    /**
     * @var BuildingCatalogUrlService
     */
    private $_buildingCatalogUrlService;
    /**
     * @var BuildingCatalogDataService
     */
    private $_buildingCatalogDataService;

    /**
     * CatalogController constructor.
     * @param $id
     * @param Module $module
     * @param BuildingCatalogUrlService $buildingCatalogUrlService
     * @param BuildingCatalogDataService $buildingCatalogDataService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                BuildingCatalogUrlService $buildingCatalogUrlService,
                                BuildingCatalogDataService $buildingCatalogDataService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingCatalogUrlService = $buildingCatalogUrlService;
        $this->_buildingCatalogDataService = $buildingCatalogDataService;

        $this->layout = 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
            ]
        ];
    }

    /**
     * @return array|string
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;

        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $template = ViewToTemplateHelper::getTemplate($cookieView);
        if ($cookieView !== 'map' && array_key_exists('box', $queryParams) && array_key_exists('zoom', $queryParams)) {
            $template = 'index-map';
            $this->actionView('map');
        }

        $catalogData = $this->_buildingCatalogDataService->getData($queryParams, $cookieView, BuildingCatalogDataService::MODE_INDEX);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->assetManager->bundles = [
                BootstrapPluginAsset::class => false,
                BootstrapAsset::class => false,
                JqueryAsset::class => false,
                CommonAsset::class => false,
                GoogleAsset::class => false,
                FilterAsset::class => false,
                CatalogAsset::class => false,
                AutoCompleteAsset::class => false
            ];

            return [
                'html' => $this->renderAjax($template, $catalogData),
                'success' => true,
                'seo' => $catalogData['seo'],
            ];
        }

        $metaService = new MetaService($this);
        $metaService->registerMeta($catalogData['seo']);

        return $this->render($template, $catalogData);
    }

    /**
     * @return array
     */
    public function actionIndexAjax()
    {
        $queryParams = Yii::$app->request->queryParams;
        $seoUrl = $this->_buildingCatalogUrlService->getUrl($queryParams);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $catalogData = $this->_buildingCatalogDataService->getData($queryParams, $cookieView, BuildingCatalogDataService::MODE_INDEX_AJAX);
        $markers = ArrayHelper::remove($catalogData, 'markers');
        $seo = $catalogData['seo'];
        /** @var BuildingFilter $filter */
        $filter = ArrayHelper::remove($catalogData, 'buildingFilter');

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'seo' => $seo,
            'url' => $seoUrl,
            'domain' => array_key_exists('city', $queryParams),
            'markers' => $markers['items'],
            'polygon' => $catalogData['polygon'],
            'filter' => $filter->run(),
            'success' => true
        ];
    }

    /**
     * @return array
     */
    public function actionIndexAjaxShort()
    {
        $queryParams = Yii::$app->request->queryParams;
        $seoUrl = $this->_buildingCatalogUrlService->getUrl($queryParams);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $catalogData = $this->_buildingCatalogDataService->getData($queryParams, $cookieView, BuildingCatalogDataService::MODE_INDEX);

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'url' => $seoUrl,
            'success' => true,
        ];
    }

    /**
     * @param $view
     * @return array
     */
    public function actionView($view)
    {
        if (!in_array($view, ['grid', 'list', 'map'])) {
            return ['success' => false];
        }

        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'catalog-view',
            'value' => $view,
            'domain' => '.' . Yii::$app->params['domainName'],
            'expire' => time() + 3600 * 6,
        ]));

        return ['success' => true];
    }
}
