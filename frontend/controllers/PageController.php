<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\models\Translation;
use common\services\entities\PageService;
use Yii;
use yii\base\Module;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * @package frontend\controllers
 */
class PageController extends FrontEndController
{
    /**
     * @var PageService
     */
    private $_pageService;

    /**
     * PageController constructor.
     * @param string $id
     * @param Module $module
     * @param PageService $pageService
     * @param array $config
     */
    public function __construct(string $id, Module $module, PageService $pageService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_pageService = $pageService;
    }

    /**
     * @param $slug
     * @return string
     */
    public function actionView($slug)
    {
        $page = $this->_pageService->getOne(['page_translations.value' => $slug, 'page_translations.key' => Translation::KEY_SLUG]);

        if ($page === null) {
            throw new NotFoundHttpException(Yii::t('main', 'Page not found'));
        }

        $this->view->title = $page['title'];

        return $this->render('view', [
            'page' => $page
        ]);
    }

    /**
     * @return string
     */
    public function actionAgent()
    {
        $this->layout = 'catalog';

        return $this->render('agent-list');
    }

    /**
     * @return string
     */
    public function actionAgentProfile()
    {
        $this->layout = 'catalog';

        return $this->render('agent-profile');
    }

    /**
     * @return string
     */
    public function actionTariff()
    {
        $this->layout = 'catalog';

        return $this->render('tariff');
    }

    /**
     * @return string
     */
    public function actionHomesList()
    {
        $this->layout = 'catalog';

        return $this->render('list-homes');
    }

    /**
     * @return string
     */
    public function actionFormHtml()
    {
        $this->layout = 'main';

        return $this->render('form-steps-html');
    }

    /**
     * @return string
     */
    public function actionNewsFeed()
    {
        $this->layout = 'catalog';

        return $this->render('news-feed');
    }


    /**
     * @return string
     */
    public function actionArticle()
    {
        $this->layout = 'catalog';

        return $this->render('article');
    }

    /**
     * @return string
     */
    public function actionForum()
    {
        $this->layout = 'catalog';

        return $this->render('forum');
    }

    /**
     * @return string
     */
    public function actionForumQuestion()
    {
        $this->layout = 'catalog';

        return $this->render('forum-question');
    }

    /**
     * @return string
     */
    public function actionNewConstructions()
    {
        return $this->redirect('/landing/landing/new-constructions');
    }

    /**
     * @return string
     */
    public function actionWallet()
    {
        $this->layout = 'catalog';
        return $this->render('wallet');
    }
}
