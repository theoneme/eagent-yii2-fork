<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\mappers\forms\AgentFormToMessageMapper;
use common\mappers\forms\ContactFormToMessageMapper;
use common\services\entities\UserService;
use common\services\NotificationService;
use frontend\forms\contact\ContactAgentForm;
use frontend\forms\contact\ContactForm;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ContactController
 * @package frontend\controllers
 */
class ContactController extends FrontEndController
{
    /**
     * @var NotificationService
     */
    private $_notificationService;

    /**
     * ContactController constructor.
     * @param string $id
     * @param Module $module
     * @param NotificationService $notificationService
     * @param array $config
     */
    public function __construct(string $id, Module $module, NotificationService $notificationService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_notificationService = $notificationService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['contact-agent', 'contact-us'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['contact-agent', 'contact-us'],
            ]
        ];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionContactAgent()
    {
        $contactAgentForm = new ContactAgentForm();
        $input = Yii::$app->request->post();
        $contactAgentForm->load($input);

        if (array_key_exists('ajax', $input)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($contactAgentForm);
        }

        if ($contactAgentForm->validate()) {
            /** @var UserService $userService */
            $userService = Yii::$container->get(UserService::class);
            $agent = $userService->getOne(['user.id' => $contactAgentForm['agentId']]);
            $params = AgentFormToMessageMapper::getMappedData([
                'attributes' => $contactAgentForm->attributes,
                'agent' => $agent
            ]);

            return [
                'success' => $this->_notificationService->sendNotification('email', $params)
            ];
        }

        return ['success' => false];
    }


    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionContactUs()
    {
        $form = new ContactForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        if (array_key_exists('ajax', $input)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($form);
        }

        if ($form->validate()) {
            $params = ContactFormToMessageMapper::getMappedData([
                'attributes' => $form->attributes,
            ]);
            $this->_notificationService->sendNotification('email', $params);

            return [
                'success' => true,
            ];
        }

        return ['success' => false];
    }
}
