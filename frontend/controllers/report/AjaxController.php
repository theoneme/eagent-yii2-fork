<?php

namespace frontend\controllers\report;

use common\controllers\FrontEndController;
use common\models\Property;
use common\services\entities\PropertyService;
use common\services\entities\ReportService;
use frontend\forms\CompareReportForm;
use frontend\forms\PreCompareReportForm;
use frontend\forms\ReportForm;
use frontend\services\CompareReportService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AjaxController
 * @package frontend\controllers\report
 */
class AjaxController extends FrontEndController
{
    /**
     * @var ReportService
     */
    private $_propertyService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param PropertyService $propertyService
     * @param array $config
     */
    public function __construct(string $id, Module $module, PropertyService $propertyService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_propertyService = $propertyService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['render', 'render-compare', 'pre-render-compare'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['render', 'render-compare', 'pre-render-compare'],
            ],
            'access' => [
                'class' => AccessControl::class,
                'only' => ['render', 'render-compare', 'pre-render-compare'],
                'rules' => [
                    [
                        'actions' => ['render', 'render-compare', 'pre-render-compare'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionRender()
    {
        $properties = Yii::$app->request->get('id');

        if ($properties === null) {
            return [
                'success' => false
            ];
        }

        $properties = $this->_propertyService->getMany(['id' => $properties]);
        $reportForm = new ReportForm([
            'id' => array_column($properties['items'], 'id')
        ]);

        return [
            'html' => $this->renderAjax('report', [
                'reportForm' => $reportForm,
                'properties' => $properties
            ]),
            'success' => true,
        ];
    }

    /**
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionRenderCompare()
    {
        $input = Yii::$app->request->post();
        $reportForm = new CompareReportForm();

        if (!empty($input['PreCompareReportForm'])) {
            $reportForm->load($input['PreCompareReportForm'], '');

            if (!empty($reportForm['id'])) {
                $properties = $this->_propertyService->getMany(['id' => $reportForm['id']]);

                /** @var CompareReportService $compareReportService */
                $compareReportService = Yii::$container->get(CompareReportService::class);
                $reportForm->message = $compareReportService->getMessage($properties, $reportForm->attributes);

                return [
                    'html' => $this->renderAjax('compare-report', [
                        'reportForm' => $reportForm,
                        'properties' => $properties
                    ]),
                    'success' => true,
                ];
            }
        }

        return ['success' => false];
    }

    /**
     * @return array|Response
     */
    public function actionPreRenderCompare()
    {
        $reportForm = new PreCompareReportForm();

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $reportForm->load($input);
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($reportForm);
            }

            if ($reportForm->validate()) {
                return $this->redirect(['/report/catalog/index',
                    'zoom' => 14,
                    'lat' => $reportForm['lat'],
                    'lng' => $reportForm['lng'],
                    'radius' => 1.5,
                    'address' => $reportForm['address'],
                    'category' => 'flats',
                    'operation' => Property::OPERATION_SALE,
                    'rooms' => [
                        'min' => $reportForm['rooms'],
                        'max' => $reportForm['rooms']
                    ],
                    'property_area' => [
                        'min' => (int)((int)$reportForm['property_area'] * 0.8),
                        'max' => (int)((int)$reportForm['property_area'] * 1.2)
                    ],
                    'source_property_area' => $reportForm['property_area'],
                    'source_rooms' => $reportForm['rooms'],
                ]);
            }
        }

        return [
            'html' => $this->renderAjax('pre-compare-report', [
                'reportForm' => $reportForm,
            ]),
            'success' => true,
        ];
    }
}
