<?php

namespace frontend\controllers\report;

use common\controllers\FrontEndController;
use common\mappers\CatalogSortMapper;
use common\mappers\LocationBoxFilterMapper;
use common\models\Property;
use common\repositories\elastic\PropertyRepository;
use common\services\entities\CategoryService;
use common\services\entities\PropertyService;
use common\services\MetaService;
use frontend\forms\PreCompareReportForm;
use frontend\mappers\map\PropertyMapMapper;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\controllers\property
 */
class CatalogController extends FrontEndController
{
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * @var LocationService
     */
    private $_locationService;
    /**
     * @var PropertyRepository
     */
    private $_elasticPropertyRepository;

    /**
     * CatalogController constructor.
     * @param $id
     * @param Module $module
     * @param PropertyService $propertyService
     * @param CategoryService $categoryService
     * @param LocationService $locationService
     * @param PropertyRepository $elasticPropertyRepository
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PropertyService $propertyService,
                                CategoryService $categoryService,
                                LocationService $locationService,
                                PropertyRepository $elasticPropertyRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyService = $propertyService;
        $this->_categoryService = $categoryService;
        $this->_locationService = $locationService;
        $this->_elasticPropertyRepository = $elasticPropertyRepository;

        $this->layout = 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
            ]
        ];
    }

    /**
     * @param $category
     * @param $operation
     * @return string|Response
     */
    public function actionIndex($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return $this->redirect(Url::current(['category' => $categoryItem['slug']]));
        }

        $this->_propertyService->setRepository($this->_elasticPropertyRepository);
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $params = $this->extractParams($categoryItem, Yii::$app->request->queryParams);
        $propertiesData = $this->preparePage($params);
        $seo = $this->getSeo($propertiesData, $params);
        $markers = PropertyMapMapper::getMappedData($propertiesData['properties']['items']);

        $reportForm = new PreCompareReportForm([
            'property_area' => $params['source_property_area'],
            'rooms' => $params['source_rooms'],
            'address' => $params['address']
        ]);

        $viewParams = [
            'lat' => $locationData['lat'],
            'lng' => $locationData['lng'],
            'zoom' => array_key_exists('zoom', $params) ? (int)$params['zoom'] : 11,
            'box' => $params['box'],
            'properties' => $propertiesData['properties'],
            'markers' => $markers,
            'seo' => $seo,
            'reportForm' => $reportForm
        ];

        return $this->render($propertiesData['view'], $viewParams);
    }

    /**
     * @param $categoryItem
     * @param array $params
     * @return array
     */
    protected function extractParams($categoryItem, array $params = [])
    {
        $params['status'] = Property::STATUS_ACTIVE;
        $params['box'] = $params['box'] ?? null;
        $params['per-page'] = $params['per-page'] ?? 200;
        $params['root'] = $categoryItem['root'];
        $params['lft'] = $categoryItem['lft'];
        $params['rgt'] = $categoryItem['rgt'];
        $params = array_merge($params, LocationBoxFilterMapper::getMappedData($params['box']));

        return $params;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function preparePage(array $params)
    {
        $orderBy = ['id' => SORT_DESC];
        $params['ads_allowed'] = true;
        if (array_key_exists('sort', $params)) {
            $mappedSortData = CatalogSortMapper::getMappedData($params['sort']);

            switch ($mappedSortData['field']) {
                case 'default_price':
                    $orderBy = ['default_price' => $mappedSortData['order']];
                    break;
            }
        }

        return [
            'properties' => $this->_propertyService->getMany($params, [
                'perPage' => $params['per-page'],
                'orderBy' => $orderBy
            ]),
            'view' => 'index-table'
        ];
    }

    /**
     * @param $propertiesData
     * @param array $params
     * @return array
     */
    protected function getSeo($propertiesData, array $params = [])
    {
        $text = Yii::t('catalog', '{count, plural, one{# property found} other{# properties found}} near {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}', [
            'address' => $params['address'],
            'count' => count($propertiesData['properties']['items']),
            'rooms' => $params['source_rooms'],
            'area' => $params['source_property_area']
        ]);

        $seo = [
            'heading' => $text,
            'title' => $text,
        ];

        $metaService = new MetaService($this);
        $metaService->registerMeta($seo);

        return $seo;
    }
}