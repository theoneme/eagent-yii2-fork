<?php

namespace frontend\controllers\report;

use common\controllers\FrontEndController;
use common\helpers\Auth;
use common\mappers\forms\ReportFormToMessageMapper;
use common\models\Report;
use common\services\entities\ReportService;
use common\services\NotificationService;
use frontend\forms\CompareReportForm;
use frontend\forms\ReportForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ManageController
 * @package frontend\controllers\report
 */
class ManageController extends FrontEndController
{
    /**
     * @var ReportService
     */
    private $_reportService;
    /**
     * @var NotificationService
     */
    private $_notificationService;

    /**
     * ReportController constructor.
     * @param string $id
     * @param Module $module
     * @param ReportService $reportService
     * @param NotificationService $notificationService
     * @param array $config
     */
    public function __construct(string $id, Module $module, ReportService $reportService, NotificationService $notificationService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_reportService = $reportService;
        $this->_notificationService = $notificationService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['create', 'create-compare'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['create', 'create-compare'],
            ],
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'create-compare'],
                'rules' => [
                    [
                        'actions' => ['create', 'create-compare'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $reportForm = new ReportForm();
        $input = Yii::$app->request->post();
        $reportForm->load($input);

        if (array_key_exists('ajax', $input)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($reportForm);
        }

        if ($reportForm->validate()) {
            $insertId = $this->_reportService->create([
                'from_id' => Yii::$app->user->getId(),
                'type' => Report::REPORT_PROPERTIES,
                'customDataArray' => [
                    'properties' => $reportForm->id,
                    'firstName' => $reportForm->firstName,
                    'lastName' => $reportForm->lastName,
                    'middleName' => $reportForm->middleName,
                    'email' => $reportForm->email,
                    'phone' => $reportForm->phone
                ]
            ]);

            if ($insertId !== null) {
                $params = ReportFormToMessageMapper::getMappedData([
                    'attributes' => $reportForm->attributes,
                    'agent' => Auth::user()->username,
                    'insertId' => $insertId
                ]);

                $this->_notificationService->sendNotification('email', $params);
            }

            return [
                'success' => true,
                'insertId' => $insertId
            ];
        }

        return ['success' => false];
    }

    /**
     * @return array|Response
     */
    public function actionCreateCompare()
    {
        $reportForm = new CompareReportForm();
        $input = Yii::$app->request->post();
        $reportForm->load($input);

        if (array_key_exists('ajax', $input)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($reportForm);
        }

        if ($reportForm->validate()) {
            $insertId = $this->_reportService->create([
                'from_id' => Yii::$app->user->getId(),
                'type' => Report::REPORT_COMPARE_PRICE,
                'customDataArray' => [
                    'properties' => $reportForm->id,
                    'prices' => $reportForm->prices,
                    'comments' => $reportForm->comments,
                    'message' => $reportForm->message,
                    'currency' => Yii::$app->params['app_currency'],
                    'propertyArea' => $reportForm->property_area,
                    'rooms' => $reportForm->rooms,
                    'address' => $reportForm->address
                ]
            ]);

//            return [
//                'success' => true,
//                'insertId' => $insertId
//            ];

            return $this->redirect(['/report/report/compare-report', 'id' => $insertId]);
        }

        return ['success' => false];
    }
}
