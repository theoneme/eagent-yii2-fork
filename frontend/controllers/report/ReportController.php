<?php

namespace frontend\controllers\report;

use common\controllers\FrontEndController;
use common\helpers\Auth;
use common\models\Report;
use common\services\entities\PropertyService;
use common\services\entities\ReportService;
use frontend\mappers\map\PropertyMapMapper;
use yii\base\Module;
use yii\web\NotFoundHttpException;

/**
 * Class ReportController
 * @package frontend\controllers
 */
class ReportController extends FrontEndController
{
    /**
     * @var ReportService
     */
    private $_reportService;
    /**
     * @var PropertyService
     */
    private $_propertyService;

    /**
     * ReportController constructor.
     * @param string $id
     * @param Module $module
     * @param ReportService $reportService
     * @param PropertyService $propertyService
     * @param array $config
     */
    public function __construct(string $id, Module $module, ReportService $reportService, PropertyService $propertyService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_reportService = $reportService;
        $this->_propertyService = $propertyService;
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionReport($id)
    {
        $this->layout = 'catalog';

        $report = $this->_reportService->getOne(['id' => $id, 'type' => Report::REPORT_PROPERTIES]);

        if ($report === null) {
            throw new NotFoundHttpException('Report not found');
        }

        $properties = $this->_propertyService->getMany(['id' => $report['customData']['properties']]);

        $propertyMarkers = PropertyMapMapper::getMappedData($properties['items']);

        return $this->render('report', [
            'properties' => $properties,
            'propertyMarkers' => $propertyMarkers,
            'report' => $report
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCompareReport($id)
    {
        $this->layout = 'catalog';

        $report = $this->_reportService->getOne(['id' => $id, 'type' => REPORT::REPORT_COMPARE_PRICE, 'from_id' => Auth::user()->getCurrentId()]);

        if ($report === null) {
            throw new NotFoundHttpException('Report not found');
        }

        $properties = $this->_propertyService->getMany(['id' => $report['customData']['properties']]);

        $propertyMarkers = PropertyMapMapper::getMappedData($properties['items']);

        return $this->render('compare-report', [
            'properties' => $properties,
            'propertyMarkers' => $propertyMarkers,
            'report' => $report
        ]);
    }
}
