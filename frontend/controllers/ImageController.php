<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use Imagine\Image\Box;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * Class ImageController
 * @package backend\controllers
 */
class ImageController extends FrontEndController
{
    /**
     * @return string
     */
    public function actionUpload()
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $response = [];
        $imageFiles = UploadedFile::getInstancesByName('uploaded_images');
        $post = Yii::$app->request->post();
        $path = '/uploads/temp/' . $year . '/' . $month . '/' . $day . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }
            if (Yii::$app instanceof \yii\console\Application) {
                chown($dir, 'www-data');
                chgrp($dir, 'www-data');
            }
        }
        foreach ($imageFiles as $file) {
            try {
                $sourceName = $file->name ?? 'unknown';
                $hash = uniqid('', true);
                $previewName = $name = $path . $hash . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@webroot') . $name);
                if (in_array($file->extension, ['jpg', 'gif', 'png', 'jpeg'])) {
                    if (isset($post['r']) && $post['r'] >= -360 && $post['r'] <= 360) {
                        Image::getImagine()->open(Yii::getAlias('@webroot') . $name)->rotate($post['r'])->save(Yii::getAlias('@webroot') . $name, ['quality' => 100]);
                    }
                    if (isset($post['x'], $post['y'], $post['w'], $post['h']) && $post['w'] >= 100 && $post['h'] >= 100) {
                        Image::crop(Yii::getAlias('@webroot') . $name, $post['w'], $post['h'], [$post['x'], $post['y']])->save(Yii::getAlias('@webroot') . $name, ['quality' => 100]);
                    }
                    if ($file->extension === 'png') {
                        Image::getImagine()
                            ->open(Yii::getAlias('@webroot') . $name)
//                    ->resize(new Box($size[0], $size[1]))
                            ->save(Yii::getAlias('@webroot') . "{$path}{$hash}.jpg", ['quality' => 80]);
                        $name = "{$path}{$hash}.jpg";
                    } else {
                        Image::getImagine()
                            ->open(Yii::getAlias('@webroot') . $name)
//                    ->resize(new Box($size[0], $size[1]))
                            ->save(Yii::getAlias('@webroot') . $name, ['quality' => 80]);
                    }
                } else {
                    $previewName = '/images/attachment-icon.png';
                }

                $response = [
                    'initialPreview' => [$previewName],
                    'initialPreviewConfig' => [
                        [
                            'caption' => basename($name),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => 'image_new_' . $post['fileindex'],
                        ]
                    ],
                    'initialPreviewThumbTags' => [
                        '{actions}' => '{actions}'
                    ],
                    'uploadedPath' => $name,
                    'imageKey' => 'image_new_' . $post['fileindex'],
                ];
            } catch (\Exception $e) {
                $response = [
                    'error' => "File {$sourceName} is corrupted"
                ];
            }
        }

        return Json::encode($response);
    }

    /**
     * @return string
     */
    public function actionUploadProfile()
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $response = [];
        $imageFiles = UploadedFile::getInstancesByName('uploaded_images');
        $post = Yii::$app->request->post();
        $path = '/uploads/temp/' . $year . '/' . $month . '/' . $day . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!file_exists($dir) && !mkdir($dir, 0777, true) && !is_dir($dir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }
        foreach ($imageFiles as $file) {
            try {
                $sourceName = $file->name ?? 'unknown';
                $hash = hash('crc32b', $file->name . time());
                $name = $path . hash('crc32b', $file->name . time()) . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@webroot') . $name);
                if (isset($post['r']) && $post['r'] >= -360 && $post['r'] <= 360) {
                    Image::getImagine()->open(Yii::getAlias('@webroot') . $name)->rotate($post['r'])->save(Yii::getAlias('@webroot') . $name, ['quality' => 100]);
                }
                if (isset($post['x'], $post['y'], $post['w'], $post['h']) && $post['w'] >= 100 && $post['h'] >= 100) {
                    Image::crop(Yii::getAlias('@webroot') . $name, $post['w'], $post['h'], [$post['x'], $post['y']])->save(Yii::getAlias('@webroot') . $name, ['quality' => 100]);
                }
                $size = getimagesize(Yii::getAlias('@webroot') . $name);
                $thumbSize = min($size[0], $size[1]) < 1000 ? min($size[0], $size[1]) : 1000;
                if ($file->extension === 'png') {
                    Image::thumbnail(Yii::getAlias('@webroot') . $name, $thumbSize, $thumbSize)
                        ->resize(new Box(1000, 1000))
                        ->save(Yii::getAlias('@webroot') . "{$path}{$hash}.jpg", ['quality' => 80]);
                    $name = "{$path}{$hash}.jpg";
                } else {
                    Image::thumbnail(Yii::getAlias('@webroot') . $name, $thumbSize, $thumbSize)
                        ->resize(new Box(1000, 1000))
                        ->save(Yii::getAlias('@webroot') . $name, ['quality' => 80]);
                }

                $response = [
//                'initialPreviewAsData' => true,
                    'initialPreview' => [$name],
                    'initialPreviewConfig' => [
                        [
                            'caption' => basename($name),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => 'image_new_' . $post['fileindex'],
                        ]
                    ],
                    'initialPreviewThumbTags' => [
                        '{actions}' => '{actions}'
                    ],
                    'uploadedPath' => $name,
                    'imageKey' => 'image_new_' . $post['fileindex'],
                ];
            } catch (\Exception $e) {
                $response = [
                    'error' => "File {$sourceName} is corrupted"
                ];
            }
        }

        return Json::encode($response);
    }

    /**
     * @return string
     */
    public function actionUploadWide()
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $response = [];
        $imageFiles = UploadedFile::getInstancesByName('uploaded_images');
        $post = Yii::$app->request->post();
        $path = '/uploads/temp/' . $year . '/' . $month . '/' . $day . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir) && !mkdir($dir, 0777, true) && !is_dir($dir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }
        foreach ($imageFiles as $file) {
            $name = $path . hash('crc32b', $file->name . time()) . '.' . $file->extension;
            $file->saveAs(Yii::getAlias('@webroot') . $name);
            if (isset($post['r']) && $post['r'] >= -360 && $post['r'] <= 360) {
                Image::getImagine()->open(Yii::getAlias('@webroot') . $name)->rotate($post['r'])->save(Yii::getAlias('@webroot') . $name, ['quality' => 100]);
            }
            if (isset($post['x'], $post['y'], $post['w'], $post['h']) && $post['w'] >= 150 && $post['h'] >= 100) {
                Image::crop(Yii::getAlias('@webroot') . $name, $post['w'], $post['h'], [$post['x'], $post['y']])->save(Yii::getAlias('@webroot') . $name, ['quality' => 100]);
            }
            Image::thumbnail(Yii::getAlias('@webroot') . $name, 900, 600)->resize(new Box(900, 600))->save(Yii::getAlias('@webroot') . $name, ['quality' => 80]);
            $response = [
//                'initialPreviewAsData' => true,
                'initialPreview' => [$name],
                'initialPreviewConfig' => [
                    [
                        'caption' => basename($name),
                        'url' => Url::toRoute('/image/delete'),
                        'key' => 'image_new_' . $post['fileindex'],
                    ]
                ],
                'initialPreviewThumbTags' => [
                    '{actions}' => '{actions}'
                ],
                'uploadedPath' => $name,
                'imageKey' => 'image_new_' . $post['fileindex'],
            ];
        }
        return Json::encode($response);
    }

    /**
     * @return bool
     */
    public function actionDelete()
    {
        // TODO Придумать нормальную логику удаления
        return true;
    }
}