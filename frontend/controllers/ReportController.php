<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\services\entities\ReportService;
use common\services\NotificationService;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ReportController
 * @package frontend\controllers
 */
class ReportController extends FrontEndController
{
    /**
     * @var ReportService
     */
    private $_reportService;
    /**
     * @var NotificationService
     */
    private $_notificationService;

    /**
     * ReportController constructor.
     * @param string $id
     * @param Module $module
     * @param ReportService $reportService
     * @param NotificationService $notificationService
     * @param array $config
     */
    public function __construct(string $id, Module $module, ReportService $reportService, NotificationService $notificationService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_reportService = $reportService;
        $this->_notificationService = $notificationService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['create', 'render'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['create', 'render'],
            ],
            'access' => [
                'class' => AccessControl::class,
                'only' => ['render', 'create'],
                'rules' => [
                    [
                        'actions' => ['render', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionRender()
    {
        return $this->redirect(['/report/ajax/render']);
//        $properties = Yii::$app->request->get('id');
//
//        if ($properties === null) {
//            return [
//                'success' => false
//            ];
//        }
//
//        /** @var PropertyService $propertyService */
//        $propertyService = Yii::$container->get(PropertyService::class);
//
//        $properties = $propertyService->getMany(['id' => $properties]);
//        $reportForm = new ReportForm([
//            'id' => array_column($properties['items'], 'id')
//        ]);
//
//        return [
//            'html' => $this->renderAjax('modal', [
//                'reportForm' => $reportForm,
//                'properties' => $properties
//            ]),
//            'success' => true,
//        ];
    }

    /**
     * @return Response
     */
    public function actionCreate()
    {
        return $this->redirect(['/report/manage/create']);
//        $reportForm = new ReportForm();
//        $input = Yii::$app->request->post();
//        $reportForm->load($input);
//
//        if (array_key_exists('ajax', $input)) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//
//            return ActiveForm::validate($reportForm);
//        }
//
//        if ($reportForm->validate()) {
//            $insertId = $this->_reportService->create([
//                'from_id' => Yii::$app->user->getId(),
//                'customDataArray' => [
//                    'properties' => $reportForm->id,
//                    'firstName' => $reportForm->firstName,
//                    'lastName' => $reportForm->lastName,
//                    'middleName' => $reportForm->middleName,
//                    'email' => $reportForm->email,
//                    'phone' => $reportForm->phone
//                ]
//            ]);
//
//            if ($insertId !== null) {
//                $params = ReportFormToMessageMapper::getMappedData([
//                    'attributes' => $reportForm->attributes,
//                    'agent' => Auth::user()->username,
//                    'insertId' => $insertId
//                ]);
//
//                $this->_notificationService->sendNotification('email', $params);
//            }
//
//            return [
//                'success' => true,
//                'insertId' => $insertId
//            ];
//        }
//
//        return ['success' => false];
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionView($id)
    {
        return $this->redirect(['/report/report/report', 'id' => $id]);
//        $this->layout = 'catalog';
//
//        $report = $this->_reportService->getOne(['id' => $id]);
//
//        if ($report === null) {
//            throw new NotFoundHttpException('Report not found');
//        }
//
//        /** @var PropertyService $propertyService */
//        $propertyService = Yii::$container->get(PropertyService::class);
//        $properties = $propertyService->getMany(['id' => $report['customData']['properties']]);
//
//        $propertyMarkers = PropertyMapMapper::getMappedData($properties['items']);
//
//        return $this->render('view', [
//            'properties' => $properties,
//            'propertyMarkers' => $propertyMarkers,
//            'report' => $report
//        ]);
    }
}
