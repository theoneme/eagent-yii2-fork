<?php

namespace frontend\controllers\location;

use common\controllers\FrontEndController;
use common\helpers\DataHelper;
use Yii;

/**
 * Class ListController
 * @package frontend\controllers\location
 */
class ListController extends FrontEndController
{
    /**
     * @return string
     */
    public function actionLocations()
    {
        $language = Yii::$app->language;

        $data = Yii::$app->cache->get("locations-tree-{$language}");
        if (!$data) {
            $data = DataHelper::getLocationTree();
            ksort($data);

            Yii::$app->cache->set("locations-tree-{$language}", $data, 100000);
        }

        return $this->render('locations', [
            'data' => $data
        ]);
    }
}
