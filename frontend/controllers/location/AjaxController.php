<?php

namespace frontend\controllers\location;

use common\controllers\FrontEndController;
use common\mappers\LocationModalMapper;
use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\DistrictService;
use common\services\entities\MicroDistrictService;
use common\services\entities\RegionService;
use frontend\mappers\LocationMapper;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers
 */
class AjaxController extends FrontEndController
{
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var RegionService
     */
    private $_regionService;
    /**
     * @var CountryService
     */
    private $_countryService;
    /**
     * @var LocationService
     */
    private $_locationService;
    /**
     * @var DistrictService
     */
    private $_districtService;
    /**
     * @var MicroDistrictService
     */
    private $_microDistrictService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param CityService $cityService
     * @param RegionService $regionService
     * @param CountryService $countryService
     * @param LocationService $locationService
     * @param DistrictService $districtService
     * @param MicroDistrictService $microDistrictService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                CityService $cityService,
                                RegionService $regionService,
                                CountryService $countryService,
                                LocationService $locationService,
                                DistrictService $districtService,
                                MicroDistrictService $microDistrictService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_cityService = $cityService;
        $this->_regionService = $regionService;
        $this->_countryService = $countryService;
        $this->_locationService = $locationService;
        $this->_districtService = $districtService;
        $this->_microDistrictService = $microDistrictService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['locations', 'set-location', 'countries', 'regions', 'cities', 'big-cities', 'city-parts'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['locations', 'set-location', 'countries', 'regions', 'cities', 'big-cities', 'city-parts'],
            ]
        ];
    }

    /**
     * @param $request
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionLocations($request)
    {
        $locations = [];

        if (!empty($request) && mb_strlen($request) > 2) {
            $cities = $this->_cityService->getMany(['request' => $request], ['limit' => 10]);

            if (!empty($cities['items'])) {
                $regionIds = array_map(function ($value) {
                    return $value['region_id'];
                }, $cities['items']);

                $regions = $this->_regionService->getMany(['id' => array_unique($regionIds)], ['indexBy' => 'id']);
                $countryIds = array_map(function ($value) {
                    return $value['country_id'];
                }, $cities['items']);
                $countries = $this->_countryService->getMany(['id' => array_unique($countryIds)], ['indexBy' => 'id']);

                $locations = LocationMapper::getMappedData([
                    'cities' => $cities['items'],
                    'regions' => $regions['items'],
                    'countries' => $countries['items']
                ]);
            }

            $regions = $this->_regionService->getMany(['request' => $request], ['limit' => 10]);
            if (!empty($regions['items'])) {
                $countryIds = array_map(function ($value) {
                    return $value['country_id'];
                }, $regions['items']);
                $countries = $this->_countryService->getMany(['id' => array_unique($countryIds)], ['indexBy' => 'id']);

                $locations = array_merge($locations, LocationMapper::getMappedData([
                    'regions' => $regions['items'],
                    'countries' => $countries['items']
                ], LocationMapper::MODE_REGION));
            }

            $countries = $this->_countryService->getMany(['request' => $request], ['limit' => 10]);
            $locations = array_merge($locations, LocationMapper::getMappedData([
                'countries' => $countries['items']
            ], LocationMapper::MODE_COUNTRY));
        }

        return $locations;
    }

    /**
     * @return array|Response
     */
    public function actionSetLocation()
    {
        $location = Yii::$app->request->post('location');
        if (empty($location)) {
            return ['success' => false];
        }

        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        if ($location !== $locationData['slug']) {
            return $this->redirect(Url::to(['/site/index', 'app_city' => $location]));
        }

        return ['success' => false];
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCountries()
    {
        $countries = $this->_countryService->getMany([]);
        $mappedCountries = LocationModalMapper::getMappedData($countries['items'], 'country');
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        return [
            'success' => true,
            'html' => $this->render('countries', [
                'countries' => $mappedCountries,
                'locationData' => $locationData
            ])
        ];
    }

    /**
     * @param null $country_id
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegions($country_id = null)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $regions = $this->_regionService->getMany(['country_id' => $country_id ?? $locationData['country_id']]);
//        $mappedRegions['+'][] = [
//            'id' => $country_id ?? $locationData['country_id'],
//            'title' => Yii::t('wizard', 'All Regions'),
//            'entity' => 'country-special'
//        ];
        $mappedRegions = [];
        $mappedRegions = array_merge($mappedRegions, LocationModalMapper::getMappedData($regions['items'], 'region'));

        return [
            'success' => true,
            'html' => $this->render('regions', [
                'regions' => $mappedRegions,
                'locationData' => $locationData
            ])
        ];
    }

    /**
     * @param null $region_id
     * @param null $request
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCities($region_id = null, $request = null)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $condition = ['request' => $request];

        if ($region_id === null) {
            if ($locationData['region_id'] === null) {
                $condition = ['country_id' => $locationData['country_id'], $condition];
            } else {
                $condition = ['region_id' => $locationData['region_id'], $condition];
            }
        } else {
            $condition = ['region_id' => $region_id, $condition];
        }

        $cities = $this->_cityService->getMany($condition, ['limit' => 1250]);

        $mappedCities = [];
        $mappedCities = array_merge($mappedCities, LocationModalMapper::getMappedData($cities['items'], 'city'));

        return [
            'success' => true,
            'html' => $this->render('cities', [
                'cities' => $mappedCities,
                'locationData' => $locationData
            ])
        ];
    }

    /**
     * @param $country_id
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionBigCities($country_id = null)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $country = $this->_countryService->getOne(['country.id' => $country_id ?? $locationData['country_id']]);
        if ($country['code'] === 'ru') {
            $cities = $this->_cityService->getMany(['country_id' => $country['id'], 'is_big' => true]);
        } else {
            $cities = $this->_cityService->getMany(['is_big' => true]);
        }

        $mappedCities = LocationModalMapper::getMappedData($cities['items']);

        return [
            'success' => true,
            'html' => $this->render('cities', [
                'cities' => $mappedCities,
                'locationData' => $locationData
            ])
        ];
    }

    /**
     * @return array
     */
    public function actionCityParts()
    {
        $currentDistrict = Yii::$app->request->get('district');
        $currentMicroDistrict = Yii::$app->request->get('microdistrict');
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        if ($locationData['city_id'] === null) {
            $districts = $microDistricts = [];
        } else {
            $condition = ['city_id' => $locationData['city_id']];
            $districts = $this->_districtService->getMany($condition)['items'];
            $microDistricts = $this->_microDistrictService->getMany($condition)['items'];

            $districts = array_map(function ($value) {
                return [
                    'slug' => $value['slug'],
                    'title' => $value['title']
                ];
            }, $districts);
            $microDistricts = LocationModalMapper::getMappedData($microDistricts);
        }

        return [
            'success' => true,
            'html' => [
                'districts' => $this->render('districts', [
                    'districts' => $districts,
                    'currentDistrict' => $currentDistrict
                ]),
                'microdistricts' => $this->render('microdistricts', [
                    'microDistricts' => $microDistricts,
                    'currentMicroDistrict' => $currentMicroDistrict
                ]),
            ]
        ];
    }
}
