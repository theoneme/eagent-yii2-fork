<?php

namespace frontend\controllers\user;

use common\controllers\FrontEndController;
use common\forms\ar\composite\ContactForm;
use common\mappers\SelectizeUserMapper;
use common\services\entities\UserService;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers\user
 */
class AjaxController extends FrontEndController
{
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param UserService $userService
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserService $userService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_userService = $userService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-contact', 'users', 'users-selectize'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['render-contact', 'users', 'users-selectize'],
            ]
        ];
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderContact($iterator)
    {
        $contactForm = new ContactForm();

        return [
            'html' => $this->renderAjax('contact', [
                'contactForm' => $contactForm,
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $request
     * @return mixed
     */
    public function actionUsers($request)
    {
        $users = $this->_userService->getMany(['username' => $request], ['limit' => 10]);

        return $users['items'];
    }

    /**
     * @param $request
     * @return mixed
     */
    public function actionUsersSelectize($request)
    {
        $users = $this->_userService->getMany(['username' => $request], ['limit' => 10]);
        $users = SelectizeUserMapper::getMappedData($users['items']);

        return $users;
    }
}
