<?php
namespace frontend\controllers\user;

use common\components\CurrencyHelper;
use common\controllers\FrontEndController;
use common\dto\UserWalletDTO;
use common\helpers\Auth;
use common\models\CompanyMember;
use common\models\User;
use common\services\entities\PaymentMethodService;
use common\services\entities\UserWalletService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class WalletController
 * @package frontend\controllers\user
 */
class WalletController extends FrontEndController
{
    /**
     * @var UserWalletService
     */
    private $_userWalletService;
    /**
     * @var PaymentMethodService
     */
    private $_paymentMethodService;

    /**
     * WalletController constructor.
     * @param string $id
     * @param Module $module
     * @param UserWalletService $userWalletService
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserWalletService $userWalletService, PaymentMethodService $paymentMethodService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userWalletService = $userWalletService;
        $this->_paymentMethodService = $paymentMethodService;
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // Пускаем простых юзеров
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return empty(Auth::user()->company_user_id);
                        },
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed|string|Response
     */
    public function actionIndex()
    {
        $wallets = $this->_userWalletService->getMany(['user_id' => Auth::user()->getCurrentId()], ['indexBy' => 'currency_code']);

        if (empty($wallets['items'][Yii::$app->params['app_currency']])) {
            $wallets['items'][Yii::$app->params['app_currency']] = (new UserWalletDTO([
                'id' => 'XXXXXXXXX',
                'user_id' => Auth::user()->getCurrentId(),
                'balance' => 0,
                'currency_code' => Yii::$app->params['app_currency'],
                'histories' => [],
            ]))->getShortData();
        }
        return $this->render('index', ['wallets' => $wallets['items']]);
    }
}
