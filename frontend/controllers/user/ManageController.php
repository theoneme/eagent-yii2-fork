<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:03
 */

namespace frontend\controllers\user;

use common\controllers\FrontEndController;
use common\dto\UserDTO;
use common\helpers\FormHelper;
use common\models\User;
use common\repositories\sql\UserRepository;
use frontend\forms\ar\UserForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class ManageController
 * @package frontend\controllers\user
 */
class ManageController extends FrontEndController
{
    public const ACTION_CREATE = 0;
    public const ACTION_UPDATE = 1;

    /**
     * @var UserRepository
     */
    private $_userRepository;

    /**
     * ManageController constructor.
     * @param string $id
     * @param Module $module
     * @param UserRepository $userRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserRepository $userRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['update'],
                'rules' => [
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return mixed|string|Response
     */
    public function actionUpdate()
    {
        $this->layout = 'catalog';

        $userForm = new UserForm();
        /** @var User $user */
        $user = $this->_userRepository->findOneByCriteria(['user.id' => Yii::$app->user->getId()]);
        $userForm->_user = $user;
        $userDTO = new UserDTO($user);
        $userData = $userDTO->getData();
        $userForm->prepareUpdate($userData);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $userForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($userForm);
            }

            if ($userForm->validate()) {
                $userForm->bindData();
                $userForm->save();

                return $this->redirect(['/agent/agent/view', 'id' => $userForm->_user->id]);
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_USER);
        $userForm->buildLayout($formConfig);

        return $this->render('form', [
            'userForm' => $userForm,
            'userDTO' => $userData,
            'route' => ['/user/manage/update', 'id' => Yii::$app->user->getId()],
            'action' => self::ACTION_UPDATE
        ]);
    }
}
