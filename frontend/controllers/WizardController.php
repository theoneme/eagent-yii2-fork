<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.09.2018
 * Time: 11:34
 */

namespace frontend\controllers;

use common\controllers\FrontEndController;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;

/**
 * Class SiteController
 * @package frontend\controllers
 */
class WizardController extends FrontEndController
{
    /**
     * WizardController constructor.
     * @param $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->layout = 'catalog';
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionWizard()
    {
        /** @var LocationService $locationService */
        $locationService = Yii::$container->get(LocationService::class);
        $locationData = $locationService->getLocationData(Yii::$app->params['runtime']['location']);

        return $this->render('wizard', [
            'address' => $locationData['address']
        ]);
    }


}