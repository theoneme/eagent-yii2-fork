<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 20:16
 */

namespace frontend\controllers\property;

use common\controllers\FrontEndController;
use common\models\Property;
use common\models\Translation;
use common\models\User;
use common\repositories\elastic\PropertyRepository;
use common\services\entities\BuildingService;
use common\services\entities\CityService;
use common\services\entities\PropertyService;
use common\services\entities\UserService;
use common\services\MetaService;
use common\services\seo\PropertySeoService;
use frontend\forms\contact\ContactAgentForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class PropertyController
 * @package frontend\controllers\property
 */
class PropertyController extends FrontEndController
{
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var PropertySeoService
     */
    private $_propertySeoService;
    /**
     * @var PropertyRepository
     */
    private $_sqlPropertyRepository;

    /**
     * PropertyController constructor.
     * @param $id
     * @param Module $module
     * @param PropertyService $propertyService
     * @param BuildingService $buildingService
     * @param PropertySeoService $propertySeoService
     * @param UserService $userService
     * @param \common\repositories\sql\PropertyRepository $propertyRepository
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PropertyService $propertyService,
                                BuildingService $buildingService,
                                PropertySeoService $propertySeoService,
                                UserService $userService,
                                \common\repositories\sql\PropertyRepository $propertyRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyService = $propertyService;
        $this->_buildingService = $buildingService;
        $this->_userService = $userService;
        $this->_propertySeoService = $propertySeoService;
        $this->_sqlPropertyRepository = $propertyRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'update'],
                'rules' => [
                    [
                        'actions' => ['create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param $slug
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionShow($slug)
    {
        $property = $this->_propertyService->getOne(['and',
            ['property_translations.value' => $slug],
            ['property_translations.key' => Translation::KEY_SLUG],
            ['not', ['property.status' => Property::STATUS_DELETED]]
        ]);

        if ($property === null) {
            throw new NotFoundHttpException(Yii::t('main', '{entity} not found', ['entity' => 'Property']));
        }
        if ($this->_sqlPropertyRepository->inc($property['id'], 'views_count')) {
            $property['views_count']++;
        }

        $realtors = $this->_userService->getMany([
            'type' => User::TYPE_REALTOR,
            'status' => User::STATUS_ACTIVE,
            'lat' => $property['lat'],
            'lng' => $property['lng']
        ], ['limit' => 3]);
        if (empty($realtors['items'])) {
            $realtors = $this->_userService->getMany([
                'id' => 7086
            ], ['limit' => 1]);
        }
        $seoParams = array_map(function ($var) {
            return $var['value'];
        }, $property['attributes']);
        $seoParams['type'] = $property['type'];
        $seoParams['address'] = $property['address'];
        if ($property['locale'] === Yii::$app->language) {
            $seoParams['saddress'] = $property['address'];
        } else if (!empty($property['city_id'])) {
            /* @var CityService $cityService */
            $cityService = Yii::$container->get(CityService::class);
            $city = $cityService->getOne(['id' => $property['city_id']]);
            if ($city) {
                $seoParams['saddress'] = $city['title'];
            }
        }
        $seoParams['city'] = $property['addressData']['city'] ?? '';
        $seoParams['price'] = $property['raw_price'];
        $seoParams['category'] = $property['category'][Translation::KEY_ADDITIONAL_TITLE] ?? $property['category'][Translation::KEY_TITLE] ?? '';
        $seo = $this->_propertySeoService->getSeo($property['categorySeoTemplate'], $seoParams);
        $metaService = new MetaService($this);
        $seo['image'] = $property['image'];
        $seo['description'] = !empty($property['description']) ? $property['description'] : $seo['description'];
        $metaService->registerMeta($seo);

//        /** @var PropertyRepository $propertyRepository */
//        $propertyRepository = Yii::$container->get(PropertyRepository::class);
//        $this->_propertyService->setRepository($propertyRepository);

//        $similarParams = SimilarPropertiesParamsMapper::getMappedData($property);
//        $similarProperties = $this->_propertyService->getMany($similarParams, [
//            'limit' => 6,
//            'exclude' => $property['id'],
//            $orderBy = ['id' => SORT_DESC]
//        ]);
//        $similarUrl = Url::to(array_merge(['/property/catalog/search'], $similarParams));
//
//        $cheaperParams = array_merge($similarParams, [
//            'price' => [
//                'max' => $property['raw_price'],
//            ]
//        ]);
//        $cheaperProperties = $this->_propertyService->getMany($cheaperParams, [
//            'limit' => 4,
//            'exclude' => $property['id'],
//            'pagination' => false,
//            'orderBy' => ['id' => SORT_DESC]
//        ]);
//
//        $moreExpensiveParams = array_merge($similarParams, [
//            'price' => [
//                'min' => $property['raw_price']
//            ]
//        ]);
//        $moreExpensiveProperties = $this->_propertyService->getMany($moreExpensiveParams, [
//            'limit' => 4,
//            'exclude' => $property['id'],
//            'pagination' => false,
//            'orderBy' => ['id' => SORT_DESC]
//        ]);
//
//        $biggerPropertiesParams = array_merge($similarParams, [
//            'price' => [
//                'min' => (int)($property['raw_price'] * 0.9),
//                'max' => (int)($property['raw_price'] * 1.1)
//            ]
//        ]);
//        if (array_key_exists('property_area', $property['attributes'])) {
//            $biggerPropertiesParams['property_area']['min'] = $property['attributes']['property_area']['value'];
//        }
//        $biggerProperties = $this->_propertyService->getMany($biggerPropertiesParams, [
//            'limit' => 4,
//            'exclude' => $property['id'],
//            'pagination' => false,
//            'orderBy' => ['id' => SORT_DESC]
//        ]);

        $contactAgentForm = new ContactAgentForm();
        $building = $this->_buildingService->getOne(['building.id' => $property['building_id']]);

        $viewParams = [
            'property' => $property,
//            'similarProperties' => $similarProperties['items'],
//            'similarUrl' => $similarUrl,
            'seo' => $seo,
            'realtors' => $realtors['items'],
            'contactAgentForm' => $contactAgentForm,
            'building' => $building,
            'cheaperProperties' => [],
            'moreExpensiveProperties' => [],
            'biggerProperties' => []
        ];

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'html' => $this->renderAjax('show', $viewParams),
                'success' => true,
                'seo' => $seo,
            ];
        }

        $this->layout = 'catalog';
        $categorySlugs = array_map(
            function ($var) {
                return $var[Yii::$app->language] ?? null;
            },
            Yii::$app->cacheLayer->getCategoryAliasCache('ru-RU')
        );
        $viewParams['catalogCategory'] = $categorySlugs['flats'] ?? 'flats';
        return $this->render('show-direct', $viewParams);
    }
}