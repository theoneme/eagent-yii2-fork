<?php

namespace frontend\controllers\property;

use common\controllers\FrontEndController;
use common\helpers\DataHelper;
use common\models\Property;
use common\services\entities\PropertyService;
use common\services\entities\UserService;
use common\services\NotificationService;
use frontend\forms\contact\MessageForm;
use frontend\forms\contact\MortgageForm;
use frontend\forms\contact\OfferPriceForm;
use frontend\forms\contact\QuizForm;
use frontend\forms\contact\ShowcaseForm;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AjaxController
 * @package frontend\controllers\property
 */
class AjaxController extends FrontEndController
{
    /**
     * @var NotificationService
     */
    private $_notificationService;
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param NotificationService $notificationService
     * @param PropertyService $propertyService
     * @param UserService $userService
     * @param array $config
     */
    public function __construct(string $id, Module $module, NotificationService $notificationService, PropertyService $propertyService, UserService $userService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_notificationService = $notificationService;
        $this->_propertyService = $propertyService;
        $this->_userService = $userService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['contact-agent', 'send-message', 'show-contacts', 'offer-price', 'request-showcase', 'request-mortgage', 'quiz', 'building-properties'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['contact-agent', 'send-message', 'show-contacts', 'offer-price', 'request-showcase', 'request-mortgage', 'quiz', 'building-properties'],
            ]
        ];
    }

    /**
     * @param $entity_id
     * @param $owner_id
     * @return array
     */
    public function actionShowContacts($entity_id, $owner_id)
    {
        $user = $this->_userService->getOne(['user.id' => $owner_id]);
        $property = $this->_propertyService->getOne(['property.id' => $entity_id]);

        if ($user === null || $property === null) {
            return ['success' => false];
        }

        return [
            'html' => $this->renderAjax('contacts-modal', [
                'user' => $user,
                'property' => $property
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $entity_id
     * @return array
     */
    public function actionSendMessage($entity_id)
    {
        $property = $this->_propertyService->getOne(['property.id' => $entity_id]);

        if ($property === null) {
            return ['success' => false];
        }

        $messageForm = new MessageForm([
            'message' => Yii::t('property', 'I am interested in {property}', [
                'property' => $property['title'],
            ])
        ]);

        $input = Yii::$app->request->post();
        $messageForm->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($messageForm);
            }

            if ($messageForm->validate()) {
                $params = [
                    'to' => array_merge([$property['user']['email']], Yii::$app->params['ourEmails']),
                    'receiverName' => $property['user']['username'],
                    'senderName' => $messageForm['name'],
                    'senderEmail' => $messageForm['email'],
                    'senderPhone' => $messageForm['phone'],
                    'senderMessage' => $messageForm['message'],
                    'subject' => Yii::t('notification', 'Message for property owner'),
                    'link' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                    'view' => 'agent-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }
        }

        return [
            'html' => $this->renderAjax('send-message-modal', [
                'messageForm' => $messageForm,
                'entityId' => $entity_id
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $entity_id
     * @return array
     */
    public function actionOfferPrice($entity_id)
    {
        $property = $this->_propertyService->getOne(['property.id' => $entity_id]);

        if ($property === null) {
            return ['success' => false];
        }

        $priceForm = new OfferPriceForm([
            'currency_code' => Yii::$app->params['app_currency']
        ]);
        $currencies = DataHelper::getCurrencies();

        $input = Yii::$app->request->post();
        $priceForm->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($priceForm);
            }

            if ($priceForm->validate()) {
                $params = [
                    'to' => array_merge([$property['user']['email']], Yii::$app->params['ourEmails']),
                    'receiverName' => $property['user']['username'],
                    'senderName' => $priceForm['name'],
                    'senderEmail' => $priceForm['email'],
                    'senderPhone' => $priceForm['phone'],
                    'senderMessage' => Yii::t('property', 'Hi! I offer price {price} {currency}', [
                        'price' => $priceForm['price'],
                        'currency' => $property['currency_code']
                    ]),
                    'subject' => Yii::t('notification', 'Property Price Offer'),
                    'link' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                    'view' => 'agent-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }

            return ['success' => false];
        }

        return [
            'html' => $this->renderAjax('offer-price-modal', [
                'priceForm' => $priceForm,
                'entityId' => $entity_id,
                'currencies' => $currencies
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $entity_id
     * @return array
     */
    public function actionRequestShowcase($entity_id)
    {
        $property = $this->_propertyService->getOne(['property.id' => $entity_id]);

        if ($property === null) {
            return ['success' => false];
        }

        $showcaseForm = new ShowcaseForm();

        $input = Yii::$app->request->post();
        $showcaseForm->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($showcaseForm);
            }

            if ($showcaseForm->validate()) {
                $params = [
                    'to' => array_merge([$property['user']['email']], Yii::$app->params['ourEmails']),
                    'receiverName' => $property['user']['username'],
                    'senderName' => $showcaseForm['name'],
                    'senderEmail' => $showcaseForm['email'],
                    'senderPhone' => $showcaseForm['phone'],
                    'senderMessage' => Yii::t('property', 'Hi! I want to request a show case on {date} with additions: {time}', [
                        'date' => $showcaseForm['date'],
                        'time' => $showcaseForm['time']
                    ]),
                    'subject' => Yii::t('notification', 'Property Request Showcase'),
                    'link' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                    'view' => 'agent-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }

            return ['success' => false];
        }

        return [
            'html' => $this->renderAjax('request-showcase-modal', [
                'showcaseForm' => $showcaseForm,
                'entityId' => $entity_id,
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $entity_id
     * @return array
     */
    public function actionRequestMortgage($entity_id)
    {
        $property = $this->_propertyService->getOne(['property.id' => $entity_id]);

        if ($property === null) {
            return ['success' => false];
        }

        $mortgageForm = new MortgageForm([
            'propertyName' => $property['title'],
            'credit' => $property['raw_price'],
            'currencyCode' => Yii::$app->params['app_currency']
        ]);
        $currencies = DataHelper::getCurrencies();

        $input = Yii::$app->request->post();
        $mortgageForm->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($mortgageForm);
            }

            if ($mortgageForm->validate()) {
                $params = [
                    'to' => array_merge([$property['user']['email']], Yii::$app->params['ourEmails']),
                    'receiverName' => $property['user']['username'],
                    'senderName' => $mortgageForm['name'],
                    'senderEmail' => $mortgageForm['email'],
                    'senderPhone' => $mortgageForm['phone'],
                    'senderMessage' => Yii::t('property', 'Hi! I want to request a mortgage for property {property}. Credit: {credit} {currency}, Loan Time: {loanTerm, plural, one{# year} other{# years}}, Age: {age, plural, one{# year} other{# years}}, Family Status: {familyStatus}', [
                        'property' => $mortgageForm['propertyName'],
                        'credit' => $mortgageForm['credit'],
                        'loanTerm' => $mortgageForm['loanTerm'],
                        'age' => $mortgageForm['age'],
                        'familyStatus' => $mortgageForm->getFamilyStatus($mortgageForm['familyStatus']),
                        'currency' => $mortgageForm['currencyCode'],
                    ]),
                    'subject' => Yii::t('notification', 'Property Request Mortgage'),
                    'link' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                    'view' => 'agent-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }

            return ['success' => false];
        }

        return [
            'html' => $this->renderAjax('request-mortgage-modal', [
                'mortgageForm' => $mortgageForm,
                'entityId' => $entity_id,
                'currencies' => $currencies
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $entity_id
     * @param null $name
     * @param null $email
     * @return array
     */
    public function actionQuiz($entity_id, $name = null, $email = null)
    {
        $property = $this->_propertyService->getOne(['property.id' => $entity_id]);

        if ($property === null) {
            return ['success' => false];
        }

        $quizForm = new QuizForm([
            'name' => $name,
            'email' => $email
        ]);
        $intervals = DataHelper::getQuizIntervals();

        $input = Yii::$app->request->post();
        $quizForm->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($quizForm);
            }

            if ($quizForm->validate()) {
                $attributes = [];
                foreach ($quizForm->attributes as $key => $attribute) {
                    switch ($key) {
                        case 'interval':
                            $attributes[$quizForm->getAttributeLabel($key)] = $intervals[$attribute] ?? Yii::t('property', 'Not Set');

                            break;
                        case 'buy':
                        case 'mortgage':
                        case 'secondary':
                            $attributes[$quizForm->getAttributeLabel($key)] = $attribute ? Yii::t('main', 'Yes') : Yii::t('main', 'No');

                            break;
                        default:
                            if (!empty($attribute)) {
                                $attributes[$quizForm->getAttributeLabel($key)] = $attribute;
                            }
                    }
                }

                $params = [
                    'to' => Yii::$app->params['ourEmails'],
                    'senderName' => $name,
                    'senderEmail' => $email,
                    'senderMessage' => Yii::t('property', 'Additional answers after contact agent form'),
                    'subject' => Yii::t('notification', 'Property Request Quiz'),
                    'attributes' => $attributes,
                    'link' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                    'view' => 'quiz-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }

            return ['success' => false];
        }

        return [
            'html' => $this->renderAjax('quiz-modal', [
                'intervals' => $intervals,
                'quizForm' => $quizForm,
                'entityId' => $entity_id
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $operation
     * @param $building_id
     * @param $rooms
     * @return array
     */
    public function actionBuildingProperties($operation, $building_id, $rooms)
    {
        $params = Yii::$app->request->queryParams;
//        $params['category_id'] = 2;
        $params['ads_allowed'] = true;
        $params['status'] = Property::STATUS_ACTIVE;
        $properties = $this->_propertyService->getMany($params, [
            'pagination' => true,
            'perPage' => 10,
            'orderBy' => ['id' => SORT_DESC],
        ]);

        return [
            'html' => $this->renderAjax('building-properties', [
                'properties' => $properties
            ]),
            'success' => true,
        ];
    }
}
