<?php

namespace frontend\controllers\property;

use common\assets\GoogleAsset;
use common\controllers\FrontEndController;
use common\services\entities\CategoryService;
use common\services\MetaService;
use frontend\assets\CatalogAsset;
use frontend\assets\CommonAsset;
use frontend\assets\FilterAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use frontend\helpers\ViewToTemplateHelper;
use frontend\services\catalog\PropertyCatalogDataService;
use frontend\services\catalog\PropertyCatalogUrlService;
use frontend\widgets\PropertyFilter;
use Yii;
use yii\base\Module;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Cookie;
use yii\web\JqueryAsset;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\controllers\property
 */
class CatalogController extends FrontEndController
{
    /**
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * @var PropertyCatalogUrlService
     */
    private $_propertyCatalogUrlService;
    /**
     * @var PropertyCatalogDataService
     */
    private $_propertyCatalogDataService;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
            ]
        ];
    }

    /**
     * CatalogController constructor.
     * @param $id
     * @param Module $module
     * @param CategoryService $categoryService
     * @param PropertyCatalogUrlService $propertyCatalogUrlService
     * @param PropertyCatalogDataService $propertyCatalogDataService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                CategoryService $categoryService,
                                PropertyCatalogUrlService $propertyCatalogUrlService,
                                PropertyCatalogDataService $propertyCatalogDataService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_categoryService = $categoryService;
        $this->_propertyCatalogUrlService = $propertyCatalogUrlService;
        $this->_propertyCatalogDataService = $propertyCatalogDataService;

        $this->layout = 'catalog';
    }

    /**
     * @param $category
     * @param $operation
     * @return array|string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return $this->redirect(Url::current(['category' => $categoryItem['slug']]));
        }

        $queryParams = Yii::$app->request->queryParams;

        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $template = ViewToTemplateHelper::getTemplate($cookieView);
        if ($cookieView !== 'map' && array_key_exists('box', $queryParams) && array_key_exists('zoom', $queryParams)) {
            $template = 'index-map';
            $this->actionView('map');
        }
        Yii::$app->params['runtime']['catalogTemplate'] = $template;
        $catalogData = $this->_propertyCatalogDataService->getData($queryParams, $categoryItem, $cookieView, PropertyCatalogDataService::MODE_INDEX);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->assetManager->bundles = [
                BootstrapPluginAsset::class => false,
                BootstrapAsset::class => false,
                JqueryAsset::class => false,
                CommonAsset::class => false,
                GoogleAsset::class => false,
                FilterAsset::class => false,
                CatalogAsset::class => false,
                AutoCompleteAsset::class => false
            ];

            return [
                'html' => $this->renderAjax($template, $catalogData),
                'success' => true,
                'seo' => $catalogData['seo'],
            ];
        }

        $metaService = new MetaService($this);
        $metaService->registerMeta($catalogData['seo']);

        return $this->render($template, $catalogData);
    }

    /**
     * @param $category
     * @param $operation
     * @return array
     */
    public function actionIndexAjax($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return [
                'success' => false
            ];
        }

        $queryParams = Yii::$app->request->queryParams;
        $seoUrl = $this->_propertyCatalogUrlService->getUrl($queryParams);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $catalogData = $this->_propertyCatalogDataService->getData($queryParams, $categoryItem, $cookieView, PropertyCatalogDataService::MODE_INDEX_AJAX);
        $markers = ArrayHelper::remove($catalogData, 'markers');
        $seo = $catalogData['seo'];
        /** @var PropertyFilter $filter */
        $filter = ArrayHelper::remove($catalogData, 'propertyFilter');

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'seo' => $seo,
            'url' => $seoUrl,
            'domain' => array_key_exists('city', $queryParams),
            'markers' => $markers['items'],
            'polygon' => $catalogData['polygon'],
            'filter' => $filter->run(),
            'success' => true
        ];
    }

    /**
     * @param $category
     * @param $operation
     * @return array
     */
    public function actionIndexAjaxShort($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return [
                'success' => false
            ];
        }

        $queryParams = Yii::$app->request->queryParams;
        $seoUrl = $this->_propertyCatalogUrlService->getUrl($queryParams);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $catalogData = $this->_propertyCatalogDataService->getData($queryParams, $categoryItem, $cookieView, PropertyCatalogDataService::MODE_INDEX);

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'url' => $seoUrl,
            'success' => true,
        ];
    }

    /**
     * @return Response
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionSearch()
    {
        return $this->redirect($this->_propertyCatalogUrlService->getUrl(Yii::$app->request->queryParams));
    }

    /**
     * @param $view
     * @return array
     */
    public function actionView($view)
    {
        if (!in_array($view, ['grid', 'list', 'map'])) {
            return ['success' => false];
        }

        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'catalog-view',
            'value' => $view,
            'domain' => '.' . Yii::$app->params['domainName'],
            'expire' => time() + 3600 * 6,
        ]));

        return ['success' => true];
    }
}
