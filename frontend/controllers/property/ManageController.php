<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.11.2018
 * Time: 17:23
 */

namespace frontend\controllers\property;

use common\controllers\FrontEndController;
use common\factories\DynamicFormFactory;
use common\forms\ar\BuildingForm;
use common\helpers\Auth;
use common\helpers\FormHelper;
use common\mappers\DynamicFormValuesMapper;
use common\mappers\TranslationsMapper;
use common\models\AttributeGroup;
use common\models\Building;
use common\models\CompanyMember;
use common\models\Property;
use common\models\User;
use common\repositories\sql\PropertyRepository;
use common\services\BuildingForPropertyService;
use common\services\entities\PropertyService;
use frontend\forms\ar\PropertyForm;
use frontend\forms\StepZeroForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ManageController
 * @package frontend\controllers\property
 */
class ManageController extends FrontEndController
{
    public const ACTION_CREATE = 0;
    public const ACTION_UPDATE = 1;

    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var BuildingForPropertyService
     */
    private $_buildingForPropertyService;

    /**
     * ManageController constructor.
     * @param $id
     * @param Module $module
     * @param PropertyService $propertyService
     * @param PropertyRepository $propertyRepository
     * @param BuildingForPropertyService $buildingForPropertyService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PropertyService $propertyService,
                                PropertyRepository $propertyRepository,
                                BuildingForPropertyService $buildingForPropertyService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyService = $propertyService;
        $this->_propertyRepository = $propertyRepository;
        $this->_buildingForPropertyService = $buildingForPropertyService;

        $this->layout = 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // Пускаем простых юзеров
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return empty(Auth::user()->company_user_id);
                        },
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Проверяем права на создание обьектов
                    [
                        'allow' => false,
                        'actions' => ['create', 'update', 'delete'],
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('createAsCompany');
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed|string|Response
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $propertyForm = new PropertyForm(['_property' => new Property()]);
        $buildingForm = new BuildingForm();

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $propertyForm->load($input);

            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($propertyForm);
            }

            $hasToManipulateBuilding = $this->_buildingForPropertyService->hasToManipulateBuilding($propertyForm->category->category_id);

            $success = $propertyForm->validate();
            if ($success && $hasToManipulateBuilding === true) {
                $buildingForm = $this->_buildingForPropertyService->initForm($buildingForm, $propertyForm);
                if ($buildingForm->_building->isNewRecord === true) {
                    $buildingForm->type = Building::TYPE_DEFAULT;
                    $buildingForm->locale = $propertyForm->locale;
                    $buildingForm->load($input);
                    if ($buildingForm->validate()) {
                        $buildingForm->bindData();
                        if ($success = ($success && $buildingForm->save())) {
                            $propertyForm->_property->building_id = $buildingForm->_building->id;
                        }
                    }
                } else {
                    $propertyForm->_property->building_id = $buildingForm->_building->id;
                }
            }

            if ($success) {
                $propertyForm->bindData();
                if ($propertyForm->save()) {
                    $slug = TranslationsMapper::getMappedData($propertyForm->_property['translations'])['slug'];
                    return $this->redirect(['/property/property/show', 'slug' => $slug]);
                }
            }
        } else {
            $stepZeroForm = new StepZeroForm(['locale' => Yii::$app->language]);

            if ($stepZeroForm->load(Yii::$app->request->get()) && $stepZeroForm->validate()) {
                $propertyForm->load([
                    'CategoryForm' => [
                        'category_id' => $stepZeroForm->category_id
                    ],
                    'PropertyForm' => [
                        'type' => $stepZeroForm->type,
                        'locale' => $stepZeroForm->locale
                    ]
                ]); // проинициализировать DynamicForm
            } else {
                return $this->render('@frontend/views/common/step-zero', [
                    'model' => $stepZeroForm
                ]);
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_PROPERTY, $propertyForm->category->category_id);
        $propertyForm->buildLayout($formConfig);

        return $this->render('form', [
            'propertyForm' => $propertyForm,
            'buildingForm' => $buildingForm,
            'route' => ['/property/manage/create'],
            'action' => self::ACTION_CREATE
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     */
    public function actionUpdate($id)
    {
        /** @var Property $property */
        $property = $this->_propertyRepository->findOneByCriteria(['property.id' => $id]);

        if ($property === null || $property['user_id'] !== Auth::user()->getCurrentId()) {
            throw new NotFoundHttpException('Property Not Found');
        }

        $buildingForm = new BuildingForm();
        $propertyForm = new PropertyForm(['_property' => $property]);
        $propertyDTO = $this->_propertyService->getOne(['property.id' => $id]);
        $propertyForm->prepareUpdate($propertyDTO);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $propertyForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($propertyForm);
            }

            if ($propertyForm->validate()) {
                $propertyForm->bindData();
                if ($propertyForm->save()) {
                    $slug = TranslationsMapper::getMappedData($propertyForm->_property['translations'])['slug'];
                    return $this->redirect(['/property/property/show', 'slug' => $slug]);
                }
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_PROPERTY, $propertyForm->category->category_id);
        unset($formConfig[5]['config'][1]);
        $propertyForm->buildLayout($formConfig);

        return $this->render('form', [
            'propertyForm' => $propertyForm,
            'buildingForm' => $buildingForm,
            'route' => ['/property/manage/update', 'id' => $id],
            'action' => self::ACTION_UPDATE
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var Property $property */
        $property = $this->_propertyRepository->findOneByCriteria(['property.id' => $id]);

        if ($property !== null && $property->user_id === Auth::user()->getCurrentId()) {
            $property->updateAttributes(['status' => Property::STATUS_DELETED, 'updated_at' => time()]);
        }

        return $this->goBack(Yii::$app->request->referrer);
    }

    /**
     * @param $category_id
     * @param $type
     * @param null $entity_id
     * @return string
     */
    public function actionAttributes($category_id, $type, $entity_id = null)
    {
        $dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_PROPERTY_CATEGORY, $category_id, $type);

        if (!empty($entity_id) && !empty($dynamicForm->attributes)) {
            $property = $this->_propertyService->getOne(['property.id' => $entity_id]);
            $dynamicForm->load(DynamicFormValuesMapper::getMappedData($property['attributes'], $dynamicForm->config), '');
        }

        return $this->renderAjax('partial/attributes', [
            'dynamicForm' => $dynamicForm
        ]);
    }
}