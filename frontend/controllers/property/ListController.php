<?php

namespace frontend\controllers\property;

use common\controllers\FrontEndController;
use common\decorators\PropertyStatusDecorator;
use common\helpers\Auth;
use common\models\CompanyMember;
use common\models\Property;
use common\models\User;
use common\repositories\sql\PropertyRepository;
use common\services\entities\PropertyService;
use frontend\mappers\map\PropertyMapMapper;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Class ListController
 * @package frontend\controllers\property
 */
class ListController extends FrontEndController
{
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * ListController constructor.
     * @param string $id
     * @param Module $module
     * @param LocationService $locationService
     * @param PropertyService $propertyService
     * @param PropertyRepository $propertyRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                LocationService $locationService,
                                PropertyService $propertyService,
                                PropertyRepository $propertyRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyService = $propertyService;
        $this->_propertyRepository = $propertyRepository;
        $this->_locationService = $locationService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // Пускаем простых юзеров
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return empty(Auth::user()->company_user_id);
                        },
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $this->layout = 'catalog';

        $params = Yii::$app->request->queryParams;
        $params['user_id'] = Auth::user()->getCurrentId();

        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        if (!array_key_exists('status', $params) || (array_key_exists('status', $params) && $params['status'] === 'all')) {
            $params['status'] = [
                Property::STATUS_ACTIVE,
                Property::STATUS_REQUIRES_MODIFICATION,
                Property::STATUS_REQUIRES_MODERATION
            ];
        }

        $properties = $this->_propertyService->getMany($params, [
            'pagination' => true,
            'perPage' => 36,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $propertyMarkers = PropertyMapMapper::getMappedData($properties['items']);
        $tabs = $this->buildTabHeader();

        return $this->render('index', [
            'properties' => $properties,
            'propertyMarkers' => $propertyMarkers,
            'tabs' => $tabs,
            'lat' => $locationData['lat'],
            'lng' => $locationData['lng'],
        ]);
    }

    /**
     * @return array
     */
    protected function buildTabHeader()
    {
        $statuses = PropertyStatusDecorator::getStatusLabels();
        $condition = ['user_id' => Auth::user()->getCurrentId(), 'status' => [
            Property::STATUS_ACTIVE,
            Property::STATUS_REQUIRES_MODIFICATION,
            Property::STATUS_REQUIRES_MODERATION
        ]];

        $tabs[] = [
            'title' => Yii::t('account', 'All Properties'),
            'count' => $this->_propertyRepository->countByCriteria($condition),
            'active' => 'all' === Yii::$app->request->get('status', 'all'),
            'status' => 'all'
        ];

        foreach ($statuses as $status => $label) {
            $tabs[] = [
                'title' => $label,
                'status' => $status,
                'count' => $this->_propertyRepository->countByCriteria(array_merge($condition, ['status' => $status])),
                'active' => $status === (int)Yii::$app->request->get('status')
            ];
        }

        return $tabs;
    }
}
