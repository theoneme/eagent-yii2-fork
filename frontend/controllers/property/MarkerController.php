<?php

namespace frontend\controllers\property;

use common\controllers\FrontEndController;
use common\dto\advanced\PropertyLightDTO;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\models\Property;
use common\models\Translation;
use common\repositories\sql\PropertyRepository;
use common\services\entities\CategoryService;
use common\services\PropertyMarkerService;
use frontend\services\catalog\LocationToFilterService;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class MarkerController
 * @package frontend\controllers\property
 */
class MarkerController extends FrontEndController
{
    /**
     * @var PropertyRepositoryInterface
     */
    protected $_propertyRepository;
    /**
     * @var CategoryService
     */
    protected $_categoryService;
    /**
     * @var \common\repositories\elastic\PropertyRepository
     */
    protected $_elasticPropertyRepository;
    /**
     * @var LocationToFilterService
     */
    protected $_locationToFilterService;

    /**
     * MarkerController constructor.
     * @param $id
     * @param Module $module
     * @param PropertyRepository $propertyRepository
     * @param CategoryService $categoryService
     * @param \common\repositories\elastic\PropertyRepository $elasticPropertyRepository
     * @param LocationToFilterService $locationToFilterService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PropertyRepository $propertyRepository,
                                CategoryService $categoryService,
                                \common\repositories\elastic\PropertyRepository $elasticPropertyRepository,
                                LocationToFilterService $locationToFilterService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyRepository = $propertyRepository;
        $this->_categoryService = $categoryService;
        $this->_elasticPropertyRepository = $elasticPropertyRepository;
        $this->_locationToFilterService = $locationToFilterService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['catalog', 'account', 'view'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['catalog', 'account', 'view'],
            ]
        ];
    }

    /**
     * @param $category
     * @param $operation
     * @return array
     */
    public function actionCatalog($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return [
                'success' => false
            ];
        }

        $params = Yii::$app->request->queryParams;
        $params['status'] = Property::STATUS_ACTIVE;
        $params['root'] = $categoryItem['root'];
        $params['lft'] = $categoryItem['lft'];
        $params['rgt'] = $categoryItem['rgt'];
        $params = $this->_locationToFilterService->getFilters($params);

        $markerService = new PropertyMarkerService($this->_elasticPropertyRepository);
        $markers = $markerService->getMarkers($params);

        return [
            'success' => true,
            'markers' => $markers['items']
        ];
    }

    /**
     * @param $user
     * @return array
     */
    public function actionAccount($user)
    {
        $params = Yii::$app->request->queryParams;
        $params['user_id'] = $user;

        if (!array_key_exists('status', $params) || (array_key_exists('status', $params) && $params['status'] === 'all')) {
            $params['status'] = [
                Property::STATUS_ACTIVE,
                Property::STATUS_REQUIRES_MODIFICATION,
                Property::STATUS_REQUIRES_MODERATION
            ];
        }

        $markerService = new PropertyMarkerService($this->_elasticPropertyRepository);
        $markers = $markerService->getMarkers($params);

        return [
            'success' => true,
            'markers' => $markers['items']
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionView($id)
    {
        $property = $this->_propertyRepository
            ->with(['translations' => function ($query) {
                /** @var mixed $query */
                return $query->andOnCondition(['property_translations.locale' => [Yii::$app->language, 'en-GB'], 'key' => [Translation::KEY_SLUG, Translation::KEY_TITLE]]);
            }, 'attachments', 'propertyAttributes'])
            ->groupBy('property.id')
            ->findOneByCriteria(['and',
                ['property.id' => $id],
                ['not', ['property.status' => Property::STATUS_DELETED]]
            ], true);

        if ($property === null) {
            return [
                'success' => false
            ];
        }

        $propertyDTO = new PropertyLightDTO($property);
        $property = $propertyDTO->getData();

        $markerData = [
            'price' => $property['price'],
            'image' => $property['image'],
            'url' => Url::to(['/property/property/show', 'slug' => $property['slug']]),
        ];
        if (array_key_exists('bedrooms', $property['attributes'])) {
            $markerData['bedrooms'] = Yii::t('catalog', '{bedrooms, plural, one{# bedroom} other{# bedrooms}}', ['bedrooms' => $property['attributes']['bedrooms']]);
        }
        if (array_key_exists('rooms', $property['attributes'])) {
            $markerData['rooms'] = Yii::t('catalog', '{rooms, plural, one{# room} other{# rooms}}', ['rooms' => $property['attributes']['rooms']]);
        }
        if (array_key_exists('property_area', $property['attributes'])) {
            $markerData['propertyArea'] = Yii::t('catalog', 'Area {area} m²', ['area' => $property['attributes']['property_area']]);
        }

        return [
            'success' => true,
            'marker' => $markerData
        ];
    }
}
