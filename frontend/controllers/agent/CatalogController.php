<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.11.2018
 * Time: 17:08
 */

namespace frontend\controllers\agent;

use common\controllers\FrontEndController;
use common\interfaces\filters\PropertyFilterInterface;
use common\models\Property;
use common\models\User;
use common\services\entities\UserService;
use common\services\MetaService;
use common\services\seo\CatalogSeoService;
use frontend\mappers\map\UserMapMapper;
use frontend\services\elasticsearch\PropertyFilterService;
use frontend\services\LocationService;
use frontend\widgets\AgentFilter;
use Yii;
use yii\base\Module;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\controllers\agent
 */
class CatalogController extends FrontEndController
{
    /**
     * @var string
     */
    public $layout = 'catalog';
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var CatalogSeoService
     */
    private $_catalogSeoService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * CatalogController constructor.
     * @param $id
     * @param Module $module
     * @param CatalogSeoService $catalogSeoService
     * @param LocationService $locationService
     * @param UserService $userService
     * @param array $config
     */
    public function __construct($id, Module $module, CatalogSeoService $catalogSeoService, LocationService $locationService, UserService $userService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userService = $userService;
        $this->_catalogSeoService = $catalogSeoService;
        $this->_locationService = $locationService;
    }

    /**
     * @param null $category
     * @return string
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex($category = null)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        $params = Yii::$app->request->queryParams;
        $params['status'] = User::STATUS_ACTIVE;
//        $params['type'] = User::TYPE_REALTOR;
        $params['lat'] = $params['lat'] ?? $locationData['lat'];
        $params['lng'] = $params['lng'] ?? $locationData['lng'];
        $params['minPropertyCount'] = 2;

        $userData = $this->_userService->getMany($params, [
            'pagination' => true,
            'perPage' => 36,
            'orderBy' => ['user.id' => SORT_DESC]
        ]);

        /** @var PropertyFilterService $filterService */
        $filterService = Yii::$container->get(PropertyFilterInterface::class);
        $filter = new AgentFilter($filterService, [
//            'catalogView' => $view ?? 'grid',
            'queryParams' => $params,
            'locationData' => $locationData
        ]);
        $params['count'] = $userData['pagination']->totalCount;
        $params['attributes'] = $filter->filters;
        $params['location_slug'] = $locationData['slug'];
        $params['location_entity'] = $locationData['entity'];
        $seo = $this->_catalogSeoService->getSeo(CatalogSeoService::TEMPLATE_AGENT, $params);

        $metaService = new MetaService($this);
        $metaService->registerMeta($seo);

        $propertyCounts = Property::find()
            ->select('count(property.id)')
            ->where(['property.user_id' => array_map(function ($var) {
                return $var['id'];
            }, $userData['items'])])
            ->groupBy('property.user_id')
            ->indexBy('user_id')
            ->column();

        $agentMarkers = UserMapMapper::getMappedData($userData['items']);

        return $this->render('index', [
            'agents' => $userData,
            'agentMarkers' => $agentMarkers,
            'seo' => $seo,
            'filter' => $filter,
            'propertyCounts' => $propertyCounts
        ]);
    }

    /**
     * @return Response
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionSearch()
    {
        $queryParams = Yii::$app->request->queryParams;

        if (array_key_exists('city', $queryParams)) {
            $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
            if ($queryParams['city'] !== $locationData['slug']) {
                $queryParams['app_city'] = $queryParams['city'];
                unset($queryParams['city']);
            }
        }

        return $this->redirect(Url::to(array_merge(['/agent/catalog/index'], $queryParams)));
    }
}