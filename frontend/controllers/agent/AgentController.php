<?php

namespace frontend\controllers\agent;

use common\controllers\FrontEndController;
use common\models\Property;
use common\models\Translation;
use common\models\User;
use common\repositories\sql\PropertyRepository;
use common\services\entities\CityService;
use common\services\entities\PropertyService;
use common\services\entities\UserService;
use common\services\MetaService;
use common\services\seo\CatalogSeoService;
use common\services\seo\UserSeoService;
use frontend\forms\contact\ContactAgentForm;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class AgentController
 * @package frontend\controllers
 */
class AgentController extends FrontEndController
{
    /**
     * @var string
     */
    public $layout = 'catalog';
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var CatalogSeoService
     */
    private $_userSeoService;
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;

    /**
     * AgentController constructor.
     * @param $id
     * @param Module $module
     * @param UserSeoService $userSeoService
     * @param UserService $userService
     * @param PropertyService $propertyService
     * @param CityService $cityService
     * @param PropertyRepository $propertyRepository
     * @param array $config
     */
    public function __construct($id, Module $module, UserSeoService $userSeoService, UserService $userService, PropertyService $propertyService, CityService $cityService, PropertyRepository $propertyRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userService = $userService;
        $this->_propertyService = $propertyService;
        $this->_userSeoService = $userSeoService;
        $this->_cityService = $cityService;
        $this->_propertyRepository = $propertyRepository;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view-ajax-short'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view-ajax-short'],
            ]
        ];
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionView($id)
    {
        $agent = $this->_userService->getOne(['and',
            ['id' => $id],
            ['not', ['user.status' => User::STATUS_DELETED]]
        ]);
        if ($agent === null) {
            Yii::$app->session->setFlash('error', Yii::t('main', 'Requested agent page is not found'));
            return $this->redirect(['/site/index']);
        }
        $seo = $this->_userSeoService->getSeo(UserSeoService::TEMPLATE_AGENT, [
            'name' => $agent['username'],
            'description' => isset($agent['translations'][Translation::KEY_DESCRIPTION]) ? StringHelper::truncate($agent['translations'][Translation::KEY_DESCRIPTION], 200) : ''
        ]);
        $metaService = new MetaService($this);
        $seo['image'] = $agent['avatar'];
        $metaService->registerMeta($seo);
        $properties = $this->_propertyService->getMany([
            'status' => Property::STATUS_ACTIVE,
            'user_id' => $agent['id'],
            'page' => Yii::$app->request->get('page'),
            'per-page' => Yii::$app->request->get('per-page'),
            'ads_allowed' => true
        ], [
            'pagination' => true,
            'perPage' => 5,
            'orderBy' => ['id' => SORT_DESC]
        ]);
        $properties['pagination']->params = Yii::$app->request->get();
        $properties['pagination']->getPage(true);
        $soldProperties = ['items' => [], 'pagination' => null];
        $reviews = [];

        $showLoginModal = Yii::$app->request->get('showLoginModal');

        $cities = [];

        $propertyCities = array_unique(array_filter($this->_propertyRepository->findColumnByCriteria(['and',
            ['user_id' => $agent['id']],
            ['not', ['status' => Property::STATUS_DELETED]]
        ], 'city_id')));
        if (!empty($propertyCities)) {
            $cities = $this->_cityService->getMany(['id' => $propertyCities])['items'];
        }
        $categorySlugs = array_map(function ($var) {
            return $var[Yii::$app->language] ?? null;
        }, Yii::$app->cacheLayer->getCategoryAliasCache('ru-RU'));

        $contactAgentForm = new ContactAgentForm();

        return $this->render('view', [
            'agent' => $agent,
            'properties' => $properties,
            'soldProperties' => $soldProperties,
            'reviews' => $reviews,
            'showLoginModal' => $showLoginModal,
            'contactAgentForm' => $contactAgentForm,

            'cities' => $cities,
            'categorySlugs' => $categorySlugs
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionViewAjaxShort($id)
    {
        $agent = $this->_userService->getOne(['and',
            ['id' => $id],
            ['not', ['user.status' => User::STATUS_DELETED]]
        ]);

        $properties = $this->_propertyService->getMany([
            'user_id' => $id,
            'page' => Yii::$app->request->get('page'),
            'per-page' => Yii::$app->request->get('per-page'),
            'ads_allowed' => true
        ], [
            'pagination' => true,
            'perPage' => 5,
            'orderBy' => ['id' => SORT_DESC]
        ]);
        $properties['pagination']->params = Yii::$app->request->get();
        $properties['pagination']->getPage(true);

        return [
            'catalog' => $this->render('property-active', [
                'properties' => $properties,
                'agent' => $agent
            ]),
            'success' => true,
            'url' => Url::to(array_merge(['/agent/agent/view', 'id' => $id], Yii::$app->request->queryParams))
        ];
    }
}
