<?php

namespace frontend\controllers\agent;

use common\controllers\FrontEndController;
use common\mappers\LocationBoxFilterMapper;
use common\mappers\LocationFilterMapper;
use common\services\AgentMarkerService;
use common\services\entities\UserService;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class MarkerController
 * @package frontend\controllers\agent
 */
class MarkerController extends FrontEndController
{
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * MarkerController constructor.
     * @param $id
     * @param Module $module
     * @param UserService $userService
     * @param LocationService $locationService
     * @param array $config
     */
    public function __construct($id, Module $module, UserService $userService, LocationService $locationService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userService = $userService;
        $this->_locationService = $locationService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view', 'catalog'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view', 'catalog'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionCatalog()
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        $params = Yii::$app->request->queryParams;
//        $params['type'] = User::TYPE_REALTOR;
        if (array_key_exists('box', $params)) {
            $params = array_merge($params, LocationBoxFilterMapper::getMappedData($params['box']));
        } else {
            $params = array_merge($params, LocationFilterMapper::getMappedData($locationData));
        }

        $markerService = new AgentMarkerService();
        $markers = $markerService->getMarkers($params);

        return [
            'success' => true,
            'markers' => $markers['items']
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionView($id)
    {
        $user = $this->_userService->getOne(['id' => $id]);

        if ($user === null) {
            return [
                'success' => false
            ];
        }

        $markerData = [
            'name' => $user['username'],
            'image' => $user['avatar'],
            'url' => Url::to(['/agent/agent/view', 'id' => $id])
        ];

        return [
            'success' => true,
            'marker' => $markerData
        ];
    }
}
