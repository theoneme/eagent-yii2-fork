<?php

namespace frontend\controllers;

use common\controllers\FrontEndController;
use common\services\MetaService;
use common\services\seo\IndexCrosslinkService;
use common\services\seo\PageSeoService;
use frontend\services\IndexPageService;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;

/**
 * Class SiteController
 * @package frontend\controllers
 */
class SiteController extends FrontEndController
{
    /**
     * @var PageSeoService
     */
    private $_pageSeoService;
    /**
     * @var IndexCrosslinkService
     */
    private $_indexCrosslinkService;
    /**
     * @var IndexPageService
     */
    private $_indexPageService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * SiteController constructor.
     * @param $id
     * @param Module $module
     * @param PageSeoService $pageSeoService
     * @param IndexCrosslinkService $indexCrosslinkService
     * @param IndexPageService $indexPageService
     * @param LocationService $locationService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PageSeoService $pageSeoService,
                                IndexCrosslinkService $indexCrosslinkService,
                                IndexPageService $indexPageService,
                                LocationService $locationService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_pageSeoService = $pageSeoService;
        $this->_indexCrosslinkService = $indexCrosslinkService;
        $this->_indexPageService = $indexPageService;
        $this->_locationService = $locationService;
    }

    /**
     * @param null $operation
     * @return string
     */
    public function actionIndex($operation = null)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        $categorySlugs = array_map(
            function ($var) {
                return $var[Yii::$app->language] ?? null;
            },
            Yii::$app->cacheLayer->getCategoryAliasCache('ru-RU')
        );

        $seoParams['location_slug'] = $locationData['slug'];
        $seoParams['location_entity'] = $locationData['entity'];
        $seoParams['minPrice'] = PageSeoService::getMinPriceByOperation($operation);

        $seo = $this->_pageSeoService->getSeo(PageSeoService::getTemplateByOperation($operation), $seoParams);
        $metaService = new MetaService($this);
        $metaService->registerMeta($seo);

        $crosslinkData = $this->_indexCrosslinkService->getData($locationData);
        $data = $this->_indexPageService->getData();

        return $this->render('index', [
            'properties' => $data['properties'],
            'localBuildings' => $data['localBuildings'],
            'globalBuildings' => $data['globalBuildings'],
            'address' => $locationData['address'],
            'operation' => $operation,
            'crosslinkData' => $crosslinkData,
            'categorySlugs' => $categorySlugs
        ]);
    }

    /**
     * @return string
     */
    public function actionContact()
    {
        $this->layout = 'catalog';

        return $this->render('contact');
    }
}
