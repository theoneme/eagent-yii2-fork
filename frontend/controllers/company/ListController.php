<?php

namespace frontend\controllers\company;

use common\controllers\FrontEndController;
use common\models\CompanyMember;
use common\models\User;
use common\services\entities\CompanyMemberService;
use frontend\services\CompaniesByMemberService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;

/**
 * Class ListController
 * @package frontend\controllers\company
 */
class ListController extends FrontEndController
{
    /**
     * @var CompanyMemberService
     */
    private $_companyMemberService;
    /**
     * @var CompaniesByMemberService
     */
    private $_companiesByMemberService;

    /**
     * ListController constructor.
     * @param string $id
     * @param Module $module
     * @param CompaniesByMemberService $companiesByMemberService
     * @param CompanyMemberService $companyMemberService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                CompaniesByMemberService $companiesByMemberService,
                                CompanyMemberService $companyMemberService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_companiesByMemberService = $companiesByMemberService;
        $this->_companyMemberService = $companyMemberService;
    }

//    /**
//     * @inheritdoc
//     */
//    public function behaviors()
//    {
//        return [
//            'companyAccess' => [
//                'class' => AccessControl::class,
//                'only' => ['members'],
//                'rules' => [
//                    [
//                        'actions' => ['members'],
//                        'class' => CompanyAccessRule::class,
//                        'companyField' => 'company_id'
//                    ]
//                ],
//            ],
//            'access' => [
//                'class' => AccessControl::class,
//                'only' => ['index'],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => ['index'],
//                        'roles' => ['@']
//                    ],
//                ],
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // В эти экшены пускаем без выбора Компании
                    [
                        'allow' => true,
                        'actions' => ['index'],
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function ($rule, $action) {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = empty($user->company_user_id) || $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function ($rule, $action) {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];

                            return $member === null || !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $this->layout = 'catalog';

        $params = Yii::$app->request->queryParams;
        $params['user_id'] = Yii::$app->user->getId();

        $data = $this->_companiesByMemberService->getCompanies($params, [
            'pagination' => true
        ]);

        return $this->render('index', [
            'data' => $data
        ]);
    }

    /**
     * @param $company_id
     * @return string
     */
    public function actionMembers($company_id)
    {
        $this->layout = 'catalog';

        $params = Yii::$app->request->queryParams;
        $members = $this->_companyMemberService->getMany($params, [
            'pagination' => true,
            'perPage' => 36,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        return $this->render('members', [
            'members' => $members,
            'companyId' => $company_id,
            'currentMember' => Yii::$app->params['runtime']['companyMember']
        ]);
    }
}
