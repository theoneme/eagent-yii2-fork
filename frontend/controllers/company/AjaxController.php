<?php

namespace frontend\controllers\company;

use common\controllers\FrontEndController;
use common\forms\ar\composite\ContactForm;
use common\repositories\sql\CompanyRepository;
use common\repositories\sql\UserRepository;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\controllers\company
 */
class AjaxController extends FrontEndController
{
    /**
     * @var CompanyRepository
     */
    private $_companyRepository;
    /**
     * @var UserRepository
     */
    private $_userRepository;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param CompanyRepository $companyRepository
     * @param UserRepository $userRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, CompanyRepository $companyRepository, UserRepository $userRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_companyRepository = $companyRepository;
        $this->_userRepository = $userRepository;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-contact', 'profile-switch'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['render-contact', 'profile-switch'],
            ]
        ];
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderContact($iterator)
    {
        $contactForm = new ContactForm();

        return [
            'html' => $this->renderAjax('contact', [
                'contactForm' => $contactForm,
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @return array
     */
    public function actionProfileSwitch()
    {
        $result = ['success' => true];
        $input = Yii::$app->request->post('identity');

        if ($input !== null) {
            $hasAccess = $this->_companyRepository->joinWith(['companyMembers'])->existsByCriteria([
                'company.user_id' => (int)$input,
                'company_member.user_id' => Yii::$app->user->getId()
            ]);

            if ($hasAccess === true) {
                $this->_userRepository->updateOneByCriteria(['id' => Yii::$app->user->getId()], ['company_user_id' => $input, 'updated_at' => time()], UserRepository::MODE_LIGHT);

                return $result;
            }

            return ['success' => false];
        }

        $this->_userRepository->updateOneByCriteria(['id' => Yii::$app->user->getId()], ['company_user_id' => null, 'updated_at' => time()], UserRepository::MODE_LIGHT);

        return $result;
    }
}
