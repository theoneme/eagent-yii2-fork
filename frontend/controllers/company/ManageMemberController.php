<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.11.2018
 * Time: 17:23
 */

namespace frontend\controllers\company;

use common\controllers\FrontEndController;
use common\dto\CompanyMemberDTO;
use common\forms\ar\CompanyMemberForm;
use common\models\CompanyMember;
use common\models\Property;
use common\models\User;
use common\repositories\sql\CompanyMemberRepository;
use common\repositories\sql\CompanyRepository;
use common\services\entities\CompanyMemberService;
use common\services\entities\UserService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class ManageMemberController
 * @package frontend\controllers\company
 */
class ManageMemberController extends FrontEndController
{
    /**
     * @var CompanyRepository
     */
    private $_companyMemberRepository;
    /**
     * @var CompanyMemberService
     */
    private $_userService;

    /**
     * ManageMemberController constructor.
     * @param $id
     * @param Module $module
     * @param UserService $userService
     * @param CompanyMemberRepository $companyMemberRepository
     * @param array $config
     */
    public function __construct($id, Module $module, UserService $userService, CompanyMemberRepository $companyMemberRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_companyMemberRepository = $companyMemberRepository;
        $this->_userService = $userService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function ($rule, $action) {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = empty($user->company_user_id) || $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function ($rule, $action) {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Проверяем права на управление компанией
                    [
                        'allow' => false,
                        'actions' => ['update', 'delete', 'create'],
                        'matchCallback' => function ($rule, $action) {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return $member->company_id !== (int)Yii::$app->request->get('company_id') || !$member->can('manageCompanyMembers');
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $company_id
     * @return mixed|string|Response
     */
    public function actionCreate($company_id)
    {
        $this->layout = 'catalog';

        $form = new CompanyMemberForm(['_companyMember' => new CompanyMember()]);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $input['CompanyMemberForm']['company_id'] = $company_id;
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $form->save();
                return $this->redirect(['/company/list/members', 'company_id' => $form->_companyMember->company_id]);
            }
        }

        return $this->render('form', [
            'companyMemberForm' => $form,
            'selectizeData' => [],
            'action' => ['/company/manage-member/create', 'company_id' => $company_id],
        ]);
    }

    /**
     * @param $id
     * @param $company_id
     * @return array|string|Response
     */
    public function actionUpdate($id, $company_id)
    {
        $this->layout = 'catalog';

        $companyMember = $this->_companyMemberRepository->findOneByCriteria(['company_member.id' => $id]);
        $form = new CompanyMemberForm(['_companyMember' => $companyMember]);

        $companyMemberDTO = new CompanyMemberDTO($companyMember);
        $companyMemberData = $companyMemberDTO->getData();
        $form->prepareUpdate($companyMemberData);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $form->save();
                return $this->redirect(['/company/list/members', 'company_id' => $companyMemberData['company_id']]);
            }
        }

        $user = $this->_userService->getOne(['id' => $companyMemberData['user_id']]);
        $selectizeData = [$companyMemberData['user_id'] => "{$user['username']} (ID: {$user['id']}; Email: {$user['email']})"];

        return $this->render('form', [
            'companyMemberForm' => $form,
            'selectizeData' => $selectizeData,
            'action' => ['/company/manage-member/update', 'id' => $id, 'company_id' => $company_id],
        ]);
    }

    /**
     * @param $id
     * @param $company_id
     * @return Response
     */
    public function actionDelete($id, $company_id)
    {
        /** @var Property $property */
        $member = $this->_companyMemberRepository->findOneByCriteria(['id' => $id]);

        if ($member !== null) {
            $member->delete();
        }

        return $this->goBack(Yii::$app->request->referrer);
    }
}