<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.11.2018
 * Time: 17:23
 */

namespace frontend\controllers\company;

use common\controllers\FrontEndController;
use common\dto\CompanyDTO;
use common\helpers\FormHelper;
use common\models\Company;
use common\models\CompanyMember;
use common\models\Property;
use common\models\User;
use common\repositories\sql\CompanyRepository;
use common\services\entities\CompanyMemberService;
use frontend\forms\ar\CompanyForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ManageController
 * @package frontend\controllers\company
 */
class ManageController extends FrontEndController
{
    public const ACTION_CREATE = 0;
    public const ACTION_UPDATE = 1;

    /**
     * @var CompanyRepository
     */
    private $_companyRepository;
    /**
     * @var CompanyMemberService
     */
    private $_companyMemberService;

    /**
     * ManageController constructor.
     * @param $id
     * @param Module $module
     * @param CompanyRepository $companyRepository
     * @param CompanyMemberService $companyMemberService
     * @param array $config
     */
    public function __construct($id, Module $module, CompanyRepository $companyRepository, CompanyMemberService $companyMemberService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_companyRepository = $companyRepository;
        $this->_companyMemberService = $companyMemberService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // В эти экшены пускаем без выбора Компании
                    [
                        'allow' => true,
                        'actions' => ['create'],
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = empty($user->company_user_id) || $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Проверяем права на управление компанией
                    [
                        'allow' => false,
                        'actions' => ['update', 'delete'],
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return $member->company_id !== (int)Yii::$app->request->get('id') || !$member->can('manageCompany');
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed|string
     */
    public function actionCreate()
    {
        $this->layout = 'catalog';

        $form = new CompanyForm(['_company' => new Company()]);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($form);
            }

            if ($form->validate()) {
                $form->bindData();

                if ($form->save()) {
                    $this->_companyMemberService->create([
                        'CompanyMemberForm' => [
                            'company_id' => $form->_company->id,
                            'user_id' => Yii::$app->user->getId(),
                            'role' => CompanyMember::ROLE_OWNER
                        ]
                    ]);

                    return $this->redirect(['/agent/agent/view', 'id' => $form->_company->user_id]);
                }
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_COMPANY);
        $form->buildLayout($formConfig);

        return $this->render('form', [
            'companyForm' => $form,
            'route' => ['/company/manage/create'],
            'action' => self::ACTION_CREATE
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $this->layout = 'catalog';

        $form = new CompanyForm();
        /** @var Company $company */
        $company = $this->_companyRepository->findOneByCriteria(['company.id' => $id]);
        if ($company === null) {
            throw new NotFoundHttpException('Company Not Found');
        }

        $form->_company = $company;
        $companyDTO = new CompanyDTO($company);
        $companyData = $companyDTO->getData();
        $form->prepareUpdate($companyData);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($form);
            }

            if ($form->validate()) {
                $form->bindData();
                $form->save();

                return $this->redirect(['/agent/agent/view', 'id' => $form->_company->user_id]);
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_COMPANY);
        $form->buildLayout($formConfig);

        return $this->render('form', [
            'companyForm' => $form,
            'companyDTO' => $companyData,
            'route' => ['/company/manage/update', 'id' => $id],
            'action' => self::ACTION_UPDATE
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var Company $company */
        $company = $this->_companyRepository->findOneByCriteria(['company.id' => $id]);

        if ($company !== null) {
            $company->updateAttributes(['status' => Property::STATUS_DELETED, 'updated_at' => time()]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}