<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 20:16
 */

namespace frontend\controllers\request;

use common\controllers\FrontEndController;
use common\helpers\Auth;
use common\helpers\DataHelper;
use common\helpers\FormHelper;
use common\interfaces\repositories\RequestRepositoryInterface;
use common\models\CompanyMember;
use common\models\Property;
use common\models\Request;
use common\models\User;
use common\repositories\sql\RequestRepository;
use common\services\entities\RequestService;
use frontend\forms\ar\RequestForm;
use frontend\forms\StepZeroForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ManageController
 * @package frontend\controllers\request
 */
class ManageController extends FrontEndController
{
    public const ACTION_CREATE = 0;
    public const ACTION_UPDATE = 1;

    /**
     * @var RequestService
     */
    private $_requestService;
    /**
     * @var RequestRepositoryInterface
     */
    private $_requestRepository;

    /**
     * RequestController constructor.
     * @param $id
     * @param Module $module
     * @param RequestService $requestService
     * @param RequestRepository $requestRepository
     * @param array $config
     */
    public function __construct($id, Module $module, RequestService $requestService, RequestRepository $requestRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_requestRepository = $requestRepository;
        $this->_requestService = $requestService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // Пускаем простых юзеров
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return empty(Auth::user()->company_user_id);
                        },
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Проверяем права на создание обьектов
                    [
                        'allow' => false,
                        'actions' => ['create', 'update', 'delete'],
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('createAsCompany');
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed|string|Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $this->layout = 'catalog';
        $categories = DataHelper::getCategories();

        $requestForm = new RequestForm();
        $requestForm->_request = new Request();
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $requestForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($requestForm);
            }

            if ($requestForm->validate()) {
                $requestForm->bindData();
                $requestForm->save();

                return $this->redirect(['/request/list/index']);
            }
        } else {
            $stepZeroForm = new StepZeroForm();
            if ($stepZeroForm->load(Yii::$app->request->get()) && $stepZeroForm->validate()) {
                $requestForm->load([
                    'CategoryForm' => [
                        'category_id' => $stepZeroForm->category_id
                    ],
                    'RequestForm' => [
                        'type' => $stepZeroForm->type,
                        'locale' => $stepZeroForm->locale
                    ]
                ]); // проинициализировать DynamicForm
            } else {
                return $this->render('@frontend/views/common/step-zero', [
                    'model' => $stepZeroForm
                ]);
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_REQUEST, $requestForm->category->category_id);
        $requestForm->buildLayout($formConfig);

        return $this->render('form', [
            'requestForm' => $requestForm,
            'categories' => $categories,
            'route' => ['/request/manage/create'],
            'action' => self::ACTION_CREATE
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     */
    public function actionUpdate($id)
    {
        $this->layout = 'catalog';
        $categories = DataHelper::getCategories();

        $requestForm = new RequestForm();
        /** @var Property $request */
        $request = $this->_requestRepository->findOneByCriteria(['request.id' => $id]);

        if ($request === null) {
            throw new NotFoundHttpException('Request Not Found');
        }

        $requestForm->_request = $request;
        $requestDTO = $this->_requestService->getOne(['request.id' => $id]);
        $requestForm->prepareUpdate($requestDTO);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $requestForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($requestForm);
            }

            if ($requestForm->validate()) {
                $requestForm->bindData();
                $requestForm->save();

                return $this->redirect(['/request/list/index']);
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_REQUEST, $requestForm->category->category_id);
        $requestForm->buildLayout($formConfig);

        return $this->render('form', [
            'requestForm' => $requestForm,
            'categories' => $categories,
            'route' => ['/request/manage/update', 'id' => $id],
            'action' => self::ACTION_UPDATE
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var Request $request */
        $request = $this->_requestRepository->findOneByCriteria(['request.id' => $id]);

        if ($request !== null && $request->user_id === Auth::user()->getCurrentId()) {
            $request->updateAttributes(['status' => Request::STATUS_DELETED, 'updated_at' => time()]);
        }

        return $this->goBack(Yii::$app->request->referrer);
    }
}