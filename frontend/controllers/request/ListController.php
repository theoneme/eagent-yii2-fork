<?php

namespace frontend\controllers\request;

use common\controllers\FrontEndController;
use common\decorators\PropertyStatusDecorator;
use common\helpers\Auth;
use common\models\CompanyMember;
use common\models\Request;
use common\models\User;
use common\repositories\sql\RequestRepository;
use common\services\entities\RequestService;
use frontend\mappers\map\PropertyMapMapper;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;

/**
 * Class ListController
 * @package frontend\controllers\request
 */
class ListController extends FrontEndController
{
    /**
     * @var RequestService
     */
    private $_requestService;
    /**
     * @var RequestRepository
     */
    private $_requestRepository;

    /**
     * ListController constructor.
     * @param string $id
     * @param Module $module
     * @param RequestService $requestService
     * @param RequestRepository $requestRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, RequestService $requestService, RequestRepository $requestRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_requestService = $requestService;
        $this->_requestRepository = $requestRepository;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // Пускаем простых юзеров
                    [
                        'allow' => true,
                        'matchCallback' => function () {
                            return empty(Auth::user()->company_user_id);
                        },
                    ],
                    // Для остальных проверяем, что у юзера выбрана Компания
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = $user->currentCompany === null || !array_key_exists($user->id, $user->currentCompany->companyMembers);
                            if (!$result) {
                                Yii::$app->params['runtime']['companyMember'] = $user->currentCompany->companyMembers[$user->id];
                            }
                            return $result;
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function () {
                            /* @var CompanyMember $member */
                            $member = Yii::$app->params['runtime']['companyMember'];
                            return !$member->can('viewCompany', ['member' => $member, 'company' => $member->company]);
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'catalog';

        $params = Yii::$app->request->queryParams;
        $params['user_id'] = Auth::user()->getCurrentId();
        if (!array_key_exists('status', $params) || (array_key_exists('status', $params) && $params['status'] === 'all')) {
            $params['status'] = [
                Request::STATUS_ACTIVE,
                Request::STATUS_REQUIRES_MODIFICATION,
                Request::STATUS_REQUIRES_MODERATION
            ];
        }

        $items = $this->_requestService->getMany($params, [
            'pagination' => true,
            'perPage' => 36,
            'orderBy' => new Expression('request.created_at DESC')
        ]);

        $markers = PropertyMapMapper::getMappedData($items['items']);

        $tabs = $this->buildTabHeader();

        return $this->render('index', [
            'items' => $items,
            'markers' => $markers,
            'tabs' => $tabs
//            'search' => $search
//            'tabHeader' => $this->buildTabHeader($searchModel)
        ]);
    }

    /**
     * @return array
     */
    protected function buildTabHeader()
    {
        $statuses = PropertyStatusDecorator::getStatusLabels();
        $condition = ['user_id' => Auth::user()->getCurrentId(), 'status' => [
            Request::STATUS_ACTIVE,
            Request::STATUS_REQUIRES_MODIFICATION,
            Request::STATUS_REQUIRES_MODERATION
        ]];

        $tabs[] = [
            'title' => Yii::t('account', 'All Properties'),
            'count' => $this->_requestRepository->countByCriteria($condition),
            'active' => 'all' === Yii::$app->request->get('status', 'all'),
            'status' => 'all'
        ];

        foreach ($statuses as $status => $label) {
            $tabs[] = [
                'title' => $label,
                'status' => $status,
                'count' => $this->_requestRepository->countByCriteria(array_merge($condition, ['status' => $status])),
                'active' => $status === (int)Yii::$app->request->get('status')
            ];
        }

        return $tabs;
    }
}
