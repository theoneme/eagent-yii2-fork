<?php

namespace frontend\controllers\request;

use common\controllers\FrontEndController;
use common\interfaces\FilterInterface;
use common\models\Request;
use common\services\entities\PropertyService;
use common\services\entities\RequestService;
use frontend\mappers\map\RequestMapMapper;
use frontend\services\LocationService;
use frontend\widgets\RequestFilter;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\controllers\request
 */
class CatalogController extends FrontEndController
{
    /**
     * @var PropertyService
     */
    private $_requestService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * CatalogController constructor.
     * @param $id
     * @param Module $module
     * @param RequestService $requestService
     * @param LocationService $locationService
     * @param array $config
     */
    public function __construct($id, Module $module, RequestService $requestService, LocationService $locationService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_requestService = $requestService;
        $this->_locationService = $locationService;

        $this->layout = 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view'],
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        $params = Yii::$app->request->queryParams;
        $params['status'] = Request::STATUS_ACTIVE;

        $requests = $this->_requestService->getMany($params, [
            'pagination' => true,
            'perPage' => 36,
            'orderBy' => ['created_at' => SORT_DESC]
        ]);

        /** @var FilterInterface $propertyFilterService */
        $requestFilter = new RequestFilter([
            'catalogView' => 'grid',
            'queryParams' => $params,
            'locationData' => $locationData,
            'resultCount' => $requests['pagination']->totalCount
        ]);

        $requestMarkers = RequestMapMapper::getMappedData($requests['items']);

        return $this->render('index-table', [
            'requests' => $requests,
            'requestMarkers' => $requestMarkers,
            'requestFilter' => $requestFilter,
            'lat' => $locationData['lat'],
            'lng' => $locationData['lng']
        ]);
    }
}
