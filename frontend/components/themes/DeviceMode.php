<?php

namespace frontend\components\themes;

use Detection\MobileDetect;
use yii\web\Request;

/**
 * Class DeviceMode
 * @package frontend\components\themes
 */
class DeviceMode
{
    /**
     * @var MobileDetect
     */
    private $detect;

    /**
     * DeviceMode constructor.
     * @param MobileDetect $detect
     */
    public function __construct(MobileDetect $detect)
    {
        $this->detect = $detect;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isMobile(Request $request): bool
    {
        return
            $this->detect->isMobile($request->getUserAgent(), $request->getHeaders()) ||
            $this->detect->isTablet($request->getUserAgent(), $request->getHeaders());
    }
}