<?php

namespace frontend\components\themes\filters;

use frontend\components\themes\ClientMode;
use Yii;
use yii\base\ActionFilter;
use yii\helpers\Url;
use yii\web\Cookie;

/**
 * Class ViewModeFilter
 * @package frontend\components\themes\filters
 */
class ViewModeFilter extends ActionFilter
{
    /**
     * @var int
     */
    public $expire = 3600;

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action): bool
    {
        if ($mode = Yii::$app->request->get('mode')) {
            $response = Yii::$app->response;
            $response->cookies->add(new Cookie([
                'name' => ClientMode::COOKIE_NAME,
                'value' => $mode,
                'expire' => time() + $this->expire,
                'domain' => '.' . Yii::$app->params['domainName'],
            ]));
            $response->redirect(Url::current(['mode' => null]), 301);

            return false;
        }

        return true;
    }
}