<?php

namespace frontend\components\themes;

use yii\web\Request;

/**
 * Class ClientMode
 * @package frontend\components
 */
class ClientMode
{
    public const COOKIE_NAME = 'mode';

    public const MODE_MOBILE = 'mobile';
    public const MODE_DESKTOP = 'desktop';

    /**
     * @param Request $request
     * @return bool
     */
    public function isMobile(Request $request): bool
    {
        return $this->is($request, self::MODE_MOBILE);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isDesktop(Request $request): bool
    {
        return $this->is($request, self::MODE_DESKTOP);
    }

    /**
     * @param Request $request
     * @param $mode
     * @return bool
     */
    private function is(Request $request, $mode): bool
    {
        return $request->getCookies()->getValue(self::COOKIE_NAME) === $mode;
    }
}