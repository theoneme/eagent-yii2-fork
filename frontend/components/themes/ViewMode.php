<?php

namespace frontend\components\themes;

use yii\web\Request;

/**
 * Class ClientMode
 * @package frontend\components
 */
class ViewMode
{
    /**
     * @var ClientMode
     */
    private $client;
    /**
     * @var DeviceMode
     */
    private $device;

    /**
     * ViewMode constructor.
     * @param ClientMode $client
     * @param DeviceMode $device
     */
    public function __construct(ClientMode $client, DeviceMode $device)
    {
        $this->client = $client;
        $this->device = $device;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function isMobile(Request $request): bool
    {
        return
            $this->client->isMobile($request) ||
            (!$this->client->isDesktop($request) && $this->device->isMobile($request));
    }
}