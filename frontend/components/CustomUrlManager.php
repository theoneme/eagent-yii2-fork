<?php

namespace frontend\components;

use Yii;
use yii\base\InvalidConfigException;
use yii\web\UrlManager;

/**
 * Class CustomUrlManager
 * @package frontend\components
 */
class CustomUrlManager extends UrlManager
{
    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!array_key_exists('domainName', Yii::$app->params)) {
            throw new InvalidConfigException('Domain name must be configured in params');
        }
        if (!empty($this->rules)) {
            $this->rules = $this->modifyRules($this->rules);
        }

        parent::init();
    }

    /**
     * @param array $rules
     * @param bool $append
     */
    public function addRules($rules, $append = true)
    {
        $rules = $this->modifyRules($rules);
        parent::addRules($rules, $append);
    }

    /**
     * @param $rules
     * @return array
     */
    protected function modifyRules($rules)
    {
        $additionalRules = [];

        foreach ($rules as $pattern => $route) {
            if (is_array($route) && array_key_exists('pattern', $route) && array_key_exists('route', $route)) {
                $pattern = $route['pattern'];
                if (!preg_match('/\/\//', $pattern)) {
                    $pattern = '//<app_city>.' . Yii::$app->params['domainName'] . (strpos($pattern, '/') === 0 ? '' : '/') . $pattern;

                    $additionalRules[] = [
                        'pattern' => $pattern,
                        'route' => $route['route'],
                        'suffix' => $route['suffix'] ?? null
                    ];
                }
            } else if (!preg_match('/\/\//', $pattern)) {
                $additionalRules['//<app_city>.' . Yii::$app->params['domainName'] . (strpos($pattern, '/') === 0 ? '' : '/') . $pattern] = $route;
            }
        }

        return array_merge($additionalRules, $rules);
    }
}