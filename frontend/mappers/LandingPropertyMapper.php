<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;
use common\models\AttributeFilter;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class LandingPropertyMapper
 * @package frontend\mappers
 */
class LandingPropertyMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        $data = [];

        foreach($rawData['properties'] as $property) {
            $data[] = [
                'buildingName' => $rawData['buildings'][$property['building_id']]['name'] ?? null,
                'rooms' => $property['attributes']['rooms'] ?? null,
                'area' => $property['attributes']['property_area'] ?? null,
                'installment' => $rawData['buildings'][$property['building_id']]['attributes']['new_construction_installment_plan'] ?? null,
                'discount' => $rawData['buildings'][$property['building_id']]['attributes']['new_construction_discount'] ?? null,
                'slug' => $property['slug'],
                'thumb' => $property['image'],
                'sourcePrice' => $property['source_price'],
                'buildingId' => $property['building_id']
            ];
        }

        return $data;
    }
}