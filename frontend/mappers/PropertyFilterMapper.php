<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;
use common\models\AttributeFilter;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class PropertyFilterMapper
 * @package common\mappers
 */
class PropertyFilterMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        if (!array_key_exists('priceChunks', Yii::$app->params)) {
            throw new InvalidConfigException('Missing price chunks configuration');
        }

        $chunks = Yii::$app->params['priceChunks'];
        $data = [];

        $data['category']['title'] = Yii::t('catalog', 'Category');
        $data['category']['type'] = AttributeFilter::TYPE_RADIOLIST;
        if (array_key_exists('category', $rawData['params'])) {
            $data['category']['checked'] = $rawData['params']['category'];
        } else {
            $data['category']['checked'] = false;
        }

        $data['category']['values'] = self::extractCategories($rawData['categories']);
        $data['category'] = array_merge($data['category'], self::extractCheckedData($rawData['categories'], $data['category']['checked']));

        $price = $rawData['prices']['min'];
        $maxOptions = 9;
        $data['price']['left']['values'][0] = 0;
        $data['price']['left']['min'] = $rawData['prices']['min'];
        $data['price']['right']['max'] = $rawData['prices']['max'];
        for ($i = 0; $i < $maxOptions * 2; $i++) {
            foreach ($chunks as $chunk) {
                if ($i < $maxOptions) {
                    $key = 'left';
                } else {
                    $key = 'right';
                }

                if ($price > $chunk['range']['min'] && $price <= $chunk['range']['max']) {
                    $data['price'][$key]['values'][$i + 1] = ceil($price / $chunk['step']) * $chunk['step'];
                    $price += $chunk['step'];
                    break;
                }
            }
        }
        $data['price']['right']['values'][$i + 1] = null;
        $data['price']['left']['checked'] = $rawData['params']['price']['min'] ?? null;
        $data['price']['right']['checked'] = $rawData['params']['price']['max'] ?? null;

        $data['operation']['title'] = Yii::t('catalog', 'I`m interested in');
        $data['operation']['checked'] = $rawData['params']['operation'];
        $data['operation']['values'] = [
            'sale' => [
                'title' => Yii::t('catalog', 'Buy'),
                'count' => null,
            ],
            'rent' => [
                'title' => Yii::t('catalog', 'Rent'),
                'count' => null
            ]
        ];

        $attributes = array_intersect_key($rawData['attributes'], $rawData['formatted']);
        foreach ($attributes as $attributeKey => $attribute) {
            $data[$attribute['alias']]['title'] = $attribute['translations']['title'];
            $data[$attribute['alias']]['type'] = array_key_exists($attributeKey, $rawData['attributeFilters'])
                ? AttributeFilter::$typeToView[$rawData['attributeFilters'][$attributeKey]['type']]
                : AttributeFilter::TYPE_DROPDOWN;

            if ((int)$rawData['attributeFilters'][$attributeKey]['type'] === AttributeFilter::TYPE_RANGE) {
                $values = array_map(function ($value) {
                    return (float)str_replace('-', '.', $value);
                }, array_keys($rawData['formatted'][$attributeKey]));

                $data[$attribute['alias']]['values']['min'] = !empty($values) ? min($values) : null;
                $data[$attribute['alias']]['values']['max'] = !empty($values) ? max($values) : null;
                $data[$attribute['alias']]['checked']['min'] = $rawData['params'][$attribute['alias']]['min'] ?? null;
                $data[$attribute['alias']]['checked']['max'] = $rawData['params'][$attribute['alias']]['max'] ?? null;
            } else {
                foreach ($rawData['formatted'][$attributeKey] as $valueKey => $value) {
                    $valueAlias = array_key_exists($valueKey, $rawData['attributeValues'])
                        ? $rawData['attributeValues'][$valueKey]['alias']
                        : $valueKey;

                    $data[$attribute['alias']]['values'][$valueAlias]['title'] = array_key_exists($valueKey, $rawData['attributeValues'])
                        ? UtilityHelper::upperFirstLetter($rawData['attributeValues'][$valueKey]['translations']['title'])
                        : $valueKey;

                    $data[$attribute['alias']]['values'][$valueAlias]['count'] = $value['count'];
                }
                if (array_key_exists($attribute['alias'], $rawData['params'])) {
                    $data[$attribute['alias']]['checked'] = $rawData['params'][$attribute['alias']];
                } else {
                    $data[$attribute['alias']]['checked'] = false;
                }
            }
        }

        return $data;
    }

    /**
     * @param $categories
     * @return array
     */
    private static function extractCategories($categories)
    {
        $data = [];
        foreach ($categories as $category) {
            $data[$category['slug']] = [
                'title' => $category['title'],
                'circle' => $category['customData']['circle'] ?? 'red',
                'children' => !empty($category['children']) ? self::extractCategories($category['children']) : []
            ];
        }

        return $data;
    }

    /**
     * @param $categories
     * @param $checked
     * @return array|null
     */
    private static function extractCheckedData($categories, $checked)
    {
        foreach ($categories as $category) {
            if ($category['slug'] === $checked) {
                return [
                    'checkedTitle' => $category['title'],
                    'checkedCircle' => $category['customData']['circle'] ?? 'red',
                ];
            }

            $checkedData = self::extractCheckedData($category['children'], $checked);
            if (!empty($checkedData)) {
                return $checkedData;
            }
        }

        return null;
    }
}