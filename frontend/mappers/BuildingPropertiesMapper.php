<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;
use common\models\AttributeFilter;
use common\models\Property;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class BuildingPropertiesMapper
 * @package frontend\mappers
 */
class BuildingPropertiesMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        if(!array_key_exists('counts', $rawData)) {
            throw new InvalidConfigException('Missing counts');
        }

        $result = [];

        $activeStatuses = [
            Property::STATUS_ACTIVE,
            Property::STATUS_REQUIRES_MODIFICATION,
            Property::STATUS_REQUIRES_MODERATION
        ];

        foreach($rawData['counts'] as $item) {
            $isActive = in_array($item['status'], $activeStatuses);

            if($isActive === true) {
                $type = $item['type'] == Property::TYPE_SALE ? 'sale' : 'rent';
            } else {
                $type = 'off';
            }

            $currentCount = $result[$type]['items'][$item['value_alias']]['count'] ?? 0;
            $minPrice = $result[$type]['items'][$item['value_alias']]['minPrice'] ?? 1000000000;
            $maxPrice = $result[$type]['items'][$item['value_alias']]['maxPrice'] ?? 0;
            $globalMaxPrice = $result[$type]['maxPrice'] ?? 0;
            $globalMinPrice = $result[$type]['minPrice'] ?? 100000000000;
            $globalCount = $result[$type]['count'] ?? 0;

            $result[$type]['items'][$item['value_alias']] = [
                'minPrice' => min($item['minPrice'], $minPrice),
                'maxPrice' => max($item['maxPrice'], $maxPrice),
                'count' => $currentCount + $item['count']
            ];
            $result[$type]['maxPrice'] = max($item['maxPrice'], $globalMaxPrice);
            $result[$type]['minPrice'] = min($item['minPrice'], $globalMinPrice);
            $result[$type]['count'] = $item['count'] + $globalCount;
        }

        return $result;
    }
}