<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers;

use common\interfaces\DataMapperInterface;
use yii\base\InvalidConfigException;

/**
 * Class CompanyMemberMapper
 * @package frontend\mappers
 */
class CompanyMemberMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        if (!array_key_exists('companies', $rawData)) {
            throw new InvalidConfigException('Missing companies configuration');
        }

        if (!array_key_exists('members', $rawData)) {
            throw new InvalidConfigException('Missing members configuration');
        }

        $data = [];

        foreach ($rawData['members'] as $member) {
            $currentCompany = $rawData['companies'][$member['company_id']];
            $data[] = [
                'id' => (int)$member['company_id'],
                'userId' => (int)$currentCompany['user_id'],
                'role' => (int)$member['role'],
                'memberStatus' => (int)$member['status'],
                'companyStatus' => (int)$currentCompany['status'],
                'companyTitle' => $currentCompany['title'],
                'companyLogo' => $currentCompany['logo'],
            ];
        }

        return $data;
    }
}