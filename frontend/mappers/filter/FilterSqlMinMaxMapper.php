<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers\filter;

use common\interfaces\DataMapperInterface;
use yii\base\InvalidConfigException;

/**
 * Class FilterMinMaxMapper
 * @package frontend\mappers
 */
class FilterSqlMinMaxMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        $result = [];

        foreach ($rawData as $item) {
            $result[] = [
                'attribute_id' => $item['attribute_id'],
                'value_alias' => $item['min'],
                'count' => 1,
            ];
            $result[] = [
                'attribute_id' => $item['attribute_id'],
                'value_alias' => $item['max'],
                'count' => 1,
            ];
        }

        return $result;
    }
}