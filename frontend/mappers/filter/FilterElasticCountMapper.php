<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers\filter;

use common\interfaces\DataMapperInterface;

/**
 * Class FilterElasticCountMapper
 * @package frontend\mappers\filter
 */
class FilterElasticCountMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param string $aggregationName
     * @return array
     */
    public static function getMappedData($rawData, $aggregationName = 'property_attribute')
    {
        $result = [];

        if (array_key_exists('aggregations', $rawData)) {
            foreach ($rawData['aggregations'] as $attributeId => $aggregation) {
                if (array_key_exists($aggregationName, $aggregation)) {
                    foreach ($aggregation[$aggregationName]['values']['buckets'] as $item) {
                        $result[] = [
                            'attribute_id' => $attributeId,
                            'count' => $item['doc_count'],
                            'value_alias' => $item['key']
                        ];
                    }
                }
            }
        }

        return $result;
    }
}