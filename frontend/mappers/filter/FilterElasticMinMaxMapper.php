<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers\filter;

use common\interfaces\DataMapperInterface;

/**
 * Class FilterElasticMinMaxMapper
 * @package frontend\mappers\filter
 */
class FilterElasticMinMaxMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param string $aggregationName
     * @return array
     */
    public static function getMappedData($rawData, $aggregationName = 'property_attribute')
    {
        $result = [];

        if (array_key_exists('aggregations', $rawData)) {
            foreach ($rawData['aggregations'] as $attributeId => $aggregation) {
                if (array_key_exists($aggregationName, $aggregation)) {
                    if ($aggregation[$aggregationName]['minValue']['value'] !== null) {
                        $result[] = [
                            'attribute_id' => $attributeId,
                            'count' => 1,
                            'value_alias' => $aggregation[$aggregationName]['minValue']['value']
                        ];
                    }

                    if ($aggregation[$aggregationName]['maxValue']['value'] !== null) {
                        $result[] = [
                            'attribute_id' => $attributeId,
                            'count' => 1,
                            'value_alias' => $aggregation[$aggregationName]['maxValue']['value']
                        ];
                    }
                }
            }
        }

        return $result;
    }
}