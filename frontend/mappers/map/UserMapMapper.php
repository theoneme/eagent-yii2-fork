<?php

namespace frontend\mappers\map;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class UserMapMapper
 * @package common\mappers
 */
class UserMapMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return ArrayHelper::map($rawData, 'id', function ($value) {
            return base64_encode(json_encode([
                'lat' => $value['lat'],
                'lng' => $value['lng'],
                'id' => $value['id'],
                'type' => 'user'
            ]));
        });
    }
}