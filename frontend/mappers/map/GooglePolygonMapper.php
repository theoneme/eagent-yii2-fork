<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers\map;

use common\interfaces\DataMapperInterface;

/**
 * Class GooglePolygonMapper
 * @package frontend\mappers\map
 */
class GooglePolygonMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @param string $lngParamName
     * @return array
     */
    public static function getMappedData($rawData, $lngParamName = 'lng')
    {
        return array_map(function ($value) use ($lngParamName) {
            return [
                'lat' => $value[0],
                $lngParamName => $value[1]
            ];
        }, $rawData);
    }
}