<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers\map;

use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class RequestMapMapper
 * @package frontend\mappers\map
 */
class RequestMapMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return ArrayHelper::map($rawData, 'id', function ($value) {
            return base64_encode(json_encode([
                'lat' => $value['lat'],
                'lng' => $value['lng'],
                'id' => $value['id'],
                'type' => 'request'
            ]));
        });
    }
}