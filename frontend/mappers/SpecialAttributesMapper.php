<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.05.2019
 * Time: 16:21
 */

namespace frontend\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;
use yii\helpers\ArrayHelper;

/**
 * Class SpecialAttributesMapper
 * @package frontend\mappers
 */
class SpecialAttributesMapper implements DataMapperInterface
{
    public const TYPE_MULTISTRING = 0;
    public const TYPE_STRING = 10;

    /**
     * @param $rawData
     * @param string $attribute
     * @param int $type
     * @return mixed
     */
    public static function getMappedData($rawData, $attribute = 'building_amenitie', $type = self::TYPE_MULTISTRING)
    {
        if (array_key_exists($attribute, $rawData['attributes'])) {
            $special = ArrayHelper::remove($rawData['attributes'], $attribute);

            if ($type === self::TYPE_MULTISTRING) {
                $value = array_map(function ($value) {
                    return UtilityHelper::upperFirstLetter(trim($value));
                }, explode(',', $special['value']));
            } else {
                $value = UtilityHelper::upperFirstLetter(trim($special['value']));
            }

            $rawData['specialAttributes'][$attribute] = $value;
        }

        return $rawData;
    }
}