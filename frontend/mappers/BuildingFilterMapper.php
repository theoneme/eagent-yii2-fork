<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers;

use common\helpers\UtilityHelper;
use common\interfaces\DataMapperInterface;
use common\models\AttributeFilter;
use common\models\Building;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class BuildingFilterMapper
 * @package frontend\mappers
 */
class BuildingFilterMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    public static function getMappedData($rawData)
    {
        $data = [];

        $data['operation']['title'] = Yii::t('catalog', 'Operation');
        $data['operation']['checked'] = $rawData['params']['operation'] ?? 'all';
        $data['operation']['values'] = [
            'all' => [
                'title' => Yii::t('catalog', 'All Buildings'),
                'count' => null,
                'circle' => 'yellow'
            ],
            Building::TYPE_DEFAULT_TEXT => [
                'title' => Yii::t('catalog', 'Secondary Buildings'),
                'count' => null,
                'circle' => 'red'
            ],
            Building::TYPE_NEW_CONSTRUCTION_TEXT => [
                'title' => Yii::t('catalog', 'New Constructions'),
                'count' => null,
                'circle' => 'magenta'
            ]
        ];

        $attributes = array_intersect_key($rawData['attributes'], $rawData['formatted']);
        foreach ($attributes as $attributeKey => $attribute) {
            $data[$attribute['alias']]['title'] = $attribute['translations']['title'];
            $data[$attribute['alias']]['type'] = array_key_exists($attributeKey, $rawData['attributeFilters'])
                ? AttributeFilter::$typeToView[$rawData['attributeFilters'][$attributeKey]['type']]
                : AttributeFilter::TYPE_DROPDOWN;

            if ((int)$rawData['attributeFilters'][$attributeKey]['type'] === AttributeFilter::TYPE_RANGE) {
                $values = array_map(function ($value) {
                    return (float)str_replace('-', '.', $value);
                }, array_keys($rawData['formatted'][$attributeKey]));

                $data[$attribute['alias']]['values']['min'] = !empty($values) ? min($values) : null;
                $data[$attribute['alias']]['values']['max'] = !empty($values) ? max($values) : null;
                $data[$attribute['alias']]['checked']['min'] = $rawData['params'][$attribute['alias']]['min'] ?? null;
                $data[$attribute['alias']]['checked']['max'] = $rawData['params'][$attribute['alias']]['max'] ?? null;
            } else {
                foreach ($rawData['formatted'][$attributeKey] as $valueKey => $value) {
                    $valueAlias = array_key_exists($valueKey, $rawData['attributeValues'])
                        ? $rawData['attributeValues'][$valueKey]['alias']
                        : $valueKey;

                    $data[$attribute['alias']]['values'][$valueAlias]['title'] = array_key_exists($valueKey, $rawData['attributeValues'])
                        ? UtilityHelper::upperFirstLetter($rawData['attributeValues'][$valueKey]['translations']['title'])
                        : $valueKey;

                    $data[$attribute['alias']]['values'][$valueAlias]['count'] = $value['count'];
                }
                if (array_key_exists($attribute['alias'], $rawData['params'])) {
                    $data[$attribute['alias']]['checked'] = $rawData['params'][$attribute['alias']];
                } else {
                    $data[$attribute['alias']]['checked'] = false;
                }
            }
        }

        return $data;
    }
}