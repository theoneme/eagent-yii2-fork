<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:44
 */

namespace frontend\mappers;

use common\interfaces\DataMapperInterface;
use Yii;
use yii\base\InvalidConfigException;

/**
 * Class LocationMapper
 * @package frontend\mappers
 */
class LocationMapper implements DataMapperInterface
{
    public const MODE_CITY = 0;
    public const MODE_REGION = 10;
    public const MODE_COUNTRY = 20;

    /**
     * @param $rawData
     * @param int $mode
     * @return array
     */
    public static function getMappedData($rawData, $mode = self::MODE_CITY)
    {
        switch ($mode) {
            case self::MODE_REGION:
                $result = self::mapRegions($rawData);

                break;
            case self::MODE_COUNTRY:
                $result = self::mapCountries($rawData);

                break;
            case self::MODE_CITY:
            default:
                $result = self::mapCities($rawData);

                break;
        }

        return $result;
    }

    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    protected static function mapCities($rawData)
    {
        if (!array_key_exists('cities', $rawData)) {
            throw new InvalidConfigException('Missing cities');
        }

        if (!array_key_exists('countries', $rawData)) {
            throw new InvalidConfigException('Missing countries');
        }

        if (!array_key_exists('regions', $rawData)) {
            throw new InvalidConfigException('Missing regions');
        }

        $cityLabel = Yii::t('labels', 'City');
        $result = [];

        foreach ($rawData['cities'] as $city) {
            $region = $rawData['regions'][$city['region_id']]['title'];
            $country = $rawData['countries'][$city['country_id']]['title'];


            $item = [
                $city['title'],
                $region,
                "{$country} ({$cityLabel})"
            ];

            $result[] = [
                'title' => implode(', ', $item),
                'short' => $city['title'],
                'slug' => $city['slug'],
                'entity' => 'city',
                'entityId' => $city['id']
            ];
        }

        return $result;
    }

    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    protected static function mapRegions($rawData)
    {
        if (!array_key_exists('countries', $rawData)) {
            throw new InvalidConfigException('Missing countries');
        }

        if (!array_key_exists('regions', $rawData)) {
            throw new InvalidConfigException('Missing regions');
        }

        $regionLabel = Yii::t('labels', 'Region');
        $result = [];

        foreach ($rawData['regions'] as $region) {
            $country = $rawData['countries'][$region['country_id']]['title'];

            $item = [
                $region['title'],
                "{$country} ({$regionLabel})"
            ];

            $result[] = [
                'title' => implode(', ', $item),
                'short' => $region['title'],
                'slug' => "region-{$region['slug']}",
                'entity' => 'region',
                'entityId' => $region['id']
            ];
        }

        return $result;
    }

    /**
     * @param $rawData
     * @return array
     * @throws InvalidConfigException
     */
    protected static function mapCountries($rawData)
    {
        if (!array_key_exists('countries', $rawData)) {
            throw new InvalidConfigException('Missing countries');
        }

        $regionLabel = Yii::t('labels', 'Country');
        $result = [];

        foreach ($rawData['countries'] as $country) {
            $result[] = [
                'title' => "{$country['title']} ({$regionLabel})",
                'short' => $country['title'],
                'slug' => "country-{$country['slug']}",
                'entity' => 'country',
                'entityId' => $country['id']
            ];
        }

        return $result;
    }
}