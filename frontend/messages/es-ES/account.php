<?php

return [
    'Action' => 'La acción',
    'Add Company Member' => 'Añadir parte de la empresa',
    'All Properties' => 'Todos los objetos',
    'Are you sure?' => '¿Está usted seguro?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Atrás a las empresas',
    'Company Members' => 'Los participantes de la empresa',
    'Create Company' => 'Crear una empresa',
    'Logo' => 'El logotipo de',
    'My Companies' => 'Mi empresa',
    'My Properties' => 'Mis objetos',
    'My Requests' => 'Mi solicitud',
    'Role' => 'El papel de la',
    'Status' => 'El estado de',
    'Title' => 'Nombre de la',
    'Username' => 'El nombre de usuario',
];
