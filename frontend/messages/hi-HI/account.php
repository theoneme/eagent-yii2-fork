<?php

return [
    'Action' => 'कार्रवाई',
    'Add Company Member' => 'जोड़ने के लिए, एक भागीदार कंपनी के लिए',
    'All Properties' => 'सभी वस्तुओं',
    'Are you sure?' => 'आप यकीन कर रहे हैं?',
    'Avatar' => 'अवतार',
    'Back to companies' => 'वापस करने के लिए कंपनियों',
    'Company Members' => 'कंपनी के सदस्यों में',
    'Create Company' => 'एक कंपनी बनाने के लिए',
    'Logo' => 'लोगो',
    'My Companies' => 'मेरी कंपनी',
    'My Properties' => 'मेरी वस्तुओं',
    'My Requests' => 'मेरे अनुप्रयोगों',
    'Role' => 'भूमिका',
    'Status' => 'स्थिति',
    'Title' => 'नाम',
    'Username' => 'उपयोगकर्ता नाम',
];
