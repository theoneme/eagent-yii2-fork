<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.08.2018
 * Time: 16:56
 */

return [
    'color' => null,
    'interactive' => true,
    'sourcePath' => ['frontend', 'common'],
    'messagePath' => 'frontend/messages',
    'languages' => array_diff(array_keys(Yii::$app->params['supportedLocales']), ['en-GB']),
    'translator' => 'Yii::t',
    'sort' => true,
    'overwrite' => true,
    'removeUnused' => true,
    'markUnused' => true,
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/BaseYii.php',
    ],
    'only' => [
        '*.php',
    ],
    'format' => 'php',
    'db' => 'db',
    'sourceMessageTable' => '{{%source_message}}',
    'messageTable' => '{{%message}}',
    'catalog' => 'messages',
    'ignoreCategories' => ['notification', 'model', 'yii', 'labels', 'app', 'crm', 'instance', 'iStatic', 'landing'],
];
