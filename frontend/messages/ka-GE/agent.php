<?php 
 return [
    'About Us' => 'ჩვენს შესახებ',
    'All Activity' => 'ყველა საქმიანობა',
    'Bed / Bath' => 'საძინებელი / სველი წერტილები',
    'Call' => 'დარეკეთ',
    'Cancel' => 'გაუქმება',
    'Menu' => 'მენიუ',
    'More' => 'მეტი',
    'Our Listings & Sales' => 'ჩვენი ობიექტების და გაყიდვების',
    'Our Past Sales' => 'ჩვენი წარსული გაყიდვების',
    'Our active listings' => 'ჩვენი აქტიური განცხადებები',
    'Professional Information' => 'პროფესიული ინფორმაცია',
    'Property Address' => 'მისამართი ქონების',
    'Ratings & Reviews' => 'რეიტინგები და მიმოხილვა',
    'Represented' => 'წარმოდგენილია',
    'Save' => 'შენახვა',
    'Search property in cities' => 'ძებნა, უძრავი ქონების ქალაქებსა',
    'Send' => 'გაგზავნას',
    'Send Request' => 'თხოვნით',
    'Share' => 'Share',
    'Show contact' => 'კონტაქტები არიან',
    'Sold Date' => 'იყიდება თარიღი',
    'Specialties: {specialties}' => 'სახეობა: {specialties}',
    'Write a review' => 'დატოვეთ კომენტარი',
    '{count, plural, one{# Listing} other{# Listings}}' => '{count, plural, one{# რეკლამა} few{# დეკლარაციების} other{# რეკლამა}}',
    '{count} Recent Sales' => '',
    '{count} Reviews' => '{count} მიმოხილვა',
    '{count} Sales Last year' => '{count} გაყიდვები გასული წლის',
    '{count} total reviews' => '{count} უბრალოდ მიმოხილვა',
    '{entity} for sale in {city}' => '{entity} იყიდება {city}',
];