<?php

return [
    'Action' => 'მოქმედება',
    'Add Company Member' => 'დამატება მონაწილე კომპანია',
    'All Properties' => 'ყველა ობიექტი',
    'Are you sure?' => 'დარწმუნებული ხართ?',
    'Avatar' => 'ავატარი',
    'Back to companies' => 'უკან კომპანიები',
    'Company Members' => 'წევრები კომპანიის',
    'Create Company' => 'შექმნათ კომპანიის',
    'Logo' => 'ლოგო',
    'My Companies' => 'ჩემი კომპანია',
    'My Properties' => 'ჩემი ობიექტების',
    'My Requests' => 'ჩემი განცხადებები',
    'Role' => 'როლი',
    'Status' => 'სტატუსი',
    'Title' => 'დასახელება',
    'Username' => 'მომხმარებლის სახელი',
];
