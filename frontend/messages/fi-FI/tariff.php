<?php 
 return [
    'Additional priority for your listings in catalog and similar propositions block' => 'Esineitä on ensimmäisiä paikkoja luetteloon ja estää vastaavia ehdotuksia',
    'Additional priority in agents catalog' => 'Majoitus ensimmäisistä paikoista luettelossa aineet',
    'Additional priority on receiving requests' => 'Ilmoittautumisajankohta ensimmäisessä vaiheessa',
    'Advanced' => 'Advanced',
    'Bank banner in the partners section on the main page' => 'Banner Bank kumppanit estää pääsivulla',
    'Banner advertising on the site' => 'Banneri-mainonta verkkosivuilla',
    'Banner location' => 'Sijainti banneri',
    'Choose tariff' => 'Valita tariffi',
    'Description' => 'Kuvaus',
    'Free CRM and support. Search for other propositions if needed' => 'Ilmainen CRM ja tukea. Valikoima muita vaihtoehtoja, jos tarpeen',
    'Free customer management CRM' => 'Ilmainen CRM ylläpitää asiakkaan',
    'Grants access for receiving requests from clients which are interested in mortgage. <br> Also provides extra traffic for your site' => 'Avulla voit vastaanottaa pyyntöjä asiakkaita, joita kiinnostaa kiinnitykset. <br> Myös tarjoaa lisää liikennettä sivustoosi',
    'Helps you to attract more visitors for your site' => 'Auttaa houkutella enemmän kävijöitä verkkosivuilla',
    'Minimal' => 'Vähintään',
    'No' => 'Ei',
    'Object Card' => 'Kohde on kortti',
    'On the main page below in the Partners section' => 'Pääsivun alareunassa lohkon Kumppanit',
    'Requests for property purchase' => 'Hakemukset kiinteistöjen osto',
    'Requests for property sales' => 'Hakemukset kiinteistöjen myynnistä',
    'Standard' => 'Standardi',
    'Tariffs for Banks for receiving requests on mortgage' => 'Hinnat pankkien tarjoukset asuntolainasta',
    'Tariffs for agents for receiving requests on property purchases' => 'Hintoja aineita, vastaanottaa hakemuksia kiinteistöjen osto',
    'Tariffs for agents for receiving requests on property sales' => 'Hintoja aineita, vastaanottaa hakemuksia kiinteistöjen myynnistä',
    'Yes' => 'Kyllä',
    'You will receive requests (leads) from clients who are interested in buying property' => 'Saat pyyntöjä (johtaa) asiakkailta kiinnostunut ostamaan kiinteistöjä',
    'You will receive requests (leads) from clients who are interested in selling property' => 'Saat pyyntöjä (johtaa) mistä asiakkaat ovat kiinnostuneita real estate',
    '{count} hits per month' => '{count} osoittaa kuukaudessa',
    '{count} per month' => '{count} / kk',
    '{object} objects<br/>({count} hits)' => '{object} object<br />({count})',
    '{price} per month' => '{price} / kk',
];