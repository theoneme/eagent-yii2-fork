<?php 
 return [
    '(work with materials cost)' => '(työ materiaalikustannukset)',
    '-- Mortgage Insurance description --' => '- Kuvaus vakuutus --kiinnitys',
    'Additional answers after contact agent form' => 'Muita vastauksia kun yhteyttä viestinnän muoto, jossa agentti',
    'Amenities' => 'Edut',
    'Anastasia' => 'Anastasia',
    'Back' => 'Sitten',
    'Building on map' => 'Rakennus kartalla',
    'By sending request you are automatically agree with {tos}' => 'Lähettämällä hakemuksen hyväksyt automaattisesti {tos}',
    'Call' => 'Soita',
    'Call up' => 'Soita',
    'Close' => 'Lähellä',
    'Contact Seller' => 'Yhteyttä omistajaan',
    'Contact owner (agent)' => 'Yhteystiedot, omistajan (agentti)',
    'Cooling' => 'Jäähdytys',
    'Date' => 'Päivämäärä',
    'Days on {site}' => 'Päivän {site}',
    'Developer: {developer}' => 'Kehittäjä: {developer}',
    'Divorced' => 'Eronnut',
    'Don`t forget to tell that you found this announcement on {site}' => 'Älä unohda mainita, että olet löytänyt tämän mainoksen {site}',
    'Down Payment' => 'Aloitusmaksu',
    'Email' => 'Sähköposti',
    'Event' => 'Tapahtuma',
    'Facts' => 'Tiedot',
    'First Name' => 'Nimi',
    'Form of Ownership' => 'Kiinteistön tyyppi',
    'Get Special Offer' => 'Autenttista. tarjous',
    'Heating' => 'Lämmitys',
    'Hi! I offer price {price} {currency}' => 'Hei! Ehdotan hinta {price} {currency}',
    'Hi! I want to request a mortgage for property {property}. Credit: {credit} {currency}, Loan Time: {loanTerm, plural, one{# year} other{# years}}, Age: {age, plural, one{# year} other{# years}}, Family Status: {familyStatus}' => 'Hei! Haluat pyytää kiinnitys omaisuutta {property}. Määrä: {credit} {currency} Aikavälillä: {loanTerm, plural, one{# vuosi} few{# vuosi} other{# vuotta}}, Ikä: {age, plural, one{# vuosi} few{# vuosi} other{# vuotta}}, Niin # {familyStatus}',
    'Hi! I want to request a show case on {date} with additions: {time}' => 'Hei! Haluan tilata näytön kiinteistön päivämäärä {date}: {time}',
    'Hide' => 'Piilota',
    'Houses for sale in {index}' => 'Koteja myytävänä {index}',
    'I am interested in {property}' => 'Olen kiinnostunut {property}',
    'If you want to buy with property using mortgage, begin with sending request with button below' => 'Jos haluat ostaa omaisuutta kiinnitys, aloita lähettämällä sovelluksen kautta alla olevaa painiketta',
    'Include Insurance' => 'Myös vakuutukset',
    'Interest Rate' => 'Korko',
    'Loan Term' => 'Maturiteetti',
    'Located in the building{title} at the address: {address}' => 'Sijaitsee rakennuksessa{title}: {address}',
    'Main Parameters' => 'Tärkeimmät parametrit',
    'Married' => 'Naimisissa',
    'Member from {date}' => 'Osallistuja {date}',
    'Monthly Payment' => 'Kuukausimaksu',
    'More' => 'Lisää',
    'Mortgage' => 'Kiinnitys',
    'Mortgage Insurance' => 'Vakuutus asuntolaina',
    'Moscow' => 'Moskova',
    'Nearby Cities' => 'Läheiset kaupungit',
    'Nearby Zip Codes' => 'Seuraava postinumerot',
    'Next' => 'Edelleen',
    'No' => 'Ei',
    'No attributes specified' => 'Parametrit on määritetty',
    'Not Set' => 'Ei määritetty',
    'Not specified' => 'Ei määritetty',
    'Offer your price' => 'Tarjous hinta',
    'Other topics in {city}' => 'Muita ehdotuksia {city}',
    'Parking' => 'Pysäköinti',
    'Phone' => 'Puhelin',
    'Post Announcement' => 'Sijoittaa mainoksen',
    'Price' => 'Hinta',
    'Price / m²' => 'Hinta / m2',
    'Price History' => 'Historia hinnat',
    'Price from {min} to {max} per m²' => 'Hinta alkaen {min} - {max} m2',
    'Properties for sale in {index}' => 'Huoneistoja myytävänä {index}',
    'Properties with bigger area for same price' => 'Esineitä, joilla on suurempi pinta-ala samaan hintaan',
    'Property Area' => 'Square',
    'Property Parameters' => 'Esine parametrit',
    'Property Price' => 'Hinta esine',
    'Property listed' => 'Laitos sijaitsee',
    'Property listed by' => 'Laitos sijaitsee',
    'Property on map' => 'Kohde kartalla',
    'Publish date: <strong>{date}</strong>' => 'Julkaisun aikaan: <strong>{date}</strong>',
    'Recent sales' => 'Edellinen myynti',
    'Request a showcase' => 'Ota',
    'Save' => 'Tallenna',
    'Saves' => 'Tallenna',
    'Send' => 'Lähetä',
    'Send Message' => 'Lähetä viesti',
    'Send Request' => 'Lähetä pyyntö',
    'Share' => 'Jaa',
    'Show Contacts' => 'Yhteystiedot näyttö',
    'Single' => 'Ole naimisissa',
    'Single Person' => 'Ole naimisissa',
    'St. Peterburg' => 'St. Peterburs',
    'Stats for this property' => 'Tilastot tämän objektin',
    'Terms of Service' => 'Käyttöehdot',
    'Thanks for your answers!' => 'Kiitos vastauksistanne!',
    'The estimated cost of repairing your condo will be' => 'Kustannusarvio korjaus teidän asunto on',
    'This is mortgage monthly payment with provided parameters' => 'Kuukausimaksu määritettyjä parametreja',
    'Turnkey repair services' => 'Korjauspalvelut avaimet käteen-periaatteella',
    'VELES' => 'VELES',
    'Video tour' => 'Video tour',
    'View other properties in this building' => 'Nähdä toisen omaisuutta, tässä rakennuksessa',
    'What i personally like in this property' => 'Mitä pidän erityisesti siitä, että tämä majoituspaikka',
    'Year Built' => 'Vuonna rakennettu',
    'Yes' => 'Kyllä',
    'Your message' => 'Viestisi',
    'in the last {days, plural, one{# day} other{# days}}' => '{days, plural, one{# viimeinen päivä} few{# viimeinen päivä} other{# viimeinen päivä}}',
    'month' => 'kuukausi',
    'per month' => 'joka kuukausi',
    'this property to their favorites in the last {days, plural, one{# day} other{# days}}' => 'tämä esine kirjanmerkki {days, plural, one{# viimeinen päivä} few{# viimeinen päivä} other{# viimeinen päivä}}',
    '{area} m²' => '',
    '{bathrooms, plural, one{# bathroom} other{# bathrooms}}' => '{bathrooms, plural, one{# kylpy} few{# kylpy} other{# kylpylä}}',
    '{baths, plural, one{# bath} other{# bath}}' => '{baths, plural, one{# kylpyhuone} few{# kylpyhuonetta} other{# kylpyhuoneet}}',
    '{bedrooms, plural, one{# bedroom} other{# bedrooms}}' => '{bedrooms, plural, one{# makuuhuone} few{# makuuhuone} other{# makuuhuone}}',
    '{beds, plural, one{# bed} other{# beds}}' => '{beds, plural, one{# makuuhuone} few{# makuuhuone} other{# makuuhuone}}',
    '{parking, plural, one{# space} other{# spaces}}' => '{parking, plural, one{# paikka} few{# paikka} other{# istuimet}}',
    '{rooms, plural, =0{} one{# room} other{# rooms}} Properties with similar area but cheaper price' => '{rooms, plural, =0{} one{#huone} other{#huone}} esineitä samalla alueella halvemmalla',
    '{rooms, plural, =0{} one{# room} other{# rooms}} Properties with similar area but higher price' => '{rooms, plural, =0{} one{#huone} other{#huone}} esineitä samalla alueella korkeammat kustannukset',
    '{rooms, plural, one{# room} other{# rooms}}' => '{rooms, plural, one{# huone} few{# huone} other{# huone}}',
    '{saves, plural, one{user} other{users}} saved' => '{saves, plural, one{käyttäjä} few{käyttäjä} other{käyttäjä}} on pidetty',
    '{views, plural, one{view} other{views}}' => '{views, plural, one{näytä} few{näytä} other{näkymät}}',
    '{years, plural, one{# year} other{# years}}' => '{years, plural, one{# vuosi} few{# vuosi} other{# vuotta}}',
];