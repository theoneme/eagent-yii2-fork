<?php

return [
    'Action' => 'Toiminta',
    'Add Company Member' => 'Jos haluat lisätä osallistujan yritys',
    'All Properties' => 'Kaikki esineet',
    'Are you sure?' => 'Oletko varma?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Takaisin yrityksille',
    'Company Members' => 'Jäsenet yhtiön',
    'Create Company' => 'Luoda yritys',
    'Logo' => 'Logo',
    'My Companies' => 'Oma yritys',
    'My Properties' => 'Minun esineitä',
    'My Requests' => 'Minun sovelluksia',
    'Role' => 'Rooli',
    'Status' => 'Tila',
    'Title' => 'Nimi',
    'Username' => 'Käyttäjätunnus',
];
