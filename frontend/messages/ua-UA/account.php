<?php

return [
    'Action' => 'Дія',
    'Add Company Member' => 'Додати учасника компанії',
    'All Properties' => 'Всі об\'єкти',
    'Are you sure?' => 'Ви впевнені?',
    'Avatar' => 'Аватар',
    'Back to companies' => 'Тому компаніям до',
    'Company Members' => 'Учасники компанії',
    'Create Company' => 'Створити компанію',
    'Logo' => 'Логотип',
    'My Companies' => 'Мої компанії',
    'My Properties' => 'Мої об\'єкти',
    'My Requests' => 'Мої заявки',
    'Role' => 'Роль',
    'Status' => 'Статус',
    'Title' => 'Назва',
    'Username' => 'Ім\'я користувача',
];
