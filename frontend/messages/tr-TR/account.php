<?php

return [
    'Action' => 'Eylem',
    'Add Company Member' => 'Üye ekle, şirket',
    'All Properties' => 'Tüm nesneleri',
    'Are you sure?' => 'Emin misiniz?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Önce şirketler',
    'Company Members' => 'Katılımcılar şirketi',
    'Create Company' => 'Şirket kurmak',
    'Logo' => 'Logo',
    'My Companies' => 'Benim şirket',
    'My Properties' => 'Benim nesneleri',
    'My Requests' => 'Benim başvuru',
    'Role' => 'Rol',
    'Status' => 'Durumu',
    'Title' => 'Adı',
    'Username' => 'Kullanıcı adı',
];
