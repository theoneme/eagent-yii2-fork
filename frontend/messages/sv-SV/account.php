<?php

return [
    'Action' => 'Åtgärd',
    'Add Company Member' => 'För att lägga till en deltagare till företaget',
    'All Properties' => 'Alla objekt',
    'Are you sure?' => 'Är du säker?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Tillbaka till företag',
    'Company Members' => 'Medlemmarna i företaget',
    'Create Company' => 'För att skapa ett företag',
    'Logo' => 'Logotyp',
    'My Companies' => 'Mitt företag',
    'My Properties' => 'Mitt objekt',
    'My Requests' => 'Mina ansökningar',
    'Role' => 'Roll',
    'Status' => 'Status',
    'Title' => 'Namn',
    'Username' => 'Användarnamn',
];
