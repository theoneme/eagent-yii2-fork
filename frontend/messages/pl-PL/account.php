<?php

return [
    'Action' => 'Akcja',
    'Add Company Member' => 'Dodaj uczestnika firmy',
    'All Properties' => 'Wszystkie obiekty',
    'Are you sure?' => 'Jesteś pewien?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Powrót do firmy',
    'Company Members' => 'Uczestnicy firmy',
    'Create Company' => 'Założyć firmę',
    'Logo' => 'Logo',
    'My Companies' => 'Moje firmy',
    'My Properties' => 'Moje obiekty',
    'My Requests' => 'Moje wnioski',
    'Role' => 'Rola',
    'Status' => 'Status',
    'Title' => 'Nazwa',
    'Username' => 'Nazwa użytkownika',
];
