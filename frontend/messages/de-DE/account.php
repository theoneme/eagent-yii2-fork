<?php

return [
    'Action' => 'Aktion',
    'Add Company Member' => 'Hinzufügen eines Teilnehmers des Unternehmens',
    'All Properties' => 'Alle Objekte',
    'Are you sure?' => 'Sind Sie sicher?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Zurück zu den Unternehmen',
    'Company Members' => 'Die Teilnehmer des Unternehmens',
    'Create Company' => 'Eine Firma',
    'Logo' => 'Logo',
    'My Companies' => 'Meine Firma',
    'My Properties' => 'Meine Objekte',
    'My Requests' => 'Meine Angebote',
    'Role' => 'Rolle',
    'Status' => 'Status',
    'Title' => 'Titel',
    'Username' => 'Benutzername',
];
