<?php 
 return [
    'Building Amenities' => 'مزايا المبنى',
    'Building Parameters' => 'والمعلمات من المبنى',
    'Call' => 'الاتصال',
    'Cancel' => 'إلغاء',
    'Condos in house{title}{address}' => 'شقة في منزل{title}{address}',
    'Construction Progress' => 'التقدم البناء',
    'Documentation' => 'الوثائق',
    'For Rent' => 'للايجار',
    'For Sale' => 'للبيع',
    'I am interested in property in building {building}' => 'أنا مهتم في العقارات في بناء {building}',
    'Location' => 'الموقع',
    'Menu' => 'القائمة',
    'More' => 'المزيد',
    'Send' => 'إرسال',
    'Send Request' => 'إرسال طلب',
    'Share' => 'حصة',
    'Similar Buildings' => 'مبان مماثلة',
    'Unavailable' => 'غير متوفر',
    '{quarter} quarter' => '{quarter} الربع',
    '{time, plural, one{# minute} other{# minutes}} {transport}' => '{time, plural, one{# مين} few{# دقيقة} other{# دقائق}} {transport}',
    '{type} {name}' => '',
    '{year} year' => '{year} سنة',
];