<?php 
 return [
    '<span >{rooms, plural, one{#+ rooms} other{#+ rooms}}</span>' => '<span>{rooms, plural, one{#+ غرفة} few{#+ غرفة} other{ # + }}</span>',
    'Action' => 'العمل',
    'Add comment for this property' => 'إضافة تعليق على هذا الكائن',
    'Address' => 'عنوان',
    'Address or ZIP code' => 'العنوان أو الرمز البريدي',
    'Advertisement' => 'الإعلان',
    'Agent name' => 'اسم الوكيل',
    'All Buildings' => 'جميع المباني',
    'Any Price' => 'أي سعر',
    'Area' => 'المنطقة',
    'Area {area} m²' => 'المنطقة هي {area} m2',
    'Average price per square meter {price}' => 'متوسط سعر المتر المربع {price}',
    'Bedrooms' => 'غرف نوم',
    'Buy' => 'شراء',
    'Cancel' => 'إلغاء',
    'Catalog Mode' => 'وضع كتالوج',
    'Category' => 'الفئة',
    'Cheap first' => 'أولا رخيصة',
    'Choose' => 'اختيار',
    'Clear' => 'نظيفة',
    'Close' => 'قريب',
    'Comment' => 'استعراض',
    'Comment: {comment}' => 'التعليق: {comment}',
    'Contract Price' => 'السعر قابل للتفاوض',
    'Create Report' => 'لإنشاء مجموعة',
    'Default Mode' => 'الوضع القياسي',
    'Description' => 'الوصف',
    'Description is missing' => 'لا يوجد وصف متاح',
    'Districts' => 'المناطق',
    'Districts and microdistricts' => 'المناطق والمقاطعات',
    'Enter city, region or country' => 'وأدخل مدينة أو منطقة أو بلد',
    'Expensive first' => 'أولا',
    'Favorite' => 'المفضلة',
    'Filter' => 'فلتر',
    'Floor' => 'الكلمة',
    'From {min}' => 'من {min}',
    'I`m interested in' => 'أنا مهتم في',
    'Kitchen Area' => 'منطقة المطبخ',
    'Kitchen {area} m²' => 'المطبخ هو {area} m2',
    'List' => 'قائمة',
    'List of similar properties:' => 'قائمة من الكائنات المماثلة:',
    'Living Area' => 'الذين يعيشون في المنطقة',
    'Location' => 'عنوان',
    'Make Report' => 'لإنشاء مجموعة',
    'Map' => 'خريطة',
    'Map Filter Mode' => 'تصفية على الخريطة',
    'Max' => 'ماكس',
    'Menu' => 'القائمة',
    'Microdistricts' => 'الأحياء',
    'Min' => 'منخفضة',
    'Minimum price per square meter {price}' => 'أدنى سعر المتر المربع {price}',
    'More' => 'المزيد',
    'More actions' => 'إجراءات أخرى',
    'Name' => 'اسم',
    'New Constructions' => 'المباني',
    'Newest first' => 'أولا الجديدة',
    'No districts were found for this region' => 'في هذه المنطقة من المناطق لم يتم العثور على',
    'No microdistricts were found for this region' => 'هذه المنطقة لم يتم العثور على أحياء',
    'Number of Balconies' => 'عدد من الشرفات',
    'Number of Bathrooms' => 'عدد الحمامات',
    'Number of Bedrooms' => 'عدد غرف النوم',
    'Number of Garages' => 'عدد من المرائب',
    'Number of Loggias' => 'عدد والمقطع',
    'Number of Rooms' => 'عدد الغرف',
    'Operation' => 'العملية',
    'Photos' => 'الصورة',
    'Possible price' => 'ممكن السعر',
    'Possible price: {price}' => 'ممكن سعر: {price}',
    'Price' => 'السعر',
    'Price per m²' => 'السعر لكل m2',
    'Property Area' => 'مربع',
    'Publish date' => 'تاريخ وضع',
    'Recommended price for fast sale {price}' => 'السعر الموصى به لبيع سريعة {price}',
    'Recommended price {price}' => 'السعر الموصى بها من {price}',
    'Rent' => 'للايجار',
    'Report created {date}' => 'مجموعة تم إنشاؤها من قبل {date}',
    'Request Showcase' => 'طلب عرض',
    'Request a showcase' => 'تأخذ',
    'Reset' => 'إعادة تعيين',
    'Rooms' => 'غرفة',
    'Save' => 'حفظ',
    'Search' => 'البحث',
    'Search by map' => 'البحث عن طريق الخريطة',
    'Secondary Buildings' => 'الثانوية المباني',
    'See more' => 'قراءة المزيد',
    'Select {attribute}' => 'حدد {attribute}',
    'Selected Properties' => 'اختيار الكائنات',
    'Send Report' => 'إرسال اختيار',
    'Send selected properties to agent' => 'إرسال الكائنات المحددة إلى الوكيل',
    'Share' => 'حصة',
    'Show' => 'تظهر',
    'Showcase' => 'مشاهدة',
    'Sort Type' => 'نوع من الفرز',
    'Sort by' => 'فرز حسب',
    'Specify parameters of your property, so we can make an analyze.' => 'تحديد المعلمات من شقتك ، حتى نتمكن من القيام التحليل.',
    'Start typing and select one of the options' => 'البدء في الكتابة واختيار واحد من الخيارات المتاحة',
    'Summary' => 'مجموع',
    'Title' => 'اسم',
    'To {max}' => 'إلى {max}',
    'Unknown agency' => 'غير معروف وكالة',
    'View Request' => 'لعرض التطبيق',
    'Views' => 'الآراء',
    'We have analyzed listings in internet that match your request. {count, plural, one{# property was analyzed} other{# properties were analyzed}}' => 'قمنا بتحليل الأشياء الموضوعة في الإنترنت تطابق بحثك. {count, plural, one{تم تحليل # وجوه} few{أن تم تحليل # وجوه} other{تحليل # من الأشياء}}',
    'We made analyze for your property located by address {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}' => 'قمنا بتحليل الممتلكات الخاصة بك الموجودة في {address} {rooms, plural, one{# غرفة} other{# غرفة}} منطقة {area, plural, one{# متر مربع} few{# للمتر المربع الواحد} other{# متر مربع}}',
    'Year Built' => 'العام',
    '{bedrooms, plural, one{# bedroom} other{# bedrooms}}' => '{bedrooms, plural, one{# نوم} few{# نوم} other{# نوم}}',
    '{count, plural, one{# object} other{# objects}}' => '{count, plural, one{# وجوه} few{# وجوه} other{# كائنات}}',
    '{count, plural, one{# property found} other{# properties found}} near {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}' => '{count, plural, one{# العثور على كائن} few{# العثور على كائن} other{# العثور على كائن}} حوالي {address} {rooms, plural, one{# غرفة} other{# غرفة}} مربع {area, plural, one{# متر مربع} few{# للمتر المربع الواحد} other{# متر مربع}}',
    '{count, plural, one{# result} other{# results}}' => '{count, plural, one{# وجوه} few{# وجوه} other{# كائنات}}',
    '{flats, plural, one{# flat} other{# flats}} for rent' => '{flats, plural, one{# مسطحة} few{# مسطحة} other{# شقق}} للإيجار',
    '{flats, plural, one{# flat} other{# flats}} for sale' => '{flats, plural, one{# مسطحة} few{# مسطحة} other{# شقق}} للبيع',
    '{quarter} quarter' => '{quarter} الربع',
    '{rooms, plural, one{# room} other{# rooms}}' => '{rooms, plural, one{# غرفة} few{# غرفة} other{# غرفة}}',
    '{rooms, plural, one{#+ room} other{#+ rooms}}' => '{rooms, plural, one{#+ غرفة} few{#+ غرفة} other{ # + }}',
];