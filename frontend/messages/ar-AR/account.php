<?php

return [
    'Action' => 'العمل',
    'Add Company Member' => 'إضافة إلى مشاركة الشركة',
    'All Properties' => 'جميع الكائنات',
    'Are you sure?' => 'هل أنت متأكد ؟ ',
    'Avatar' => 'الصورة الرمزية',
    'Back to companies' => 'العودة إلى الشركات',
    'Company Members' => 'أعضاء الشركة',
    'Create Company' => 'لإنشاء شركة',
    'Logo' => 'شعار',
    'My Companies' => 'بلدي الشركة',
    'My Properties' => 'بلدي الكائنات',
    'My Requests' => 'بلدي تطبيقات',
    'Role' => 'دور',
    'Status' => 'حالة',
    'Title' => 'اسم',
    'Username' => 'اسم المستخدم',
];
