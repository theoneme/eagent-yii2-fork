<?php

return [
    'Action' => 'L\'action',
    'Add Company Member' => 'Ajouter le membre de la société',
    'All Properties' => 'Tous les objets',
    'Are you sure?' => 'Êtes-vous sûr?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Retour aux entreprises',
    'Company Members' => 'Les participants de la société',
    'Create Company' => 'Créer une entreprise',
    'Logo' => 'Le logo',
    'My Companies' => 'Mon entreprise',
    'My Properties' => 'Mes objets',
    'My Requests' => 'Mes candidatures',
    'Role' => 'Le rôle de la',
    'Status' => 'Le statut de',
    'Title' => 'Le nom de',
    'Username' => 'Le nom de l\'utilisateur',
];
