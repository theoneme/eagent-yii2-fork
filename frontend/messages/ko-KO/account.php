<?php

return [
    'Action' => '작',
    'Add Company Member' => '를 추가하는 참가자가 회사',
    'All Properties' => '모든 객체',
    'Are you sure?' => '하시겠습니까?',
    'Avatar' => '아바타',
    'Back to companies' => '다시 회사',
    'Company Members' => '회원은 회사',
    'Create Company' => '회사를 만들',
    'Logo' => '로고',
    'My Companies' => '회사',
    'My Properties' => '내체',
    'My Requests' => '내 응용 프로그램',
    'Role' => '역할',
    'Status' => '상태',
    'Title' => '이름',
    'Username' => '사용자 이름',
];
