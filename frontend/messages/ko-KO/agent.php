<?php 
 return [
    'About Us' => 'About us',
    'All Activity' => '모든 활동',
    'Bed / Bath' => '실/욕실',
    'Call' => '전화',
    'Cancel' => '취소',
    'Menu' => '메뉴',
    'More' => 'More',
    'Our Listings & Sales' => '우리의시설 및 판매',
    'Our Past Sales' => '우리의 과거 판매',
    'Our active listings' => '우리의 활동 광고',
    'Professional Information' => '전문적인 정보',
    'Property Address' => '의 주소 속성',
    'Ratings & Reviews' => '평점과 리뷰',
    'Represented' => '로 표시',
    'Save' => '저장',
    'Search property in cities' => '검색에서 부동산 도시',
    'Send' => '보내기',
    'Send Request' => '요청을 보내기',
    'Share' => '공유',
    'Show contact' => '연락처를 표시',
    'Sold Date' => '판매 날짜',
    'Specialties: {specialties}' => '직업:{specialties}',
    'Write a review' => '리뷰 쓰기',
    '{count, plural, one{# Listing} other{# Listings}}' => '{count, plural, one{#광고} few{#선언} other{#광고}}',
    '{count} Recent Sales' => '',
    '{count} Reviews' => '{count}리뷰',
    '{count} Sales Last year' => '{count}의 판매를 지난 일년',
    '{count} total reviews' => '{count}리뷰',
    '{entity} for sale in {city}' => '{entity}에서 판매{city}',
];