<?php 
 return [
    '<p>Choose one of the options.</p><p><b>For Example:</b> Repair required </p>' => '<p>옵션 중 하나를 선택합니다.</p><p><b>예를 들어:</b> 개요 </p>',
    '<p>Choose whether you sell an property with furniture or not.</p><p><b>For Example:</b> No </p>' => '<p>선택하신 판매와 함께 아파트의 가구 또는 없습니다.</p><p><b>예를 들어:</b> 아 </p>',
    '<p>Choose your status.</p><p><b>For Example:</b> Owner </p>' => '<p>상태를 선택합니다.</p><p><b>예를 들어:</b> 자 </p>',
    '<p>Do you have mansard in the house?</p><p><b>For Example:</b> Yes </p>' => '<p>집 다락방니까?</p><p><b>예를 들어:</b> Yes </p>',
    '<p>Do you sell property with furniture?</p><p><b>For Example:</b> Yes </p>' => '<p>당신이 속성을 판매 가까?</p><p><b>예를 들어:</b> Yes </p>',
    '<p>Enter all amenities in your house separated by commas.</p><p><b>For Example:</b> AC, Electric owen, Washer, Refrigerator </p>' => '<p>목록은 모든 편의 쉼표로 구분됩니다.</p><p><b>예:</b> 에어컨,전기 스토브,세탁기,냉장고 </p>',
    '<p>Enter all amenities in your property separated by commas.</p><p><b>For Example:</b> AC, Electric owen, Washer, Refrigerator </p>' => '<p>목록 모든 시설에 아파트,쉼표로 구분됩니다.</p><p><b>예:</b> 에어컨,전기 스토브,세탁기,냉장고 </p>',
    '<p>Enter all amenities of the residential complex separated by commas.</p><p><b>For Example:</b> Gated community, swimming pool </p>' => '<p>목록 모든 편의시설의 별장 정착을 쉼표로 구분됩니다.</p><p><b>예를 들어:</b> 보안을,저녁에는 학교,공동 작업 공간,수영장 </p>',
    '<p>Enter all amenities of the stead separated by commas.</p><p><b>For Example:</b> Gas, electricity, water, sewage </p>' => '<p>목록 시설의 모든 토지의 쉼표로 구분됩니다.</p><p><b>예:</b> 가스,전기,물,하수구 </p>',
    '<p>Enter all amenities of your building separated by commas.</p><p><b>For Example:</b> Gym, Security 24/7, Parking </p>' => '<p>모든 편의 시설,건물의 쉼표로 구분됩니다.</p><p><b>예를 들어:</b> 체육관,보안 24/7,주차장 </p>',
    '<p>Enter detailed description of building.</p><p><b>For Example:</b> Park Grove will is located minutes from everything that makes Coconut Grove the best place to live in Miami: the blue waters of Biscayne Bay, and year-round sunlight and warmth.The nearby Coconut Grove Sailing Club, the Coral Reef Yacht Club, and the Dinner Key Marina all offer access to the water and to the area’s vibrant boating culture. </p>' => '<p>자세한 설명을 작성합니다.</p><p><b>예를 들어:</b> 건물에 위치한 도시의 중심 지역과 교통이 편리합니다. 아름다운 지역,여러 가지 운동장,실내 yard. 근처에 학교 No.4 니다. 에서 도보 거리에,수영장,클럽,체육관,많은 수 있습니다. </p>',
    '<p>Enter the area of the stead in square meters.</p><p><b>For Example:</b> 800 </p>' => '<p>지정 영역의 토지에서 평방 미터입니다.</p><p><b>예제:</b> 800 </p>',
    '<p>Enter the kitchen area (m²).</p><p><b>For Example:</b> 20 </p>' => '<p>지정 부엌은 평방 미터의 영역입니다.</p><p><b>예를 들어:</b> 20 </p>',
    '<p>Enter the name of the residential complex. Not required.</p><p><b>For Example:</b> Redwood </p>' => '<p>의 이름을 입력하고 코티지 합니다.</p><p><b>예를 들어:</b> 진주 </p>',
    '<p>Enter the total area (m²) of your apartment.</p><p><b>For Example:</b> 126 </p>' => '<p>을 입력하십시오 지역에 아파트의 평방 미터입니다.</p><p><b>예를 들어:</b> 126 </p>',
    '<p>Enter the total area of the house.</p><p><b>For Example:</b> 126 </p>' => '<p>입력 전체 면적의 집입니다.</p><p><b>예를 들어:</b> 126 </p>',
    '<p>Enter the total living area (m²).</p><p><b>For Example:</b> 53 </p>' => '<p>를 입력 생활의 지역에서 아파트 평방 미터입니다.</p><p><b>예를 들어:</b> 53 </p>',
    '<p>Enter the total living area. Not required.</p><p><b>For Example:</b> 53 </p>' => '<p>를 입력 살고있는 지역입니다. 지 않는 필수 입력 항목입니다.</p><p><b>예를 들어:</b> 53 </p>',
    '<p>If the building was overhauled, indicate in what year.</p><p><b>For Example:</b> 2015 </p>' => '<p>를 입력해의 수리,는 경우를 제작하였다.</p><p><b>예:</b> 2015 </p>',
    '<p>Please enter the zip code of the region where the property is located.</p><p><b>For Example:</b> 10028 </p>' => '<p>사용 우편 번호의 지역에는 이 숙박 시설은 위치하고 있습니다.</p><p><b>예제:</b> 92760 </p>',
    '<p>Select a currency from the list.</p><p><b>For Example:</b> Dollar </p>' => '<p>선택 목록에서 통화합니다.</p><p><b>예를 들어:</b> 달러 </p>',
    '<p>Select one of the available options.</p><p><b>For Example:</b> Private </p>' => '<p>중 하나를 선택할 수 있습니다.</p><p><b>예를 들어:</b> Private </p>',
    '<p>Select the type of bathroom from the list.</p><p><b>For Example:</b> Full </p>' => '<p>선택 유형의 화장실에서는 목록이다.</p><p><b>예:</b> 결합 </p>',
    '<p>Select type of floor plan. Not required.</p><p><b>For Example:</b> Loft </p>' => '<p>의 유형을 선택하고 계획이다. 하지 않습니다.</p><p><b>예를 들어:</b> 무료 </p>',
    '<p>Select your house cooling type.</p><p><b>For Example:</b> Split-system </p>' => '<p>선택 냉각시키는 방법을 집니다.</p><p><b>예제:</b> 분할 시스템 </p>',
    '<p>Select your house heating type.</p><p><b>For Example:</b> Autonomous gas heating </p>' => '<p>선택 방법의 집 난방합니다.</p><p><b>예:</b> 가스 난방 </p>',
    '<p>Specify building or complex name.</p><p><b>For Example:</b> Green tree </p>' => '<p>이름을 입력하의 건물 또는 주거 단지입니다.</p><p><b>예제:</b> 그린 트리 인 </p>',
    '<p>Specify commission you are willing to pay to the agent who will lead the buyer in percentage (%).</p><p><b>For Example:</b> 3 </p>' => '<p>의 백분율을 지정위원회 기꺼이 지불하는 에이전트를 위해 구매자에게 있습니다.</p><p><b>예를 들면:</b> 3 </p>',
    '<p>Specify number of balconies in your house.</p><p><b>For Example:</b> 1 </p>' => '<p>수를 지정의 발코니에 집니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify number of balconies in your property.</p><p><b>For Example:</b> 1 </p>' => '<p>의 수를 지정 발코니 아파트에서입니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify number of bathrooms in the house.</p><p><b>For Example:</b> 1 </p>' => '<p>지정할 수의 욕실에서 집니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify number of bathrooms in your property.</p><p><b>For Example:</b> 1 </p>' => '<p>지정 욕실 수 있습니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify number of bedrooms in the house.</p><p><b>For Example:</b> 2 </p>' => '<p>의 수를 지정 객실에는 집입니다.</p><p><b>예를 들어:</b> 2 </p>',
    '<p>Specify number of bedrooms in your property.</p><p><b>For Example:</b> 2 </p>' => '<p>지정된 침실 수 있습니다.</p><p><b>예를 들어:</b> 2 </p>',
    '<p>Specify number of car spaces in garage.</p><p><b>For Example:</b> 2 </p>' => '<p>수를 지정한 공간에서하고 있습니다.</p><p><b>예를 들어:</b> 2 </p>',
    '<p>Specify number of loggias in your house.</p><p><b>For Example:</b> 1 </p>' => '<p>수를 지정의 발코니에 집니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify number of loggias in your property.</p><p><b>For Example:</b> 1 </p>' => '<p>의 수를 지정 발코니 아파트에서입니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify number of rooms in the house.</p><p><b>For Example:</b> 4 </p>' => '<p>의 수를 지정 객실에서 집니다.</p><p><b>예를 들면:</b> 4 </p>',
    '<p>Specify number of rooms in your property.</p><p><b>For Example:</b> 4 </p>' => '<p>지정 객실 수 있습니다.</p><p><b>예를 들면:</b> 4 </p>',
    '<p>Specify number of storeys in building.</p><p><b>For Example:</b> 2 </p>' => '<p>수를 지정의 바닥에서 건물입니다.</p><p><b>예를 들어:</b> 2 </p>',
    '<p>Specify number of storeys in the house.</p><p><b>For Example:</b> 2 </p>' => '<p>수를 지정의 바닥에서 집니다.</p><p><b>예를 들어:</b> 2 </p>',
    '<p>Specify number of terraces in your house. Not required.</p><p><b>For Example:</b> 1 </p>' => '<p>지정된 숫자의 테라스에 집니다. 지 않는 필수 입력 항목입니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify number of your apartment.</p><p><b>For Example:</b> 205 </p>' => '<p>지정한 아파트의 번호입니다.</p><p><b>예를 들어:</b> 205 </p>',
    '<p>Specify property ceiling height in meters.</p><p><b>For Example:</b> 2.8 </p>' => '<p>지정한 천장의 높이에서 미터입니다.</p><p><b>예제:</b> 2.8 </p>',
    '<p>Specify property price.</p><p><b>For Example:</b> 300 000 </p>' => '<p>가격을 입력하의 개체입니다.</p><p><b>예를 들어:</b> 300 000 </p>',
    '<p>Specify purpose of use of the land.</p><p><b>For Example:</b> Residential </p>' => '<p>지정할 수의 욕실에서 집니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify sales options.</p><p><b>For Example:</b> Mortgage available </p>' => '<p>의 매개 변수를 지정합니다.</p><p><b>예를 들어:</b> 저당 가능 </p>',
    '<p>Specify the floor where your condo is located.</p><p><b>For Example:</b> 1 </p>' => '<p>지정한 바닥에는 아파트입니다.</p><p><b>예를 들어:</b> 1 </p>',
    '<p>Specify the folio or Tax ID number. This field is optional.</p><p><b>For Example:</b> 941-100-7-55 </p>' => '<p>입력을 지적 숫자의 개체입니다. 이 필드는 필요하지 않습니다.</p><p><b>예를 들어:</b> 77:50:456342:1567 </p>',
    '<p>Specify the house number.</p><p><b>For Example:</b> 28 </p>' => '<p>집 번호를 입력합니다.</p><p><b>예를 들어:</b> 28 </p>',
    '<p>Specify the material used in the construction of building.</p><p><b>For Example:</b> Brick </p>' => '<p>지정 물질의 건물의 건설입니다.</p><p><b>예를 들어:</b> 벽돌 </p>',
    '<p>Specify the material used in the construction of house.</p><p><b>For Example:</b> Wood </p>' => '<p>지정 물질의 건축을 집니다.</p><p><b>예를 들어:</b> 나무 </p>',
    '<p>Specify the year when property was completed.</p><p><b>For Example:</b> 2008 </p>' => '<p>지정해 건설되었다.</p><p><b>예제:</b> 2008 </p>',
    '<p>Start typing the address and choose from the options.</p><p><b>For Example:</b> 230 Wellington Street Traverse City, Michigan 49686, USA </p>' => '<p>시작 주소를 입력하고 옵션 중 하나를 선택합니다.</p><p><b>예제:</b> 230Wellington Street 트래버스시티 미시간 주 49686,미국 </p>',
    '<p>Write a detailed description of residential complex (Community).</p><p><b>For Example:</b> An inhabited flat community with good infrastructure. Nearby is a pine forest. Also there are school, kindergarten, a sports complex, shops. </p>' => '<p>에 대한 자세한 설명 코티지 마을입니다. 하지 않습니다.</p><p><b>예를 들어,</b> 거주 저지대 마을과 함께 좋은 인프라가 있습니다. 인근에 소나무 숲도 있습니다. 마을에가있는 학교,유치원,스포츠 콤플렉스,상점 등을 즐기실 수 있습니다. </p>',
    '<p>Write a detailed description of your house.</p><p><b>For Example:</b> The mansion has undergone a complete restoration. Every element has been rebuilt from the foundation up through the roof. </p>' => '<p>세부 사항에 대해 설명합니다.</p><p><b>예를 들어:</b> 새로운 지붕,이중창,강화 마루,마무리 장식의 벽입니다. 아름다운 현대적인 부엌,넓은 거실,넓은 테라스,의 훌륭한 전망을 감상할 수 있는 강습니다. </p>',
    'Add New Contact' => '를 추가하는 새로운 연락처',
    'Add {attribute}' => '{attribute}',
    'Additional Contact' => '추가 연락처',
    'Additional Contact Info' => '추가 연락처 정보',
    'Additional Information' => '추가 정보',
    'Address' => '주소',
    'Advertising on Partner Sites Allowed' => '허용에 광고 파트너 사이트',
    'Advertising on Site Allowed' => '은 광고에서 허용되는 포털',
    'Amenities' => '편의시설',
    'Amenities inside home' => '시설 내에 house',
    'Apartment number' => '아파트호',
    'Attach of detach {social} account' => '을 연결 또는 분리 계{social}',
    'Attach {provider} account' => '를 첨부하 계정{provider}',
    'Back' => '전',
    'Balcony' => '발코니',
    'Bathroom type' => '의 유형 욕실',
    'Building Description' => '빌딩 설명',
    'Building Details' => '건물의 자세',
    'Building Documents' => '문서의 건축물',
    'Building Parameters' => '의 매개 변수는 건물',
    'Building Photos' => '건물의 사진',
    'Building Title' => '의 이름을 건물',
    'Building amenities' => '시설의 건설',
    'Ceiling height' => '천장의 높이',
    'Choose a condo planning option' => '의 레이아웃을 선택 아파트',
    'Choose if you sell a condo with or without furniture' => '지정한 아파트 판매 가구와 함께 또는없이',
    'Choose if you want to buy a condo with or without furniture' => '지정할지 여부를 아파트를 구입하와 함께 또는없는 가구',
    'Choose the main language in which the original advertisement will be placed' => '선택한 기본 언어에 배치됩니다 원래 광고',
    'Choose the type of ownership for this property' => '지정 유형의 소유권에 대한 이 아파트',
    'Choose what you want to do' => '선택하이 무엇을 하고 싶',
    'Commission to Buyer`s agent' => '에이전트의위원회',
    'Company Banner' => '배너 회사',
    'Company Description' => '회사명',
    'Company Info' => '에 대한 정보를 회사',
    'Company Logo' => '회사 로고',
    'Company Title' => '회사 이름',
    'Company banner and logo' => '배너 및 회사 로고',
    'Construction Phases' => '단계의 건설',
    'Construction Progress' => '건설 진행',
    'Construction material' => '건설 자재',
    'Contact Info' => '연락처',
    'Continue' => '계속',
    'Cooling type' => '냉각 유형',
    'Currency' => '화',
    'Description of residential complex (Community)' => '설명 cottage village',
    'Detach {provider} account' => '분리 계{provider}',
    'Drag & drop photos here &hellip;' => '사진기',
    'Edit Company Member' => '편집한 회원사',
    'Enter contact info' => '입력하신의 연락처 정보',
    'Fill your announcement with maximum amount of details, so it will be seen more often' => '채 광고에서 상세으로 가능한,따라서 그것은 것이 종종 보입니다.',
    'Floor' => '층',
    'Floor plan' => '레이아웃',
    'Folio or Tax ID number' => '지적 수',
    'From' => '서',
    'Furniture' => '가구',
    'Heating type' => '난방 유형',
    'Hide other languages' => '숨기기 다른 언어',
    'Hint' => '팁',
    'Hint title' => '이름 힌트',
    'House Parameters' => '의 매개변수 집',
    'House condition' => '집의 상태',
    'House number' => '집 번호',
    'How many cars space in garage?' => '얼마나 많은 자동차에 차고가?',
    'I want to list my property for rent' => '고 싶은 포스팅의 발표의 항복을 위해 부동산 임대',
    'I want to list my property for sale' => '내가 원하는 게시하는 발표에서는 부동산의 판매',
    'I want to place a request to purchase a property' => '내가 원하는 요청하는 부동산의 구입',
    'I want to place a request to rent a my property' => '내가 원하는 요청하량의 부동산',
    'I`m ready to pay commission to agent' => '내가 기꺼이 지불하는 위원회는 에이전트로',
    'If you attach your social account to this profile then you may login with this social network on this site' => '에 연결할 경우 소셜 네트워크 계정이,당신이 할 수있을 것입 로그에 이 사이트를 통해 선택된 소셜 네트워크입니다.',
    'If you wish to change your password, you can do it here' => '하려면 비밀번호를 변경하고,당신은 여기에서 수행 할 수 있습니다',
    'Kitchen area' => '주방',
    'Land use' => '의 상태 사이트',
    'Language' => '언어',
    'List your property' => '장소의체',
    'List your property or request for Free' => '장소의 객체는 응용 프로그램을 위한 무료입니다.',
    'Living area' => '거실',
    'Location Settings' => '위치 설정',
    'Loggia' => '로지아',
    'Main Info' => '기본정보',
    'Mansard' => 'Attic',
    'Member Role' => '의 역할을자',
    'Member Status' => '상태의 참가자',
    'Nearby Sites' => '가까운 장소',
    'Number of bathrooms' => '수 욕실',
    'Number of bedrooms' => '숫자의 침실',
    'Number of rooms' => '객실수',
    'Overhaul year' => '년도 점검',
    'Ownership' => '의 상태를 판매자',
    'Password' => '비밀번호',
    'Password Confirmation' => '비밀번호 확인',
    'Password Settings' => '암호 설정',
    'Phone' => '전화',
    'Photo and Description' => '사진 및 설명',
    'Photo and Video' => '사진 및 비디오',
    'Please choose type of your advertisement:' => '지정 유형의 광고',
    'Post Company' => '게시 회사',
    'Post your request' => '입찰을 놓',
    'Previously uploaded building photos' => '이전에 업로드된 사진의 건물',
    'Price and terms of deal' => '가격 및 용어의 트랜잭션',
    'Primary Information' => '기본정보',
    'Profile Description' => '프로필 설명',
    'Profile Photo' => '사 provia',
    'Profile Short Description' => '에 대한 간략한 설명 프로필',
    'Property Address' => '개체의 주소',
    'Property Description' => '객실 설명',
    'Property Number of Rooms' => '숫자의 객실에서는 시설',
    'Property Parameters' => '의 매개 변수는 아파트',
    'Property Photos' => '사진 물체의',
    'Property Price' => '개체의 가격',
    'Property Total Area' => '전체 면적의체',
    'Property amenities' => '편의체',
    'Property category' => '객실 카테고리',
    'Property condition' => '객체의 상태',
    'Property description' => '객실 설명',
    'Property subcategory' => '하위 범주의 부동산',
    'Property subtype' => '객실 유형',
    'Property type' => '유형의 시설',
    'Publish Company' => '게시 회사',
    'Publish Listing' => '게시하는 시설',
    'Publish Request' => '게시 응용 프로그램',
    'Request Description' => '응용 프로그램에 대한 설명',
    'Request Parameters' => '의 매개변수 응용 프로그램',
    'Request Title' => '는 이름의 응용 프로그램',
    'Residential complex name' => '의 이름 코티지 settlement',
    'Residential complex parameters and description' => '매개 변수고 설명해 별도의 합의',
    'Sale with or without furniture' => '판매와 함께 또는없는 가구',
    'Save Changes' => '변경 사항을 저장',
    'Save and Continue' => '저장하고 계속',
    'Select contact type' => '연락처를 선택 유형',
    'Select the category of property from the options' => '카테고리를 선택의 부동산의 옵션',
    'Select the subcategory of property from the options' => '하위 범주를 선택합의 속성에서 옵션',
    'Select {attribute}' => '선택{attribute}',
    'Seller Status' => '의 상태를 판매자',
    'Set Address' => '주소를 지정',
    'Set Company Description' => '지정 회사에 대한 설명',
    'Set Company Title' => '의 이름을 지정 회사',
    'Set Member Role' => '지정한 역할의 참가자',
    'Set Member Status' => '지정 상태의 참가자',
    'Set Password' => '암호를 지정',
    'Set Password Confirmation' => '입력한 비밀번호 확인',
    'Set Permission for Advertising on Partner Sites' => '해상도 설정에서 광고 파트너 사이트',
    'Set Permission for Advertising on Site' => '해상도 설정을 광고하는 웹사이트에',
    'Set Profile Description' => '우리에게 조금 자신에 대해',
    'Set Profile Short Description' => '정에 대한 간단한 설명 프로필',
    'Set Request Description' => '지정 응용 프로그램에 대한 설명',
    'Set Request Title' => '의 이름을 입력하고 응용 프로그램',
    'Set User' => '지정한 사용자',
    'Set Your Additional Contact' => '입력하고 추가 연락처',
    'Set Your Phone' => '전화',
    'Set Your Username' => '당신의 이름을 입력하',
    'Set address of your property' => '주소를 지정합체의',
    'Set the floor where you want property to be on' => '을 지정하는 층 구입하려는 아파트',
    'Set the floor where your condo is located' => '을 지정하는 바닥에는 아파트',
    'Set the total property area indicated in the registration documents' => '지정의 전체 크기는 아파트에서 지정한 등록 서류',
    'Set the year in which the building was built' => '지정한 올해에는 건설',
    'Set the year in which the building was or will be renovated' => '지정한 올해에는 그것이었다는 것입 제공',
    'Set which built year should building have' => '지정한 올해의 건물의 건설',
    'Set {attribute}' => '지정{attribute}',
    'Show other languages' => '쇼 다른 언어',
    'Social Settings' => '설정을 소셜 네트워킹',
    'Specify additional options for the sale of this condo' => '추가 옵션을 지정한 판매의 이 아파트',
    'Specify all the additional features and advantages that are presented in the house. Write them separated by commas.<br>For example: playground, underground parking, internet in the house, security, intercom, etc.' => '지정한 모든 추가적인 특징 및 장점은 집에 있습니다. 쓰고 그들을 쉼표로 구분됩니다.<br>예를 들어:아이들을 위한 놀이터,지하 주차장,인터넷,집에서 보안,인터 등등.',
    'Specify all the additional features and improvements that are sold with the condo. Write them separated by commas.<br>For example: kitchen furniture, air conditioning, heated floors, washing machine, etc.' => '지정한 모든 추가적인 기능 및 향상된 판매되는과 함께 아파트입니다. 쓰고 그들을 쉼표로 구분됩니다.<br>예를 들어:부엌,에어컨,바닥 난방,세탁기,등등.',
    'Specify building amenities' => '을 지정하는 추가 시설의 건설',
    'Specify desired kitchen area' => '선택하고 원하는 지역 주방',
    'Specify if this is a new construction' => '는지 여부를 새로운 건축',
    'Specify if you want property to be in a new construction' => '입력해야하는 아파트에 있는 건물 또는 아',
    'Specify kitchen area' => '입력 주방 지역',
    'Specify owner`s status for this property' => '지정의 상태는 사람은 배치',
    'Specify property amenities' => '을 지정하는 추가 아파트의 시설',
    'Specify property cadastral number' => '입력을 지적도의 번호를 아파트',
    'Specify the area of all residential premises, exclude the area of the kitchen, bathroom, balconies and loggias' => '지정 영역의 모든 주거 건물을 포함하지 않는,주방,욕실,발코니 및 로지아',
    'Specify the area of stead' => '지정 영역의 땅',
    'Specify the condition of the condo (in your opinion)' => '지정한 아파트의 조건(에 귀하의 의견이)',
    'Specify the condition of the condo for your request' => '지정 조건은 아파트에 대한 응용 프로그램',
    'Specify the desired area of all residential premises, exclude the area of the kitchen, bathroom, balconies and loggias' => '을 지정하여 원하는 지역의 모든 삶의 영역을 포함하지 않는,부엌,화장실,발코니 및 로지아',
    'Specify the number of balconies for your request' => '의 수를 지정 발코니를 위한 응용 프로그램',
    'Specify the number of bathrooms' => '지정한 번호의 욕실',
    'Specify the number of floors for your request' => '수를 지정한 층을 위해 응용 프로그램',
    'Specify the number of floors in your building' => '수를 지정의 바닥에서 당신의 건물',
    'Specify the number of loggias for your request' => '의 수를 지정 발코니를 위한 응용 프로그램',
    'Specify the number of rooms for this property' => '의 수를 지정 객실에서 아파트',
    'Specify the number of rooms for your request' => '지정한 객실의 수를 응용 프로그램',
    'Specify the number of separate bedrooms in your condo. Guest can not be considered a bedroom' => '지정한 번호의 개인 객실에서 당신의 아파트입니다. 손님으로 간주할 수 없습 침실',
    'Specify the number of separate bedrooms you want to be in property' => '지정 무엇을 원하는 번호를 별도의 욕실에서 아파트',
    'Specify the property category' => '카테고리를 지정한 부동산',
    'Specify the property subcategory' => '지정한 하위 범주의 부동산',
    'Specify the stead amenities' => '지정한 시설의 땅',
    'Specify the type of bathroom' => '지정 유형의 화장실',
    'Specify what the house is built from. The options are: brick, monolith and foam concrete, monolith and brick, panel, wooden, etc.' => '지정한 벽 재료의 건물이다. 이 옵션은 벽돌,모노리스와 폼,기둥 벽돌,패널,나무,등등.',
    'Specify which material should building have' => '지정해야 한 건물',
    'Stead' => '토지',
    'Step {first} from {total}' => '단계{first}{total}',
    'Submit Request' => '게시 응용 프로그램',
    'Summary' => '총',
    'Terms of sale' => '판매 조건',
    'Terrace' => '테라스',
    'To' => '하기',
    'Total area' => '총 지역',
    'Total storeys' => '모든 층',
    'Type of ownership' => '의 유형을 소유권',
    'Update Profile' => '프로필 편집',
    'Upload Building Images' => '사진을 업로드하의 건물',
    'Upload Company Banner' => '업로드 배너',
    'Upload Company Logo' => '업로드하는 회사 로고',
    'Upload Profile Photo' => '당신의 사진을 업로드',
    'Upload Property Images' => '의 이미지를 업로드하는 개체',
    'Upload Your Profile Photo' => '당신의 사진을 업로드',
    'Upload quality photos.' => '업로드 고품질 사진',
    'User' => '사용자',
    'Username' => '사용자 이름',
    'Value {value} is too short or too long' => '의 가치{value}너무 짧거나 길',
    'Video Tour' => '비디오 투어',
    'We will also auto-translate into several popular languages to draw attention to your announcement' => '우리 또한 자동 번역 위해 몇 가지 일반적 언어에 관심을 끌기 위해,당신의 광고',
    'What is the number of loggias in your condo?' => '얼마나 많은 발코니에서 당신의 아파트가?',
    'Year of build' => '년도',
    'You can choose one of the following options: sell, buy, rent or rent out property' => '중 하나를 선택할 수 있습니다 다음과 같은 작업:판매,구매,임대 또는 임대 시설',
    'You can upload up to {count} photos' => '업로드 할 수 있습니다{count}사진',
    'Youtube Video Tour' => '비디오 투어에 유튜브',
    'Youtube link on video tour for this property' => '에 대한 링크를 비디오 투어에 유튜브에 대한 이체',
    'ZIP Code' => '우편 번호',
    'Company Type' => '회사의 유형',
    'Specify Company Type' => '지정 회사의 유형',
];