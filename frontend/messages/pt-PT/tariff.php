<?php 
 return [
    'Additional priority for your listings in catalog and similar propositions block' => 'Seus objetos serão colocados nos primeiros lugares no diretório e o bloco ofertas similares',
    'Additional priority in agents catalog' => 'Pelos primeiros lugares no diretório de agentes',
    'Additional priority on receiving requests' => 'Os pedidos na primeira fila',
    'Advanced' => 'Avançado',
    'Bank banner in the partners section on the main page' => 'Bandeira do banco em um bloco de parceiros na página principal',
    'Banner advertising on the site' => 'Banners de publicidade no site',
    'Banner location' => 'Localização do banner',
    'Choose tariff' => 'Selecionar a taxa de',
    'Description' => 'Descrição',
    'Free CRM and support. Search for other propositions if needed' => 'Frete CRM e de suporte. A seleção de outras opções, se necessário',
    'Free customer management CRM' => 'Frete CRM com a manutenção de clientes',
    'Grants access for receiving requests from clients which are interested in mortgage. <br> Also provides extra traffic for your site' => 'Permite-lhe receber pedidos de clientes interessados em hipoteca. <br> Também dá tráfego adicional para o seu site',
    'Helps you to attract more visitors for your site' => 'Ajuda a atrair mais visitantes para o seu site',
    'Minimal' => 'Mínima',
    'No' => 'Não',
    'Object Card' => 'Cartão do objeto',
    'On the main page below in the Partners section' => 'Na página principal, na parte inferior do bloco de Parceiros',
    'Requests for property purchase' => 'O pedido de compra de imóveis',
    'Requests for property sales' => 'O pedido de venda de imóveis',
    'Standard' => 'Padrão',
    'Tariffs for Banks for receiving requests on mortgage' => 'As tarifas para os bancos para obtenção de inscrição de hipoteca',
    'Tariffs for agents for receiving requests on property purchases' => 'Taxas de agentes em pedidos para compra de imóveis',
    'Tariffs for agents for receiving requests on property sales' => 'Taxas de agentes de recebimento de pedidos de venda de imóveis',
    'Yes' => 'Sim',
    'You will receive requests (leads) from clients who are interested in buying property' => 'Você vai receber pedidos (лиды) dos clientes interessados na compra de imóveis',
    'You will receive requests (leads) from clients who are interested in selling property' => 'Você vai receber pedidos (лиды) de clientes, interessados na venda de imóveis',
    '{count} hits per month' => '{count} de impressões por mês',
    '{count} per month' => '{count} por mês',
    '{object} objects<br/>({count} hits)' => '{object} objeto<br />({count} impressões)',
    '{price} per month' => '{price} ',
];