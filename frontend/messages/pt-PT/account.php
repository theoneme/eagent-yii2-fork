<?php

return [
    'Action' => 'Ação',
    'Add Company Member' => 'Para adicionar um membro a empresa',
    'All Properties' => 'Todos os objetos',
    'Are you sure?' => 'Você tem certeza?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'De volta às empresas',
    'Company Members' => 'Os participantes da empresa',
    'Create Company' => 'Criar uma empresa',
    'Logo' => 'O logotipo',
    'My Companies' => 'Meus empresa',
    'My Properties' => 'Meus objetos',
    'My Requests' => 'Meus pedidos',
    'Role' => 'Papel',
    'Status' => 'Estado',
    'Title' => 'Nome',
    'Username' => 'Nome de usuário',
];
