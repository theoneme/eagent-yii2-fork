<?php 
 return [
    'Building Amenities' => 'Vantagens do edifício',
    'Building Parameters' => 'As opções de construção',
    'Call' => 'Serviço',
    'Cancel' => 'Cancelamento',
    'Condos in house{title}{address}' => 'Apartamento na casa de{title}{address}',
    'Construction Progress' => 'Curso de construção',
    'Documentation' => 'Documentação',
    'For Rent' => 'para alugar',
    'For Sale' => 'para venda',
    'I am interested in property in building {building}' => 'Estou interessado em um imóvel no edifício {building}',
    'Location' => 'Localização',
    'Menu' => 'Menu',
    'More' => 'Mais',
    'Send' => 'Enviar',
    'Send Request' => 'Enviar uma consulta',
    'Share' => 'Compartilhar',
    'Similar Buildings' => 'Semelhantes edifício',
    'Unavailable' => 'não estão disponíveis',
    '{quarter} quarter' => '{quarter} trimestre',
    '{time, plural, one{# minute} other{# minutes}} {transport}' => '{time, plural, one{# minuto} few{# minutos} other{# minutos}} {transport}',
    '{type} {name}' => '',
    '{year} year' => '{year} ano',
];