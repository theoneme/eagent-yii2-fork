<?php 
 return [
    '<span >{rooms, plural, one{#+ rooms} other{#+ rooms}}</span>' => '<span >{rooms, plural, one{#+ banho} few{#+ quartos} other{#+ quartos}}</span>',
    'Action' => 'Ação',
    'Add comment for this property' => 'Adicione um comentário a este objeto',
    'Address' => 'Endereço',
    'Address or ZIP code' => 'Endereço ou código postal',
    'Advertisement' => 'Publicidade',
    'Agent name' => 'Nome do agente',
    'All Buildings' => 'Todos os edifícios',
    'Any Price' => 'Qualquer preço',
    'Area' => 'Praça',
    'Area {area} m²' => 'A área de {area} m²',
    'Average price per square meter {price}' => 'O preço médio por metro quadrado {price}',
    'Bedrooms' => 'Quartos',
    'Buy' => 'Compra',
    'Cancel' => 'Cancelamento',
    'Catalog Mode' => 'Modo de diretório',
    'Category' => 'Categoria',
    'Cheap first' => 'Primeiro baratos',
    'Choose' => 'Escolher',
    'Clear' => 'Limpar',
    'Close' => 'Fechar',
    'Comment' => 'Comentário',
    'Comment: {comment}' => 'Comentário de: {comment}',
    'Contract Price' => 'Preço negociável',
    'Create Report' => 'Formar uma seleção',
    'Default Mode' => 'Modo padrão',
    'Description' => 'Descrição',
    'Description is missing' => 'Nenhuma descrição disponível',
    'Districts' => 'Áreas',
    'Districts and microdistricts' => 'Distritos e bairros',
    'Enter city, region or country' => 'Escreva a cidade, região ou país',
    'Expensive first' => 'Primeiro caros',
    'Favorite' => 'Favoritos',
    'Filter' => 'Filtro',
    'Floor' => 'Andar',
    'From {min}' => 'A partir de {min}',
    'I`m interested in' => 'Me interessa',
    'Kitchen Area' => 'A área da cozinha',
    'Kitchen {area} m²' => 'Cozinha {area} m2',
    'List' => 'Lista',
    'List of similar properties:' => 'A lista de objetos semelhantes:',
    'Living Area' => 'Área construída',
    'Location' => 'Endereço',
    'Make Report' => 'Formar uma seleção',
    'Map' => 'Mapa',
    'Map Filter Mode' => 'O filtro de mapa',
    'Max' => 'Um máximo de',
    'Menu' => 'Menu',
    'Microdistricts' => 'Bairros',
    'Min' => 'Mínimo de',
    'Minimum price per square meter {price}' => 'O preço mínimo por metro quadrado {price}',
    'More' => 'Mais',
    'More actions' => 'Outras ações',
    'Name' => 'Nome',
    'New Constructions' => 'Os novos edifícios',
    'Newest first' => 'Primeiro, as novas',
    'No districts were found for this region' => 'Para esta região áreas não encontrado',
    'No microdistricts were found for this region' => 'A região de bairros não encontrado',
    'Number of Balconies' => 'O número de varandas',
    'Number of Bathrooms' => 'O número de sol',
    'Number of Bedrooms' => 'Número de quartos',
    'Number of Garages' => 'O número de garagens',
    'Number of Loggias' => 'O número de galerias',
    'Number of Rooms' => 'Número de quartos',
    'Operation' => 'Operação',
    'Photos' => 'Foto',
    'Possible price' => 'Possível preço',
    'Possible price: {price}' => 'Possível preço: {price}',
    'Price' => 'Preço',
    'Price per m²' => 'Preço por m²',
    'Property Area' => 'A área objeto',
    'Publish date' => 'Data de lançamento',
    'Recommended price for fast sale {price}' => 'Preço sugerido para venda rápida de {price}',
    'Recommended price {price}' => 'Preço sugerido {price}',
    'Rent' => 'Aluguel',
    'Report created {date}' => 'Uma seleção criada {date}',
    'Request Showcase' => 'Solicitar o visualizar',
    'Request a showcase' => 'Inscrever-se para o visualizar',
    'Reset' => 'Repor',
    'Rooms' => 'Quartos',
    'Save' => 'Manter',
    'Search' => 'Procurar',
    'Search by map' => 'Pesquisa mapa',
    'Secondary Buildings' => 'Edifício secundário',
    'See more' => 'Leia mais',
    'Select {attribute}' => 'Selecione {attribute}',
    'Selected Properties' => 'Os objetos selecionados',
    'Send Report' => 'Enviar uma seleção de',
    'Send selected properties to agent' => 'Enviar objetos selecionados agente',
    'Share' => 'Compartilhar',
    'Show' => 'Mostrar',
    'Showcase' => 'Ver',
    'Sort Type' => 'O tipo de classificação',
    'Sort by' => 'Ordenar por',
    'Specify parameters of your property, so we can make an analyze.' => 'Especifique as configurações de seu apartamento, para que possamos fazer a análise.',
    'Start typing and select one of the options' => 'Comece a imprimir e seleccione uma das opções',
    'Summary' => 'Total',
    'Title' => 'Nome',
    'To {max}' => 'Antes de {max}',
    'Unknown agency' => 'Desconhecido agência',
    'View Request' => 'Visualizar pedido',
    'Views' => 'Visualizações',
    'We have analyzed listings in internet that match your request. {count, plural, one{# property was analyzed} other{# properties were analyzed}}' => 'Nós analisamos os objetos colocados na internet e relevantes para a sua pesquisa. {count, plural, one{Foi analisado # objecto} few{Foi analisado # objecto} other{Foi analisado # objetos}}',
    'We made analyze for your property located by address {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}' => 'Nós analisamos o seu objeto, localizado em {address} de {rooms, plural, one{# banho} other{# banho}} e a praça {area, plural, one{# metro quadrado} few{# metros quadrados} other{# metros quadrados}}',
    'Year Built' => 'Ano',
    '{bedrooms, plural, one{# bedroom} other{# bedrooms}}' => '{bedrooms, plural, one{# quarto} few{# quartos} other{# quartos}}',
    '{count, plural, one{# object} other{# objects}}' => '{count, plural, one{# objeto} few{# objeto} other{# objetos}}',
    '{count, plural, one{# property found} other{# properties found}} near {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}' => '{count, plural, one{# o objecto encontrado} few{# objecto encontrado} other{# objectos encontrado}} cerca de {address} {rooms, plural, one{# banho} other{# banho}} e a praça {area, plural, one{# metro quadrado} few{# metros quadrados} other{# metros quadrados}}',
    '{count, plural, one{# result} other{# results}}' => '{count, plural, one{# objeto} few{# objeto} other{# objetos}}',
    '{flats, plural, one{# flat} other{# flats}} for rent' => '{flats, plural, one{# apartamento} few{# apartamento} other{# apartamentos}} para alugar',
    '{flats, plural, one{# flat} other{# flats}} for sale' => '{flats, plural, one{# apartamento} few{# apartamento} other{# apartamentos}} para venda',
    '{quarter} quarter' => '{quarter} trimestre',
    '{rooms, plural, one{# room} other{# rooms}}' => '{rooms, plural, one{# banho} few{# a sala} other{# salas}}',
    '{rooms, plural, one{#+ room} other{#+ rooms}}' => '{rooms, plural, one{#+ banho} few{#+ quartos} other{#+ quartos}}',
];