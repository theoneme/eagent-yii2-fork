<?php

return [
    'Action' => '行動',
    'Add Company Member' => '追加参加者の会社',
    'All Properties' => 'すべてのオブジェクト',
    'Are you sure?' => 'ご確認ください。',
    'Avatar' => 'アバター',
    'Back to companies' => '戻会社',
    'Company Members' => 'さらに、この会社はスタッフ',
    'Create Company' => '会社づくり',
    'Logo' => 'ロゴ',
    'My Companies' => '私の会社',
    'My Properties' => '私のオブジェ',
    'My Requests' => '私の応用',
    'Role' => 'の役割',
    'Status' => '状況',
    'Title' => '名称',
    'Username' => 'ユーザー名',
];
