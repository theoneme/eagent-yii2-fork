<?php

return [
    'Access Denied. You don’t have permission to access.' => 'アクセスを閉じます。 ご不十分なアクセス権です。',
    'Address' => '住所',
    'Agents' => '剤',
    'Already a member?' => '既に登録すか？',
    'Amenities' => '利点',
    'April' => '月',
    'Arabic' => 'アラブ',
    'Attention!' => '注意！',
    'August' => '月',
    'Back to Sign In' => '背面にログイン',
    'Bathrooms' => 'バスルーム',
    'Belarusian' => 'ベラルーシ',
    'Buildings' => '建物',
    'Buy' => '購入',
    'By joining I agree to {privacy} and {terms}, and receive emails from {site}.' => '登録同意いただ{privacy}{terms}とともに、メール受信の通知から{site}',
    'Chinese' => '中国',
    'Choose a Password' => '選べるパスワード',
    'Choose your new password' => '新しいパスワード',
    'Close' => '近',
    'Commercial Property' => '商業施設',
    'Common' => '合計',
    'Company Settings' => '設定会社',
    'Comparative report' => '比較報告書',
    'Condos' => 'アパート',
    'Confirm Password' => 'の確認パスワード',
    'Contact Agent' => 'お剤',
    'Contact local expert' => 'ご連絡は、現地の専門家',
    'Contact us' => 'お問合せ',
    'Contacts' => '連絡先',
    'Copy to Clipboard' => '',
    'Costs' => '費用',
    'Create CRM for free' => '作成CRM無料',
    'Create Company for free' => 'を当社に無料で',
    'Crop image' => '作物の画像',
    'Currencies' => '通貨',
    'Current balance' => '現在のバランス',
    'December' => '月',
    'Desktop version' => 'フルバージョン',
    'Do you have a questions? Contact local expert' => 'お子さまなどをしっかりとか。 連絡先の現地の専門家',
    'Do you have a questions? Contact us' => 'お子さまなどをしっかりとか。 お問合せ',
    'Do you really want to delete this item?' => 'に削除します。',
    'Email' => '',
    'Enter Address' => 'アドレスを入力し',
    'Enter email address' => 'メールアドレスを入力',
    'Enter phone in full format, including county code. Example +7 (123) 456 78 90' => '電話番号を指定します。全などの国コードです。 例えば+7(123)456 78 90',
    'Enter the email you used in registration. A password reset link will be sent to your email' => 'に入力してくださいメール登録時に登録します。 リンクのパスワードをリセットを送付させていただきましメール',
    'Enter your email or phone' => '入力メールや電話',
    'February' => '月',
    'Filter' => 'フィルター',
    'Finnish' => 'フィンランド',
    'Flats' => 'アパート',
    'Floor' => '階',
    'Follow us' => 'フォ',
    'Forgot Password' => 'パスワードをお忘れの方はこちら',
    'French' => 'フランス',
    'Georgian' => 'ジョージアン',
    'German' => 'ドイツ',
    'Get a free estimate of your property' => '車無料の評価において、資産',
    'Get in touch' => '個人情報に関するお問い合わせ先',
    'Go Home' => 'ホームへ',
    'Greek' => 'ギリシャ',
    'Hebrew' => 'ヘブライ語',
    'Hindi' => 'ヒンディー語',
    'Home' => 'ホーム',
    'Houses' => 'ホーム',
    'I`m not sure' => '私のプレの設定顔は丸いいただけるのは間違いなし)',
    'Income' => '利益',
    'Internal Server Error.' => '内部サーバエラーになります。',
    'Italian' => 'イタリア',
    'January' => '月',
    'Japanese' => '日本語',
    'July' => '月',
    'June' => '月',
    'Kitchen Area' => 'キッチン領域',
    'Korean' => '韓国語',
    'Land' => '地球',
    'Language' => '言語',
    'Less' => '以',
    'List your property' => '広告を',
    'List your request' => 'ご注文に際し',
    'Living Area' => '生活エリア',
    'Log in' => '入',
    'Logout' => '施',
    'March' => '月',
    'May' => '月',
    'Messages' => 'メッセージ',
    'Mobile version' => 'モバイル版',
    'More' => '以上',
    'My Accounts' => 'マイアカウント',
    'My Announcements' => '私の広告',
    'My CRM' => '私のCRM',
    'My Companies' => '私の会社',
    'My Profiles' => '私のプロファイル',
    'My Properties' => '私のオブジェ',
    'My Requests' => '私の応用',
    'My profile' => 'マイプロフィール',
    'New Constructions' => '建物',
    'New Password' => '新しいパスワード',
    'New!' => 'New!',
    'No' => 'No',
    'No results found' => '何も見つかり',
    'Not a member yet?' => '登録されていないですか。',
    'Notifications' => '通知',
    'November' => '月',
    'October' => '月',
    'Page not found' => 'ページが見つかりませんで',
    'Pass the test' => 'の試験',
    'Password' => 'パスワード',
    'Payment Management' => '決済管理',
    'Phone' => '電話',
    'Phone must contain from {0} numbers' => '電話が含まれている必要があ{0}文字',
    'Phone must contain up to {0} numbers' => '電話だけではなく、どなたでも無料で以上{0}文字',
    'Please set the amount of replenishment' => '',
    'Polish' => 'ポーランド',
    'Portuguese' => 'ポルトガル語',
    'Post for free' => '場無料',
    'Powered by {who}' => '',
    'Premium agent' => 'プレミアム剤',
    'Price' => '価格',
    'Print' => '印刷',
    'Privacy policy' => 'プライバシー方針',
    'Profile Settings' => '定型設定',
    'Properties for sale' => '物件の販売',
    'Property Address' => 'の住所の物件',
    'Property Category' => '物件カテゴリ',
    'Property Name' => 'プロパティの名前',
    'Property Price' => 'プロパティの値',
    'Register now' => '今すぐ登録してください',
    'Remember me' => 'グイン情報を記憶させ',
    'Rent' => '賃料',
    'Rental Property' => '賃',
    'Report' => '選択',
    'Request details are available only for agents, who bought our premium tariff' => '詳細の閲覧のみを受けた者が購入した保険料率ます。',
    'Requested agent page is not found' => 'お客様のアクセスしたページ剤で見つかりませんで',
    'Requested building page is not found' => '要求ページで見つかりませんでしたホーム',
    'Requests' => '用途',
    'Reset Password' => 'パスワードのリセット',
    'Rooms' => 'ルーム',
    'Rotate image' => '画像を回転し',
    'Russian' => 'ロシア',
    'Sale' => '販売',
    'Secondary Buildings' => '二次ビル',
    'Sell' => '販売',
    'Sell, Buy, Rent real estate directly' => '売買-賃貸物件を直接',
    'September' => '月',
    'Settings' => '設定',
    'Share on' => '',
    'Sign In' => '入',
    'Sign Up' => '登録',
    'Something is wrong' => 'もっ',
    'Spanish' => 'スペイン語',
    'Special proposition is available. Fill your contacts and we will send you details' => 'があります。 完了の連絡先情報を送付いたします情報',
    'Special proposition is available. Selected agent will contact you:' => 'があります。 もっと知りたいと思ったのか？ お答え致します:',
    'Submit' => '送信',
    'Support' => '支援',
    'Swedish' => 'スウェーデン',
    'Tariffs' => '料金',
    'Terms of service' => 'ユーザー同意書',
    'Terms of use' => '利用規約',
    'Thai' => 'タイ',
    'The page you are looking for was moved, removed, renamed or might never existed.' => 'をお探しのページに移動、消去、名前の変更、またはその恐れのあこれまで存在しなかったです。',
    'This email address has already been taken' => 'このメールアドレスを占領しよう',
    'This phone has already been taken' => 'このtelefnって使用されて',
    'Total Area' => '総面積',
    'Turkish' => 'トルコ',
    'Ukrainian' => 'ウクライナ',
    'Useful Links' => '役立つリンク',
    'User with such email as from {client} already exists' => '',
    'Vietnamese' => 'ベトナム',
    'View My Profile' => 'マイ-プロファイルに進ん',
    'View Tariffs' => 'ビュー料金',
    'Welcome to {site}' => 'ウ{site}',
    'Wish List' => 'ほしい物リスト',
    'Yes' => 'あり',
    'Your Name' => 'お名前',
    'Your link' => '',
    'by' => 'から',
    'from {price}' => 'から{price}',
    'in city {city}' => '{city}',
    'it\'s fun and easy!' => 'らくらく。',
    'or' => 'または',
    'with privacy policy' => 'プライバシー方針',
    'with terms of service' => 'の利用規約',
    '{area} m²' => '{area}m2',
    '{attribute} is invalid' => '{attribute}のエラーがあった場合',
    '{code} wallet' => '{code}の入れ',
    '{count, plural, one{# condo} other{# condos}}' => '{count, plural, one{#平} few{#平} other{#アパート}}',
    '{count, plural, one{# room} other{# rooms}}' => '{count, plural, one{#室} few{#室} other{#室}}',
    '{entity} for sale' => '{entity}の販売',
    '{entity} not found' => '{entity}見つかりませんで',
    '{months} months' => '{months}ヶ月',
    '{username} ({id} {email} {phone})' => '',
];
