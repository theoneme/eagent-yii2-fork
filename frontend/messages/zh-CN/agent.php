<?php 
 return [
    'About Us' => '关于我们',
    'All Activity' => '所有活动',
    'Bed / Bath' => '卧室',
    'Call' => '呼叫',
    'Cancel' => '取消',
    'Menu' => '菜单',
    'More' => '更多',
    'Our Listings & Sales' => '我们的设施和销售',
    'Our Past Sales' => '我们去销售',
    'Our active listings' => '我们的活动广告',
    'Professional Information' => '专业信息',
    'Property Address' => '地址的酒店',
    'Ratings & Reviews' => '评级和评论',
    'Represented' => '表示',
    'Save' => '保存',
    'Search property in cities' => '搜索房地产的城市',
    'Send' => '发送',
    'Send Request' => '发送一个请求',
    'Share' => '分享',
    'Show contact' => '联系人显示',
    'Sold Date' => '销售日期',
    'Specialties: {specialties}' => '职业：{specialties}',
    'Write a review' => '写一篇',
    '{count, plural, one{# Listing} other{# Listings}}' => '{count, plural, one{#广告} few{#声明} other{#广告}}',
    '{count} Recent Sales' => '',
    '{count} Reviews' => '{count}评论',
    '{count} Sales Last year' => '{count}的销售量在过去的一年',
    '{count} total reviews' => '{count}只是评论',
    '{entity} for sale in {city}' => '{entity}出售{city}',
    'Call' => '呼叫',
    'Cancel' => '取消',
    'Menu' => '菜单',
    'More' => '更多',
    'Save' => '保存',
    'Send' => '发送',
    'Send Request' => '发送一个请求',
    'Share' => '分享',
    'Show contact' => '联系人显示',
];