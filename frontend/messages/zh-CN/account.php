<?php

return [
    'Action' => '动作',
    'Add Company Member' => '添加一个参与公司',
    'All Properties' => '所有对象',
    'Are you sure?' => '你确定吗？',
    'Avatar' => '头像',
    'Back to companies' => '回到公司',
    'Company Members' => '该公司的成员',
    'Create Company' => '建立一个公司',
    'Logo' => '标志',
    'My Companies' => '我的公司',
    'My Properties' => '我的目的',
    'My Requests' => '我应用程序',
    'Role' => '的作用',
    'Status' => '状态',
    'Title' => '名称',
    'Username' => '用户名',
];
