<?php 
 return [
    'Additional priority for your listings in catalog and similar propositions block' => 'Вашы аб\'екты будуць на першых месцах у каталогу і блоку падобных прапаноў',
    'Additional priority in agents catalog' => 'Размяшчэнне на першых месцах у каталогу агентаў',
    'Additional priority on receiving requests' => 'Атрыманне заявак у першай чарзе',
    'Advanced' => 'Прасунуты',
    'Bank banner in the partners section on the main page' => 'Банэр банка ў блоку партнёраў на галоўнай старонцы',
    'Banner advertising on the site' => 'Банэрная рэклама на сайце',
    'Banner location' => 'Размяшчэнне банэра',
    'Choose tariff' => 'Выбраць тарыф',
    'Description' => 'Апісанне',
    'Free CRM and support. Search for other propositions if needed' => 'Бясплатная CRM і падтрымка. Падбор іншых варыянтаў пры неабходнасці',
    'Free customer management CRM' => 'Бясплатная CRM па вядзенню кліентаў',
    'Grants access for receiving requests from clients which are interested in mortgage. <br> Also provides extra traffic for your site' => 'Дазволіць вам атрымліваць заяўкі ад кліентаў, зацікаўленых у іпатэцы. <br> Таксама дае дадатковы трафік для вашага сайта',
    'Helps you to attract more visitors for your site' => 'Дапамагае прыцягнуць больш наведвальнікаў на ваш сайт',
    'Minimal' => 'Мінімальны',
    'No' => 'Няма',
    'Object Card' => 'Картка аб\'екта',
    'On the main page below in the Partners section' => 'На галоўнай старонцы ўнізе ў блоку Партнёры',
    'Requests for property purchase' => 'Заяўкі на куплю нерухомасці',
    'Requests for property sales' => 'Заяўкі на продаж нерухомасці',
    'Standard' => 'Стандартны',
    'Tariffs for Banks for receiving requests on mortgage' => 'Тарыфы для банкаў на атрыманне заявак па іпатэцы',
    'Tariffs for agents for receiving requests on property purchases' => 'Тарыфы для агентаў на атрыманне заявак па куплі нерухомасці',
    'Tariffs for agents for receiving requests on property sales' => 'Тарыфы для агентаў на атрыманне заявак па продажы нерухомасці',
    'Yes' => 'Ды',
    'You will receive requests (leads) from clients who are interested in buying property' => 'Вы будзеце атрымліваць заяўкі (ліды) ад кліентаў, зацікаўленых у куплі нерухомасці',
    'You will receive requests (leads) from clients who are interested in selling property' => 'Вы будзеце атрымліваць заяўкі (ліды) ад кліентаў, зацікаўленых у продажы нерухомасці',
    '{count} hits per month' => '{count} паказаў у месяц',
    '{count} per month' => '{count} у месяц',
    '{object} objects<br/>({count} hits)' => '{object} аб\'екта<br />({count} паказаў)',
    '{price} per month' => '{price} у месяц',
];