<?php

return [
    'Action' => 'Дзеянне',
    'Add Company Member' => 'Дадаць ўдзельніка кампаніі',
    'All Properties' => 'Усе аб\'екты',
    'Are you sure?' => 'Вы ўпэўненыя?',
    'Avatar' => 'Аватар',
    'Back to companies' => 'Таму да кампаніям',
    'Company Members' => 'Удзельнікі кампаніі',
    'Create Company' => 'Стварыць кампанію',
    'Logo' => 'Лагатып',
    'My Companies' => 'Мае кампаніі',
    'My Properties' => 'Мае аб\'екты',
    'My Requests' => 'Мае заяўкі',
    'Role' => 'Ролю',
    'Status' => 'Статус',
    'Title' => 'Назва',
    'Username' => 'Імя карыстальніка',
];
