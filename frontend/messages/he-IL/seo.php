<?php

return [
    ' Material - {value}' => ' חומר - {value}',
    ' area {value} m²' => ' עם שטח של {value} m2',
    ' area{minValue}{maxValue} m²' => ' עם שטח של{minValue}{maxValue} m2',
    ' at {address}' => ' בשדה {address}',
    ' from {value}' => ' מ {value}',
    ' in city {value}' => ' בעוד{value}',
    ' in {value}' => ' בעוד {value}',
    ' kitchen {value} m²' => ' המטבח {value} m2',
    ' made of «{value}»' => ' חומר "{value}"',
    ' to {value}' => ' ל - {value}',
    ' with parameters {attributes}' => ' עם פרמטרים {attributes}',
    ' {value, plural, one{# bedroom} other{# bedrooms}}' => ' {value, plural, one{# השינה} few{# השינה} other{# השינה}}',
    ' {value} bedroom' => ' {value} ישן.',
    ' {value} m²' => ' {value} m2',
    ' | {value, plural, one{# Bedroom} other{# Bedrooms}}' => ' | {value, plural, one{# השינה} few{# השינה} other{# השינה}}',
    ' «{title}»' => '',
    '(district {district})' => '(אזור {district})',
    '(microdistrict {microdistrict})' => '(השכונה {microdistrict})',
    '. {value, plural, one{# Bedroom} other{# Bedrooms}}' => '. {value, plural, one{# השינה} few{# השינה} other{# השינה}}',
    '. {value, plural, one{# Floor} other{# Floors}}' => '. {value, plural, one{# הרצפה} few{# הרצפה} other{# קומות}}',
    '. {value, plural, one{# Room} other{# Rooms}}' => '. {value, plural, one{# חדר} few{# חדר} other{# חדר}}',
    'Buildings' => 'הבניין',
    'Buy Property{City} | Convenient Search{minPrice}' => 'לקנות נכס{City} | Easy-to-חיפוש{minPrice}',
    'Buy a condo or a house on your own without intermediaries{city} easily with the eAgent.me service' => 'לקנות דירה או בית את עצמך ללא מתווכים{city} קל עם שירות eAgent.לי',
    'Buy a condo or a house{city} without intermediaries' => 'לקנות דירה או בית{city} ללא מתווכים',
    'Buy a condo{city}' => 'לקנות נכס{city}',
    'Buy or sell real estate{city} easily with the eAgent.me service' => 'לקנות או למכור נכס{city} קל עם שירות eAgent.לי',
    'Commercial property {action}{Area}' => 'נכס מסחרי {action} {Area}',
    'Commercial property{area} {action}{address}' => 'נכס מסחרי{area} {action}{address}',
    'Commercial property{area} {action}{city}. Detailed information on eAgent website.' => 'נכס מסחרי{area} {action}{city}. מידע מפורט באתר eAgent.',
    'Condos for sale and rent in house{name}{address}' => 'דירות למכירה, להשכרה בבית{name}{address}',
    'Free Property Appraisal{City} | We Will Help to Appraise for Free' => 'חופשי הערכת שווי של נכס{City} | אנחנו נעזור לך להעריך חינם',
    'Free appraisal of a condo or a house{city}' => 'הערכה חינם של הדירה או הבית{city}',
    'Free appraisal of a condo or a house{city} with the eAgent.me service' => 'הערכה חינם של דירה או בית{city} שירות eAgent.לי',
    'House{address}' => 'בבית{address}',
    'House{address}. Detailed information on eAgent website.' => 'הבית{address}. מידע מפורט באתר eAgent.',
    'House{name}{address}' => 'הבית{name}{address}',
    'New constructions' => 'מבנים',
    'On our site you can find {category} {forAction}, as well as place your ads {aboutAction} of {category} for free. eAgent.me is a convenient real estate portal. Free placement of ads about sale and rent of real estate{city}' => 'באתר שלנו אתה יכול למצוא {category} {forAction} חינם כדי למקם את המודעות שלך {aboutAction} {category}. eAgent.לי נוח פורטל הנדל " ן. מודעות חינם על מכירה, השכרה של נדל " ן{city}',
    'Our real estate agents will help you not only to find the best options for real estate, but also help you get mortgage loans and issue a deal' => 'שלנו סוכני נדל "ן יעזור לך לא רק למצוא את הטוב ביותר הנדל" ן, עזרה לקבל הלוואות משכנתא כדי לעשות עסקה.',
    'Prices from {value}' => 'מחירים מ {value}',
    'Real Estate {region}' => 'הנכס {region}',
    'Real estate agencies and agents{city}' => 'סוכנויות וסוכני נדל " ן{city}',
    'Real estate agent {name}' => 'סוכן הנדל " ן {name}',
    'Real estate agents{city}' => 'סוכני נדל " ן{city}',
    'Real estate in {city}' => 'רכוש בעוד {city}',
    'Rent' => 'להשכרה',
    'Rent Property{City} | Convenient Search{minPrice}' => 'לשכור נכס{City} | Easy-to-חיפוש{minPrice}',
    'Rent a condo or house{city} without intermediaries' => 'לשכור דירה או בית{city} ללא מתווכים',
    'Renting a condo or house independently without intermediaries{city} is easy with the service eAgent.me' => 'לשכור דירה או בית את עצמך ללא מתווכים{city} קל עם שירות eAgent.לי',
    'Sale' => 'מוכר',
    'Sell Property{City} | List Your Property For Free' => 'למכור את הנכס{City} | מודעות חינם',
    'Sell a condo or a house{city} without intermediaries' => 'כדי למכור דירה או בית{city} ללא מתווכים',
    'The eAgent service helps not only to buy and sell condos, houses, commercial real estate and land{city}, but also to get free assistance with paperwork and mortgage lending for real estate' => 'שירות eAgent עוזר לא רק כדי לקנות, למכור דירות, בתים, נכס מסחרי, קרקעות{city}, אבל גם בחינם כדי לעזור עם הניירת, משכנתאות הלוואות על נדל " ן',
    'There is no buildings found for your request' => 'מבנים לבקשתך, לא נמצאה',
    'There is no {category} {forAction} in this city. You can be the first to place an announcement {aboutAction} or request for what interests you.' => 'בעיר הזאת אין {category} {forAction}. אתה יכול להיות הראשון לפרסם מודעה {aboutAction} או בקשה על משהו שמעניין אותך.',
    'To sell a condo or a house on your own without intermediaries{city} easily with the eAgent.me service' => 'למכור דירה או בית את עצמך ללא מתווכים{city} קל עם שירות eAgent.לי',
    'We will help to sell a condo or a house{city} and if you need to provide all the necessary services from preparing a condo for sale to a contract of sale. To sell without intermediaries simply with the site eAgent' => 'אנחנו נעזור לך למכור את הדירה או הבית{city}, אם אתה צריך לספק את כל השירותים הדרושים מתוך הכנה של הדירה למכירה לפני סיום חוזה המכירה. למכור ללא מתווכים, פשוט על ידי eAgent באתר',
    'We will help you buy a condo or a house{city} and if you need to provide all the necessary services from the selection of a condo to obtain a mortgage loan and a contract of sale. Buy without intermediaries just with the site eAgent' => 'אנחנו נעזור לך לקנות דירה או בית{city}, אם אתה צריך לספק את כל השירותים הנדרשים מן הדירות כדי לקבל הלוואת משכנתא, חוזה המכירה. לקנות ללא מתווכים רק eAgent באתר',
    'We will help you evaluate your condo or house for free{city} and also provide your assessment of the real estate market{city}' => 'אנחנו נעזור לך בחינם כדי להעריך את הדירה או את הבית{city}, כמו גם לספק את ההערכה של שוק הנדל " ן{city}',
    'We will help you not only to choose the best option, but also to process documents for free' => 'אנחנו נעזור לך לבחור את האפשרות הטובה ביותר, והוא גם חופשי לבצע מסמכים',
    'We will help you not only to choose the best option, but also to process documents {forAction} of {category}{attributes}{city} for free' => 'אנחנו נעזור לך לבחור את האפשרות הטובה ביותר, והוא גם חופשי לבצע מסמכים {forAction} {category}{attributes}{city}',
    'We will help you rent a condo or a house{city} and if you need we will provide all the necessary services from the selection of a condo rental option to the conclusion of a rental agreement. Rent without intermediaries with eAgent site is safe and easy' => 'אנחנו נעזור לשכור דירה או בית{city}, אם אתה צריך לספק את כל השירותים הדרושים מתוך מבחר של האפשרות לשכור את הדירה לחתום על הסכם השכירות. שכר דירה ללא מתווכים עם האתר eAgent בטוח וקל',
    'about rent' => 'על השכרת דירה',
    'about sale' => 'למכירה',
    'for Rent' => 'להשכרה',
    'for Sale' => 'למכירה',
    'for rent' => 'להשכרה',
    'for sale' => 'למכירה',
    '{Category} {action}{Area}' => '',
    '{Category} |{Area}{Action}{price}{Address}' => '',
    '{Category}{Action}{price}{Address}' => '',
    '{Category}{landCategory}{Area} {action}{saddress}' => '',
    '{Floors}{Rooms}{Category}{Bedrooms}{Action}{price}{BuildingName}{Address}' => '',
    '{Rooms}{Category}{Bedrooms}{Action}{price}{BuildingName}{Address}' => '',
    '{count, plural, =0{Agencies and Agents} one{# Agency or Agent} other{# Agencies and Agents}} (Real estate){City}' => '{count, plural, =0{סוכנויות וסוכנים} one{# סוכנות או סוכן} few{# הסוכנות הסוכן} other{# סוכנויות וסוכנים.}} (אמיתי){City}',
    '{count, plural, =0{Condos} one{# condo} other{# condos}} for sale(for rent) in House{name}{Address}' => '{count, plural, =0{דירות} one{# שטוח} other{# שטוח}} למכירה(שכר הדירה) בבית{name}{Address}',
    '{count, plural, =0{} other{#}} {Category} {action}{Attributes}{City}{minPrice}' => '',
    '{count, plural, =0{} other{#}} {Category} {forAction}{Attributes}{City}{minPrice}{maxPrice} | List your property for free on eAgent.me website' => '{0} {Category} {forAction}{Attributes}{City}{minPrice}{maxPrice} | פרסום חינם עבור נדל " ן באתר eAgent.לי',
    '{count, plural, one{# agency and agent} other{# agencies and agents}} for real estate sale, purchase and rental{city} you can find on the site eAgent' => '{count, plural, one{# הסוכנות והסוכן} few{# הסוכנות הסוכן} other{# סוכנויות וסוכנים}} במכירה, רכישה והשכרה של נכסים{city} אתה יכול למצוא באתר eAgent',
    '{count, plural, one{# real estate agent} other{# real estate agents}} {city} you can find at the eAgent website' => '{count, plural, one{# סוכן הנדל " ן} few{# סוכן הנדל " ן} other{# סוכני נדל " ן}}{city} אתה יכול למצוא באתר eAgent',
    '{countSale, plural, =0{Condos} one{# condo} other{# condos}} for sale, {countRent, plural, =0{condos} one{# condo} other{# condos}} for rent in a residential house{name}{address}. You can view detailed information about the house and compare prices on eAgent.me portal' => '{countSale, plural, =0{דירות} one{# שטוח} other{# דירות}} למכירה, {countRent, plural, =0{דירות} one{# שטוח} other{# דירות}} לשכור בית{name}{address}. אתה יכול לקרוא מידע מפורט על המבנה ולהשוות מחירים על eAgent באתר.לי',
    '{entity} for sale in {city}' => '{entity} למכירה {city}',
    '{floors}{rooms}house{bedrooms}{material}{area} {action}{address}' => '{floors}{rooms}בית{bedrooms}{material}{area} {action}{address}',
    '{floors}{rooms}house{bedrooms}{material}{area} {action}{city}. Detailed information on eAgent website.' => '{floors}{rooms}בית{bedrooms}{material}{area} {action}{city}. מידע מפורט באתר eAgent.',
    '{floors}{rooms}{category} {action}{bedrooms}{Area}{kitchenArea}' => '',
    '{floors}{rooms}{nbedrooms}{category} {action}{buildingName}.{flatNumber}{saddress}' => '',
    '{rooms}condo{bedrooms}{area} {action} {price} | {address}' => '{rooms}הדירה{bedrooms}{area} {action} {price} | {address}',
    '{rooms}condo{bedrooms}{area} {action}{address}' => '{rooms}הדירה{bedrooms}{area} {action}{address}',
    '{rooms}{category} {action}{bedrooms}{Area}{kitchenArea}' => '',
    '{rooms}{nbedrooms}{category} {action}{buildingName}.{flatNumber}{saddress}' => '',
    '{value} Room ' => '{value} החדר ',
    '{value} Storey ' => '{value} הסיפור ',
    '{value} room ' => '{value}-חדר ',
    '{value}-storey ' => '{value}-סיפור ',
];
