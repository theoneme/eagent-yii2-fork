<?php

return [
    'Action' => 'פעולה',
    'Add Company Member' => 'כדי להוסיף משתתף של החברה',
    'All Properties' => 'כל האובייקטים',
    'Are you sure?' => 'אתה בטוח?',
    'Avatar' => 'אווטאר',
    'Back to companies' => 'חזרה לחברות',
    'Company Members' => 'חברי החברה',
    'Create Company' => 'כדי ליצור חברה.',
    'Logo' => 'לוגו',
    'My Companies' => 'החברה שלי',
    'My Properties' => 'האובייקטים שלי',
    'My Requests' => 'היישומים שלי',
    'Role' => 'התפקיד',
    'Status' => 'סטטוס',
    'Title' => 'שם',
    'Username' => 'שם משתמש',
];
