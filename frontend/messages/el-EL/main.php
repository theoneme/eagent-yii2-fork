<?php

return [
    'Access Denied. You don’t have permission to access.' => 'Δεν επιτρέπεται η πρόσβαση. Δεν έχετε επαρκή δικαιώματα πρόσβασης.',
    'Address' => 'Διεύθυνση',
    'Agents' => 'Πράκτορες',
    'Already a member?' => 'Ήδη μέλος;',
    'Amenities' => 'Πλεονεκτήματα',
    'April' => 'Απρίλιος',
    'Arabic' => 'Αραβικά',
    'Attention!' => 'Προσοχή!',
    'August' => 'Αύγουστος',
    'Back to Sign In' => 'Επιστροφή στην εξουσιοδότησης',
    'Bathrooms' => 'Μπάνια',
    'Belarusian' => 'Λευκορωσικά',
    'Buildings' => 'Το κτίριο',
    'Buy' => 'Αγοράστε',
    'By joining I agree to {privacy} and {terms}, and receive emails from {site}.' => 'Με την εγγραφή σας, συμφωνείτε {privacy} και {terms}, καθώς και για να λαμβάνετε τις ειδοποιήσεις μέσω Email από {site}',
    'Chinese' => 'Κινέζικα',
    'Choose a Password' => 'Επιλέξτε τον κωδικό πρόσβασης',
    'Choose your new password' => 'Επιλέξτε ένα νέο κωδικό πρόσβασης',
    'Close' => 'Κλείσιμο',
    'Commercial Property' => 'Εμπορική ακίνητη περιουσία',
    'Common' => 'Συνολικός',
    'Company Settings' => 'Ρυθμίσεις της εταιρείας',
    'Comparative report' => 'Συγκριτική έκθεση',
    'Condos' => 'Διαμέρισμα',
    'Confirm Password' => 'Επιβεβαιώστε τον κωδικό πρόσβασης',
    'Contact Agent' => 'Επικοινωνήστε με τον μεσίτη',
    'Contact local expert' => 'Επικοινωνήστε με έναν τοπικό εμπειρογνώμονα',
    'Contact us' => 'Επικοινωνήστε μαζί μας',
    'Contacts' => 'Επαφές',
    'Copy to Clipboard' => '',
    'Costs' => 'Το κόστος',
    'Create CRM for free' => 'Δημιουργία CRM δωρεάν',
    'Create Company for free' => 'Δημιουργήσει μια εταιρεία δωρεάν',
    'Crop image' => 'Για να περικόψετε την εικόνα',
    'Currencies' => 'Νόμισμα',
    'Current balance' => 'Το τρέχον υπόλοιπο',
    'December' => 'Δεκέμβριος',
    'Desktop version' => 'Η πλήρης έκδοση',
    'Do you have a questions? Contact local expert' => 'Έχετε ερωτήσεις; Επικοινωνήστε με έναν τοπικό εμπειρογνώμονα',
    'Do you have a questions? Contact us' => 'Έχετε ερωτήσεις; Επικοινωνήστε μαζί μας',
    'Do you really want to delete this item?' => 'Είστε βέβαιοι ότι θέλετε να διαγράψετε αυτή την εγγραφή;',
    'Email' => '',
    'Enter Address' => 'Πληκτρολογήστε τη διεύθυνση',
    'Enter email address' => 'Πληκτρολογήστε τη διεύθυνση email',
    'Enter phone in full format, including county code. Example +7 (123) 456 78 90' => 'Καθορίστε το τηλέφωνό σας σε πλήρη μορφή, μαζί με τον κωδικό της χώρας. Για παράδειγμα +7 (123) 456 78 90',
    'Enter the email you used in registration. A password reset link will be sent to your email' => 'Πληκτρολογήστε το email που χρησιμοποιήσατε κατά την εγγραφή σας. Το link για να επαναφέρετε τον κωδικό πρόσβασής σας έρθει ένα e-mail',
    'Enter your email or phone' => 'Πληκτρολογήστε το email ή το τηλέφωνό σας',
    'February' => 'Νοέμβριος',
    'Filter' => 'Φίλτρο',
    'Finnish' => 'Φινλανδικά',
    'Flats' => 'Διαμέρισμα',
    'Floor' => 'Όροφος',
    'Follow us' => 'Ακολουθήστε μας',
    'Forgot Password' => 'Ξεχάσατε τον κωδικό σας',
    'French' => 'Γαλλικά',
    'Georgian' => 'Γεωργιανά',
    'German' => 'Γερμανικά',
    'Get a free estimate of your property' => 'Πάρτε μια δωρεάν εκτίμηση του ακινήτου σας',
    'Get in touch' => 'Πώς να επικοινωνήσετε μαζί μας',
    'Go Home' => 'Μετάβαση στην αρχική',
    'Greek' => 'Ελληνικά',
    'Hebrew' => 'Εβραϊκά',
    'Hindi' => 'Χίντι',
    'Home' => 'Στο σπίτι',
    'Houses' => 'Το σπίτι',
    'I`m not sure' => 'Δεν είμαι σίγουρος(σίγουρη)',
    'Income' => 'Τα έσοδα',
    'Internal Server Error.' => 'Εσωτερικό σφάλμα διακομιστή.',
    'Italian' => 'Ιταλικά',
    'January' => 'Ιανουάριος',
    'Japanese' => 'Ιαπωνικά',
    'July' => 'Ιούλιος',
    'June' => 'Ιούνιος',
    'Kitchen Area' => 'Κουζίνα',
    'Korean' => 'Κορεατικά',
    'Land' => 'Η γη',
    'Language' => 'Γλώσσα',
    'Less' => 'Λιγότερο',
    'List your property' => 'Καταχωρήστε την αγγελία σας',
    'List your request' => 'Για να τοποθετήσετε την αίτηση',
    'Living Area' => 'Καλύπτεται περιοχή',
    'Log in' => 'Συνδεθείτε',
    'Logout' => 'Έξοδος',
    'March' => 'Μάρτιος',
    'May' => 'Μάιος',
    'Messages' => 'Μηνύματα',
    'Mobile version' => 'Mobile έκδοση',
    'More' => 'Περισσότερα',
    'My Accounts' => 'Οι λογαριασμοί μου',
    'My Announcements' => 'Αγγελίες μου',
    'My CRM' => 'Μου CRM',
    'My Companies' => 'Οι εταιρείες',
    'My Profiles' => 'Μου προφίλ',
    'My Properties' => 'Τα αντικείμενα',
    'My Requests' => 'Οι αιτήσεις',
    'My profile' => 'Το προφίλ μου',
    'New Constructions' => 'Νέα κτίρια',
    'New Password' => 'Ένα νέο κωδικό πρόσβασης',
    'New!' => 'Νέο!',
    'No' => 'Όχι',
    'No results found' => 'Τίποτα δεν βρέθηκε',
    'Not a member yet?' => 'Δεν έχετε εγγραφεί ακόμα;',
    'Notifications' => 'Ειδοποιήσεις',
    'November' => 'Οκτώβριος',
    'October' => 'Οκτώβριος',
    'Page not found' => 'Η σελίδα δεν βρέθηκε',
    'Pass the test' => 'Να περάσει το τεστ',
    'Password' => 'Τον κωδικό πρόσβασης',
    'Payment Management' => 'Διαχείριση πληρωμών',
    'Phone' => 'Τηλέφωνο',
    'Phone must contain from {0} numbers' => 'Το τηλέφωνό σας πρέπει να αποτελείται από {0} χαρακτήρες',
    'Phone must contain up to {0} numbers' => 'Το τηλέφωνό σας πρέπει να περιέχει όχι περισσότερο από {0} χαρακτήρες',
    'Please set the amount of replenishment' => '',
    'Polish' => 'Πολωνικά',
    'Portuguese' => 'Πορτογαλικά',
    'Post for free' => 'Για να τοποθετήσετε δωρεάν',
    'Powered by {who}' => '',
    'Premium agent' => 'Premium πράκτορας',
    'Price' => 'Τιμή',
    'Print' => 'Εκτύπωση',
    'Privacy policy' => 'Πολιτική προστασίας προσωπικών δεδομένων',
    'Profile Settings' => 'Ρυθμίσεις προφίλ',
    'Properties for sale' => 'Ακίνητα προς πώληση',
    'Property Address' => 'Διεύθυνση ακινήτου',
    'Property Category' => 'Κατηγορία ακινήτων',
    'Property Name' => 'Όνομα ακινήτων',
    'Property Price' => 'Το κόστος των ακινήτων',
    'Register now' => 'Εγγραφείτε τώρα',
    'Remember me' => 'Να με θυμάσαι',
    'Rent' => 'Ενοικίαση',
    'Rental Property' => 'Ενοικιάσεις ακινήτων',
    'Report' => 'Συλλογή',
    'Request details are available only for agents, who bought our premium tariff' => 'Λεπτομέρειες της αίτησης είναι διαθέσιμα στην προβολή μόνο παράγοντες που έχουν αγοραστεί premium ποσοστό.',
    'Requested agent page is not found' => 'Η ζητούμενη σελίδα παράγοντα δεν βρέθηκε',
    'Requested building page is not found' => 'Η ζητούμενη σελίδα στο σπίτι δεν βρέθηκε',
    'Requests' => 'Οι αιτήσεις',
    'Reset Password' => 'Επαναφορά του κωδικού πρόσβασης',
    'Rooms' => 'Δωμάτιο',
    'Rotate image' => 'Περιστρέψετε την εικόνα',
    'Russian' => 'Ελληνικά',
    'Sale' => 'Πώληση',
    'Secondary Buildings' => 'Δευτερεύοντα κτίρια',
    'Sell' => 'Πώληση',
    'Sell, Buy, Rent real estate directly' => 'Το πουλήσεις, μπορείς να Αγοράσεις, Арендуй ακινήτων άμεσα',
    'September' => 'Το σεπτέμβριο',
    'Settings' => 'Ρυθμίσεις',
    'Share on' => '',
    'Sign In' => 'Συνδεθείτε',
    'Sign Up' => 'Εγγραφείτε',
    'Something is wrong' => 'Κάτι πήγε στραβά',
    'Spanish' => 'Ισπανικά',
    'Special proposition is available. Fill your contacts and we will send you details' => 'Υπάρχει μια ειδική προσφορά. Συμπληρώστε τα στοιχεία επικοινωνίας σας και θα σας στείλουμε τις πληροφορίες',
    'Special proposition is available. Selected agent will contact you:' => 'Υπάρχει μια ειδική προσφορά. Θέλετε να μάθετε λεπτομέρειες; Σας απαντήσει:',
    'Submit' => 'Αποστολή',
    'Support' => 'Υποστήριξη',
    'Swedish' => 'Σουηδικά',
    'Tariffs' => 'Οι τιμές',
    'Terms of service' => 'Συμφωνία χρήστη',
    'Terms of use' => 'Όροι χρήσης',
    'Thai' => 'Ταϊλάνδης',
    'The page you are looking for was moved, removed, renamed or might never existed.' => 'Η σελίδα που ψάχνετε έχει μετακινηθεί, διαγραφεί, μετονομαστεί ή, ίσως, ποτέ δεν υπήρχε.',
    'This email address has already been taken' => 'Αυτή η διεύθυνση ηλεκτρονικού ταχυδρομείου έχει ήδη κάποιον απασχολημένο',
    'This phone has already been taken' => 'Το телеофн ήδη κάποιον χρησιμοποιείται',
    'Total Area' => 'Η συνολική έκταση',
    'Turkish' => 'Τουρκικά',
    'Ukrainian' => 'Ουκρανικά',
    'Useful Links' => 'Χρήσιμες συνδέσεις',
    'User with such email as from {client} already exists' => '',
    'Vietnamese' => 'Βιετναμικά',
    'View My Profile' => 'Μπείτε στο προφίλ μου',
    'View Tariffs' => 'Προβολή των τιμολογίων',
    'Welcome to {site}' => 'Καλώς ήρθατε στο {site}',
    'Wish List' => 'Λίστα επιθυμιών σας',
    'Yes' => 'Ναι',
    'Your Name' => 'Το όνομα',
    'Your link' => '',
    'by' => 'από',
    'from {price}' => 'από {price}',
    'in city {city}' => 'του{city}',
    'it\'s fun and easy!' => 'είναι εύκολο και απλό!',
    'or' => 'ή',
    'with privacy policy' => 'με την πολιτική προστασίας προσωπικών δεδομένων',
    'with terms of service' => 'με τους όρους χρήσης',
    '{area} m²' => '{area} m2',
    '{attribute} is invalid' => '{attribute} περιέχει σφάλμα',
    '{code} wallet' => '{code} πορτοφόλι',
    '{count, plural, one{# condo} other{# condos}}' => '{count, plural, one{# διαμέρισμα} few{# διαμέρισμα} other{# διαμερισμάτων}}',
    '{count, plural, one{# room} other{# rooms}}' => '{count, plural, one{# δωμάτιο} few{# δωμάτιο} other{# δωμάτια}}',
    '{entity} for sale' => '{entity} προς πώληση',
    '{entity} not found' => '{entity} δεν βρέθηκε',
    '{months} months' => '{months} μήνες',
    '{username} ({id} {email} {phone})' => '',
];
