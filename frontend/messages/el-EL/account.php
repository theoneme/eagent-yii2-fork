<?php

return [
    'Action' => 'Δράση',
    'Add Company Member' => 'Προσθήκη μέλους της εταιρείας',
    'All Properties' => 'Όλα τα αντικείμενα',
    'Are you sure?' => 'Είστε σίγουροι;',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Πίσω στις επιχειρήσεις',
    'Company Members' => 'Τα μέλη της εταιρείας',
    'Create Company' => 'Δημιουργήσει μια εταιρεία',
    'Logo' => 'Το λογότυπο',
    'My Companies' => 'Οι εταιρείες',
    'My Properties' => 'Τα αντικείμενα',
    'My Requests' => 'Οι αιτήσεις',
    'Role' => 'Ρόλο',
    'Status' => 'Το καθεστώς',
    'Title' => 'Όνομα',
    'Username' => 'Όνομα χρήστη',
];
