<?php

return [
    'Access Denied. You don’t have permission to access.' => 'Доступ закрыт. У вас недостаточно прав доступа.',
    'Address' => 'Адрес',
    'Agents' => 'Агенты',
    'Already a member?' => 'Уже зарегистрированы?',
    'Amenities' => 'Преимущества',
    'April' => 'Апрель',
    'Arabic' => 'Арабский',
    'Attention!' => 'Внимание!',
    'August' => 'Август',
    'Back to Sign In' => 'Вернуться к авторизации',
    'Bathrooms' => 'Ванные',
    'Belarusian' => 'Белорусский',
    'Buildings' => 'Здания',
    'Buy' => 'Купить',
    'By joining I agree to {privacy} and {terms}, and receive emails from {site}.' => 'Регистрируясь, Вы соглашаетесь {privacy} и {terms}, а также на получение Email уведомлений от {site}',
    'Chinese' => 'Китайский',
    'Choose a Password' => 'Выберите пароль',
    'Choose your new password' => 'Выберите новый пароль',
    'Close' => 'Закрыть',
    'Commercial Property' => 'Коммерческая недвижимость',
    'Common' => 'Общее',
    'Company Settings' => 'Настройки компании',
    'Comparative report' => 'Сравнительный отчет',
    'Condos' => 'Квартиры',
    'Confirm Password' => 'Подтвердите пароль',
    'Contact Agent' => 'Связаться с агентом',
    'Contact local expert' => 'Связаться с местным экспертом',
    'Contact us' => 'Связаться с нами',
    'Contacts' => 'Контакты',
    'Copy to Clipboard' => '',
    'Costs' => 'Расходы',
    'Create CRM for free' => 'Создать CRM бесплатно',
    'Create Company for free' => 'Создать компанию бесплатно',
    'Crop image' => 'Обрезать изображение',
    'Currencies' => 'Валюты',
    'Current balance' => 'Текущий баланс',
    'December' => 'Декабрь',
    'Desktop version' => 'Полная версия',
    'Do you have a questions? Contact local expert' => 'У вас есть вопросы? Свяжитесь с местным экспертом',
    'Do you have a questions? Contact us' => 'У вас есть вопросы? Свяжитесь с нами',
    'Do you really want to delete this item?' => 'Вы действительно хотите удалить эту запись?',
    'Email' => '',
    'Enter Address' => 'Введите адрес',
    'Enter email address' => 'Введите email адрес',
    'Enter phone in full format, including county code. Example +7 (123) 456 78 90' => 'Укажите телефон в полном виде, включая код страны. Например +7 (123) 456 78 90',
    'Enter the email you used in registration. A password reset link will be sent to your email' => 'Введите email, который вы использовали при регистрации. Ссылка для сброса пароля придет вам на почту',
    'Enter your email or phone' => 'Введите email или телефон',
    'February' => 'Февраль',
    'Filter' => 'Фильтр',
    'Finnish' => 'Финский',
    'Flats' => 'Квартиры',
    'Floor' => 'Этаж',
    'Follow us' => 'Следите за нами',
    'Forgot Password' => 'Забыли пароль',
    'French' => 'Французский',
    'Georgian' => 'Грузинский',
    'German' => 'Немецкий',
    'Get a free estimate of your property' => 'Получить бесплатную оценку вашей недвижимости',
    'Get in touch' => 'Как с нами связаться',
    'Go Home' => 'Перейти на главную',
    'Greek' => 'Греческий',
    'Hebrew' => 'Иврит',
    'Hindi' => 'Хинди',
    'Home' => 'Домой',
    'Houses' => 'Дома',
    'I`m not sure' => 'Я не уверен (уверена)',
    'Income' => 'Доходы',
    'Internal Server Error.' => 'Внутренняя ошибка сервера.',
    'Italian' => 'Итальянский',
    'January' => 'Январь',
    'Japanese' => 'Японский',
    'July' => 'Июль',
    'June' => 'Июнь',
    'Kitchen Area' => 'Площадь кухни',
    'Korean' => 'Корейский',
    'Land' => 'Земля',
    'Language' => 'Язык',
    'Less' => 'Меньше',
    'List your property' => 'Разместить объявление',
    'List your request' => 'Разместить заявку',
    'Living Area' => 'Жилая площадь',
    'Log in' => 'Войти',
    'Logout' => 'Выйти',
    'March' => 'Март',
    'May' => 'Май',
    'Messages' => 'Сообщения',
    'Mobile version' => 'Мобильная версия',
    'More' => 'Больше',
    'My Accounts' => 'Мои учетные записи',
    'My Announcements' => 'Мои объявления',
    'My CRM' => 'Мои CRM',
    'My Companies' => 'Мои компании',
    'My Profiles' => 'Мои профили',
    'My Properties' => 'Мои объекты',
    'My Requests' => 'Мои заявки',
    'My profile' => 'Мой профиль',
    'New Constructions' => 'Новостройки',
    'New Password' => 'Новый пароль',
    'New!' => 'Новинка!',
    'No' => 'Нет',
    'No results found' => 'Ничего не найдено',
    'Not a member yet?' => 'Еще не зарегистрированы?',
    'Notifications' => 'Уведомления',
    'November' => 'Ноябрь',
    'October' => 'Октябрь',
    'Page not found' => 'Страница не найдена',
    'Pass the test' => 'Пройти тест',
    'Password' => 'Пароль',
    'Payment Management' => 'Управление платежами',
    'Phone' => 'Телефон',
    'Phone must contain from {0} numbers' => 'Телефон должен содержать от {0} знаков',
    'Phone must contain up to {0} numbers' => 'Телефон должен содержать не более {0} знаков',
    'Please set the amount of replenishment' => '',
    'Polish' => 'Польский',
    'Portuguese' => 'Португальский',
    'Post for free' => 'Разместить бесплатно',
    'Powered by {who}' => '',
    'Premium agent' => 'Премиум агент',
    'Price' => 'Цена',
    'Print' => 'Печать',
    'Privacy policy' => 'Политика конфиденциальности',
    'Profile Settings' => 'Настройки профиля',
    'Properties for sale' => 'Недвижимость на продажу',
    'Property Address' => 'Адрес недвижимости',
    'Property Category' => 'Категория недвижимости',
    'Property Name' => 'Название недвижимости',
    'Property Price' => 'Стоимость недвижимости',
    'Register now' => 'Зарегистрироваться сейчас',
    'Remember me' => 'Запомнить меня',
    'Rent' => 'Аренда',
    'Rental Property' => 'Аренда недвижимости',
    'Report' => 'Подборка',
    'Request details are available only for agents, who bought our premium tariff' => 'Детали заявки доступны к просмотру только агентам, у которых приобретен премиальный тариф.',
    'Requested agent page is not found' => 'Запрашиваемая страница агента не найдена',
    'Requested building page is not found' => 'Запрашиваемая страница дома не найдена',
    'Requests' => 'Заявки',
    'Reset Password' => 'Сброс пароля',
    'Rooms' => 'Комнаты',
    'Rotate image' => 'Повернуть изображение',
    'Russian' => 'Русский',
    'Sale' => 'Продажа',
    'Secondary Buildings' => 'Вторичные здания',
    'Sell' => 'Продажа',
    'Sell, Buy, Rent real estate directly' => 'Продавай, Покупай, Арендуй недвижимость напрямую',
    'September' => 'Сентябрь',
    'Settings' => 'Настройки',
    'Share on' => '',
    'Sign In' => 'Войти',
    'Sign Up' => 'Зарегистрироваться',
    'Something is wrong' => 'Что-то пошло не так',
    'Spanish' => 'Испанский',
    'Special proposition is available. Fill your contacts and we will send you details' => 'Есть спец предложение. Заполните контактные данные и мы пришлем вам информацию',
    'Special proposition is available. Selected agent will contact you:' => 'Есть спец предложение. Хотите узнать подробности? Вам ответит:',
    'Submit' => 'Отправить',
    'Support' => 'Поддержка',
    'Swedish' => 'Шведский',
    'Tariffs' => 'Тарифы',
    'Terms of service' => 'Пользовательское соглашение',
    'Terms of use' => 'Условия пользования',
    'Thai' => 'Тайский',
    'The page you are looking for was moved, removed, renamed or might never existed.' => 'Страница, которую вы ищете, была перемещена, удалена, переименована или, возможно, никогда не существовала.',
    'This email address has already been taken' => 'Этот email адрес уже кем-то занят',
    'This phone has already been taken' => 'Этот телеофн уже кем-то используется',
    'Total Area' => 'Общая площадь',
    'Turkish' => 'Турецкий',
    'Ukrainian' => 'Украинский',
    'Useful Links' => 'Полезные ссылки',
    'User with such email as from {client} already exists' => '',
    'Vietnamese' => 'Вьетнамский',
    'View My Profile' => 'Зайти в мой профиль',
    'View Tariffs' => 'Просмотр тарифов',
    'Welcome to {site}' => 'Добро пожаловать на {site}',
    'Wish List' => 'Список желаний',
    'Yes' => 'Да',
    'Your Name' => 'Ваше имя',
    'Your link' => '',
    'by' => 'от',
    'from {price}' => 'от {price}',
    'in city {city}' => 'в г.{city}',
    'it\'s fun and easy!' => 'это легко и просто!',
    'or' => 'или',
    'with privacy policy' => 'с политикой конфиденциальности',
    'with terms of service' => 'с пользовательским соглашением',
    '{area} m²' => '{area} м²',
    '{attribute} is invalid' => '{attribute} содержит ошибку',
    '{code} wallet' => '{code} кошелек',
    '{count, plural, one{# condo} other{# condos}}' => '{count, plural, one{# квартира} few{# квартиры} other{# квартир}}',
    '{count, plural, one{# room} other{# rooms}}' => '{count, plural, one{# комната} few{# комнаты} other{# комнат}}',
    '{entity} for sale' => '{entity} на продажу',
    '{entity} not found' => '{entity} не найдено',
    '{months} months' => '{months} месяцев',
    '{username} ({id} {email} {phone})' => '',
];
