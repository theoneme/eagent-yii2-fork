<?php

return [
    'Action' => 'Действие',
    'Add Company Member' => 'Добавить участника компании',
    'All Properties' => 'Все объекты',
    'Are you sure?' => 'Вы уверены?',
    'Avatar' => 'Аватар',
    'Back to companies' => 'Назад к компаниям',
    'Company Members' => 'Участники компании',
    'Create Company' => 'Создать компанию',
    'Logo' => 'Логотип',
    'My Companies' => 'Мои компании',
    'My Properties' => 'Мои объекты',
    'My Requests' => 'Мои заявки',
    'Role' => 'Роль',
    'Status' => 'Статус',
    'Title' => 'Название',
    'Username' => 'Имя пользователя',
];
