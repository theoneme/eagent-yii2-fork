<?php

return [
    '<span >{rooms, plural, one{#+ rooms} other{#+ rooms}}</span>' => '<span >{rooms, plural, one{#+ комната} few{#+ комнаты} other{#+ комнат}}</span>',
    'Action' => 'Действие',
    'Add comment for this property' => 'Добавьте комментарий к данному объекту',
    'Address' => 'Адрес',
    'Address or ZIP code' => 'Адрес или почтовый индекс',
    'Advertisement' => 'Реклама',
    'Agent name' => 'Имя агента',
    'All Buildings' => 'Все здания',
    'Any Price' => 'Любая цена',
    'Area' => 'Площадь',
    'Area {area} m²' => 'Площадь {area} м²',
    'Average price per square meter {price}' => 'Средняя цена за квадратный метр {price}',
    'Bedrooms' => 'Спальни',
    'Buy' => 'Покупка',
    'Cancel' => 'Отмена',
    'Catalog Mode' => 'Режим каталога',
    'Category' => 'Категория',
    'Cheap first' => 'Сначала дешевые',
    'Choose' => 'Выбрать',
    'Clear' => 'Очистить',
    'Close' => 'Закрыть',
    'Comment' => 'Комментарий',
    'Comment: {comment}' => 'Комментарий: {comment}',
    'Contract Price' => 'Договорная цена',
    'Create Report' => 'Сформировать подборку',
    'Default Mode' => 'Стандартный режим',
    'Description' => 'Описание',
    'Description is missing' => 'Описание отсутствует',
    'Districts' => 'Районы',
    'Districts and microdistricts' => 'Районы и микрорайоны',
    'Enter city, region or country' => 'Введите город, регион или страну',
    'Expensive first' => 'Сначала дорогие',
    'Favorite' => 'Избранное',
    'Filter' => 'Фильтр',
    'Floor' => 'Этаж',
    'From {min}' => 'От {min}',
    'I`m interested in' => 'Меня интересует',
    'Kitchen Area' => 'Площадь кухни',
    'Kitchen {area} m²' => 'Кухня {area} м²',
    'List' => 'Список',
    'List of similar properties:' => 'Список похожих объектов:',
    'Living Area' => 'Жилая площадь',
    'Location' => 'Адрес',
    'Make Report' => 'Сформировать подборку',
    'Map' => 'Карта',
    'Map Filter Mode' => 'Режим фильтра по карте',
    'Max' => 'Максимум',
    'Menu' => 'Меню',
    'Microdistricts' => 'Микрорайоны',
    'Min' => 'Минимум',
    'Minimum price per square meter {price}' => 'Минимальная цена за квадратный метр {price}',
    'More' => 'Больше',
    'More actions' => 'Другие действия',
    'Name' => 'Имя',
    'New Constructions' => 'Новостройки',
    'Newest first' => 'Сначала новые',
    'No districts were found for this region' => 'Для данного региона районы не найдены',
    'No microdistricts were found for this region' => 'Для данного региона микрорайоны не найдены',
    'Number of Balconies' => 'Количество балконов',
    'Number of Bathrooms' => 'Количество ванн',
    'Number of Bedrooms' => 'Количество спален',
    'Number of Garages' => 'Количество гаражей',
    'Number of Loggias' => 'Количество лоджий',
    'Number of Rooms' => 'Количество комнат',
    'Operation' => 'Операция',
    'Photos' => 'Фото',
    'Possible price' => 'Возможная цена',
    'Possible price: {price}' => 'Возможная цена: {price}',
    'Price' => 'Цена',
    'Price per m²' => 'Цена за м²',
    'Property Area' => 'Площадь объекта',
    'Publish date' => 'Дата размещения',
    'Recommended price for fast sale {price}' => 'Рекомендуемая цена для быстрой продажи {price}',
    'Recommended price {price}' => 'Рекомендуемая цена {price}',
    'Rent' => 'Аренда',
    'Report created {date}' => 'Подборка создана {date}',
    'Request Showcase' => 'Запросить просмотр',
    'Request a showcase' => 'Записаться на просмотр',
    'Reset' => 'Сбросить',
    'Rooms' => 'Комнаты',
    'Save' => 'Сохранить',
    'Search' => 'Поиск',
    'Search by map' => 'Поиск по карте',
    'Secondary Buildings' => 'Вторичные здания',
    'See more' => 'Подробнее',
    'Select {attribute}' => 'Выберите {attribute}',
    'Selected Properties' => 'Выбранные объекты',
    'Send Report' => 'Отправить подборку',
    'Send selected properties to agent' => 'Отправить выбранные объекты агенту',
    'Share' => 'Поделиться',
    'Show' => 'Показать',
    'Showcase' => 'Просмотр',
    'Sort Type' => 'Тип сортировки',
    'Sort by' => 'Сортировать по',
    'Specify parameters of your property, so we can make an analyze.' => 'Укажите параметры вашей квартиры, чтобы мы могли сделать анализ.',
    'Start typing and select one of the options' => 'Начните печатать и выберите один из предлагаемых вариантов',
    'Summary' => 'Итого',
    'Title' => 'Название',
    'To {max}' => 'До {max}',
    'Unknown agency' => 'Неизвестное агентство',
    'View Request' => 'Просмотреть заявку',
    'Views' => 'Просмотры',
    'We have analyzed listings in internet that match your request. {count, plural, one{# property was analyzed} other{# properties were analyzed}}' => 'Мы проанализировали объекты, размещенные в интернете и соответствующие вашему запросу. {count, plural, one{Был проанализирован # объект} few{Было проанализировано # объекта} other{Было проанализировано # объектов}}',
    'We made analyze for your property located by address {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}' => 'Мы проанализировали ваш объект, расположенный по адресу {address} с {rooms, plural, one{# комнатой} other{# комнатами}} и площадью {area, plural, one{# квадратный метр} few{# квадратных метра} other{# квадратных метров}}',
    'Year Built' => 'Год',
    '{bedrooms, plural, one{# bedroom} other{# bedrooms}}' => '{bedrooms, plural, one{# спальня} few{# спальни} other{# спален}}',
    '{count, plural, one{# object} other{# objects}}' => '{count, plural, one{# объект} few{# объекта} other{# объектов}}',
    '{count, plural, one{# property found} other{# properties found}} near {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}' => '{count, plural, one{# объект найден} few{# объекта найдено} other{# объектов найдено}} около {address} с {rooms, plural, one{# комнатой} other{# комнатами}} и площадью {area, plural, one{# квадратный метр} few{# квадратных метра} other{# квадратных метров}}',
    '{count, plural, one{# result} other{# results}}' => '{count, plural, one{# объект} few{# объекта} other{# объектов}}',
    '{flats, plural, one{# flat} other{# flats}} for rent' => '{flats, plural, one{# квартира} few{# квартиры} other{# квартир}} в аренду',
    '{flats, plural, one{# flat} other{# flats}} for sale' => '{flats, plural, one{# квартира} few{# квартиры} other{# квартир}} на продажу',
    '{quarter} quarter' => '{quarter} квартал',
    '{rooms, plural, one{# room} other{# rooms}}' => '{rooms, plural, one{# комната} few{# комнаты} other{# комнат}}',
    '{rooms, plural, one{#+ room} other{#+ rooms}}' => '{rooms, plural, one{#+ комната} few{#+ комнаты} other{#+ комнат}}',
];
