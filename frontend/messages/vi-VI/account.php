<?php

return [
    'Action' => 'Hành động',
    'Add Company Member' => 'Để thêm một người tham gia, công ty',
    'All Properties' => 'Tất cả các đối tượng',
    'Are you sure?' => 'Bạn có chắc không?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Trở lại với công ty',
    'Company Members' => 'Các thành viên của công ty',
    'Create Company' => 'Để tạo ra một công ty',
    'Logo' => 'Logo',
    'My Companies' => 'Công ty của tôi',
    'My Properties' => 'Đối tượng của tôi',
    'My Requests' => 'Các ứng dụng của tôi',
    'Role' => 'Vai trò',
    'Status' => 'Tình trạng',
    'Title' => 'Tên',
    'Username' => 'Tên',
];
