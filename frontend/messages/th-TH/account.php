<?php

return [
    'Action' => 'การกระทำ',
    'Add Company Member' => 'เพื่อเพิ่มเป็นผู้มีส่วนร่วม@item participation is optional ไปที่บริษัท',
    'All Properties' => 'วัตถุทั้งหมด',
    'Are you sure?' => 'คุณแน่ใจหรือไม่?',
    'Avatar' => 'แมปกับร่างอวตาร',
    'Back to companies' => 'กลับไปที่บริษัท',
    'Company Members' => 'สมาชิกของบริษัท',
    'Create Company' => 'เพื่อสร้างบริษัท',
    'Logo' => 'โลโก้',
    'My Companies' => 'บริษัทของผม',
    'My Properties' => 'ฉัน™à§à±à•à-à',
    'My Requests' => 'โปรแกรมของฉัน',
    'Role' => 'บทบาท',
    'Status' => 'สถานะ',
    'Title' => 'ชื่อ',
    'Username' => 'ชื่อผู้ใช้',
];
