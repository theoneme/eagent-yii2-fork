<?php

return [
    'Action' => 'L\'azione',
    'Add Company Member' => 'Aggiungi membro della società',
    'All Properties' => 'Tutti gli oggetti',
    'Are you sure?' => 'Sei sicuro?',
    'Avatar' => 'Avatar',
    'Back to companies' => 'Fa per le aziende',
    'Company Members' => 'I membri della società',
    'Create Company' => 'Creare una società',
    'Logo' => 'Logo',
    'My Companies' => 'I miei azienda',
    'My Properties' => 'I miei oggetti',
    'My Requests' => 'Le mie richieste',
    'Role' => 'Il ruolo',
    'Status' => 'Lo stato',
    'Title' => 'Il nome',
    'Username' => 'Nome utente',
];
