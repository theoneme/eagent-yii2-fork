<?php
namespace frontend\modules\instance\widgets;

use yii\bootstrap\Alert as BaseAlert;
use yii\helpers\Html;

/**
 * Class AlertB4
 * @package frontend\modules\instance\widgets
 */
class AlertB4 extends BaseAlert
{

    /**
     * Initializes the widget options.
     * This method sets the default values for various options.
     */
    protected function initOptions()
    {
        Html::addCssClass($this->options, ['alert', 'fade', 'show']);

        if ($this->closeButton !== false) {
            $this->closeButton = array_merge([
                'data-dismiss' => 'alert',
                'aria-hidden' => 'true',
                'class' => 'close',
            ], $this->closeButton);
        }
    }
}
