<?php

namespace frontend\modules\instance\widgets;

use common\helpers\Auth;
use common\services\entities\CategoryService;
use common\services\entities\UserService;
use frontend\modules\instance\models\InstanceSetting;
use Yii;
use yii\base\Widget;

/**
 * Class HeaderWidget
 * @package frontend\modules\instance\widgets
 */
class HeaderWidget extends Widget
{
    public const COLOR_MODE_WHITE = 0;
    public const COLOR_MODE_BLACK = 10;

    public const WIDTH_MODE_SHORT = 0;
    public const WIDTH_MODE_WIDE = 10;

    /**
     * @var string
     */
    public $template = 'header-widget';
    /**
     * @var array
     */
    public $instance;
    /**
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var int
     */
    public $colorMode = self::COLOR_MODE_WHITE;
    /**
     * @var int
     */
    public $widthMode = self::WIDTH_MODE_SHORT;

    /**
     * HeaderWidget constructor.
     * @param CategoryService $categoryService
     * @param UserService $userService
     * @param array $config
     */
    public function __construct(CategoryService $categoryService, UserService $userService, array $config = [])
    {
        parent::__construct($config);
        $this->_categoryService = $categoryService;
        $this->_userService = $userService;
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $categories = [];
        $settingCategories = $this->instance['settings'][InstanceSetting::SETTING_CATEGORIES]['value'] ?? [];
        if (!empty($settingCategories)) {
            $categories = $this->_categoryService->getMany(['id' => $settingCategories], ['limit' => 5]);
            $categories = $categories['items'];
        }

        $logoImg = $this->instance['settings'][InstanceSetting::SETTING_LOGO]['value'] ?? null;
        $logoText = $this->instance['settings'][InstanceSetting::SETTING_LOGO_TEXT]['value'] ?? '';
        $languages = $this->instance['settings'][InstanceSetting::SETTING_LANGUAGES]['value'] ?? [];
        $allLanguages = Yii::$app->params['supportedLocales'];
        $user = Auth::user();
        $user = $user ? $this->_userService->getOne(['id' => $user['id']]) : null;

        $availableLanguages = array_intersect_key($allLanguages, array_flip($languages));
        return $this->render($this->template, [
            'colorMode' => $this->colorMode,
            'widthMode' => $this->widthMode,
            'categories' => $categories,
            'logoImg' => $logoImg,
            'logoText' => $logoText,
            'languages' => $availableLanguages,
            'isGuest' => Yii::$app->user->isGuest,
            'user' => $user
        ]);
    }
}