<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.06.2019
 * Time: 11:37
 */

use yii\helpers\Html;

?>

<div id="city-parts" class="new-modal modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center"><?= Yii::t('instance', 'Districts and microdistricts') ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <h5>
                            <?= Yii::t('instance', 'Districts') ?>
                        </h5>
                        <div class='modal-double-column' data-role="district-container">

                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h5>
                            <?= Yii::t('instance', 'Microdistricts') ?>
                        </h5>
                        <div class="col-district" data-role="microdistrict-container">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-left">
                    <?= Html::a(Yii::t('instance', 'Choose'), '#', [
                        'data-action' => 'submit-city-parts',
                        'data-dismiss' => 'modal',
                        'class' => 'button small blue d-inline-block'
                    ]) ?>
                    <?= Html::a(Yii::t('instance', 'Clear'), '#', [
                        'data-action' => 'clear-city-parts',
                        'class' => 'button small transparent d-inline-block'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>