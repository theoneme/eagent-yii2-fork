<?php

use frontend\widgets\auth\LoginWidget;
use frontend\widgets\auth\SignupWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $user
 */

$bundle = $this->getAssetManager()->getBundle(\frontend\modules\instance\assets\IndexAsset::class);

?>

    <footer>
        <div class="wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-3 col-sm-12">
                        <h3 class="footer-title"><?= Yii::t('instance', 'About us') ?></h3>
                        <p>
                            <?= $user['translations']['description'] ?? Yii::t('instance', 'Description is missing') ?>
                        </p>
                    </div>
                    <div class="col-12 col-lg-3 col-sm-12">
                        <h3 class="footer-title">
                            <?= Yii::t('instance', 'Recent Properties For Sale') ?>
                        </h3>
                        <?php if (!empty($properties)) { ?>
                            <?php foreach ($properties as $property) { ?>
                                <div class="footer-property d-flex">
                                    <?= Html::a(Html::img($property['image']), ['/instance/property/property/view', 'slug' => $property['slug']], ['class' => 'img d-block'])?>
                                    <div class="info">
                                        <?= Html::a($property['title'], ['/instance/property/property/view', 'slug' => $property['slug']])?>
                                        <div class="address"><?= $property['address'] ?></div>
                                        <div class="price"><?= $property['price'] ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-3 col-sm-12">
                        <h3 class="footer-title"><?= Yii::t('instance', 'Contact Us') ?></h3>
                        <address>
                            <div>Company Name</div>
                            <div>4877 Spruce Drive</div>
                            <div>West Newton, PA 15089</div>
                        </address>
                        <?php if ($user['phone']) { ?>
                            <?= Html::a($user['phone'], "tel:{$user['phone']}", [
                                'class' => 'd-block'
                            ]) ?>
                        <?php } ?>
                        <?php if ($user['email']) { ?>
                            <?= Html::a($user['email'], "mailto:{$user['email']}", [
                                'class' => 'd-block'
                            ]) ?>
                        <?php } ?>
                    </div>
                    <div class="col-12 col-lg-3 col-sm-12">
                        <h3 class="footer-title"><?= Yii::t('instance', 'Useful Links') ?></h3>
                        <ul class="no-list useful-links">
                            <li class="d-block">
                                <?= Html::a(Yii::t('instance', 'All Properties'), [
                                    '/instance/property/catalog/index', 'operation' => \common\models\Property::OPERATION_SALE, 'category' => 'flats'
                                ]) ?>
                            </li>
                            <li class="d-block">
                                <?= Html::a(Yii::t('instance', 'Privacy Policy'), '#') ?>
                            </li>
                            <li class="d-block">
                                <?= Html::a(Yii::t('instance', 'Terms And Conditions'), '#') ?>
                            </li>
                            <li class="d-block">
                                <?= Html::a(Yii::t('instance', 'Compare Property'), ['/instance/compare/view']) ?>
                            </li>
                            <li class="d-block">
                                <?= Html::a(Yii::t('instance', 'Desktop version'), Url::current(['mode' => 'desktop']), [
                                    'data-method' => 'post',
                                ]) ?>
                            </li>
                            <li class="d-block">
                                <?= Html::a(Yii::t('instance', 'Mobile version'), Url::current(['mode' => 'mobile']), [
                                    'data-method' => 'post',
                                ]) ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col py-4 text-right">
                        <a href="#" id="goTop">
                            <?= Yii::t('instance', 'Go To Top') ?><i class="icon-caret-down go-top-caret"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-line">
            <div class="wrapper">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="/agency-landing"><?= Yii::t('instance','Free website creation for a real estate agency on the platform eAgent.me')?></a>
                        <div class="pay d-flex align-items-center">
                            We accept:
                            <i class="icon-paypal-logo mx-2"></i>
                            <i class="icon-visa-pay-logo mx-2"></i>
                            <i class="icon-mastercard mx-2"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?= SignupWidget::widget(['template' => '@frontend/modules/instance/widgets/auth/views/signup']) ?>
<?= LoginWidget::widget(['template' => '@frontend/modules/instance/widgets/auth/views/login']) ?>

<?php
$compareToggleUrl = Url::to(['/instance/compare/toggle']);
$script = <<<JS
    $(document.body).on('click', '#goTop', function() {
        $('html, body').animate({scrollTop: 0},1000);
        
        return false;
    });

    $(document).on('click', '[data-action="toggle-compare"]', function() {
        let self = $(this);
        $.post('{$compareToggleUrl}', 
            {'id': $(this).data('id')}, 
            function(result) {
                if(result.success === true) {
                    self.toggleClass('active');
                    if (self.hasClass('active')) {
                        self.attr('data-hint', self.data('hint-remove'));
                    } else {
                        self.attr('data-hint', self.data('hint-add'));
                    }
                }
            }
        );
        return false;
    });
JS;
$this->registerJs($script);
