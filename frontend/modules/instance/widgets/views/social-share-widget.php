<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var $url string
 * @var $showLink boolean
 */

?>

    <div class="col-12 col-sm-6 share d-flex">
        <div><?= Yii::t('instance', 'Share') ?></div>
        <?= Html::a('<i class="icon-facebook-logo"></i>',
            'https://www.facebook.com/sharer/sharer.php?u=' . $url,
            ['class' => 'px-3', 'data-hint' => Yii::t('main', 'Share on') . ' Facebook']
        ) ?>
        <?= Html::a('<i class="icon-twitter"></i>',
            'https://twitter.com/intent/tweet?' . 'url=' . $url,
            ['class' => 'px-3 social-share', 'data-hint' => Yii::t('main', 'Share on') . ' Twitter']
        ) ?>
        <?= Html::a('<i class="icon-google-plus"></i>',
            'https://plus.google.com/share?url=' . $url,
            ['class' => 'px-3 social-share', 'data-hint' => Yii::t('main', 'Share on') . ' Google+']
        ) ?>

        <!--        --><? //= Html::a('<i class="fa fa-link" aria-hidden="true"></i>',
        //            null,
        //            ['class' => 'px-3 social-share', 'data-hint' => Yii::t('main', 'Copy to Clipboard')]
        //        ) ?>
    </div>


    <!--<div class="referral-link-block text-center" --><?//= $showLink ? '' : 'style= "display:none;"'?><!-->
    <!--    <div class="your-link">--><?//= Yii::t('main', 'Your link') ?><!--</div>-->
    <!--    --><?//= Html::textInput('link', $url, ['id' => 'clipboard-link']) ?>
    <!--</div>-->

<?php $script = <<<JS
    $(document).on('click', 'a.social-share', function() {
        let ww = $(window).width();
        let wh = $(window).height();
        let params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no' + ',width=' + ww * 0.6 + ',height=' + wh * 0.7 + ',left=' + ww * 0.2 + ',top=' + wh * 0.15;
        window.open($(this).attr('href'), $(this).data('hint'), params);
        
        return false;
    });

    $(document).on('click', 'a.clipboard', function(){
        $('#clipboard-link').select();
        document.execCommand('copy');
    });
JS;

$this->registerJs($script);
