<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:16
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\CurrencyHelper;

/**
 * @var string $catalogView
 * @var array $filters
 * @var string $sort
 * @var string $address
 * @var array $params
 * @var integer $gridSize
 */

?>

<?= Html::beginForm(Url::to(['/instance/property/catalog/index']), 'get', [
    'id' => 'filter-form',
]); ?>
<?= Html::hiddenInput('sort', $sort, ['id' => 'filter-sort']) ?>
<?= Html::hiddenInput('box', $params['box'] ?? null, ['id' => 'filter-box']) ?>
<?= Html::hiddenInput('zoom', $params['zoom'] ?? null, ['id' => 'filter-zoom']) ?>
<?= Html::hiddenInput('district', $params['district'] ?? null, ['id' => 'filter-district']) ?>
<?= Html::hiddenInput('microdistrict', $params['microdistrict'] ?? null, ['id' => 'filter-microdistrict']) ?>
    <div class="d-flex flex-wrap justify-content-between">
        <div class="filter-top col-12 col-lg-8">
            <div class="row">
                <div class="col-12 col-sm-3 p-0">
                    <div class="input-group search-filter w-100">
                        <?= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
                        <?= Html::input('search', 'city-helper', $address, [
                            'class' => 'form-control input-grey',
                            'id' => 'filter-city-helper',
                            'placeholder' => Yii::t('instance', 'Enter city, region or country')
                        ]) ?>
                        <div class="input-group-append">
                            <i class="icon-search"></i>
                        </div>
                    </div>
                    <div class="position-absolute district-form text-left d-none">
                        <div class="district-inner">
                            <p class="">
                                <?= Yii::t('instance', 'Start typing and select one of the options') ?>
                            </p>
                            <div class="autocomplete-loc-suggestions">

                            </div>
                        </div>
                        <div class="district-open">
                            <a class="d-flex justify-content-between w-100" href="#" data-action="load-city-parts" data-toggle="modal"
                               data-target="#city-parts">
                                <span><?= Yii::t('instance', 'Districts and microdistricts') ?></span>
                                <i class="icon-next"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php $priceFilter = ArrayHelper::remove($filters, 'price'); ?>
                <?php if ($priceFilter) { ?>
                    <div class="filter-item col-4 col-sm-2 p-0">
                        <div id="filter-prices" class="filter-name w-100" data-action="expand-filter">
                            <div class="filter-type-name">
                                <span id="up-price"></span>
                                <span id="begin-price">
                            <?php if ($priceFilter['left']['checked'] !== null) { ?>
                                <?= CurrencyHelper::format(Yii::$app->params['app_currency'], $priceFilter['left']['checked']) ?>+
                            <?php } ?>
                        </span>
                                <span id="delimiter-price">
                            <?php if ($priceFilter['left']['checked'] === null && $priceFilter['right']['checked'] === null) { ?>
                                <?= Yii::t('instance', 'Any Price') ?>
                            <?php } ?>
                        </span>
                                <span id="end-price">
                            <?php if ($priceFilter['right']['checked'] !== null) { ?>
                                <?= CurrencyHelper::format(Yii::$app->params['app_currency'], $priceFilter['right']['checked']) ?>
                            <?php } ?>
                        </span>
                            </div>
                        </div>
                        <div class="filter-box filter-price d-none">
                            <div class="filter-price-diapason clearfix text-center">
                                <div class="row">
                                    <div class="col-5 pr-0">
                                        <?= Html::input('number', 'price[min]', $priceFilter['left']['checked'], [
                                            'placeholder' => Yii::t('instance', 'Min'),
                                            'class' => 'w-100',
                                            'id' => 'min-price-input'
                                        ]) ?>
                                    </div>
                                    <div class="col-2 p-0">
                                        —
                                    </div>
                                    <div class="col-5 pl-0">
                                        <?= Html::input('number', 'price[max]', $priceFilter['right']['checked'], [
                                            'placeholder' => Yii::t('instance', 'Max'),
                                            'class' => 'w-100',
                                            'id' => 'max-price-input'
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-column-left text-left">
                                <?php foreach ($priceFilter['left']['values'] as $value) { ?>
                                    <div class="filter-choose" data-action="set-left-price"
                                         data-value="<?= $value ?>"><?= CurrencyHelper::format(Yii::$app->params['app_currency'], $value) ?>
                                        +
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="filter-column-right text-right d-none">
                                <?php foreach ($priceFilter['right']['values'] as $value) { ?>
                                    <?php if ($value) { ?>
                                        <div class="filter-choose" data-action="set-right-price"
                                             data-value="<?= $value ?>"><?= CurrencyHelper::format(Yii::$app->params['app_currency'], $value) ?>
                                            +
                                        </div>
                                    <?php } else { ?>
                                        <div class="filter-choose" data-action="set-right-price"
                                             data-value=""><?= Yii::t('instance', 'Any Price') ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php $operationFilter = ArrayHelper::remove($filters, 'operation'); ?>
                <?php if ($operationFilter) { ?>
                    <div class="filter-item filter-select col-4 col-sm-2 p-0">
<!--                        --><?//= Html::dropDownList('operation', $roomFilter['checked']['min'], $operationFilter['values'], ['class' => 'w-100']) ?>
                        <select class='w-100' name="operation" id="operation">
                            <?php foreach ($operationFilter['values'] as $valueKey => $value) { ?>
                                <option value="<?= $valueKey ?>" <?= $valueKey == $operationFilter['checked'] ? 'selected' : '' ?>><?= $value['title'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>

                <?php $categoryFilter = ArrayHelper::remove($filters, 'category'); ?>
                <?php if ($categoryFilter) { ?>
                    <div class="filter-item filter-select col-4 col-sm-2 p-0">
                        <?= Html::dropDownList('category', $categoryFilter['checked'], \common\helpers\DataHelper::getCategoryTree('slug'), ['class' => 'w-100']) ?>
                    </div>
                <?php } ?>

                <div class="filter-item col-12 col-sm-3 p-0">
                    <div id="filter-more" class="filter-name" data-action="expand-filter">
                        <div class="filter-type-name">
                            <?= Yii::t('instance', 'More') ?>
                        </div>
                    </div>
                    <div class="filter-box filter-more d-none p-3">
                        <div class="row">
                            <?php foreach ($filters as $key => $filter) { ?>
                                <div class="col-12 col-sm-6">
                                    <?= $this->render("filter/{$filter['type']}", [
                                        'filter' => $filter,
                                        'key' => $key
                                    ]) ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <?= Html::submitInput(Yii::t('instance', 'Show'), ['class' => 'button big w-100']) ?>
                            </div>
                            <div class="col-6">
                                <?= Html::a(Yii::t('instance', 'Reset'), '#', ['class' => 'button big transparent w-100', 'data-action' => 'reset-filter']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ul class="tab-links col-lg-4 col-12 no-list catalog-view d-flex p-0 justify-content-end">
            <li class="col-lg-3 col-lg-offset-2 col-md-4 col-xs-4">
                <?= Html::a('<i class="icon-menu"></i>&nbsp;' . Yii::t('instance', 'Grid'), '#', [
                    'class' => 'text-center d-block ' . ($catalogView === 'grid' ? 'active' : ''),
                    'data-view' => 'grid',
                    'data-action' => 'change-catalog-view',
                    'data-url' => Url::current([0 => '/instance/property/catalog/index', 'box' => null, 'zoom' => null])
                ]) ?>
            </li>
            <li class="col-lg-3 col-md-4 col-xs-4">
                <?= Html::a('<i class="icon-list"></i>&nbsp;' . Yii::t('instance', 'Table'), '#', [
                    'class' => 'text-center d-block ' . ($catalogView === 'list' ? 'active' : ''),
                    'data-view' => 'list',
                    'data-action' => 'change-catalog-view',
                    'data-url' => Url::current([0 => '/instance/property/catalog/index', 'box' => null, 'zoom' => null])
                ]) ?>
            </li>
            <li class="col-lg-3 col-md-4 col-xs-4">
                <?= Html::a('<i class="icon-gps"></i>&nbsp;' . Yii::t('instance', 'Map'), '#', [
                    'class' => 'text-center d-block ' . ($catalogView === 'map' ? 'active' : ''),
                    'data-view' => 'map',
                    'data-action' => 'change-catalog-view',
//                'data-url' => Url::current(['box' => null, 'zoom' => null])
                ]) ?>
            </li>
        </ul>
    </div>

<?= Html::endForm() ?>

<?= $this->render('filter/city-part-modal') ?>

<?php $script = <<<JS
    $('body').on('click', function (e) {
        if (!$(e.target).closest(".search-filter").length) {
            $('.district-form').addClass('d-none');
        }
        
        if (!$(e.target).closest(".filters").length) {
            $('.filter-item').removeClass('open');
        }
    })
JS;
$this->registerJs($script); ?>

<?php $css = <<<css
    .district-form {
        top: 75px;
        left: 0;
        border: 1px solid #ccc;
        background-color: #fff;
        font-size: 14px;
    }

    .district-inner {
        padding: 15px 10px;
    }

    .district-open {
        padding: 5px 0;
        border-top: 1px solid #ccc;
    }

    .district-open a {
        color: #0e0e0e;
        width: 100%;
        padding: 10px;
    }

    .city-letter {
        top: 5px;
        left: 0;
        font-size: 17px;
        color: #aaa;
    }

    ul.city-list {
        padding: 0 0 0 20px;
    }
css;
$this->registerCss($css);