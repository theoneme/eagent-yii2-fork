<?php

namespace frontend\modules\instance\widgets;

use common\services\entities\UserService;
use frontend\modules\instance\models\Block;
use frontend\modules\instance\services\InstanceBlockDataService;
use yii\base\Widget;

/**
 * Class FooterWidget
 * @package frontend\modules\instance\widgets
 */
class FooterWidget extends Widget
{
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var InstanceBlockDataService
     */
    private $_instanceBlockDataService;
    /**
     * @var string
     */
    public $template = 'footer-widget';
    /**
     * @var integer
     */
    public $instance;

    /**
     * FooterWidget constructor.
     * @param UserService $userService
     * @param array $config
     */
    public function __construct(UserService $userService, InstanceBlockDataService $instanceBlockDataService, array $config = [])
    {
        parent::__construct($config);
        $this->_userService = $userService;
        $this->_instanceBlockDataService = $instanceBlockDataService;
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $user = $this->_userService->getOne(['id' => $this->instance['user_id']]);
        $properties = $this->_instanceBlockDataService->getData($this->instance, Block::TYPE_PROPERTIES);

        return $this->render($this->template, [
            'user' => $user,
            'properties' => array_slice($properties['properties'], 0, 2)
        ]);
    }
}