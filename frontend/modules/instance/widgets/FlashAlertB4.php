<?php
namespace frontend\modules\instance\widgets;

use Yii;
use common\widgets\Alert as BaseAlert;

/**
 * Class FlashAlertB4
 * @package frontend\modules\instance\widgets
 */
class FlashAlertB4 extends BaseAlert
{
    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();
        $appendClass = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        foreach ($flashes as $type => $flash) {
            if (!isset($this->alertTypes[$type])) {
                continue;
            }

            foreach ((array) $flash as $i => $message) {
                echo AlertB4::widget([
                    'body' => $message,
                    'closeButton' => $this->closeButton,
                    'options' => array_merge($this->options, [
                        'id' => $this->getId() . '-' . $type . '-' . $i,
                        'class' => $this->alertTypes[$type] . $appendClass,
                    ]),
                ]);
            }

            $session->removeFlash($type);
        }
    }
}
