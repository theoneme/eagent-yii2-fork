<?php

use frontend\models\auth\RequestResetForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var RequestResetForm $requestResetForm
 */

?>

<div class="modal fade" id="request-reset-modal" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog site-modal" role="document">
        <div class="modal-content">
            <div class="modal-header light-grey-bg text-center flex-wrap">
                <h2 class="w-100 p-0 m-0">
                    <?= Yii::t('instance', 'Reset Password') ?>
                </h2>
                <div class="grey w-100">
                    <?= Yii::t('instance', 'Enter the email you used in registration. A password reset link will be sent to your email') ?>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'request-reset-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'action' => ['/instance/auth/request-reset'],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                    ]
                ]); ?>
                <?= $form->field($requestResetForm, 'email', [
                    'inputOptions' => [
                        'placeholder' => Yii::t('instance', 'Enter email address')
                    ]
                ]) ?>
                <div class="d-flex justify-content-between align-items-center">
                    <?= Html::submitInput(Yii::t('instance', 'Submit'), ['class' => 'button small rounded']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="text-small modal-footer text-center grey flex-wrap">
                <div class="w-100">
                    <?= Html::a(Yii::t('instance', 'Back to Sign In'), '#', [
                        'data-dismiss' => 'modal',
                        'data-toggle' => 'modal',
                        'data-target' => '#login-modal'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>