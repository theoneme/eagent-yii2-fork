<?php

use frontend\models\auth\SignupForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var SignupForm $signupForm
 * @var \yii\web\View $this
 */
?>
<div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog site-modal" role="document">
        <div class="modal-content">
            <div class="modal-header light-grey-bg text-center flex-wrap">
                <h2 class="w-100 p-0 m-0"><?= Yii::t('instance', 'Create an Account') ?></h2>
                <div class="grey w-100"><?= Yii::t('instance', 'It takes few minutes') ?></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'signup-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'action' => ['/instance/auth/signup'],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                    ]
                ]); ?>
                    <?= $form->field($signupForm, 'login', [
                        'inputOptions' => [
                            'placeholder' => Yii::t('instance', 'Enter your email or phone'),
                            'class' => 'form-control'
                        ]
                    ]) ?>
                    <?= $form->field($signupForm, 'password', [
                        'inputOptions' => [
                            'placeholder' => Yii::t('instance', 'Choose a Password'),
                            'class' => 'form-control'
                        ]
                    ])->passwordInput() ?>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="text-small">
                            <?= Yii::t('instance', 'Do u have an account?') ?>
                            <?= Html::a(Yii::t('instance', 'Sign In'), '#', [
                                'data-dismiss' => 'modal',
                                'data-toggle' => 'modal',
                                'data-target' => '#login-modal'
                            ]) ?>
                        </div>
                        <?= Html::submitInput(Yii::t('instance', 'Join'), ['class' => 'button small rounded']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="text-small modal-footer text-center grey flex-wrap">
                <div class="w-100">
                    <?= Yii::t('instance', 'By clicking JOIN button you agree {privacy} and {terms}, and receive emails from {site}', [
                        'privacy' => Html::a(Yii::t('instance', 'with privacy policy'),
                            ['/privacy-policy'],
                            ['target' => '_blank']
                        ),
                        'terms' => Html::a(Yii::t('instance', 'with terms of service'),
                            ['/terms-of-service'],
                            ['target' => '_blank']
                        ),
                        'site' => Yii::$app->name
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $script = <<<JS
    let signupForm = $('#signup-form');

	$('#signup-modal').on('hidden.bs.modal', function (e) {
		signupForm[0].reset();
		signupForm.data('step', 1);
	});
JS;

$this->registerJs($script);