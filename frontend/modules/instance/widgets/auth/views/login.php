<?php

use common\models\auth\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var LoginForm $loginForm
 */

?>
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
        <div class="modal-dialog site-modal" role="document">
            <div class="modal-content">
                <div class="modal-header light-grey-bg text-center flex-wrap">
                    <h2 class="w-100 p-0 m-0">
                        <?= Yii::t('instance', 'Sign in to your account') ?>:
                    </h2>
                    <div class="grey w-100">
                        <?= Yii::t('instance', 'Welcome home!') ?>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/instance/auth/login'],
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                        ]
                    ]); ?>
                        <?= $form->field($loginForm, 'login', [
                            'inputOptions' => [
                                'placeholder' => Yii::t('instance', 'Email') . ' / ' . Yii::t('instance', 'Phone'),
                                'class' => 'form-control'
                            ]
                        ]) ?>
                        <?= $form->field($loginForm, 'password', [
                            'inputOptions' => [
                                'placeholder' => Yii::t('instance', 'Password'),
                                'class' => 'form-control'
                            ]
                        ])->passwordInput() ?>
<!--                        <div class="row py-4">-->
<!--                            <div class="col-6">-->
<!--                                --><?//= Html::a(
//                                    '<i class="icon-facebook-logo" aria-hidden="true"></i>' . Yii::t('instance', 'Facebook login'),
//                                    'https://eagent.me/auth/social?authclient=facebook',
//                                    ['class' => 'text-uppercase facebook']
//                                ) ?>
<!--                            </div>-->
<!--                            <div class="col-6">-->
<!--                                --><?//= Html::a(
//                                    '<i class="icon-google-plus" aria-hidden="true"></i>' . Yii::t('instance', 'Google login'),
//                                    'https://eagent.me/auth/social?authclient=google',
//                                    ['class' => 'text-uppercase google']
//                                ) ?>
<!--                            </div>-->
<!--                        </div>-->
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="text-small">
                                <div>
                                    <?= Yii::t('instance', 'Don\'t have an account?')?>
                                    <?= Html::a(Yii::t('instance', 'Sign Up'), '#', [
                                        'data-dismiss' => 'modal',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#signup-modal'
                                    ]) ?>
                                </div>
<!--                                <div>-->
<!--                                    --><?//= Yii::t('instance', 'Forgot your password?')?>
<!--                                    --><?//= Html::a(Yii::t('instance', 'Restore'), '#', [
//                                        'data-dismiss' => 'modal',
//                                        'data-toggle' => 'modal',
//                                        'data-target' => '#request-reset-modal'
//                                    ]) ?>
<!--                                </div>-->
                            </div>
                            <?= Html::submitInput(Yii::t('instance', 'Log in'), ['class' => 'button small rounded']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    $('#login-modal').on('hidden.bs.modal', function (e) {
		$('#login-form')[0].reset();
	});
JS;

$this->registerJs($script);