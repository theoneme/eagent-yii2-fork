<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 17.10.2018
 * Time: 18:20
 */

use frontend\models\auth\ResetPasswordForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var ResetPasswordForm $resetPasswordForm
 */

?>

    <div class="modal fade" id="reset-password-modal" tabindex="-1" role="dialog" aria-labelledby="register"
         aria-hidden="true">
        <div class="modal-dialog site-modal" role="document">
            <div class="modal-content">
                <div class="modal-header light-grey-bg text-center flex-wrap">
                    <h2 class="w-100 p-0 m-0">
                        <?= Yii::t('instance', 'Reset Password') ?>
                    </h2>
                    <div class="grey w-100">
                        <?= Yii::t('instance', 'Choose your new password') ?>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'reset-password-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/instance/auth/reset-password'],
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                        ]
                    ]); ?>
                    <?= $form->field($resetPasswordForm, 'password')->passwordInput(['placeholder' => Yii::t('instance', 'New Password')])->label(false) ?>
                    <?= $form->field($resetPasswordForm, 'confirm')->passwordInput(['placeholder' => Yii::t('instance', 'Confirm Password')])->label(false) ?>
                    <div class="d-flex justify-content-between align-items-center">
                        <?= Html::submitInput(Yii::t('instance', 'Submit'), ['class' => 'button small rounded']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    $('#reset-password-modal').modal('show');
JS;
$this->registerJs($script);

