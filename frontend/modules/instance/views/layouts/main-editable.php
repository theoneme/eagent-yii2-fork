<?php

use common\widgets\Alert;
use frontend\modules\instance\assets\EditableAsset;
use frontend\modules\instance\assets\InstanceAsset;
use frontend\modules\instance\models\InstanceSetting;
use frontend\modules\instance\widgets\FlashAlertB4;
use frontend\modules\instance\widgets\FooterWidget;
use frontend\modules\instance\widgets\HeaderWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 * @var array $currentInstance
 */

InstanceAsset::register($this);
EditableAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon"
          href="<?= Url::to([$this->params['currentInstance']['settings'][InstanceSetting::SETTING_FAVICON]['value'] ?? '/images/favicon.png'], true) ?>"
          type="image/png"/>
    <link rel="shortcut icon"
          href="<?= Url::to([$this->params['currentInstance']['settings'][InstanceSetting::SETTING_FAVICON]['value'] ?? '/images/favicon.png'], true) ?>"
          type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="edit-menu">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <?= Html::a(Yii::t('instance', 'View'), ['/instance/site/index', 'site_alias' => $this->params['currentInstance']['id']], ['class' => 'nav-link navbar-brand'])?>
                    <?= Html::a(Yii::t('instance', 'Settings'), ['/instance/manage/update', 'id' => $this->params['currentInstance']['id']], ['class' => 'nav-link navbar-brand'])?>
                </li>
            </ul>
        </div>
        <div class="mx-auto order-0">
            <?= Html::a(Yii::t('instance', 'Edit mode'), null, ['class' => 'navbar-brand mx-auto'])?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <?= Html::a(Yii::t('instance', 'Save Changes'), '#', ['class' => 'nav-link navbar-brand', 'data-action' => "save-editable-changes"])?>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="main-block">
    <?= FlashAlertB4::widget(['options' => ['class' => 'alert']])?>
    <?= HeaderWidget::widget([
        'instance' => $this->params['currentInstance']
    ]) ?>
    <div class="content">
        <?= $content ?>
    </div>
    <div class="m-footer"></div>
</div>
<?= FooterWidget::widget([
    'instance' => $this->params['currentInstance']
]) ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

