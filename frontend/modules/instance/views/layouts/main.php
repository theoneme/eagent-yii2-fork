<?php

use frontend\modules\instance\assets\InstanceAsset;
use frontend\modules\instance\models\InstanceSetting;
use frontend\modules\instance\widgets\FooterWidget;
use frontend\modules\instance\widgets\HeaderWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 */

InstanceAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon"
          href="<?= Url::to([$this->params['currentInstance']['settings'][InstanceSetting::SETTING_FAVICON]['value'] ?? '/images/favicon.png'], true) ?>"
          type="image/png"/>
    <link rel="shortcut icon"
          href="<?= Url::to([$this->params['currentInstance']['settings'][InstanceSetting::SETTING_FAVICON]['value'] ?? '/images/favicon.png'], true) ?>"
          type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="main-block">
    <?= HeaderWidget::widget([
        'instance' => $this->params['currentInstance']
    ]) ?>
    <div class="content">
        <?= $content ?>
    </div>
    <div class="m-footer"></div>
</div>
<?= FooterWidget::widget([
    'instance' => $this->params['currentInstance']
]) ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

