<?php

use frontend\modules\instance\assets\IndexAsset;
use frontend\modules\instance\models\InstanceSetting;
use yii\helpers\Html;
use yii\mail\BaseMessage;
use yii\web\View;

/**
 * @var View $this
 * @var BaseMessage $content
 */
$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php $this->head() ?>
</head>
<body bgcolor="#f6f6f6" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">
<?php $this->beginBody() ?>
    <table style="padding:30px 10px;background:#eee;width:100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td>
                    <table style="max-width:650px;min-width:320px" cellspacing="0" align="center">
                        <tbody>
                        <tr>
                            <td style="text-align:left;font-family:arial;padding-bottom:14px;font-size: 36px;color: #666;font-weight: bold;">
                                <a href="https://eagent.me" target="_blank"
                                   style="text-decoration: none;display: block;">
                                    <?= Html::img($this->params['currentInstance']['settings'][InstanceSetting::SETTING_LOGO]['value'] ?? "{$bundle->baseUrl}/images/default-logo.png", ['alt' => 'logo']) ?>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="background:#fff;border:1px solid #e4e4e4;padding:50px 30px" align="center">
                                <?= $content ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <?php if (!empty($this->params['currentInstance']['settings'][InstanceSetting::SETTING_LOGO_TEXT]['value'])) {?>
                <tr>
                    <td>
                        <table style="max-width:650px" align="center">
                            <tbody>
                            <tr>
                                <td style="color:#b4b4b4;font-size:11px;padding-top:10px;line-height:15px">
                                    © <?= $this->params['currentInstance']['settings'][InstanceSetting::SETTING_LOGO_TEXT]['value'] ?? "" ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <?php }?>
        </tbody>
    </table>

</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
