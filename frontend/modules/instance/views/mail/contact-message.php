<?php
use frontend\modules\instance\models\InstanceSetting;

/**
 * @var string $senderName
 * @var string $senderEmail
 * @var string $senderMessage
 */

?>

<table align="center" style="width: 100%">
    <tbody>
    <tr>
        <td style="color:#666;text-align:center;">
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="color:#666;font-size:20px;font-weight:bold;text-align:left">
                        <?= Yii::t('instance', 'Message from contact form', [], $locale) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <?php if (!empty($senderEmail)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('instance', 'Sender Email') ?>:</b> <?= $senderEmail ?>
                        </td>
                    </tr>
                <?php } ?>
                <?php if (!empty($senderName)) { ?>
                    <tr>
                        <td style="color:#666;font-size:18px;text-align:left">
                            <b><?= Yii::t('instance', 'Sender Name') ?>:</b> <?= $senderName ?>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <b><?= Yii::t('instance', 'Message') ?>:</b> <?= $senderMessage ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                </tr>
                </tbody>
            </table>
            <table style="margin:auto; width: 100%;" align="center">
                <tbody>
                <tr>
                    <td style="height: 10px"></td>
                </tr>
                <tr>
                    <td style="color:#666;font-size:18px;text-align:left">
                        <?= Yii::t('instance', 'Sent from "{name}" site', ['name' => $this->params['currentInstance']['settings'][InstanceSetting::SETTING_LOGO_TEXT]['value'] ?? ""]) ?>
                    </td>
                </tr>
                <tr>
                    <td style="height: 20px"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>