<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 13:42
 */

use yii\helpers\Html;

/**
 * @var array $districts
 * @var string $currentDistrict
 */

?>

<?php if (!empty($districts)) { ?>
    <ul class="no-list city-list">
        <?php foreach ($districts as $key => $district) { ?>
            <li>
                <?= Html::radio('city-part-helper', $district['slug'] === $currentDistrict, [
                    'id' => "district-{$key}",
                    'value' => $district['slug'],
                    'class' => 'radio-checkbox',
                    'data-entity' => 'district'
                ]) ?>
                <label for="<?= "district-{$key}" ?>">
                    <span class="ftgn-title"><?= $district['title'] ?></span>
                </label>
            </li>
        <?php } ?>
    </ul>
<?php } else { ?>
    <p>
        <?= Yii::t('instance', 'No districts were found for this region') ?>
    </p>
<?php } ?>
