<?php

use common\models\Property;
use common\models\Translation;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $property
 */

?>

<a href="<?= Url::to(['/instance/property/property/view', 'slug' => $property['slug']]) ?>" class="prop d-block">
    <div class="img-block w-100"
         style="background: url(<?= $property['image'] ?>);background-size: cover">
        <div class="center-block text-center">
            <button class="button small green d-inline-block"><?= Yii::t('instance', 'More') ?></button>
        </div>
    </div>
    <div class="titles">
        <h4>
            <?= $property['title'] ?>
        </h4>
        <p class="grey"><?= StringHelper::truncate($property['description'], 70) ?></p>
    </div>
    <div class="squares d-flex flex-wrap">
        <div class="col-7">
            <?= $property['category'][Translation::KEY_ADDITIONAL_TITLE] ?? $property['category'][Translation::KEY_TITLE] ?>
            <?= !empty($property['attributes']['rooms']) ? Yii::t('instance', '{rooms, plural, one{# room} other{# rooms}}', [
                'rooms' => $property['attributes']['rooms']
            ]) : '' ?>
        </div>
        <div class="col-5">
            <?= !empty($property['attributes']['property_area']) ? Yii::t('instance', '{area} m²', ['area' => $property['attributes']['property_area']]) : '' ?>
        </div>
        <div class="col-7 lft-brdr"></div>
        <div class="col-5 lft-brdr"></div>
    </div>
    <div class="options row">
        <div class="col-8">
            <div><?= $property['price'] ?></div>
            <div class="grey sticker">
                <?= $property['type'] == Property::TYPE_SALE ? Yii::t('instance', 'For Sale') : Yii::t('instance', 'For Rent') ?>
            </div>
        </div>
        <div class="col-4 nw">
            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i class="icon-heart grey"></i></div>
            <?= Html::tag('div', '<i class="icon-plus-black-symbol"></i>', [
                'class' => 'hint--top-right favorite d-inline-block' . ($property['inCompare'] ? ' active' : ''),
                'data' => [
                    'hint' => $property['inCompare'] ? Yii::t('instance', 'Remove from compare') : Yii::t('instance', 'Add to compare'),
                    'hint-add' => Yii::t('instance', 'Add to compare'),
                    'hint-remove' => Yii::t('instance', 'Remove from compare'),
                    'action' => 'toggle-compare',
                    'id' => $property['id']
                ]
            ])?>
        </div>
    </div>
</a>