<?php

use common\helpers\UtilityHelper;
use common\models\Building;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $building
 */

?>

<a href="<?= Url::to(['/instance/building/building/view', 'slug' => $building['slug']]) ?>" class="prop d-block">
    <div class="img-block w-100" style="background: url(<?= $building['image'] ?>);background-size: cover">
        <div class="center-block text-center">
            <button class="button small green d-inline-block"><?= Yii::t('instance', 'More') ?></button>
        </div>
    </div>
    <div class="titles">
        <h4>
            <?= !empty($building['name']) ? UtilityHelper::upperFirstLetter($building['name']) : $building['title'] ?>
        </h4>
        <p class="grey"><?= $building['address'] ?></p>
    </div>
    <div class="squares d-flex flex-wrap">
        <?php $count = 0;
        foreach ($building['attributes'] as $key => $attribute) {
            if ($count === 2) break; ?>
            <?php if ($key === 'new_construction_discount') {
                $count++; ?>
                <div class="col-6">
                    <?= Yii::t('instance', 'Discount {percent}%', [
                        'percent' => $building['attributes']['new_construction_discount']
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($key === 'new_construction_mortgage') {
                $count++; ?>
                <div class="col-6">
                    <?= Yii::t('instance', 'Mortgage {percent}%', [
                        'percent' => $building['attributes']['new_construction_mortgage']
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($key === 'new_construction_installment') {
                $count++; ?>
                <div class="col-6">
                    <?= $building['attributes']['new_construction_installment'] ?>
                </div>
            <?php } ?>
            <?php if ($key === 'floors') {
                $count++; ?>
                <div class="col-6">
                    <?= Yii::t('instance', '{floors, plural, one{# floor} other{# floors}}', [
                        'floors' => $building['attributes']['floors']
                    ]) ?>
                </div>
            <?php } ?>
            <?php if ($key === 'ready_year') {
                $count++; ?>
                <div class="col-6">
                    <?= Yii::t('instance', 'Ready Year {year}', [
                        'year' => $building['attributes']['ready_year']
                    ]) ?>
                </div>
            <?php } ?>
        <?php } ?>
        <div class="col-6 lft-brdr"></div>
        <div class="col-6 lft-brdr"></div>
    </div>
    <div class="options row">
        <div class="col-8">
<!--            <div>--><?//= $building['price'] ?><!--</div>-->
            <div class="grey sticker">
                <?= $building['type'] == Building::TYPE_NEW_CONSTRUCTION
                    ? Yii::t('instance', 'New Construction')
                    : Yii::t('instance', 'Secondary Building') ?>
            </div>
        </div>
        <div class="col-4 nw">
            <div class="hint--top-right favorite d-inline-block"
                 data-hint="Add to bookmark"><i
                        class="icon-heart grey"></i></div>
            <div href="#" class="hint--top-right favorite d-inline-block"
                 data-hint="Add to compare"><i class="icon-plus-black-symbol"></i></div>
        </div>
    </div>
</a>