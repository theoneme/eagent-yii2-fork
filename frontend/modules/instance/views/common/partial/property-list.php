<?php

use common\models\Property;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;

/**
 * @var View $this
 * @var array $property
 */

?>

<div class="prop list-view row">
    <div class="img-block col-12 col-md-3"
         style="background: url(<?= $property['image'] ?>);background-size: cover">
        <div class="center-block text-center">
            <?= Html::a(Yii::t('instance', 'Browse photos'), array_shift($property['images']), [
                'data-fancybox' => 'gallery',
                'data-role' => 'fancy-gallery-item',
                'class' => 'button small green d-inline-block',
            ]) ?>
            <?php foreach ($property['images'] as $image) {
                echo Html::a('', $image, [
                    'data-fancybox' => 'gallery',
                    'data-role' => 'fancy-gallery-item',
                    'class' => 'd-none',
                ]);
            } ?>
        </div>
        <div class="bottom-info">
            <?= Yii::t('instance', '{count, plural, one{# photo} other{# photos}}', ['count' => count($property['thumbnails'])]) ?>
        </div>
    </div>
    <div class="col-12 col-md-6 col-xl-7 py-2 px-3">
        <div class="list-info">
            <div class="titles">
                <h4>
                    <?= Html::a($property['title'], ['/instance/property/property/view', 'slug' => $property['slug']]) ?>
                </h4>
                <p class="grey">
                    <?= StringHelper::truncate($property['description'], 70) ?>
                </p>
            </div>
            <div class="squares d-flex flex-wrap grey p-0">
                <?php if (array_key_exists('rooms', $property['attributes'])) { ?>
                    <div class="col-6 col-sm-3">
                        <i class="icon-bed"></i>
                        <?= Yii::t('instance', '{rooms, plural, one{# room} other{# rooms}}', [
                            'rooms' => $property['attributes']['rooms']
                        ]) ?>
                    </div>
                <?php } ?>
                <?php if (array_key_exists('bathrooms', $property['attributes'])) { ?>
                    <div class="col-6 col-sm-3">
                        <i class="icon-bathtub"></i>
                        <?= Yii::t('instance', '{bathrooms, plural, one{# bathroom} other{# bathrooms}}', [
                            'bathrooms' => $property['attributes']['bathrooms']
                        ]) ?>
                    </div>
                <?php } ?>
                <?php if (array_key_exists('property_area', $property['attributes'])) { ?>
                    <div class="col-6 col-sm-3">
                        <i class="icon-dimensions"></i>
                        <?= Yii::t('instance', '{area} m²', [
                            'area' => $property['attributes']['property_area']
                        ]) ?>
                    </div>
                <?php } ?>
                <?php if (array_key_exists('garages', $property['attributes'])) { ?>
                    <div class="col-6 col-sm-3">
                        <i class="icon-parked-car"></i>
                        <?= Yii::t('instance', '{garages, plural, one{# garage} other{# garages}}', [
                            'garages' => $property['attributes']['garages']
                        ]) ?>
                    </div>
                <?php } ?>
                <div class="col-3 lft-brdr d-none d-sm-block"></div>
                <div class="col-3 lft-brdr d-none d-sm-block"></div>
                <div class="col-3 lft-brdr d-none d-sm-block"></div>
                <div class="col-3 lft-brdr d-none d-sm-block"></div>
            </div>
            <div class="description grey mt-3">
                <?= !empty($property['description'])
                    ? StringHelper::truncate($property['description'], 70)
                    : Yii::t('instance', 'Description is missing') ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3 col-lg-2 pl-0">
        <div class="options row flex-wrap align-items-center">
            <div class="col-12 col-sm-6 col-md-12">
                <div class="price text-center"><?= $property['price'] ?></div>
                <div class="grey sticker mb-3 text-left text-md-center">
                    <?= $property['type'] == Property::TYPE_SALE ? Yii::t('instance', 'For Sale') : Yii::t('instance', 'For Rent') ?>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-12 nw p-0">
                <?= Html::a(Yii::t('instance', 'Learn More'), ['/instance/property/property/view', 'slug' => $property['slug']], [
                    'class' => 'button big w-100 transparent mb-3'
                ]) ?>
                <a href="#" class="hint--top-right favorite d-inline-flex" data-hint="Add to bookmark"><i class="icon-heart grey"></i>&nbsp;<span>Fav</span></a>
                <?= Html::tag('div', '<i class="icon-plus-black-symbol"></i>', [
                    'class' => 'hint--top-right favorite d-inline-block' . ($property['inCompare'] ? ' active' : ''),
                    'data' => [
                        'hint' => $property['inCompare'] ? Yii::t('instance', 'Remove from compare') : Yii::t('instance', 'Add to compare'),
                        'hint-add' => Yii::t('instance', 'Add to compare'),
                        'hint-remove' => Yii::t('instance', 'Remove from compare'),
                        'action' => 'toggle-compare',
                        'id' => $property['id']
                    ]
                ])?>
            </div>
        </div>
    </div>
</div>