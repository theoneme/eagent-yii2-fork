<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;

/**
 * @var View $this
 * @var array $building
 */

?>

<div class="prop list-view row">
    <div class="img-block col-12 col-md-3" style="background: url(<?= $building['image'] ?>);background-size: cover">
        <div class="center-block text-center">
            <?= Html::a(Yii::t('instance', 'Browse photos'), array_shift($building['images']), [
                'data-fancybox' => 'gallery',
                'data-role' => 'fancy-gallery-item',
                'class' => 'button small green d-inline-block',
            ]) ?>
            <?php foreach ($building['images'] as $image) {
                echo Html::a('', $image, [
                    'data-fancybox' => 'gallery',
                    'data-role' => 'fancy-gallery-item',
                    'class' => 'd-none',
                ]);
            } ?>
        </div>
        <div class="bottom-info">
            <?= Yii::t('instance', '{count, plural, one{# photo} other{# photos}}', ['count' => count($building['thumbnails'])]) ?>
        </div>
    </div>
    <div class="col-12 col-md-6 col-xl-7 py-2 px-3">
        <div class="list-info">
            <div class="titles">
                <h4>
                    <?= Html::a($building['title'], ['/instance/building/building/view', 'slug' => $building['slug']]) ?>
                </h4>
                <p class="grey">
                    <?= $building['address'] ?>
                </p>
            </div>
            <div class="description grey mt-3">
                <?= !empty($building['description'])
                    ? StringHelper::truncate($building['description'], 270)
                    : Yii::t('instance', 'Description is missing') ?>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3 col-lg-2 pl-0">
        <div class="options row flex-wrap align-items-center">
            <div class="col-12 col-sm-6 col-md-12 nw p-0">
                <?= Html::a(Yii::t('instance', 'Learn More'), ['/instance/property/property/view', 'slug' => $building['slug']], [
                    'class' => 'button big w-100 transparent mb-3'
                ]) ?>
                <a href="#" class="hint--top-right favorite d-inline-flex"
                   data-hint="Add to bookmark"><i
                            class="icon-heart grey"></i>&nbsp;<span>Fav</span></a>
                <a href="#" class="hint--top-right favorite d-inline-flex"
                   data-hint="Add to compare"><i class="icon-plus-black-symbol"></i>&nbsp;<span>Comp</span></a>
            </div>
        </div>
    </div>
</div>