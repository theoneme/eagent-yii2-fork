<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.04.2019
 * Time: 14:39
 */

use frontend\assets\plugins\AutoCompleteAsset;
use frontend\assets\plugins\MoreLessAdvancedAsset;
use frontend\modules\instance\controllers\ManageController;
use frontend\modules\instance\models\InstanceSetting;
use yii\helpers\Url;

/**
 * @var \frontend\modules\instance\forms\ar\InstanceForm $instanceForm
 * @var \yii\widgets\ActiveForm $form
 * @var integer $action
 */

MoreLessAdvancedAsset::register($this);
AutoCompleteAsset::register($this);

$languages = InstanceSetting::SETTING_LANGUAGES;
$operations = InstanceSetting::SETTING_OPERATIONS;
$locations = InstanceSetting::SETTING_LOCATIONS;
$categories = InstanceSetting::SETTING_CATEGORIES;
$otherListings = InstanceSetting::SETTING_OTHER_LISTINGS;
$newConstructions = InstanceSetting::SETTING_NEW_CONSTRUCTION;
$blocks = InstanceSetting::SETTING_BLOCKS;
$contactEmail = InstanceSetting::SETTING_CONTACT_EMAIL;

$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
?>
<!--    <div class="block-with-notes type-adv"-->
<!--         data-toggle="popover" data-placement="right"-->
<!--         data-original-title="--><?//= Yii::t('instance', 'Specify site alias') ?><!--"-->
<!--         data-content="--><?//= Yii::t('instance', 'Specify site alias') ?><!--">-->
<!--        --><?//= $form->field($instanceForm, "alias")->textInput() ?>
<!--    </div>-->

    <div class="block-with-notes type-adv"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Select Languages') ?>"
         data-content="<?= Yii::t('instance', 'Select Languages') ?>">
        <?= $form->field($instanceForm->settings[$languages], "[{$languages}]value")->checkboxList($instanceForm->settings[$languages]['options'], [
            'id' => 'settings-locale',
            'item' => function ($index, $label, $name, $checked, $value) {
                $chk = $checked ? 'checked' : '';
                $output = "<div class='chover big-radio-btn'>
                <input name='{$name}' id='form-{$name}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='checkbox'>
                <label for='form-{$name}-{$index}'>{$label}</label>
            </div>";
                return $output;
            },
        ]) ?>
    </div>

    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Select Operations') ?>"
         data-content="<?= Yii::t('instance', 'Select Operations') ?>">
        <?= $form->field($instanceForm->settings[$operations], "[{$operations}]value")->checkboxList($instanceForm->settings[$operations]['options'], [
            'id' => 'settings-operations',
            'item' => function ($index, $label, $name, $checked, $value) use ($isUpdate) {
                $chk = $checked || !$isUpdate ? 'checked' : '';
                $output = "<div class='chover big-radio-btn'>
                <input name='{$name}' id='form-{$name}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='checkbox'>
                <label for='form-{$name}-{$index}'>{$label}</label>
            </div>";
                return $output;
            },
        ]) ?>
    </div>

    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Select Blocks') ?>"
         data-content="<?= Yii::t('instance', 'Select Blocks') ?>">
        <?= $form->field($instanceForm->settings[$blocks], "[{$blocks}]value")->checkboxList($instanceForm->settings[$blocks]['options'], [
            'id' => 'settings-blocks',
            'item' => function ($index, $label, $name, $checked, $value) use ($isUpdate) {
                $chk = $checked || !$isUpdate ? 'checked' : '';
                $output = "<div class='chover big-radio-btn'>
                <input name='{$name}' id='form-{$name}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='checkbox'>
                <label for='form-{$name}-{$index}'>{$label}</label>
            </div>";
                return $output;
            },
        ]) ?>
    </div>

    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Select Locations') ?>"
         data-content="<?= Yii::t('instance', 'Select Locations') ?>">
        <div class="form-group field-settings-operation">
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">
                        <?= Yii::t('instance', 'Select Locations') ?>
                    </label>
                </div>
                <div class="col-md-8">
                    <div data-role="dynamic-locations-container">
                        <?php foreach ($instanceForm->settings[$locations]['value'] as $key => $location) { ?>
                            <?= $this->render('../location', [
                                'createForm' => false,
                                'form' => $form,
                                'locationForm' => $instanceForm->settings[$locations],
                                'iterator' => $key
                            ]) ?>
                        <?php } ?>
                    </div>
                    <a href="#" data-action="add-location">
                        <i class="icon-plus-black-symbol"></i>
                        <?= Yii::t('instance', 'Add Location') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Select Categories') ?>"
         data-content="<?= Yii::t('instance', 'Select Categories') ?>">

        <?= $form->field($instanceForm->settings[$categories], "[{$categories}]value", [
            'template' => "<div class='row'>
                <div class='col-md-4'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {hint}
                    <div class='wizard-categories'>{input}</div>
                    {error}
                </div>
            </div>"
        ])->checkboxList($instanceForm->settings[$categories]['options'], [
            'id' => 'settings-categories',
            'item' => function ($index, $label, $name, $checked, $value) use($isUpdate) {
                $chk = $checked || !$isUpdate ? 'checked' : '';
                $output = "<div class='chover'>
                <input name='{$name}' id='form-{$name}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='checkbox'>
                <label for='form-{$name}-{$index}'>{$label}</label>
            </div>";
                return $output;
            },
        ]) ?>
    </div>
    <div class="block-with-notes type-adv"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Specify contact email') ?>"
         data-content="<?= Yii::t('instance', 'Specify contact email') ?>">
        <?= $form->field($instanceForm->settings[$contactEmail], "[{$contactEmail}]value")->textInput() ?>
    </div>

    <div class="block-with-notes type-adv"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Show Other Listings') ?>"
         data-content="<?= Yii::t('instance', 'Show Other Listings') ?>">
        <?= $form->field($instanceForm->settings[$otherListings], "[{$otherListings}]value")->radioList($instanceForm->settings[$otherListings]['options'], [
            'id' => 'settings-other-listings',
            'item' => function ($index, $label, $name, $checked, $value) {
                $chk = $checked ? 'checked' : '';
                $output = "<div class='chover big-radio-btn'>
                <input name='{$name}' id='form-{$name}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='radio'>
                <label for='form-{$name}-{$index}'>{$label}</label>
            </div>";
                return $output;
            },
            'value' => $instanceForm->settings[$otherListings]['value'] ?? 0
        ]) ?>
    </div>

    <div class="block-with-notes type-adv"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Show New Constructions') ?>"
         data-content="<?= Yii::t('instance', 'Show New Constructions') ?>">
        <?= $form->field($instanceForm->settings[$newConstructions], "[{$newConstructions}]value")->radioList($instanceForm->settings[$newConstructions]['options'], [
            'id' => 'settings-other-listings',
            'item' => function ($index, $label, $name, $checked, $value) {
                $chk = $checked ? 'checked' : '';
                $output = "<div class='chover big-radio-btn'>
                <input name='{$name}' id='form-{$name}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='radio'>
                <label for='form-{$name}-{$index}'>{$label}</label>
            </div>";
                return $output;
            },
            'value' => $instanceForm->settings[$newConstructions]['value'] ?? 0
        ]) ?>
    </div>

<?php $showLanguagesText = Yii::t('instance', 'Show other languages');
$hideLanguagesText = Yii::t('instance', 'Hide other languages');
$contactCount = count($instanceForm->settings[$locations]['value']);
$locationUrl = Url::to(['/instance/manage/render-location']);
$languagesCount = count($instanceForm->settings[$languages]['value']);
$script = <<<JS
    let iterator = {$contactCount};

    $('#settings-locale').MoreLessAdvanced({
        withArrows: false,
        moreText: '{$showLanguagesText}',
        lessText: '{$hideLanguagesText}',
        containerSelector: '#settings-locale',
        itemSelector: '.chover',
        itemsToShow: $languagesCount
    });
    
    $('[data-action=add-location]').on('click', function() {
        $.get('{$locationUrl}', {iterator: iterator}, function(result) {
            if(result.success === true) {
                $('[data-role=dynamic-locations-container]').append(result.html);
            } 
            iterator++;
        });
        
        return false;
    });

    $(document).on('click', '[data-action=remove-location]', function() {
        $(this).closest('[data-role=location-item]').remove(); 
        
        return false;
    });
JS;

$this->registerJs($script);