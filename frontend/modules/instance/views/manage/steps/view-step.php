<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.04.2019
 * Time: 14:39
 */

use common\helpers\FileInputHelper;
use frontend\modules\instance\models\InstanceSetting;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var \frontend\modules\instance\forms\ar\InstanceForm $instanceForm
 */

$favicon = InstanceSetting::SETTING_FAVICON;
$logo = InstanceSetting::SETTING_LOGO;
$logoText = InstanceSetting::SETTING_LOGO_TEXT;

?>
    <div class="block-with-notes type-adv"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('instance', 'Specify logo text') ?>"
         data-content="<?= Yii::t('instance', 'Specify logo text') ?>">
        <?= $form->field($instanceForm->settings[$logoText], "[{$logoText}]value")->textInput() ?>
    </div>
    <div class="form-group">
        <p>
            <?= Yii::t('instance', 'Upload logo of your site') ?>
        </p>
        <div>
            <div class="block-with-notes"
                 data-toggle="popover" data-placement="right"
                 data-original-title="<?= Yii::t('instance', 'Site Logo') ?>"
                 data-content="<?= Yii::t('instance', 'Upload Site Logo') ?>">
                <?= FileInput::widget(
                    ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                        'name' => 'uploaded_images[]',
                        'options' => [
                            'multiple' => false,
                            'data-field' => 'logo',
                            'data-role' => 'instance-file-input'
                        ],
                        'pluginOptions' => [
                            'uploadUrl' => Url::to(['/instance/image/upload']),
                            'maxImageWidth' => 256,
                            'maxImageHeight' => 256,
                            'dropZoneTitle' => Yii::t('instance', 'Drag & drop logo here'),
                            'overwriteInitial' => false,
                            'initialPreview' => !empty($instanceForm->settings[$logo]['value']) ? $instanceForm->settings[$logo]['value'] : [],
                            'initialPreviewConfig' => !empty($instanceForm->settings[$logo]['value']) ? [[
                                'caption' => basename($instanceForm->settings[$logo]['value']),
                                'url' => Url::toRoute('/image/delete'),
                                'key' => 0
                            ]] : [],
                        ]
                    ])
                ) ?>
            </div>
            <?= $form->field($instanceForm->settings[$logo], "[{$logo}]value", ['template' => '{input}'])->hiddenInput([
                'value' => FileInputHelper::buildOriginImagePath($instanceForm->settings[$logo]['value']),
                'data-key' => 'image_init_0',
                'id' => 'instanceform-setting-logo'
            ])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <p>
            <?= Yii::t('instance', 'Upload favicon of your site') ?>
        </p>
        <div>
            <div class="block-with-notes"
                 data-toggle="popover" data-placement="right"
                 data-original-title="<?= Yii::t('instance', 'Site Favicon') ?>"
                 data-content="<?= Yii::t('instance', 'Upload Site Favicon') ?>">
                <?= FileInput::widget(
                    ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                        'name' => 'uploaded_images[]',
                        'options' => [
                            'multiple' => false,
                            'data-field' => 'favicon',
                            'data-role' => 'instance-file-input'
                        ],
                        'pluginOptions' => [
                            'uploadUrl' => Url::to(['/instance/image/upload']),
                            'maxImageWidth' => 64,
                            'maxImageHeight' => 64,
                            'minImageWidth' => 16,
                            'minImageHeight' => 16,
                            'dropZoneTitle' => Yii::t('instance', 'Drag & drop logo here'),
                            'overwriteInitial' => false,
                            'initialPreview' => !empty($instanceForm->settings[$favicon]['value']) ? $instanceForm->settings[$favicon]['value'] : [],
                            'initialPreviewConfig' => !empty($instanceForm->settings[$favicon]['value']) ? [[
                                'caption' => basename($instanceForm->settings[$favicon]['value']),
                                'url' => Url::toRoute('/image/delete'),
                                'key' => 0
                            ]] : [],
                        ]
                    ])
                ) ?>
            </div>
            <?= $form->field($instanceForm->settings[$favicon], "[{$favicon}]value", ['template' => '{input}'])->hiddenInput([
                'value' => FileInputHelper::buildOriginImagePath($instanceForm->settings[$favicon]['value']),
                'data-key' => 'image_init_0',
                'id' => 'instanceform-setting-favicon'
            ])->label(false) ?>
        </div>
    </div>

<?php $script = <<<JS
let hasUploadError = false;

$("[data-role=instance-file-input]").on("fileuploaded", function(event, data) {
    let response = data.response,
        field = $(this).data('field');
    
    $('#instanceform-setting-' + field).val(response.uploadedPath).data('key', response.imageKey);
}).on("filedeleted", function() {
    let field = $(this).data('field');
    $('#instanceform-setting-' + field).val('');
}).on("filebatchuploadcomplete", function() {
    if (hasUploadError === false) {
        $(this).closest("form").submit();
    } else {
        hasUploadError = false;
    }
}).on("fileuploaderror", function(event, data) {
    hasUploadError = true;
    // $('#' + data.id).find('.kv-file-remove').click();
});
JS;

$this->registerJs($script);
