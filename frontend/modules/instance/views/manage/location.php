<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.04.2019
 * Time: 10:44
 */

use frontend\modules\instance\models\InstanceSetting;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\modules\instance\forms\ar\composite\SettingForm $locationForm
 */

$locations = InstanceSetting::SETTING_LOCATIONS;

?>

<?php if ($createForm === true) { ?>
    <?php $form = new ActiveForm([
        'id' => 'instance-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]);
    ob_end_clean(); ?>
<?php } ?>

    <div class="row" data-role="location-item">
        <div class="col-md-11 col-sm-11 col-xs-12">
            <?= $form->field($locationForm, "[{$locations}]value[{$iterator}][address]", [
                    'template' => '{label}{input}{error}'
            ])->textInput([
                'placeholder' => Yii::t('instance', 'Start typing your location'),
                'id' => "settings-locations-{$iterator}-value"
            ])->label(false) ?>
        </div>
        <div class="col-md-1 col-sm-1 col-xs-2">
            <?= Html::a('<i class="icon-close"></i>', '#', ['data-action' => 'remove-location']) ?>
        </div>

        <?= Html::activeHiddenInput($locationForm, "[{$locations}]value[{$iterator}][entity]", [
            'data-role' => 'entity-input',
        ]) ?>
        <?= Html::activeHiddenInput($locationForm, "[{$locations}]value[{$iterator}][entity_id]", [
            'data-role' => 'entityId-input'
        ]) ?>
    </div>

<?php if ($createForm === true) { ?>
    <?php $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    var attributes = $attributes;
    $.each(attributes, function() {
        $("#instance-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);
} ?>

<?php $cityUrl = Url::to(['/location/ajax/locations']);
$script = <<<JS
    new AutoComplete({
        selector: "#settings-locations-{$iterator}-value",
        autoFocus: false,
        minChars: 3,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.get('{$cityUrl}', { request: request }, function(data) { 
                response(data);
            });     
        },
        renderItem: function (item, search){ 
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-entityId="' + item.entityId + '" data-entity="' + item.entity + '" data-title="' + item.title + '" data-slug="' + item.slug + '">'
                + item.title.replace(re, "<b>$1</b>")
                + '</div>';
        },
        onSelect: function(e, term, item) {
            let entity = item.getAttribute('data-entity'),
                entityId = item.getAttribute('data-entityId'),
                address = item.getAttribute('data-title'),
                searchInput = $("#settings-locations-{$iterator}-value"),
                container = searchInput.closest('[data-role=location-item]');

            container.find('[data-role=entity-input]').val(entity);
            container.find('[data-role=entityId-input]').val(entityId);
            searchInput.val(address);
        }
    });
JS;

$this->registerJs($script);
