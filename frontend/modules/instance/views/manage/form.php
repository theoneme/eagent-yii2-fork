<?php

use frontend\assets\WizardAsset;
use frontend\modules\instance\controllers\ManageController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

WizardAsset::register($this);
\frontend\modules\instance\assets\WizardAsset::register($this);

/**
 * @var \frontend\modules\instance\forms\ar\InstanceForm $instanceForm
 * @var array $route
 * @var integer $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('instance', 'Manage your own real estate site') ?></h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($route),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div class='row'>
                <div class='col-md-4'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {hint}
                    {input}
                    {error}
                </div>
            </div>",
            ],
            'options' => [
                'id' => 'instance-form',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <div class="row wizard-row">
                <div class="col-md-9 col-sm-12">
                    <div class="wizards-steps">
                        <?php foreach ($instanceForm->steps as $key => $step) { ?>
                            <section id="step<?= $key ?>">
                                <div class="wizard-step-header">
                                    <?= $step['title'] ?>
                                    <span><?= Yii::t('instance', 'Step {first} from {total}', ['first' => $key, 'total' => count($instanceForm->steps)]) ?></span>
                                </div>
                                <div class="row step-content">
                                    <div class="col-md-8">
                                        <?php foreach ($step['config'] as $stepConfig) { ?>
                                            <?php if ($stepConfig['type'] === 'view') { ?>
                                                <?= $this->render("steps/{$stepConfig['value']}", [
                                                    'form' => $form,
                                                    'instanceForm' => $instanceForm,
                                                    'step' => $step,
                                                    'action' => $action
                                                ]) ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </section>
                        <?php } ?>
                        <div class="text-center">
                            <?= Html::a(Yii::t('instance', 'Back'), Yii::$app->request->referrer, [
                                'class' => 'btn btn-big btn-white-blue'
                            ]) ?>
                            <?php if ($action === ManageController::ACTION_CREATE) { ?>
                                <?= Html::submitInput(Yii::t('instance', 'Create Site'), [
                                    'class' => 'btn btn-big btn-blue-white'
                                ]) ?>
                            <?php } else { ?>
                                <?= Html::submitInput(Yii::t('instance', 'Save Changes'), [
                                    'class' => 'btn btn-big btn-blue-white'
                                ]) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <nav class="col-md-3 visible-lg visible-md progress-menu-nav menu-wizard-aside">
                    <div class="nav" data-spy="affix" data-offset-top="300">
                        <div class="progress-steps">
                            <div class="progress-percent text-center">
                                0
                            </div>
                            <div class="progress-info">
                                <?= Yii::t('instance', 'Configure your site') ?>
                            </div>
                        </div>
                        <ul class="no-list progress-menu">
                            <?php foreach ($instanceForm->steps as $key => $step) { ?>
                                <li>
                                    <a href="#step<?= $key ?>">
                                        <?= $step['title'] ?>
                                        <div>

                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php $canonical = Url::canonical();
$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
$script = <<<JS
let isUpdate = {$isUpdate},
    calculatorFlag = false,
    validationPerformed = false,
    progressCalculator = new ProgressCalculator({
        formSelector: '#instance-form'
    }),
    form = $('#instance-form'),
    direction = 'right';

if(isUpdate) {
    form.yiiActiveForm('validate', true);
    progressCalculator.updateProgress();
} else {
    validationPerformed = true
}

form.on('submit', function() { 
    calculatorFlag = true;
    let hasToUpload = false;
    $.each($("[data-role=instance-file-input]"), function() {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            hasToUpload = true;
            return false;
        }
    });

    if(hasToUpload === false) {
        if(validationPerformed === true) { 
             return true;
        } else {  
            validationPerformed = true;
            return false;
        }
    } else { 
        return false;
    }
}).on("afterValidateAttribute", function() {
    if(calculatorFlag === false) {
        progressCalculator.updateProgress();
    }
}).on("afterValidate", function() {
    progressCalculator.updateProgress();
    calculatorFlag = false;
});

$('.block-with-notes').popover({
    trigger: 'hover',
    placement: direction,
    html: true
});
$('body').scrollspy({
    target: '.progress-menu-nav',
    offset: 300
});

$(".progress-menu a").on('click', function(event) {
    if (this.hash !== "") {
        event.preventDefault();
        let hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800);
    }
});

JS;
$this->registerJs($script);

?>