<?php

use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\modules\instance\assets\CompareAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $config
 * @var array $data
 */
SlickAsset::register($this);
FancyBoxAsset::register($this);
CompareAsset::register($this);

?>
<div class="container-fluid light-grey-bg">
    <div class="wrapper py-5">
        <h2><?= Yii::t('instance', 'Compare Property')?></h2>
        <p><?= Yii::t('instance', 'It is easy and takes 15 min maximum')?></p>
        <div class="compare overflow-auto my-5">
            <table class="table-compare">
                <thead>
                <tr class="light-grey-bg">
                    <th class="text-center"><?= Yii::t('instance', 'Your Property')?>:</th>
                    <?php foreach ($data as $item) { ?>
                        <th>
                            <div class="w-100 image" style="background-image: url(<?= $item['image']?>);background-size: cover;"></div>
                        </th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($config as $row) {?>
                    <tr>
                        <td class="text-right"><?= $row['label']?></td>
                        <?php foreach ($data as $item) { ?>
                            <td class="text-center"><?= $item[$row['attribute']] ?? '-'?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                <tr class="buttons">
                    <td></td>
                    <?php foreach ($data as $item) {
                        echo Html::tag('td',
                            Html::a(Yii::t('instance', 'MORE DETAILS'), ['/instance/property/property/view', 'slug' => $item['slug']], ['class' => 'button small blue'])
                        );
                    } ?>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>