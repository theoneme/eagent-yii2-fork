<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:12
 */

use common\assets\GoogleAsset;
use frontend\assets\FilterAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use frontend\assets\plugins\CatalogMapAsset;
use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\modules\instance\assets\CatalogAsset;
use frontend\widgets\BuildingFilter;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $buildings
 * @var array $seo
 * @var View $this
 * @var BuildingFilter $buildingFilter
 * @var string $lat
 * @var string $lng
 */

FancyBoxAsset::register($this);
CatalogAsset::register($this);
CatalogMapAsset::register($this);
GoogleAsset::register($this);
AutoCompleteAsset::register($this);
FilterAsset::register($this);
FontAwesomeAsset::register($this);

?>
    <div class="wrapper1920">
        <div class="filter" id="filter-component">
            <?= $buildingFilter->run() ?>
        </div>
        <div class="container-fluid light-grey-bg" id="buildings-list">
            <?= $this->render('partial/list', [
                'seo' => $seo,
                'buildings' => $buildings
            ]) ?>
        </div>
    </div>

<?php $baseUrl = Url::to(['/instance/building/catalog/search']);
$catalogViewUrl = Url::to(['/instance/building/catalog/view']);
$markerListUrl = Url::to(['/building/marker/catalog']);
$markerViewUrl = Url::to(['/instance/building/marker/view']);
$markerBuildingIcon = Url::to(['/images/marker.png'], true);
$complexUrl = Url::to(['/instance/building/catalog/index-ajax']);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);

$script = <<<JS
    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };

    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    
    $(document).on('click', '#buildings-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if(result.success === true) {
                $('#buildings-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({result: true}, 'Page', result.url);
            } 
        });
        
        return false;
    });
    
    let fn = function(result) {
        if (result.domain === false) {
            // map.setMarkers(result.markers);
            $('#buildings-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $(document).on('submit', filter.options.filterSelector, function() {
         let params = filter.buildQuery();
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
         let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $('[data-fancybox="gallery"]').fancybox({
        baseClass: 'fancybox-advanced',
        hash: false,
        buttons: [
            'close'
        ],
        infobar: false,
        thumbs: false,
        mobile: {
            infobar: false,
            caption: false,
            thumbs: false
        },
        btnTpl: {
            close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg viewBox="0 0 60 60"><path d="M0,0 L60,60 M60,0 L0,60"/></svg></button>',
            arrowLeft: '<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M60,0 L30,30 L60,60"></path></svg></a>',
            arrowRight: '<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M0,0 L30,30 L0,60"></path></svg></a>'
        }
    });
JS;
$this->registerJs($script);