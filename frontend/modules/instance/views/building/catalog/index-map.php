<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:12
 */

use common\assets\GoogleAsset;
use frontend\assets\FilterAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use frontend\assets\plugins\CatalogMapAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\modules\instance\assets\CatalogAsset;
use frontend\widgets\BuildingFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $buildings
 * @var array $seo
 * @var View $this
 * @var BuildingFilter $buildingFilter
 * @var string $lat
 * @var string $lng
 * @var integer $zoom
 * @var string $box
 */

CatalogAsset::register($this);
CatalogMapAsset::register($this);
GoogleAsset::register($this);
AutoCompleteAsset::register($this);
FilterAsset::register($this);
FontAwesomeAsset::register($this);

?>

    <div class="wrapper1920">
        <div class="filter" id="filter-component">
            <?= $buildingFilter->run() ?>
        </div>
        <div class="container-fluid light-grey-bg">
            <div class="row">
                <div class="col-12 col-md-8 col-xl-7">
                    <div id="buildings-list">
                        <?= $this->render('partial/map', [
                            'seo' => $seo,
                            'buildings' => $buildings
                        ]) ?>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-xl-5 pl-0">
                    <div class="map sticky-top vh-100">
                        <?= Html::textInput('google-map-search', null, ['class' => 'form-control google-map-search d-none', 'id' => 'google-map-search']); ?>
                        <div id="map_catalog" class="w-100 h-100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $baseUrl = Url::to(['/instance/building/catalog/search']);
$catalogViewUrl = Url::to(['/instance/building/catalog/view']);
$markerListUrl = Url::to(['/building/marker/catalog']);
$markerViewUrl = Url::to(['/instance/building/marker/view']);
$complexUrl = Url::to(['/instance/building/catalog/index-ajax']);
$markerBuildingIcon = Url::to(['/images/marker.png'], true);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);
$polygon = json_encode($polygon);

$script = <<<JS
    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };
    
    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    let params = filter.buildQuery();
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        zoom: {$zoom},
        box: '{$box}',
        mode: 'advanced',
        markerViewRoutes: {
            building: '{$markerViewUrl}',
        },
        markerIcons: {
            building: '{$markerBuildingIcon}'
        },
        polygon: {$polygon}
    });
    map.init();
    map.fetchMarkers('{$markerListUrl}', params);
    
    let fn = function(result) {
        if (result.domain === false) {
            map.setMarkers(result.markers);
            map.setPolygon(result.polygon);
            $('#buildings-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $('#' + map.options.mapCanvasId).on('mapDragEnd mapZoomChanged', function() {
        filter.setBox($(this).data('box'));
        filter.setZoom($(this).data('zoom'));
    
        filter.process();
    });
    
    $(document).on('submit', filter.options.filterSelector, function() {
        let params = filter.buildQuery();
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
        let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '#buildings-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if (result.success === true) {
                map.clearMarkerCache();
                $('#buildings-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({
                    result: true
                }, 'Page', result.url);
            }
        });
    
        return false;
    })
JS;
$this->registerJs($script);