<?php

use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\modules\instance\assets\IndexAsset;
use yii\web\View;

/**
 * @var View $this
 * @var array $blocks
 */

SlickAsset::register($this);
FontAwesomeAsset::register($this);
FancyBoxAsset::register($this);
IndexAsset::register($this);

foreach ($blocks as $block) {
    echo $this->render("blocks/{$block['alias']}", ['fields' => $block['fields'], 'data' => $block['data'] ?? []]);
}