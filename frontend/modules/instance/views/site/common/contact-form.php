<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.04.2019
 * Time: 14:19
 */

use frontend\modules\instance\forms\ContactForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var ContactForm $model
 */

?>

<div class="contact">
    <?php $form = ActiveForm::begin([
        'action' => ['/instance/contact/send'],
        'id' => 'contact-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    <div class="row">
        <div class="col-12 col-md-6">
            <?= $form->field($model, 'name')->textInput([
                'placeholder' => Yii::t('instance', 'Enter your Name') . '*',
                'id' => 'contact-form-name',
            ]) ?>
        </div>
        <div class="col-12 col-md-6">
            <?= $form->field($model, 'email')->textInput([
                'placeholder' => Yii::t('instance', 'Enter your Email') . '*',
                'id' => 'contact-form-email',
            ])?>
        </div>
    </div>
    <div class="w-100">
        <?= $form->field($model, 'subject')->textInput([
            'id' => 'contact-form-subject',
        ]) ?>
    </div>
    <div class="w-100">
        <?= $form->field($model, 'message')->textarea([
            'id' => 'contact-form-message',
            'rows' => 8,
            'class' => 'form-control mb-5'
        ]) ?>
    </div>
    <div class="text-right mb-5">
        <?= Html::submitInput(Yii::t('instance', 'Submit'), ['class' => 'button big d-inline-block'])?>
    </div>
    <?php ActiveForm::end() ?>
</div>