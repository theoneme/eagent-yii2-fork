<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.04.2019
 * Time: 14:19
 */

use common\models\Property;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $categories
 * @var array $locations
 */

?>

<?= Html::beginForm(['/instance/property/catalog/index'], 'get', [
    'id' => 'banner-form',
    'class' => 'd-flex flex-wrap main-search'
]) ?>
<?= Html::dropDownList('operation', Property::OPERATION_SALE, [
    Property::OPERATION_SALE => Yii::t('instance', 'For Sale'),
    Property::OPERATION_RENT => Yii::t('instance', 'For Rent')
], [
    'class' => 'col-md-3 col-12'
]) ?>
<?= Html::dropDownList('category', null, $categories, ['class' => 'col-md-3 col-12']) ?>
<div class="input-group search flex-wrap col-md-6 col-12">
<!--    <div class="input-group-addon arrow col-md-2 col-2">-->
<!--        <i class="fa fa-location-arrow"></i>-->
<!--    </div>-->
<!--    --><?//= Html::input('search', 'city-helper', null, [
//        'class' => 'col-md-8 col-10',
//        'id' => 'banner-filter-city-helper',
//        'placeholder' => Yii::t('iStatic', 'Enter city, region or country')
//    ]) ?>
    <?= Html::dropDownList('city-helper', null, $locations, ['id' => 'banner-filter-city-helper', 'class' => 'col-md-10 col-12']) ?>
    <div class="input-group-addon btn-submit col-md-2 col-12">
        <?= Html::submitInput(Yii::t('instance', 'Search'), ['class' => 'button blue']) ?>
    </div>
</div>
<?= Html::endForm() ?>
