<?php

use common\helpers\FileInputHelper;
use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\modules\instance\assets\IndexAsset;
use kartik\file\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $blocks
 */

SlickAsset::register($this);
FontAwesomeAsset::register($this);
FancyBoxAsset::register($this);
IndexAsset::register($this);

foreach ($blocks as $block) {
    echo $this->render("blocks-editable/{$block['alias']}", [
        'fields' => $block['fields'],
        'data' => $block['data'],
        'blockId' => $block['id']
    ]);
}

Modal::begin([
//    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'file-input-modal',
    'size' => 'modal-lg',
    'header' => null,
    'closeButton' => false,
]); ?>
    <div class='modalContent'>
        <?= FileInput::widget(
            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                'id' => 'file-upload-input',
                'name' => 'uploaded_images',
                'options' => ['multiple' => false],
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['/instance/image/upload']),
                    'dropZoneTitle' => Yii::t('instance', 'Drag & drop photo here'),
                    'overwriteInitial' => true,
                    'showCaption' => false,
                    'showBrowse' => false,
                    'showClose' => false,
                    'browseOnZoneClick' => true,
                    'otherActionButtons' => '',
                    'layoutTemplates' => [
                        'preview' => '
                            <div class="file-preview {class}">
                                {close}
                                <div class="{dropClass}">
                                    <div class="file-input-button-wrapper"> 
                                        <div class="file-input-button">' . Yii::t('instance', 'Upload file') . '</div> 
                                    </div>
                                    <div class="file-preview-thumbnails">
                                    
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="file-preview-status text-center text-success"></div>
                                    <div class="kv-fileinput-error"></div>
                                </div>
                            </div>
                        '
                    ]
                ]
            ])
        ) ?>
    </div>
<?php Modal::end();

$css = <<<css
    .main-block {
        margin-top: 45px;
    }
    .desktop-menu {
        top: 45px;
    }
    [contenteditable]:focus {
        background-color: rgba(200, 200, 200, 0.3);
    }
    [data-img-editable="1"] {
        cursor: pointer;
    }
css;
$this->registerCss($css);

$saveUrl = Url::to(['/instance/ajax/content', 'site_alias' => $this->params['currentInstance']['id']]);
$imageDeleteRoute = Url::toRoute('/image/delete');
$script = <<<JS
    let fileInput = $("#file-upload-input");
    let fileFieldAlias = null;
    
    $('[data-action=save-editable-changes]').on('click', function() {
        let inputs = $('[data-editable=1][data-changed=1]'),   
            data = {fields: []};
        let imgInputs = $('[data-img-editable=1][data-changed=1]');
        
        $.each(inputs, function() {
            data.fields.push({
                value: $(this).text(),
                alias: $(this).data('alias'),
                block_id: $(this).closest('section').data('block'),
                type: $(this).data('type')
            });
        });
        $.each(imgInputs, function() {
            if ($(this).data('img').length) {
                data.fields.push({
                    value: $(this).data('img'),
                    alias: $(this).data('alias'),
                    block_id: $(this).closest('section').data('block'),
                    type: $(this).data('type')
                });
            }
        });
        
        if(data.fields.length > 0) {
            $.post('{$saveUrl}', data, function(result) {
                if(result.success === true) {
                    alert('saved');
                }
            });
        }
        
        return false;
    });
    
    fileInput.on('fileselect', function() {
        $(this).fileinput("upload");
    }).on('fileuploaded', function(event, data) {
        let response = data.response;
        let field = $('[data-alias=' + fileFieldAlias + ']');
        field.attr('data-changed', 1);
        if (field.is('img')) {
            field.data('img', response.uploadedPath).attr('src', response.uploadedPath);
        } else {
            field.data('img', response.uploadedPath).attr('style', 'background: url(' + response.uploadedPath + ');background-size: cover');
        }
    }).on("filebatchuploadcomplete", function() {
        $('#file-input-modal').modal('hide');
    });
    
    $(document).on('focus', '[contenteditable]', function() {
        const el = $(this);
        el.data('before', el.html());
    }).on('blur keyup paste input', '[contenteditable]', function() {
        const el = $(this);
        if (el.data('before') !== el.html()) {
            el.data('before', el.html());
            el.attr('data-changed', 1);
        }
    });
    
    $(document).on('click', '[data-img-editable=1]', function(e) {
        if (!$(e.target).is('[data-editable=1],input,select,option')) {
            fileFieldAlias = $(this).data('alias');
            openFileInput();
            return false;    
        }
    });
    
    $(document).on('click', 'header a.logo', function(e) {
        return false;
    });
    
    function openFileInput() {
        let inputId = fileInput.data('krajee-fileinput');
        let opts = window[inputId];
        opts = $.extend(true, {}, opts, {
            initialPreviewThumbTags: {
                '{actions}': '{actions}'
            },
            imageKey: 'image_new_' + fileFieldAlias
        });
        fileInput.fileinput('destroy');
        fileInput.fileinput(opts);
        $('#file-input-modal').modal('show');
    }
JS;

$this->registerJs($script);