<?php

use frontend\modules\instance\assets\IndexAsset;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);
?>

<section class="banner overflow-hidden" style="background: url(<?= ($fields['banner-img'] ?? Url::to(["{$bundle->baseUrl}/images/header_image.jpg"])) ?>);background-size: cover">
    <div class="wrapper">
        <div class="center-block text-center">
            <h1><?= $fields['title'] ?? Yii::t('instance', 'Buy property in Spain') ?></h1>
            <h4><?= $fields['subtitle'] ?? Yii::t('instance', 'Our agency will help you find the best offers for apartments and houses in Spain.') ?></h4>

            <?= $this->render('../common/banner-form', ['categories' => $data['categories'], 'locations' => $data['locations']]) ?>
        </div>
    </div>
</section>