<?php

use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 */

?>

<section class="light-grey-bg" id="contact-block">
    <div class="map" id="gmap">

    </div>
    <div class="container-fluid">
        <h2 class="text-center"><?= $fields['title'] ?? Yii::t('instance', 'We are working for you') ?></h2>
        <h6 class="text-center grey"><?= $fields['subtitle'] ?? Yii::t('instance', 'If you want to buy, rent or sell property, please contact us and we will be glad to provide you with professional services in real estate.') ?></h6>
        <?= $this->render('../common/contact-form', ['model' => $data['contactForm']]) ?>
    </div>
</section>