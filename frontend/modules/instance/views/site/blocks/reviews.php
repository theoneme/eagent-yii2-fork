<?php

use frontend\modules\instance\assets\IndexAsset;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);

?>

    <section class="light-grey-bg reviews">
        <div class="wrapper">
            <div class="container-fluid">
                <h2 class="text-center">
                    <?= $fields['title'] ?? Yii::t('instance', 'Reviews') ?>
                </h2>
                <div class="slider-review">
                    <blockquote class="review-cite">
                        <cite class="grey text-center d-block">
                            <?= $fields['review-1-text'] ?? Yii::t('instance', 'Having read reviews on other sites, went cautiously to the agency. Although I know that a lot of reviews are also customized. "I will not believe until I check." It\'s about me. Our deal (real estate purchase) was led by Svetlana Topoleva. She really has a high level of professionalism, up to date. He is able to find an approach to the client, during the connection to the negotiation process (as we did). To all my fears, approached with understanding. If you need help, I will definitely recommend.') ?>
                        </cite>
                        <div class="text-center">
                            <div class="d-inline-flex team">
                                <div class="img">
                                    <?= Html::img([$fields['review-1-img'] ?? "{$bundle->baseUrl}/images/sara-1.png"]) ?>
                                </div>
                                <div class="text text-left">
                                    <div class="name">
                                        <?= $fields['review-1-name'] ?? 'Sara Strawberry' ?>
                                    </div>
                                    <div class="company grey">
                                        <?= $fields['review-1-who'] ?? Yii::t('instance', 'Client') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </blockquote>
                    <blockquote class="review-cite">
                        <cite class="grey text-center d-block">
                            <?= $fields['review-2-text'] ?? Yii::t('instance', 'Good day! Thanks to your agency and specialists Svetlana Topoleva and Ekaterina Vorobyeva, we acquired the property we dreamed of. In a short time, they managed to understand our wishes, solve related problems and find exactly what we wanted. Thank you very much for your cooperation!') ?>
                        </cite>
                        <div class="text-center">
                            <div class="d-inline-flex team">
                                <div class="img">
                                    <?= Html::img([$fields['review-2-img'] ?? "{$bundle->baseUrl}/images/sara-2.png"]) ?>
                                </div>
                                <div class="text text-left">
                                    <div class="name">
                                        <?= $fields['review-2-name'] ?? 'Sara Strawberry' ?>
                                    </div>
                                    <div class="company grey">
                                        <?= $fields['review-2-who'] ?? Yii::t('instance', 'Client') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </blockquote>
                    <blockquote class="review-cite">
                        <cite class="grey text-center d-block">
                            <?= $fields['review-3-text'] ?? Yii::t('instance', 'Probably, there are no such words as to express my gratitude to Artem Kovalenko! Incredible professional, but most importantly - extraordinary kindness people. Thanks for the help of cooperation!') ?>
                        </cite>
                        <div class="text-center">
                            <div class="d-inline-flex team">
                                <div class="img">
                                    <?= Html::img([$fields['review-3-img'] ?? "{$bundle->baseUrl}/images/sara-4.png"]) ?>
                                </div>
                                <div class="text text-left">
                                    <div class="name">
                                        <?= $fields['review-3-name'] ?? 'Sara Strawberry' ?>
                                    </div>
                                    <div class="company grey">
                                        <?= $fields['review-3-who'] ?? Yii::t('instance', 'Client') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </blockquote>
                </div>
            </div>
        </div>
    </section>

<?php
$script = <<<JS
    $(".slider-review").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true
    });
JS;

$this->registerJs($script);
