<?php

use common\models\Property;
use frontend\modules\instance\assets\IndexAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);
?>

<section class="about-us">
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-8">
                    <h2><?= $fields['title'] ?? Yii::t('instance', 'The advantages of working with our Real Estate Agency {name}', ['name' => '']) ?></h2>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= $fields['subtitle1'] ?? Yii::t('instance', 'Local market knowledge') ?>
                                </h4>
                                <p>
                                    <?= $fields['content1'] ?? Yii::t('instance', 'We are engaged in real estate for 10 years. We are well versed in local legislation and will help you arrange a property.') ?>
                                </p>
                            </div>

                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= $fields['subtitle2'] ?? Yii::t('instance', 'Experienced agents') ?>
                                </h4>
                                <p>
                                    <?= $fields['content2'] ?? Yii::t('instance', 'Our company employs 15 experienced agents who are licensed to work with real estate in Spain.') ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= $fields['subtitle3'] ?? Yii::t('instance', 'Legal support') ?>
                                </h4>
                                <p>
                                    <?= $fields['content3'] ?? Yii::t('instance', 'Our company employs lawyers who will help you arrange real estate in accordance with all local laws and will help verify property rights and protect against risks.') ?>
                                </p>
                            </div>
                            <div class="about-block">
                                <p>
                                    <?= $fields['content4'] ?? Yii::t('instance', 'We will help you choose and arrange a property. And do it with professionalism and love.') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Html::tag('div', '', [
                    'class' => 'd-none d-md-block col-md-4 about-us-img',
                    'style' => "background: url(" . ($fields['about-img'] ?? Url::to(["{$bundle->baseUrl}/images/about_us.jpg"])) . ");background-size: cover",
                ])?>
            </div>
            <div class="about-block">
                <?= Html::a(
                    Yii::t('instance', 'Explore Property'),
                    ['/instance/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_SALE],
                    ['class' => "button big white-blue"]
                )?>
            </div>
        </div>
    </div>
</section>