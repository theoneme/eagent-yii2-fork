<?php

use frontend\modules\instance\assets\IndexAsset;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);

?>

    <section class="our-team">
        <div class="wrapper">
            <div class="container-fluid">
                <h2 class="text-center">
                    <?= $fields['title'] ?? Yii::t('instance', 'Our team. Real Estate Agency {name}', ['name' => '']) ?>
                </h2>
                <h5 class="text-center grey">
                    <?= $fields['subtitle'] ?? Yii::t('instance', 'Our company employs professionals who will help not only to find the best real estate offers, but also to issue a deal.') ?>
                </h5>
            </div>
        </div>
        <div class="wrapper1920">
            <div class="slider-pro">
                <div class="pro">
                    <div class="img"
                         style="background: url(<?= Url::to([$fields['human-1-img'] ?? "{$bundle->baseUrl}/images/sara-1.png"]) ?>);background-size: cover"></div>
                    <div class="pro-info  w-100">
                        <div class="row">
                            <div class="col-7">
                                <h3><?= $fields['human-1-name'] ?? 'Sara Strawberry' ?></h3>
                                <h6><?= $fields['human-1-specialty'] ?? Yii::t('instance', 'Manager') ?></h6>
                            </div>
                            <div class="col-5 deals">

                            </div>
                        </div>
                        <div class="text">
                            <p>
                                <?= $fields['human-1-description'] ?? Yii::t('instance', 'Team member description') ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pro">
                    <div class="img"
                         style="background: url(<?= $fields['human-2-img'] ?? Url::to(["{$bundle->baseUrl}/images/sara-2.png"]) ?>);background-size: cover"></div>
                    <div class="pro-info  w-100">
                        <div class="row">
                            <div class="col-7">
                                <h3><?= $fields['human-2-name'] ?? 'Sara Strawberry' ?></h3>
                                <h6><?= $fields['human-2-specialty'] ?? Yii::t('instance', 'Manager') ?></h6>
                            </div>
                            <div class="col-5 deals">

                            </div>
                        </div>
                        <div class="text">
                            <p>
                                <?= $fields['human-2-description'] ?? Yii::t('instance', 'Team member description') ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pro">
                    <div class="img"
                         style="background: url(<?= Url::to([$fields['human-3-img'] ?? "{$bundle->baseUrl}/images/sara-4.png"]) ?>);background-size: cover"></div>
                    <div class="pro-info  w-100">
                        <div class="row">
                            <div class="col-7">
                                <h3><?= $fields['human-3-name'] ?? 'Sara Strawberry' ?></h3>
                                <h6><?= $fields['human-3-specialty'] ?? Yii::t('instance', 'Manager') ?></h6>
                            </div>
                            <div class="col-5 deals">

                            </div>
                        </div>
                        <div class="text">
                            <p>
                                <?= $fields['human-3-description'] ?? Yii::t('instance', 'Team member description') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="text-center">-->
<!--                <a href="#" class="button transparent big d-inline-block">--><?//= Yii::t('instance', 'More about us') ?><!--</a>-->
<!--            </div>-->
        </div>
    </section>

<?php
$script = <<<JS
    $(".slider-pro").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        arrows: false,
        infinite: false,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }]
    });
JS;

$this->registerJs($script);