<?php

use common\models\Building;
use frontend\modules\instance\assets\IndexAsset;
use frontend\modules\instance\models\InstanceBlockField;
use frontend\modules\instance\models\InstanceSetting;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 */

?>
<section class="explore light-grey-bg">
    <div class="wrapper">
        <div class="container-fluid">
            <h2 class="text-center"><?= $fields['title'] ?? Yii::t('instance', 'We are offering property in Spain') ?></h2>
            <h5 class="text-center">
                <i class="icon-location-pointer"></i>
                <?= $data['address']?>
<!--                <span class="grey-color">Change location</span>-->
            </h5>
            <div class="row">
                <?php foreach ($data['categories'] as $i => $category) {
                    echo $this->render('../partial/category', ['category' => $category, 'fields' => $fields, 'index' => $i + 1, 'editable' => false]);
                } ?>
                <?php if ($this->params['currentInstance']['settings'][InstanceSetting::SETTING_NEW_CONSTRUCTION] ?? false) {
                    $bundle = $this->getAssetManager()->getBundle(IndexAsset::class);
                    $img = Url::to([$fields["explore-new-construction-img"] ?? "{$bundle->baseUrl}/images/new-construction.jpg"]);
                    ?>
                    <div class="col-12 col-md-8 explore-prop-block">
                        <?= Html::a(
                            $this->render('../partial/category-link-content', ['category' => ['title' => Yii::t('instance', 'New Constructions')], 'editable' => false]),
                            ['/instance/building/catalog/index', 'operation' => Building::TYPE_NEW_CONSTRUCTION_TEXT],
                            [
                                'class' => 'explore-prop d-block',
                                'style' => "background:url({$img});background-size: cover",
                            ]
                        )?>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
</section>