<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 25.04.2019
 * Time: 15:07
 */

use common\models\Property;
use frontend\modules\instance\assets\IndexAsset;
use frontend\modules\instance\models\InstanceBlockField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $category
 * @var array $fields
 * @var integer $index
 * @var boolean $editable
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);

$img = Url::to([$fields["explore-category-{$category['id']}-img"] ?? "{$bundle->baseUrl}/images/home_{$index}.jpg"]);
$linkOptions = [
    'class' => 'explore-prop d-block',
    'style' => "background:url({$img});background-size: cover",
];
if ($editable) {
    $linkOptions = array_merge($linkOptions, [
        'data' => [
            'img' => $fields["explore-category-{$category['id']}-img"] ?? null,
            'img-editable' => 1,
            'changed' => 0,
            'alias' => "explore-category-{$category['id']}-img",
            'type' => InstanceBlockField::TYPE_FILE
        ],
    ]);
}
?>

<div class="col-12 col-md-<?= $index === 5 ? 8 : 4?> explore-prop-block">
    <?= Html::a(
        $this->render('category-link-content', ['category' => $category, 'editable' => $editable]),
        ['/instance/property/catalog/index', 'category' => $category['slug'], 'operation' => Property::OPERATION_SALE],
        $linkOptions
    )?>
</div>
