<?php

use yii\web\View;

/**
 * @var View $this
 * @var array $category
 * @var boolean $editable
 */

?>

<div class='explore-info text-center'>
    <h3><?= $category['title']?></h3>
<!--    <h6>33 Properties</h6>-->
    <?php if (!$editable) { ?>
        <div class='button small orange d-inline-block'><?= Yii::t('instance', 'explore more') ?></div>
    <?php } ?>
</div>