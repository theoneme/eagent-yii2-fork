<?php

use common\models\Property;
use common\models\Translation;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $property
 */

?>

<div class="col-12 col-sm-6 col-md-3">
    <div class="prop">
        <div class="img-block w-100" style="background: url(<?= $property['image']?>);background-size: cover">
            <!--<div class="img">
                <img src="../images/condo1.jpg" alt="">
            </div>-->
            <div class="center-block text-center">
                <?= Html::a(Yii::t('instance', 'Browse photos'), array_shift($property['images']), [
                    'data-fancybox' => 'gallery',
                    'data-role' => 'fancy-gallery-item',
                    'class' => 'button small green d-inline-block',
                ]) ?>
                <?php foreach ($property['images'] as $image) {
                    echo Html::a('', $image, [
                        'data-fancybox' => 'gallery',
                        'data-role' => 'fancy-gallery-item',
                        'class' => 'd-none',
                    ]);
                }?>
            </div>
            <div class="bottom-info">
                <?= Yii::t('instance', '{count, plural, one{# photo} other{# photos}}', ['count' => count($property['thumbnails'])])?>
            </div>
        </div>
        <div class="titles">
            <h4>
                <?= Html::a($property['title'], ['/property/property/show', 'slug' => $property['slug']])?>
            </h4>
            <p class="grey"><?= $property['address']?></p>
        </div>
        <div class="squares d-flex flex-wrap">
            <div class="col-7">
                <?= $property['category'][Translation::KEY_ADDITIONAL_TITLE] ?? $property['category'][Translation::KEY_TITLE] ?>
                <?= !empty($property['attributes']['bedrooms']) ? Yii::t('instance', '- {bedrooms} bd', ['bedrooms' => $property['attributes']['bedrooms']]) : '' ?>
            </div>
            <div class="col-5">
                <?= !empty($property['attributes']['area']) ? Yii::t('instance', '{area} m²', ['area' => $property['attributes']['area']]) : '' ?>
            </div>
            <div class="col-7 lft-brdr"></div>
            <div class="col-5 lft-brdr"></div>
        </div>
        <div class="options row">
            <div class="col-8">
                <div><?= $property['price']?></div>
                <div class="grey sticker">
                    <?= $property['type'] == Property::TYPE_SALE ? Yii::t('instance', 'For Sale') : Yii::t('instance', 'For Rent')?>
                </div>
            </div>
            <div class="col-4 nw">
                <a href="#" class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i class="icon-heart grey"></i></a>
                <?= Html::a('<i class="icon-plus-black-symbol grey"></i>', '#', [
                    'class' => 'hint--top-right favorite d-inline-block' . ($property['inCompare'] ? ' active' : ''),
                    'data' => [
                        'hint' => $property['inCompare'] ? Yii::t('instance', 'Remove from compare') : Yii::t('instance', 'Add to compare'),
                        'hint-add' => Yii::t('instance', 'Add to compare'),
                        'hint-remove' => Yii::t('instance', 'Remove from compare'),
                        'action' => 'toggle-compare',
                        'id' => $property['id']
                    ]
                ])?>
            </div>
        </div>
    </div>
</div>