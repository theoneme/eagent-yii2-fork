<?php

use common\models\Property;
use frontend\modules\instance\assets\IndexAsset;
use frontend\modules\instance\models\InstanceBlockField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 * @var integer $blockId
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);
?>

<section class="about-us" data-block="<?= $blockId ?>">
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-8">
                    <?= Html::tag('h2', $fields['title'] ?? Yii::t('instance', 'The advantages of working with our Real Estate Agency {name}', ['name' => '']), [
                        'contenteditable' => true,
                        'data' => [
                            'editable' => 1,
                            'changed' => 0,
                            'alias' => 'title',
                            'type' => InstanceBlockField::TYPE_STRING
                        ],
                    ]) ?>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= Html::tag('span', $fields['subtitle1'] ?? Yii::t('instance', 'Local market knowledge'), [
                                        'contenteditable' => true,
                                        'data' => [
                                            'editable' => 1,
                                            'changed' => 0,
                                            'alias' => 'subtitle1',
                                            'type' => InstanceBlockField::TYPE_STRING
                                        ],
                                    ]) ?>
                                </h4>
                                <?= Html::tag('p', $fields['content1'] ?? Yii::t('instance', 'We are engaged in real estate for 10 years. We are well versed in local legislation and will help you arrange a property.'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'content1',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                ]) ?>
                            </div>

                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= Html::tag('span', $fields['subtitle2'] ?? Yii::t('instance', 'Experienced agents'), [
                                        'contenteditable' => true,
                                        'data' => [
                                            'editable' => 1,
                                            'changed' => 0,
                                            'alias' => 'subtitle2',
                                            'type' => InstanceBlockField::TYPE_STRING
                                        ],
                                    ]) ?>
                                </h4>
                                <?= Html::tag('p', $fields['content2'] ?? Yii::t('instance', 'Our company employs 15 experienced agents who are licensed to work with real estate in Spain.'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'content2',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                ]) ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="about-block">
                                <h4>
                                    &#9900;
                                    <?= Html::tag('span', $fields['subtitle3'] ?? Yii::t('instance', 'Legal support'), [
                                        'contenteditable' => true,
                                        'data' => [
                                            'editable' => 1,
                                            'changed' => 0,
                                            'alias' => 'subtitle3',
                                            'type' => InstanceBlockField::TYPE_STRING
                                        ],
                                    ]) ?>
                                </h4>
                                <?= Html::tag('p', $fields['content3'] ?? Yii::t('instance', 'Our company employs lawyers who will help you arrange real estate in accordance with all local laws and will help verify property rights and protect against risks.'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'content3',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                ]) ?>
                            </div>
                            <div class="about-block">
                                <?= Html::tag('p', $fields['content4'] ?? Yii::t('instance', 'We will help you choose and arrange a property. And do it with professionalism and love.'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'content4',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= Html::tag('div', '', [
                    'class' => 'd-none d-md-block col-md-4 about-us-img',
                    'style' => "background: url(" . ($fields['about-img'] ?? Url::to(["{$bundle->baseUrl}/images/about_us.jpg"])) . ");background-size: cover",
                    'data' => [
                        'img' => $fields['about-img'] ?? null,
                        'img-editable' => 1,
                        'changed' => 0,
                        'alias' => 'about-img',
                        'type' => InstanceBlockField::TYPE_FILE
                    ],
                ])?>
            </div>
            <div class="about-block">
                <?= Html::a(
                    Yii::t('instance', 'Explore Property'),
                    ['/instance/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_SALE],
                    ['class' => "button big white-blue"]
                )?>
            </div>
        </div>
    </div>
</section>