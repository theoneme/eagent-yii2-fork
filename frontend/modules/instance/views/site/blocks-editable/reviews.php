<?php

use yii\web\View;
use yii\helpers\Html;
use frontend\modules\instance\models\InstanceBlockField;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 * @var integer $blockId
 */

$bundle = $this->getAssetManager()->getBundle(\frontend\modules\instance\assets\IndexAsset::class);

?>

<section class="light-grey-bg reviews" data-block="<?= $blockId ?>">
    <div class="wrapper">
        <div class="container-fluid">
            <?= Html::tag('h2', $fields['title'] ?? Yii::t('instance', 'Reviews'), [
                'contenteditable' => true,
                'data' => [
                    'editable' => 1,
                    'changed' => 0,
                    'alias' => 'title',
                    'type' => InstanceBlockField::TYPE_STRING
                ],
                'class' => 'text-center'
            ]) ?>
            <div class="slider-review">
                <blockquote class="review-cite">
                    <?= Html::tag('cite', $fields['review-1-text'] ?? Yii::t('instance', 'Having read reviews on other sites, went cautiously to the agency. Although I know that a lot of reviews are also customized. "I will not believe until I check." It\'s about me. Our deal (real estate purchase) was led by Svetlana Topoleva. She really has a high level of professionalism, up to date. He is able to find an approach to the client, during the connection to the negotiation process (as we did). To all my fears, approached with understanding. If you need help, I will definitely recommend.'), [
                        'contenteditable' => true,
                        'data' => [
                            'editable' => 1,
                            'changed' => 0,
                            'alias' => 'review-1-text',
                            'type' => InstanceBlockField::TYPE_STRING
                        ],
                        'class' => 'grey text-center d-block'
                    ]) ?>
                    <div class="text-center">
                        <div class="d-inline-flex team">
                            <div class="img">
                                <?= Html::img([$fields['review-1-img'] ?? "{$bundle->baseUrl}/images/sara-1.png"], [
                                    'data' => [
                                        'img' => $fields['review-1-img'] ?? null,
                                        'img-editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-1-img',
                                        'type' => InstanceBlockField::TYPE_FILE
                                    ],
                                ]) ?>
                            </div>
                            <div class="text text-left">
                                <?= Html::tag('div', $fields['review-1-name'] ?? 'Sara Strawberry', [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-1-name',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                    'class' => 'name'
                                ]) ?>
                                <?= Html::tag('div', $fields['review-1-who'] ?? Yii::t('instance', 'Client'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-1-who',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                    'class' => 'company grey'
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </blockquote>
                <blockquote class="review-cite">
                    <?= Html::tag('cite', $fields['review-2-text'] ?? Yii::t('instance', 'Good day! Thanks to your agency and specialists Svetlana Topoleva and Ekaterina Vorobyeva, we acquired the property we dreamed of. In a short time, they managed to understand our wishes, solve related problems and find exactly what we wanted. Thank you very much for your cooperation!'), [
                        'contenteditable' => true,
                        'data' => [
                            'editable' => 1,
                            'changed' => 0,
                            'alias' => 'review-2-text',
                            'type' => InstanceBlockField::TYPE_STRING
                        ],
                        'class' => 'grey text-center d-block'
                    ]) ?>
                    <div class="text-center">
                        <div class="d-inline-flex team">
                            <div class="img">
                                <?= Html::img([$fields['review-2-img'] ?? "{$bundle->baseUrl}/images/sara-2.png"], [
                                    'data' => [
                                        'img' => $fields['review-2-img'] ?? null,
                                        'img-editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-2-img',
                                        'type' => InstanceBlockField::TYPE_FILE
                                    ],
                                ]) ?>
                            </div>
                            <div class="text text-left">
                                <?= Html::tag('div', $fields['review-2-name'] ?? 'Sara Strawberry', [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-2-name',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                    'class' => 'name'
                                ]) ?>
                                <?= Html::tag('div', $fields['review-2-who'] ?? Yii::t('instance', 'Client'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-2-who',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                    'class' => 'company grey'
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </blockquote>
                <blockquote class="review-cite">
                    <?= Html::tag('cite', $fields['review-3-text'] ?? Yii::t('instance', 'Probably, there are no such words as to express my gratitude to Artem Kovalenko! Incredible professional, but most importantly - extraordinary kindness people. Thanks for the help of cooperation!'), [
                        'contenteditable' => true,
                        'data' => [
                            'editable' => 1,
                            'changed' => 0,
                            'alias' => 'review-3-text',
                            'type' => InstanceBlockField::TYPE_STRING
                        ],
                        'class' => 'grey text-center d-block'
                    ]) ?>
                    <div class="text-center">
                        <div class="d-inline-flex team">
                            <div class="img">
                                <?= Html::img([$fields['review-3-img'] ?? "{$bundle->baseUrl}/images/sara-4.png"], [
                                    'data' => [
                                        'img' => $fields['review-3-img'] ?? null,
                                        'img-editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-3-img',
                                        'type' => InstanceBlockField::TYPE_FILE
                                    ],
                                ]) ?>
                            </div>
                            <div class="text text-left">
                                <?= Html::tag('div', $fields['review-3-name'] ?? 'Sara Strawberry', [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-3-name',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                    'class' => 'name'
                                ]) ?>
                                <?= Html::tag('div', $fields['review-3-who'] ?? Yii::t('instance', 'Client'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'review-3-who',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                    'class' => 'company grey'
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </blockquote>
            </div>
        </div>
    </section>

<?php
$script = <<<JS
    $(".slider-review").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        dots: false,
        arrows: true
    });
JS;

$this->registerJs($script);
