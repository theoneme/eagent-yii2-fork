<?php

use frontend\modules\instance\assets\IndexAsset;
use frontend\modules\instance\models\InstanceBlockField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 * @var integer $blockId
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);
?>

<section class="banner overflow-hidden"
         style="background: url(<?= ($fields['banner-img'] ?? Url::to(["{$bundle->baseUrl}/images/header_image.jpg"])) ?>);background-size: cover"
         data-img="<?= $fields['banner-img'] ?? null ?>"
         data-img-editable="1"
         data-alias="banner-img"
         data-type="<?= InstanceBlockField::TYPE_FILE ?>"
         data-block="<?= $blockId ?>"
>
    <div class="wrapper">
        <div class="center-block text-center">
            <?= Html::tag('h1', $fields['title'] ?? Yii::t('instance', 'Buy property in Spain'), [
                'contenteditable' => true,
                'data' => [
                    'editable' => 1,
                    'changed' => 0,
                    'alias' => 'title',
                    'type' => InstanceBlockField::TYPE_STRING
                ]
            ]) ?>
            <?= Html::tag('h4', $fields['subtitle'] ?? Yii::t('instance', 'Our agency will help you find the best offers for apartments and houses in Spain.'), [
                'contenteditable' => true,
                'data' => [
                    'editable' => 1,
                    'changed' => 0,
                    'alias' => 'subtitle',
                    'type' => InstanceBlockField::TYPE_STRING
                ]
            ]) ?>
            <?= $this->render('../common/banner-form', ['categories' => $data['categories'], 'locations' => $data['locations']]) ?>
        </div>
    </div>
</section>