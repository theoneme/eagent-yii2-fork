<?php

use common\models\Property;
use frontend\modules\instance\models\InstanceBlockField;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 * @var integer $blockId
 */

?>

    <section class="light-grey-bg properties" data-block="<?= $blockId ?>">
        <div class="wrapper">
            <div class="container-fluid">
                <?= Html::tag('h2', $fields['title'] ?? Yii::t('instance', 'Our newest property'), [
                    'contenteditable' => true,
                    'data' => [
                        'editable' => 1,
                        'changed' => 0,
                        'alias' => 'title',
                        'type' => InstanceBlockField::TYPE_STRING
                    ],
                    'class' => 'text-center'
                ]) ?>
                <?= Html::tag('h5', $fields['subtitle'] ?? Yii::t('instance', 'Every day new objects come to us which you can see in our catalog.'), [
                    'contenteditable' => true,
                    'data' => [
                        'editable' => 1,
                        'changed' => 0,
                        'alias' => 'subtitle',
                        'type' => InstanceBlockField::TYPE_STRING
                    ],
                    'class' => 'text-center grey'
                ]) ?>

                <div class="row">
                    <?php if (!empty($data['properties'])) {
                        foreach ($data['properties'] as $property) { ?>
                            <div class="col-12 col-sm-6 col-md-3">
                                <?= $this->render('@frontend/modules/instance/views/common/partial/property-grid', ['property' => $property]); ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="text-center">
                    <?= Html::a(
                        Yii::t('instance', 'Explore Property'),
                        ['/instance/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_SALE],
                        ['class' => "button transparent big d-inline-block"]
                    ) ?>
                </div>
            </div>
        </div>
    </section>

<?php
$script = <<<JS
    $('[data-fancybox="gallery"]').fancybox({
        baseClass: 'fancybox-advanced',
        hash: false,
        buttons: [
            'close'
        ],
        infobar: false,
        thumbs: false,
        mobile: {
            infobar: false,
            caption: false,
            thumbs: false
        },
        btnTpl: {
            close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg viewBox="0 0 60 60"><path d="M0,0 L60,60 M60,0 L0,60"/></svg></button>',
            arrowLeft: '<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M60,0 L30,30 L60,60"></path></svg></a>',
            arrowRight: '<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M0,0 L30,30 L0,60"></path></svg></a>'
        }
    });
JS;

$this->registerJs($script);