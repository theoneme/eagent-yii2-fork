<?php

use frontend\modules\instance\assets\IndexAsset;
use frontend\modules\instance\models\InstanceBlockField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 * @var integer $blockId
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);

?>

    <section class="our-team" data-block="<?= $blockId ?>">
        <div class="wrapper">
            <div class="container-fluid">
                <?= Html::tag('h2', $fields['title'] ?? Yii::t('instance', 'Our team. Real Estate Agency {name}', ['name' => '']), [
                    'contenteditable' => true,
                    'data' => [
                        'editable' => 1,
                        'changed' => 0,
                        'alias' => 'title',
                        'type' => InstanceBlockField::TYPE_STRING
                    ],
                    'class' => 'text-center'
                ]) ?>
                <?= Html::tag('h5', $fields['subtitle'] ?? Yii::t('instance', 'Legal support'), [
                    'contenteditable' => true,
                    'data' => [
                        'editable' => 1,
                        'changed' => 0,
                        'alias' => 'subtitle',
                        'type' => InstanceBlockField::TYPE_STRING
                    ],
                    'class' => 'text-center grey'
                ]) ?>
            </div>
        </div>
        <div class="wrapper1920">
            <div class="slider-pro">
                <div class="pro">
                    <?= Html::tag('div', '', [
                        'class' => 'img',
                        'style' => "background: url(" . ($fields['human-1-img'] ?? Url::to(["{$bundle->baseUrl}/images/sara-1.png"])) . ");background-size: cover",
                        'data' => [
                            'img' => $fields['human-1-img'] ?? null,
                            'img-editable' => 1,
                            'changed' => 0,
                            'alias' => 'human-1-img',
                            'type' => InstanceBlockField::TYPE_FILE,
                        ],
                    ]) ?>
                    <div class="pro-info w-100">
                        <div class="row">
                            <div class="col-7">
                                <h3>
                                    <?= Html::tag('span', $fields['human-1-name'] ?? 'Sara Strawberry', [
                                        'contenteditable' => true,
                                        'data' => [
                                            'editable' => 1,
                                            'changed' => 0,
                                            'alias' => 'human-1-name',
                                            'type' => InstanceBlockField::TYPE_STRING
                                        ],
                                    ]) ?>
                                </h3>
                                <?= Html::tag('h6', $fields['human-1-specialty'] ?? Yii::t('instance', 'Manager'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'human-1-specialty',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-5 deals">

                            </div>
                        </div>
                        <div class="text">
                            <?= Html::tag('p', $fields['human-1-description'] ?? Yii::t('instance', 'Team member description'), [
                                'contenteditable' => true,
                                'data' => [
                                    'editable' => 1,
                                    'changed' => 0,
                                    'alias' => 'human-1-description',
                                    'type' => InstanceBlockField::TYPE_STRING
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="pro">
                    <?= Html::tag('div', '', [
                        'class' => 'img',
                        'style' => "background: url(" . ($fields['human-2-img'] ?? Url::to(["{$bundle->baseUrl}/images/sara-2.png"])) . ");background-size: cover",
                        'data' => [
                            'img' => $fields['human-2-img'] ?? null,
                            'img-editable' => 1,
                            'changed' => 0,
                            'alias' => 'human-2-img',
                            'type' => InstanceBlockField::TYPE_FILE
                        ],
                    ]) ?>
                    <div class="pro-info w-100">
                        <div class="row">
                            <div class="col-7">
                                <h3>
                                    <?= Html::tag('span', $fields['human-2-name'] ?? 'Sara Strawberry', [
                                        'contenteditable' => true,
                                        'data' => [
                                            'editable' => 1,
                                            'changed' => 0,
                                            'alias' => 'human-2-name',
                                            'type' => InstanceBlockField::TYPE_STRING
                                        ],
                                    ]) ?>
                                </h3>
                                <?= Html::tag('h6', $fields['human-2-specialty'] ?? Yii::t('instance', 'Manager'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'human-2-specialty',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-5 deals">

                            </div>
                        </div>
                        <div class="text">
                            <?= Html::tag('p', $fields['human-2-description'] ?? Yii::t('instance', 'Team member description'), [
                                'contenteditable' => true,
                                'data' => [
                                    'editable' => 1,
                                    'changed' => 0,
                                    'alias' => 'human-2-description',
                                    'type' => InstanceBlockField::TYPE_STRING
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="pro">
                    <?= Html::tag('div', '', [
                        'class' => 'img',
                        'style' => "background: url(" . ($fields['human-3-img'] ?? Url::to(["{$bundle->baseUrl}/images/sara-4.png"])) . ");background-size: cover",
                        'data' => [
                            'img' => $fields['human-3-img'] ?? null,
                            'img-editable' => 1,
                            'changed' => 0,
                            'alias' => 'human-3-img',
                            'type' => InstanceBlockField::TYPE_FILE
                        ],
                    ]) ?>
                    <div class="pro-info w-100">
                        <div class="row">
                            <div class="col-7">
                                <h3>
                                    <?= Html::tag('span', $fields['human-3-name'] ?? 'Sara Strawberry', [
                                        'contenteditable' => true,
                                        'data' => [
                                            'editable' => 1,
                                            'changed' => 0,
                                            'alias' => 'human-3-name',
                                            'type' => InstanceBlockField::TYPE_STRING
                                        ],
                                    ]) ?>
                                </h3>
                                <?= Html::tag('h6', $fields['human-3-specialty'] ?? Yii::t('instance', 'Manager'), [
                                    'contenteditable' => true,
                                    'data' => [
                                        'editable' => 1,
                                        'changed' => 0,
                                        'alias' => 'human-3-specialty',
                                        'type' => InstanceBlockField::TYPE_STRING
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-5 deals">

                            </div>
                        </div>
                        <div class="text">
                            <?= Html::tag('p', $fields['human-3-description'] ?? Yii::t('instance', 'Team member description'), [
                                'contenteditable' => true,
                                'data' => [
                                    'editable' => 1,
                                    'changed' => 0,
                                    'alias' => 'human-3-description',
                                    'type' => InstanceBlockField::TYPE_STRING
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="text-center">-->
<!--                <a href="#" class="button transparent big d-inline-block">--><?//= Yii::t('instance', 'More about us') ?><!--</a>-->
<!--            </div>-->
        </div>
    </section>

<?php
$script = <<<JS
    $(".slider-pro").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        arrows: false,
        infinite: false,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }]
    });
JS;

$this->registerJs($script);