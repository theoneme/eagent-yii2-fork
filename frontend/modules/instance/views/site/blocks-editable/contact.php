<?php

use yii\web\View;
use yii\helpers\Html;
use frontend\modules\instance\models\InstanceBlockField;

/**
 * @var View $this
 * @var array $data
 * @var array $fields
 * @var integer $blockId
 */

?>

<section class="light-grey-bg" data-block="<?= $blockId ?>">
    <div class="map">

    </div>
    <div class="container-fluid">
        <?= Html::tag('h2', $fields['title'] ?? Yii::t('instance', 'We are working for you'), [
            'contenteditable' => true,
            'data' => [
                'editable' => 1,
                'changed' => 0,
                'alias' => 'title',
                'type' => InstanceBlockField::TYPE_STRING
            ],
            'class' => 'text-center'
        ]) ?>
        <?= Html::tag('h6', $fields['subtitle'] ?? Yii::t('instance', 'If you want to buy, rent or sell property, please contact us and we will be glad to provide you with professional services in real estate.'), [
            'contenteditable' => true,
            'data' => [
                'editable' => 1,
                'changed' => 0,
                'alias' => 'subtitle',
                'type' => InstanceBlockField::TYPE_STRING
            ],
            'class' => 'text-center grey'
        ]) ?>
        <?= $this->render('../common/contact-form', ['model' => $data['contactForm']]) ?>
    </div>
</section>