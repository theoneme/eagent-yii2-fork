<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 03.06.2019
 * Time: 13:43
 */

use frontend\assets\plugins\FontAwesomeAsset;
use frontend\modules\instance\assets\MobileCatalogAsset;
use yii\helpers\Url;

/**
 */

FontAwesomeAsset::register($this);
MobileCatalogAsset::register($this);
?>
    <div class="filter-top-mobile d-flex justify-content-between p-3 position-relative">
        <a href="#" id="open-filter" class="filter-link"><i class="icon-parameters align-bottom"></i><span
                    class="align-bottom ml-2">Фильтр</span></a>
        <a href="#" id="map" class="filter-link"><i class="icon-navigation align-bottom"></i><span
                    class="align-bottom ml-2">Карта</span></a>
    </div>
    <div class="container-fluid">
        <h1 class="text-left py-3">7 Квартиры (Продажа) | London (Англия Соединенное Королевство) </h1>
        <div class="row">
            <div class="col-12 col-sm-6">

                <a href="/kvartira-2-spalni-ploshchadyu-64-m-na-prodazhu-v-40249-property" class="prop d-block">
                    <div class="img-block w-100"
                         style="background: url(https://s3.eu-central-1.amazonaws.com/eagent-media/cache/320x230/uploads/property/2018/12/28/kvartira-2-spalni-ploshchadyu-64-m-na-prodazhu-v-40249-5c25a96b0e602.jpg);background-size: cover">
                        <div class="center-block text-center">
                            <button class="button small green d-inline-block">Подробнее</button>
                        </div>
                    </div>
                    <div class="titles">
                        <h4>
                            Квартира 2 спальни площадью 64 м² на продажу в г.London </h4>
                        <p class="grey">Выставлена на продажу квартира в Великобритании, расположенная по адре...</p>
                    </div>
                    <div class="squares d-flex flex-wrap">
                        <div class="col-7">
                            Квартиры
                        </div>
                        <div class="col-5">
                            64 m²
                        </div>
                        <div class="col-7 lft-brdr"></div>
                        <div class="col-5 lft-brdr"></div>
                    </div>
                    <div class="options row">
                        <div class="col-8">
                            <div>69&nbsp;043&nbsp;317р.</div>
                            <div class="grey sticker">
                                На продажу
                            </div>
                        </div>
                        <div class="col-4 nw">
                            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i
                                        class="icon-heart grey"></i></div>
                            <div class="hint--top-right favorite d-inline-block" data-hint="Добавить в сравнение"
                                 data-hint-add="Добавить в сравнение" data-hint-remove="Убрать из сравнения"
                                 data-action="toggle-compare" data-id="40249"><i class="icon-plus-black-symbol"></i>
                            </div>
                        </div>
                    </div>
                </a></div>
            <div class="col-12 col-sm-6">

                <a href="/4-komnatnaya-kvartira-ploshchadyu-130-m-na-prodazhu-40242-property" class="prop d-block">
                    <div class="img-block w-100"
                         style="background: url(https://s3.eu-central-1.amazonaws.com/eagent-media/cache/320x230/uploads/property/2018/12/28/4-komnatnaya-kvartira-ploshchadyu-130-m-na-prodazhu-40242-5c25a93133ab0.jpg);background-size: cover">
                        <div class="center-block text-center">
                            <button class="button small green d-inline-block">Подробнее</button>
                        </div>
                    </div>
                    <div class="titles">
                        <h4>
                            4-комнатная квартира площадью 130 м² на продажу </h4>
                        <p class="grey">Лондон <br>
                            Westminster <br>
                            Пентхаус в престижном комплексе в центр...</p>
                    </div>
                    <div class="squares d-flex flex-wrap">
                        <div class="col-7">
                            Квартиры 4 комнаты
                        </div>
                        <div class="col-5">
                            130 m²
                        </div>
                        <div class="col-7 lft-brdr"></div>
                        <div class="col-5 lft-brdr"></div>
                    </div>
                    <div class="options row">
                        <div class="col-8">
                            <div>108&nbsp;848&nbsp;493р.</div>
                            <div class="grey sticker">
                                На продажу
                            </div>
                        </div>
                        <div class="col-4 nw">
                            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i
                                        class="icon-heart grey"></i></div>
                            <div class="hint--top-right favorite d-inline-block" data-hint="Добавить в сравнение"
                                 data-hint-add="Добавить в сравнение" data-hint-remove="Убрать из сравнения"
                                 data-action="toggle-compare" data-id="40242"><i class="icon-plus-black-symbol"></i>
                            </div>
                        </div>
                    </div>
                </a></div>
            <div class="col-12 col-sm-6">

                <a href="/kvartira-3-spalni-ploshchadyu-113-m-na-prodazhu-v-36261-property" class="prop d-block">
                    <div class="img-block w-100"
                         style="background: url(https://s3.eu-central-1.amazonaws.com/eagent-media/cache/320x230/uploads/property/2018/12/17/kvartira-3-spalni-ploshchadyu-113-m-na-prodazhu-v-36261-5c17d1c64e936.jpg);background-size: cover">
                        <div class="center-block text-center">
                            <button class="button small green d-inline-block">Подробнее</button>
                        </div>
                    </div>
                    <div class="titles">
                        <h4>
                            Квартира 3 спальни площадью 113 м² на продажу в г.London </h4>
                        <p class="grey"></p>
                    </div>
                    <div class="squares d-flex flex-wrap">
                        <div class="col-7">
                            Квартиры
                        </div>
                        <div class="col-5">
                            113 m²
                        </div>
                        <div class="col-7 lft-brdr"></div>
                        <div class="col-5 lft-brdr"></div>
                    </div>
                    <div class="options row">
                        <div class="col-8">
                            <div>83&nbsp;870&nbsp;888р.</div>
                            <div class="grey sticker">
                                На продажу
                            </div>
                        </div>
                        <div class="col-4 nw">
                            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i
                                        class="icon-heart grey"></i></div>
                            <div class="hint--top-right favorite d-inline-block" data-hint="Добавить в сравнение"
                                 data-hint-add="Добавить в сравнение" data-hint-remove="Убрать из сравнения"
                                 data-action="toggle-compare" data-id="36261"><i class="icon-plus-black-symbol"></i>
                            </div>
                        </div>
                    </div>
                </a></div>
            <div class="col-12 col-sm-6">

                <a href="/3-komnatnaya-kvartira-ploshchadyu-92-m-na-prodazhu-36254-property" class="prop d-block">
                    <div class="img-block w-100"
                         style="background: url(https://s3.eu-central-1.amazonaws.com/eagent-media/cache/320x230/uploads/property/2018/12/17/3-komnatnaya-kvartira-ploshchadyu-92-m-na-prodazhu-36254-5c17d16cf1cfc.jpg);background-size: cover">
                        <div class="center-block text-center">
                            <button class="button small green d-inline-block">Подробнее</button>
                        </div>
                    </div>
                    <div class="titles">
                        <h4>
                            3-комнатная квартира площадью 92 м² на продажу </h4>
                        <p class="grey"></p>
                    </div>
                    <div class="squares d-flex flex-wrap">
                        <div class="col-7">
                            Квартиры 3 комнаты
                        </div>
                        <div class="col-5">
                            92 m²
                        </div>
                        <div class="col-7 lft-brdr"></div>
                        <div class="col-5 lft-brdr"></div>
                    </div>
                    <div class="options row">
                        <div class="col-8">
                            <div>115&nbsp;302&nbsp;522р.</div>
                            <div class="grey sticker">
                                На продажу
                            </div>
                        </div>
                        <div class="col-4 nw">
                            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i
                                        class="icon-heart grey"></i></div>
                            <div class="hint--top-right favorite d-inline-block" data-hint="Добавить в сравнение"
                                 data-hint-add="Добавить в сравнение" data-hint-remove="Убрать из сравнения"
                                 data-action="toggle-compare" data-id="36254"><i class="icon-plus-black-symbol"></i>
                            </div>
                        </div>
                    </div>
                </a></div>
            <div class="col-12 col-sm-6">

                <a href="/kvartira-ploshchadyu-29-m-na-prodazhu-30653-property" class="prop d-block">
                    <div class="img-block w-100"
                         style="background: url(https://s3.eu-central-1.amazonaws.com/eagent-media/cache/320x230/uploads/property/2018/11/30/kvartira-ploshchadyu-29-m-na-prodazhu-30653-5c0137501de48.jpg);background-size: cover">
                        <div class="center-block text-center">
                            <button class="button small green d-inline-block">Подробнее</button>
                        </div>
                    </div>
                    <div class="titles">
                        <h4>
                            Квартира площадью 29 м² на продажу </h4>
                        <p class="grey">Выставлена на продажу квартира в Великобритании, расположенная по адре...</p>
                    </div>
                    <div class="squares d-flex flex-wrap">
                        <div class="col-7">
                            Квартиры
                        </div>
                        <div class="col-5">
                            29 m²
                        </div>
                        <div class="col-7 lft-brdr"></div>
                        <div class="col-5 lft-brdr"></div>
                    </div>
                    <div class="options row">
                        <div class="col-8">
                            <div>25&nbsp;715&nbsp;149р.</div>
                            <div class="grey sticker">
                                На продажу
                            </div>
                        </div>
                        <div class="col-4 nw">
                            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i
                                        class="icon-heart grey"></i></div>
                            <div class="hint--top-right favorite d-inline-block" data-hint="Добавить в сравнение"
                                 data-hint-add="Добавить в сравнение" data-hint-remove="Убрать из сравнения"
                                 data-action="toggle-compare" data-id="30653"><i class="icon-plus-black-symbol"></i>
                            </div>
                        </div>
                    </div>
                </a></div>
            <div class="col-12 col-sm-6">

                <a href="/3-komnatnaya-kvartira-na-prodazhu-30633-property" class="prop d-block">
                    <div class="img-block w-100"
                         style="background: url(https://s3.eu-central-1.amazonaws.com/eagent-media/cache/320x230/uploads/property/2018/11/30/3-komnatnaya-kvartira-na-prodazhu-30633-5c01368f95e20.jpg);background-size: cover">
                        <div class="center-block text-center">
                            <button class="button small green d-inline-block">Подробнее</button>
                        </div>
                    </div>
                    <div class="titles">
                        <h4>
                            3-комнатная квартира на продажу </h4>
                        <p class="grey">Лондон<br>
                            Westminster <br>
                            Квартира в новом доме с идеальным местора...</p>
                    </div>
                    <div class="squares d-flex flex-wrap">
                        <div class="col-7">
                            Квартиры 3 комнаты
                        </div>
                        <div class="col-5">
                        </div>
                        <div class="col-7 lft-brdr"></div>
                        <div class="col-5 lft-brdr"></div>
                    </div>
                    <div class="options row">
                        <div class="col-8">
                            <div>105&nbsp;942&nbsp;021р.</div>
                            <div class="grey sticker">
                                На продажу
                            </div>
                        </div>
                        <div class="col-4 nw">
                            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i
                                        class="icon-heart grey"></i></div>
                            <div class="hint--top-right favorite d-inline-block" data-hint="Добавить в сравнение"
                                 data-hint-add="Добавить в сравнение" data-hint-remove="Убрать из сравнения"
                                 data-action="toggle-compare" data-id="30633"><i class="icon-plus-black-symbol"></i>
                            </div>
                        </div>
                    </div>
                </a></div>
            <div class="col-12 col-sm-6">

                <a href="/apartment-for-sale-28312-property" class="prop d-block">
                    <div class="img-block w-100"
                         style="background: url(https://s3.eu-central-1.amazonaws.com/eagent-media/cache/320x230/uploads/property/2018/11/apartment-for-sale-28312-5bf560002e5b8.jpg);background-size: cover">
                        <div class="center-block text-center">
                            <button class="button small green d-inline-block">Подробнее</button>
                        </div>
                    </div>
                    <div class="titles">
                        <h4>
                            Apartment for sale </h4>
                        <p class="grey"></p>
                        <p> We are delighted to offer a 8 property off-plan investment portfol...</p>
                    </div>
                    <div class="squares d-flex flex-wrap">
                        <div class="col-7">
                            Квартиры
                        </div>
                        <div class="col-5">
                        </div>
                        <div class="col-7 lft-brdr"></div>
                        <div class="col-5 lft-brdr"></div>
                    </div>
                    <div class="options row">
                        <div class="col-8">
                            <div>366&nbsp;054&nbsp;741р.</div>
                            <div class="grey sticker">
                                На продажу
                            </div>
                        </div>
                        <div class="col-4 nw">
                            <div class="hint--top-right favorite d-inline-block" data-hint="Add to bookmark"><i
                                        class="icon-heart grey"></i></div>
                            <div class="hint--top-right favorite d-inline-block" data-hint="Добавить в сравнение"
                                 data-hint-add="Добавить в сравнение" data-hint-remove="Убрать из сравнения"
                                 data-action="toggle-compare" data-id="28312"><i class="icon-plus-black-symbol"></i>
                            </div>
                        </div>
                    </div>
                </a></div>
        </div>
    </div>


    <div class="d-none map-over">
        <div id="map_catalog">

        </div>
    </div>

    <div id="filter-component" class="d-none">

        <form id="filter-form" action="http://ekaterinburg.eagent.front/flats-for-sale-catalog" method="get"><input
                    type="hidden" id="filter-sort" name="sort"><input type="hidden" id="filter-box" name="box"><input
                    type="hidden" id="filter-zoom" name="zoom">
            <div class="filters open">
                <div class="loading-overlay hidden"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4">
                            <div class="filter-title">
                                Я хочу
                            </div>
                        </div>
                        <div class="col-8 d-flex">
                            <div class="filter-choose w-100">
                                <input type="radio" id="sale" name="operation" value="sale" checked=""> <label
                                        class="text-center w-100 active" for="sale">Купить</label></div>
                            <div class="filter-choose w-100">
                                <input type="radio" id="rent" name="operation" value="rent"> <label
                                        class="text-center w-100 " for="rent">Арендовать</label></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="filter-title">Категория</div>
                    <div>
                        <div class="filter-item filter-types">
                            <div class="filter-box" data-role="mobile-category-filter">
                                <div class="filter-type-group active">
                                    <div class="filter-type-group-name">
                                        <input type="radio" id="cat-flats" class="radio-checkbox" name="category"
                                               value="flats" checked="" data-role="parent-category"> <label
                                                for="cat-flats">
                                            <span class="filter-circle filter-circle-red"></span>
                                            <span class="ftgn-title">Квартиры</span>
                                        </label>
                                    </div>
                                    <div class="filter-sub-types">
                                        <div class="filter-sub-type">
                                            <input type="radio" id="cat-flats-kvartiry" name="category"
                                                   value="flats-kvartiry"> <label for="cat-flats-kvartiry">
                                                <span class="ftgn-title">Квартиры</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="filter-sub-types">
                                        <div class="filter-sub-type">
                                            <input type="radio" id="cat-flats-apartamenty" name="category"
                                                   value="flats-apartamenty"> <label for="cat-flats-apartamenty">
                                                <span class="ftgn-title">Апартаменты</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-type-group ">
                                    <div class="filter-type-group-name">
                                        <input type="radio" id="cat-houses" class="radio-checkbox" name="category"
                                               value="houses" data-role="parent-category"> <label for="cat-houses">
                                            <span class="filter-circle filter-circle-magenta"></span>
                                            <span class="ftgn-title">Дома</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="filter-type-group ">
                                    <div class="filter-type-group-name">
                                        <input type="radio" id="cat-land" class="radio-checkbox" name="category"
                                               value="land" data-role="parent-category"> <label for="cat-land">
                                            <span class="filter-circle filter-circle-blue"></span>
                                            <span class="ftgn-title">Земля</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="filter-type-group ">
                                    <div class="filter-type-group-name">
                                        <input type="radio" id="cat-commercial-property" class="radio-checkbox"
                                               name="category" value="commercial-property" data-role="parent-category">
                                        <label for="cat-commercial-property">
                                            <span class="filter-circle filter-circle-yellow"></span>
                                            <span class="ftgn-title">Коммерческая недвижимость</span>
                                        </label>
                                    </div>
                                    <div class="filter-sub-types">
                                        <div class="filter-sub-type">
                                            <input type="radio" id="cat-commercial-property-ofisnye-zdaniya"
                                                   name="category" value="commercial-property-ofisnye-zdaniya"> <label
                                                    for="cat-commercial-property-ofisnye-zdaniya">
                                                <span class="ftgn-title">Офисные здания / помещения</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="filter-sub-types">
                                        <div class="filter-sub-type">
                                            <input type="radio" id="cat-commercial-property-torgovye-pomeshcheniya"
                                                   name="category" value="commercial-property-torgovye-pomeshcheniya">
                                            <label for="cat-commercial-property-torgovye-pomeshcheniya">
                                                <span class="ftgn-title">Торговые помещения / центры</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group filter-item">
                    <label class="filter-title" for="min-price-input">Цена</label>
                    <div class="d-flex two-param">
                        <input type="number" id="min-price-input" class="input-grey" name="price[min]"
                               placeholder="Минимум">
                        <div class="dash">
                            –
                        </div>
                        <input type="number" id="max-price-input" class="input-grey" name="price[max]"
                               placeholder="Максимум"></div>
                </div>
                <div class="form-group filter-item">
                    <div class="row">
                        <div class="col-4">
                            <div class="filter-title">
                                Количество комнат
                            </div>
                        </div>
                        <div class="col-8 d-flex">
                            <div class="filter-choose w-100">
                                <input type="radio" id="rooms-1" name="rooms[min]" value="1"> <label
                                        class="text-center w-100 " for="rooms-1">1+</label></div>
                            <div class="filter-choose w-100">
                                <input type="radio" id="rooms-2" name="rooms[min]" value="2"> <label
                                        class="text-center w-100 " for="rooms-2">2+</label></div>
                            <div class="filter-choose w-100">
                                <input type="radio" id="rooms-3" name="rooms[min]" value="3"> <label
                                        class="text-center w-100 " for="rooms-3">3+</label></div>
                            <div class="filter-choose w-100">
                                <input type="radio" id="rooms-4" name="rooms[min]" value="4"> <label
                                        class="text-center w-100 " for="rooms-4">4+</label></div>
                            <div class="filter-choose w-100">
                                <input type="radio" id="rooms-5" name="rooms[min]" value="5"> <label
                                        class="text-center w-100 " for="rooms-5">5+</label></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="filter-title" for="property_area-min">Общая площадь (м²)</label>
                    <div class="d-flex two-param">
                        <input type="number" id="property_area-min" name="property_area[min]" placeholder="От 22"
                               min="0">
                        <div class="dash">–</div>
                        <input type="number" name="property_area[max]" placeholder="До 1254" min="0"></div>
                </div>
                <div class="form-group">
                    <label class="filter-title" for="living_area-min">Жилая площадь (м²)</label>
                    <div class="d-flex two-param">
                        <input type="number" id="living_area-min" name="living_area[min]" placeholder="От 10" min="0">
                        <div class="dash">–</div>
                        <input type="number" name="living_area[max]" placeholder="До 120" min="0"></div>
                </div>
                <div class="form-group">
                    <label class="filter-title" for="kitchen_area-min">Площадь кухни (м²)</label>
                    <div class="d-flex two-param">
                        <input type="number" id="kitchen_area-min" name="kitchen_area[min]" placeholder="От 5" min="0">
                        <div class="dash">–</div>
                        <input type="number" name="kitchen_area[max]" placeholder="До 50" min="0"></div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-4"><label class="filter-title" for="condition">Состояние</label></div>
                        <div class="col-8">
                            <select class="w-100" name="condition" id="condition">
                                <option value="">
                                    Выберите Состояние
                                </option>
                                <option value="v-otlichnom-sostoyanii">В отличном состоянии (113)</option>
                                <option value="khoroshee">Хорошее (92)</option>
                                <option value="trebuet-kosmeticheskogo-remonta">Требует косметического ремонта (31)
                                </option>
                                <option value="trebuet-kapitalnogo-remonta">Требует капитального ремонта (2)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="filter-title" for="balconies-min">Количество балконов</label>
                    <div class="d-flex two-param">
                        <input type="number" id="balconies-min" name="balconies[min]" placeholder="От 1" min="0">
                        <div class="dash">–</div>
                        <input type="number" name="balconies[max]" placeholder="До 2" min="0">
                    </div>
                </div>
                <div class="form-group">
                    <label class="filter-title" for="loggias-min">Количество лоджий</label>
                    <div class="d-flex two-param">
                        <input type="number" id="loggias-min" name="loggias[min]" placeholder="От 1" min="0">
                        <div class="dash">–</div>
                        <input type="number" name="loggias[max]" placeholder="До 3" min="0">
                    </div>
                </div>
                <div class="filter-ops d-flex justify-content-between align-items-center">
                    <a class="" href="#" data-action="reset-filter">Сбросить</a>
                    <div class="mobile-results-count">
                        464 объекта
                    </div>
                    <input type="submit" class="" value="Показать">
                </div>
            </div>

        </form>
    </div>


<?php $markerUserViewUrl = Url::to(['/agent/marker/view']);
$markerUserIcon = Url::to(['/images/user-marker.png'], true);
$markerListUrl = Url::to(['/agent/marker/catalog']);

$script = <<<JS
    /*let map = new CatalogMap({
        mode: 'advanced',
        markerViewRoutes: {
            user: '{$markerUserViewUrl}',  
        },
        markerIcons: {
            user: '{$markerUserIcon}'
        }
    });
    map.init();
    
    map.fetchMarkers('{$markerListUrl}', {});*/
    
    $(document.body).on('click', '#map', function() { 
      $('.map-over').toggleClass('d-none');
      $('body').toggleClass('overflow-hidden');
      return false;
    });
    
     $(document.body).on('click', '#open-filter', function() { 
      $('#filter-component').toggleClass('d-none');
      $('body').toggleClass('overflow-hidden');
      return false;
    });
     
     $(window).scroll(function() {
        if ($(this).scrollTop() >= 100) {
            if (!$('.filter-top-mobile').hasClass('scroll-content')) {
                $('.filter-top-mobile').addClass('scroll-content');
                $('.call').addClass('scroll-content');
                $('header').addClass('overflow-hidden');
            }
        } else {
            if ($('.filter-top-mobile').hasClass('scroll-content')) {
                $('.filter-top-mobile').removeClass('scroll-content');
                $('.call').removeClass('scroll-content');
                $('header').removeClass('overflow-hidden');
            }
        }
    });
JS;

$this->registerJs($script);