<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use yii\widgets\LinkPager;

/**
 * @var array $sortData
 * @var array $seo
 * @var array $properties
 * @var integer $gridSize
 */

?>

    <div class="loading-overlay hidden"></div>

    <h1 class="text-left py-3"><?= $seo['heading'] ?></h1>
<!--    <h5 class="text-center grey mb-5">-->
<!--        Lorem Ipsum is simply dummy text of the printing and Lorem Ipsum has been the industry's standard-->
<!--    </h5>-->
<?php if (count($properties['items']) > 0) { ?>
    <div class="row">
        <?php foreach ($properties['items'] as $property) { ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2">
                <?= $this->render('@frontend/modules/instance/views/common/partial/property-grid', ['property' => $property]); ?>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <nav class="col-12 d-flex justify-content-around my-3">
            <?php if ($properties['pagination']) {
                $properties['pagination']->route = '/instance/property/catalog/index-ajax-short';

                echo LinkPager::widget([
                    'pagination' => $properties['pagination'],
                    'linkOptions' => ['data-action' => 'switch-page', 'class' => 'page-link'],
                    'linkContainerOptions' => ['class' => 'page-item'],
                    'disabledListItemSubTagOptions' => [
                        'tag' => 'a',
                        'class' => ['page-link']
                    ],
                    'prevPageLabel' => '«&nbsp;' .  Yii::t('instance', 'Prev Page'),
                    'nextPageLabel' => Yii::t('instance', 'Next Page') . '&nbsp;»',
                ]); ?>
            <?php } ?>
        </nav>
    </div>
<?php } else { ?>
    <div class="home-list">
        <p>
            <?= !empty($seo['emptyText']) ? $seo['emptyText'] : Yii::t('instance', 'No results found'); ?>
        </p>
    </div>
<?php } ?>