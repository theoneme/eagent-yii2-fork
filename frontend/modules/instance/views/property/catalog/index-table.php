<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:12
 */

use common\assets\GoogleAsset;
use frontend\assets\FilterAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use frontend\assets\plugins\CatalogMapAsset;
use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\modules\instance\assets\CatalogAsset;
use frontend\widgets\PropertyFilter;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $properties
 * @var array $seo
 * @var array $sortData
 * @var View $this
 * @var PropertyFilter $propertyFilter
 * @var string $lat
 * @var string $lng
 */

FancyBoxAsset::register($this);
CatalogAsset::register($this);
CatalogMapAsset::register($this);
GoogleAsset::register($this);
AutoCompleteAsset::register($this);
FilterAsset::register($this);
FontAwesomeAsset::register($this);

?>
    <div class="wrapper1920">
        <div class="filter" id="filter-component">
            <?= $propertyFilter->run() ?>
        </div>
        <div class="container-fluid light-grey-bg" id="properties-list">
            <?= $this->render('partial/list', [
                'sortData' => $sortData,
                'seo' => $seo,
                'properties' => $properties
            ]) ?>
        </div>
    </div>

    <!--вставишь потом куда надо -->
    <div id="fancy-caption-container"
         class="d-none <? //= empty($building['attributes']) ? 'empty-fancy-container' : '' ?>">
        <div>
            <!-- <?php /* if (!empty($building['attributes'])) { ?>
                <h4 class="up-title text-left">
                    <?= Yii::t('building', 'Building Parameters') ?>
                </h4>
                <ul class="no-list fancybox-attributes">
                    <?php foreach ($building['attributes'] as $attribute) { ?>
                        <li>
                            <?= "{$attribute['title']}: {$attribute['value']}" ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php } */ ?>-->
            <h3>4&nbsp;700&nbsp;000р.</h3>
            <h4 class="up-title text-left">
                Параметры объекта </h4>
            <ul class="no-list fancybox-attributes">
                <li>
                    Общая площадь (м²): 81
                </li>
                <li>
                    Жилая площадь (м²): 48
                </li>
                <li>
                    Площадь кухни (м²): 12
                </li>
                <li>
                    Планировка: изолированная
                </li>
                <li>
                    Санузел: раздельный
                </li>
                <li>
                    Количество балконов: 1
                </li>
                <li>
                    Количество комнат: 3
                </li>
            </ul>

            <div class="col-12 col-md-6 col-lg-12">
                <div class="d-flex align-items-center prop-agent py-4">
                    <div class="img rounded-circle overflow-hidden">
                        <? /*= Html::img($siteOwner['avatar'], [
                            'alt' => $siteOwner['username'],
                            'class' => 'mx-auto'
                        ])*/ ?>

                        <img class="mx-auto"
                             src="https://s3.eu-central-1.amazonaws.com/eagent-media/images/agent-no-image.png"
                             alt="Helen1">
                    </div>
                    <div class="text">
                        <div class="name font-weight-bold">
                            <? //= $siteOwner['username'] ?>
                            Username Agentovich
                        </div>
                        <div class="company grey">
                            <?= Yii::t('instance', 'Agent') ?>
                        </div>
                    </div>
                </div>
                <? /*php $form = ActiveForm::begin([
                    'action' => Url::to(['/contact/contact-agent']),
                    'id' => 'side-contact-agent-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>
                <?= $form->field($contactAgentForm, 'name')->textInput([
                    'placeholder' => Yii::t('instance', 'Your Name'),
                ])->label(false) ?>
                <?= $form->field($contactAgentForm, 'phone')->textInput([
                    'placeholder' => Yii::t('instance', 'Your Phone'),
                ])->label(false) ?>
                <?= $form->field($contactAgentForm, 'email')->textInput([
                    'type' => 'email',
                    'placeholder' => Yii::t('instance', 'Your Email'),
                ])->label(false) ?>
                <?= $form->field($contactAgentForm, 'message')->textarea([
                    'class' => 'w-100',
                    'placeholder' => Yii::t('instance', 'Write Your Text'),
                ])->label(false) ?>
                <?= Html::submitInput(Yii::t('instance', 'Contact Agent'), [
                    'class' => 'button big orange w-100'
                ]) ?>
                <?php ActiveForm::end()*/ ?>

                <form id="side-contact-agent-form" action="/contact/contact-agent" method="post">
                    <input type="hidden" name="_csrf-instance"
                           value="_E17obRZ7C13ADNOtNw2sMml5LTGWdruTBcPn_KH1JK0DgHLwRqNaQUwYArElGnEkP_T1bIPttd7UWD6yuOB6A==">
                    <div class="form-group field-contactagentform-name required validating">

                        <input type="text" id="contactagentform-name" class="form-control" name="ContactAgentForm[name]"
                               placeholder="Ваше имя" aria-required="true">

                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-contactagentform-phone">

                        <input type="text" id="contactagentform-phone" class="form-control"
                               name="ContactAgentForm[phone]" placeholder="Ваш телефон">

                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-contactagentform-email required">

                        <input type="email" id="contactagentform-email" class="form-control"
                               name="ContactAgentForm[email]" placeholder="Ваш Email" aria-required="true">

                        <div class="help-block"></div>
                    </div>
                    <div class="form-group field-contactagentform-message">

                        <textarea id="contactagentform-message" class="w-100" name="ContactAgentForm[message]"
                                  placeholder="Напишите сообщение"></textarea>

                        <div class="help-block"></div>
                    </div>
                    <input type="submit" class="button big orange w-100" value="Связаться с агентом"></form>
            </div>

        </div>
    </div>
<?php $baseUrl = Url::to(['/instance/property/catalog/search']);
$catalogViewUrl = Url::to(['/instance/property/catalog/view']);
$markerListUrl = Url::to(['/property/marker/catalog']);
$markerViewUrl = Url::to(['/instance/property/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$complexUrl = Url::to(['/instance/property/catalog/index-ajax']);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);

$script = <<<JS
    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };

    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    
    $(document).on('click', '#properties-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if(result.success === true) {
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({result: true}, 'Page', result.url);
            } 
        });
        
        return false;
    });
    
    let fn = function(result) {
        if (result.domain === false) {
            // map.setMarkers(result.markers);
            $('#properties-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $(document).on('submit', filter.options.filterSelector, function() {
         let params = filter.buildQuery();
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
         let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    let fancyCaptionContainer = '#fancy-caption-container';
    $('[data-fancybox="gallery"]').fancybox({
        baseClass: 'fancybox-advanced',
        hash: false,
        buttons: [
            'close'
        ],
        infobar: false,
        /*beforeShow : function( instance, current ) {
            $('.fancybox-caption').append($(fancyCaptionContainer + '> div'));
        },
        beforeClose : function() {
            $(fancyCaptionContainer).append($('.fancybox-caption > div'));
        },*/
        thumbs: false,
        mobile: {
            infobar: false,
            caption: false,
            thumbs: false
        },
        btnTpl: {
            close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg viewBox="0 0 60 60"><path d="M0,0 L60,60 M60,0 L0,60"/></svg></button>',
            arrowLeft: '<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M60,0 L30,30 L60,60"></path></svg></a>',
            arrowRight: '<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}" href="javascript:;"><svg viewBox="0 0 60 60"><path d="M0,0 L30,30 L0,60"></path></svg></a>'
        }
    });
JS;
$this->registerJs($script);