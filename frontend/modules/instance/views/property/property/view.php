<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.05.2019
 * Time: 11:38
 */

use common\models\Property;
use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\assets\plugins\ModalMapAsset;
use frontend\assets\plugins\MortgageCalculatorAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\modules\instance\assets\PropertyAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var array $property
 * @var array $building
 * @var array $siteOwner
 * @var array $similarProperties
 * @var \frontend\forms\contact\ContactAgentForm $contactAgentForm
 * @var \yii\web\View $this
 */

SlickAsset::register($this);
FontAwesomeAsset::register($this);
ModalMapAsset::register($this);
PropertyAsset::register($this);
FancyBoxAsset::register($this);
MortgageCalculatorAsset::register($this);

?>

    <div class="wrapper1920">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="photo" role="tabpanel" aria-labelledby="a-photo">
                <div class="slider-property">
                    <?php foreach ($property['images'] as $image) { ?>
                        <div>
                            <?= Html::a(null, $image, [
                                'class' => 'img d-block',
                                'style' => "background:url({$image}); background-size: cover",
                                'data-fancybox' => 'primary-fancy',
                            ]) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="a-map">
                <div id="modal-map" class="property-map"
                     data-lat="<?= $property['lat'] ?>"
                     data-lng="<?= $property['lng'] ?>" data-type="property">
                </div>
            </div>
        </div>
    </div>
    <div class="filter">
        <div class="wrapper1920">
            <div class="d-flex justify-content-around">
                <ul class="nav tab-links col-12 col-lg-6 col-xl-4 no-list catalog-view d-flex p-0 justify-content-end"
                    role="tablist">
                    <li class="col-6">
                        <a class="active text-center d-block" id="a-photo" href="#photo" role="tab"
                           data-toggle="tab"
                           aria-controls="map" aria-selected="true">
                            <i class="icon-camera"></i>
                            <?= Yii::t('instance', 'Photo') ?>
                        </a>
                    </li>
                    <li class="col-6">
                        <a class="text-center d-block" href="#map" id="a-map" role="tab" data-toggle="tab"
                           data-action="init-gmap">
                            <i class="icon-gps"></i>
                            <?= Yii::t('instance', 'Map') ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="light-grey-bg">
    <div class="wrapper">
    <div class="container-fluid">
        <div class="grey py-3">
            <?= Html::a(Yii::t('instance', 'Home'), ['/instance/site/index']) ?> /
            <?= Html::a($property['category']['title'], ['/instance/property/catalog/index', 'category' => $property['category']['slug'], 'operation' => Property::OPERATION_SALE]) ?>
            /
            <?= $property['title'] ?>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-8">
                <h1><?= $property['title'] ?></h1>
                <h5 class="grey">
                    <?= $property['address'] ?>
                </h5>
                <?php if (!empty($property['description'])) { ?>
                    <p>
                        <?= $property['description'] ?>
                    </p>
                <?php } ?>

                <div class="my-4 property-section">
                    <h3><?= Yii::t('instance', 'Property Features:') ?></h3>
                    <?php if (!empty($property['attributes'])) { ?>
                        <ul class="no-list prop-feat my-3 row">
                            <?php foreach ($property['attributes'] as $attribute) { ?>
                                <li class="col-6 col-sm-4">
                                    <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <?= Yii::t('instance', 'No attributes specified') ?>
                    <?php } ?>
                </div>
                <?php if (!empty($building) && !empty($building['attributes'])) { ?>
                    <div class="my-4 property-section">
                        <h3><?= Yii::t('instance', 'Building Features:') ?></h3>
                        <?php if (!empty($building['attributes'])) { ?>
                            <ul class="no-list prop-feat my-3 row">
                                <?php foreach ($building['attributes'] as $attribute) { ?>
                                    <li class="col-6 col-sm-4">
                                        <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <?= Yii::t('instance', 'No attributes specified') ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if (!empty($property['video'])) { ?>
                    <div class="mb-3 property-section">
                        <h3><?= Yii::t('instance', 'Video Tour:') ?></h3>
                        <div class="prop-video position-relative">
                            <iframe height=480 class="w-100"
                                    src="<?= $property['video']['content'] ?>">
                            </iframe>
                        </div>
                    </div>
                <?php } ?>
                <div class="row share-report pt-3 pb-4">
                    <?= \frontend\widgets\SocialShareWidget::widget([
                        'template' => '@frontend/modules/instance/widgets/views/social-share-widget',
                        'url' => Url::current([], true)
                    ]) ?>
                </div>
                <?php if (!empty($similarProperties)) { ?>
                    <div class="row">
                        <div class="col-12">
                            <h4 class="font-weight-bold"><?= Yii::t('instance', 'Here is some Similar property:') ?></h4>
                            <p class="grey">
                                <?= Yii::t('instance', 'near {address}', [
                                    'address' => $property['address']
                                ]) ?>
                            </p>
                            <div class="similar-list pt-2">
                                <?php foreach ($similarProperties as $similar) { ?>
                                    <div class="similar-item d-flex py-3">
                                        <?= Html::a(Html::img($similar['image']), ['/instance/property/property/view', 'slug' => $similar['slug']], [
                                            'class' => 'img'
                                        ]) ?>
                                        <div class="info">
                                            <?= Html::a($similar['title'], ['/instance/property/property/view', 'slug' => $similar['slug']], [
                                                'class' => 'font-weight-bold'
                                            ]) ?>
                                            <div class="address grey"><?= $similar['address'] ?></div>
                                            <div class="price font-weight-bold"><?= $similar['price'] ?></div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-12 col-lg-4">
                <div class="bg-white p-4">
                    <div class="row">
                        <div class="col-6 col-md-3 col-xl-6 col-xl-6 prop-price pb-4 pr-0">
                            <h2 class="p-0"><?= $property['price'] ?></h2>
                            <h5 class="text-uppercase grey">
                                <?= $property['type'] === Property::TYPE_SALE
                                    ? Yii::t('instance', 'For Sale')
                                    : Yii::t('instance', 'For Rent') ?>
                            </h5>
                        </div>
                        <div class="col-6 col-md-3 col-xl-6 col-xl-5 rate">
                            <div class="stars d-flex pt-1 pb-3">
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                            </div>
                            <div class="row grey">
                                <div class="col-6"><?= Yii::t('instance', 'Rating:') ?></div>
                                <div class="col-6 text-right">
                                    <span class="font-weight-bold">5.0</span>/5.0
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-12 grey">
                            <div class="squares row mb-3">
                                <div class="col-6">
                                    <i class="icon-bed"></i>
                                    <?php if (array_key_exists('rooms', $property['attributes'])) { ?>
                                        <?= Yii::t('instance', '{rooms, plural, one{# room} other{# rooms}}', [
                                            'rooms' => $property['attributes']['rooms']['value']
                                        ]) ?>
                                    <?php } else { ?>
                                        <?= Yii::t('instance', '{rooms} rooms', [
                                            'rooms' => '?'
                                        ]) ?>
                                    <?php } ?>
                                </div>
                                <div class="col-6">
                                    <i class="icon-bathtub"></i>
                                    <?php if (array_key_exists('bathrooms', $property['attributes'])) { ?>
                                        <?= Yii::t('instance', '{bathrooms, plural, one{# bathroom} other{# bathrooms}}', [
                                            'bathrooms' => $property['attributes']['bathrooms']['value']
                                        ]) ?>
                                    <?php } else { ?>
                                        <?= Yii::t('instance', '{bathrooms} bathrooms', [
                                            'bathrooms' => '?'
                                        ]) ?>
                                    <?php } ?>
                                </div>
                                <div class="col-6 lft-brdr"></div>
                                <div class="col-6 lft-brdr"></div>
                            </div>
                            <div class="squares row mb-3">
                                <div class="col-6">
                                    <i class="icon-dimensions"></i>
                                    <?php if (array_key_exists('property_area', $property['attributes'])) { ?>
                                        <?= Yii::t('instance', '{area} m²', [
                                            'area' => $property['attributes']['property_area']['value']
                                        ]) ?>
                                    <?php } else { ?>
                                        <?= Yii::t('instance', '{area} m²', [
                                            'area' => '?'
                                        ]) ?>
                                    <?php } ?>
                                </div>
                                <div class="col-6">
                                    <i class="icon-parked-car"></i>
                                    <?php if (array_key_exists('garages', $property['attributes'])) { ?>
                                        <?= Yii::t('instance', '{garages, plural, one{# garage} other{# garages}}', [
                                            'garages' => $property['attributes']['garages']['value']
                                        ]) ?>
                                    <?php } else { ?>
                                        <?= Yii::t('instance', '{garages} garages', [
                                            'garages' => '?'
                                        ]) ?>
                                    <?php } ?>
                                </div>
                                <div class="col-6 lft-brdr"></div>
                                <div class="col-6 lft-brdr"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-12">
                            <div class="d-flex align-items-center prop-agent py-4">
                                <div class="img rounded-circle overflow-hidden">
                                    <?= Html::img($siteOwner['avatar'], [
                                        'alt' => $siteOwner['username'],
                                        'class' => 'mx-auto'
                                    ]) ?>
                                </div>
                                <div class="text">
                                    <div class="name font-weight-bold">
                                        <?= $siteOwner['username'] ?>
                                    </div>
                                    <div class="company grey">
                                        <?= Yii::t('instance', 'Agent') ?>
                                    </div>
                                </div>
                            </div>
                            <?php $form = ActiveForm::begin([
                                'action' => Url::to(['/contact/contact-agent']),
                                'id' => 'side-contact-agent-form',
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                            ]); ?>
                            <?= $form->field($contactAgentForm, 'name')->textInput([
                                'placeholder' => Yii::t('instance', 'Your Name'),
                            ])->label(false) ?>
                            <?= $form->field($contactAgentForm, 'phone')->textInput([
                                'placeholder' => Yii::t('instance', 'Your Phone'),
                            ])->label(false) ?>
                            <?= $form->field($contactAgentForm, 'email')->textInput([
                                'type' => 'email',
                                'placeholder' => Yii::t('instance', 'Your Email'),
                            ])->label(false) ?>
                            <?= $form->field($contactAgentForm, 'message')->textarea([
                                'class' => 'w-100',
                                'placeholder' => Yii::t('instance', 'Write Your Text'),
                            ])->label(false) ?>
                            <?= Html::submitInput(Yii::t('instance', 'Contact Agent'), [
                                'class' => 'button big orange w-100'
                            ]) ?>
                            <?php ActiveForm::end() ?>
                        </div>
                        <div class="col-12 col-md-6 col-lg-12">
                            <div class="property-add options px-0 mt-4 row border border-light rounded rounded-sm">
                                <div class="col-6">
                                    <a href="#"
                                       class="hint--top-right favorite d-inline-flex font-weight-normal m-0"
                                       data-hint="Add to bookmark">
                                        <i class="icon-heart grey"></i>&nbsp;
                                        <span><?= Yii::t('instance', 'Add to Favorite') ?></span>
                                    </a>
                                </div>
                                <div class="col-6">
                                    <?= Html::a('<i class="icon-plus-black-symbol"></i><span>' . Yii::t('instance', 'Compare') . '</span>', '#', [
                                        'class' => 'hint--top-right favorite d-inline-flex font-weight-normal m-0' . ($property['inCompare'] ? ' active' : ''),
                                        'data' => [
                                            'hint' => $property['inCompare'] ? Yii::t('instance', 'Remove from compare') : Yii::t('instance', 'Add to compare'),
                                            'hint-add' => Yii::t('instance', 'Add to compare'),
                                            'hint-remove' => Yii::t('instance', 'Remove from compare'),
                                            'action' => 'toggle-compare',
                                            'id' => $property['id']
                                        ]
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-12">
                            <p class="pt-2">
                                <?= Yii::t('instance', 'Property listed by {owner}', [
                                    'owner' => Html::a($property['user']['username'], '#')
                                ]) ?>
                            </p>
                        </div>
                        <div class="col-12">
                            <div class="mortgage">
                                <h4 class="text-left font-weight-bold">
                                    <?= Yii::t('instance', 'Mortgage') ?>
                                </h4>
                                <div class="mortgage-price" id="mortgages">
                                    <div class="d-flex">
                                        <span data-role="mortgage-payment"></span>&nbsp;/&nbsp;
                                        <span><?= Yii::t('instance', 'month') ?></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="calculator">
                                                <a href="#" class="button small green h-auto calculator-btn mt-3">
                                                    <?= Yii::t('instance', 'Mortgage calculator') ?>
                                                </a>

                                                <div class="calculator-modal">
                                                    <h4 class="text-center"><?= Yii::t('instance', 'Monthly Payment') ?></h4>
                                                    <div class="calc-sum text-center">
                                                        <span data-role="mortgage-payment"></span> <?= Yii::t('instance', 'per month') ?>
                                                    </div>
                                                    <div class="calc-warning text-center">
                                                        <?= Yii::t('instance', 'This is mortgage monthly payment with provided parameters') ?>
                                                    </div>
                                                    <div class="calc-preq text-center">
                                                        <p class="text-center">
                                                            <?= Yii::t('instance', 'If you want to buy with property using mortgage, begin with sending request with button below') ?>
                                                        </p>
                                                        <?= Html::a(Yii::t('instance', 'Send Request'), [
                                                            '/property/ajax/request-mortgage', 'entity_id' => $property['id']
                                                        ], [
                                                            'class' => 'button small green d-inline-block',
                                                            'data-action' => 'request-mortgage',
                                                            'data-target' => '#dynamic-modal',
                                                            'data-toggle' => 'modal'
                                                        ]) ?>
                                                    </div>
                                                    <div class="calculator-padding">
                                                        <form class="calculator-form">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="compact-mortgage-price"><?= Yii::t('instance', 'Property Price') ?></label>
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon"><?= Yii::$app->params['app_currency'] ?></span>
                                                                            <?= Html::input('number', 'mortgage-property-price', $property['raw_price'], [
                                                                                'data-role' => 'mortgage-price',
                                                                                'id' => 'compact-mortgage-price',
                                                                                'class' => 'form-control'
                                                                            ]) ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <div class="form-group">
                                                                        <label for="compact-mortgage-down-payment"><?= Yii::t('instance', 'Down Payment') ?></label>
                                                                        <div class="row m-0">
                                                                            <div class="col-8 p-0">
                                                                                <div class="input-group">
                                                                                    <?= Html::input('number', 'mortgage-down-payment', (int)($property['raw_price'] / 2), [
                                                                                        'data-role' => 'mortgage-down-payment',
                                                                                        'id' => 'compact-mortgage-down-payment',
                                                                                        'class' => 'form-control'
                                                                                    ]) ?>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-4 p-0">
                                                                                <div class="input-group">
                                                                                    <?= Html::input('number', 'mortgage-down-payment-percent', 50, [
                                                                                        'data-role' => 'mortgage-down-payment-percent',
                                                                                        'id' => 'compact-mortgage-down-payment-percent',
                                                                                        'class' => 'form-control'
                                                                                    ]) ?>
                                                                                    <span class="input-group-addon">%</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-sm-5">
                                                                    <div class="form-group">
                                                                        <label for="compact-mortgage-loan-term"><?= Yii::t('instance', 'Loan Term') ?></label>
                                                                        <?= Html::dropDownList('mortgage-loan-term', null, [
                                                                            5 => Yii::t('instance', '{years, plural, one{# year} other{# years}}', ['years' => 5]),
                                                                            10 => Yii::t('instance', '{years, plural, one{# year} other{# years}}', ['years' => 10]),
                                                                            15 => Yii::t('instance', '{years, plural, one{# year} other{# years}}', ['years' => 15]),
                                                                            20 => Yii::t('instance', '{years, plural, one{# year} other{# years}}', ['years' => 20]),
                                                                            30 => Yii::t('instance', '{years, plural, one{# year} other{# years}}', ['years' => 30]),
                                                                        ], ['data-role' => 'mortgage-loan-term', 'id' => 'compact-mortgage-loan-term']) ?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-7">
                                                                    <div class="form-group">
                                                                        <label for="compact-mortgage-interest-rate"><?= Yii::t('instance', 'Interest Rate') ?></label>
                                                                        <div class="input-group">
                                                                            <?= Html::input('number', 'mortgage-interest-rate', 6, [
                                                                                'id' => 'compact-mortgage-interest-rate',
                                                                                'data-role' => 'mortgage-interest-rate',
                                                                                'class' => 'form-control'
                                                                            ]) ?>
                                                                            <span class="input-group-addon">%</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <div class="chover">
                                                                            <input id="compact-mortgage-property-insurance"
                                                                                   data-role="mortgage-property-insurance"
                                                                                   class="radio-checkbox"
                                                                                   name="mortgage-property-insurance"
                                                                                   value="1"
                                                                                   type="checkbox">
                                                                            <label for="compact-mortgage-property-insurance">
                                                                                <?= Yii::t('instance', 'Include Insurance') ?>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <a href="#" class="button small green h-auto mt-3">
                                                <?= Yii::t('instance', 'Credit application') ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $markerPropertyIcon = Url::to(['/images/marker.png'], true);

$language = Yii::$app->language;
$currency = Yii::$app->params['app_currency'];
$price = $property['price'];

$script = <<<JS
    $(".slider-property").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        arrows: true,
        infinite: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    let mapInitialized = false;
    $('[data-action=init-gmap]').on('click', function() {
        if(mapInitialized === false) {
            let modalMap = new ModalMap({
                markerIcons: {
                    property: '{$markerPropertyIcon}'
                }
            });
            modalMap.init();
            modalMap.setMarker();
            
            mapInitialized = true;
        } 
    });
    
    /* Open calculator */
    $(document.body).on('click', '.calculator-btn', function() {
        $('.calculator-modal').toggleClass('open');
        return false;
    });

JS;

$this->registerJs($script);

if ($property['type'] === Property::TYPE_SALE) {
    $script = <<<JS
        var mortgageCalculator = new MortgageCalculator({
            locale: '{$language}',
            currency: '{$currency}'
        });
        mortgageCalculator.calculate();
        mortgageCalculator.registerHandlers();
JS;
}
$this->registerJs($script);

