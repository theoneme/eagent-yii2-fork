<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 19.04.2019
 * Time: 14:02
 */

namespace frontend\modules\instance\traits;

/**
 * Trait SettingValidatorsTrait
 * @package frontend\modules\instance\traits
 */
trait SettingValidatorsTrait
{
    /**
     * @param $attribute
     */
    public function categories($attribute)
    {
        if (empty($this->email) && empty($this->phone)) {
            $this->addError($attribute, Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => 'Email or phone']));
        }
    }
}