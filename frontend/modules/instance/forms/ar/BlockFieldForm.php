<?php

namespace frontend\modules\instance\forms\ar;

use common\behaviors\ImageBehavior;
use common\forms\ar\composite\MetaForm;
use common\forms\CompositeForm;
use frontend\modules\instance\models\InstanceBlockField;
use frontend\modules\instance\models\InstanceTranslation;
use Yii;

/**
 * Class InstanceForm
 *
 * @property MetaForm[] $meta
 *
 * @package frontend\modules\instance\forms\ar
 */
class BlockFieldForm extends CompositeForm
{
    /**
     * @var InstanceBlockField
     */
    public $_blockField;
    /**
     * @var integer
     */
    public $block_id;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var integer
     */
    public $site_id;

    /**
     * InstanceForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['block_id', 'type', 'site_id'], 'integer'],
            ['alias', 'string'],
            ['type', 'default', 'value' => InstanceBlockField::TYPE_STRING]
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'meta' => MetaForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_blockField->save(false);
    }

    /**
     * @param $blockFieldDTO
     * @return bool
     */
    public function prepareUpdate($blockFieldDTO)
    {
        $success = $this->load($blockFieldDTO, '');

        foreach ($blockFieldDTO['translations'] as $locale => $translation) {
            $this->meta[$locale]->load($translation, '');
        }

        return $success;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_blockField->load($this->attributes, '');
        foreach ($this->meta as $locale => $meta) {
            if (!empty($meta->title)) {
                $title = $this->_blockField->bind('translations');
                $title->attributes = [
                    'value' => $meta->title,
                    'locale' => $locale,
                    'key' => InstanceTranslation::KEY_TITLE,
                    'entity' => InstanceTranslation::ENTITY_FIELD
                ];
                if (in_array($this->type, [InstanceBlockField::TYPE_FILE])) {
                    $title->attachBehavior('imageBehavior', [
                        'class' => ImageBehavior::class,
                        'imageField' => 'value',
                        'saveToAws' => false,
                    ]);
                }
            }
        }

        return true;
    }
}