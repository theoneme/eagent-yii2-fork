<?php

namespace frontend\modules\instance\forms\ar;

use common\behaviors\ImageBehavior;
use common\forms\CompositeForm;
use common\helpers\Auth;
use frontend\modules\instance\forms\ar\composite\SettingForm;
use frontend\modules\instance\models\Instance;
use frontend\modules\instance\models\InstanceSetting;
use Yii;

/**
 * Class InstanceForm
 *
 * @property SettingForm[] $settings
 *
 * @package frontend\modules\instance\forms\ar
 */
class InstanceForm extends CompositeForm
{
    /**
     * @var Instance
     */
    public $_instance;
    /**
     * @var array
     */
    public $steps = [];
    /**
     * @var array
     */
    public $config = [];
    /**
     * @var integer
     */
    public $status;
    /**
     * @var string
     */
    public $alias;
    /**
     * @var array
     */
    private $oldBlocks = [];

    /**
     * InstanceForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $settings = [];
        foreach ($this->config as $setting) {
            $model = new SettingForm([
                'type' => $setting['type'],
                'options' => $setting['options'] ?? [],
                'value' => $setting['value'] ?? null,
                'key' => $setting['key'],
            ], ['rules' => $setting['rules'] ?? []]);
            if (array_key_exists('labels', $setting)) {
                $model->setLabels($setting['labels']);
            }

            $settings[$setting['key']] = $model;
        }
        $this->settings = $settings;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => Instance::STATUS_ACTIVE],
            ['alias', 'string', 'max' => 50],
//            ['alias', 'required'],
//            ['alias', 'unique', 'targetClass' => Instance::class, 'when' => function ($model) {
//                return $model->alias !== $this->_instance->alias;
//            }]
        ];
    }

    /**
     * @return array
     */
    protected function internalForms()
    {
        return array(
            'settings' => SettingForm::class,
        );
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'alias' => Yii::t('instance', 'Site Name'),
        ];
    }

    /**
     * @param bool $withValidation
     * @return bool
     */
    public function save($withValidation = true)
    {
        return $this->_instance->save(false);
    }

    /**
     * @param $instanceDTO
     * @return bool
     */
    public function prepareUpdate($instanceDTO)
    {
        $success = $this->load($instanceDTO, '');

        foreach ($instanceDTO['settings'] as $key => $setting) {
            $success = $success && $this->settings[$setting['key']]->load($setting, '');
        }
        $this->oldBlocks = $instanceDTO['blocks'];

        return $success;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function bindData()
    {
        $this->_instance->load($this->attributes, '');
        $this->_instance->user_id = Auth::user()->getCurrentId();

        foreach ($this->settings as $key => $setting) {
            if (!array_key_exists('value', $setting->attributes) || empty($setting['value'])) {
                continue;
            }

            $instanceSetting = $this->_instance->bind('instanceSettings');
            $instanceSetting->attributes = [
                'key' => $setting['key'],
                'type' => $setting['type'],
                'value' => is_array($setting['value']) ? json_encode($setting['value']) : $setting['value']
            ];

            if ($key === InstanceSetting::SETTING_BLOCKS) {
                foreach ($setting['value'] as $block) {
                    $oldId = array_search($block, $this->oldBlocks);
                    $instanceBlock = $this->_instance->bind('instanceBlocks', $oldId !== false ? $oldId : null);
                    $instanceBlock->attributes = [
                        'block_id' => $block,
                    ];
                }
            }

            if (in_array($key, [InstanceSetting::SETTING_FAVICON, InstanceSetting::SETTING_LOGO])) {
                $instanceSetting->attachBehavior('imageBehavior', [
                    'class' => ImageBehavior::class,
                    'imageField' => 'value',
                    'saveToAws' => false,
                ]);
            }
        }

        return true;
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}