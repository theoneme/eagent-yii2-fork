<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\modules\instance\forms\ar\composite;

use yii\base\Model;

/**
 * Class SettingForm
 * @package frontend\modules\instance\forms\ar\composite
 */
class SettingForm extends Model
{
    /**
     * @var array
     */
    protected $_labels = [];
    /**
     * @var array
     */
    protected $_config = [];
    /**
     * @var integer
     */
    public $key;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var mixed
     */
    public $value;
    /**
     * @var array
     */
    public $options = [];

    /**
     * SettingForm constructor.
     * @param array $rules
     * @param array $config
     */
    public function __construct(array $config = [], array $rules = [])
    {
        parent::__construct($config);
        $this->_config = $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [
            [['key', 'type'], 'required'],
        ];

        if (!empty($this->_config) && array_key_exists('rules', $this->_config)) {
            $rules = array_merge($rules, $this->_config['rules']);
        }

        return $rules;
    }

    /**
     * @param $labels
     */
    public function setLabels($labels)
    {
        $this->_labels = $labels;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return $this->_labels;
    }
}