<?php

namespace frontend\modules\instance\forms;

use Yii;
use yii\base\Model;

/**
 * Class ContactForm
 * @package frontend\forms
 */
class ContactForm extends Model
{
    /**
     * @var array
     */
    public $name;
    /**
     * @var string
     */
    public $subject;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $message;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['email'], 'email'],
            [['message'], 'string'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'subject' => Yii::t('instance', 'Subject'),
            'name' => Yii::t('instance', 'Your Name'),
            'message' => Yii::t('instance', 'Your Message'),
            'email' => Yii::t('instance', 'Your Email Address'),
        ];
    }
}
