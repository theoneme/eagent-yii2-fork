<?php

namespace frontend\modules\instance;

use frontend\modules\instance\components\CustomUrlRule;
use frontend\modules\instance\interfaces\repositories\BlockRepositoryInterface;
use frontend\modules\instance\interfaces\repositories\InstanceBlockFieldRepositoryInterface;
use frontend\modules\instance\interfaces\repositories\InstanceBlockRepositoryInterface;
use frontend\modules\instance\interfaces\repositories\InstanceRepositoryInterface;
use frontend\modules\instance\models\Block;
use frontend\modules\instance\models\Instance;
use frontend\modules\instance\models\InstanceBlock;
use frontend\modules\instance\models\InstanceBlockField;
use frontend\modules\instance\repositories\sql\BlockRepository;
use frontend\modules\instance\repositories\sql\InstanceBlockFieldRepository;
use frontend\modules\instance\repositories\sql\InstanceBlockRepository;
use frontend\modules\instance\repositories\sql\InstanceRepository;
use Yii;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

/**
 * Class Module
 * @package frontend\modules\instance
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\instance\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        Yii::$container->set(InstanceRepositoryInterface::class, InstanceRepository::class);
        Yii::$container->set(InstanceRepository::class, function () {
            return new InstanceRepository(new Instance());
        });
        Yii::$container->set(BlockRepositoryInterface::class, BlockRepository::class);
        Yii::$container->set(BlockRepository::class, function () {
            return new BlockRepository(new Block());
        });
        Yii::$container->set(InstanceBlockRepositoryInterface::class, InstanceBlockRepository::class);
        Yii::$container->set(InstanceBlockRepository::class, function () {
            return new InstanceBlockRepository(new InstanceBlock());
        });
        Yii::$container->set(InstanceBlockFieldRepositoryInterface::class, InstanceBlockFieldRepository::class);
        Yii::$container->set(InstanceBlockFieldRepository::class, function () {
            return new InstanceBlockFieldRepository(new InstanceBlockField());
        });

        Yii::$app->i18n->translations['instance*'] = [
            'class' => PhpMessageSource::class,
            'basePath' => '@frontend/modules/instance/messages',

            'fileMap' => [
                'instance' => 'instance.php',
            ],
        ];

        $app->getUrlManager()->addRules([
            [
                'class' => CustomUrlRule::class,
                'pattern' => '<site_alias:[\w-]+>-site',
                'route' => 'instance/site/index'
            ],
            [
                'class' => CustomUrlRule::class,
                'pattern' => '<site_alias:[\w-]+>-site/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>',
                'route' => 'instance/<controller>/<action>'
            ],
            [
                'class' => CustomUrlRule::class,
                'pattern' => '<site_alias:[\w-]+>-site/<controller:[\w-]+>/<action:[\w-]+>',
                'route' => 'instance/<controller>/<action>'
            ],
            [
                'class' => CustomUrlRule::class,
                'pattern' => '<site_alias:[\w-]+>-site/<entity:[\w-]+>/<controller:[\w-]+>/<action:[\w-]+>',
                'route' => 'instance/<entity>/<controller>/<action>'
            ],
        ], false);
    }
}
