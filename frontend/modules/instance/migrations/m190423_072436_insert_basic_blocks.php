<?php

use frontend\modules\instance\models\Block;
use yii\db\Migration;

/**
 * Class m190423_072436_insert_basic_blocks
 */
class m190423_072436_insert_basic_blocks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('site_block', ['alias', 'type'], [
           ['banner', Block::TYPE_BANNER],
           ['explore', Block::TYPE_EXPLORE],
           ['about', Block::TYPE_ABOUT],
           ['properties', Block::TYPE_PROPERTIES],
           ['team', Block::TYPE_TEAM],
           ['reviews', Block::TYPE_REVIEWS],
           ['contact', Block::TYPE_CONTACT],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('site_block');
    }
}
