<?php

use yii\db\Migration;

/**
 * Class m190424_125843_add_alias_to_site_instance
 */
class m190424_125843_add_alias_to_site_instance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('site_instance', 'alias', $this->string(50));
        $this->createIndex('index_site_instance_alias', 'site_instance', 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_instance', 'alias');
    }
}
