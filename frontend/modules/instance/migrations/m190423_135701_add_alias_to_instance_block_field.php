<?php

use yii\db\Migration;

/**
 * Class m190423_135701_add_alias_to_instance_block_field
 */
class m190423_135701_add_alias_to_instance_block_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('site_instance_block_field', 'alias', $this->string());
        $this->createIndex('index_site_instance_block_field_alias', 'site_instance_block_field', 'alias');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_instance_block_field', 'alias');
    }
}
