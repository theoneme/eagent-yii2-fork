<?php

use yii\db\Migration;

/**
 * Class m190417_074814_site_constructor
 */
class m190417_074814_site_constructor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('site_instance', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()
        ]);

        $this->createTable('site_instance_setting', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'key' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'value' => $this->binary()->notNull(),
        ]);
        $this->addForeignKey('fk_instance_setting_site_id_site', 'site_instance_setting', 'site_id', 'site_instance', 'id', 'CASCADE');

        $this->createTable('site_translation', [
            'id' => $this->primaryKey(),
            'entity' => $this->integer()->notNull(),
            'entity_id' => $this->integer()->notNull(),
            'key' => $this->integer()->notNull(),
            'value' => $this->text(),
            'locale' => $this->string(5)->notNull()
        ]);
        $this->createIndex('index_site_translation_entity_id_entity_locale', 'site_translation', ['entity_id', 'entity', 'locale']);

        $this->createTable('site_block', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(25)->notNull(),
            'type' => $this->integer()->notNull(),
        ]);

        $this->createTable('site_instance_block', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'block_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('site_instance_block_field', [
            'id' => $this->primaryKey(),
            'site_id' => $this->integer()->notNull(),
            'block_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('site_instance_block_field');
        $this->dropTable('site_instance_block');
        $this->dropTable('site_block');
        $this->dropTable('site_translation');
        $this->dropTable('site_instance_setting');
        $this->dropTable('site_instance');
    }
}
