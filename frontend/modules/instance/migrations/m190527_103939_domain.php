<?php

use yii\db\Migration;

/**
 * Class m190527_103939_domain
 */
class m190527_103939_domain extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('site_instance', 'domain', $this->string(55));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_instance', 'domain');
    }
}
