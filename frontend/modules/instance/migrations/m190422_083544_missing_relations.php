<?php

use yii\db\Migration;

/**
 * Class m190422_083544_missing_relations
 */
class m190422_083544_missing_relations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk_instance_block_site_id_site', 'site_instance_block', 'site_id', 'site_instance', 'id', 'CASCADE');
        $this->addForeignKey('fk_instance_block_block_id_block', 'site_instance_block', 'block_id', 'site_block', 'id', 'CASCADE');

        $this->addForeignKey('fk_instance_block_field_site_id_site', 'site_instance_block_field', 'site_id', 'site_instance', 'id', 'CASCADE');
        $this->addForeignKey('fk_instance_block_field_block_id_block', 'site_instance_block_field', 'block_id', 'site_instance_block', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_instance_block_site_id_site', 'site_instance_block');
        $this->dropForeignKey('fk_instance_block_block_id_block', 'site_instance_block');

        $this->dropForeignKey('fk_instance_block_field_site_id_site', 'site_instance_block_field');
        $this->dropForeignKey('fk_instance_block_field_block_id_block', 'site_instance_block_field');
    }
}
