<?php

namespace frontend\modules\instance\dto;

use common\interfaces\DTOInterface;

/**
 * Class BlockDTO
 * @package frontend\modules\instance\dto
 */
class BlockDTO implements DTOInterface
{
    /**
     * @var array
     */
    private $_block;

    /**
     * BlockDTO constructor.
     * @param array $block
     */
    public function __construct(array $block)
    {
        $this->_block = $block;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        return [
            'id' => $this->_block['id'],
            'alias' => $this->_block['alias'],
            'type' => $this->_block['type'],
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [
            'id' => $this->_block['id'],
            'alias' => $this->_block['alias'],
            'type' => $this->_block['type'],
        ];
    }
}