<?php

namespace frontend\modules\instance\dto;

use common\interfaces\DTOInterface;
use common\mappers\TranslationsMapper;
use frontend\modules\instance\models\InstanceBlockField;

/**
 * Class InstanceBlockFieldDTO
 * @package frontend\modules\instance\dto
 */
class InstanceBlockFieldDTO implements DTOInterface
{
    /**
     * @var InstanceBlockField
     */
    private $_instanceBlockField;

    /**
     * InstanceBlockFieldDTO constructor.
     * @param InstanceBlockField $instanceBlockField
     */
    public function __construct(InstanceBlockField $instanceBlockField)
    {
        $this->_instanceBlockField = $instanceBlockField;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $translations = TranslationsMapper::getMappedData($this->_instanceBlockField['translations'], TranslationsMapper::MODE_FULL);

        return [
            'id' => $this->_instanceBlockField['id'],
            'block_id' => $this->_instanceBlockField['block_id'],
            'site_id' => $this->_instanceBlockField['site_id'],
            'type' => $this->_instanceBlockField['type'],
            'alias' => $this->_instanceBlockField['alias'],
            'translations' => $translations,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $translations = TranslationsMapper::getMappedData($this->_instanceBlockField['translations']);

        return [
            'id' => $this->_instanceBlockField['id'],
//            'block_id' => $this->_instanceBlockField['block_id'],
//            'site_id' => $this->_instanceBlockField['site_id'],
            'type' => $this->_instanceBlockField['type'],
            'alias' => $this->_instanceBlockField['alias'],
            'translations' => $translations,
        ];
    }
}