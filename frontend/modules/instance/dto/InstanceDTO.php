<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:56
 */

namespace frontend\modules\instance\dto;

use common\interfaces\DTOInterface;
use frontend\modules\instance\models\InstanceSetting;
use yii\helpers\ArrayHelper;

/**
 * Class InstanceDTO
 * @package frontend\modules\instance\dto
 */
class InstanceDTO implements DTOInterface
{
    /**
     * @var array
     */
    private $_instance;

    /**
     * InstanceDTO constructor.
     * @param array $instance
     */
    public function __construct(array $instance)
    {
        $this->_instance = $instance;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $settings = ArrayHelper::map($this->_instance['instanceSettings'], 'key', function ($setting) {
            return [
                'type' => $setting['type'],
                'key' => $setting['key'],
                'value' => (int)$setting['type'] === InstanceSetting::SETTING_TYPE_ARRAY ? json_decode($setting['value'], true) : $setting['value']
            ];
        });
        $blocks = ArrayHelper::map($this->_instance['instanceBlocks'], 'id', 'block_id');

        return [
            'id' => $this->_instance['id'],
            'alias' => $this->_instance['alias'],
            'created_at' => $this->_instance['created_at'],
            'user_id' => $this->_instance['user_id'],
            'settings' => $settings,
            'blocks' => $blocks
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        return [

        ];
    }
}