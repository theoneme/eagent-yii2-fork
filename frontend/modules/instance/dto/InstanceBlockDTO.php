<?php

namespace frontend\modules\instance\dto;

use common\interfaces\DTOInterface;
use frontend\modules\instance\mappers\InstanceBlockFieldsMapper;
use frontend\modules\instance\models\InstanceBlock;

/**
 * Class InstanceBlockDTO
 * @package frontend\modules\instance\dto
 */
class InstanceBlockDTO implements DTOInterface
{
    /**
     * @var InstanceBlock
     */
    private $_instanceBlock;

    /**
     * InstanceBlockDTO constructor.
     * @param array $instanceBlock
     */
    public function __construct(array $instanceBlock)
    {
        $this->_instanceBlock = $instanceBlock;
    }

    /**
     * @param $mode
     * @return array
     */
    public function getData($mode = self::MODE_FULL)
    {
        if ($mode === self::MODE_SHORT) {
            return $this->getShortData();
        }

        if ($mode === self::MODE_FULL) {
            return $this->getFullData();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getFullData()
    {
        $fields = InstanceBlockFieldsMapper::getMappedData($this->_instanceBlock['fields']);

        return [
            'id' => $this->_instanceBlock['id'],
            'alias' => $this->_instanceBlock['block']['alias'],
            'type' => $this->_instanceBlock['block']['type'],
            'block_id' => $this->_instanceBlock['block_id'],
            'site_id' => $this->_instanceBlock['site_id'],
            'fields' => $fields,
        ];
    }

    /**
     * @return array
     */
    public function getShortData()
    {
        $fields = InstanceBlockFieldsMapper::getMappedData($this->_instanceBlock['fields']);

        return [
            'id' => $this->_instanceBlock['id'],
            'alias' => $this->_instanceBlock['block']['alias'],
            'type' => $this->_instanceBlock['block']['type'],
            'block_id' => $this->_instanceBlock['block_id'],
            'site_id' => $this->_instanceBlock['site_id'],
            'fields' => $fields,
            'data' => []
        ];
    }
}