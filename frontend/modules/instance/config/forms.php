<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.04.2019
 * Time: 14:57
 */

use common\models\Property;
use frontend\modules\instance\models\InstanceSetting;

return [
    'instance' => [
        'steps' => [
            1 => [
                'title' => Yii::t('instance', 'General Settings'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'general-step'
                    ],
                ]
            ],
            2 => [
                'title' => Yii::t('instance', 'View Info'),
                'config' => [
                    [
                        'type' => 'view',
                        'value' => 'view-step'
                    ],
                ]
            ],
        ],
        'settings' => [
            InstanceSetting::SETTING_LANGUAGES => [
                'key' => InstanceSetting::SETTING_LANGUAGES,
                'type' => InstanceSetting::SETTING_TYPE_ARRAY,
                'labels' => [
                    'value' => Yii::t('instance', 'Site Languages'),
                ],
                'options' => Yii::$app->params['languages'],
                'value' => [Yii::$app->language],
                'rules' => [
                    [['value'], 'each', 'rule' => ['in', 'range' => array_keys(Yii::$app->params['languages'])]],
                    [['value'], 'required']
                ]
            ],
            InstanceSetting::SETTING_OPERATIONS => [
                'key' => InstanceSetting::SETTING_OPERATIONS,
                'type' => InstanceSetting::SETTING_TYPE_ARRAY,
                'labels' => [
                    'value' => Yii::t('instance', 'Site Operations')
                ],
                'options' => [
                    Property::TYPE_SALE => Yii::t('instance', 'Sale'),
                    Property::TYPE_RENT => Yii::t('instance', 'Rent'),
                ],
                'rules' => [
                    [['value'], 'each', 'rule' => ['in', 'range' => [
                        Property::TYPE_SALE,
                        Property::TYPE_RENT
                    ]]],
                    [['value'], 'required']
                ]
            ],
            InstanceSetting::SETTING_LOCATIONS => [
                'key' => InstanceSetting::SETTING_LOCATIONS,
                'type' => InstanceSetting::SETTING_TYPE_ARRAY,
                'labels' => [
                    'value' => Yii::t('instance', 'Site Locations')
                ],
                'value' => [],
                'rules' => [
                    [['value'], 'safe'],
//                    [['value'], 'required']
                ]
            ],
            InstanceSetting::SETTING_BLOCKS => [
                'key' => InstanceSetting::SETTING_BLOCKS,
                'type' => InstanceSetting::SETTING_TYPE_ARRAY,
                'labels' => [
                    'value' => Yii::t('instance', 'Site Blocks')
                ],
                'value' => [],
                'options' => [],
                'rules' => [
                    [['value'], 'each', 'rule' => ['integer']],
                    [['value'], 'required']
                ]
            ],
            InstanceSetting::SETTING_CATEGORIES => [
                'key' => InstanceSetting::SETTING_CATEGORIES,
                'type' => InstanceSetting::SETTING_TYPE_ARRAY,
                'labels' => [
                    'value' => Yii::t('instance', 'Site Categories')
                ],
                'value' => [],
                'options' => [],
                'rules' => [
                    [['value'], 'each', 'rule' => ['integer']],
                    [['value'], 'required']
                ]
            ],
            InstanceSetting::SETTING_CONTACT_EMAIL => [
                'key' => InstanceSetting::SETTING_CONTACT_EMAIL,
                'type' => InstanceSetting::SETTING_TYPE_STRING,
                'labels' => [
                    'value' => Yii::t('instance', 'Specify contact email')
                ],
                'rules' => [
                    [['value'], 'string'],
                    [['value'], 'email'],
                ],
            ],
            InstanceSetting::SETTING_OTHER_LISTINGS => [
                'key' => InstanceSetting::SETTING_OTHER_LISTINGS,
                'type' => InstanceSetting::SETTING_TYPE_BOOLEAN,
                'labels' => [
                    'value' => Yii::t('instance', 'Show Other Listings')
                ],
                'options' => [
                    1 => Yii::t('instance', 'Yes'),
                    0 => Yii::t('instance', 'No'),
                ],
                'rules' => [
                    [['value'], 'boolean'],
                ]
            ],
            InstanceSetting::SETTING_NEW_CONSTRUCTION => [
                'key' => InstanceSetting::SETTING_NEW_CONSTRUCTION,
                'type' => InstanceSetting::SETTING_TYPE_BOOLEAN,
                'labels' => [
                    'value' => Yii::t('instance', 'Show New Constructions')
                ],
                'options' => [
                    1 => Yii::t('instance', 'Yes'),
                    0 => Yii::t('instance', 'No'),
                ],
                'rules' => [
                    [['value'], 'boolean'],
                ]
            ],
            InstanceSetting::SETTING_LOGO_TEXT => [
                'key' => InstanceSetting::SETTING_LOGO_TEXT,
                'type' => InstanceSetting::SETTING_TYPE_STRING,
                'labels' => [
                    'value' => Yii::t('instance', 'Specify logo text')
                ],
                'rules' => [
                    [['value'], 'string'],
                ],
                'value' => Yii::t('instance', 'Site Name'),
            ],
            InstanceSetting::SETTING_LOGO => [
                'key' => InstanceSetting::SETTING_LOGO,
                'type' => InstanceSetting::SETTING_TYPE_FILE,
                'rules' => [
                    [['value'], 'string'],
                ]
            ],
            InstanceSetting::SETTING_FAVICON => [
                'key' => InstanceSetting::SETTING_FAVICON,
                'type' => InstanceSetting::SETTING_TYPE_FILE,
                'rules' => [
                    [['value'], 'string'],
                ]
            ],
        ]
    ],
];
