<?php

namespace frontend\modules\instance\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class EditableAsset
 * @package frontend\modules\instance\assets
 */
class EditableAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/web';
    public $css = [
        'css/site/editable.css'
    ];
    public $js = [

    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
    ];
}