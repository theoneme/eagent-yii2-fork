<?php

namespace frontend\modules\instance\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class InstanceAsset
 * @package frontend\modules\instance\assets
 */
class InstanceAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/web';
    public $css = [

    ];
    public $js = [

    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
    ];
}