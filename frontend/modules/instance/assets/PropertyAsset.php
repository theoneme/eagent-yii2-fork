<?php

namespace frontend\modules\instance\assets;

use yii\web\AssetBundle;

/**
 * Class PropertyAsset
 * @package frontend\modules\instance\assets
 */
class PropertyAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/web';
    public $css = [
        'css/site/property.css'
    ];
    public $depends = [
        InstanceAsset::class
    ];
}