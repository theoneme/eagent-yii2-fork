<?php

namespace frontend\modules\instance\assets;

use yii\web\AssetBundle;

/**
 * Class IndexAsset
 * @package frontend\modules\instance\assets
 */
class IndexAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/web';
    public $css = [
        'css/site/index.css'
    ];
    public $js = [
        'js/popper.min.js'
    ];
    public $depends = [
        InstanceAsset::class
    ];
}