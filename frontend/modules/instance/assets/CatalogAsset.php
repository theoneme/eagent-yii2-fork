<?php

namespace frontend\modules\instance\assets;

use yii\web\AssetBundle;

/**
 * Class IndexAsset
 * @package frontend\modules\instance\assets
 */
class CatalogAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/web';
    public $css = [
        'css/site/catalog.css'
    ];
    public $js = [

    ];
    public $depends = [
        InstanceAsset::class
    ];
}