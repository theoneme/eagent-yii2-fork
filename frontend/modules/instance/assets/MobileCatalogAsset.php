<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 03.06.2019
 * Time: 13:47
 */

namespace frontend\modules\instance\assets;

use yii\web\AssetBundle;

/**
 * Class MobileCatalogAsset
 * @package frontend\modules\instance\assets
 */
class MobileCatalogAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/web';
    public $css = [
        'css/site/mobile-catalog.css'
    ];
    public $js = [

    ];
    public $depends = [
        InstanceAsset::class
    ];
}