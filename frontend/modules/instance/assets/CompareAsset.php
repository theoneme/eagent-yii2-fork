<?php

namespace frontend\modules\instance\assets;

use yii\web\AssetBundle;

/**
 * Class CompareAsset
 * @package frontend\modules\instance\assets
 */
class CompareAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/web';
    public $css = [
        'css/site/compare.css'
    ];
    public $js = [
        'js/popper.min.js'
    ];
    public $depends = [
        InstanceAsset::class
    ];
}