<?php

namespace frontend\modules\instance\controllers;

use common\helpers\FileHelper;
use common\mappers\forms\RequestResetFormMapper;
use common\models\auth\LoginForm;
use common\models\User;
use common\models\UserSocial;
use common\repositories\sql\UserRepository;
use common\repositories\sql\UserSocialRepository;
use common\services\entities\UserService;
use common\services\NotificationService;
use frontend\mappers\social\SocialAttributesMapper;
use frontend\models\auth\RequestResetForm;
use frontend\models\auth\ResetPasswordForm;
use frontend\models\auth\SignupForm;
use Yii;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\base\InvalidParamException;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AuthController
 * @package frontend\controllers
 */
class AuthController extends InstanceBaseController
{
    /**
     * @var UserRepository
     */
    private $_userRepository;
    /**
     * @var UserSocialRepository
     */
    private $_userSocialRepository;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var NotificationService
     */
    private $_notificationService;

    /**
     * AuthController constructor.
     * @param string $id
     * @param Module $module
     * @param UserRepository $userRepository
     * @param UserSocialRepository $userSocialRepository
     * @param UserService $userService
     * @param NotificationService $notificationService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                UserRepository $userRepository,
                                UserSocialRepository $userSocialRepository,
                                UserService $userService,
                                NotificationService $notificationService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userRepository = $userRepository;
        $this->_userSocialRepository = $userSocialRepository;
        $this->_userService = $userService;
        $this->_notificationService = $notificationService;
    }

    /**
     * @return array
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'social' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'onSocialSuccess'],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup', 'detach'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'detach'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array|string|Response
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $loginForm = new LoginForm($this->_userRepository);
        $input = Yii::$app->request->post();
        if(!empty($input)) {
            $loginForm->load($input);

            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($loginForm);
            }

            if ($loginForm->validate() && $loginForm->login()) {
                return $this->redirect(Url::previous());
            }
        }

        return $this->render('login', [
            'loginForm' => $loginForm
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return array|string|Response
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSignup()
    {
        $loginForm = new LoginForm($this->_userRepository);
        $signupForm = new SignupForm($this->_userService);
        $input = Yii::$app->request->post();

        if(!empty($input)) {
            $signupForm->load($input);

            $loginForm->load($signupForm->attributes, '');
            if ($loginForm->validate() && $loginForm->login()) {
                return $this->redirect(Url::previous());
            }

            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($signupForm);
            }

            if (!empty($input) && $signupForm->validate() && $signupForm->signup()) {
                $loginForm = new LoginForm($this->_userRepository);
                $loginForm->setAttributes($signupForm->attributes);
                $loginForm->login();

                return $this->redirect(Url::previous());
            }
        }

        return $this->render('signup', [
            'signupForm' => $signupForm
        ]);
    }

    /**
     * @return array|Response
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionRequestReset()
    {
        $input = Yii::$app->request->post();
        if(!empty($input)) {
            /** @var RequestResetForm $requestResetForm */
            $requestResetForm = new RequestResetForm($this->_userRepository);
            $requestResetForm->load($input);

            $user = $this->_userRepository->findOneByCriteria(['email' => $requestResetForm->email]);
            if($user === null) {
                return $this->redirect(Url::previous());
            }

            $requestResetForm->_user = $user;

            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($requestResetForm);
            }

            if ($requestResetForm->validate() && $requestResetForm->requestReset()) {
                $params = RequestResetFormMapper::getMappedData([
                    'attributes' => $requestResetForm->attributes,
                    'user' => $user
                ]);

                $this->_notificationService->sendNotification('email', $params);
            }
        }

        return $this->redirect(Url::previous());
    }

    /**
     * @param $token
     * @return array|string|Response
     * @throws BadRequestHttpException
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionResetPassword($token)
    {
        try {
            $resetPasswordForm = new ResetPasswordForm($this->_userRepository, $token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $input = Yii::$app->request->post();
        if(!empty($input)) {
            $resetPasswordForm->load($input);

            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($resetPasswordForm);
            }

            if (!empty($input) && $resetPasswordForm->validate() && $resetPasswordForm->resetPassword()) {
                return $this->redirect(Url::previous());
            }
        }

        return $this->render('reset-password', [
            'resetPasswordForm' => $resetPasswordForm
        ]);
    }

    /**
     * @param ClientInterface $client
     */
    public function onSocialSuccess(ClientInterface $client)
    {
        $attributes = $client->getUserAttributes();
        $mappedAttributes = SocialAttributesMapper::getMappedData([
            'attributes' => $attributes,
            'provider' => $client->getName()
        ]);

        if (!empty($mappedAttributes['image'])) {
            $pathInfo = FileHelper::createTempDirectory('@frontend');
            $filename = uniqid('social', true) . '.jpg';
            $newAbsolutePath = $pathInfo['basePath'] . $filename;
            $newRelativePath = $pathInfo['relativePath'] . $filename;

            $file = file_get_contents($mappedAttributes['image']);
            if ($file) {
                file_put_contents($newAbsolutePath, $file);
                $mappedAttributes['image'] = $newRelativePath;
            }
        }

        /** @var UserSocial $social */
        $social = $this->_userSocialRepository->with(['user'])->findOneByCriteria([
            'source' => $client->getId(),
            'source_id' => $mappedAttributes['id'],
        ]);

        if (Yii::$app->user->isGuest) {
            if ($social !== null) {
                $user = $social->user;
                Yii::$app->user->login($user);
            } else {
                $user = $this->_userRepository->findOneByCriteria(['email' => $mappedAttributes['email']]);

                if ($user !== null && isset($mappedAttributes['email'])) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('main', 'User with such email as from {client} already exists', ['client' => $client->getTitle()]),
                    ]);
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    $userId = $this->_userRepository->create([
                        'UserForm' => [
                            'email' => $mappedAttributes['email'],
                            'username' => $mappedAttributes['username'],
                            'avatar' => $mappedAttributes['image'],
                        ],
                        'PasswordForm' => [
                            'password' => $password,
                            'confirm' => $password
                        ]
                    ]);

                    if ($userId) {
                        $userSocial = $this->_userSocialRepository->create([
                            'user_id' => $userId,
                            'source' => $client->getId(),
                            'source_id' => (string)$mappedAttributes['id'],
                            'data' => json_encode($mappedAttributes)
                        ]);
                        if ($userSocial->id !== null) {
                            /** @var User $user */
                            $user = $this->_userRepository->findOneByCriteria(['id' => $userId]);
                            Yii::$app->user->login($user);
                        } else {
                            Yii::error($user->getErrors());
                        }
                    } else {
                        Yii::error($user->getErrors());
                    }
                }
            }
        } else if (!$social) {
            $this->_userSocialRepository->create([
                'user_id' => Yii::$app->user->id,
                'source' => $client->getId(),
                'source_id' => $mappedAttributes['id'],
                'data' => json_encode($mappedAttributes)
            ]);
        }
    }

    /**
     * @param $provider
     * @return Response
     */
    public function actionDetach($provider)
    {
        $this->_userSocialRepository->deleteOneByCriteria(['user_id' => Yii::$app->user->getId(), 'source' => $provider]);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
