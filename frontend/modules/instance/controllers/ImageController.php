<?php

namespace frontend\modules\instance\controllers;

use common\controllers\FrontEndController;
use Yii;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class ImageController
 * @package frontend\modules\instance\controllers
 */
class ImageController extends FrontEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['upload'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['upload'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionUpload()
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $response = [];
        $imageFiles = UploadedFile::getInstancesByName('uploaded_images');
        $post = Yii::$app->request->post();
        $path = '/uploads/temp/' . $year . '/' . $month . '/' . $day . '/';
        $dir = Yii::getAlias('@webroot') . $path;
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
            }
        }
        foreach ($imageFiles as $file) {
            try {
                $sourceName = $file->name ?? 'unknown';
                $hash = uniqid('', true);
                $previewName = $name = $path . $hash . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@webroot') . $name);
                if (!in_array($file->extension, ['jpg', 'gif', 'png', 'jpeg'])) {
                    $previewName = '/images/attachment-icon.png';
                }

                $response = [
                    'initialPreview' => [$previewName],
                    'initialPreviewConfig' => [
                        [
                            'caption' => basename($name),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => 'image_new_' . $post['fileindex'],
                        ]
                    ],
                    'initialPreviewThumbTags' => [
                        '{actions}' => '{actions}'
                    ],
                    'uploadedPath' => $name,
                    'imageKey' => 'image_new_' . $post['fileindex'],
                ];
            } catch (\Exception $e) {
                $response = [
                    'error' => "File {$sourceName} is corrupted"
                ];
            }
        }

        return $response;
    }

    /**
     * @return bool
     */
    public function actionDelete()
    {
        // TODO Придумать нормальную логику удаления
        return true;
    }
}