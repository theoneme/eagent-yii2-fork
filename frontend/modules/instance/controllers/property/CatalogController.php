<?php

namespace frontend\modules\instance\controllers\property;

use common\services\entities\CategoryService;
use common\services\MetaService;
use frontend\helpers\ViewToTemplateHelper;
use frontend\modules\instance\controllers\InstanceBaseController;
use frontend\services\catalog\PropertyCatalogDataService;
use frontend\services\catalog\PropertyCatalogUrlService;
use frontend\widgets\PropertyFilter;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Cookie;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\modules\instance\controllers\property
 */
class CatalogController extends InstanceBaseController
{
    /**
     * @var PropertyCatalogDataService
     */
    private $_propertyCatalogDataService;
    /**
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * @var PropertyCatalogUrlService
     */
    private $_propertyCatalogUrlService;

    /**
     * CatalogController constructor.
     * @param $id
     * @param Module $module
     * @param PropertyCatalogDataService $propertyCatalogDataService
     * @param CategoryService $categoryService
     * @param PropertyCatalogUrlService $propertyCatalogUrlService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PropertyCatalogDataService $propertyCatalogDataService,
                                CategoryService $categoryService,
                                PropertyCatalogUrlService $propertyCatalogUrlService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyCatalogDataService = $propertyCatalogDataService;
        $this->_categoryService = $categoryService;
        $this->_propertyCatalogUrlService = $propertyCatalogUrlService;

        $this->layout = 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
            ]
        ];
    }

    /**
     * @param $category
     * @param $operation
     * @return array|string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return $this->redirect(Url::current(['category' => $categoryItem['slug']]));
        }

        $queryParams = Yii::$app->request->queryParams;
//        $queryParams['db-sql'] = true;

        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'map');
        $template = ViewToTemplateHelper::getTemplate($cookieView);
        if ($cookieView !== 'map' && array_key_exists('box', $queryParams) && array_key_exists('zoom', $queryParams)) {
            $template = 'index-map';
            $this->actionView('map');
        }

        $catalogData = $this->_propertyCatalogDataService->getData($queryParams, $categoryItem, $cookieView, PropertyCatalogDataService::MODE_INDEX);
        $catalogData['propertyFilter']->template = '@frontend/modules/instance/widgets/views/property-filter';

        $metaService = new MetaService($this);
        $metaService->registerMeta($catalogData['seo']);

        return $this->render($template, $catalogData);
    }

    /**
     * @param $category
     * @param $operation
     * @return array
     */
    public function actionIndexAjax($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return [
                'success' => false
            ];
        }

        $queryParams = Yii::$app->request->queryParams;
//        $queryParams['db-sql'] = true;
        $seoUrl = $this->_propertyCatalogUrlService->getUrl($queryParams, ['/instance/property/catalog/index']);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'map');
        $catalogData = $this->_propertyCatalogDataService->getData($queryParams, $categoryItem, $cookieView, PropertyCatalogDataService::MODE_INDEX_AJAX);
        $markers = ArrayHelper::remove($catalogData, 'markers');
        $seo = $catalogData['seo'];
        /** @var PropertyFilter $filter */
        $filter = ArrayHelper::remove($catalogData, 'propertyFilter');
        $filter->template = '@frontend/modules/instance/widgets/views/property-filter';

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'seo' => $seo,
            'domain' => array_key_exists('city', $queryParams),
            'markers' => $markers['items'],
            'polygon' => $catalogData['polygon'],
            'filter' => $filter->run(),
            'url' => $seoUrl,
            'success' => true
        ];
    }

    /**
     * @param $category
     * @param $operation
     * @return array
     */
    public function actionIndexAjaxShort($category, $operation)
    {
        $categoryItem = $this->_categoryService->getOne(['category_translations.value' => $category, 'category_translations.key' => 'slug']);
        if ($categoryItem['slug'] !== $category) {
            return [
                'success' => false
            ];
        }

        $queryParams = Yii::$app->request->queryParams;
//        $queryParams['db-sql'] = true;
        $seoUrl = $this->_propertyCatalogUrlService->getUrl($queryParams, ['/instance/property/catalog/index']);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'map');
        $catalogData = $this->_propertyCatalogDataService->getData($queryParams, $categoryItem, $cookieView, PropertyCatalogDataService::MODE_INDEX);

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'success' => true,
            'url' => $seoUrl
        ];
    }

    /**
     * @return Response
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionSearch()
    {
        return $this->redirect($this->_propertyCatalogUrlService->getUrl(Yii::$app->request->queryParams));
    }

    /**
     * @param $view
     * @return array
     */
    public function actionView($view)
    {
        if (!in_array($view, ['grid', 'list', 'map'])) {
            return ['success' => false];
        }

        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'catalog-view',
            'value' => $view,
            'domain' => '.' . Yii::$app->params['domainName'],
            'expire' => time() + 3600 * 6,
        ]));

        return ['success' => true];
    }
}
