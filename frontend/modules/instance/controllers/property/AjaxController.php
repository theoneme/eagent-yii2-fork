<?php

namespace frontend\modules\instance\controllers\property;

use common\models\Property;
use common\services\entities\PropertyService;
use frontend\modules\instance\controllers\InstanceBaseController;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\modules\instance\controllers\property
 */
class AjaxController extends InstanceBaseController
{
    /**
     * @var PropertyService
     */
    private $_propertyService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param PropertyService $propertyService
     * @param array $config
     */
    public function __construct(string $id, Module $module, PropertyService $propertyService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyService = $propertyService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['building-properties'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['building-properties'],
            ]
        ];
    }

    /**
     * @param $operation
     * @param $building_id
     * @param $rooms
     * @return array
     */
    public function actionBuildingProperties($operation, $building_id, $rooms)
    {
        $params = Yii::$app->request->queryParams;
//        $params['category_id'] = 2;
        $params['ads_allowed'] = true;
        $params['status'] = Property::STATUS_ACTIVE;
        $properties = $this->_propertyService->getMany($params, [
            'pagination' => true,
            'perPage' => 10,
            'orderBy' => ['id' => SORT_DESC],
        ]);

        return [
            'html' => $this->renderAjax('building-properties', [
                'properties' => $properties
            ]),
            'success' => true,
        ];
    }
}
