<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 20:16
 */

namespace frontend\modules\instance\controllers\property;

use common\mappers\SimilarPropertiesParamsMapper;
use common\models\Property;
use common\models\Translation;
use common\repositories\elastic\PropertyRepository;
use common\services\entities\BuildingService;
use common\services\entities\CityService;
use common\services\entities\PropertyService;
use common\services\entities\UserService;
use common\services\MetaService;
use common\services\seo\PropertySeoService;
use frontend\forms\contact\ContactAgentForm;
use frontend\modules\instance\controllers\InstanceBaseController;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Class PropertyController
 * @package frontend\modules\instance\controllers\property
 */
class PropertyController extends InstanceBaseController
{
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var PropertySeoService
     */
    private $_propertySeoService;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;

    /**
     * PropertyController constructor.
     * @param string $id
     * @param Module $module
     * @param PropertyService $propertyService
     * @param BuildingService $buildingService
     * @param PropertySeoService $propertySeoService
     * @param UserService $userService
     * @param \common\repositories\sql\PropertyRepository $propertyRepository
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PropertyService $propertyService,
                                BuildingService $buildingService,
                                PropertySeoService $propertySeoService,
                                UserService $userService,
                                \common\repositories\sql\PropertyRepository $propertyRepository,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyService = $propertyService;
        $this->_buildingService = $buildingService;
        $this->_userService = $userService;
        $this->_propertySeoService = $propertySeoService;
        $this->_propertyRepository = $propertyRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'update'],
                'rules' => [
                    [
                        'actions' => ['create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param $slug
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        $property = $this->_propertyService->getOne(['and',
            ['property_translations.value' => $slug],
            ['property_translations.key' => Translation::KEY_SLUG],
            ['not', ['property.status' => Property::STATUS_DELETED]]
        ]);

        if ($property === null) {
            throw new NotFoundHttpException(Yii::t('instance', '{entity} not found', ['entity' => 'Property']));
        }

        $siteOwner = $this->_userService->getOne(['id' => $this->currentInstance['user_id']]);

        if ($this->_propertyRepository->inc($property['id'], 'views_count')) {
            $property['views_count']++;
        }

        $seoParams = array_map(function ($var) {
            return $var['value'];
        }, $property['attributes']);
        $seoParams['type'] = $property['type'];
        $seoParams['address'] = $property['address'];
        if ($property['locale'] === Yii::$app->language) {
            $seoParams['saddress'] = $property['address'];
        } else if (!empty($property['city_id'])) {
            /* @var CityService $cityService */
            $cityService = Yii::$container->get(CityService::class);
            $city = $cityService->getOne(['id' => $property['city_id']]);
            if ($city) {
                $seoParams['saddress'] = $city['title'];
            }
        }
        $seoParams['city'] = $property['addressData']['city'] ?? '';
        $seoParams['price'] = $property['raw_price'];
        $seoParams['category'] = $property['category'][Translation::KEY_ADDITIONAL_TITLE] ?? $property['category'][Translation::KEY_TITLE] ?? '';
        $seo = $this->_propertySeoService->getSeo($property['categorySeoTemplate'], $seoParams);
        $metaService = new MetaService($this);
        $seo['image'] = $property['image'];
        $seo['description'] = !empty($property['description']) ? $property['description'] : $seo['description'];
        $metaService->registerMeta($seo);

        $building = $this->_buildingService->getOne(['building.id' => $property['building_id']]);

//        $this->_propertyService->setRepository($this->_propertyRepository);
        $similarParams = SimilarPropertiesParamsMapper::getMappedData($property);
        $similarProperties = $this->_propertyService->getMany($similarParams, [
            'limit' => 3,
            'exclude' => $property['id'],
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $this->layout = 'catalog';
        $categorySlugs = array_map(
            function ($var) {
                return $var[Yii::$app->language] ?? null;
            },
            Yii::$app->cacheLayer->getCategoryAliasCache('ru-RU')
        );

        return $this->render('view', [
            'siteOwner' => $siteOwner,
            'property' => $property,
            'seo' => $seo,
            'contactAgentForm' => new ContactAgentForm(),
            'building' => $building,
            'similarProperties' => $similarProperties['items'],
            'catalogCategory' => $categorySlugs['flats'] ?? 'flats'
        ]);
    }
}