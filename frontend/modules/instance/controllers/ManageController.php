<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.11.2018
 * Time: 17:23
 */

namespace frontend\modules\instance\controllers;

use common\controllers\FrontEndController;
use common\helpers\Auth;
use common\helpers\DataHelper;
use frontend\modules\instance\forms\ar\composite\SettingForm;
use frontend\modules\instance\forms\ar\InstanceForm;
use frontend\modules\instance\helpers\FormHelper;
use frontend\modules\instance\interfaces\repositories\InstanceRepositoryInterface;
use frontend\modules\instance\models\Instance;
use frontend\modules\instance\models\InstanceSetting;
use frontend\modules\instance\services\entities\InstanceService;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ManageController
 * @package frontend\modules\instance\controllers\instance
 */
class ManageController extends FrontEndController
{
    public const ACTION_CREATE = 0;
    public const ACTION_UPDATE = 1;

    /**
     * @var InstanceService
     */
    private $_instanceService;
    /**
     * @var InstanceRepositoryInterface
     */
    private $_instanceRepository;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * ManageController constructor.
     * @param $id
     * @param Module $module
     * @param InstanceRepositoryInterface $instanceRepository
     * @param InstanceService $instanceService
     * @param LocationService $locationService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                InstanceRepositoryInterface $instanceRepository,
                                InstanceService $instanceService,
                                LocationService $locationService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_instanceService = $instanceService;
        $this->_instanceRepository = $instanceRepository;
        $this->_locationService = $locationService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-location'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['render-location'],
            ]
        ]);
    }

    /**
     * @return mixed|string|Response
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_INSTANCE);
        $languages = $formConfig['settings'][InstanceSetting::SETTING_LANGUAGES];
        $formConfig['settings'][InstanceSetting::SETTING_LANGUAGES]['options'] = \frontend\modules\instance\helpers\DataHelper::sortLanguages($languages['options'], $languages['value']);
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $formConfig['settings'][InstanceSetting::SETTING_LOCATIONS]['value'][] = \frontend\modules\instance\helpers\DataHelper::getLocationData($locationData);
        $formConfig['settings'][InstanceSetting::SETTING_NEW_CONSTRUCTION]['value'] = 1;
        $settingsConfig = $formConfig['settings'];

        $categories = DataHelper::getCategories();
        $blocks = \frontend\modules\instance\helpers\DataHelper::getBlocks();
        $settingsConfig[InstanceSetting::SETTING_CATEGORIES]['options'] = $categories;
        $settingsConfig[InstanceSetting::SETTING_BLOCKS]['options'] = $blocks;

        $instanceForm = new InstanceForm([
            '_instance' => new Instance(),
            'config' => $settingsConfig
        ]);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $instanceForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($instanceForm);
            }

            if ($instanceForm->validate()) {
                $instanceForm->bindData();

                if ($instanceForm->save()) {
                    return $this->redirect(['/instance/site/edit', 'site_alias' => $instanceForm->_instance->id]);
                }
            }
        }

        $instanceForm->buildLayout($formConfig['steps']);

        return $this->render('form', [
            'instanceForm' => $instanceForm,
            'route' => ['/instance/manage/create'],
            'action' => self::ACTION_CREATE
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_INSTANCE);
        $instanceDTO = $this->_instanceService->getOne(['id' => $id]);
        $languages = $formConfig['settings'][InstanceSetting::SETTING_LANGUAGES];
        $formConfig['settings'][InstanceSetting::SETTING_LANGUAGES]['options'] = \frontend\modules\instance\helpers\DataHelper::sortLanguages($languages['options'], $instanceDTO['settings'][InstanceSetting::SETTING_LANGUAGES]['value'] ?? []);
        $settingsConfig = $formConfig['settings'];

        $categories = DataHelper::getCategories();
        $blocks = \frontend\modules\instance\helpers\DataHelper::getBlocks();
        $settingsConfig[InstanceSetting::SETTING_CATEGORIES]['options'] = $categories;
        $settingsConfig[InstanceSetting::SETTING_BLOCKS]['options'] = $blocks;

        /** @var Instance $instance */
        $instance = $this->_instanceRepository->findOneByCriteria(['id' => $id]);
        if ($instance === null || ((int)$instance['user_id'] !== Auth::user()->getCurrentId())) {
            throw new NotFoundHttpException('Instance Not Found');
        }

        $instanceForm = new InstanceForm([
            '_instance' => $instance,
            'config' => $settingsConfig
        ]);

        $instanceForm->prepareUpdate($instanceDTO);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $instanceForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($instanceForm);
            }

            if ($instanceForm->validate()) {
                $instanceForm->bindData();
                if ($instanceForm->save()) {
                    return $this->redirect(['/instance/site/edit', 'site_alias' => $instanceForm->_instance->id]);
                }
            }
        }

        $instanceForm->buildLayout($formConfig['steps']);

        return $this->render('form', [
            'instanceForm' => $instanceForm,
            'categories' => $categories,
            'route' => ['/instance/manage/update', 'id' => $id],
            'action' => self::ACTION_UPDATE
        ]);
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderLocation($iterator)
    {
        $locationForm = new SettingForm([
            'type' => InstanceSetting::SETTING_LOCATIONS
        ]);

        return [
            'html' => $this->renderAjax('location', [
                'locationForm' => $locationForm,
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }
}