<?php

namespace frontend\modules\instance\controllers;

use common\helpers\Auth;
use frontend\modules\instance\models\Block;
use frontend\modules\instance\models\InstanceSetting;
use frontend\modules\instance\services\entities\InstanceBlockService;
use frontend\modules\instance\services\InstanceBlockDataService;
use Yii;
use yii\base\Module;
use yii\helpers\Html;
use yii\web\Response;

/**
 * Class SiteController
 * @package frontend\modules\instance\controllers
 */
class SiteController extends InstanceBaseController
{
    /**
     * @var InstanceBlockService
     */
    private $_instanceBlockService;
    /**
     * @var InstanceBlockDataService
     */
    private $_instanceBlockDataService;

    /**
     * SiteController constructor.
     * @param string $id
     * @param Module $module
     * @param InstanceBlockService $instanceBlockService
     * @param InstanceBlockDataService $instanceBlockDataService
     * @param array $config
     */
    public function __construct($id, Module $module, InstanceBlockService $instanceBlockService, InstanceBlockDataService $instanceBlockDataService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_instanceBlockService = $instanceBlockService;
        $this->_instanceBlockDataService = $instanceBlockDataService;
    }

    /**
     * @return mixed|string|Response
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $this->layout = 'main';
        $blocks = $this->_instanceBlockService->getMany(['site_id' => $this->currentInstance['id']], ['orderBy' => ['site_block.type' => SORT_ASC]]);
        foreach ($blocks['items'] as $key => $block) {
            $blocks['items'][$key]['data'] = $this->_instanceBlockDataService->getData($this->currentInstance, $block['type']);
        }

        return $this->render('index', ['blocks' => $blocks['items']]);
    }

    /**
     * @return mixed|string|Response
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionEdit()
    {
        if (Yii::$app->user->isGuest || ((int)$this->currentInstance['user_id'] !== Auth::user()->getCurrentId())) {
            return $this->redirect(['/instance/site/index', 'site_alias' => $this->currentInstance['id']]);
        }
        $this->layout = 'main-editable';
        $blocks = $this->_instanceBlockService->getMany(['site_id' => $this->currentInstance['id']], ['orderBy' => ['site_block.type' => SORT_ASC]]);
        foreach ($blocks['items'] as $key => $block) {
            $blocks['items'][$key]['data'] = $this->_instanceBlockDataService->getData($this->currentInstance, $block['type']);
            if ((int)$block['type'] === Block::TYPE_CONTACT) {
                $hasContactsBlock = true;
            }
        }
        if (($hasContactsBlock ?? false) && empty($this->currentInstance['settings'][InstanceSetting::SETTING_CONTACT_EMAIL]['value'] ?? null)) {
            Yii::$app->session->setFlash('warning', Yii::t('instance', 'Contact form will not work unless you specify contact email. {link}', [
                'link' => Html::a(Yii::t('instance', 'Go to settings'), ['/instance/manage/update', 'id' => $this->currentInstance['id']])
            ]));
        }

        return $this->render('index-editable', ['blocks' => $blocks['items']]);
    }

    /**
     * @return string
     */
    public function actionCatalogMobile()
    {
        $this->layout = 'catalog-mobile';
        return $this->render('catalog-mobile');
    }
}