<?php

namespace frontend\modules\instance\controllers;

use common\services\NotificationService;
use frontend\modules\instance\forms\ContactForm;
use frontend\modules\instance\mappers\ContactFormToMessageMapper;
use frontend\modules\instance\models\InstanceSetting;
use Yii;
use yii\base\Module;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AjaxController
 * @package frontend\controllers
 */
class ContactController extends InstanceBaseController
{
    /**
     * @var NotificationService
     */
    private $_notificationService;

    /**
     * ContactController constructor.
     * @param string $id
     * @param Module $module
     * @param NotificationService $notificationService
     * @param array $config
     */
    public function __construct(string $id, Module $module, NotificationService $notificationService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_notificationService = $notificationService;
    }

    /**
     * @return array|Response
     */
    public function actionSend()
    {
        $form = new ContactForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        if (array_key_exists('ajax', $input)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($form);
        }

        if ($form->validate()) {
            $params = ContactFormToMessageMapper::getMappedData([
                'attributes' => $form->attributes,
                'to' => $this->currentInstance['settings'][InstanceSetting::SETTING_CONTACT_EMAIL]['value'] ?? '',
                'currentInstance' => $this->currentInstance,
            ]);
            $this->_notificationService->sendNotification('email', $params);
        }

        return $this->redirect(['/instance/site/index']);
    }
}
