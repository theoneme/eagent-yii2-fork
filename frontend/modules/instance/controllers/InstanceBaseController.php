<?php

namespace frontend\modules\instance\controllers;

use common\controllers\FrontEndController;
use frontend\forms\LocationForm;
use frontend\modules\instance\services\entities\InstanceService;
use frontend\services\LocationService;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class InstanceBaseController
 * @package frontend\modules\instance\controllers
 */
class InstanceBaseController extends FrontEndController
{
    /**
     * @var string
     */
    public $layout = 'main';
    /**
     * @var array
     */
    public $currentInstance;

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function beforeAction($action)
    {
        $mobileControllers = [
            'property/catalog',
            'building/catalog',
            'property/property',
            'building/building'
        ];

        if (parent::beforeAction($action)) {
            $currentController = Yii::$app->controller->id;
            if (Yii::$app->view->theme !== null && Yii::$app->view->isMobile === true && in_array($currentController, $mobileControllers)) {
                Yii::$app->view->theme->pathMap = [
                    '@frontend/modules/instance/views' => [
                        '@frontend/modules/instance/themes/mobile/views',
                        '@frontend/modules/instance/views',
                    ],
                    '@frontend/modules/instance/widgets' => [
                        '@frontend/modules/instance/themes/mobile/widgets',
                        '@frontend/modules/instance/widgets',
                    ]
                ];
            }

            Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
                'sourcePath' => '@frontend/modules/instance/web',
                'css' => [
                    'css/vendor/bootstrap.min.css',
                ]
            ];
            Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
                'sourcePath' => '@frontend/modules/instance/web',
                'js' => [
                    'js/vendor/bootstrap.min.js',
                ]
            ];
            // Kostyl
            $alias = Yii::$app->request->get('site_alias');
            if ($alias === 'era-real-estate') {
                $this->redirect(Url::current(['site_alias' => 11]));
                return false;
            }

            /* @var InstanceService $instanceService */
            $instanceService = Yii::$container->get(InstanceService::class);

            $domain = Yii::$app->request->getServerName();
            if (preg_match('/eagent\.me/', $domain)) {
                $currentInstance = $instanceService->getOne(['id' => $alias]);
            } else {
                $currentInstance = $instanceService->getOne(['domain' => $domain]);
            }

            if ($currentInstance) {
                $this->currentInstance = $this->view->params['currentInstance'] = $currentInstance;
                Yii::$app->setHomeUrl(Url::to(['/instance/site/index']));
            } else {
                throw new NotFoundHttpException();
            }

            return true;
        }

        return false;
    }

    /**
     * @return array
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    protected function verifySubdomain()
    {
        $domain = Yii::$app->request->getServerName();
        if (preg_match('/eagent\.me/', $domain)) {
            return parent::verifySubdomain();
        }

        $cookies = Yii::$app->request->cookies;
        /** @var LocationService $locationService */
        $locationService = Yii::$container->get(LocationService::class);
        $locationData = $locationService->getLocationData($cookies->getValue('location'), false);
        Yii::$app->params['runtime']['location'] = $locationData;

        $appLocation = Yii::$app->request->get('app_city');

        $entity = 'city';
        $slug = $appLocation;

        if (preg_match('/^country-[\w-]+/i', $appLocation)) {
            $entity = 'country';
            $slug = preg_replace('/^country-/i', '', $appLocation);
        }
        if (preg_match('/^region-[\w-]+/i', $appLocation)) {
            $entity = 'region';
            $slug = preg_replace('/^region-/i', '', $appLocation);
        }

        if ($slug !== null) {
            if (!empty($locationData) && $slug === $locationData['slug'] && $entity === $locationData['entity']) {
                return [
                    'redirect' => false
                ];
            }

            $locationObject = $locationService->getLocationObject($entity, $slug);
            $locationForm = new LocationForm(['object' => $locationObject]);
            $result = $locationForm->saveLocation(['slug' => $slug, 'entity' => $entity], Yii::$app->response->cookies);

            if ($result === false) {
                $url = preg_replace("/^(http(?:s)?:\/\/)[\w-]+\.(" . preg_quote(Yii::$app->params['domainName']) . '.*)/', '$1$2', Url::current([], true));

                return [
                    'redirect' => true,
                    'url' => $url
                ];
            }

            return [
                'redirect' => false
            ];
        }

        if (empty($locationData)) {
            $locationData = $locationService->getLocationData($cookies->getValue('location'));
            if (empty($locationData)) {
                throw new InvalidConfigException('Default city is missing');
            }

            Yii::$app->params['runtime']['location'] = $locationData;

            $locationObject = $locationService->getLocationObject($entity, $locationData['slug']);
            $locationForm = new LocationForm(['object' => $locationObject]);
            $locationForm->saveLocation(['slug' => $locationData['slug'], 'entity' => $entity], Yii::$app->response->cookies);
            if (Yii::$app->request->cookies['location'] !== null) {
                return [
                    'redirect' => true,
                    'url' => Url::current()
                ];
            }
        }

        return [
            'redirect' => false
        ];
    }
}