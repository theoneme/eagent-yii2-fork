<?php

namespace frontend\modules\instance\controllers;

use common\helpers\UtilityHelper;
use common\services\entities\PropertyService;
use frontend\modules\instance\mappers\ComparePropertyMapper;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class CompareController
 * @package frontend\modules\instance\controllers\instance
 */
class CompareController extends InstanceBaseController
{

    public $layout = 'catalog';
    /**
     * @var PropertyService
     */
    private $_propertyService;

    /**
     * CompareController constructor.
     * @param string $id
     * @param Module $module
     * @param PropertyService $propertyService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                PropertyService $propertyService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyService = $propertyService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['toggle'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['toggle'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionToggle()
    {
        $id = Yii::$app->request->post('id', null);
        if ($id) {
            $compare = Yii::$app->session->get('compare', []);
            if (in_array($id, $compare)) {
                unset($compare[array_search($id, $compare)]);
            }
            else {
                array_push($compare, $id);
                if (count($compare) > 20) {
                    array_shift($compare);
                }
            }
            Yii::$app->session->set('compare', $compare);

            return [
                'success' => true
            ];
        }
        else {
            return [
                'success' => false
            ];
        }
    }

    /**
     *
     */
    public function actionView(){
        $compare = Yii::$app->session->get('compare', []);

        $config = require Yii::getAlias('@frontend') . "/config/compare.php";
        $properties = $this->_propertyService->getMany(['id' => $compare]);
        $data = ComparePropertyMapper::getMappedData($properties['items']);

        return $this->render('view', [
            'config' => $config,
            'data' => $data,
        ]);
    }
}