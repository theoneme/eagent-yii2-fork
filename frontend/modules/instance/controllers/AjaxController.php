<?php

namespace frontend\modules\instance\controllers;

use frontend\modules\instance\forms\ar\BlockFieldForm;
use frontend\modules\instance\mappers\FrontendToBackendFieldsMapper;
use frontend\modules\instance\services\FieldLocator;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\modules\instance\controllers\instance
 */
class AjaxController extends InstanceBaseController
{
    /**
     * @var FieldLocator
     */
    private $_fieldLocator;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param FieldLocator $fieldLocator
     * @param array $config
     */
    public function __construct(string $id, Module $module, FieldLocator $fieldLocator, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_fieldLocator = $fieldLocator;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['content'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['content'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionContent()
    {
        $input = Yii::$app->request->post();
        $success = true;

        if (is_array($input['fields'])) {
            foreach ($input['fields'] as $field) {
                $field['site_id'] = $this->currentInstance['id'];
                $field['locale'] = Yii::$app->language;
                $data = FrontendToBackendFieldsMapper::getMappedData($field);

                $blockField = $this->_fieldLocator->findOrCreate($field);
                $form = new BlockFieldForm([
                    '_blockField' => $blockField
                ]);
                $form->load($data);
                $form->bindData();

                $success = $form->save();
            }
        }

        return [
            'success' => $success
        ];
    }
}