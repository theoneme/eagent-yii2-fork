<?php

namespace frontend\modules\instance\controllers\building;

use common\controllers\FrontEndController;
use common\dto\advanced\BuildingLightDTO;
use common\interfaces\repositories\BuildingRepositoryInterface;
use common\models\Building;
use common\models\Property;
use common\models\Translation;
use common\repositories\sql\BuildingRepository;
use common\repositories\sql\PropertyRepository;
use common\services\BuildingMarkerService;
use frontend\services\catalog\LocationToFilterService;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class MarkerController
 * @package frontend\modules\instance\controllers\building
 */
class MarkerController extends FrontEndController
{
    /**
     * @var BuildingRepositoryInterface
     */
    private $_buildingRepository;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var \common\repositories\elastic\BuildingRepository
     */
    private $_elasticBuildingRepository;
    /**
     * @var LocationToFilterService
     */
    private $_locationToFilterService;

    /**
     * MarkerController constructor.
     * @param $id
     * @param Module $module
     * @param BuildingRepository $buildingRepository
     * @param PropertyRepository $propertyRepository
     * @param \common\repositories\elastic\BuildingRepository $elasticBuildingRepository
     * @param LocationToFilterService $locationToFilterService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                BuildingRepository $buildingRepository,
                                PropertyRepository $propertyRepository,
                                \common\repositories\elastic\BuildingRepository $elasticBuildingRepository,
                                LocationToFilterService $locationToFilterService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingRepository = $buildingRepository;
        $this->_propertyRepository = $propertyRepository;
        $this->_elasticBuildingRepository = $elasticBuildingRepository;
        $this->_locationToFilterService = $locationToFilterService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['catalog', 'account', 'view'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['catalog', 'account', 'view'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionCatalog()
    {
        $params = Yii::$app->request->queryParams;
        $params['status'] = Building::STATUS_ACTIVE;
        $params = $this->_locationToFilterService->getFilters($params);

        $markerService = new BuildingMarkerService($this->_elasticBuildingRepository);
        $markers = $markerService->getMarkers($params);

        return [
            'success' => true,
            'markers' => $markers['items']
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionView($id)
    {
        $building = $this->_buildingRepository
            ->with(['translations' => function ($query) {
                return $query->andOnCondition(['building_translations.locale' => [Yii::$app->language, 'en-GB'], 'key' => [Translation::KEY_SLUG, Translation::KEY_TITLE]]);
            }, 'attachments', 'buildingAttributes'])
            ->groupBy('building.id')
            ->findOneByCriteria(['and',
                ['building.id' => $id],
                ['not', ['building.status' => Building::STATUS_DELETED]]
            ], true);

        if ($building === null) {
            return [
                'success' => false
            ];
        }

        $buildingDTO = new BuildingLightDTO($building);
        $building = $buildingDTO->getData();

        $saleCount = $this->_propertyRepository->joinWith(['category.translations'])->countByCriteria([
            'building_id' => $building['id'],
            'category_translations.value' => 'flats',
            'category_translations.key' => 'slug',
            'type' => Property::TYPE_SALE,
        ]);
        $rentCount = $this->_propertyRepository->joinWith(['category.translations'])->countByCriteria([
            'building_id' => $building['id'],
            'category_translations.value' => 'flats',
            'category_translations.key' => 'slug',
            'type' => Property::TYPE_RENT,
        ]);

        $markerData = [
            'image' => $building['image'],
            'url' => Url::to(['/instance/building/building/view', 'slug' => $building['slug']]),
            'flatsForRent' => Yii::t('instance', '{flats, plural, one{# flat} other{# flats}} for rent', ['flats' => $rentCount]),
            'flatsForSale' => Yii::t('instance', '{flats, plural, one{# flat} other{# flats}} for sale', ['flats' => $saleCount]),
        ];

        return [
            'success' => true,
            'marker' => $markerData
        ];
    }
}
