<?php

namespace frontend\modules\instance\controllers\building;

use common\mappers\SimilarBuildingsParamsMapper;
use common\services\entities\BuildingService;
use common\services\entities\UserService;
use common\services\MetaService;
use common\services\seo\BuildingSeoService;
use common\services\seo\PropertySeoService;
use frontend\forms\contact\ContactAgentForm;
use frontend\mappers\SpecialAttributesMapper;
use frontend\modules\instance\controllers\InstanceBaseController;
use frontend\services\BuildingPropertiesService;
use Yii;
use yii\base\Module;

/**
 * Class BuildingController
 * @package frontend\modules\instance\controllers\building
 */
class BuildingController extends InstanceBaseController
{
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var PropertySeoService
     */
    private $_buildingSeoService;
    /**
     * @var BuildingPropertiesService
     */
    private $_buildingPropertiesService;

    /**
     * BuildingController constructor.
     * @param $id
     * @param Module $module
     * @param BuildingPropertiesService $buildingPropertiesService
     * @param BuildingService $buildingService
     * @param BuildingSeoService $buildingSeoService
     * @param UserService $userService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                BuildingPropertiesService $buildingPropertiesService,
                                BuildingService $buildingService,
                                BuildingSeoService $buildingSeoService,
                                UserService $userService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingService = $buildingService;
        $this->_userService = $userService;
        $this->_buildingSeoService = $buildingSeoService;
        $this->_buildingPropertiesService = $buildingPropertiesService;
    }

    /**
     * @param $slug
     * @return array|string
     */
    public function actionView($slug)
    {
        $building = $this->_buildingService->getOne(['slug' => $slug]);

        if ($building === null) {
            Yii::$app->session->setFlash('error', Yii::t('instance', 'Requested building page is not found'));
            return $this->redirect(['/site/index']);
        }

        $siteOwner = $this->_userService->getOne(['id' => $this->currentInstance['user_id']]);

        $counts = $this->_buildingPropertiesService->getCounts($building['id']);

        $metaService = new MetaService($this);

        $seoParams = array_map(function ($var) {
            return $var['value'];
        }, $building['attributes']);
        $seoParams['address'] = $building['address'];
        $seoParams['title'] = $building['name'] ?? null;
        $seoParams['countSale'] = $counts['sale']['count'] ?? 0;
        $seoParams['countRent'] = $counts['rent']['count'] ?? 0;
        $seoParams['count'] = $seoParams['countSale'] + $seoParams['countRent'];
        $seo = $this->_buildingSeoService->getSeo(BuildingSeoService::TEMPLATE_BUILDING, $seoParams);
        $seo['image'] = $building['image'];
        $metaService->registerMeta($seo);

        $similarParams = SimilarBuildingsParamsMapper::getMappedData($building);
        $similarBuildings = $this->_buildingService->getMany($similarParams, [
            'limit' => 3,
            'exclude' => $building['id']
        ]);

        $this->layout = 'catalog';

        $building = SpecialAttributesMapper::getMappedData($building, 'building_amenitie');
        $building = SpecialAttributesMapper::getMappedData($building, 'new_construction_builder', SpecialAttributesMapper::TYPE_STRING);

        return $this->render('view', [
            'counts' => $counts,
            'building' => $building,
            'similar' => $similarBuildings['items'],
            'seo' => $seo,
            'siteOwner' => $siteOwner,
            'contactAgentForm' => new ContactAgentForm(),
        ]);
    }
}