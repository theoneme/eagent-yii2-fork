<?php

namespace frontend\modules\instance\controllers\building;

use common\services\MetaService;
use frontend\helpers\ViewToTemplateHelper;
use frontend\modules\instance\controllers\InstanceBaseController;
use frontend\services\catalog\BuildingCatalogDataService;
use frontend\services\catalog\BuildingCatalogUrlService;
use frontend\widgets\BuildingFilter;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;
use yii\web\Response;

/**
 * Class CatalogController
 * @package frontend\modules\instance\controllers\building
 */
class CatalogController extends InstanceBaseController
{
    /**
     * @var BuildingCatalogUrlService
     */
    private $_buildingCatalogUrlService;
    /**
     * @var BuildingCatalogDataService
     */
    private $_buildingCatalogDataService;

    /**
     * CatalogController constructor.
     * @param $id
     * @param Module $module
     * @param BuildingCatalogUrlService $buildingCatalogUrlService
     * @param BuildingCatalogDataService $buildingCatalogDataService
     * @param array $config
     */
    public function __construct($id, Module $module,
                                BuildingCatalogUrlService $buildingCatalogUrlService,
                                BuildingCatalogDataService $buildingCatalogDataService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingCatalogUrlService = $buildingCatalogUrlService;
        $this->_buildingCatalogDataService = $buildingCatalogDataService;

        $this->layout = 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['view', 'index-ajax', 'index-ajax-short'],
            ]
        ];
    }

    /**
     * @return array|string
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
//        $queryParams['db-sql'] = true;

        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $template = ViewToTemplateHelper::getTemplate($cookieView);
        if ($cookieView !== 'map' && array_key_exists('box', $queryParams) && array_key_exists('zoom', $queryParams)) {
            $template = 'index-map';
            $this->actionView('map');
        }

        $catalogData = $this->_buildingCatalogDataService->getData($queryParams, $cookieView, BuildingCatalogDataService::MODE_INDEX);
        $catalogData['buildingFilter']->template = '@frontend/modules/instance/widgets/views/building-filter';

        $metaService = new MetaService($this);
        $metaService->registerMeta($catalogData['seo']);

        return $this->render($template, $catalogData);
    }

    /**
     * @return array
     */
    public function actionIndexAjax()
    {
        $queryParams = Yii::$app->request->queryParams;
//        $queryParams['db-sql'] = true;

        $seoUrl = $this->_buildingCatalogUrlService->getUrl($queryParams, ['/instance/building/catalog/index']);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $catalogData = $this->_buildingCatalogDataService->getData($queryParams, $cookieView, BuildingCatalogDataService::MODE_INDEX_AJAX);
        $markers = ArrayHelper::remove($catalogData, 'markers');
        $seo = $catalogData['seo'];
        /** @var BuildingFilter $filter */
        $filter = ArrayHelper::remove($catalogData, 'buildingFilter');
        $filter->template = '@frontend/modules/instance/widgets/views/building-filter';

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'seo' => $seo,
            'url' => $seoUrl,
            'domain' => array_key_exists('city', $queryParams),
            'markers' => $markers['items'],
            'polygon' => $catalogData['polygon'],
            'filter' => $filter->run(),
            'success' => true
        ];
    }

    /**
     * @return array
     */
    public function actionIndexAjaxShort()
    {
        $queryParams = Yii::$app->request->queryParams;
//        $queryParams['db-sql'] = true;

        $seoUrl = $this->_buildingCatalogUrlService->getUrl($queryParams, ['/instance/building/catalog/index']);
        $cookieView = Yii::$app->request->cookies->getValue('catalog-view', 'grid');
        $catalogData = $this->_buildingCatalogDataService->getData($queryParams, $cookieView, BuildingCatalogDataService::MODE_INDEX);

        return [
            'catalog' => $this->render("partial/{$cookieView}", $catalogData),
            'success' => true,
            'url' => $seoUrl
        ];
    }

    /**
     * @param $view
     * @return array
     */
    public function actionView($view)
    {
        if (!in_array($view, ['grid', 'list', 'map'])) {
            return ['success' => false];
        }

        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'catalog-view',
            'value' => $view,
            'domain' => '.' . Yii::$app->params['domainName'],
            'expire' => time() + 3600 * 6,
        ]));

        return ['success' => true];
    }
}
