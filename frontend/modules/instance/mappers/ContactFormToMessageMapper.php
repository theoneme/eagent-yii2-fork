<?php
namespace frontend\modules\instance\mappers;

use common\interfaces\DataMapperInterface;
use frontend\modules\instance\models\InstanceSetting;
use Yii;

/**
 * Class ContactFormToMessageMapper
 * @package common\mappers\forms
 */
class ContactFormToMessageMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public static function getMappedData($rawData)
    {
        return [
            'to' => $rawData['to'],
            'receiverName' => 'eAgent',
            'senderName' => $rawData['attributes']['name'],
            'senderEmail' => $rawData['attributes']['email'],
            'senderMessage' => $rawData['attributes']['message'],
            'subject' => $rawData['attributes']['subject'],
            'view' => 'contact-message',
            'locale' => Yii::$app->language,
            'viewPath' => '@frontend/modules/instance/views/mail',
            'viewParams' => ['currentInstance' => $rawData['currentInstance']]
        ];
    }
}