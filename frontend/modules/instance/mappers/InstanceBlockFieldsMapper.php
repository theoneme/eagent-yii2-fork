<?php

namespace frontend\modules\instance\mappers;

use common\interfaces\DataMapperInterface;
use common\mappers\TranslationsMapper;
use frontend\modules\instance\models\InstanceTranslation;
use yii\helpers\ArrayHelper;

/**
 * Class InstanceBlockFieldsMapper
 * @package frontend\modules\instance\mappers
 */
class InstanceBlockFieldsMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array|mixed
     */
    public static function getMappedData($rawData)
    {
        $result = ArrayHelper::map($rawData, 'alias', function ($var) {
            $translation = TranslationsMapper::getMappedData($var['translations'], TranslationsMapper::MODE_SHORT);
            return $translation[InstanceTranslation::KEY_TITLE] ?? null;
        });

        return $result;
    }
}