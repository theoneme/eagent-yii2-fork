<?php

namespace frontend\modules\instance\mappers;

use common\interfaces\DataMapperInterface;
use frontend\modules\instance\decorators\BlockTypeDecorator;
use yii\helpers\ArrayHelper;

/**
 * Class BlockMapper
 * @package frontend\modules\instance\mappers
 */
class BlockMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return ArrayHelper::map($rawData, 'id', function ($value) {
            return BlockTypeDecorator::decorate($value['type']);
        });
    }
}