<?php

namespace frontend\modules\instance\mappers;

use common\interfaces\DataMapperInterface;
use common\models\Translation;
use Yii;

/**
 * Class ComparePropertyMapper
 * @package frontend\modules\instance\mappers
 */
class ComparePropertyMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array
     */
    public static function getMappedData($rawData)
    {
        return array_map(function($var){
            return [
                'slug' => $var['slug'],
                'image' => $var['image'],
                'price' => $var['price'],
                'title' => $var['title'],
                'address' => $var['address'],
                'category' => $var['category'][Translation::KEY_TITLE] ?? null,
                'rooms' => $var['attributes']['rooms'] ?? null,
                'bathrooms' => $var['attributes']['bathrooms'] ?? null,
                'property_area' => !empty($var['attributes']['property_area']) ? Yii::t('main', '{area} m²', ['area' => $var['attributes']['property_area']]) : null,
                'living_area' => !empty($var['attributes']['living_area']) ? Yii::t('main', '{area} m²', ['area' => $var['attributes']['living_area']]) : null,
                'kitchen_area' => !empty($var['attributes']['kitchen_area']) ? Yii::t('main', '{area} m²', ['area' => $var['attributes']['kitchen_area']]) : null,
                'floor' => $var['attributes']['floor'] ?? null,
                'amenities' => $var['attributes']['amenities'] ?? null,
            ];
        }, $rawData);
    }
}