<?php

namespace frontend\modules\instance\mappers;

use common\interfaces\DataMapperInterface;

/**
 * Class FrontendToBackendFieldsMapper
 * @package frontend\modules\instance\mappers
 */
class FrontendToBackendFieldsMapper implements DataMapperInterface
{
    /**
     * @param $rawData
     * @return array|mixed
     */
    public static function getMappedData($rawData)
    {
        return [
            'BlockFieldForm' => [
                'block_id' => $rawData['block_id'] ?? null,
                'alias' => $rawData['alias'] ?? null,
                'type' => $rawData['type'] ?? null,
                'site_id' => $rawData['site_id'],
            ],
            'MetaForm' => [
                $rawData['locale'] => [
                    'title' => $rawData['value'] ?? null
                ]
            ]
        ];
    }
}