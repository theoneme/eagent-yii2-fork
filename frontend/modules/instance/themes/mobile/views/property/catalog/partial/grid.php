<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.06.2019
 * Time: 14:08
 */

use yii\widgets\LinkPager;

/**
 * @var array $sortData
 * @var array $seo
 * @var array $properties
 * @var integer $gridSize
 */

?>

<div class="loading-overlay hidden"></div>

<div class="mb-3">
    <h1 class="text-left"><?= $seo['heading'] ?></h1>
</div>
<?php if (count($properties['items']) > 0) { ?>
    <div class="row">
        <?php foreach ($properties['items'] as $property) { ?>
            <div class="col-12 col-sm-6">
                <?= $this->render('@frontend/modules/instance/views/common/partial/property-grid', ['property' => $property]); ?>
            </div>
        <?php } ?>
    </div>
    <?php
    if ($properties['pagination']) {
        $properties['pagination']->route = '/instance/property/catalog/index-ajax-short';

        echo LinkPager::widget([
            'pagination' => $properties['pagination'],
            'linkOptions' => ['data-action' => 'switch-page', 'class' => 'page-link'],
            'linkContainerOptions' => ['class' => 'page-item'],
            'disabledListItemSubTagOptions' => [
                'tag' => 'a',
                'class' => ['page-link']
            ],
            'prevPageLabel' => '«&nbsp;' .  Yii::t('instance', 'Prev Page'),
            'nextPageLabel' => Yii::t('instance', 'Next Page') . '&nbsp;»',
        ]);
    }
} else { ?>
    <div class="home-list">
        <p>
            <?= !empty($seo['emptyText']) ? $seo['emptyText'] : Yii::t('main', 'No results found'); ?>
        </p>
    </div>
<?php } ?>
