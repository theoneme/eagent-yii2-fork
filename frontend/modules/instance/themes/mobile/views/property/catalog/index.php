<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.06.2019
 * Time: 14:06
 */

use frontend\widgets\PropertyFilter;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $properties
 * @var array $seo
 * @var array $sortData
 * @var View $this
 * @var PropertyFilter $propertyFilter
 * @var string $lat
 * @var string $lng
 * @var string $box
 * @var string $zoom
 */

\frontend\assets\plugins\FontAwesomeAsset::register($this);
\frontend\modules\instance\themes\mobile\assets\CatalogAsset::register($this);
\frontend\assets\FilterAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\frontend\assets\plugins\CatalogMapAsset::register($this);

?>

<div class="filter-top-mobile d-flex justify-content-between p-3 position-relative">
    <a href="#" id="open-filter" class="filter-link"><i class="icon-parameters align-bottom"></i>
        <span class="align-bottom ml-2"><?= Yii::t('instance', 'Filter') ?></span>
    </a>
    <a href="#" id="open-map" class="filter-link"><i class="icon-navigation align-bottom"></i>
        <span class="align-bottom ml-2"><?= Yii::t('instance', 'Map') ?></span>
    </a>
</div>

<div class="container-fluid" id="properties-list">
    <?= $this->render('partial/grid', [
        'sortData' => $sortData,
        'seo' => $seo,
        'properties' => $properties
    ]) ?>
</div>

<div class="d-none map-over">
    <div id="map_catalog" class="h-100">

    </div>
</div>

<div id="filter-component">
    <?= $propertyFilter->run() ?>
</div>

<?php $baseUrl = Url::to(['/property/catalog/search']);
$catalogViewUrl = Url::to(['/property/catalog/view']);
$markerListUrl = Url::to(['/property/marker/catalog']);
$markerViewUrl = Url::to(['/property/marker/view']);
$complexUrl = Url::to(['/property/catalog/index-ajax']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);
$polygon = json_encode($polygon);

$script = <<<JS
    let filterTopMobile = $('.filter-top-mobile');
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 100) {
            if (!filterTopMobile.hasClass('scroll-content')) {
                $('.filter-top-mobile').addClass('scroll-content');
                $('.call').addClass('scroll-content');
                $('header').addClass('overflow-hidden');
            }
        } else {
            if (filterTopMobile.hasClass('scroll-content')) {
                $('.filter-top-mobile').removeClass('scroll-content');
                $('.call').removeClass('scroll-content');
                $('header').removeClass('overflow-hidden');
            }
        }
    });
    
    $(document.body).on('click', '#open-filter', function() {
        $('.filters').addClass('open');
        $('body').toggleClass('overflow-hidden');
        return false;
    });
    
    $(document.body).on('click', '#open-map', function() {
        $('.map-over').toggleClass('d-none');
        $('body').toggleClass('overflow-hidden');
    });

    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };
    
    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        submitOnChange: false,
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    let params = filter.buildQuery();
    
    let mapInitialized = false;
    let map = null;
    
    $(document).on('click', '#open-map', function() { 
        if(mapInitialized === false) {
            map = new CatalogMap({
                lat: '{$lat}',
                lon: '{$lng}',
                zoom: {$zoom},
                box: '{$box}',
                mode: 'advanced',
                markerViewRoutes: {
                    property: '{$markerViewUrl}',
                },
                markerIcons: {
                    property: '{$markerPropertyIcon}'
                },
                polygon: {$polygon}
            });
            map.init();
            map.fetchMarkers('{$markerListUrl}', params);
            mapInitialized = true;
            
            $('#' + map.options.mapCanvasId).on('mapDragEnd mapZoomChanged', function() {
                filter.setBox($(this).data('box'));
                filter.setZoom($(this).data('zoom'));
            
                filter.process();
            });
        }
    });
    
    let fn = function(result) {
        if (result.domain === false) {
            if(mapInitialized === true) {
                map.setMarkers(result.markers);
                map.setPolygon(result.polygon);
            }
            $('#properties-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $(document).on('submit', filter.options.filterSelector, function() {
        let params = filter.buildQuery();
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
        let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '#properties-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if (result.success === true) {
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({
                    result: true
                }, 'Page', result.url);
            }
        });
    
        return false;
    });
JS;
$this->registerJs($script);