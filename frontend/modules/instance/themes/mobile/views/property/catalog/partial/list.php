<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.06.2019
 * Time: 14:08
 */

/**
 * @var array $sortData
 * @var array $seo
 * @var array $properties
 */

echo $this->render('grid', [
    'sortData' => $sortData,
    'seo' => $seo,
    'properties' => $properties,
]);
