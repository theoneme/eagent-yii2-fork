<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.06.2019
 * Time: 14:06
 */

use frontend\widgets\PropertyFilter;
use yii\web\View;

/**
 * @var array $properties
 * @var array $seo
 * @var array $sortData
 * @var View $this
 * @var PropertyFilter $propertyFilter
 * @var string $lat
 * @var string $lng
 * @var string $box
 * @var integer $zoom
 */

echo $this->render('index', [
    'properties' => $properties,
    'propertyFilter' => $propertyFilter,
    'sortData' => $sortData,
    'lat' => $lat,
    'lng' => $lng,
    'zoom' => $zoom,
    'box' => $box,
    'seo' => $seo,
    'polygon' => $polygon
]);