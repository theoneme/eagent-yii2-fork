<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.06.2019
 * Time: 14:08
 */

/**
 * @var array $seo
 * @var array $buildings
 */

echo $this->render('grid', [
    'seo' => $seo,
    'buildings' => $buildings,
]);