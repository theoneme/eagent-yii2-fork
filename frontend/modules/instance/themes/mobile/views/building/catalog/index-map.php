<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.06.2019
 * Time: 14:06
 */

use frontend\widgets\BuildingFilter;
use yii\web\View;

/**
 * @var array $buildings
 * @var array $seo
 * @var View $this
 * @var BuildingFilter $buildingFilter
 * @var string $lat
 * @var string $lng
 * @var string $box
 * @var integer $zoom
 */

echo $this->render('index', [
    'buildings' => $buildings,
    'buildingFilter' => $buildingFilter,
    'lat' => $lat,
    'lng' => $lng,
    'seo' => $seo,
    'zoom' => $zoom,
    'box' => $box,
    'polygon' => $polygon
]);