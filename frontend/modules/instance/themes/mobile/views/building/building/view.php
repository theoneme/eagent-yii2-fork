<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 13.06.2019
 * Time: 15:58
 */

use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use frontend\assets\plugins\ModalMapAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\forms\contact\ContactAgentForm;
use frontend\modules\instance\themes\mobile\assets\PropertyAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\models\Building;

/**
 * @var array $building
 * @var array $similar
 * @var array $siteOwner
 * @var array $counts
 * @var ContactAgentForm $contactAgentForm
 * @var View $this
 */

SlickAsset::register($this);
FontAwesomeAsset::register($this);
ModalMapAsset::register($this);
PropertyAsset::register($this);
FancyBoxAsset::register($this);

?>

    <div class="wrapper1920">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="photo" role="tabpanel" aria-labelledby="a-photo">
                <div class="slider-property">
                    <?php foreach ($building['images'] as $image) { ?>
                        <div>
                            <?= Html::a(null, $image, [
                                'class' => 'img d-block',
                                'style' => "background:url({$image}); background-size: cover",
                                'data-fancybox' => 'primary-fancy',
                            ]) ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="tab-pane fade" id="map" role="tabpanel" aria-labelledby="a-map">
                <div id="modal-map" class="property-map"
                     data-lat="<?= $building['lat'] ?>"
                     data-lng="<?= $building['lng'] ?>" data-type="property">
                </div>
            </div>
        </div>
    </div>
    <div class="filter">
        <div class="wrapper1920">
            <div class="d-flex justify-content-around">
                <ul class="nav tab-links col-12 col-lg-6 col-xl-4 no-list catalog-view d-flex p-0 justify-content-end"
                    role="tablist">
                    <li class="col-6">
                        <a class="active text-center d-block" id="a-photo" href="#photo" role="tab"
                           data-toggle="tab"
                           aria-controls="map" aria-selected="true">
                            <i class="icon-camera"></i>
                            <?= Yii::t('instance', 'Photo') ?>
                        </a>
                    </li>
                    <li class="col-6">
                        <a class="text-center d-block" href="#map" id="a-map" role="tab" data-toggle="tab"
                           data-action="init-gmap">
                            <i class="icon-gps"></i>
                            <?= Yii::t('instance', 'Map') ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="light-grey-bg">
        <div class="wrapper">
            <div class="container-fluid">
                <div class="grey py-3">
                    <?= Html::a(Yii::t('instance', 'Home'), ['/instance/site/index']) ?> /
                    <?= Html::a(Yii::t('instance', 'Catalog'), ['/instance/building/catalog/index', 'operation' => Building::TYPE_NEW_CONSTRUCTION_TEXT]) ?>
                    /
                    <?= $building['title'] ?>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <h1><?= $building['title'] ?></h1>
                        <h5 class="grey">
                            <?= $building['address'] ?>
                        </h5>
                        <h6>
                            <?php if (isset($building['specialAttributes']['new_construction_builder'])) { ?>
                                <?= Yii::t('instance', 'Developer: {developer}', [
                                    'developer' => $building['specialAttributes']['new_construction_builder']
                                ]) ?>
                            <?php } ?>
                        </h6>

                        <?php if (!empty($building['description'])) { ?>
                            <p>
                                <?= $building['description'] ?>
                            </p>
                        <?php } ?>
                        <?php if (!empty($building['attributes'])) { ?>
                            <div class="my-4 property-section">
                                <h3><?= Yii::t('instance', 'Building Features:') ?></h3>
                                <ul class="no-list prop-feat my-3 row">
                                    <?php foreach ($building['attributes'] as $attribute) { ?>
                                        <li class="col-6 col-sm-4">
                                            <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } else { ?>
                            <?= Yii::t('instance', 'No attributes specified') ?>
                        <?php } ?>

                        <?php if (isset($building['specialAttributes']['building_amenitie'])) { ?>
                            <div class="my-4 property-section">
                                <h3><?= Yii::t('instance', 'Building Amenities:') ?></h3>
                                <ul class="no-list prop-feat my-3 row">
                                    <?php foreach ($building['specialAttributes']['building_amenitie'] as $attribute) { ?>
                                        <li class="col-6 col-sm-4">
                                            <?= $attribute ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>

                        <?php if (count($counts) > 0) { ?>
                            <h4 class="font-weight-bold mt-4">
                                <?= Yii::t('instance', 'Condos in house{title}{address}', [
                                    'address' => !empty($building['address']) ? Yii::t('instance', ' at {address}', ['address' => $building['address']]) : '',
                                    'title' => !empty($building['translations']['name']) ? Yii::t('instance', ' «{title}»', ['title' => $building['translations']['name']]) : '',
                                ]) ?>
                            </h4>
                            <div class="bedrooms-block mt-4 mb-5">
                                <ul class="no-list history-switch no-margin nav">
                                    <?php
                                    $active = true;
                                    if (array_key_exists('sale', $counts)) {
                                        echo Html::tag('li',
                                            Html::a(
                                                '<span class="badge badge-secondary">' . $counts['sale']['count'] . '</span> ' . Yii::t('instance', 'For Sale'),
                                                '#sale',
                                                ['data-toggle' => 'tab', 'class' => $active ? 'active' : '']
                                            )
                                        );
                                        $active = false;
                                    }
                                    if (array_key_exists('rent', $counts)) {
                                        echo Html::tag('li',
                                            Html::a(
                                                '<span class="badge badge-secondary">' . $counts['rent']['count'] . '</span> ' . Yii::t('instance', 'For Rent'),
                                                '#rent',
                                                ['data-toggle' => 'tab', 'class' => $active ? 'active' : '']
                                            )
                                        );
                                        $active = false;
                                    }
                                    if (array_key_exists('off', $counts)) {
                                        echo Html::tag('li',
                                            Html::a(
                                                '<span class="badge badge-secondary">' . $counts['off']['count'] . '</span> ' . Yii::t('instance', 'Unavailable'),
                                                '#off',
                                                ['data-toggle' => 'tab', 'class' => $active ? 'active' : '']
                                            )
                                        );
                                    } ?>
                                </ul>
                                <div class="tab-content tab-history">
                                    <?php
                                    $active = true;
                                    if (array_key_exists('sale', $counts)) { ?>
                                        <div class="history-content tab-pane fade show <?= $active ? 'active' : '' ?>"
                                             id="sale">
                                            <?php foreach ($counts['sale']['items'] as $rooms => $data) {
                                                echo $this->render('room-group', ['tab' => 'sale', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                                            } ?>
                                        </div>
                                        <?php $active = false;
                                    }
                                    if (array_key_exists('rent', $counts)) { ?>
                                        <div class="history-content tab-pane fade show <?= $active ? 'active' : '' ?>"
                                             id="rent">
                                            <?php foreach ($counts['rent']['items'] as $rooms => $data) {
                                                echo $this->render('room-group', ['tab' => 'rent', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                                            } ?>
                                        </div>
                                        <?php $active = false;
                                    }
                                    if (array_key_exists('off', $counts)) { ?>
                                        <div class="history-content tab-pane fade show <?= $active ? 'active' : '' ?>"
                                             id="off">
                                            <?php foreach ($counts['off']['items'] as $rooms => $data) {
                                                echo $this->render('room-group', ['tab' => 'off', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                                            } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (!empty($building['progress'])) { ?>
                            <?php $progress = array_pop($building['progress']) ?>
                            <div class="mb-3 property-section">
                                <h3>
                                    <?= Yii::t('instance', 'Construction Progress ({quarter} quarter {year} year)', [
                                        'quarter' => $progress['quarter'],
                                        'year' => $progress['year']
                                    ]) ?>
                                </h3>
                                <div class="row">
                                    <?php foreach ($progress['images'] as $key => $image) { ?>
                                        <div class="col-6 col-md-3">
                                            <?php if ($key === 3) { ?>
                                                <div class="prop-photo more-photos"
                                                     style="background-image: url(<?= $progress['thumbnails'][$key] ?>)">
                                                    <div class="bg">
                                                        <a class="center-block text-center" href="<?= $image ?>"
                                                           data-fancybox="progress-<?= $progress['year'] ?>-<?= $progress['quarter'] ?>"
                                                           data-role="progress-fancy-item">
                                                            <?= Yii::t('instance', '+{count} more', [
                                                                'count' => count($progress['images']) - 3
                                                            ]) ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <?= Html::a(null, $image, [
                                                    'class' => $key > 3 ? 'd-none' : 'prop-photo d-block',
                                                    'data-role' => 'progress-fancy-item',
                                                    'data-fancybox' => "progress-{$progress['year']}-{$progress['quarter']}",
                                                    'style' => "background-image: url({$progress['thumbnails'][$key]})"
                                                ]); ?>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (!empty($building['video'])) { ?>
                            <div class="mb-3 property-section">
                                <h3><?= Yii::t('instance', 'Video Tour:') ?></h3>
                                <div class="prop-video position-relative">
                                    <iframe height=480 class="w-100"
                                            src="<?= $building['video']['content'] ?>">
                                    </iframe>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row share-report pt-3 pb-4">
                            <?= \frontend\widgets\SocialShareWidget::widget([
                                'template' => '@frontend/modules/instance/widgets/views/social-share-widget',
                                'url' => Url::current([], true)
                            ]) ?>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h4 class="font-weight-bold"><?= Yii::t('instance', 'Here are some Similar buildings:') ?></h4>
                                <p class="grey">
                                    <?= Yii::t('instance', 'near {address}', [
                                        'address' => $building['address']
                                    ]) ?>
                                </p>
                                <div class="similar-list pt-2">
                                    <?php foreach ($similar as $similarItem) { ?>
                                        <div class="similar-item d-flex py-3">
                                            <?= Html::a(Html::img($similarItem['image']), ['/instance/building/building/view', 'slug' => $similarItem['slug']], [
                                                'class' => 'img'
                                            ]) ?>
                                            <div class="info">
                                                <?= Html::a($similarItem['title'], ['/instance/building/building/view', 'slug' => $similarItem['slug']], [
                                                    'class' => 'font-weight-bold'
                                                ]) ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="bg-white p-4">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-12">
                                    <div class="d-flex align-items-center prop-agent py-4">
                                        <div class="img rounded-circle overflow-hidden">
                                            <?= Html::img($siteOwner['avatar'], [
                                                'alt' => $siteOwner['username'],
                                                'class' => 'mx-auto'
                                            ]) ?>
                                        </div>
                                        <div class="text">
                                            <div class="name font-weight-bold">
                                                <?= $siteOwner['username'] ?>
                                            </div>
                                            <div class="company grey">
                                                <?= Yii::t('instance', 'Agent') ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $form = ActiveForm::begin([
                                        'action' => Url::to(['/contact/contact-agent']),
                                        'id' => 'side-contact-agent-form',
                                        'enableAjaxValidation' => true,
                                        'enableClientValidation' => false,
                                    ]); ?>
                                    <?= $form->field($contactAgentForm, 'name')->textInput([
                                        'placeholder' => Yii::t('instance', 'Your Name'),
                                    ])->label(false) ?>
                                    <?= $form->field($contactAgentForm, 'phone')->textInput([
                                        'placeholder' => Yii::t('instance', 'Your Phone'),
                                    ])->label(false) ?>
                                    <?= $form->field($contactAgentForm, 'email')->textInput([
                                        'type' => 'email',
                                        'placeholder' => Yii::t('instance', 'Your Email'),
                                    ])->label(false) ?>
                                    <?= $form->field($contactAgentForm, 'message')->textarea([
                                        'class' => 'w-100',
                                        'placeholder' => Yii::t('instance', 'Write Your Text'),
                                    ])->label(false) ?>
                                    <?= Html::submitInput(Yii::t('instance', 'Contact Agent'), [
                                        'class' => 'button big orange w-100'
                                    ]) ?>
                                    <?php ActiveForm::end() ?>
                                </div>
                                <div class="col-12 col-md-6 col-lg-12">
                                    <div class="property-add options px-0 mt-4 row border border-light rounded rounded-sm">
                                        <div class="col-6">
                                            <a href="#"
                                               class="hint--top-right favorite d-inline-flex font-weight-normal m-0"
                                               data-hint="Add to bookmark">
                                                <i class="icon-heart grey"></i>&nbsp;
                                                <span><?= Yii::t('instance', 'Add to Favorite') ?></span>
                                            </a>
                                        </div>
                                        <div class="col-6">
                                            <a href="#"
                                               class="hint--top-right favorite d-inline-flex font-weight-normal m-0"
                                               data-hint="Add to compare">
                                                <i class="icon-plus-black-symbol"></i>&nbsp;
                                                <span><?= Yii::t('instance', 'Compare') ?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $markerPropertyIcon = Url::to(['/images/marker.png'], true);
$propertiesUrl = Url::to(['/instance/property/ajax/building-properties']);

$script = <<<JS
    $(".slider-property").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        arrows: true,
        infinite: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }]
    });

    let mapInitialized = false;
    $('[data-action=init-gmap]').on('click', function() {
        if(mapInitialized === false) {
            let modalMap = new ModalMap({
                markerIcons: {
                    property: '{$markerPropertyIcon}'
                }
            });
            modalMap.init();
            modalMap.setMarker();
            
            mapInitialized = true;
        } 
    });
    
    let tabFn = function() {
        $('.bedroom-group [data-page]').off('click').on('click', function() {
            let container = $(this).closest('.bedroom-group').find('[data-role=properties-container]'),
                href = $(this).attr('href');
            
            $.get(href, {}, function(result) {
                if(result.success === true) {
                    container.data('loaded', 1);
                    container.html(result.html);
                    tabFn();
                } 
            });
            
            return false;
        });
    };
    
    $('[data-action=load-properties]').on('click', function() {
        let buildingId = $(this).data('building'),
            operation = $(this).data('operation'),
            rooms = $(this).data('rooms'),
            that = $(this),
            container = that.closest('.bedroom-group').find('[data-role=properties-container]');
        
        if(container.data('loaded') !== 1) {
            $.get('{$propertiesUrl}', {operation: operation, building_id: buildingId, rooms: rooms}, function(result) {
                if(result.success === true) {
                    container.data('loaded', 1);
                    container.html(result.html);
                    tabFn();
                } 
            });
        }
    });
    
    $('[data-role=progress-fancy-item]').fancybox({
        hash: false,
    });
JS;

$this->registerJs($script);


