<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:53
 */

/**
 * @var array $filter
 * @var \yii\widgets\ActiveForm $form
 * @var string $key
 */

?>

<div class="form-group">
    <label for="<?= $key ?>"><?= $filter['title'] ?></label>
    <select class='w-100' name="<?= $key ?>" id="<?= $key ?>">
        <option value="">
            <?= Yii::t('instance', 'Select {attribute}', ['attribute' => $filter['title']]) ?>
        </option>
        <?php foreach ($filter['values'] as $valueKey => $value) { ?>
            <option value="<?= $valueKey ?>" <?= $valueKey == $filter['checked'] ? 'selected' : '' ?>><?= "{$value['title']} ({$value['count']})" ?></option>
        <?php } ?>
    </select>
</div>