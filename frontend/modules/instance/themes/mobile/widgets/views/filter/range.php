<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:53
 */

use yii\helpers\Html;

/**
 * @var array $filter
 * @var \yii\widgets\ActiveForm $form
 * @var string $key
 */

?>

<div class="form-group">
    <label for="<?= $key ?>-min"><?= $filter['title'] ?></label>
    <div class="row">
        <div class="col-5 pr-0">
            <?= Html::textInput("{$key}[min]", $filter['checked']['min'], [
                'placeholder' => Yii::t('instance', 'From {min}', ['min' => $filter['values']['min']]),
                'type' => 'number',
                'min' => 0,
                'id' => "{$key}-min",
                'class' => 'w-100'
            ]) ?>
        </div>
        <div class="col-2 p-0 text-center">&#8211;</div>
        <div class="col-5 pl-0">
            <?= Html::textInput("{$key}[max]", $filter['checked']['max'], [
                'placeholder' => Yii::t('instance', 'To {max}', ['max' => $filter['values']['max']]),
                'type' => 'number',
                'min' => 0,
                'class' => 'w-100'
            ]) ?>
        </div>
    </div>
</div>