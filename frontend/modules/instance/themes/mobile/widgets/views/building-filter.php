<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:16
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $catalogView
 * @var array $filters
 * @var string $address
 * @var array $params
 * @var integer $gridSize
 */

?>

<?= Html::beginForm(Url::to(['/instance/building/catalog/index']), 'get', [
    'id' => 'filter-form',
]); ?>
<?= Html::hiddenInput('box', $params['box'] ?? null, ['id' => 'filter-box']) ?>
<?= Html::hiddenInput('zoom', $params['zoom'] ?? null, ['id' => 'filter-zoom']) ?>
<?= Html::hiddenInput('district', $params['district'] ?? null, ['id' => 'filter-district']) ?>
<?= Html::hiddenInput('microdistrict', $params['microdistrict'] ?? null, ['id' => 'filter-microdistrict']) ?>
    <div class="filters">
        <div class="loading-overlay hidden"></div>

        <div class="form-group">
            <div class="filter-title"><?= Yii::t('instance', 'Catalog Mode') ?></div>
            <div>
                <div class="filter-mode">
                    <?= Html::a('<i class="icon-menu"></i>&nbsp;' . Yii::t('instance', 'Default Mode'), '#', [
                        'class' => 'switch-items ' . ($catalogView === 'grid' ? 'font-bold' : ''),
                        'data-view' => 'grid',
                        'data-action' => 'change-catalog-view',
                        'data-url' => Url::current([0 => '/property/catalog/index', 'box' => null, 'zoom' => null])
                    ]) ?>
                </div>
                <div class="filter-mode">
                    <?= Html::a('<i class="icon-gps"></i>&nbsp;' . Yii::t('instance', 'Search by map'), '#', [
                        'class' => 'switch-items ' . ($catalogView === 'map' ? 'font-bold' : ''),
                        'data-view' => 'map',
                        'data-action' => 'change-catalog-view',
                    ]) ?>
                </div>
            </div>
        </div>

        <?php $operationFilter = ArrayHelper::remove($filters, 'operation'); ?>
        <?php if ($operationFilter) { ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-4">
                        <div class="filter-title">
                            <?= $operationFilter['title'] ?>
                        </div>
                    </div>
                    <div class="col-8 d-flex">
                        <?php $checked = ($operationFilter['checked'] ?? $operationFilter['values']); ?>
                        <?php foreach ($operationFilter['values'] as $key => $value) { ?>
                            <div class="filter-choose w-100">
                                <?= Html::radio('operation', $key === $checked, ['value' => $key, 'id' => $key]) ?>
                                <?= Html::tag('label', $value['title'], [
                                    'class' => 'text-center w-100 ' . ($key === $checked ? 'active' : ''),
                                    'for' => $key,
                                ]) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php foreach ($filters as $key => $filter) { ?>
            <?= $this->render("filter/{$filter['type']}", [
                'filter' => $filter,
                'key' => $key
            ]) ?>
        <?php } ?>

        <div class="filter-ops d-flex justify-content-between align-items-center">
            <?= Html::a(Yii::t('instance', 'Reset'), '#', ['class' => '', 'data-action' => 'reset-filter']) ?>
            <div class="mobile-results-count">
                <?= Yii::t('instance', '{count, plural, one{# result} other{# results}}', [
                    'count' => $resultCount
                ]) ?>
            </div>
            <?= Html::submitInput(Yii::t('instance', 'Show'), ['class' => '']) ?>
        </div>
    </div>
<?= Html::endForm() ?>