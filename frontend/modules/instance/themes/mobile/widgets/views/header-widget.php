<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 04.06.2019
 * Time: 15:54
 */


use common\helpers\UtilityHelper;
use common\models\Building;
use common\models\Property;
use frontend\modules\instance\assets\IndexAsset;
use frontend\modules\instance\models\InstanceSetting;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var integer $colorMode
 * @var integer $widthMode
 * @var string $logoImg
 * @var array $languages
 * @var string $logoText
 * @var array $categories
 * @var boolean $isGuest
 */

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);
?>

    <header>
        <div>
            <div class="d-flex justify-content-between desktop-menu align-items-center p-2">
                <nav class="navbar">
                    <a class="drop-button d-block w-100" href="#" data-toggle="collapse"
                       data-target="#top-menu">
                        <div class="gamb-button">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                    </a>
                    <div class="drop-menu shadow-sm mobile-menu text-left text-md-right collapse navbar-collapse"
                         id="top-menu">
                        <div class="nav-first-menu nav-menu">
                            <ul class="navbar-nav top-menu no-list">
                                <li class="d-md-none text-right">
                                    <a class="text-uppercase" href="#" data-toggle="collapse"
                                       data-target="#top-menu">
                                        <?= Yii::t('instance', 'Close') ?>
                                        <span class="cross mobile"></span>
                                    </a>
                                </li>
                                <li class="d-block d-md-inline-block dropdown">
                                    <a class="dropdown-toggle text-uppercase" data-toggle="dropdown" href="#"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="icon-earth-globe"></i>
                                        <?= Yii::t('main', Yii::$app->params['languages'][Yii::$app->language]) ?>
                                    </a>
                                    <div class="dropdown-menu languages-menu">
                                        <?php foreach ($languages as $locale => $code) {
                                            echo Html::a('<b>'
                                                . Yii::t('main', Yii::$app->params['languages'][$locale], [], $locale)
                                                . '</b>&nbsp;'
                                                . mb_strtolower(Yii::t('main', 'Language', [], $locale)), UtilityHelper::localeCurrentUrl($code), [
                                                'class' => $locale === Yii::$app->language ? 'active dropdown-item font-weight-normal' : 'font-weight-normal dropdown-item'
                                            ]);
                                        } ?>
                                    </div>
                                </li>
                                <?php if ($this->params['currentInstance']['settings'][InstanceSetting::SETTING_NEW_CONSTRUCTION]['value'] ?? false) { ?>
                                    <li class="d-block d-md-inline-block">
                                        <?= Html::a(Yii::t('instance', 'New Constructions'), [
                                            '/instance/building/catalog/index', 'operation' => Building::TYPE_NEW_CONSTRUCTION_TEXT
                                        ], ['class' => 'text-uppercase']) ?>
                                    </li>
                                <?php } ?>
                                <?php foreach ($categories as $category) { ?>
                                    <li class="d-block d-md-inline-block">
                                        <?= Html::a($category['title'], [
                                            '/instance/property/catalog/index', 'category' => $category['slug'], 'operation' => Property::OPERATION_SALE
                                        ], ['class' => 'text-uppercase']) ?>
                                    </li>
                                <?php } ?>
                                <li class="d-block d-md-inline-block">
                                    <?= Html::a(Yii::t('instance', 'Contact Us'), '#contact-block', ['class' => 'text-uppercase']) ?>
                                </li>
                                <?php if ($isGuest) { ?>
                                    <li class="d-block d-md-inline-block">
                                        <?= Html::a(Yii::t('instance', 'Log in'), '#', [
                                            'class' => 'text-uppercase',
                                            'data-target' => '#login-modal',
                                            'data-toggle' => 'modal'
                                        ]) ?>
                                    </li>
                                    <li class="d-block d-md-inline-block">
                                        <?= Html::a(Yii::t('instance', 'Registration'), '#', [
                                            'class' => 'text-uppercase',
                                            'data-target' => '#signup-modal',
                                            'data-toggle' => 'modal',
                                        ]) ?>
                                    </li>
                                <?php } else { ?>
                                    <li class="d-block d-md-inline-block">
                                        <?= Html::a(Yii::t('instance', 'Log out'), ['/instance/auth/logout'], [
                                            'class' => 'text-uppercase',
                                        ]) ?>
                                    </li>
                                    <li class="d-block d-md-inline-block">
                                        <a class="text-uppercase d-flex align-items-center h-100 py-0" href="#">
                                            <div class="head-avatar rounded overflow-hidden mr-1">
                                                <?= Html::img($user['avatar'], ['alt' => $user['username'], 'title' => $user['username'], 'class' => 'h-100 mx-auto d-block']); ?>
                                            </div>
                                            <?= $user['username'] ?>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="d-flex justify-content-between align-items-center">
                    <a class="logo text-left" href="/">
                        <?= Html::img($logoImg ?? "{$bundle->baseUrl}/images/default-logo.png", ['alt' => 'LOGO', 'title' => 'LOGO']) ?>
                    </a>
                    <a class="text-uppercase"><?= $logoText?> </a>
                </div>
                <a href="tel:+79122518948" class="call text-center rounded-circle">
                    <i class="icon-phone"></i>
                </a>
            </div>
        </div>
    </header>

<?php $script = <<<JS
    $(document.body).on('click', function(e) { 
      if (!$(e.target).closest("nav").length) {
        $('#top-menu').collapse('hide')
      }
    });
JS;

$this->registerJs($script);