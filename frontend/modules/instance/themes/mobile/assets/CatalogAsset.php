<?php

namespace frontend\modules\instance\themes\mobile\assets;

use frontend\modules\instance\assets\InstanceAsset;
use yii\web\AssetBundle;

/**
 * Class CatalogAsset
 * @package frontend\modules\instance\themes\mobile\assets
 */
class CatalogAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/instance/themes/mobile/web';
    public $css = [
        'css/site/catalog.css'
    ];
    public $js = [

    ];
    public $depends = [
        InstanceAsset::class
    ];
}