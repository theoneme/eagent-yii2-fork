<?php

namespace frontend\modules\instance\helpers;

/**
 * Class FormHelper
 * @package frontend\modules\instance\helpers
 */
class FormHelper
{
    public const ENTITY_INSTANCE = 'instance';

    /**
     * @param $entity
     * @return mixed
     */
    public static function getFormConfig($entity)
    {
        $formConfig = require dirname(__DIR__, 1) . "/config/forms.php";

        switch ($entity) {
            case self::ENTITY_INSTANCE:
            default:
                $config = $formConfig[$entity];

                break;
        }

        return $config;
    }
}