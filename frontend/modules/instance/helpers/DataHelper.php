<?php

namespace frontend\modules\instance\helpers;

use frontend\modules\instance\mappers\BlockMapper;
use frontend\modules\instance\services\entities\BlockService;
use Yii;

/**
 * Class DataHelper
 * @package frontend\modules\instance\helpers
 */
class DataHelper
{
    /**
     * @return array
     */
    public static function getBlocks()
    {
        /** @var BlockService $inst */
        $blockService = Yii::$container->get(BlockService::class);
        $blocks = $blockService->getMany([], ['orderBy' => 'id']);

        return BlockMapper::getMappedData($blocks['items']);
    }

    /**
     * @param array $options
     * @param array $values
     * @return mixed
     */
    public static function sortLanguages($options, $values)
    {
        uksort($options, function ($a) use ($values) {
            return in_array($a, $values) ? -1 : 1;
        });
        return $options;
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function getLocationData($data)
    {
        $result = [
            'entity' => $data['entity'] ?? '',
            'address' => $data['address'] ?? '',
        ];
        switch ($data['entity']) {
            case 'city':
                $result['entity_id'] = $data['city_id'] ?? null;
                break;
            case 'region':
                $result['entity_id'] = $data['region_id'] ?? null;
                break;
            case 'country':
                $result['entity_id'] = $data['country_id'] ?? null;
                break;
        }
        return $result;
    }
}