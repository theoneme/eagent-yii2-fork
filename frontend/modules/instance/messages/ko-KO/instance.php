<?php 
 return [
    ' at {address}' => ' {address}',
    ' «{title}»' => '',
    '+{count} more' => '다른 사람+{count}',
    '- {bedrooms} bd' => '-{bedrooms}SP',
    'About Block' => '블록에 대한 회사',
    'About us' => 'About us',
    'Add Location' => '위치 추가',
    'Add to Favorite' => '즐겨찾기에 추가',
    'Add to compare' => 'Add to compare',
    'Agent' => 'Agent',
    'All Properties' => '모든 객체',
    'Any Price' => '모든 가격',
    'Area {area} m²' => '지역{area}m2',
    'Back' => '전',
    'Back to Sign In' => '다시 로그인',
    'Banner Block' => '단위와 배너',
    'Browse photos' => '사진을 참조하십시오',
    'Building Amenities:' => '의 이점:',
    'Building Features:' => '의 매개변수:',
    'Buy property in Spain' => '에서 부동산을 구매하기 위해 스페인',
    'By clicking JOIN button you agree {privacy} and {terms}, and receive emails from {site}' => '등록 버튼을 클릭할{privacy}{terms},또한 이메일 통지를 받{site}에서',
    'Catalog' => '디렉토리',
    'Catalog Mode' => '카탈로그 모드',
    'Category' => '카테고리',
    'Choose' => '선택',
    'Choose a Password' => '암호를 선택합',
    'Choose your new password' => '선택하신 새로운 비밀번호',
    'Clear' => '청정',
    'Client' => '클라이언트',
    'Close' => '닫기',
    'Compare' => '비',
    'Compare Property' => '의 비교 부동산',
    'Condos in house{title}{address}' => '아파트에서 집{title}{address}',
    'Configure your site' => '당신의 웹 사이트를 사용자 정의',
    'Confirm Password' => '비밀번호 확인',
    'Construction Progress ({quarter} quarter {year} year)' => '건설 진행({quarter}분기{year}년)',
    'Contact Agent' => '에이전트 연락처',
    'Contact Block' => '연락처 블록',
    'Contact Us' => 'Contact us',
    'Contact form will not work unless you specify contact email. {link}' => '연락처 양식을 작동하지 않는 경우에 당신을 지정하지 않은 연락 주시기 바랍니다. {link}',
    'Create Site' => '웹사이트를 만들기',
    'Create an Account' => 'Create an account',
    'Credit application' => '응용 프로그램를 위한 대출',
    'Default Mode' => '표준 모드',
    'Description is missing' => '설명 없음',
    'Desktop version' => '전체 버전',
    'Developer: {developer}' => '개발자:{developer}',
    'Discount {percent}%' => '할인{percent}%',
    'Districts' => '소',
    'Districts and microdistricts' => '지역 및 지구',
    'Do u have an account?' => '당신은 계정이 있나요?',
    'Don\'t have an account?' => '당신은 계정이 없으십니까?',
    'Down Payment' => '초기불',
    'Drag & drop logo here' => '드래그 앤 드롭 로고기',
    'Drag & drop photo here' => '로 끌어진 사진기',
    'Edit mode' => '편집 모드',
    'Email' => '',
    'Enter city, region or country' => '입력하는 도시,지역이나 국가',
    'Enter email address' => '입력하신 이메일 주소',
    'Enter the email you used in registration. A password reset link will be sent to your email' => '입력하시기 바랍 이메일로 등록할 때 사용하는니다. 를 재설정할 수 있는 링크가 귀하의 비밀번호를 발급해 당신에 메일',
    'Enter your Email' => '입력하신 이메일 주소',
    'Enter your Name' => '당신의 이름을 입력하',
    'Enter your email or phone' => '입력하신 이메일이나 전화',
    'Every day new objects come to us which you can see in our catalog.' => '매일 우리는 새로운 객체에서 찾을 수있는 우리 카탈로그입니다.',
    'Experienced agents' => '경제',
    'Explore Block' => '블록 카테고리',
    'Explore Property' => '를 넣체',
    'Filter' => '필터',
    'For Rent' => '임대',
    'For Sale' => '판매',
    'Free website creation for a real estate agency on the platform eAgent.me' => '무료 웹 사이트 생성을 위한 부동산 eAgent 플랫폼입니다.나',
    'From {min}' => '{min}',
    'General Settings' => '일반정',
    'Go To Top' => '위층',
    'Go to settings' => '정',
    'Good day! Thanks to your agency and specialists Svetlana Topoleva and Ekaterina Vorobyeva, we acquired the property we dreamed of. In a short time, they managed to understand our wishes, solve related problems and find exactly what we wanted. Thank you very much for your cooperation!' => 'Good day! 덕분에 당신의 기관 및 전문가 스베틀라나 포플라 및 카테리나 Vorobyeva 우리는 부동산 구매는 꿈입니다. 짧은 시간에 그들은 관리를 이해하는 우리의 요청,문제를 해결하고 정확하게 찾을 수 있습니다. 귀하의 협조에 감사드립니다!',
    'Grid' => 'Mesh',
    'Having read reviews on other sites, went cautiously to the agency. Although I know that a lot of reviews are also customized. "I will not believe until I check." It\'s about me. Our deal (real estate purchase) was led by Svetlana Topoleva. She really has a high level of professionalism, up to date. He is able to find an approach to the client, during the connection to the negotiation process (as we did). To all my fears, approached with understanding. If you need help, I will definitely recommend.' => '데에 다른 사이트를,걸을 조심스럽게하는 기관입니다. 지만 저는 많은 없는 또한 사용자 정의합니다. "내가 믿지 않을 때까지 당신은 그것을 확인하십시오". 그것은 나에 대해합니다. 우리의 처리(판매)led 스베틀라나 Topoleva 니다. 그녀는 정말은 높은 수준의 전문성,최신 상태에 모든 이벤트이다. 을 찾는 방법을 알고 접근하는 모든 클라이언트가 연결되어 있는 동안은 협상과정(하). 내 모든 두려움과 함께 이해합니다. 도움이 필요한 경우,확실히 좋습니다.',
    'Here are some Similar buildings:' => '와 유사한 건축물',
    'Here is some Similar property:' => '비슷한 항목:',
    'Hide other languages' => '숨기기 다른 언어',
    'Home' => '홈',
    'If you want to buy with property using mortgage, begin with sending request with button below' => '당신이 구입하려는 경우 제공을 통해 주시기 시작과 함께 보내는 응용 프로그램을 통해 아',
    'If you want to buy, rent or sell property, please contact us and we will be glad to provide you with professional services in real estate.' => '당신이 구입하려는 경우,대여 또는 판매하는 부동산,십시오 우리 행복할 것이다 당신에게 제공과 전문 서비스 분야에서의 부동산합니다.',
    'Include Insurance' => '보험을 포함',
    'Interest Rate' => '관심 속도',
    'It is easy and takes 15 min maximum' => '그것은 쉽게 걸리는 더 이상 15 분',
    'It takes few minutes' => '그것은 몇 분 정도 걸릴 것입',
    'Join' => '등록',
    'Learn More' => 'Learn more',
    'Legal support' => '법적 지원',
    'Loan Term' => '성숙',
    'Local market knowledge' => '의 지식을 현지 시장',
    'Log in' => '로그인',
    'Log out' => '나',
    'MORE DETAILS' => '자세히보기',
    'Manage your own real estate site' => '당신의 자신을 만들은 부동산 웹사이트',
    'Manager' => '관리자',
    'Map' => '지도',
    'Max' => 'Max',
    'Menu' => '메뉴',
    'Message' => '메시지',
    'Message from contact form' => '메시지에서 문의 양식',
    'Microdistricts' => '지역',
    'Min' => '도',
    'Mobile version' => '모바일 버전',
    'Monthly Payment' => '월별 지급',
    'More' => 'More',
    'Mortgage' => '담보',
    'Mortgage calculator' => '저당 계산기',
    'Mortgage {percent}%' => '담보{percent}%',
    'New Construction' => '뉴',
    'New Constructions' => '건축물',
    'New Password' => '새로운 비밀번호',
    'Next Page' => '다음 페이지',
    'No' => 'No',
    'No attributes specified' => '이 매개 변수를 지정',
    'No districts were found for this region' => '소 이 지역에서 발견되지 않았',
    'No microdistricts were found for this region' => '지역 이 지역에서 발견되지 않았',
    'No results found' => '아무것도 발견',
    'Our agency will help you find the best offers for apartments and houses in Spain.' => '우리의 기관을 선택하는 데 도움이 됩 최고의 거래에 아파트와 가정에서는 스페인.',
    'Our company employs 15 experienced agents who are licensed to work with real estate in Spain.' => '우리의 회사는 15 경험있는 에이전트가 작동하도록 허가 스페인에서 부동산합니다.',
    'Our company employs lawyers who will help you arrange real estate in accordance with all local laws and will help verify property rights and protect against risks.' => '우리의 회사는 변호사를 고용하는 데 도움이 될 것입니다 문제는 제공 받은 모든 현지 법률과하는 데 도움이 될 것입 확인 소유권을 보호하기 위해에 대한 위험이 있습니다.',
    'Our company employs professionals who will help not only to find the best real estate offers, but also to issue a deal.' => '우리의 회사는 전문가를 고용하지 않습니다 사람만을 찾아 최고의 거래에서 부동산,그러나 또한 거래를 할 수 있습니다.',
    'Our newest property' => '우리의 최신 뉴스',
    'Our team. Real Estate Agency {name}' => '우리의 팀이 있습니다. 부동산{name}',
    'Password' => '비밀번호',
    'Phone' => '전화',
    'Photo' => '사진',
    'Prev Page' => '이전 페이지',
    'Price' => '가격',
    'Privacy Policy' => '개인 정보 보호 정책',
    'Probably, there are no such words as to express my gratitude to Artem Kovalenko! Incredible professional, but most importantly - extraordinary kindness people. Thanks for the help of cooperation!' => '아마 없는 말로 표현할 수 있는 내 감사하는 아르 코 발렌 코! 놀라운 전문적인,하지만 가장 중요한 특별한 친절의 사람들입니다. 당신의 도움에 대한 감사와 협력!',
    'Properties Block' => '블록체',
    'Property Features:' => '개체 매개변수:',
    'Property Price' => '개체의 가격',
    'Property listed by {owner}' => '객체에 배치 사용자에 의해{owner}',
    'Rating:' => '평가:',
    'Ready Year {year}' => '완료 년{year}',
    'Recent Properties For Sale' => '새로운 특성을 판매',
    'Registration' => '체크',
    'Remove from compare' => '에서 제거하고 비교',
    'Rent' => '임대',
    'Requested building page is not found' => 'Zaprashivala 건물이 발견되지 않았',
    'Reset' => 'Reset',
    'Reset Password' => '비밀번호 재설정',
    'Reviews' => '리뷰',
    'Reviews Block' => '단위 리뷰',
    'Sale' => '판매',
    'Save Changes' => '변경 사항을 저장',
    'Search' => '검색',
    'Search by map' => '지도로 검색',
    'Secondary Building' => '재판매가',
    'Select Blocks' => '선택하 블록',
    'Select Categories' => '카테고리 선택',
    'Select Languages' => '언어 선택',
    'Select Locations' => '위치 선택',
    'Select Operations' => '선택 작업체',
    'Select {attribute}' => '선택{attribute}',
    'Send Request' => '요청을 보내기',
    'Sender Email' => '이메일 발송',
    'Sender Name' => '의 이름을 보낸 사람',
    'Sent from "{name}" site' => '에서 보낸 사이트"{name}"',
    'Settings' => '설정',
    'Share' => '공유',
    'Show' => '보',
    'Show New Constructions' => '쇼 새로운 건축물',
    'Show Other Listings' => '쇼 다른 객체',
    'Show other languages' => '쇼 다른 언어',
    'Sign In' => '로그인',
    'Sign Up' => '등록',
    'Sign in to your account' => '귀하의 계정에 로그인',
    'Site Blocks' => '웹사이트 블록',
    'Site Categories' => '사이트 카테고리',
    'Site Favicon' => '파비콘의 웹사이트',
    'Site Languages' => '사이트 언어',
    'Site Locations' => '사이트 위치',
    'Site Logo' => '웹사이트에 로고',
    'Site Name' => '의 이름이트',
    'Site Operations' => '부동산 트랜잭션',
    'Specify contact email' => '이메일 주소',
    'Specify logo text' => '텍스트를 지정 다하고',
    'Start typing and select one of the options' => '입력하기 시작 중 하나를 선택하여 사용할 수 있는 옵션',
    'Start typing your location' => '입력하기 시작 위치',
    'Step {first} from {total}' => '단계{first}{total}',
    'Subject' => '테마',
    'Submit' => '보내기',
    'Table' => '테이블',
    'Team Block' => '팀 단위',
    'Team member description' => '설명 팀 구성원',
    'Terms And Conditions' => '계약 조건',
    'The advantages of working with our Real Estate Agency {name}' => '작업의 장점이 우리의 기관{name}',
    'This is mortgage monthly payment with provided parameters' => '월별 지급에서 지정된 매개변수',
    'To {max}' => '{max}',
    'Unavailable' => '사용할 수 없',
    'Unknown type' => '알 수 없음',
    'Upload Site Favicon' => '파비콘을 업로드한 사이트',
    'Upload Site Logo' => '사이트 업로드 로고',
    'Upload favicon of your site' => '파비콘을 업로드 웹사이트의',
    'Upload file' => '파일 다운로드',
    'Upload logo of your site' => '업로드하고 당신의 웹사이트',
    'Useful Links' => '유용한 링크',
    'Video Tour:' => '비디오 투:',
    'View' => '보',
    'View Info' => '디스플레이 설정',
    'We are engaged in real estate for 10 years. We are well versed in local legislation and will help you arrange a property.' => '우리는 부동산에 종사하는 10 년 동안 유효합니다. 우리는 잘에 정통한 현지 법률과 당신이 도움이 될 것입을 얻을 제공합니다.',
    'We are offering property in Spain' => '우리가 제공하는 숙박 시설에는 스페인',
    'We are working for you' => '우리는 당신을 위해 일',
    'We will help you choose and arrange a property. And do it with professionalism and love.' => '우리는 당신을 도움이 될 것입니다 선택하고 부동산을 구매합니다. 고 그것을 전문성과 사랑합니다.',
    'Welcome home!' => '집에 오신 것을 환영',
    'Write Your Text' => '메시지를 작성',
    'Yes' => '네',
    'Your Email' => '이메일',
    'Your Email Address' => '이메일 주소',
    'Your Message' => '귀하의 메시지',
    'Your Name' => '이름',
    'Your Phone' => '휴대폰',
    'Your Property' => '귀하의 재산',
    'explore more' => '더보기',
    'month' => '달',
    'near {address}' => '에 대한{address}',
    'per month' => '매달',
    'with privacy policy' => '개인 정보 보호 정책',
    'with terms of service' => '사용자와 계약',
    '{area} m²' => '{지역]m2',
    '{bathrooms, plural, one{# bathroom} other{# bathrooms}}' => '{bathrooms, plural, one{#WC} few{#욕실} other{#욕실}}',
    '{bathrooms} bathrooms' => '{bathrooms}욕실',
    '{bedrooms, plural, one{# bedroom} other{# bedrooms}}' => '{bedrooms, plural, one{#침실} few{#침실} other{#침실}}',
    '{condos, plural, one{# condo} other{# condos}}' => '{condos, plural, one{#평} few{#평} other{#아파트}}',
    '{count, plural, one{# photo} other{# photos}}' => '{count, plural, one{#사진} few{#사진} other{#사진}}',
    '{count, plural, one{# result} other{# results}}' => '{count, plural, one{#결과} few{#결과} other{#결과}}',
    '{entity} not found' => '{entity}발견되지 않았',
    '{flats, plural, one{# flat} other{# flats}} for rent' => '{flats, plural, one{#평} few{#평} other{#아파트}}for rent',
    '{flats, plural, one{# flat} other{# flats}} for sale' => '{flats, plural, one{#평} few{#평} other{#아파트}}판매',
    '{floors, plural, one{# floor} other{# floors}}' => '{floors, plural, one{#층} few{#층} other{#의 바닥}}',
    '{garages, plural, one{# garage} other{# garages}}' => '{garages, plural, one{#차고} few{#차고} other{#의 차고}}',
    '{garages} garages' => '{garages}주차장',
    '{rooms, plural, one{# room} other{# rooms}}' => '{rooms, plural, one{#실} few{#실} other{#실}}',
    '{rooms} rooms' => '{rooms}객실',
    '{years, plural, one{# year} other{# years}}' => '{years, plural, one{#년} few{#년} other{#년}}',
];