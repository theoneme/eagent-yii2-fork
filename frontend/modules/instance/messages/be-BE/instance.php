<?php 
 return [
    ' at {address}' => ' па адрасе {address}',
    ' «{title}»' => '',
    '+{count} more' => 'яшчэ +{count}',
    '- {bedrooms} bd' => '- {bedrooms} сп',
    'About Block' => 'Блок аб кампаніі',
    'About us' => 'Аб нас',
    'Add Location' => 'Дадаць размяшчэнне',
    'Add to Favorite' => 'Дадаць у абранае',
    'Add to compare' => 'Дадаць у параўнанне',
    'Agent' => 'Агент',
    'All Properties' => 'Усе аб\'екты',
    'Any Price' => 'Любая цана',
    'Area {area} m²' => 'Плошча {area} м2',
    'Back' => 'Таму',
    'Back to Sign In' => 'Вярнуцца да аўтарызацыі',
    'Banner Block' => 'Блок з банэрам',
    'Browse photos' => 'Паглядзець фота',
    'Building Amenities:' => 'Перавагі будынка:',
    'Building Features:' => 'Параметры будынка:',
    'Buy property in Spain' => 'Купіць нерухомасць у Іспаніі',
    'By clicking JOIN button you agree {privacy} and {terms}, and receive emails from {site}' => 'Націснуўшы на кнопку ЗАРЭГІСТРАВАЦЦА, вы згаджаецеся {privacy} і {terms}, а таксама на атрыманне Email апавяшчэнняў ад {site}',
    'Catalog' => 'Каталог',
    'Catalog Mode' => 'Рэжым каталога',
    'Category' => 'Катэгорыя',
    'Choose' => 'Выбраць',
    'Choose a Password' => 'Выберыце пароль',
    'Choose your new password' => 'Выберыце ваш новы пароль',
    'Clear' => 'Ачысціць',
    'Client' => 'Кліент',
    'Close' => 'Зачыніць',
    'Compare' => 'Параўнаць',
    'Compare Property' => 'Параўнанне нерухомасці',
    'Condos in house{title}{address}' => 'Кватэры ў доме{title}{address}',
    'Configure your site' => 'Наладзьце ваш сайт',
    'Confirm Password' => 'Пацвердзіце пароль',
    'Construction Progress ({quarter} quarter {year} year)' => 'Ход будаўніцтва ({quarter} квартал {year} года)',
    'Contact Agent' => 'Звязацца з агентам',
    'Contact Block' => 'Блок з кантактамі',
    'Contact Us' => 'Звязацца з намі',
    'Contact form will not work unless you specify contact email. {link}' => 'Кантактная форма не будзе працаваць, калі вы не пакажаце кантактны email. {link}',
    'Create Site' => 'Стварыць сайт',
    'Create an Account' => 'Стварыць рахунак',
    'Credit application' => 'Заяўка на крэдыт',
    'Default Mode' => 'Стандартны рэжым',
    'Description is missing' => 'Апісанне адсутнічае',
    'Desktop version' => 'Поўная версія',
    'Developer: {developer}' => 'Забудоўшчык: {developer}',
    'Discount {percent}%' => 'Зніжка {percent}%',
    'Districts' => 'Раёны',
    'Districts and microdistricts' => 'Раёны і мікрараёны',
    'Do u have an account?' => 'У вас ёсць рахунак?',
    'Don\'t have an account?' => 'У вас няма акаўнта?',
    'Down Payment' => 'Пачатковы ўзнос',
    'Drag & drop logo here' => 'Перацягнуць сюды лагатып',
    'Drag & drop photo here' => 'Перацягнуць фота сюды',
    'Edit mode' => 'Рэжым рэдагавання',
    'Email' => '',
    'Enter city, region or country' => 'Калі ласка, увядзіце горад, рэгіён або краіну',
    'Enter email address' => 'Калі ласка, увядзіце email адрас',
    'Enter the email you used in registration. A password reset link will be sent to your email' => 'Калі ласка, увядзіце e-mail, які вы выкарыстоўвалі пры рэгістрацыі. Спасылка для скіду пароля прыйдзе вам на пошту',
    'Enter your Email' => 'Калі ласка, увядзіце ваш email',
    'Enter your Name' => 'Калі ласка, увядзіце ваша імя',
    'Enter your email or phone' => 'Калі ласка, увядзіце ваш e-mail або тэлефон',
    'Every day new objects come to us which you can see in our catalog.' => 'Кожны дзень да нас прыходзяць новыя аб\'екты якія вы можаце паглядзець у нашым каталогу.',
    'Experienced agents' => 'Дасведчаныя агенты',
    'Explore Block' => 'Блок з катэгорыямі',
    'Explore Property' => 'Прагледзець аб\'екты',
    'Filter' => 'Фільтр',
    'For Rent' => 'У арэнду',
    'For Sale' => 'На продаж',
    'Free website creation for a real estate agency on the platform eAgent.me' => 'Бясплатнае стварэнне сайта для Агенцтва нерухомасці на платформе eAgent.me',
    'From {min}' => 'Ад {min}',
    'General Settings' => 'Агульныя налады',
    'Go To Top' => 'Наверх',
    'Go to settings' => 'Перайсці да налад',
    'Good day! Thanks to your agency and specialists Svetlana Topoleva and Ekaterina Vorobyeva, we acquired the property we dreamed of. In a short time, they managed to understand our wishes, solve related problems and find exactly what we wanted. Thank you very much for your cooperation!' => 'Добры дзень! Дзякуючы вашаму агенцтву і спецыялістам Святлане Таполевай і Кацярыны Вараб\'ёвай мы набылі тую нерухомасць, пра якую марылі. За кароткі час ім удалося зразумець нашы пажаданні, вырашыць звязаныя з гэтым праблемы і знайсці менавіта тое, што мы хацелі. Вялікае дзякуй за супрацоўніцтва!',
    'Grid' => 'Сетка',
    'Having read reviews on other sites, went cautiously to the agency. Although I know that a lot of reviews are also customized. "I will not believe until I check." It\'s about me. Our deal (real estate purchase) was led by Svetlana Topoleva. She really has a high level of professionalism, up to date. He is able to find an approach to the client, during the connection to the negotiation process (as we did). To all my fears, approached with understanding. If you need help, I will definitely recommend.' => 'Начытаўшыся водгукаў на іншых сайтах, ішла з асцярогай ў агенцтва. Хоць ведаю, што шмат водгукаў таксама ёсць заказныя. "Не паверу, пакуль не праверу". Гэта пра мяне. Нашу здзелку (купля нерухомасці) вяла Святлана Тополева. Яна сапраўды мае высокі ўзровень прафесійнасці, у курсе ўсіх падзей. Умее знайсці падыход да кліенту, падчас падключыцца да перамоўнага працэсу (як было ў нас). Да ўсіх маім страхам, падыходзіла з разуменнем. Калі спатрэбіцца дапамога, адназначна буду рэкамендаваць.',
    'Here are some Similar buildings:' => 'Падобныя будынкі:',
    'Here is some Similar property:' => 'Падобныя аб\'екты:',
    'Hide other languages' => 'Схаваць іншыя мовы',
    'Home' => 'Дадому',
    'If you want to buy with property using mortgage, begin with sending request with button below' => 'Калі вы хочаце купіць гэтую нерухомасць праз іпатэку, пачніце з адпраўкі заяўкі праз кнопку знізу',
    'If you want to buy, rent or sell property, please contact us and we will be glad to provide you with professional services in real estate.' => 'Калі вы хочаце купіць, арандаваць або прадаць нерухомасць, напішыце, калі ласка, нам і мы будзем рады аказаць вам прафесійныя паслугі ў сферы нерухомасці.',
    'Include Insurance' => 'Ўключыць страхоўку',
    'Interest Rate' => 'Працэнтная стаўка',
    'It is easy and takes 15 min maximum' => 'Гэта проста і зойме не больш за 15 хвілін',
    'It takes few minutes' => 'Гэта зойме некалькі хвілін',
    'Join' => 'Зарэгістравацца',
    'Learn More' => 'Даведайцеся больш',
    'Legal support' => 'Юрыдычнае суправаджэнне',
    'Loan Term' => 'Тэрмін пагашэння',
    'Local market knowledge' => 'Веданне мясцовага рынку',
    'Log in' => 'Увайсці',
    'Log out' => 'Выйсці',
    'MORE DETAILS' => 'БОЛЬШ падрабязную інфармацыю',
    'Manage your own real estate site' => 'Стварыце ваш уласны сайт па нерухомасці',
    'Manager' => 'Менеджэр',
    'Map' => 'Карта',
    'Max' => 'Максімум',
    'Menu' => 'Меню',
    'Message' => 'Паведамленне',
    'Message from contact form' => 'Паведамленне з кантактнай формы',
    'Microdistricts' => 'Мікрараёны',
    'Min' => 'Мінімум',
    'Mobile version' => 'Мабільная версія',
    'Monthly Payment' => 'Штомесячны плацёж',
    'More' => 'Больш',
    'Mortgage' => 'Іпатэка',
    'Mortgage calculator' => 'Калькулятар іпатэкі',
    'Mortgage {percent}%' => 'Іпатэка {percent}%',
    'New Construction' => 'Новабудоўля',
    'New Constructions' => 'Новабудоўлі',
    'New Password' => 'Новы пароль',
    'Next Page' => 'Наступная старонка',
    'No' => 'Няма',
    'No attributes specified' => 'Параметры не пазначаны',
    'No districts were found for this region' => 'Раёны ў дадзеным рэгіёне не знойдзены',
    'No microdistricts were found for this region' => 'Мікрараёны ў дадзеным рэгіёне не знойдзены',
    'No results found' => 'Нічога не знойдзена',
    'Our agency will help you find the best offers for apartments and houses in Spain.' => 'Наша агенцтва дапаможа падабраць выгадныя прапановы па кватэрах і хатах у Іспаніі.',
    'Our company employs 15 experienced agents who are licensed to work with real estate in Spain.' => 'У нашай кампаніі працуе 15 вопытных агентаў, якія маюць ліцэнзіі на працу з нерухомасцю ў Іспаніі.',
    'Our company employs lawyers who will help you arrange real estate in accordance with all local laws and will help verify property rights and protect against risks.' => 'У нашай кампаніі працуюць юрысты якія дапамогуць аформіць нерухомасць з улікам усіх мясцовых законаў і дапамогуць праверыць права ўласнасці і засцерагчы ад рызык.',
    'Our company employs professionals who will help not only to find the best real estate offers, but also to issue a deal.' => 'У нашай кампаніі працуюць прафесіяналы, якія дапамогуць не толькі падабраць лепшыя прапановы па нерухомасці, але таксама аформіць здзелку.',
    'Our newest property' => 'Нашы апошнія навінкі',
    'Our team. Real Estate Agency {name}' => 'Наша каманда. Агенцтва Нерухомасці {name}',
    'Password' => 'Пароль',
    'Phone' => 'Тэлефон',
    'Photo' => 'Фота',
    'Prev Page' => 'Папярэдняя старонка',
    'Price' => 'Цана',
    'Privacy Policy' => 'Палітыка прыватнасці',
    'Probably, there are no such words as to express my gratitude to Artem Kovalenko! Incredible professional, but most importantly - extraordinary kindness people. Thanks for the help of cooperation!' => 'Напэўна, няма такіх слоў, якімі можна выказаць маю падзяку Арцёму Каваленка! Неверагодны прафесіянал, але самае галоўнае - незвычайнай дабрыні чалавек. Дзякуй за дапамогу і супрацоўніцтва!',
    'Properties Block' => 'Блок з аб\'ектамі',
    'Property Features:' => 'Параметры аб\'екта:',
    'Property Price' => 'Кошт аб\'екта',
    'Property listed by {owner}' => 'Аб\'ект размешчаны карыстальнікам {owner}',
    'Rating:' => 'Рэйтынг:',
    'Ready Year {year}' => 'Год здачы {year}',
    'Recent Properties For Sale' => 'Новыя аб\'екты на продаж',
    'Registration' => 'Рэгістрацыя',
    'Remove from compare' => 'Прыбраць з параўнання',
    'Rent' => 'Арэнда',
    'Requested building page is not found' => 'Запрашивамое будынак не знойдзена',
    'Reset' => 'Скінуць',
    'Reset Password' => 'Скід пароля',
    'Reviews' => 'Водгукі',
    'Reviews Block' => 'Блок з водгукамі',
    'Sale' => 'Продаж',
    'Save Changes' => 'Захаваць змены',
    'Search' => 'Пошук',
    'Search by map' => 'Пошук па карце',
    'Secondary Building' => 'Другасны рынак',
    'Select Blocks' => 'Выберыце блокі',
    'Select Categories' => 'Выберыце катэгорыі',
    'Select Languages' => 'Выберыце мовы',
    'Select Locations' => 'Выберыце размяшчэння',
    'Select Operations' => 'Выберыце аперацыі з аб\'ектамі',
    'Select {attribute}' => 'Выберыце {attribute}',
    'Send Request' => 'Адправіць запыт',
    'Sender Email' => 'Email адпраўніка',
    'Sender Name' => 'Імя адпраўніка',
    'Sent from "{name}" site' => 'Адпраўлена з сайта "{name}"',
    'Settings' => 'Налады',
    'Share' => 'Падзяліцца',
    'Show' => 'Паказаць',
    'Show New Constructions' => 'Паказваць новабудоўлі',
    'Show Other Listings' => 'Паказаць іншыя аб\'екты',
    'Show other languages' => 'Паказаць іншыя мовы',
    'Sign In' => 'Увайсці',
    'Sign Up' => 'Зарэгістравацца',
    'Sign in to your account' => 'Ўвайдзіце ў свой рахунак',
    'Site Blocks' => 'Блокі сайта',
    'Site Categories' => 'Катэгорыі сайта',
    'Site Favicon' => 'Favicon сайта',
    'Site Languages' => 'Мовы сайта',
    'Site Locations' => 'Размяшчэння сайта',
    'Site Logo' => 'Лагатып сайта',
    'Site Name' => 'Назва сайта',
    'Site Operations' => 'Аперацыі з нерухомасцю',
    'Specify contact email' => 'Пазначце кантактны email',
    'Specify logo text' => 'Пакажыце тэкст побач з лагатыпам',
    'Start typing and select one of the options' => 'Пачніце ўводзіць і абярыце адзін з прапанаваных варыянтаў',
    'Start typing your location' => 'Пачніце ўводзіць месцазнаходжанне',
    'Step {first} from {total}' => 'Крок {first} з {total}',
    'Subject' => 'Тэма',
    'Submit' => 'Адправіць',
    'Table' => 'Табліца',
    'Team Block' => 'Блок з камандай',
    'Team member description' => 'Апісанне члена каманды',
    'Terms And Conditions' => 'Умовы пагаднення',
    'The advantages of working with our Real Estate Agency {name}' => 'Перавагі працы з нашым Агенцтвам Нерухомасці {name}',
    'This is mortgage monthly payment with provided parameters' => 'Памесячны плацёж па ўказаных параметрах',
    'To {max}' => 'Да {max}',
    'Unavailable' => 'Недаступна',
    'Unknown type' => 'Невядомы тып',
    'Upload Site Favicon' => 'Загрузіце Favicon для сайта',
    'Upload Site Logo' => 'Загрузіце лагатып сайта',
    'Upload favicon of your site' => 'Загрузіце Favicon вашага сайта',
    'Upload file' => 'Загрузіць файл',
    'Upload logo of your site' => 'Загрузіце лагатып вашага сайта',
    'Useful Links' => 'Карысныя спасылкі',
    'Video Tour:' => 'Видеотур:',
    'View' => 'Прагляд',
    'View Info' => 'Налады адлюстравання',
    'We are engaged in real estate for 10 years. We are well versed in local legislation and will help you arrange a property.' => 'Мы займаемся нерухомасцю 10 гадоў. Мы добра разбіраемся ў мясцовым заканадаўстве і дапаможам вам аформіць нерухомасць.',
    'We are offering property in Spain' => 'Мы прапануем нерухомасць у Іспаніі',
    'We are working for you' => 'Мы працуем для вас',
    'We will help you choose and arrange a property. And do it with professionalism and love.' => 'Мы дапаможам падабраць і аформіць нерухомасць. І выканаем гэта з прафесіяналізмам і любоўю.',
    'Welcome home!' => 'Сардэчна запрашаем дадому',
    'Write Your Text' => 'Напішыце паведамленне',
    'Yes' => 'Ды',
    'Your Email' => 'Ваш Email',
    'Your Email Address' => 'Ваш email адрас',
    'Your Message' => 'Ваша паведамленне',
    'Your Name' => 'Ваша імя',
    'Your Phone' => 'Ваш тэлефон',
    'Your Property' => 'Ваша нерухомасць',
    'explore more' => 'глядзець далей',
    'month' => 'месяц',
    'near {address}' => 'каля {address}',
    'per month' => 'кожны месяц',
    'with privacy policy' => 'з палітыкай прыватнасці',
    'with terms of service' => 'з карыстацкім пагадненнем',
    '{area} m²' => '{area} m2',
    '{bathrooms, plural, one{# bathroom} other{# bathrooms}}' => '{bathrooms, plural, one{# санвузел} few{# санвузла} other{# санвузлоў}}',
    '{bathrooms} bathrooms' => '{bathrooms} санвузлоў',
    '{bedrooms, plural, one{# bedroom} other{# bedrooms}}' => '{bedrooms, plural, one{# спальня} few{# спальні} other{# спальняў}}',
    '{condos, plural, one{# condo} other{# condos}}' => '{condos, plural, one{# кватэра} few{# кватэры} other{# кватэр}}',
    '{count, plural, one{# photo} other{# photos}}' => '{count, plural, one{# фатаграфія} few{# фатаграфіі} other{# фатаграфій}}',
    '{count, plural, one{# result} other{# results}}' => '{count, plural, one{# вынік} few{# выніку} other{# вынікаў}}',
    '{entity} not found' => '{entity} не знойдзена',
    '{flats, plural, one{# flat} other{# flats}} for rent' => '{flats, plural, one{# кватэра} few{# кватэры} other{# кватэр}} у арэнду',
    '{flats, plural, one{# flat} other{# flats}} for sale' => '{flats, plural, one{# кватэра} few{# кватэры} other{# кватэр}} на продаж',
    '{floors, plural, one{# floor} other{# floors}}' => '{floors, plural, one{# паверх} few{# паверха} other{# паверхаў}}',
    '{garages, plural, one{# garage} other{# garages}}' => '{garages, plural, one{# гараж} few{# гаража} other{# гаражоў}}',
    '{garages} garages' => '{garages} гаражоў',
    '{rooms, plural, one{# room} other{# rooms}}' => '{rooms, plural, one{# пакой} few{# пакоя} other{# пакояў}}',
    '{rooms} rooms' => '{rooms} пакояў',
    '{years, plural, one{# year} other{# years}}' => '{years, plural, one{# год} few{# года} other{# гадоў}}',
];