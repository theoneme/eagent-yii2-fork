<?php 
 return [
    ' at {address}' => ' {address}',
    ' «{title}»' => '',
    '+{count} more' => 'muuta +{count}',
    '- {bedrooms} bd' => '- {bedrooms} SP',
    'About Block' => 'Lohko yrityksen',
    'About us' => 'Meistä',
    'Add Location' => 'Lisää sijainti',
    'Add to Favorite' => 'Lisää suosikkeihin',
    'Add to compare' => 'Lisää vertailuun',
    'Agent' => 'Agentti',
    'All Properties' => 'Kaikki esineet',
    'Any Price' => 'Mistään hinnasta',
    'Area {area} m²' => 'Alue on {area} m2',
    'Back' => 'Sitten',
    'Back to Sign In' => 'Takaisin login',
    'Banner Block' => 'Yksikkö banneri',
    'Browse photos' => 'Katso kuvat',
    'Building Amenities:' => 'Edut rakennus:',
    'Building Features:' => 'Parametrit rakennus:',
    'Buy property in Spain' => 'Ostaa omaisuutta Espanjassa',
    'By clicking JOIN button you agree {privacy} and {terms}, and receive emails from {site}' => 'Klikkaamalla REKISTERÖIDY-painiketta hyväksyt {privacy} ja {terms}, ja myös vastaanottaa Sähköposti-ilmoituksia {site}',
    'Catalog' => 'Hakemisto',
    'Catalog Mode' => 'Catalog tila',
    'Category' => 'Luokka',
    'Choose' => 'Valitse',
    'Choose a Password' => 'Valitse salasana',
    'Choose your new password' => 'Valitse uusi salasana',
    'Clear' => 'Puhdas',
    'Client' => 'Asiakas',
    'Close' => 'Lähellä',
    'Compare' => 'Vertaa',
    'Compare Property' => 'Vertailu kiinteistöjen',
    'Condos in house{title}{address}' => 'Huoneisto talossa{title}{address}',
    'Configure your site' => 'Muokata sivustosi',
    'Confirm Password' => 'Vahvista salasana',
    'Construction Progress ({quarter} quarter {year} year)' => 'Rakentamisen edistymistä ({quarter} neljänneksellä {year} vuosi)',
    'Contact Agent' => 'Ota yhteyttä agentti',
    'Contact Block' => 'Yhteystiedot lohko',
    'Contact Us' => 'Ota yhteyttä',
    'Contact form will not work unless you specify contact email. {link}' => 'Yhteydenottolomake ei toimi, jos et määritä yhteyttä sähköpostitse. {link}',
    'Create Site' => 'Luoda verkkosivusto',
    'Create an Account' => 'Luo tili',
    'Credit application' => 'Hakemus laina',
    'Default Mode' => 'Standard-tilassa',
    'Description is missing' => 'Kuvausta ei ole saatavilla',
    'Desktop version' => 'Täysi versio',
    'Developer: {developer}' => 'Kehittäjä: {developer}',
    'Discount {percent}%' => 'Alennus {percent}%',
    'Districts' => 'Alueilla',
    'Districts and microdistricts' => 'Alueet ja piirit',
    'Do u have an account?' => 'Sinulla on tili?',
    'Don\'t have an account?' => 'Sinä älä ole tiliä?',
    'Down Payment' => 'Aloitusmaksu',
    'Drag & drop logo here' => 'Vedä ja pudota logo täällä',
    'Drag & drop photo here' => 'Vedä kuva täällä',
    'Edit mode' => 'Edit-tilassa',
    'Email' => '',
    'Enter city, region or country' => 'Kirjoita kaupunki, alue tai maa',
    'Enter email address' => 'Kirjoita sähköpostiosoitteesi',
    'Enter the email you used in registration. A password reset link will be sent to your email' => 'Syötä sähköpostiosoite, jota käytit rekisteröityessäsi. Linkki uuden salasanan lähetetään sinulle postitse',
    'Enter your Email' => 'Kirjoita sähköpostiosoitteesi',
    'Enter your Name' => 'Kirjoita nimesi',
    'Enter your email or phone' => 'Kirjoita sähköpostiosoite tai puhelin',
    'Every day new objects come to us which you can see in our catalog.' => 'Joka päivä saamme uusia esineitä, jotka voit löytää luettelo.',
    'Experienced agents' => 'Kokenut aineet',
    'Explore Block' => 'Lohko luokat',
    'Explore Property' => 'Voit tarkastella objekteja',
    'Filter' => 'Suodatin',
    'For Rent' => 'Vuokraa',
    'For Sale' => 'Myytävänä',
    'Free website creation for a real estate agency on the platform eAgent.me' => 'Ilmainen web-sivuston luominen-real estate Agency eAgent alustan.minua',
    'From {min}' => 'Alkaen {min}',
    'General Settings' => 'Yleiset asetukset',
    'Go To Top' => 'Yläkerrassa',
    'Go to settings' => 'Mene asetukset',
    'Good day! Thanks to your agency and specialists Svetlana Topoleva and Ekaterina Vorobyeva, we acquired the property we dreamed of. In a short time, they managed to understand our wishes, solve related problems and find exactly what we wanted. Thank you very much for your cooperation!' => 'Hyvä päivä! Kiitos Viraston ja asiantuntija Svetlana Poppeli ja Ekaterina Vorobyeva me osti kiinteistön, jolla oli unelma. Lyhyessä ajassa he onnistuivat ymmärtää pyyntöjä, ratkaista ongelmia ja löytää juuri sitä, mitä halusimme. Kiitos yhteistyöstä!',
    'Grid' => 'Mesh',
    'Having read reviews on other sites, went cautiously to the agency. Although I know that a lot of reviews are also customized. "I will not believe until I check." It\'s about me. Our deal (real estate purchase) was led by Svetlana Topoleva. She really has a high level of professionalism, up to date. He is able to find an approach to the client, during the connection to the negotiation process (as we did). To all my fears, approached with understanding. If you need help, I will definitely recommend.' => 'Luettuani arvosteluja muissa sivustoissa, käveli varovaisesti Virasto. Vaikka tiedän, että paljon arvostelua on myös mukautettu. "En usko ennen kuin check it out". Se on minusta. Meidän kauppa (myynti) led Svetlana Topoleva. Hän todella on korkea ammattitaito, ajan tasalla kaikki tapahtumia. Tietää, miten löytää lähestymistapa jokaiselle asiakkaalle, kun se on yhdistetty neuvottelu-prosessi (kuin me). Kaikki pelkoni, tuli ymmärrystä. Jos tarvitset apua, ehdottomasti suosittelen.',
    'Here are some Similar buildings:' => 'Vastaavia rakennuksia:',
    'Here is some Similar property:' => 'Samanlaisia kohteita:',
    'Hide other languages' => 'Piilottaa muut kielet',
    'Home' => 'Kotiin',
    'If you want to buy with property using mortgage, begin with sending request with button below' => 'Jos haluat ostaa omaisuutta kiinnitys, aloita lähettämällä sovelluksen kautta alla olevaa painiketta',
    'If you want to buy, rent or sell property, please contact us and we will be glad to provide you with professional services in real estate.' => 'Jos haluat ostaa, vuokrata tai myydä kiinteistöjä, kirjoita meille ja me mielellämme antaa sinulle ammatillisten palvelujen alalla kiinteistöjä.',
    'Include Insurance' => 'Myös vakuutukset',
    'Interest Rate' => 'Korko',
    'It is easy and takes 15 min maximum' => 'Se on helppoa ja kestää enempää kuin 15 minuuttia',
    'It takes few minutes' => 'Se kestää muutaman minuutin',
    'Join' => 'Rekisteröityä',
    'Learn More' => 'Lue lisää',
    'Legal support' => 'Oikeudellinen tuki',
    'Loan Term' => 'Maturiteetti',
    'Local market knowledge' => 'Tietoa paikallisten markkinoiden',
    'Log in' => 'Kirjaudu',
    'Log out' => 'Ulos',
    'MORE DETAILS' => 'Lue LISÄÄ',
    'Manage your own real estate site' => 'Luo oma kiinteistöjen verkkosivuilla',
    'Manager' => 'Manager',
    'Map' => 'Kartta',
    'Max' => 'Max',
    'Menu' => 'Valikko',
    'Message' => 'Viesti',
    'Message from contact form' => 'Viesti yhteydenottolomakkeella',
    'Microdistricts' => 'Lähiöissä',
    'Min' => 'Vähintään',
    'Mobile version' => 'Mobiili versio',
    'Monthly Payment' => 'Kuukausimaksu',
    'More' => 'Lisää',
    'Mortgage' => 'Kiinnitys',
    'Mortgage calculator' => 'Kiinnitys laskin',
    'Mortgage {percent}%' => 'Kiinnitys {percent}%',
    'New Construction' => 'Uusi',
    'New Constructions' => 'Rakennukset',
    'New Password' => 'Uusi salasana',
    'Next Page' => 'Seuraava sivu',
    'No' => 'Ei',
    'No attributes specified' => 'Parametrit on määritetty',
    'No districts were found for this region' => 'Tiloissa tällä alueella ei löytynyt',
    'No microdistricts were found for this region' => 'Lähiöissä tällä alueella ei löytynyt',
    'No results found' => 'Mitään ei löytynyt',
    'Our agency will help you find the best offers for apartments and houses in Spain.' => 'Toimistomme auttaa sinua valitsemaan parhaat tarjoukset asuntoja ja koteja Espanjassa.',
    'Our company employs 15 experienced agents who are licensed to work with real estate in Spain.' => 'Yrityksemme on 15 kokenut aineet, jotka ovat lisensoitu työskennellä kiinteistöjen Espanjassa.',
    'Our company employs lawyers who will help you arrange real estate in accordance with all local laws and will help verify property rights and protect against risks.' => 'Yrityksemme työllistää lakimiehiä, jotka auttavat ongelman omaisuutta sovelletaan kaikkia paikallisia lakeja ja auttaa omistajuuden ja suojautua riskejä.',
    'Our company employs professionals who will help not only to find the best real estate offers, but also to issue a deal.' => 'Yrityksemme työllistää ammattilaisia, jotka eivät vain löydä parhaat tarjoukset kiinteistöjen, mutta myös tehdä paljon.',
    'Our newest property' => 'Uusimmat uutiset',
    'Our team. Real Estate Agency {name}' => 'Meidän joukkue. Real Estate Agency {name}',
    'Password' => 'Salasana',
    'Phone' => 'Puhelin',
    'Photo' => 'Kuva',
    'Prev Page' => 'Edellinen sivu',
    'Price' => 'Hinta',
    'Privacy Policy' => 'Tietosuojakäytäntö',
    'Probably, there are no such words as to express my gratitude to Artem Kovalenko! Incredible professional, but most importantly - extraordinary kindness people. Thanks for the help of cooperation!' => 'Luultavasti, ei ole sanoja, jotka voivat Ilmaista kiitollisuuteni Artem Kovalenko! Uskomaton ammattilainen, mutta mikä tärkeintä - ylimääräinen ystävällisyys ihmisiä. Kiitos avusta ja yhteistyöstä!',
    'Properties Block' => 'Estää esineitä',
    'Property Features:' => 'Esine parametrit:',
    'Property Price' => 'Hinta esine',
    'Property listed by {owner}' => 'Objekti sijoitetaan käyttäjän {owner}',
    'Rating:' => 'Luokitus:',
    'Ready Year {year}' => 'Valmistuminen vuosi {year}',
    'Recent Properties For Sale' => 'Uusia ominaisuuksia myytävänä',
    'Registration' => 'Tarkista',
    'Remove from compare' => 'Poista vertailusta',
    'Rent' => 'Vuokraa',
    'Requested building page is not found' => 'Zaprashivala rakennus ei löytynyt',
    'Reset' => 'Reset',
    'Reset Password' => 'Palauta salasana',
    'Reviews' => 'Arviot',
    'Reviews Block' => 'Yksikön arvostelut',
    'Sale' => 'Myynti',
    'Save Changes' => 'Tallenna muutokset',
    'Search' => 'Haku',
    'Search by map' => 'Haku kartta',
    'Secondary Building' => 'Jälleenmyynti',
    'Select Blocks' => 'Valitse lohkot',
    'Select Categories' => 'Valitse luokat',
    'Select Languages' => 'Valitse kielet',
    'Select Locations' => 'Valitse sijainnit',
    'Select Operations' => 'Valitse toiminnot esineitä',
    'Select {attribute}' => 'Valitse {attribute}',
    'Send Request' => 'Lähetä pyyntö',
    'Sender Email' => 'Sähköpostin lähettäjän',
    'Sender Name' => 'Lähettäjän nimi',
    'Sent from "{name}" site' => 'Lähetetty sivustolla "{name}"',
    'Settings' => 'Asetukset',
    'Share' => 'Jaa',
    'Show' => 'Esityksessä',
    'Show New Constructions' => 'Näytä uudet rakennukset',
    'Show Other Listings' => 'Näytä muita esineitä',
    'Show other languages' => 'Näytä muita kieliä',
    'Sign In' => 'Kirjaudu',
    'Sign Up' => 'Rekisteröityä',
    'Sign in to your account' => 'Kirjaudu sisään tilillesi',
    'Site Blocks' => 'Korttelin verkkosivuilla',
    'Site Categories' => 'Sivuston kategoriat',
    'Site Favicon' => 'Favicon verkkosivuilla',
    'Site Languages' => 'Sivuston kielet',
    'Site Locations' => 'Sivuston sijainti',
    'Site Logo' => 'Sivuston logo',
    'Site Name' => 'Sivuston nimi',
    'Site Operations' => 'Kiinteistökaupoissa',
    'Specify contact email' => 'Sähköpostiosoitteesi',
    'Specify logo text' => 'Määritä tekstin vieressä logo',
    'Start typing and select one of the options' => 'Aloita kirjoittaminen ja valitse jokin käytettävissä olevista vaihtoehdoista',
    'Start typing your location' => 'Aloita kirjoittamalla sijainti',
    'Step {first} from {total}' => 'Vaihe {first} {total}',
    'Subject' => 'Teema',
    'Submit' => 'Lähetä',
    'Table' => 'Taulukko',
    'Team Block' => 'Yksikön joukkue',
    'Team member description' => 'Kuvaus tiimin jäsen',
    'Terms And Conditions' => 'Sopimuksen ehdot',
    'The advantages of working with our Real Estate Agency {name}' => 'Edut työskennellä meidän Virasto {name}',
    'This is mortgage monthly payment with provided parameters' => 'Kuukausimaksu määritettyjä parametreja',
    'To {max}' => '{max}',
    'Unavailable' => 'Käytettävissä',
    'Unknown type' => 'Tuntematon',
    'Upload Site Favicon' => 'Lataa Favicon sivustoosi',
    'Upload Site Logo' => 'Ladata sivuston logo',
    'Upload favicon of your site' => 'Ladata Favicon sivuston',
    'Upload file' => 'Lataa tiedosto',
    'Upload logo of your site' => 'Lataa logo sivuston',
    'Useful Links' => 'Hyödyllisiä linkkejä',
    'Video Tour:' => 'Video kiertue:',
    'View' => 'Katselu',
    'View Info' => 'Näytön asetukset',
    'We are engaged in real estate for 10 years. We are well versed in local legislation and will help you arrange a property.' => 'Olemme mukana kiinteistö 10 vuotta. Olemme hyvin perehtynyt paikallisen lainsäädännön ja auttaa sinua saamaan omaisuutta.',
    'We are offering property in Spain' => 'Tarjoamme omaisuutta Espanjassa',
    'We are working for you' => 'Meidän työskennellä sinulle',
    'We will help you choose and arrange a property. And do it with professionalism and love.' => 'Me autamme sinua valita ja ostaa kiinteistöjä. Ja tehdä se ammattitaidolla ja rakkaudella.',
    'Welcome home!' => 'Tervetuloa kotiin',
    'Write Your Text' => 'Kirjoita viesti',
    'Yes' => 'Kyllä',
    'Your Email' => 'Sähköpostiisi',
    'Your Email Address' => 'Sähköpostiosoitteesi',
    'Your Message' => 'Viestisi',
    'Your Name' => 'Nimesi',
    'Your Phone' => 'Puhelimen',
    'Your Property' => 'Kiinteistön',
    'explore more' => 'katso lisää',
    'month' => 'kuukausi',
    'near {address}' => 'noin {address}',
    'per month' => 'joka kuukausi',
    'with privacy policy' => 'tietosuojakäytäntö',
    'with terms of service' => 'käyttäjän kanssa sopimusta',
    '{area} m²' => '{alue] m2',
    '{bathrooms, plural, one{# bathroom} other{# bathrooms}}' => '{bathrooms, plural, one{# WC} few{# kylpyhuoneet} other{# kylpyhuoneet}}',
    '{bathrooms} bathrooms' => '{bathrooms} kylpyhuoneet',
    '{bedrooms, plural, one{# bedroom} other{# bedrooms}}' => '{bedrooms, plural, one{# makuuhuone} few{# makuuhuone} other{# makuuhuone}}',
    '{condos, plural, one{# condo} other{# condos}}' => '{condos, plural, one{# taulu} few{# taulu} other{# apartments}}',
    '{count, plural, one{# photo} other{# photos}}' => '{count, plural, one{# photo} few{# valokuvat} other{# valokuvat}}',
    '{count, plural, one{# result} other{# results}}' => '{count, plural, one{# tulos} few{# tulos} other{# tuloksia}}',
    '{entity} not found' => '{entity} ei löytynyt',
    '{flats, plural, one{# flat} other{# flats}} for rent' => '{flats, plural, one{# taulu} few{# taulu} other{# apartments}} vuokrattavana',
    '{flats, plural, one{# flat} other{# flats}} for sale' => '{flats, plural, one{# taulu} few{# taulu} other{# apartments}} myytävänä',
    '{floors, plural, one{# floor} other{# floors}}' => '{floors, plural, one{# kerroksessa} few{# kerroksessa} other{# kerrosten}}',
    '{garages, plural, one{# garage} other{# garages}}' => '{garages, plural, one{# autotalli} few{# autotalli} other{# autotallit}}',
    '{garages} garages' => '{garages} autotallit',
    '{rooms, plural, one{# room} other{# rooms}}' => '{rooms, plural, one{# huone} few{# huone} other{# huone}}',
    '{rooms} rooms' => '{rooms} huoneita',
    '{years, plural, one{# year} other{# years}}' => '{years, plural, one{# vuosi} few{# vuosi} other{# vuotta}}',
];