<?php

namespace frontend\modules\instance\models;

use common\behaviors\LinkableBehavior;
use Yii;

/**
 * This is the model class for table "site_instance_block_field".
 *
 * @property int $id
 * @property int $site_id
 * @property int $block_id
 * @property int $type
 * @property string $alias
 *
 * @property InstanceBlock $block
 * @property Instance $site
 *
 * @mixin LinkableBehavior
 */
class InstanceBlockField extends \yii\db\ActiveRecord
{
    public const TYPE_STRING = 0;
    public const TYPE_FILE = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_instance_block_field';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['translations'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_id', 'block_id', 'type'], 'required'],
            [['site_id', 'block_id', 'type'], 'integer'],
            [['alias'], 'string'],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => InstanceBlock::class, 'targetAttribute' => ['block_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instance::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(InstanceBlock::class, ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(InstanceTranslation::class, ['entity_id' => 'id'])
            ->from('site_translation block_field_translations')
            ->andOnCondition(['block_field_translations.entity' => InstanceTranslation::ENTITY_FIELD])
            ->andWhere(['block_field_translations.locale' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Instance::class, ['id' => 'site_id']);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        InstanceTranslation::deleteAll(['entity' => InstanceTranslation::ENTITY_FIELD, 'entity_id' => $this->id]);

        return parent::beforeDelete();
    }
}
