<?php

namespace frontend\modules\instance\models\search;

use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use frontend\modules\instance\dto\InstanceBlockDTO;
use frontend\modules\instance\interfaces\repositories\InstanceBlockRepositoryInterface;
use frontend\modules\instance\models\InstanceBlock;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class InstanceBlockSearch
 * @package common\models\search
 */
class InstanceBlockSearch extends InstanceBlock
{
    /**
     * @var RepositoryInterface
     */
    protected $_instanceBlockRepository;
    /**
     * @var array
     */
    protected $_config;
    /**
     * @var array
     */

    /**
     * InstanceBlockSearch constructor.
     * @param InstanceBlockRepositoryInterface $instanceBlockRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(InstanceBlockRepositoryInterface $instanceBlockRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_instanceBlockRepository = $instanceBlockRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return array
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_instanceBlockRepository);

        $query = $this->initQuery($params);

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['id' => $exclude]]];
        }

        $data = $query->findManyByCriteria($criteria, true);

        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new InstanceBlockDTO($value);
            return $dto->getData(InstanceBlockDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        $query = $this->_instanceBlockRepository
            ->with(['fields.translations'])
            ->joinWith(['block'])
            ->groupBy('site_instance_block.id');

        return $query;
    }
}
