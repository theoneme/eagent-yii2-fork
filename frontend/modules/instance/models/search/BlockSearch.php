<?php

namespace frontend\modules\instance\models\search;

use common\helpers\UtilityHelper;
use common\interfaces\RepositoryInterface;
use frontend\modules\instance\dto\BlockDTO;
use frontend\modules\instance\dto\InstanceBlockDTO;
use frontend\modules\instance\interfaces\repositories\BlockRepositoryInterface;
use frontend\modules\instance\models\Block;

/**
 * Class BlockSearch
 * @package frontend\modules\instance\models\search
 */
class BlockSearch extends Block
{
    /**
     * @var RepositoryInterface
     */
    protected $_blockRepository;
    /**
     * @var array
     */
    protected $_config;

    /**
     * BlockSearch constructor.
     * @param BlockRepositoryInterface $blockRepository
     * @param array $c
     * @param array $config
     */
    public function __construct(BlockRepositoryInterface $blockRepository, array $c = [], array $config = [])
    {
        parent::__construct($config);
        $this->_blockRepository = $blockRepository;
        $this->_config = $c;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return array
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function search($params)
    {
        $limit = $this->_config['limit'] ?? null;
        $indexBy = $this->_config['indexBy'] ?? null;
        $orderBy = $this->_config['orderBy'] ?? null;
        $exclude = $this->_config['exclude'] ?? null;

        $result = [
            'items' => []
        ];

        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $params)) {
            $formName = '';
        }

        $this->load($params, $formName);
        if (!$this->validate()) {
            return $result;
        }

        $criteria = array_filter($this->attributes);
        $criteria = UtilityHelper::fixAmbiguousCondition($criteria, self::tableName(), $this->_blockRepository);

        $query = $this->initQuery($params);

        if ($limit !== null) {
            $query->limit($limit);
        }
        if ($indexBy !== null) {
            $query->indexBy($indexBy);
        }
        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }
        if ($exclude !== null) {
            $criteria = ['and', $criteria, ['not', ['id' => $exclude]]];
        }

        $data = $query->findManyByCriteria($criteria, true);

        $result['items'] = array_filter(array_map(function ($value) {
            $dto = new BlockDTO($value);
            return $dto->getData(InstanceBlockDTO::MODE_SHORT);
        }, $data));

        return $result;
    }

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        $query = $this->_blockRepository;

        return $query;
    }
}
