<?php

namespace frontend\modules\instance\models;

/**
 * This is the model class for table "site_translation".
 *
 * @property int $id
 * @property int $entity
 * @property int $entity_id
 * @property int $key
 * @property string $value
 * @property string $locale
 */
class InstanceTranslation extends \yii\db\ActiveRecord
{
    public const ENTITY_FIELD = 0;

    public const KEY_TITLE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'key', 'locale'], 'required'],
            [['entity', 'entity_id', 'key'], 'integer'],
            [['value'], 'string'],
            [['locale'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

        ];
    }
}
