<?php

namespace frontend\modules\instance\models;

/**
 * This is the model class for table "site_block".
 *
 * @property int $id
 * @property string $alias
 * @property int $type
 */
class Block extends \yii\db\ActiveRecord
{
    public const TYPE_BANNER = 0;
    public const TYPE_EXPLORE = 10;
    public const TYPE_ABOUT = 20;
    public const TYPE_PROPERTIES = 30;
    public const TYPE_TEAM = 40;
    public const TYPE_REVIEWS = 50;
    public const TYPE_CONTACT = 60;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'type'], 'required'],
            [['type'], 'integer'],
            [['alias'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

        ];
    }
}
