<?php

namespace frontend\modules\instance\models;

use common\behaviors\LinkableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "site_instance".
 *
 * @property int $id
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 * @property string $alias
 * @property string $domain
 *
 * @property InstanceSetting[] $instanceSettings
 * @property InstanceBlock[] $instanceBlocks
 *
 * @mixin LinkableBehavior
 */
class Instance extends \yii\db\ActiveRecord
{
    public const STATUS_ACTIVE = 30;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_instance';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['instanceSettings', 'instanceBlocks'],
            ],
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['alias', 'domain'], 'string', 'max' => 50]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstanceSettings()
    {
        return $this->hasMany(InstanceSetting::class, ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstanceBlocks()
    {
        return $this->hasMany(InstanceBlock::class, ['site_id' => 'id']);
    }
}
