<?php

namespace frontend\modules\instance\models;

/**
 * This is the model class for table "site_instance_setting".
 *
 * @property int $id
 * @property int $site_id
 * @property int $key
 * @property int $type
 * @property resource $value
 *
 * @property Instance $site
 */
class InstanceSetting extends \yii\db\ActiveRecord
{
    public const SETTING_LANGUAGES = 0;
    public const SETTING_LOCATIONS = 10;
    public const SETTING_CATEGORIES = 20;
    public const SETTING_OPERATIONS = 30;
    public const SETTING_LOGO = 40;
    public const SETTING_OTHER_LISTINGS = 50;
    public const SETTING_FAVICON = 60;
    public const SETTING_BLOCKS = 70;
    public const SETTING_SEO = 80;
    public const SETTING_NEW_CONSTRUCTION = 90;
    public const SETTING_LOGO_TEXT = 100;
    public const SETTING_CONTACT_EMAIL = 110;

    public const SETTING_TYPE_STRING = 0;
    public const SETTING_TYPE_NUMBER = 10;
    public const SETTING_TYPE_FILE = 20;
    public const SETTING_TYPE_ARRAY = 30;
    public const SETTING_TYPE_BOOLEAN = 40;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_instance_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'type', 'value'], 'required'],
            [['site_id', 'key', 'type'], 'integer'],
            [['value'], 'string'],
//            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instance::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Instance::class, ['id' => 'site_id']);
    }
}
