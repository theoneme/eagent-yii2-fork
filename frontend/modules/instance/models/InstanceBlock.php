<?php

namespace frontend\modules\instance\models;

/**
 * This is the model class for table "site_instance_block".
 *
 * @property int $id
 * @property int $site_id
 * @property int $block_id
 *
 * @property Block $block
 * @property Instance $instance
 * @property InstanceBlockField[] $fields
 */
class InstanceBlock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_instance_block';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_id', 'block_id'], 'required'],
            [['site_id', 'block_id'], 'integer'],
            [['block_id'], 'exist', 'skipOnError' => true, 'targetClass' => Block::class, 'targetAttribute' => ['block_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instance::class, 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(Block::class, ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(Instance::class, ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(InstanceBlockField::class, ['block_id' => 'id']);
    }
}
