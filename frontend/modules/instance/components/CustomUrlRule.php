<?php

namespace frontend\modules\instance\components;

use Yii;
use yii\web\UrlManager;

/**
 * Class CustomUrlRule
 * @package frontend\modules\instance\components
 */
class CustomUrlRule extends \frontend\components\CustomUrlRule
{
    /**
     * Add language to params if not specified and create a URL according to the given route and parameters.
     * @param UrlManager $manager the URL manager
     * @param string $route the route. It should not have slashes at the beginning or the end.
     * @param array $params the parameters
     * @return string|bool the created URL, or `false` if this rule cannot be used for creating this URL.
     */
    public function createUrl($manager, $route, $params)
    {
        if (empty($params['site_alias'])) {
            $params['site_alias'] = Yii::$app->controller->currentInstance['id'] ?? null;
        }
        return parent::createUrl($manager, $route, $params);
    }
}