<?php

namespace frontend\modules\instance\decorators;

use common\interfaces\DecoratorInterface;
use frontend\modules\instance\models\Block;
use Yii;

/**
 * Class BlockTypeDecorator
 * @package frontend\modules\instance\decorators
 */
class BlockTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('instance', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            Block::TYPE_BANNER => Yii::t('instance', 'Banner Block'),
            Block::TYPE_EXPLORE => Yii::t('instance', 'Explore Block'),
            Block::TYPE_ABOUT => Yii::t('instance', 'About Block'),
            Block::TYPE_PROPERTIES => Yii::t('instance', 'Properties Block'),
            Block::TYPE_TEAM => Yii::t('instance', 'Team Block'),
            Block::TYPE_REVIEWS => Yii::t('instance', 'Reviews Block'),
            Block::TYPE_CONTACT => Yii::t('instance', 'Contact Block'),
        ];
    }
}
