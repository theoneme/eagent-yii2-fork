<?php

namespace frontend\modules\instance\repositories\sql;

use common\repositories\sql\AbstractSqlArRepository;
use frontend\modules\instance\interfaces\repositories\InstanceBlockRepositoryInterface;
use frontend\modules\instance\interfaces\repositories\InstanceRepositoryInterface;

/**
 * Class InstanceBlockRepository
 * @package frontend\modules\instance\repositories\sql
 */
class InstanceBlockRepository extends AbstractSqlArRepository implements InstanceBlockRepositoryInterface
{

}