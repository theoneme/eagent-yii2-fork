<?php

namespace frontend\modules\instance\repositories\sql;

use common\repositories\sql\AbstractSqlArRepository;
use frontend\modules\instance\interfaces\repositories\InstanceBlockFieldRepositoryInterface;

/**
 * Class InstanceBlockFieldRepository
 * @package frontend\modules\instance\repositories\sql
 */
class InstanceBlockFieldRepository extends AbstractSqlArRepository implements InstanceBlockFieldRepositoryInterface
{

}