<?php

namespace frontend\modules\instance\repositories\sql;

use common\repositories\sql\AbstractSqlArRepository;
use frontend\modules\instance\interfaces\repositories\BlockRepositoryInterface;

/**
 * Class BlockRepository
 * @package frontend\modules\instance\repositories\sql
 */
class BlockRepository extends AbstractSqlArRepository implements BlockRepositoryInterface
{

}