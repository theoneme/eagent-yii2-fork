<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 13:54
 */

namespace frontend\modules\instance\repositories\sql;

use common\repositories\sql\AbstractSqlArRepository;
use frontend\modules\instance\interfaces\repositories\InstanceRepositoryInterface;

/**
 * Class InstanceRepository
 * @package frontend\modules\instance\repositories\sql
 */
class InstanceRepository extends AbstractSqlArRepository implements InstanceRepositoryInterface
{

}