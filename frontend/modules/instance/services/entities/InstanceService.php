<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 17:18
 */

namespace frontend\modules\instance\services\entities;

use common\interfaces\EntityServiceInterface;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\interfaces\RepositoryInterface;
use frontend\modules\instance\dto\InstanceDTO;
use frontend\modules\instance\interfaces\repositories\InstanceRepositoryInterface;

/**
 * Class InstanceService
 * @package frontend\modules\instance\services\entities
 */
class InstanceService implements EntityServiceInterface
{
    /**
     * @var PropertyRepositoryInterface
     */
    private $_instanceRepository;

    /**
     * InstanceService constructor.
     * @param InstanceRepositoryInterface $instanceRepository
     */
    public function __construct(InstanceRepositoryInterface $instanceRepository)
    {
        $this->_instanceRepository = $instanceRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $instance = $this->_instanceRepository
            ->with(['instanceSettings', 'instanceBlocks'])
            ->findOneByCriteria($criteria, true);

        if ($instance === null) {
            return null;
        }


        $instanceDTO = new InstanceDTO($instance);
        return $instanceDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        return [];
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_instanceRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_instanceRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_instanceRepository = $repository;
    }

    /**
     * @return PropertyRepositoryInterface|InstanceRepositoryInterface
     */
    public function getRepository()
    {
        return $this->_instanceRepository;
    }
}