<?php

namespace frontend\modules\instance\services\entities;

use common\interfaces\EntityServiceInterface;
use common\interfaces\RepositoryInterface;
use frontend\modules\instance\dto\BlockDTO;
use frontend\modules\instance\interfaces\repositories\BlockRepositoryInterface;
use frontend\modules\instance\models\search\BlockSearch;

/**
 * Class BlockService
 * @package frontend\modules\instance\services\entities
 */
class BlockService implements EntityServiceInterface
{
    /**
     * @var BlockRepositoryInterface
     */
    private $_blockRepository;

    /**
     * BlockService constructor.
     * @param BlockRepositoryInterface $blockRepository
     */
    public function __construct(BlockRepositoryInterface $blockRepository)
    {
        $this->_blockRepository = $blockRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $block = $this->_blockRepository->findOneByCriteria($criteria, true);

        if ($block === null) {
            return null;
        }

        $blockDTO = new BlockDTO($block);
        return $blockDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $search = new BlockSearch($this->_blockRepository, $config);
        return $search->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_blockRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_blockRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_blockRepository = $repository;
    }

    /**
     * @return BlockRepositoryInterface
     */
    public function getRepository()
    {
        return $this->_blockRepository;
    }
}