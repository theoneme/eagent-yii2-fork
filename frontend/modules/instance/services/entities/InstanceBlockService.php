<?php

namespace frontend\modules\instance\services\entities;

use common\interfaces\EntityServiceInterface;
use common\interfaces\RepositoryInterface;
use frontend\modules\instance\dto\InstanceBlockDTO;
use frontend\modules\instance\interfaces\repositories\InstanceBlockRepositoryInterface;
use frontend\modules\instance\models\search\InstanceBlockSearch;

/**
 * Class InstanceBlockService
 * @package frontend\modules\instance\services\entities
 */
class InstanceBlockService implements EntityServiceInterface
{
    /**
     * @var InstanceBlockRepositoryInterface
     */
    private $_instanceBlockRepository;

    /**
     * InstanceBlockService constructor.
     * @param InstanceBlockRepositoryInterface $instanceBlockRepository
     */
    public function __construct(InstanceBlockRepositoryInterface $instanceBlockRepository)
    {
        $this->_instanceBlockRepository = $instanceBlockRepository;
    }

    /**
     * @param $criteria
     * @return array|null
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public function getOne($criteria)
    {
        $instance = $this->_instanceBlockRepository->findOneByCriteria($criteria, true);

        if ($instance === null) {
            return null;
        }


        $instanceDTO = new InstanceBlockDTO($instance);
        return $instanceDTO->getData();
    }

    /**
     * @param $params
     * @param array $config
     * @return array
     */
    public function getMany($params, $config = [])
    {
        $search = new InstanceBlockSearch($this->_instanceBlockRepository, $config);
        return $search->search($params);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_instanceBlockRepository->create($data);
    }

    /**
     * @param $criteria
     * @return bool
     */
    public function delete($criteria)
    {
        return $this->_instanceBlockRepository->deleteOneByCriteria($criteria);
    }

    /**
     * @param $data
     * @param $criteria
     */
    public function update($data, $criteria)
    {
        // TODO Implement this shit
    }

    /**
     * @param RepositoryInterface $repository
     */
    public function setRepository(RepositoryInterface $repository)
    {
        $this->_instanceBlockRepository = $repository;
    }

    /**
     * @return InstanceBlockRepositoryInterface
     */
    public function getRepository()
    {
        return $this->_instanceBlockRepository;
    }
}