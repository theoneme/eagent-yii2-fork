<?php

namespace frontend\modules\instance\services;

use frontend\modules\instance\interfaces\repositories\BlockRepositoryInterface;
use frontend\modules\instance\interfaces\repositories\InstanceBlockFieldRepositoryInterface;

/**
 * Class FieldsCleanupService
 * @package frontend\modules\instance\services
 */
class FieldsCleanupService
{
    /**
     * @var InstanceBlockFieldRepositoryInterface
     */
    private $_instanceBlockFieldRepository;

    /**
     * BlockService constructor.
     * @param BlockRepositoryInterface $blockRepository
     */
    public function __construct(InstanceBlockFieldRepositoryInterface $instanceBlockFieldRepository)
    {
        $this->_instanceBlockFieldRepository = $instanceBlockFieldRepository;
    }

    /**
     * @param $instanceId
     * @return array|null
     */
    public function cleanUp($instanceId)
    {
        $blocks = $this->_instanceBlockFieldRepository->findManyByCriteria([
            'id' => $instanceId
        ]);
    }
}