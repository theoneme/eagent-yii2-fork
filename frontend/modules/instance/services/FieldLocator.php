<?php

namespace frontend\modules\instance\services;

use frontend\modules\instance\interfaces\repositories\InstanceBlockFieldRepositoryInterface;
use frontend\modules\instance\models\InstanceBlockField;

/**
 * Class FieldLocator
 * @package frontend\modules\instance\services
 */
class FieldLocator
{
    /**
     * @var InstanceBlockFieldRepositoryInterface
     */
    private $_instanceBlockFieldRepository;

    /**
     * FieldLocator constructor.
     * @param InstanceBlockFieldRepositoryInterface $instanceBlockFieldRepository
     */
    public function __construct(InstanceBlockFieldRepositoryInterface $instanceBlockFieldRepository)
    {
        $this->_instanceBlockFieldRepository = $instanceBlockFieldRepository;
    }

    /**
     * @param $data
     * @return InstanceBlockField|mixed
     */
    public function findOrCreate($data)
    {
        if (!empty($data['block_id']) && !empty($data['alias'])) {
            $blockField = $this->_instanceBlockFieldRepository->findOneByCriteria(['alias' => $data['alias'], 'block_id' => $data['block_id']]);

            if ($blockField instanceof InstanceBlockField) {
                return $blockField;
            }
        }

        return new InstanceBlockField();
    }
}