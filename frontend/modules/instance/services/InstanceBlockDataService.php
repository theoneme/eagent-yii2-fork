<?php

namespace frontend\modules\instance\services;

use common\services\CategoryTreeService;
use common\services\entities\CategoryService;
use common\services\entities\PropertyService;
use frontend\modules\instance\forms\ContactForm;
use frontend\modules\instance\models\Block;
use frontend\modules\instance\models\InstanceSetting;
use frontend\services\LocationService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class InstanceBlockDataService
 * @package frontend\modules\instance\services
 */
class InstanceBlockDataService
{
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var CategoryTreeService
     */
    private $_categoryTreeService;
    /**
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * InstanceBlockDataService constructor.
     * @param PropertyService $propertyService
     * @param CategoryTreeService $categoryTreeService
     * @param CategoryService $categoryService
     * @param LocationService $locationService
     */
    public function __construct(PropertyService $propertyService,
                                CategoryTreeService $categoryTreeService,
                                CategoryService $categoryService,
                                LocationService $locationService)
    {
        $this->_propertyService = $propertyService;
        $this->_categoryTreeService = $categoryTreeService;
        $this->_categoryService = $categoryService;
        $this->_locationService = $locationService;
    }

    /**
     * @param $instance
     * @param $blockType
     * @param array $condition
     * @return array
     */
    public function getData($instance, $blockType, $condition = [])
    {
        $data = [];

        switch ($blockType) {
            case Block::TYPE_BANNER:
                $data['locations'] = [];
                $data['categories'] = ArrayHelper::map($this->_categoryTreeService->getTree(), 'slug', 'title');
                $settingLocations = $instance['settings'][InstanceSetting::SETTING_LOCATIONS]['value'] ?? [];
                if (!empty($settingLocations)) {
                    $data['locations'] = ArrayHelper::map($settingLocations, function ($var) {
                        return "{$var['entity']}_{$var['entity_id']}";
                    }, 'address');
                }
                break;
            case Block::TYPE_EXPLORE:
                $data['categories'] = [];
                $data['address'] = '';
                $settingCategories = $instance['settings'][InstanceSetting::SETTING_CATEGORIES]['value'] ?? [];
                if (!empty($settingCategories)) {
                    $categories = $this->_categoryService->getMany(['id' => $settingCategories], ['limit' => 5]);
                    $data['categories'] = $categories['items'];
                }
                $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
                if (!empty($locationData)) {
                    $data['address'] = $locationData['address'];
                }
                break;
            case Block::TYPE_CONTACT:
//                $settingLocations = $instance['settings'][InstanceSetting::SETTING_LOCATIONS];
                $data['contactForm'] = new ContactForm();
                break;
            case Block::TYPE_PROPERTIES:
                $condition = [];
                if (($instance['settings'][InstanceSetting::SETTING_OTHER_LISTINGS]['value'] ?? false)) {
                    $condition[] = ['user_id' => $instance['user_id']];
                }
                $operations = $instance['settings'][InstanceSetting::SETTING_OPERATIONS]['value'] ?? [];
                if (count($operations) === 1) {
                    $condition[] = ['operation' => $operations[0]];
                }
//                if (!empty($instance['settings'][InstanceSetting::SETTING_CATEGORIES]['value'])) {
//                    $condition[] = ['category_id' => $instance['settings'][InstanceSetting::SETTING_CATEGORIES]['value']];
//                }
                $location = $instance['settings'][InstanceSetting::SETTING_LOCATIONS]['value'] ?? [];
                if (!empty($location)) {
                    $location = $location[0];
                    switch ($location['entity']) {
                        case 'city':
                            $condition[] = ['city_id' => $location['entity_id']];
                            break;
                        case 'region':
                            $condition[] = ['region_id' => $location['entity_id']];
                            break;
                        case 'country':
                            $condition[] = ['country_id' => $location['entity_id']];
                            break;
                    }
                }
                if (!empty($condition)) {
                    $condition = array_merge(['and'], $condition);
                }
                $properties = $this->_propertyService->getMany($condition, ['limit' => 4]);
                $data['properties'] = $properties['items'];
                break;
            default:
                break;
        }

        return $data;
    }
}