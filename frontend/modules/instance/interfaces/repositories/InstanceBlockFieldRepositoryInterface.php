<?php

namespace frontend\modules\instance\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface InstanceBlockFieldRepositoryInterface
 * @package frontend\modules\instance\interfaces\repositories
 */
interface InstanceBlockFieldRepositoryInterface extends RepositoryInterface
{

}