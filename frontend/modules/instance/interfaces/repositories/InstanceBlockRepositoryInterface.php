<?php

namespace frontend\modules\instance\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface InstanceBlockRepositoryInterface
 * @package frontend\modules\instance\interfaces\repositories
 */
interface InstanceBlockRepositoryInterface extends RepositoryInterface
{

}