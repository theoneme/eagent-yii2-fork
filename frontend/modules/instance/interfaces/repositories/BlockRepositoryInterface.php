<?php

namespace frontend\modules\instance\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface BlockRepositoryInterface
 * @package frontend\modules\instance\interfaces\repositories
 */
interface BlockRepositoryInterface extends RepositoryInterface
{

}