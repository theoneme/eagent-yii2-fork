<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 14:37
 */

namespace frontend\modules\instance\interfaces\repositories;

use common\interfaces\RepositoryInterface;

/**
 * Interface InstanceRepositoryInterface
 * @package frontend\modules\instance\interfaces\repositories
 */
interface InstanceRepositoryInterface extends RepositoryInterface
{

}