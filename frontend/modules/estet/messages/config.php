<?php

return [
    'color' => null,
    'interactive' => true,
    'sourcePath' => 'frontend/modules/estet',
    'messagePath' => 'frontend/modules/estet/messages',
    'languages' => array_diff(array_keys(Yii::$app->params['supportedLocales']), ['en-GB']),
    'translator' => 'Yii::t',
    'sort' => true,
    'overwrite' => true,
    'removeUnused' => true,
    'markUnused' => true,
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/BaseYii.php',
    ],
    'only' => [
        '*.php',
    ],
    'format' => 'php',
    'db' => 'db',
    'sourceMessageTable' => '{{%source_message}}',
    'messageTable' => '{{%message}}',
    'catalog' => 'messages',
    'ignoreCategories' => ['notification', 'model', 'yii', 'labels', 'app', 'crm', 'site', 'wizard', 'catalog', 'main', 'instance'],
];
