<?php 
 return [
    '<strong> Estet </strong> company in numbers' => 'الشركة <strong>Estet</strong> في أرقام',
    'Address' => '',
    'Agreed mortgage loans totaling more than {price} {currency}' => 'منسقة قروض الرهن العقاري في كمية أكثر من {price} {currency}',
    'All complexes under construction in one place' => 'كل من مجمع قيد الانشاء في مكان واحد',
    'Artyom Kokoteev' => 'ارتيم Karateev',
    'CEO of the company "Estet"' => 'مدير شركة "Estet"',
    'Choose a convenient time to call' => 'اختيار وقت مناسب للاتصال',
    'Contact us' => 'الاتصال بنا',
    'Contacts' => 'الاتصالات',
    'Developer' => 'المطور',
    'Developer pays us a fee for making a deal.' => 'باني يدفع لنا رسوم المعاملات.',
    'Developers provide us with maximum discounts due to our sales.' => 'بسبب حجم المبيعات المطورين يعطينا خصم الأقصى.',
    'Discount {percent}%' => 'خصم {percent}%',
    'Discount: <strong>{sum}</strong> {currency}' => 'الخصم: <strong>{sum}</strong> {currency}',
    'Due to the volume of sold condos and knowledge of the market situation, we get the best possible conditions' => 'بسبب حجم الشقق المباعة و معرفة حالة السوق ، ونحن الحصول على صفقة أفضل الظروف الممكنة',
    'E-mail:' => 'البريد الإلكتروني:',
    'Enter the time when you what to receive a call' => 'أدخل الوقت عندما كنت قد ترغب في الاتصال',
    'Estet' => 'Estet',
    'Examples of actual special offers' => 'أمثلة على العروض الخاصة الحالية',
    'For detailed information on the availability and cost of the specified goods and (or) services, please contact the site manager using a special form of communication or by phone {phone} or by email {email}' => 'لمزيد من المعلومات حول توافر هذه السلع (أو الخدمات), يرجى الاتصال مدير الموقع باستخدام شكل خاص من أشكال الاتصال أو عن طريق الهاتف {phone} أو البريد الإلكتروني {email}',
    'Free taxi to the sales office' => 'مجانا سيارات الأجرة إلى مكتب المبيعات',
    'Get Special Offers' => 'للحصول على العروض الخاصة',
    'Get a free consultation' => 'الحصول على استشارة مجانية',
    'Get a free presentation' => 'للحصول على العرض مجانا',
    'Get a presentation on all under construction LCD in {city}' => 'للحصول على عرض لكل بنيت حديثا مجمع سكني من {city}',
    'Get a special offer' => 'العرض',
    'Get all special offers' => 'أن جميع الصفقات',
    'Get consultation on buying property for free' => 'الحصول على استشارة مجانية على شراء العقارات',
    'Get special offers from developers for free' => 'الحصول على عروض خاصة من المطورين مجانا',
    'Get special offers in "{name}"' => 'أن صفقات في "{name}"',
    'Hello! We can call you back in 1 minute and answer all your questions!' => 'مرحبا! يمكننا أن ندعو لكم مرة أخرى في 1 دقيقة و الإجابة على جميع الأسئلة الخاصة بك!',
    'How do we get the best conditions for our clients?' => 'كيف يمكننا الحصول على أفضل شروط العملاء ؟ ',
    'How to buy and save?' => 'كيفية شراء و حفظ ؟ ',
    'I am waiting for a call!' => 'في انتظار مكالمة!',
    'Installment for half a year' => 'أقساط بدون فوائد لمدة ستة أشهر',
    'Installment for {year, plural, one{# year} other{# years}}' => 'أقساط {year, plural, one{# السنة} few{# السنة} other{# سنة}}',
    'Installment: {month, plural, one{# month} other{# months}}' => 'أقساط {month, plural, one{# شهر} few{# أشهر} other{# أشهر}}',
    'Interior' => 'النهاية',
    'Located by address {address}' => 'يقع في العنوان {address}',
    'More than {price} {currency} of commission returned to customers' => 'عاد إلى المشترين أكثر من {price} {currency} RUR اللجنة',
    'Mortgage {percent}%' => 'الرهن العقاري {percent}%',
    'New buildings' => 'المباني',
    'Number of rooms available: {rooms}' => 'متوفر عدد الغرف: {rooms}',
    'Our advantages' => 'لدينا مزايا',
    'Please fill these contact fields:' => 'يرجى ملء تفاصيل الاتصال التالية:',
    'Request Consultation' => 'طلب استشارة',
    'Residential complex' => 'مجمع سكني',
    'See more' => 'قراءة المزيد',
    'Send Message' => 'إرسال رسالة',
    'Send Request' => 'إرسال الطلب',
    'Special offers' => 'عروض خاصة',
    'Specials:' => 'العمل:',
    'The developers give the best price when you sell more than {count} apartments per month, and you become an important partner...' => 'أفضل سعر المطورين تعطي عند بيع أكثر من {count} وحدة في الشهر ، يمكنك أن تصبح شريكا هاما...',
    'We approve {percent}% of requests for mortgage loans and select the most favorable rates' => 'ندعي {percent} ٪ من الرهن العقاري التطبيقات وحدد الأكثر فائدة معدلات',
    'We can help you save from {from} to {to} {currency} <br> when you buy a condo in the New Building in {city}' => 'ونحن يمكن أن تساعدك على إنقاذ من {from} {to} {currency}<br> عند شراء شقة في المدينة {city}',
    'We can send you detailed presentation about this building for free' => 'يمكننا أن نرسل لك عرض مفصل عن هذا المجمع مجانا',
    'We draw your attention to the fact that this website is for informational purposes only and under no circumstances is not a public offer determined by the provisions of Article 437 (2) of the Civil Code of the Russian Federation.' => 'يرجى ملاحظة أن هذا الموقع هو لأغراض إعلامية فقط و تحت أي ظرف من الظروف ليس من العرض العام تحددها المواقف من المادة 437 (2) من القانون المدني للاتحاد الروسي.',
    'We get individual rates and help save on interest payments on loans' => 'نتلقى الفردية معدلات تساعد على توفير دفع الفائدة على القروض.',
    'We get the maximum discount due to sales' => 'الحصول على أقصى قدر من الخصم بسبب حجم المبيعات',
    'We have set more than 100 agreements on buying properties in new constructions' => 'وقعنا أكثر من 100 عقود شراء العقارات في المباني الجديدة',
    'We provide actual offers from developers' => 'تقديم المقترحات ذات الصلة من المطورين',
    'We received discounts for buyers in the amount of {price} {currency}' => 'التي تلقتها الولايات المتحدة بالنسبة للمشترين من الخصومات في كمية من {price} {currency}',
    'We return to buyers 50% of the remuneration received from the developer' => 'نحن استرداد العملاء 50% من فيها من البناء',
    'We save on loan payments more than {price} per year' => 'حفظ على دفعات القرض أكثر من {price} في السنة',
    'We select condos and register transaction for free' => 'مجانا حمل من اختيار الشقق من الصفقة',
    'We select the best of the available offers of developers. <br> We get the maximum discount and installments' => 'العثور على أفضل الفنادق و المطورين. <br> الحصول على أقصى قدر من خصم القسط',
    'We select the offers of banks on the mortgage for free and make a deal' => 'سوف تلتقط العروض المجانية من البنوك على التمويل العقاري و عقد صفقة',
    'We track and provide actual discounts, installments and other important information from developers.' => 'الشاشة وتوفير ذات الصلة الخصومات والأقساط وغيرها من المعلومات ذات الصلة من المطورين.',
    'We work with all developers and <br/> we will provide relevant information on any building under construction.' => 'نحن نتعامل مع جميع المطورين ، <br /> تقديم المعلومات ذات الصلة عن أي مبنى',
    'We work with all developers of {city}' => 'نحن نتعامل مع جميع بناة {city}',
    'With prices starting from {from}' => 'مع أسعار تبدأ من {from}',
    'You automatically agree to the <a href="https://eagent.me/terms-of-service">Terms of Service</a> and <a href="https://eagent.me/privacy-policy">Privacy Policy</a> when you submit your request' => 'من خلال تقديم التطبيق الخاص بك فإنك توافق على <a href="https://eagent.me/terms-of-service">شروط الاستخدام</a> و <a href="https://eagent.me/privacy-policy">سياسة الخصوصية</a>',
    '{rooms, plural, one{#-room} other{#-rooms}} {area}м²' => '{rooms, plural, one{#نوم} other{#نوم}} {area}m2',
];