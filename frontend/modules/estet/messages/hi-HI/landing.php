<?php 
 return [
    '<strong> Estet </strong> company in numbers' => 'कंपनी <strong>Estet</strong> संख्या में',
    'Address' => '',
    'Agreed mortgage loans totaling more than {price} {currency}' => 'समन्वित में बंधक ऋण की राशि की तुलना में अधिक {price} {currency}',
    'All complexes under construction in one place' => 'सभी परिसर के निर्माण के तहत एक जगह में',
    'Artyom Kokoteev' => 'आर्टेम Karateev',
    'CEO of the company "Estet"' => 'कंपनी के निदेशक "Estet"',
    'Choose a convenient time to call' => 'एक सुविधाजनक समय चुनते करने के लिए कॉल',
    'Contact us' => 'हमसे संपर्क करें',
    'Contacts' => 'संपर्क',
    'Developer' => 'डेवलपर',
    'Developer pays us a fee for making a deal.' => 'बिल्डर हमें भुगतान करता है के लिए एक शुल्क के लेन-देन.',
    'Developers provide us with maximum discounts due to our sales.' => 'कारण के लिए हमारे मात्रा में बिक्री, डेवलपर्स हमें देता है, अधिकतम छूट है । ',
    'Discount {percent}%' => 'डिस्काउंट {percent}%',
    'Discount: <strong>{sum}</strong> {currency}' => 'छूट: <strong>{sum}</strong> {currency}',
    'Due to the volume of sold condos and knowledge of the market situation, we get the best possible conditions' => 'कारण की मात्रा को बेचा अपार्टमेंट और ज्ञान बाजार की स्थिति, हम सबसे अच्छा संभव स्थिति',
    'E-mail:' => 'ई-मेल:',
    'Enter the time when you what to receive a call' => 'समय दर्ज करें जब आप कॉल करने के लिए चाहते हो सकता है',
    'Estet' => 'Estet',
    'Examples of actual special offers' => 'उदाहरण के वर्तमान विशेष प्रस्तावों',
    'For detailed information on the availability and cost of the specified goods and (or) services, please contact the site manager using a special form of communication or by phone {phone} or by email {email}' => 'अधिक जानकारी के लिए उपलब्धता के बारे में और लागत के इस तरह के माल और (या) सेवाओं के लिए, कृपया संपर्क करें साइट प्रबंधक का उपयोग कर एक विशेष रूप से संचार या फोन के द्वारा {phone} या ईमेल {email}',
    'Free taxi to the sales office' => 'मुक्त करने के लिए टैक्सी बिक्री कार्यालय',
    'Get Special Offers' => 'करने के लिए विशेष ऑफर मिल',
    'Get a free consultation' => 'नि: शुल्क परामर्श प्राप्त',
    'Get a free presentation' => 'प्राप्त करने के लिए प्रस्तुति के लिए नि: शुल्क',
    'Get a presentation on all under construction LCD in {city}' => 'प्राप्त करने के लिए एक प्रस्तुति के लिए सभी नव निर्मित आवासीय परिसर के {city}',
    'Get a special offer' => 'की पेशकश करने के लिए',
    'Get all special offers' => 'करने के लिए सभी सौदों',
    'Get consultation on buying property for free' => 'एक नि: शुल्क परामर्श पर अचल संपत्ति खरीदने के',
    'Get special offers from developers for free' => 'विशेष ऑफर पाने के लिए डेवलपर्स से नि: शुल्क',
    'Get special offers in "{name}"' => 'करने के लिए सौदों में "{name}"',
    'Hello! We can call you back in 1 minute and answer all your questions!' => 'नमस्कार! हम कॉल कर सकते हैं तुम वापस में 1 मिनट के लिए और अपने सभी सवालों का जवाब!',
    'How do we get the best conditions for our clients?' => 'हम कैसे सबसे अच्छा पाने के लिए स्थिति के लिए अपने ग्राहकों को?',
    'How to buy and save?' => 'कैसे खरीदने के लिए और बचाने के लिए?',
    'I am waiting for a call!' => 'एक फोन का इंतज़ार कर!',
    'Installment for half a year' => 'ब्याज मुक्त किश्तों छह महीने के लिए',
    'Installment for {year, plural, one{# year} other{# years}}' => 'किश्तों के लिए {year, plural, one{# वर्ष} few{# वर्ष} other{# वर्ष}}',
    'Installment: {month, plural, one{# month} other{# months}}' => 'किश्तों के लिए {month, plural, one{# माह} few{# months} other{# months}}',
    'Interior' => 'खत्म',
    'Located by address {address}' => 'पते पर स्थित {address}',
    'More than {price} {currency} of commission returned to customers' => 'लौट आए खरीदारों के लिए अधिक से अधिक {price} {currency} RUR आयोग',
    'Mortgage {percent}%' => 'बंधक {percent}%',
    'New buildings' => 'इमारतों',
    'Number of rooms available: {rooms}' => 'उपलब्ध कमरों की संख्या: {rooms}',
    'Our advantages' => 'हमारे फायदे',
    'Please fill these contact fields:' => 'में भरने के लिए कृपया निम्न संपर्क विवरण:',
    'Request Consultation' => 'अनुरोध करने के लिए एक परामर्श',
    'Residential complex' => 'आवासीय परिसर',
    'See more' => 'और अधिक पढ़ें',
    'Send Message' => 'एक संदेश भेजने के लिए',
    'Send Request' => 'भेजने के आवेदन',
    'Special offers' => 'विशेष ऑफर',
    'Specials:' => 'कार्रवाई:',
    'The developers give the best price when you sell more than {count} apartments per month, and you become an important partner...' => 'सबसे अच्छी कीमत डेवलपर्स दे जब आप को बेचने की तुलना में अधिक {count} इकाइयों प्रति माह, और आप एक महत्वपूर्ण भागीदार बन जाते हैं...',
    'We approve {percent}% of requests for mortgage loans and select the most favorable rates' => 'हम दावा {percent}% के बंधक अनुप्रयोगों का चयन करें और सबसे लाभप्रद दरों',
    'We can help you save from {from} to {to} {currency} <br> when you buy a condo in the New Building in {city}' => 'हम मदद कर सकते हैं आप से बचाने के लिए {from} है {to} {currency}<br> जब एक अपार्टमेंट खरीदने के लिए शहर में {city}',
    'We can send you detailed presentation about this building for free' => 'हम भेज सकते हैं आप विस्तृत प्रस्तुति के बारे में इस परिसर के लिए नि: शुल्क',
    'We draw your attention to the fact that this website is for informational purposes only and under no circumstances is not a public offer determined by the provisions of Article 437 (2) of the Civil Code of the Russian Federation.' => 'कृपया ध्यान दें कि इस इंटरनेट साइट सूचना के प्रयोजनों के लिए और किसी भी परिस्थिति में नहीं है एक सार्वजनिक पेशकश के द्वारा परिभाषित पदों के लेख 437 (2) के नागरिक रूसी संघ के कोड.',
    'We get individual rates and help save on interest payments on loans' => 'हम अलग-अलग दरों और बचाने के लिए मदद पर ब्याज का भुगतान ऋण पर.',
    'We get the maximum discount due to sales' => 'अधिकतम छूट के कारण बिक्री की मात्रा',
    'We have set more than 100 agreements on buying properties in new constructions' => 'हम हस्ताक्षर किए गए 100 से अधिक के ठेके की खरीद के लिए नई इमारतों में अचल संपत्ति',
    'We provide actual offers from developers' => 'उपलब्ध कराने के प्रासंगिक प्रस्तावों के डेवलपर्स से',
    'We received discounts for buyers in the amount of {price} {currency}' => 'हमारे द्वारा प्राप्त के खरीदारों के लिए छूट की राशि में {price} {currency}',
    'We return to buyers 50% of the remuneration received from the developer' => 'हम वापसी ग्राहकों के 50% के विचार से प्राप्त बिल्डर',
    'We save on loan payments more than {price} per year' => 'बचाने के लिए ऋण पर भुगतान से अधिक {price} प्रति वर्ष',
    'We select condos and register transaction for free' => 'नि: शुल्क बाहर ले जाने के चयन और अपार्टमेंट के लेन-देन',
    'We select the best of the available offers of developers. <br> We get the maximum discount and installments' => 'सबसे अच्छा होटल का पता है, और डेवलपर्स. <br> अधिकतम छूट और किस्त',
    'We select the offers of banks on the mortgage for free and make a deal' => 'नि: शुल्क ले जाएगा प्रदान करता है बैंकों के बंधक पर और एक सौदा करने के लिए',
    'We track and provide actual discounts, installments and other important information from developers.' => 'की निगरानी प्रदान करते हैं और प्रासंगिक छूट, किश्तों और अन्य प्रासंगिक जानकारी के डेवलपर्स से है.',
    'We work with all developers and <br/> we will provide relevant information on any building under construction.' => 'हम काम के साथ सभी डेवलपर्स और <br /> प्रासंगिक जानकारी प्रदान करते हैं पर किसी भी इमारत',
    'We work with all developers of {city}' => 'हम काम के साथ सभी बिल्डरों {city}',
    'With prices starting from {from}' => 'कीमतों के साथ शुरू करने से {from}',
    'You automatically agree to the <a href="https://eagent.me/terms-of-service">Terms of Service</a> and <a href="https://eagent.me/privacy-policy">Privacy Policy</a> when you submit your request' => 'आपके आवेदन को जमा करके आप इस बात से सहमत करने के लिए <a href="https://eagent.me/terms-of-service">उपयोग की शर्तों</a> और <a href="https://eagent.me/privacy-policy">गोपनीयता नीति</a>',
    '{rooms, plural, one{#-room} other{#-rooms}} {area}м²' => '{rooms, plural, one{#बेडरूम} other{#बेडरूम}} {area}m2',
];