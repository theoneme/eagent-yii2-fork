<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.02.2019
 * Time: 15:25
 */

namespace frontend\modules\estet\assets;

use yii\web\AssetBundle;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;

/**
 * Class LandingAsset
 * @package frontend\modules\estet\assets
 */
class LandingAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/estet/web';
    public $css = [
        'css/landing.css',
    ];
    public $js = [
        'js/vendor/jquery.countdown.min.js',
    ];
    public $depends = [
        BootstrapAsset::class,
        BootstrapPluginAsset::class
    ];
}