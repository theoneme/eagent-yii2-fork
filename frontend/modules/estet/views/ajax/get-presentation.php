<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.03.2019
 * Time: 13:31
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\landing\ConsultationForm $presentationForm
 */

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <div class="modal-title text-center"><?= Yii::t('landing', 'Get a presentation on all under construction LCD in {city}', ['city' => 'Екатеринбурга']) ?></div>
</div>
<div class="modal-body">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/landing/ajax/get-presentation']),
        'id' => 'get-presentation-modal-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    <?= $form->field($presentationForm, 'name')->textInput() ?>
    <?= $form->field($presentationForm, 'phone')->textInput() ?>
    <?= $form->field($presentationForm, 'email')->textInput() ?>
    <p class="small-font">
        <?= Yii::t('landing', 'You automatically agree to the <a href="https://eagent.me/terms-of-service">Terms of Service</a> and <a href="https://eagent.me/privacy-policy">Privacy Policy</a> when you submit your request') ?>
    </p>
    <div class="text-center">
        <input id="consult-submit" type="submit" class="butn butn-form butn-blue-white"
               value="<?= Yii::t('landing', 'Send Request') ?>">
    </div>
    <?php ActiveForm::end() ?>
</div>

<?php $script = <<<JS
    $('#get-presentation-modal-form').on('beforeSubmit', function() { 
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.closest('.modal').modal('hide');
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);