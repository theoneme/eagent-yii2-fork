<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.03.2019
 * Time: 13:29
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\landing\SpecialOfferForm $offerForm
 */

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <div class="modal-title text-center"><?= Yii::t('landing', 'Get special offers from developers for free') ?></div>
</div>
<div class="modal-body">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/landing/ajax/special-offer']),
        'id' => 'special-offer-modal-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    <?= $form->field($offerForm, 'name')->textInput() ?>
    <?= $form->field($offerForm, 'phone')->textInput() ?>
    <?= $form->field($offerForm, 'email')->textInput() ?>
    <p class="small-font">
        <?= Yii::t('landing', 'You automatically agree to the <a href="https://eagent.me/terms-of-service">Terms of Service</a> and <a href="https://eagent.me/privacy-policy">Privacy Policy</a> when you submit your request') ?>
    </p>

    <div class="text-center">
        <input id="offers-submit" type="submit" class="butn butn-form butn-blue-white"
               value="<?= Yii::t('landing', 'Send Request') ?>">
    </div>
    <?php ActiveForm::end() ?>
</div>

<?php $script = <<<JS
    $('#special-offer-modal-form').on('beforeSubmit', function() { 
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.closest('.modal').modal('hide');
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);
