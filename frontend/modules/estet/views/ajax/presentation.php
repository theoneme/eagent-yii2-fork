<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.03.2019
 * Time: 13:32
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\landing\ConsultationForm $presentationForm
 * @var array $building
 * @var array $route
 * @var string $rooms
 * @var integer $minPrice
 */

?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-title text-center"><?= Yii::t('landing', 'Get special offers in "{name}"', ['name' => $building['name']]) ?></div>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="building-img hidden-xs">
                    <?= Html::img($building['image']) ?>
                </div>

                <div class="show-text">
                    <p>
                        <strong><?= $building['name'] ?></strong>
                    </p>
                    <ul class="no-list">
                        <li><?= Yii::t('landing', 'Located by address {address}', ['address' => $building['address']]) ?></li>
                        <?php if ($rooms) { ?>
                            <li><?= Yii::t('landing', 'Number of rooms available: {rooms}', ['rooms' => $rooms]) ?></li>
                        <?php } ?>
                        <?php if ($minPrice) { ?>
                            <li>
                                <?= Yii::t('landing', 'With prices starting from {from}', [
                                    'from' => \common\components\CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $minPrice)
                                ]) ?>
                            </li>
                        <?php } ?>
                        <li><?= Yii::t('landing', 'We can send you detailed presentation about this building for free') ?></li>
                        <li><?= Yii::t('landing', 'Please fill these contact fields:') ?></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to($route),
                    'id' => 'presentation-modal-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>
                <?= $form->field($presentationForm, 'name')->textInput() ?>
                <?= $form->field($presentationForm, 'phone')->textInput() ?>
                <?= $form->field($presentationForm, 'email')->textInput() ?>

                <p class="small-font">
                    <?= Yii::t('landing', 'You automatically agree to the <a href="https://eagent.me/terms-of-service">Terms of Service</a> and <a href="https://eagent.me/privacy-policy">Privacy Policy</a> when you submit your request') ?>
                </p>
                <div class="text-center">
                    <input id="consult-submit" type="submit" class="butn butn-form butn-blue-white"
                           value="<?= Yii::t('landing', 'Get Special Offers') ?>">
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    $('#presentation-modal-form').on('beforeSubmit', function() { 
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.closest('.modal').modal('hide');
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);