<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.02.2019
 * Time: 15:19
 */

use frontend\modules\estet\assets\LandingAsset;
use yii\helpers\Url;

LandingAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="<?= Url::to(['/images/favicon.png'], true) ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= Url::to(['/images/favicon.png'], true) ?>" type="image/png"/>
    <? //= Html::csrfMetaTags() ?>
    <title><? //= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="<?= in_array(Yii::$app->language, ['he-IL', 'ar-AR']) ? 'rtl' : '' ?>">
<?php $this->beginBody() ?>

<?= $content ?>

<?= \yii\bootstrap\Modal::widget([
    'id' => 'dynamic-modal',
    'options' => ['class' => 'fade new-modal']
]) ?>

<?php $script = <<<JS
    
JS;

$this->registerJs($script); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
