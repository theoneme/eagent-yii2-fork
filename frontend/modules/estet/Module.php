<?php

namespace frontend\modules\estet;

use Yii;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

/**
 * Class Module
 * @package frontend\modules\instance
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\estet\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        Yii::$app->i18n->translations['landing*'] = [
            'class' => PhpMessageSource::class,
            'basePath' => '@frontend/modules/estet/messages',

            'fileMap' => [
                'landing' => 'landing.php',
            ],
        ];
    }
}
