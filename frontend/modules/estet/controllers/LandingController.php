<?php

namespace frontend\modules\estet\controllers;

use common\controllers\FrontEndController;
use common\interfaces\repositories\BuildingRepositoryInterface;
use common\models\Building;
use common\services\entities\AttributeValueService;
use common\services\entities\BuildingService;
use common\services\entities\PropertyService;
use frontend\forms\contact\ContactForm;
use frontend\mappers\LandingPropertyMapper;
use frontend\services\LocationService;
use Yii;
use yii\base\Module;
use yii\db\Expression;

/**
 * Class LandingController
 * @package frontend\modules\estet\controllers
 */
class LandingController extends FrontEndController
{
    /**
     * @var BuildingRepositoryInterface
     */
    private $_buildingRepository;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * LandingController constructor.
     * @param string $id
     * @param Module $module
     * @param AttributeValueService $attributeValueService
     * @param PropertyService $propertyService
     * @param BuildingRepositoryInterface $buildingRepository
     * @param BuildingService $buildingService
     * @param LocationService $locationService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                AttributeValueService $attributeValueService,
                                PropertyService $propertyService,
                                BuildingRepositoryInterface $buildingRepository,
                                BuildingService $buildingService,
                                LocationService $locationService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingService = $buildingService;
        $this->_propertyService = $propertyService;
        $this->_buildingRepository = $buildingRepository;
        $this->_attributeValueService = $attributeValueService;
        $this->_locationService = $locationService;
    }

    /**
     * @return string
     */
    public function actionNewConstructions()
    {
        $this->layout = 'main';
        $contactForm = new ContactForm();

        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        // ID билдингов у которых есть скидки
        $propertyBuildings = $this->_buildingRepository->relationExists('building_attribute', 'building_id', [
            'entity_alias' => ['new_construction_discount']
        ])->select(['id'], true)->findManyByCriteria([
            'region_id' => $locationData['region_id'],
            'type' => Building::TYPE_NEW_CONSTRUCTION,
            'ads_allowed' => true
        ], true);

        $data = [];
        if(!empty($propertyBuildings)) {
            $buildingIDs = array_column($propertyBuildings, 'id');
            // рандомные проперти из доступных билдингов + сами билдинги
            $propertyBuildings = $this->_buildingService->getMany(['id' => $buildingIDs], ['indexBy' => 'id']);
            $properties = $this->_propertyService->getMany([
                'city_id' => $locationData['city_id'],
                'building_id' => $buildingIDs,
                'ads_allowed' => true
            ], ['limit' => 8, 'orderBy' => new Expression('rand()')]);

            // склеиваем все это говно в кучу, чтобы можно было вывести на странице
            $data = LandingPropertyMapper::getMappedData([
                'properties' => $properties['items'],
                'buildings' => $propertyBuildings['items']
            ]);
        }

        $buildings = $this->_buildingService->getMany([
            'type' => Building::TYPE_NEW_CONSTRUCTION,
            'region_id' => $locationData['region_id'],
            'ads_allowed' => true,
        ], [
            'orderBy' => ['id' => SORT_DESC],
            'limit' => 9
        ]);

        // кусок говна чтобы притянуть транслейшены застройщиков
        $developers = array_filter(array_map(function ($building) {
            return isset($building['attributes']['new_construction_builder']) ? str_replace(',', '-', $building['attributes']['new_construction_builder']) : null;
        }, $buildings['items']));
        $developers = $this->_attributeValueService->getMany([
            'alias' => $developers,
            'attribute_id' => 41
        ], ['indexBy' => 'alias']);
        foreach ($buildings['items'] as $key => &$building) {
            if (isset($building['attributes']['new_construction_builder'])) {
                $developer = str_replace(',', '-', $building['attributes']['new_construction_builder']);
                $building['attributes']['new_construction_builder'] = $developers['items'][$developer]['translations']['title'] ?? $building['attributes']['new_construction_builder'];
            }
        }

        return $this->render('new-constructions', [
            'contactForm' => $contactForm,
            'properties' => $data,
            'buildings' => $buildings['items']
        ]);
    }
}
