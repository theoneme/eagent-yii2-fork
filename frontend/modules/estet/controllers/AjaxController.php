<?php

namespace frontend\modules\estet\controllers;

use common\controllers\FrontEndController;
use common\interfaces\repositories\PropertyAttributeRepositoryInterface;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\services\entities\BuildingService;
use common\services\NotificationService;
use frontend\forms\landing\ConsultationForm;
use frontend\forms\landing\ContactForm;
use frontend\forms\landing\SpecialOfferForm;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AjaxController
 * @package frontend\modules\estet\controllers
 */
class AjaxController extends FrontEndController
{
    /**
     * @var NotificationService
     */
    private $_notificationService;
    /**
     * @var BuildingService
     */
    private $_buildingService;

    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param BuildingService $buildingService
     * @param NotificationService $notificationService
     * @param array $config
     */
    public function __construct(string $id, Module $module, BuildingService $buildingService, NotificationService $notificationService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_notificationService = $notificationService;
        $this->_buildingService = $buildingService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'only' => ['special-offer', 'consultation', 'contact-us', 'get-presentation', 'presentation'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['special-offer', 'consultation', 'contact-us', 'get-presentation', 'presentation'],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actionSpecialOffer()
    {
        $this->layout = false;

        $form = new SpecialOfferForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $params = [
                    'to' => Yii::$app->params['landingEmails'],
                    'receiverName' => 'Eagent Support',
                    'message' => $this->render('@common/views/mail/partial/contact-text', [
                        'contacts' => [
                            [
                                'title' => Yii::t('notification', 'Name'),
                                'value' => $form['name']
                            ],
                            [
                                'title' => Yii::t('notification', 'Email'),
                                'value' => $form['email']
                            ],
                            [
                                'title' => Yii::t('notification', 'Phone'),
                                'value' => $form['phone']
                            ],
                        ],
                        'heading' => Yii::t('notification', 'You got a special offer request from user with contacts:'),
                        'message' => Yii::t('notification', '')
                    ]),
                    'subject' => Yii::t('notification', 'Special offer request'),
                    'view' => 'simple-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }
        }

        return [
            'html' => $this->renderAjax('special-offer', [
                'offerForm' => $form,
            ]),
            'success' => true,
        ];
    }

    /**
     * @return array
     */
    public function actionConsultation()
    {
        $this->layout = false;

        $form = new ConsultationForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $params = [
                    'to' => Yii::$app->params['landingEmails'],
                    'receiverName' => 'Eagent Support',
                    'message' => $this->render('@common/views/mail/partial/contact-text', [
                        'contacts' => [
                            [
                                'title' => Yii::t('notification', 'Name'),
                                'value' => $form['name']
                            ],
                            [
                                'title' => Yii::t('notification', 'Email'),
                                'value' => $form['email']
                            ],
                            [
                                'title' => Yii::t('notification', 'Phone'),
                                'value' => $form['phone']
                            ],
                        ],
                        'heading' => Yii::t('notification', 'You got a request for consultation on buying property from user with contacts:'),
                        'message' => Yii::t('notification', '')
                    ]),
                    'subject' => Yii::t('notification', 'Consultation request'),
                    'view' => 'simple-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }
        }

        return [
            'html' => $this->renderAjax('consultation', [
                'consultationForm' => $form,
            ]),
            'success' => true,
        ];
    }

    /**
     * @return array
     */
    public function actionGetPresentation()
    {
        $this->layout = false;

        $form = new ConsultationForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $params = [
                    'to' => Yii::$app->params['landingEmails'],
                    'receiverName' => 'Eagent Support',
                    'message' => $this->render('@common/views/mail/partial/contact-text', [
                        'contacts' => [
                            [
                                'title' => Yii::t('notification', 'Name'),
                                'value' => $form['name']
                            ],
                            [
                                'title' => Yii::t('notification', 'Email'),
                                'value' => $form['email']
                            ],
                            [
                                'title' => Yii::t('notification', 'Phone'),
                                'value' => $form['phone']
                            ],
                        ],
                        'heading' => Yii::t('notification', 'You got a request for new constructions presentation from user with contacts:'),
                        'message' => Yii::t('notification', '')
                    ]),
                    'subject' => Yii::t('notification', 'Presentation request'),
                    'view' => 'simple-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }
        }

        return [
            'html' => $this->renderAjax('get-presentation', [
                'presentationForm' => $form,
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function actionPresentation($id)
    {
        $this->layout = false;

        $form = new ConsultationForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        /** @var PropertyRepositoryInterface $propertyRepository */
        $propertyRepository = Yii::$container->get(PropertyRepositoryInterface::class);
        /** @var PropertyAttributeRepositoryInterface $propertyAttributeRepository */
        $propertyAttributeRepository = Yii::$container->get(PropertyAttributeRepositoryInterface::class);

        $building = $this->_buildingService->getOne(['building.id' => $id]);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $params = [
                    'to' => Yii::$app->params['landingEmails'],
                    'receiverName' => 'Eagent Support',
                    'message' => $this->render('@common/views/mail/partial/contact-text', [
                        'contacts' => [
                            [
                                'title' => Yii::t('notification', 'Name'),
                                'value' => $form['name']
                            ],
                            [
                                'title' => Yii::t('notification', 'Email'),
                                'value' => $form['email']
                            ],
                            [
                                'title' => Yii::t('notification', 'Phone'),
                                'value' => $form['phone']
                            ],
                        ],
                        'heading' => Yii::t('notification', 'You got a request presentation for building {name} from user with contacts:', [
                            'name' => $building['name']
                        ]),
                        'message' => Yii::t('notification', '')
                    ]),
                    'subject' => Yii::t('notification', 'Presentation request'),
                    'view' => 'simple-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }
        }

        $minPrice = $propertyRepository->select(['min' => 'min(default_price)'], true)->findOneByCriteria([
            'building_id' => $id
        ], true);
        $rooms = $propertyAttributeRepository->joinWith(['property'], false)
            ->groupBy('property_attribute.id')
            ->orderBy('property_attribute.id')
            ->findManyByCriteria([
                'property.building_id' => $id,
                'entity_alias' => 'rooms'
            ], true);
        $rooms = array_unique(array_column($rooms, 'value_number'));
        sort($rooms);
        $rooms = implode(', ', $rooms);

        return [
            'html' => $this->renderAjax('presentation', [
                'presentationForm' => $form,
                'building' => $building,
                'minPrice' => $minPrice['min'],
                'rooms' => $rooms,
                'route' => ['/landing/ajax/presentation', 'id' => $id]
            ]),
            'success' => true,
        ];
    }

    /**
     * @return array|bool
     */
    public function actionContactUs()
    {
        $this->layout = false;

        $form = new ContactForm();
        $input = Yii::$app->request->post();
        $form->load($input);

        if (!empty($input)) {
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $params = [
                    'to' => Yii::$app->params['landingEmails'],
                    'receiverName' => 'Eagent Support',
                    'message' => $this->render('@common/views/mail/partial/contact-text', [
                        'contacts' => [
                            [
                                'title' => Yii::t('notification', 'Name'),
                                'value' => $form['name']
                            ],
                            [
                                'title' => Yii::t('notification', 'Email'),
                                'value' => $form['email']
                            ],
                            [
                                'title' => Yii::t('notification', 'Phone'),
                                'value' => $form['phone']
                            ],
                            [
                                'title' => Yii::t('notification', 'Message'),
                                'value' => $form['message']
                            ],
                        ],
                        'heading' => Yii::t('notification', 'You got a message from user with contacts:'),
                        'message' => Yii::t('notification', '')
                    ]),
                    'subject' => Yii::t('notification', 'eAgent Message'),
                    'view' => 'simple-message',
                    'locale' => Yii::$app->language
                ];
                $result = $this->_notificationService->sendNotification('email', $params);

                return [
                    'success' => $result,
                ];
            }
        }

        return false;
    }
}
