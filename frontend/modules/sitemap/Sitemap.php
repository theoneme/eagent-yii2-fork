<?php

namespace frontend\modules\sitemap;

/**
 * Class Sitemap
 * @package frontend\modules\sitemap
 */
class Sitemap extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'frontend\modules\sitemap\controllers';
}
