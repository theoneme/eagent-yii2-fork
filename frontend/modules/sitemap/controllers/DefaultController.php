<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 16.02.2017
 * Time: 12:20
 */

namespace frontend\modules\sitemap\controllers;

use common\controllers\FrontEndController;
use common\models\Property;
use frontend\modules\sitemap\services\SitemapService;
use Yii;
use yii\base\Module;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class DefaultController
 * @package frontend\modules\sitemap\controllers
 */
class DefaultController extends FrontEndController
{
    public const MAX_PER_FILE = 4000;
    public const BATCH_SIZE = 200;

    /**
     * @var bool
     */
    public $layout = false;
    /**
     * @var SitemapService
     */
    private $_sitemapService;
    /**
     * @var array
     */
    private $_entityToRoute = [
        'property' => ['/property/property/show'],
        'building' => ['/building/building/show'],
//        'user' => ['/agent/agent/view']
    ];

    /**
     * DefaultController constructor.
     * @param string $id
     * @param Module $module
     * @param SitemapService $sitemapService
     * @param array $config
     */
    public function __construct(string $id, Module $module, SitemapService $sitemapService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_sitemapService = $sitemapService;
    }

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->language = 'ru-RU';
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');

        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $output = null;

        $location = Yii::$app->request->get('app_city');

        foreach ($this->_entityToRoute as $entityKey => $route) {
            $count = $this->_sitemapService->getEntityCount($entityKey, $location);
            $maps = $count / self::MAX_PER_FILE;
            $iterator = 0;
            while ($iterator < $maps) {
                $output .= $this->renderPartial('sitemap', [
                    'url' => Url::to(["/sitemap/default/part", 'entity' => $entityKey, 'index' => $iterator, 'app_language' => 'ru'], true),
                ]);
                $iterator++;
            }
        }

        $output .= $this->renderPartial('sitemap', [
            'url' => Url::to(["/sitemap/default/part", 'entity' => 'category', 'index' => null, 'app_language' => 'ru'], true),
        ]);

        return $this->render('sitemap-wrapper', [
            'content' => $output
        ]);
    }

    /**
     * @param $entity
     * @param $index
     * @return string
     */
    public function actionPart($entity, $index = 0)
    {
        $location = Yii::$app->request->get('app_city');
        $output = '';
        $startOffset = $offset = $index * self::MAX_PER_FILE;

        switch ($entity) {
            case 'property':
            case 'building':
                while ($offset < $startOffset + self::MAX_PER_FILE) {
                    $items = $this->_sitemapService->getItems($entity, self::BATCH_SIZE, $offset, $location);
                    $offset += self::BATCH_SIZE;

                    foreach ($items as $item) {
                        $output .= $this->renderMultilangItem($this->_entityToRoute[$entity], $item);
                    }
                }

                break;

            case 'category':
                $items = $this->_sitemapService->getItems($entity, self::BATCH_SIZE, 0, $location);
                foreach ($items as $item) {
                    $output .= $this->renderMultilangItem(['/property/catalog/index', 'operation' => Property::OPERATION_SALE], $item, 'category');
                    $output .= $this->renderMultilangItem(['/property/catalog/index', 'operation' => Property::OPERATION_RENT], $item, 'category');
                }
        }

        return $this->render('url-wrapper', [
            'content' => $output
        ]);
    }

    /**
     * @param $route
     * @param $data
     * @param string $slugAttribute
     * @return string
     */
    private function renderMultilangItem($route, $data, $slugAttribute = 'slug')
    {
        $mainUrl = Url::to(array_merge($route, [$slugAttribute => $data['slug'], 'app_language' => 'ru']), true);

        $output = <<<xml
            <url>
                <loc>{$mainUrl}</loc>
                <priority>{$data['priority']}</priority>
xml;
        if ($data['image']) {
            $output .= <<<xml
                <image:image><image:loc>{$data['image']}</image:loc></image:image>
xml;
        }
        if ($data['lastMod']) {
            $output .= <<<xml
                <lastmod>{$data['lastMod']}</lastmod>
xml;
        }
        if ($data['changeFreq']) {
            $output .= <<<xml
                <changefreq>{$data['changeFreq']}</changefreq>
xml;
        }
        if (!empty($data['translations'])) {
            foreach ($data['translations'] as $locale => $translation) {
                if ($locale === 'ru-RU') continue;
                $href = Url::to(array_merge($route, [$slugAttribute => $translation['slug'], 'app_language' => Yii::$app->params['supportedLocales'][$locale]]), true);
                $loc = Yii::$app->params['supportedLocales'][$locale];
                $loc = $loc === 'ua' ? 'uk' : $loc;
                $output .= <<<xml
                    <xhtml:link rel="alternate" hreflang="{$loc}" href="{$href}" />
xml;
            }
        }
        $output .= <<<xml
        </url>
xml;
        return $output;
    }
}