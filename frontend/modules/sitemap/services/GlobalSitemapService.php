<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 03.11.2017
 * Time: 10:02
 */

namespace frontend\modules\sitemap\services;

use common\models\Building;
use common\models\Property;
use common\models\Translation;
use common\models\User;
use common\repositories\sql\BuildingRepository;
use common\repositories\sql\CategoryRepository;
use common\repositories\sql\PropertyRepository;
use common\repositories\sql\UserRepository;
use frontend\modules\sitemap\interfaces\SitemapDataInterface;
use frontend\modules\sitemap\mappers\SitemapMapper;
use yii\db\ActiveQuery;

/**
 * Class GlobalSitemapService
 * @package frontend\modules\sitemap\services
 */
class GlobalSitemapService implements SitemapDataInterface
{
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var BuildingRepository
     */
    private $_buildingRepository;
    /**
     * @var UserRepository
     */
    private $_userRepository;
    /**
     * @var CategoryRepository
     */
    private $_categoryRepository;

    /**
     * GlobalSitemapService constructor.
     * @param PropertyRepository $propertyRepository
     * @param BuildingRepository $buildingRepository
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(PropertyRepository $propertyRepository,
                                BuildingRepository $buildingRepository,
                                UserRepository $userRepository,
                                CategoryRepository $categoryRepository)
    {
        $this->_propertyRepository = $propertyRepository;
        $this->_buildingRepository = $buildingRepository;
        $this->_userRepository = $userRepository;
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * @param string $entity
     * @return int
     */
    public function getEntityCount($entity)
    {
        $count = 0;

        switch ($entity) {
            case 'property':
                $count = $this->_propertyRepository->countByCriteria(['status' => Property::STATUS_ACTIVE]);

                break;
            case 'building':
                $count = $this->_buildingRepository->countByCriteria(['status' => Building::STATUS_ACTIVE]);

                break;
            case 'user':
                $count = $this->_userRepository->countByCriteria(['status' => User::STATUS_ACTIVE]);

                break;
            case 'category':
                $count = $this->_categoryRepository->countByCriteria([]);
        }

        return $count;
    }

    /**
     * @param $entity
     * @param $limit
     * @param $offset
     * @return array
     */
    public function getItems($entity, $limit, $offset)
    {
        $data = [];

        switch ($entity) {
            case 'property':
            case 'building':
                $items = $this->{"_{$entity}Repository"}->select(['id', 'created_at', 'updated_at', 'slug'], true)->with(['translations' => function (ActiveQuery $query) {
                    return $query->andOnCondition(['key' => [Translation::KEY_SLUG]]);
                }, 'attachments'])
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy(['id' => SORT_ASC])
                    ->findManyByCriteria(['status' => Property::STATUS_ACTIVE], true);
                foreach ($items as $item) {
                    $data[] = SitemapMapper::getMappedData($item);
                }

                break;
            case 'category':
                $items = $this->_categoryRepository->with(['translations' => function (ActiveQuery $query) {
                    return $query->andOnCondition(['key' => [Translation::KEY_SLUG]]);
                }])
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy(['id' => SORT_ASC])
                    ->findManyByCriteria(['>', 'lvl', 0], true);
                foreach ($items as $item) {
                    $data[] = SitemapMapper::getMappedData($item);
                }

                break;
        }

        return $data;
    }
}