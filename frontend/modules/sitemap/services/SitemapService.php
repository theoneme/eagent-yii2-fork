<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 03.11.2017
 * Time: 10:02
 */

namespace frontend\modules\sitemap\services;

use frontend\modules\sitemap\interfaces\SitemapDataInterface;

/**
 * Class SitemapService
 * @package frontend\modules\sitemap\services
 */
class SitemapService implements SitemapDataInterface
{
    /**
     * @var GlobalSitemapService
     */
    private $_globalSitemapService;
    /**
     * @var LocalSitemapService
     */
    private $_localSitemapService;

    /**
     * SitemapService constructor.
     * @param GlobalSitemapService $globalSitemapService
     * @param LocalSitemapService $localSitemapService
     */
    public function __construct(GlobalSitemapService $globalSitemapService, LocalSitemapService $localSitemapService)
    {
        $this->_globalSitemapService = $globalSitemapService;
        $this->_localSitemapService = $localSitemapService;
    }

    /**
     * @param $entity
     * @param null $location
     * @return int
     */
    public function getEntityCount($entity, $location = null)
    {
        if ($location !== null) {
            return $this->_localSitemapService->getEntityCount($entity);
        }

        return $this->_globalSitemapService->getEntityCount($entity);
    }

    /**
     * @param $entity
     * @param $limit
     * @param $offset
     * @param null $location
     * @return array
     */
    public function getItems($entity, $limit = null, $offset = null, $location = null)
    {
        if ($location !== null) {
            return $this->_localSitemapService->getItems($entity, $limit, $offset);
        }

        return $this->_globalSitemapService->getItems($entity, $limit, $offset);
    }
}