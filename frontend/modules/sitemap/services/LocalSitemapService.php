<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 03.11.2017
 * Time: 10:02
 */

namespace frontend\modules\sitemap\services;

use common\mappers\LocationFilterMapper;
use common\models\Building;
use common\models\Property;
use common\models\Translation;
use common\models\User;
use common\repositories\sql\BuildingRepository;
use common\repositories\sql\CategoryRepository;
use common\repositories\sql\PropertyRepository;
use common\repositories\sql\UserRepository;
use frontend\modules\sitemap\interfaces\SitemapDataInterface;
use frontend\modules\sitemap\mappers\SitemapMapper;
use frontend\services\LocationService;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class LocalSitemapService
 * @package frontend\modules\sitemap\services
 */
class LocalSitemapService implements SitemapDataInterface
{
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var BuildingRepository
     */
    private $_buildingRepository;
    /**
     * @var UserRepository
     */
    private $_userRepository;
    /**
     * @var CategoryRepository
     */
    private $_categoryRepository;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * LocalSitemapService constructor.
     * @param PropertyRepository $propertyRepository
     * @param BuildingRepository $buildingRepository
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @param LocationService $locationService
     */
    public function __construct(PropertyRepository $propertyRepository,
                                BuildingRepository $buildingRepository,
                                UserRepository $userRepository,
                                CategoryRepository $categoryRepository,
                                LocationService $locationService)
    {
        $this->_propertyRepository = $propertyRepository;
        $this->_buildingRepository = $buildingRepository;
        $this->_userRepository = $userRepository;
        $this->_categoryRepository = $categoryRepository;
        $this->_locationService = $locationService;
    }

    /**
     * @param $entity
     * @return int
     */
    public function getEntityCount($entity)
    {
        $count = 0;

        switch ($entity) {
            case 'property':
                $criteria = $this->getLocationCriteria();
                $count = $this->_propertyRepository->countByCriteria(array_merge($criteria, ['status' => Property::STATUS_ACTIVE]));

                break;
            case 'building':
                $criteria = $this->getLocationCriteria();
                $count = $this->_buildingRepository->countByCriteria(array_merge($criteria, ['status' => Building::STATUS_ACTIVE]));

                break;
            case 'user':
                $count = $this->_userRepository->countByCriteria(['status' => User::STATUS_ACTIVE]);

                break;
            case 'category':
                $count = $this->_categoryRepository->countByCriteria([]);
        }

        return $count;
    }

    /**
     * @param $entity
     * @param $limit
     * @param $offset
     * @return array
     */
    public function getItems($entity, $limit, $offset)
    {
        $data = [];

        switch ($entity) {
            case 'property':
            case 'building':
                $criteria = $this->getLocationCriteria();
                $items = $this->{"_{$entity}Repository"}->with(['translations' => function (ActiveQuery $query) {
                    return $query->andOnCondition(['key' => [Translation::KEY_SLUG, Translation::KEY_TITLE]]);
                }, 'attachments'])
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy(['id' => SORT_ASC])
                    ->groupBy(['id'])
                    ->findManyByCriteria(array_merge($criteria, ['status' => Building::STATUS_ACTIVE]));

                foreach ($items as $item) {
                    $data[] = SitemapMapper::getMappedData($item);
                }
                break;
            case 'category':
                $items = $this->_categoryRepository->with(['translations' => function (ActiveQuery $query) {
                    return $query->andOnCondition(['key' => [Translation::KEY_SLUG]]);
                }])
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy(['id' => SORT_ASC])
                    ->findManyByCriteria(['>', 'lvl', 0], true);
                foreach ($items as $item) {
                    $data[] = SitemapMapper::getMappedData($item);
                }

                break;
        }

        return $data;
    }

    /**
     * @return array
     */
    private function getLocationCriteria()
    {
        $criteria = [];

        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $locationParams = LocationFilterMapper::getMappedData($locationData);

        $lat = ArrayHelper::remove($locationParams, 'lat');
        $lng = ArrayHelper::remove($locationParams, 'lng');
        $radius = ArrayHelper::remove($locationParams, 'radius') ?? 100;
        if ($lat !== null && $lng !== null) {
            $criteria = ['and', ['radius', $lat, $lng, $radius], $locationParams];
        }

        return $criteria;
    }
}