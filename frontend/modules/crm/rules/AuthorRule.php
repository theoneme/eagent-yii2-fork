<?php

namespace frontend\modules\crm\rules;

use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Class AuthorRule
 * @package frontend\modules\crm\rules
 */
class AuthorRule extends Rule
{
    /**
     * @var string
     */
    public $name = 'authorRule';

    /**
     * @param int|string $memberId
     * @param Item $item
     * @param array $params
     * @return bool
     */
    public function execute($memberId, $item, $params)
    {
        return !empty($params['member_id']) && $params['member_id'] === $memberId;
    }
}