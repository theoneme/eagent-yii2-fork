<?php

namespace frontend\modules\crm\rules;

use frontend\modules\crm\models\CrmInstance;
use frontend\modules\crm\models\CrmMember;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Class ViewRule
 * @package frontend\modules\crm\rules
 */
class ViewRule extends Rule
{
    /**
     * @var string
     */
    public $name = 'viewRule';

    /**
     * @param int|string $memberId
     * @param Item $item
     * @param array $params
     * @return bool
     */
    public function execute($memberId, $item, $params)
    {
        /* @var CrmMember $member */
        $member = ArrayHelper::getValue($params, 'member');
        return $member->crm !== null && $member->status === CrmMember::STATUS_ACTIVE && $member->crm->status === CrmInstance::STATUS_ACTIVE;
    }
}