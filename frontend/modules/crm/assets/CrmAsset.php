<?php

namespace frontend\modules\crm\assets;

use dmstr\web\AdminLteAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class CrmAsset
 * @package frontend\modules\crm\assets
 */
class CrmAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/crm/web';
    public $css = [
        'css/crm.css',
        'css/forms.css',
    ];
    public $js = [

    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        AdminLteAsset::class
    ];
}