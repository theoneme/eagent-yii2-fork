<?php

namespace frontend\modules\crm;

use frontend\modules\crm\models\CrmInstance;
use frontend\modules\crm\models\CrmMember;
use Yii;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

/**
 * Class Module
 * @package frontend\modules\crm
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\crm\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        \Yii::$app->i18n->translations['crm*'] = [
            'class' => PhpMessageSource::class,
            'basePath' => '@frontend/modules/crm/messages',

            'fileMap' => [
                'crm' => 'crm.php',
            ],
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getCrmMembers()
    {
        return CrmMember::find()->joinWith(['crm'])->where([
            'crm_member.user_id' => Yii::$app->user->identity->getId(),
            'crm_instance.status' => CrmInstance::STATUS_ACTIVE,
            'crm_member.status' => CrmMember::STATUS_ACTIVE,
        ])->all();
    }
}
