<?php 
 return [
    'Add new customer' => 'เพิ่มเป็นลูกค้าใหม่',
    'Add new lead' => 'เพื่อเพิ่มเบาะแสใหม่',
    'Add new member' => 'เพื่อเพิ่มใหม่ผู้มีส่วนร่วม@item participation is optional',
    'Address' => 'ที่อยู่',
    'Address details' => 'ที่อยู่ของข้อมูล',
    'Contact details' => 'ข้อมูลรายการที่อยู่ติดต่อ',
    'Created At' => 'สร้างเดท',
    'Currency' => 'สัญลักษณ์ของเงินตรา',
    '(customer management systems)' => '(ลูกค้าการจัดการระบบ/การจัดการลูกค้าของระบบ)',
    'Customer' => 'ลูกค้า',
    'Customer requests/Leads' => 'ลูกค้าสั่ง/เบาะแส',
    'Customers' => 'ลูกค้า',
    'Description' => 'รายละเอียด',
    'Fill out the information with maximum amount of details' => 'กรอกข้อมูลที่เป็นกล่องแสดงรายละเอียดเท่าที่เป็นไปได้',
    'increase sales in your company.' => 'จะเพิ่มการขายอยู่ในบริษัทของคุณ.',
    'Info' => 'รายละเอียด',
    'Invite your colleagues to join the work by this title. You can have several CRM and you can switch between it' => 'ในหัวเรื่องคุณจะสามารถที่จะเชิญเพื่อนร่วมงานของคุณจะมาร่วมกับในที่ทำงาน คุณสามารถมีหลาย CRM และนั่นสามารถสลับไปมาระหว่างพวกเขา',
    'Leads' => 'Lida',
    'Members' => 'ร่วม',
    'Name' => 'ชื่อ',
    'Name of your CRM' => 'ชื่อของของคุณ CRM',
    'Name your CRM' => 'ชื่อของคุณ CRM',
    'New Customer' => 'ลูกค้าใหม่',
    'New Lead' => 'ใหม่กปิด',
    'New Member' => 'สมาชิกใหม่',
    'Opportunity Amount' => 'ศักยภาพเพียงพอที่ใช้',
    'Other' => 'มากกว่า',
    'Owner' => 'เจ้าของ',
    'Publish Request' => 'ที่โฆษณาของโปรแกรม',
    'Admin' => 'ผู้ดูแลระบบ',
    'Content manager' => 'เนื้อหาผู้จัดการ',
    'Member' => 'งานปาร์ตี้',
    'Phone' => 'โทรศัพท์',
    'Role' => 'บทบาท',
    'Save' => 'บันทึก',
    'Source' => 'แหล่งข่าว',
    'Status' => 'สถานะ',
    'The requested page does not exist' => 'ร้องของหน้าไม่พบ',
    'Title' => 'ชื่อ',
    'Type' => 'ประเภท',
    'Update' => 'เพื่อเปลี่ยน',
    'Update customer: {name}' => 'แก้ไขลูกค้า:{name}',
    'Update lead: {name}' => 'แก้ไข Lida:{name}',
    'Update member: {name}' => 'แก้ไขผู้มีส่วนร่วม@item participation is optional:{name}',
    'Updated At' => 'วันเปลี่ยนแปลง',
    'User' => 'ผู้ใช้',
    'We give you the opportunity to use CRM for free' => 'ให้คุณกับโอกาสที่จะใช้ CRM',
    'When you publish Request, other clients and agents will be able to see it' => 'ถ้าคุณตีพิมพ์เป็นโปรแกรมมันจะเป็นสามารถมองเห็นอีกลูกค้าและเจ้าหน้าที่',
    'You can keep records of customers, requests and' => 'นี่จะอนุญาตให้บันทึกของลูกค้าและคำสั่งแล้ว',
    'You must select or add new CRM to work with it' => 'คุณต้องเลือกหรือเพิ่ม CRM ต้องทำงานกับเธอ',
    'Select or add new CRM' => 'เลือกหรือเพิ่ม CRM',
    'Basic details' => 'พื้นฐานข้อมูล',
    'Additional details' => 'รายละเอียดเพิ่มเติม@label',
    'Are you sure you want to leave the CRM?' => 'คุณแน่ใจนะว่าคุณอยากจะออกจาก CRM?',
    'Are you sure you want to delete the CRM?' => 'คุณแน่ใจหรือว่าคุณต้องการจะลบ CRM?',
    'This CRM does not exist or you no longer have access to it' => 'นี่ CRM ยังไม่มีอยู่หรือคุณยังไม่ได้รับอนุญาตให้เข้าถึง',
    'You are not allowed to perform this action' => 'คุณยังไม่ได้รับอนุญาตให้ทำการแสดงการกระทำนี้',
    'Add CRM' => 'เพิ่ม CRM',
    'Delete CRM' => 'ลบ CRM',
    'Leave CRM' => 'ต้องทิ้ง CRM',
    'Deleted' => 'กลบออกไป',
    'Active' => 'ที่ทำงานอยู่',
    'Paused' => 'ถูกพักการเรี',
    'Individual' => 'ส่วนตัว',
    'Business' => 'โฆษณา',
    'Logout' => 'ส่งออก',
    'The CRM settings has been successfully updated' => 'ที่ CRM คือการปรับแต่งปรับปรุงเรียบร้อยแล้ว',
    'Member details' => 'ข้อมูลเกี่ยวกับผู้เข้าร่วมทดสอ',
    'Come up with the name of your CRM' => 'ให้ชื่อของคุณ CRM',
    'It will be reflected in your personal account and by it you will invite members.' => 'มันจะสะท้อนอยู่ในส่วนตัวของบัญชีกับมันคุณจะเชิญคนอื่น',
    'Upload an avatar' => 'อัปโหลดรูปแทนตัว',
    'Upload a photo that makes it easier for you to distinguish your CRM' => 'อัพโหลดรูปภาพซึ่งจะง่ายกว่าที่จะแยกความแตกต่างของคุณ CRM',
    'Upload avatar for your CRM' => 'อัปโหลดรูปภาพจากของคุณ CRM',
    'CRM settings' => 'CRM customizations',
    'Property object' => 'อสังหาริมทรัพย์วัตถุ',
    'Assistant' => 'ผู้ช่วย',
    'Last name' => 'นามสกุล',
    'Middle name' => 'Patronymic',
    'Birth date' => 'วันแห่งชาติกำเนิด',
    'Photo' => 'ภาพถ่าย',
    'Family status' => 'สถานภาพการสมรส',
    'Country resident' => 'เป็นเรสซิเดนท์ของประเทศ',
    'Single' => 'ไม่ได้แต่งงาน/ไม่แต่งงานกัน',
    'Married' => 'แต่งงานกัน',
    'Divorced' => 'หย่ากัน',
    'Civil marriage' => 'ในเมืองแต่งงาน',
    'Widow/widower' => 'แม่หม้าย/พ่อหม้าย',
    'Enter birth date' => 'ป้อนชื่อของคุณวันแห่งชาติกำเนิด',
    'Cancel' => 'ต้องยกเลิก',
    'Value' => 'ค่า',
    'General information' => 'ข้อมูลทั่วไป',
    'Additional Contact' => 'เพิ่มเติมติดต่อ',
    'Set Additional Contact' => 'สำหรับกำหนดเพิ่มเติมติดต่อ',
    'Select contact type' => 'เลือกประเภทติดต่อ',
    'Enter contact info' => 'ป้อนข้อมูลรายการที่อยู่ติดต่อของคุณ',
    'Add New Contact' => 'เพื่อเพิ่มที่อยู่ติดต่อใหม่',
    'Additional Document' => 'เอกสารเพิ่มเติม',
    'Upload Document' => 'ดาวน์โหลดเอกสาร',
    'Select document type' => 'เลือกประเภทของเอกสาร',
    'Passport' => 'พาสปอร์ต',
    'Birth Certificate' => 'คลอดออกใบรับรอง',
    'Marriage Certificate' => 'การแต่งงานออกใบรับรอง',
    'Divorce Certificate' => 'การหย่าออกใบรับรอง',
    'Driver License' => 'ใบขับขี่',
    'Add New Document' => 'เพื่อเพิ่มเอกสารใหม่',
    'Documents' => 'เอกสาร',
    'Document' => 'เอกสาร',
    'Files' => 'แฟ้ม',
    'Addresses' => 'ที่อยู่',
    'Set Customer Addresses' => 'สำหรับกำหนดที่อยู่ของลูกค้า',
    'Add New Address' => 'เพิ่มที่อยู่ใหม่',
    'Site Language' => 'ภาษาบนเว็บไซต์',
    'Communication Language' => 'ภาษาของการสื่อสาร',
    'Enter the name of the member and define his access rights.<br>Administrator - can edit all information and manage members.<br>Assistant - can edit others information.<br>Content Manager - can only add and edit own information.<br>Member - can only view information without access to add and edit' => 'ป้อนชื่อของผู้มีส่วนร่วม@item participation is optional คุณต้องการเพิ่มต้องของคุณ CRM และระบุตัวเขาเข้าถึงสิทธิ<br>ผู้ดูแลระบบหรือบริการสามารถแก้ไขข้อมูลและจะจัดการผู้เข้าร่วมทดสอบ<br>นักศึกษาผู้ช่วยสามารถแก้ไขที่คนอื่นเป็นข้อมูล<br>เนื้อหาผู้จัดการได้เพียงเพิ่มและแก้ไขของตัวเองข้อมูล<br>สมาชิกได้เพียงมุมมองข้อมูลโดยไม่มีการเข้าถึงต้องการเพิ่มและการแก้ไข',
    'Add New Member' => 'เพื่อเพิ่มใหม่ผู้มีส่วนร่วม@item participation is optional',
    'Serial Number' => 'ชุดและหมายเลข',
    'Issue Date' => 'เดทของปัญหา',
    'Expiration Date' => 'วันสิ้นสุด',
    'Enter issue date' => 'ป้อนเดทของ issuance',
    'Enter expiration date' => 'ป้อนวันสิ้นสุด',
];