<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\crm\models\CrmMember */
/* @var array $users */

$this->title = Yii::t('crm', 'New Member');
$this->params['breadcrumbs'][] = ['label' => Yii::t('crm', 'Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-member-create">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'users' => $users
    ]) ?>

</div>
