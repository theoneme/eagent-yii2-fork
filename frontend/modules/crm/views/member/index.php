<?php

use frontend\modules\crm\decorators\MemberRoleDecorator;
use frontend\modules\crm\decorators\MemberStatusDecorator;
use frontend\modules\crm\models\CrmMember;
use frontend\modules\crm\models\search\CrmMemberSearch;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var View $this */
/* @var CrmMemberSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */
/* @var CrmMember $currentMember */

$this->title = Yii::t('crm', 'Members');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-member-index">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>
    <?php if ($currentMember->can('manage')) { ?>
        <p>
            <?= Html::a(Yii::t('crm', 'Add new member'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 50px;'],
                'options' => ['style' => 'width: 50px']
            ],
            [
                'attribute' => 'username',
                'label' => $searchModel->getAttributeLabel('username'),
                'value' => function ($model) {
                    /* @var $model CrmMember*/
                    return Html::img($model->user->avatar, ['style' => 'width: 50px']) . '&nbsp;' . $model->user->username;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'role',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model CrmMember*/
                    return MemberRoleDecorator::decorate($model->role);
                },
                'filter' => MemberRoleDecorator::getRoleLabels()
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model CrmMember*/
                    return MemberStatusDecorator::decorate($model->status);
                },
                'filter' => MemberStatusDecorator::getStatusLabels()
            ],
            [
                'class' => ActionColumn::class,
                'header' => 'Actions',
                'template' => '{update} {delete}',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) use ($currentMember){
                        /* @var $model CrmMember*/
                        return (in_array($model->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $currentMember->can('manage')) || (in_array($model->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $currentMember->can('manageAdmins'));
                    },
                    'delete' => function ($model, $key, $index) use ($currentMember){
                        /* @var $model CrmMember*/
                        return (in_array($model->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $currentMember->can('manage')) || (in_array($model->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $currentMember->can('manageAdmins'));
                    }
                ],
            ],
        ],
    ]); ?>
</div>
