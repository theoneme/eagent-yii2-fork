<?php

use frontend\modules\crm\decorators\MemberRoleDecorator;
use frontend\modules\crm\decorators\MemberStatusDecorator;
use frontend\modules\crm\models\CrmMember;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var View $this */
/* @var CrmMember $model */
/* @var ActiveForm $form */
/* @var array $users */
?>

<div class="wizard-content">
    <?php
    $form = ActiveForm::begin([
//        'action' => Url::to($action),

        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'id' => 'member-form',
            'enctype' => 'multipart/form-data',
            'class' => 'form-wizard'
        ],
    ]); ?>

    <div class="row wizard-row">
        <div class="col-md-9 col-sm-12">

            <div class="wizards-steps">
                <section id="panel-basic">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'Basic details') ?>
                    </div>
                    <div class="row step-content">
                        <div class="col-md-8">
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('crm', 'Add New Member') ?>"
                                 data-html="true"
                                 data-content="<?= Yii::t('crm', 'Enter the name of the member and define his access rights.<br>Administrator - can edit all information and manage members.<br>Assistant - can edit others information.<br>Content Manager - can only add and edit own information.<br>Member - can only view information without access to add and edit') ?>">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php if ($model->isNewRecord) { ?>
                                            <?= $this->render('@frontend/modules/crm/views/common/user-autocomplete', [
                                                'form' => $form,
                                                'model' => $model,
                                                'attribute' => 'user_id',
                                                'items' => $users,
                                            ]); ?>
                                        <?php } else { ?>
                                            <?= $form->field($model, 'status')->dropDownList(MemberStatusDecorator::getStatusLabels()); ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'role')->dropDownList(array_diff_key(MemberRoleDecorator::getRoleLabels(), [CrmMember::ROLE_OWNER => 0])) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div class="form-group text-right">
                    <?= Html::a(Yii::t('crm', 'Cancel'), ['/crm/member/index'], ['class' => 'pull-left btn btn-white-blue btn-big']) ?>
                    <?= Html::submitButton(Yii::t('crm', 'Save'), ['class' => 'btn btn-blue-white btn-big']) ?>
                </div>
            </div>
        </div>

        <nav class="col-md-3 visible-lg visible-md progress-menu-nav menu-wizard-aside">
            <div class="nav" data-spy="affix" data-offset-top="130">
                <div class="progress-steps">
                    <div class="progress-percent text-center">
                        0
                    </div>
                    <div class="progress-info">
                        <?= Yii::t('crm', 'Fill out the information with maximum amount of details') ?>
                    </div>
                </div>
                <ul class="no-list progress-menu">
                    <li class="active">
                        <a href="#panel-basic"><?= Yii::t('crm', 'Basic details') ?></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<<JS
    $('.block-with-notes').popover({
        trigger: 'hover',
        placement: 'right'
    });
    $('body').scrollspy({
        target: '.progress-menu-nav',
        offset: 200
    });
    
JS;
$this->registerJs($script);

?>