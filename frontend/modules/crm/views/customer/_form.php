<?php

use common\assets\GoogleAsset;
use common\helpers\FileInputHelper;
use frontend\modules\crm\decorators\CustomerFamilyStatusDecorator;
use frontend\modules\crm\decorators\CustomerStatusDecorator;
use frontend\modules\crm\decorators\CustomerTypeDecorator;
use frontend\modules\crm\models\CrmCustomer;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use frontend\modules\crm\assets\CrmAsset;

/* @var View $this */
/* @var CrmCustomer $model */
/* @var ActiveForm $form */
/* @var array $can */

CrmAsset::register($this);
GoogleAsset::register($this);

$languages = array_map(function($locale, $text){
    return Yii::t('main', $text, [], $locale);
}, array_keys(Yii::$app->params['languages']), Yii::$app->params['languages']);
?>


<div class="wizard-content">
    <?php
    $form = ActiveForm::begin([
//        'action' => Url::to($action),

        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "
                    <div class='col-md-4 control-label'>
                        {label}
                    </div>
                    <div class='col-md-8'>
                        {input}
                        {error}
                    </div>",
        ],
        'options' => [
            'id' => 'customer-form',
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-wizard'
        ],
    ]); ?>


    <div class="row wizard-row">
        <div class="col-md-9 col-sm-12">

            <div class="wizards-steps">
                <section id="panel-general">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'General information') ?>
                    </div>

                    <div class="row step-content">
                        <div class="col-md-8">
                            <div class="block-with-notes"
                            data-toggle="popover" data-placement="right"
                            data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                            data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'birth_date')->widget(DatePicker::class, [
                                    'options' => ['placeholder' => Yii::t('crm', 'Enter birth date')],
                                    'pluginOptions' => [
                                        'format' => 'dd-mm-yyyy',
                                        'autoclose' => true
                                    ]
                                ]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'info')->textarea(['rows' => 6]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <div class="form-group">
                                    <div class='col-md-4 control-label'>
                                        <?= Html::label(Yii::t('crm', 'Photo'))?>
                                    </div>
                                    <div class='col-md-8 file-input-container'>
                                        <?= FileInput::widget(
                                            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                                'id' => 'photo-upload-input',
                                                'class' => 'file-upload-input',
                                                'name' => 'uploaded_images',
                                                'options' => ['multiple' => false],
                                                'pluginOptions' => [
                                                    'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                                                    'overwriteInitial' => true,
                                                    'initialPreview' => !empty($model->photo) ? $model->photo : [],
                                                    'initialPreviewConfig' => !empty($model->photo) ? [[
                                                        'caption' => basename($model->photo),
                                                        'url' => Url::toRoute('/image/delete'),
                                                        'key' => 0
                                                    ]] : [],
                                                ]
                                            ])
                                        ) ?>
                                        <div class="images-container">
                                            <?= $form->field($model, 'photo', ['template' => '{input}'])->hiddenInput([
                                                'value' => FileInputHelper::buildOriginImagePath($model->photo),
                                                'data-key' => 'image_init_0',
                                                'id' => 'customer-photo'
                                            ])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="panel-contact">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'Contact details') ?>
                    </div>
                    <div class="row step-content">
                        <div class="col-md-8" data-role="dynamic-contacts-container">
                            <?php foreach ($model->contacts as $key => $contact) {
                                echo $this->render('contact-partial', [
                                    'model' => $contact,
                                    'form' => $form,
                                    'iterator' => $key,
                                    'createForm' => false
                                ]);
                            } ?>
                        </div>
                        <div class="col-md-12">
                            <?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('crm', 'Add New Contact'), '#', [
                                'data-action' => 'add-new-contact'
                            ]) ?>
                        </div>
                    </div>
                </section>

                <section id="panel-address">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'Address details') ?>
                    </div>
                    <div class="row step-content">
                        <div class="col-md-8" data-role="dynamic-address-container">
                            <?php foreach ($model->addresses as $key => $address) {
                                echo $this->render('address-partial', [
                                    'model' => $address,
                                    'form' => $form,
                                    'iterator' => $key,
                                    'createForm' => false
                                ]);
                            } ?>
                        </div>
                        <div class="col-md-12">
                            <?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('crm', 'Add New Address'), '#', [
                                'data-action' => 'add-new-address'
                            ]) ?>
                        </div>
                    </div>
                </section>

                <section id="panel-document">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'Documents') ?>
                    </div>
                    <div class="row step-content">
                        <div class="col-md-8" data-role="dynamic-documents-container">
                            <?php foreach ($model->documents as $key => $document) {
                                echo $this->render('document-partial', [
                                    'model' => $document,
                                    'form' => $form,
                                    'iterator' => $key,
                                    'createForm' => false
                                ]);
                            } ?>
                        </div>
                        <div class="col-md-12">
                            <?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('crm', 'Add New Document'), '#', [
                                'data-action' => 'add-new-document'
                            ]) ?>
                        </div>
                    </div>
                </section>

                <section id="panel-other">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'Other') ?>
                    </div>
                    <div class="row step-content">
                        <div class="col-md-8">
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                    <?php if ($can['editOther']) {
                                       echo $this->render('@frontend/modules/crm/views/common/member-autocomplete', [
                                           'form' => $form,
                                           'model' => $model,
                                           'attribute' => 'owner_id',
                                           'items' => $model->owner_id ? [$model->owner_id => "{$model->owner->user->username} (ID: {$model->owner->id}; Email: {$model->owner->user->email})"] : [],
                                       ]);
                                    } ?>
                            </div>
                            <?php if (!$model->isNewRecord) { ?>
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                     data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                      <?= $form->field($model, 'status')->dropDownList(CustomerStatusDecorator::getStatusLabels());?>
                                </div>
                            <?php } ?>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'type')->dropDownList(CustomerTypeDecorator::getTypeLabels()) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'family_status')->dropDownList(CustomerFamilyStatusDecorator::getStatusLabels()) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'country_resident')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'locale')->dropDownList($languages) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'communication_language')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                </section>

            <div class="form-group text-right">
                <?= Html::a(Yii::t('crm', 'Cancel'), ['/crm/customer/index'], ['class' => 'pull-left btn btn-white-blue btn-big']) ?>
                <?= Html::submitButton(Yii::t('crm', 'Save'), ['class' => 'btn btn-blue-white btn-big']) ?>
            </div>
        </div>
        </div>
            <nav class="col-md-3 visible-lg visible-md progress-menu-nav menu-wizard-aside">
                <div class="nav" data-spy="affix" data-offset-top="130">
                    <div class="progress-steps">
                        <div class="progress-percent text-center">
                            0
                        </div>
                        <div class="progress-info">
                            <?= Yii::t('crm', 'Fill out the information with maximum amount of details') ?>
                        </div>
                    </div>
                    <ul class="no-list progress-menu">
                        <li><a href="#panel-general"><?= Yii::t('crm', 'General information') ?></a></li>
                        <li><a href="#panel-contact"><?= Yii::t('crm', 'Contact details') ?></a></li>
                        <li><a href="#panel-address"><?= Yii::t('crm', 'Address details') ?></a></li>
                        <li><a href="#panel-document"><?= Yii::t('crm', 'Documents') ?></a></li>
                        <li><a href="#panel-other" ><?= Yii::t('crm', 'Other') ?></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    <?php ActiveForm::end(); ?>

</div>
<?php
$contactCount = count($model->contacts);
$documentCount = count($model->documents);
$addressCount = count($model->addresses);
$contactUrl = Url::to(['/crm/ajax/render-contact']);
$documentUrl = Url::to(['/crm/ajax/render-document']);
$addressUrl = Url::to(['/crm/ajax/render-address']);
$script = <<<JS
    let contacts = {$contactCount};
    let documents = {$documentCount};
    let addresses = {$addressCount};
    // var attachments = {};

    $('[data-action=add-new-contact]').on('click', function() {
        $.get('{$contactUrl}', {iterator: contacts}, function(result) {
            if(result.success === true) {
                $('[data-role=dynamic-contacts-container]').append(result.html);
            } 
            contacts++;
        });
        
        return false;
    });

    $(document).on('click', '[data-action=remove-contact]', function() {
        $(this).closest('[data-role=contact-item]').remove(); 
        return false;
    });
    
    $('[data-action=add-new-document]').on('click', function() {
        $.get('{$documentUrl}', {iterator: documents}, function(result) {
            if(result.success === true) {
                $('[data-role=dynamic-documents-container]').append(result.html);
            } 
            documents++;
        });
        
        return false;
    });

    $(document).on('click', '[data-action=remove-document]', function() {
        $(this).closest('[data-role=document-item]').remove(); 
        return false;
    });
    
    $('[data-action=add-new-address]').on('click', function() {
        $.get('{$addressUrl}', {iterator: addresses}, function(result) {
            if(result.success === true) {
                $('[data-role=dynamic-address-container]').append(result.html);
            } 
            addresses++;
        });
        
        return false;
    });

    $(document).on('click', '[data-action=remove-address]', function() {
        $(this).closest('[data-role=address-item]').remove(); 
        return false;
    });
    
    $('.block-with-notes').popover({
        trigger: 'hover',
        placement: 'right'
    });
    $('body').scrollspy({
        target: '.progress-menu-nav',
        offset: 200
    });
    
    $('#customer-form').on('beforeSubmit', function() {
        let returnValue = true;
        $(".file-upload-input").each(function(index){
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                returnValue = false;
                return false;
            }
        });
        if (!returnValue) {
            return false;
        }
    });
    
    let hasFileUploadError = false;
    $('#photo-upload-input').on('fileuploaded', function(event, data, previewId, index) {
        let response = data.response;
        $('#customer-photo').val(response.uploadedPath).data('key', response.imageKey);
    }).on('filedeleted', function(event, key) {
        $('#customer-photo').val(null);
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $('#customer-form').submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
    
    $(document).on("fileuploaded", ".document-upload-input", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        let group = imagesContainer.data('group');
        // let id = attachments[group] ? attachments[group] : 0;
        imagesContainer.append("<input class='image-source' name='Attachment[" + group + "][][content]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
        // attachments[group] = id + 1;
    }).on("filedeleted", ".document-upload-input", function(event, key) {
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        imagesContainer.find("input[data-key='" + key + "']").remove();
    }).on("filebatchuploadcomplete", ".document-upload-input", function(event, files, extra) {
        $('#customer-form').submit();
    });
JS;
$this->registerJs($script);

?>