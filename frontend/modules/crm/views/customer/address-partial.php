<?php

use common\widgets\GmapsActiveInputWidget;
use frontend\modules\crm\models\CrmCustomerAddress;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var CrmCustomerAddress $model
 * @var boolean $createForm
 * @var ActiveForm|null $form
 * @var integer $iterator
 * @var View $this
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = new ActiveForm([
        'id' => 'customer-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "
                <div class='col-md-4 control-label'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {input}
                    {error}
                </div>",
        ],
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-wizard'
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

    <div class="block-with-notes crm-dynamic-item"
         data-role="address-item"
         data-toggle="popover"
         data-placement="right"
         data-original-title="<?= Yii::t('crm', 'Addresses') ?>"
         data-content="<?= Yii::t('crm', 'Set Customer Addresses') ?>">
        <div class="row">
            <div class="col-md-11 col-sm-11 col-xs-10">
                <?= Html::activeHiddenInput($model, "[{$iterator}]id")?>
                <?= $form->field($model, "[{$iterator}]type")->textInput() ?>
                <?= GmapsActiveInputWidget::widget([
                    'model' => $model,
                    'form' => $form,
                    'withMap' => true,
                    'addressAttribute' => "[{$iterator}]address",
                    'latAttribute' => "[{$iterator}]lat",
                    'lngAttribute' => "[{$iterator}]lng",
                    'widgetId' => $iterator,
                    'inputOptions' => [
                        'label' => Yii::t('crm', 'Address'),
                        'value' => $model->getAddressText()
                    ],
                    'fieldOptions' => [
                        'template' => "
                            <div class='col-md-4 control-label'>
                                {label}
                            </div>
                            <div class='col-md-8'>
                                {input}
                                {error}
                            </div>
                            <div class='col-md-12'>
                                <div id='google-map{$iterator}' style='height: 240px;'></div>
                            </div>
                        ",
                    ]
                ]) ?>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2">
                <?= Html::a('<i class="fa fa-close"></i>', '#', ['data-action' => 'remove-address']) ?>
            </div>
        </div>
        <?php if ($createForm === true) { ?>
            <?php $attributes = Json::htmlEncode($form->attributes);
            $script = <<<JS
                var attributes = $attributes;
                $.each(attributes, function() {
                    $("#customer-form").yiiActiveForm("add", this);
                });
JS;
            $this->registerJs($script);
        } ?>
    </div>