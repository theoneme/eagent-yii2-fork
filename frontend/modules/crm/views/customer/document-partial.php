<?php

use common\helpers\FileInputHelper;
use frontend\modules\crm\decorators\CustomerDocumentTypeDecorator;
use frontend\modules\crm\models\CrmCustomerDocument;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var CrmCustomerDocument $model
 * @var boolean $createForm
 * @var ActiveForm|null $form
 * @var integer $iterator
 * @var View $this
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = new ActiveForm([
        'id' => 'customer-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "
                <div class='col-md-4 control-label'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {input}
                    {error}
                </div>",
        ],
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-wizard'
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

    <div class="block-with-notes crm-dynamic-item"
         data-role="document-item"
         data-toggle="popover"
         data-placement="right"
         data-original-title="<?= Yii::t('crm', 'Document') ?>"
         data-content="<?= Yii::t('crm', 'Upload Document') ?>">
        <div class="row">
            <div class="col-md-11 col-sm-11 col-xs-10">
                <?= Html::activeHiddenInput($model, "[{$iterator}]id")?>
                <?= $form->field($model, "[{$iterator}]type")->dropDownList(CustomerDocumentTypeDecorator::getTypeLabels()) ?>
                <?= $form->field($model, "[{$iterator}]serial_number")->textInput() ?>
                <div class="form-group">
                    <div class='col-md-4 control-label'>
                        <?= Html::label(Yii::t('crm', 'Files'))?>
                    </div>
                    <div class='col-md-8 file-input-container'>
                        <?= FileInput::widget(
                            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                'id' => "file-upload-input-$iterator",
                                'name' => 'uploaded_images[]',
                                'options' => [
                                    'multiple' => true,
                                    'class' => 'file-upload-input document-upload-input'
                                ],
                                'pluginOptions' => [
                                    'minFileCount' => 1,
//                                    'validateInitialCount' => true,
                                    'overwriteInitial' => false,
                                    'initialPreview' => FileInputHelper::buildPreviews($model->attachments),
                                    'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($model->attachments),
                                ]
                            ])
                        ) ?>
                        <div class="images-container" data-group="<?= $iterator?>">
                            <?php foreach ($model->attachments as $key => $attachment) { ?>
                                <?= $form->field($attachment, "[{$iterator}][{$key}]content", ['template' => '{input}'])->hiddenInput([
                                    'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                                    'data-key' => "image_init_{$key}"
                                ])->label(false) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?= $form->field($model, "[{$iterator}]issue_date")->widget(DatePicker::class, [
                    'options' => ['placeholder' => Yii::t('crm', 'Enter issue date')],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'autoclose' => true
                    ]
                ]) ?>
                <?= $form->field($model, "[{$iterator}]expiration_date")->widget(DatePicker::class, [
                    'options' => ['placeholder' => Yii::t('crm', 'Enter expiration date')],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        'autoclose' => true
                    ]
                ]) ?>
                <?= $form->field($model, "[{$iterator}]description")->textarea() ?>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-2">
                <?= Html::a('<i class="fa fa-close"></i>', '#', ['data-action' => 'remove-document']) ?>
            </div>
        </div>
        <?php if ($createForm === true) { ?>
            <?php $attributes = Json::htmlEncode($form->attributes);
            $script = <<<JS
                var attributes = $attributes;
                $.each(attributes, function() {
                    $("#customer-form").yiiActiveForm("add", this);
                });
JS;
            $this->registerJs($script);
        } ?>
    </div>

<?php
//$count = count($model->attachments);
//$script = <<<JS
//    attachments[$iterator] = $count;
//JS;
//$this->registerJs($script);
