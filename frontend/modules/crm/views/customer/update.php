<?php

use frontend\modules\crm\models\CrmCustomer;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var CrmCustomer $model */
/* @var array $can */

$this->title = Yii::t('crm', 'Update customer: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('crm', 'Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('crm', 'Update');
?>
<div class="crm-customer-update">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'can' => $can
    ]) ?>

</div>
