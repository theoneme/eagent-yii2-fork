<?php

use frontend\modules\crm\decorators\CustomerStatusDecorator;
use frontend\modules\crm\decorators\CustomerTypeDecorator;
use frontend\modules\crm\models\CrmCustomer;
use frontend\modules\crm\models\CrmMember;
use frontend\modules\crm\models\search\CrmCustomerSearch;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var View $this */
/* @var CrmCustomerSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */
/* @var CrmMember $currentMember */

$this->title = Yii::t('crm', 'Customers');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="crm-customer-index">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>
    <?php if ($currentMember->can('create')) { ?>
        <p>
            <?= Html::a(Yii::t('crm', 'Add new customer'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 50px;'],
                'options' => ['style' => 'width: 50px']
            ],
            [
                'attribute' => 'ownerName',
                'label' => $searchModel->getAttributeLabel('ownerName'),
                'value' => function ($model) {
                    /* @var $model CrmCustomer*/
                    return Html::img($model->owner->user->avatar, ['style' => 'width: 50px']) . '&nbsp;' . $model->owner->user->username;
                },
                'format' => 'html',
            ],
            'name',
            'email',
            'phone',
            //'info:ntext',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model CrmCustomer*/
                    return CustomerTypeDecorator::decorate($model->type);
                },
                'filter' => CustomerTypeDecorator::getTypeLabels()
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model CrmCustomer*/
                    return CustomerStatusDecorator::decorate($model->status);
                },
                'filter' => CustomerStatusDecorator::getStatusLabels()
            ],
            'created_at:datetime',
            'updated_at:datetime',


            [
                'class' => ActionColumn::class,
                'header' => 'Actions',
                'template' => '{update} {delete}',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) use ($currentMember) {
                        /* @var $model CrmCustomer*/
                        return $currentMember->can('edit', ['member_id' => $model->owner_id])
                            || (in_array($model->owner->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $currentMember->can('editOthers'))
                            || (in_array($model->owner->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $currentMember->can('manageAdmins'));
                    },
                    'delete' => function ($model, $key, $index) use ($currentMember) {
                        /* @var $model CrmCustomer*/
                        return $currentMember->can('edit', ['member_id' => $model->owner_id])
                            || (in_array($model->owner->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $currentMember->can('editOthers'))
                            || (in_array($model->owner->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $currentMember->can('manageAdmins'));
                    }
                ],
            ],
        ],
    ]); ?>
</div>
