<?php

use frontend\modules\crm\models\CrmCustomer;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var CrmCustomer $model */
/* @var array $can */

$this->title = Yii::t('crm', 'New Customer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('crm', 'Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-customer-create">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'can' => $can
    ]) ?>

</div>
