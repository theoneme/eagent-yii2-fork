<?php

use frontend\modules\crm\models\CrmLead;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var CrmLead $model */
/* @var array $can */

$this->title = Yii::t('crm', 'New Lead');
$this->params['breadcrumbs'][] = ['label' => Yii::t('crm', 'Leads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-lead-create">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'can' => $can
    ]) ?>

</div>
