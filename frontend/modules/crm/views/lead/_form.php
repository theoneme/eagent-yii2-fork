<?php

use common\helpers\DataHelper;
use frontend\modules\crm\decorators\LeadStatusDecorator;
use frontend\modules\crm\decorators\LeadTypeDecorator;
use frontend\modules\crm\models\CrmLead;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var View $this */
/* @var CrmLead $model */
/* @var ActiveForm $form */
/* @var array $can */
?>

<div class="wizard-content">
    <?php
    $form = ActiveForm::begin([
//        'action' => Url::to($action),

        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "
                <div class='col-md-4 control-label'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {input}
                    {error}
                </div>
            ",
        ],
        'options' => [
            'id' => 'lead-form',
            'enctype' => 'multipart/form-data',
            'class' => 'form-horizontal form-wizard'
        ],
    ]); ?>

    <div class="row wizard-row">
        <div class="col-md-9 col-sm-12">

            <div class="wizards-steps">
                <section id="panel-basic">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'Basic details') ?>
                    </div>
                    <div class="row step-content">
                        <div class="col-md-8">
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?php if ($can['editOther']) {
                                    echo $this->render('@frontend/modules/crm/views/common/member-autocomplete', [
                                        'form' => $form,
                                        'model' => $model,
                                        'attribute' => 'owner_id',
                                        'items' => $model->owner_id ? [$model->owner_id => "{$model->owner->user->username} (ID: {$model->owner->id}; Email: {$model->owner->user->email})"] : [],
                                    ]);
                                }?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $this->render('@frontend/modules/crm/views/common/customer-autocomplete', [
                                    'form' => $form,
                                    'model' => $model,
                                    'attribute' => 'customer_id',
                                    'items' => $model->customer_id ? [$model->customer_id => "{$model->customer->name} (ID: {$model->customer->id}; Email: {$model->customer->email})"] : [],
                                ])?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $this->render('@frontend/modules/crm/views/common/property-autocomplete', [
                                    'form' => $form,
                                    'model' => $model,
                                    'attribute' => 'property_id',
                                    'items' => $model->property_id ? [$model->property_id => "(ID: {$model->property_id};)"] : [],
                                ])?>
                            </div>
                            <?php if (!$model->isNewRecord) { ?>
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                     data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                    <?= $form->field($model, 'status')->dropDownList(LeadStatusDecorator::getStatusLabels());?>
                                </div>
                            <?php } ?>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'type')->dropDownList(LeadTypeDecorator::getTypeLabels()) ?>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="panel-additional">
                    <div class="wizard-step-header">
                        <?= Yii::t('crm', 'Additional details') ?>
                    </div>
                    <div class="row step-content">
                        <div class="col-md-8">
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'opportunity_amount')->textInput() ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'currency_code')->dropDownList(DataHelper::getCurrencies()) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="block-with-notes"
                                 data-toggle="popover" data-placement="right"
                                 data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
                                 data-content="<?= Yii::t('wizard', 'Hint') ?>">
                                <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                            </div>
                        </div>
                    </div>
                </section>

            <div class="text-right col-md-8">
                <div class="block-with-notes"
                data-toggle="popover" data-placement="bottom"
                data-original-title="<?= Yii::t('crm', 'Publish Request') ?>"
                data-content="<?= Yii::t('crm', 'When you publish Request, other clients and agents will be able to see it') ?>">
                    <?= Html::a(Yii::t('crm', 'Cancel'), ['/crm/lead/index'], ['class' => 'pull-left btn btn-white-blue btn-big']) ?>
                    <?= Html::submitButton(Yii::t('crm', 'Save'), ['class' => 'btn btn-blue-white btn-big']) ?>
                </div>
            </div>
            </div>
        </div>
        <nav class="col-md-3 visible-lg visible-md progress-menu-nav menu-wizard-aside">
            <div class="nav" data-spy="affix" data-offset-top="130">
                <div class="progress-steps">
                    <div class="progress-percent text-center">
                        0
                    </div>
                    <div class="progress-info">
                        <?= Yii::t('crm', 'Fill out the information with maximum amount of details') ?>
                    </div>
                </div>
                <ul class="no-list progress-menu">
                    <li class="active"><a href="#panel-basic"><?= Yii::t('crm', 'Basic details') ?></a></li>
                    <li><a href="#panel-additional"><?= Yii::t('crm', 'Additional details') ?></a></li>
                </ul>
            </div>
        </nav>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<<JS
    $('.block-with-notes').popover({
        trigger: 'hover',
        placement: 'right'
    });
    $('body').scrollspy({
        target: '.progress-menu-nav',
        offset: 200
    });
    
JS;
$this->registerJs($script);

?>