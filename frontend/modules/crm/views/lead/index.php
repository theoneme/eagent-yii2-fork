<?php

use frontend\modules\crm\decorators\LeadStatusDecorator;
use frontend\modules\crm\decorators\LeadTypeDecorator;
use frontend\modules\crm\models\CrmLead;
use frontend\modules\crm\models\CrmMember;
use frontend\modules\crm\models\search\CrmLeadSearch;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;

/* @var View $this */
/* @var CrmLeadSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */
/* @var CrmMember $currentMember */

$this->title = Yii::t('crm', 'Customer requests/Leads');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-lead-index">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>

    <?php if ($currentMember->can('create')) { ?>
        <p>
            <?= Html::a(Yii::t('crm', 'Add new lead'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 50px;'],
                'options' => ['style' => 'width: 50px']
            ],
            'title',
            [
                'attribute' => 'ownerName',
                'label' => $searchModel->getAttributeLabel('ownerName'),
                'value' => function ($model) {
                    /* @var $model CrmLead*/
                    return Html::img($model->owner->user->avatar, ['style' => 'width: 50px']) . '&nbsp;' . $model->owner->user->username;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'customerName',
                'label' => $searchModel->getAttributeLabel('customerName'),
                'value' => function ($model) {
                    /* @var $model CrmLead*/
                    return $model->customer->name;
                },
                'format' => 'html',
            ],
//            'description:ntext',
            //'source',
            //'opportunity_amount',
            //'currency_code',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model CrmLead*/
                    return LeadTypeDecorator::decorate($model->type);
                },
                'filter' => LeadTypeDecorator::getTypeLabels()
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model CrmLead*/
                    return LeadStatusDecorator::decorate($model->status);
                },
                'filter' => LeadStatusDecorator::getStatusLabels()
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class' => ActionColumn::class,
                'header' => 'Actions',
                'template' => '{update} {delete}',
                'visibleButtons' => [
                    'update' => function ($model, $key, $index) use ($currentMember) {
                        /* @var $model CrmLead*/
                        return $currentMember->can('edit', ['member_id' => $model->owner_id]) || (in_array($model->owner->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $currentMember->can('editOthers')) || (in_array($model->owner->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $currentMember->can('manageAdmins'));
                    },
                    'delete' => function ($model, $key, $index) use ($currentMember) {
                        /* @var $model CrmLead*/
                        return $currentMember->can('edit', ['member_id' => $model->owner_id]) || (in_array($model->owner->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $currentMember->can('editOthers')) || (in_array($model->owner->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $currentMember->can('manageAdmins'));
                    }
                ],
            ],
        ],
    ]); ?>
</div>
