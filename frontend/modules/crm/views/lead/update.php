<?php

use frontend\modules\crm\models\CrmLead;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var CrmLead $model */
/* @var array $can */

$this->title = Yii::t('crm', 'Update lead: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('crm', 'Leads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('crm', 'Update');
?>
<div class="crm-lead-update">

    <h1 class="text-left"><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'can' => $can
    ]) ?>

</div>
