<?php

use frontend\modules\crm\controllers\CrmBaseController;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var CrmBaseController $controller */
$controller = Yii::$app->controller;

?>

<aside class="main-sidebar">
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel text-center">
            <?php if ($controller->currentMember->crm->logo) {?>
                <div class="pull-left image">
                    <?= Html::img($controller->currentMember->crm->logo, ['class' => 'img-circle']) ?>
                </div>
            <?php } ?>
			<span class="crm-name info">
				<?php if (!Yii::$app->user->isGuest) { ?>
                    <?= $controller->currentMember->crm->name ?>
<!--					<a href="#"><i class="circle text-success"></i> Online</a>-->
				<?php } ?>
			</span>
		</div>

		<?= dmstr\widgets\Menu::widget(
			[
				'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
				'items' => [
//					[
//						'label' => Yii::t('crm', 'Menu Yii2'),
//						'options' => ['class' => 'header']
//					],
                    [
                        'label' => Yii::t('crm', 'CRM settings'),
                        'icon' => 'cog',
                        'url' => Url::toRoute('/crm/site/settings'),
                        'active' => $controller->id === 'site' && $controller->action->id === 'settings',
                        'visible' => $controller->currentMember->can('manage'),
                    ],
					[
						'label' => Yii::t('crm', 'Dashboard'),
						'icon' => 'dashboard',
						'url' => Url::toRoute('/crm/site/dashboard'),
						'active' => $controller->id === 'site' && $controller->action->id === 'dashboard',
					],
					[
						'label' => Yii::t('crm', 'Members'),
						'icon' => 'user',
						'url' => Url::toRoute('/crm/member/index'),
						'active' => $controller->id === 'member',
					],
                    [
                        'label' => Yii::t('crm', 'Customers'),
                        'icon' => 'user',
                        'url' => Url::toRoute('/crm/customer/index'),
                        'active' => $controller->id === 'customer'
                    ],
                    [
                        'label' => Yii::t('crm', 'Customer requests/Leads'),
                        'icon' => 'filter',
                        'url' => Url::toRoute('/crm/lead/index'),
                        'active' => $controller->id === 'lead'
                    ],
                    [
                        'label' => Yii::t('crm', 'Logout'),
                        'icon' => 'sign-out',
                        'url' => Url::toRoute('/crm/site/logout'),
                    ],
				],
			]
		) ?>
	</section>
</aside>
