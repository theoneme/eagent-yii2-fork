<?php

use common\helpers\UtilityHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('
            <span class="logo-mini"></span>
            <span class="logo-lg">' . Yii::$app->name . '</span>
        ',
        ['/crm/site/dashboard'],
        ['class' => 'logo']
    ) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown notifications-menu">
                    <?= Html::a('<i class="fa fa-globe"></i>&nbsp;' . Yii::t('main', 'Language'), null, [
                        'class' => 'dropdown-toggle crm-header-language-toggle',
                        'data-toggle' => 'dropdown',
                    ])?>
                    <ul class="dropdown-menu">
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <?php
                                foreach (Yii::$app->params['supportedLocales'] as $locale => $code) {
                                    $languageTitle = Yii::t('main', Yii::$app->params['languages'][$locale], [], $locale);
                                    $languageWord = mb_strtolower(Yii::t('main', 'Language', [], $locale));
                                    echo Html::tag('li',
                                        Html::a("<b>$languageTitle</b>&nbsp;$languageWord",
                                            UtilityHelper::localeCurrentUrl($code), [
                                            'class' => $locale === Yii::$app->language ? 'active' : ''
                                        ])
                                    );
                                } ?>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
