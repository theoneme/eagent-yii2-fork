<?php

use frontend\modules\crm\models\CrmInstance;
use frontend\modules\crm\models\CrmMember;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var CrmMember[] $crmMembers
 */

$this->title = 'eAgent CRM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-login">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="crm-welcome text-center">
        <div><strong><?= Yii::t('crm', 'We give you the opportunity to use CRM for free')?></strong></div>
        <div><?= Yii::t('crm', '(customer management systems)')?></div>
        <div><strong><?= Yii::t('crm', 'You can keep records of customers, requests and')?></strong></div>
        <div><strong><?= Yii::t('crm', 'increase sales in your company.')?></strong></div>
    </div>
    <h2 class="text-center"><?= Yii::t('crm', 'Select or add new CRM')?></h2>

    <div class="crm-table">
        <?php foreach ($crmMembers as $member) {?>
            <div class="crm-table-row">
                <div class="crm-table-cell">
                    <?= Html::a($member->crm->name, ['/crm/site/login', 'id' => $member->id], ['class' => 'crm-login'])?>
                </div>
                <div class="crm-table-cell pull-right">
<!--                    --><?//= Html::a('<i class="fa fa-pensil"></i>', null, [
//                        'class' => 'hint-top',
//                        'data-hint' => Yii::t('crm', 'Edit CRM')
//                    ])?>
                    <?php if ($member->can('manageCRM')) {
                        echo Html::a('<i class="fa fa-times"></i>', ['/crm/instance/delete', 'id' => $member->crm_id], [
                            'class' => 'hint--top',
                            'data-method' => 'post',
                            'data-hint' => Yii::t('crm', 'Delete CRM'),
                            'data-confirm' => Yii::t('crm', 'Are you sure you want to delete the CRM?')
                        ]);
                    }
                    else {
                        echo Html::a('<i class="fa fa-sign-out"></i>', ['/crm/member/leave', 'id' => $member->id], [
                            'class' => 'hint--top',
                            'data-method' => 'post',
                            'data-hint' => Yii::t('crm', 'Leave CRM'),
                            'data-confirm' => Yii::t('crm', 'Are you sure you want to leave the CRM?')
                        ]);
                    }?>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="text-center">
        <?= Html::button(Yii::t('crm', 'Add CRM'), ['class' => 'btn btn-success show-crm-form']) ?>
    </div>
    <div class="crm-form-block">
        <?= $this->render('crm_form', [
            'action' => ['/crm/instance/create'],
            'model' => new CrmInstance()
        ])?>
    </div>
</div>

<?php
$script = <<<JS
    $(document).on('click', '.show-crm-form', function(){
        $(this).hide();
        $('.crm-form-block').show();
    });
JS;
$this->registerJs($script);
