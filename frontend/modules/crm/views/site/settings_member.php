<?php

use frontend\modules\crm\decorators\MemberRoleDecorator;
use frontend\modules\crm\decorators\MemberStatusDecorator;
use frontend\modules\crm\models\CrmInstance;
use frontend\modules\crm\models\CrmMember;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model CrmInstance */
/* @var $form ActiveForm */
?>

<?php if ($model->isNewRecord) { ?>
    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('crm', 'Add New Member') ?>"
         data-content="<?= Yii::t('crm', 'Enter the name of the member and define his access rights.<br>Administrator - can edit all information and manage members.<br>Assistant - can edit others information.<br>Content Manager - can only add and edit own information.<br>Member - can only view information without access to add and edit') ?>">
        <?= $this->render('@frontend/modules/crm/views/common/user-autocomplete', [
            'form' => $form,
            'model' => $model,
            'attribute' => 'user_id',
            'items' => [],
        ]); ?>
    </div>
<?php } else { ?>
    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
         data-content="<?= Yii::t('wizard', 'Hint') ?>">
        <?= $form->field($model, 'status')->dropDownList(MemberStatusDecorator::getStatusLabels())
            ->textInput(['placeholder' => Yii::t('labels', 'Start typing name')]); ?>
    </div>
<?php } ?>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
     data-content="<?= Yii::t('wizard', 'Hint') ?>">
    <?= $form->field($model, 'role')->dropDownList(array_diff_key(MemberRoleDecorator::getRoleLabels(), [CrmMember::ROLE_OWNER => 0])) ?>
</div>
<div class="form-group text-right">
    <?= Html::a(Yii::t('crm', 'Save'), ['class' => 'btn btn-blue-white btn-big']) ?>
</div>