<?php

use frontend\modules\crm\models\CrmInstance;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var View $this */
/* @var mixed $action */
/* @var CrmInstance $model */
/* @var ActiveForm $form */
?>

<div class="crm-form">
    <?php
    $form = ActiveForm::begin([
        'action' => $action,
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'id' => 'crm-form',
            'enctype' => 'multipart/form-data',
        ],
        'fieldConfig' => [
            'template' => "
                <div class='row'>
                    <div class='col-md-5'>
                        {label}
                    </div>
                    <div class='col-md-7'>
                        {input}
                        {error}
                    </div>
                </div>
            ",
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-9 auto-notes"
        data-toggle="popover" data-placement="bottom"
        data-original-title="<?= Yii::t('crm', 'Name your CRM') ?>"
        data-content="<?= Yii::t('crm', 'Invite your colleagues to join the work by this title. You can have several CRM and you can switch between it') ?>">
            <?= $form->field($model, 'name')->textInput()->label(Yii::t('crm','Name of your CRM')) ?>
        </div>

        <div class="col-md-3">
            <div class="form-group text-right">
                <?= Html::submitButton(Yii::t('crm', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<<JS
    $('.auto-notes').popover({
        trigger: 'focus',
        placement: 'right'
    });
JS;
$this->registerJs($script);

?>