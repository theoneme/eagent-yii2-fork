<?php

use common\helpers\FileInputHelper;
use frontend\modules\crm\models\CrmInstance;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model CrmInstance */
/* @var $form ActiveForm */
$this->title = Yii::t('crm', 'CRM settings');
?>

    <div class="wizard-content">
        <?php
        $form = ActiveForm::begin([
//        'action' => Url::to($action),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "
                <div class='col-md-4 control-label'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {input}
                    {error}
                </div>
            ",
            ],
            'options' => [
                'id' => 'crm-settings-form',
                'enctype' => 'multipart/form-data',
                'class' => 'form-horizontal form-wizard'
            ],
        ]); ?>

        <div class="row wizard-row">
            <div class="col-md-9 col-sm-12">

                <div class="wizards-steps">
                    <section id="panel-basic">
                        <div class="wizard-step-header">
                            <?= Yii::t('crm', 'Basic details') ?>
                        </div>
                        <div class="row step-content">
                            <div class="col-md-8">
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('crm', 'Come up with the name of your CRM') ?>"
                                     data-content="<?= Yii::t('crm', 'It will be reflected in your personal account and by it you will invite members.') ?>">
                                    <?= $form->field($model, 'name')->textInput()->label(Yii::t('crm', 'Name of your CRM')) ?>
                                </div>
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('crm', 'Upload an avatar') ?>"
                                     data-content="<?= Yii::t('crm', 'Upload a photo that makes it easier for you to distinguish your CRM') ?>">

                                    <label><?= Yii::t('crm', 'Upload avatar for your CRM') ?></label>
                                    <?= FileInput::widget(
                                        ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                                            'id' => 'file-upload-input',
                                            'name' => 'uploaded_images',
                                            'options' => ['multiple' => false],
                                            'pluginOptions' => [
                                                'overwriteInitial' => true,
                                                'initialPreview' => !empty($model->logo) ? $model->logo : [],
                                                'initialPreviewConfig' => !empty($model->logo) ? [[
                                                    'caption' => basename($model->logo),
                                                    'url' => Url::toRoute('/image/delete'),
                                                    'key' => 0
                                                ]] : [],
                                                'otherActionButtons' => ''
                                            ]
                                        ])
                                    ) ?>
                                    <div class="images-container">
                                        <?= $form->field($model, 'logo', ['template' => '{input}'])->hiddenInput([
                                            'value' => $model->logo,
                                            'data-key' => 'image_init_0'
                                        ])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--                <section id="panel-members">-->
                    <!--                    <div class="wizard-step-header">-->
                    <!--                        --><? //= Yii::t('crm', 'Member details') ?>
                    <!--                    </div>-->
                    <!--                    <div class="row step-content">-->
                    <!--                        <div class="col-md-8">-->
                    <!--                            <div class="child-elements">-->
                    <!--                                --><? //= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('crm', 'Add new member'), ['/crm/member/form'], [
                    //                                    'class' => 'add-element',
                    ////                                'data-type' => 'extra',
                    //                                ])?>
                    <!--                                <div class="elements-container" data-index="-->
                    <? //= count($model->crmMembers) ? max(array_column($model->crmMembers, 'id')) : 0?><!--">-->
                    <!--                                    --><?php //foreach ($model->crmMembers as $member) {
                    //                                        $this->render('settings_member', ['model' => $member, 'form' => $form]);
                    //                                    }?>
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </section>-->

                    <div class="form-group text-right">
                        <?= Html::submitButton(Yii::t('crm', 'Save'), ['class' => 'btn btn-blue-white btn-big']) ?>
                    </div>
                </div>
            </div>

            <nav class="col-md-3 visible-lg visible-md progress-menu-nav menu-wizard-aside">
                <div class="nav" data-spy="affix" data-offset-top="130">
                    <div class="progress-steps">
                        <div class="progress-percent text-center">
                            0
                        </div>
                        <div class="progress-info">
                            <?= Yii::t('crm', 'Fill out the information with maximum amount of details') ?>
                        </div>
                    </div>
                    <ul class="no-list progress-menu">
                        <li class="active">
                            <a href="#panel-basic"><?= Yii::t('crm', 'Basic details') ?></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<<JS
    
    $('.block-with-notes').popover({
        trigger: 'hover',
        placement: 'right'
    });
    $('body').scrollspy({
        target: '.progress-menu-nav',
        offset: 200
    });
    
    $('#crm-settings-form').on('submit', function() {
        if ($("#file-upload-input").fileinput("getFilesCount") > 0) {
            $("#file-upload-input").fileinput("upload");
            return false;
        }
    });
    
    let hasFileUploadError = false;
    $('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
        let response = data.response;
        $('.images-container input').val(response.uploadedPath).data('key', response.imageKey);
    }).on('filedeleted', function(event, key) {
        $('.images-container input').val(null);
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) {
            $('#crm-settings-form').submit();
        } else {
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) {
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
JS;
$this->registerJs($script);

?>