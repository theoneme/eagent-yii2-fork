<?php

use frontend\assets\plugins\SelectizeAsset;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var ActiveRecord $model
 * @var ActiveForm $form
 * @var array $items
 * @var string $attribute
 */

$items = $items ?? [];
SelectizeAsset::register($this);
?>

<?= $form->field($model, $attribute)->dropDownList($items, ['class' => 'member-autocomplete']) ?>

<?php
$url = Url::to(['/crm/autocomplete/members']);
$script = <<<JS
    $(".member-autocomplete").selectize({
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$url", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    });
JS;

$this->registerJs($script);