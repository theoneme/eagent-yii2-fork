<?php

use frontend\assets\plugins\SelectizeAsset;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var ActiveRecord $model
 * @var ActiveForm $form
 * @var array $items
 * @var string $attribute
 */

$items = $items ?? [];
SelectizeAsset::register($this);
?>

<?= $form->field($model, $attribute)->dropDownList([], ['class' => 'user-autocomplete']) ?>

<?php
$url = Url::to(['/crm/autocomplete/users']);

$options = json_encode(array_map(function ($value) {
    return [
        'id' => $value->id,
        'value' => "{$value->username} (ID: {$value->id}; Email: {$value->email})",
        'phone' => $value->phone,
        'img' => !empty($value->avatar) ? $value->avatar : '/images/agent-no-image.png'
    ];
}, $items));
$script = <<<JS
    $(".user-autocomplete").selectize({
        options: $options,
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.post("$url", {
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
        render: {
	        option: function(data, escape) {
	            return '<div class="option custom-selectize-option flex flex-v-center" data-value="' + data.id + '">' +
                           '<img src="' + data.img + '">' +
                           '<span class="selectize-option-title">' + data.value + '</span>' + 
                       '</div>';
	        },
            item: function(data, escape) {
	            return '<div class="item custom-selectize-item" data-value="' + data.value + '">' +
                          '<img src="' + data.img + '">' 
                          + data.value + 
                       '</div>';
	        }
	    },
    });
JS;

$this->registerJs($script);