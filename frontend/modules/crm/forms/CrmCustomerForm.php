<?php
namespace frontend\modules\crm\forms;

use common\models\Attachment;
use common\services\AddressTranslationCreator;
use frontend\modules\crm\models\CrmCustomer;
use Yii;
use yii\base\Model;

/**
 * Class CrmCustomerForm
 * @package frontend\forms\ar
 */
class CrmCustomerForm extends Model
{
    /**
     * @var CrmCustomer
     */
    public $customer;

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function load($data, $formName = null)
    {
        $success = $this->customer->load($data, $formName);

        if ($success) {
            if (!empty($data['CrmCustomerContact'])) {
                foreach ($data['CrmCustomerContact'] as $contact) {
                    $contactRelation = $this->customer->bind('contacts', (int)$contact['id'] ?? null);
                    $contactRelation->attributes = [
                        'value' => $contact['value'],
                        'type' => $contact['type'],
                    ];
                }
            }
            if (!empty($data['CrmCustomerAddress'])) {
                foreach ($data['CrmCustomerAddress'] as $address) {
                    $addressRelation = $this->customer->bind('addresses', (int)$address['id'] ?? null);
                    $addressRelation->attributes = [
                        'type' => $address['type'],
                        'lat' => round($address['lat'], 8),
                        'lng' => round($address['lng'], 8),
                    ];

                    /** @var AddressTranslationCreator $addressCreator */
                    $addressCreator = Yii::$container->get(AddressTranslationCreator::class);
                    $addressCreator->create(array_merge($addressRelation->attributes, ['locale' => Yii::$app->language]));
                }
            }
            if (!empty($data['CrmCustomerDocument'])) {
                foreach ($data['CrmCustomerDocument'] as $key => $document) {
                    $documentRelation = $this->customer->bind('documents', (int)$document['id'] ?? null);
                    $documentRelation->attributes = [
                        'type' => $document['type'],
                        'serial_number' => $document['serial_number'],
                        'description' => $document['description'],
                        'issue_date' => $document['issue_date'],
                        'expiration_date' => $document['expiration_date'],
                    ];
                    $attachments = $data['Attachment'][$key] ?? [];
                    foreach ($attachments as $attachment) {
                        $documentRelation->bindAttachment($attachment['content'])->attributes = ['entity' => Attachment::ENTITY_CRM_CUSTOMER_DOCUMENT, 'content' => $attachment['content']];
                    }
                }
            }
        }
        return $success;
    }
}