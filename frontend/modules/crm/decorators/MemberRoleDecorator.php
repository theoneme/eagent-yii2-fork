<?php

namespace frontend\modules\crm\decorators;

use common\interfaces\DecoratorInterface;
use frontend\modules\crm\models\CrmMember;
use yii\helpers\Html;
use Yii;

/**
 * Class MemberRoleDecorator
 * @package frontend\modules\crm\decorators
 */
class MemberRoleDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getRoleLabels(true);
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown role');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getRoleLabels($colored = false)
    {
        return [
            CrmMember::ROLE_OWNER => $colored
                ? Html::tag('span', Yii::t('crm', 'Owner'), ['style' => 'color: #ac4137'])
                : Yii::t('crm', 'Owner'),
            CrmMember::ROLE_ADMIN => $colored
                ? Html::tag('span', Yii::t('crm', 'Admin'), ['style' => 'color: #ffa423'])
                : Yii::t('crm', 'Admin'),
            CrmMember::ROLE_MODERATOR => $colored
                ? Html::tag('span', Yii::t('crm', 'Assistant'), ['style' => 'color: #ffd288'])
                : Yii::t('crm', 'Assistant'),
            CrmMember::ROLE_CONTENT_MANAGER => $colored
                ? Html::tag('span', Yii::t('crm', 'Content manager'), ['style' => 'color: #3ab845'])
                : Yii::t('crm', 'Content manager'),
            CrmMember::ROLE_MEMBER => $colored
                ? Html::tag('span', Yii::t('crm', 'Member'), ['style' => 'color: #555'])
                : Yii::t('crm', 'Member'),
        ];
    }
}
