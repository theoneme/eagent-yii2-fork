<?php

namespace frontend\modules\crm\decorators;

use common\interfaces\DecoratorInterface;
use frontend\modules\crm\models\CrmCustomerContact;
use Yii;

/**
 * Class CustomerContactTypeDecorator
 * @package frontend\modules\crm\decorators
 */
class CustomerContactTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('crm', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            CrmCustomerContact::TYPE_EMAIL => 'Email',
            CrmCustomerContact::TYPE_PHONE => Yii::t('labels', 'Phone'),
            CrmCustomerContact::TYPE_WEBSITE => Yii::t('labels', 'Website'),
            CrmCustomerContact::TYPE_SKYPE => 'Skype',
            CrmCustomerContact::TYPE_WHATSAPP => 'WhatsApp',
            CrmCustomerContact::TYPE_VIBER => 'Viber',
            CrmCustomerContact::TYPE_TELEGRAM => 'Telegram',
            CrmCustomerContact::TYPE_FACEBOOK => 'Facebook',
            CrmCustomerContact::TYPE_VK => 'Vk',
            CrmCustomerContact::TYPE_TWITTER => 'Twitter',
            CrmCustomerContact::TYPE_GOOGLE => 'Google',
            CrmCustomerContact::TYPE_YOUTUBE => 'Youtube',
            CrmCustomerContact::TYPE_OK => Yii::t('labels', 'Odnoklassniki'),
        ];
    }
}
