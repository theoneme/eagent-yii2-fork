<?php

namespace frontend\modules\crm\decorators;

use common\interfaces\DecoratorInterface;
use frontend\modules\crm\models\CrmCustomerContact;
use frontend\modules\crm\models\CrmCustomerDocument;
use Yii;

/**
 * Class CustomerDocumentTypeDecorator
 * @package frontend\modules\crm\decorators
 */
class CustomerDocumentTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('crm', 'Unknown type');
    }

    /**
     * @return array
     */
    public static function getTypeLabels()
    {
        return [
            CrmCustomerDocument::TYPE_PASSPORT => Yii::t('crm', 'Passport'),
            CrmCustomerDocument::TYPE_BIRTH_CERTIFICATE => Yii::t('crm', 'Birth Certificate'),
            CrmCustomerDocument::TYPE_MARRIAGE_CERTIFICATE => Yii::t('crm', 'Marriage Certificate'),
            CrmCustomerDocument::TYPE_DIVORCE_CERTIFICATE => Yii::t('crm', 'Divorce Certificate'),
            CrmCustomerDocument::TYPE_DRIVER_LICENSE => Yii::t('crm', 'Driver License'),
            CrmCustomerDocument::TYPE_OTHER => Yii::t('crm', 'Other'),
        ];
    }
}
