<?php

namespace frontend\modules\crm\decorators;

use common\interfaces\DecoratorInterface;
use frontend\modules\crm\models\CrmMember;
use yii\helpers\Html;
use Yii;

/**
 * Class MemberStatusDecorator
 * @package frontend\modules\crm\decorators
 */
class MemberStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels(true);
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Unknown status');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getStatusLabels($colored = false)
    {
        return [
            CrmMember::STATUS_DELETED => $colored
                ? Html::tag('span', Yii::t('crm', 'Deleted'), ['style' => 'color: #ac4137'])
                : Yii::t('crm', 'Deleted'),
            CrmMember::STATUS_PAUSED => $colored
                ? Html::tag('span', Yii::t('crm', 'Paused'), ['style' => 'color: #ffd288'])
                : Yii::t('crm', 'Paused'),
            CrmMember::STATUS_ACTIVE => $colored
                ? Html::tag('span', Yii::t('crm', 'Active'), ['style' => 'color: #3ab845'])
                : Yii::t('crm', 'Active'),
        ];
    }
}
