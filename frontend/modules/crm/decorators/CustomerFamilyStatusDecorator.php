<?php

namespace frontend\modules\crm\decorators;

use common\interfaces\DecoratorInterface;
use frontend\modules\crm\models\CrmCustomer;
use Yii;

/**
 * Class CustomerFamilyStatusDecorator
 * @package frontend\modules\crm\decorators
 */
class CustomerFamilyStatusDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getStatusLabels();
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('labels', 'Not specified');
    }

    /**
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
            CrmCustomer::FAMILY_STATUS_SINGLE => Yii::t('crm', 'Single'),
            CrmCustomer::FAMILY_STATUS_MARRIED => Yii::t('crm', 'Married'),
            CrmCustomer::FAMILY_STATUS_DIVORCED => Yii::t('crm', 'Divorced'),
            CrmCustomer::FAMILY_STATUS_CIVIL => Yii::t('crm', 'Civil marriage'),
            CrmCustomer::FAMILY_STATUS_WIDOW => Yii::t('crm', 'Widow/widower'),
        ];
    }
}
