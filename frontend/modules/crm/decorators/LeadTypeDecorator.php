<?php

namespace frontend\modules\crm\decorators;

use common\interfaces\DecoratorInterface;
use Yii;

/**
 * Class LeadTypeDecorator
 * @package frontend\modules\crm\decorators
 */
class LeadTypeDecorator implements DecoratorInterface
{
    /**
     * @param $rawData
     * @return mixed|string
     */
    public static function decorate($rawData)
    {
        $labels = static::getTypeLabels(true);
        return array_key_exists($rawData, $labels) ? $labels[$rawData] : Yii::t('crm', 'Unknown type');
    }

    /**
     * @param bool $colored
     * @return array
     */
    public static function getTypeLabels($colored = false)
    {
        return [
//            CrmCustomer::TYPE_INDIVIDUAL => $colored
//                ? Html::tag('span', Yii::t('crm', 'Individual'), ['style' => 'color: #ac4137'])
//                : Yii::t('crm', 'Individual'),
//            CrmCustomer::TYPE_BUSINESS => $colored
//                ? Html::tag('span', Yii::t('crm', 'Business'), ['style' => 'color: #ffd288'])
//                : Yii::t('crm', 'Business'),
        ];
    }
}
