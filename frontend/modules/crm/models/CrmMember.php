<?php

namespace frontend\modules\crm\models;

use common\models\User;
use frontend\modules\crm\components\CrmDbManager;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "crm_member".
 *
 * @property int $id
 * @property int $user_id
 * @property int $crm_id
 * @property int $role
 * @property int $status
 *
 * @property CrmCustomer[] $crmCustomers
 * @property CrmLead[] $crmLeads
 * @property CrmInstance $crm
 * @property User $user
 */
class CrmMember extends ActiveRecord
{
    public const STATUS_DELETED = -50;
    public const STATUS_PAUSED = 0;
    public const STATUS_ACTIVE = 10;

    public const ROLE_OWNER = 0;
    public const ROLE_ADMIN = 10;
    public const ROLE_MODERATOR = 15;
    public const ROLE_CONTENT_MANAGER = 20;
    public const ROLE_MEMBER = 30;

    private $_access = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'crm_id'], 'required'],
            [['user_id', 'crm_id', 'role', 'status'], 'integer'],
            [['crm_id'], 'exist', 'skipOnError' => true, 'targetClass' => CrmInstance::class, 'targetAttribute' => ['crm_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_PAUSED, self::STATUS_ACTIVE]],
            ['role', 'default', 'value' => self::ROLE_MEMBER],
            ['role', 'in', 'range' => [self::ROLE_OWNER, self::ROLE_ADMIN, self::ROLE_MODERATOR, self::ROLE_CONTENT_MANAGER, self::ROLE_MEMBER]],
            [['role', 'status'], 'filter', 'filter' => 'intval'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('crm', 'ID'),
            'user_id' => Yii::t('crm', 'User'),
            'crm_id' => Yii::t('crm', 'Crm ID'),
            'role' => Yii::t('crm', 'Role'),
            'status' => Yii::t('crm', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmCustomers()
    {
        return $this->hasMany(CrmCustomer::class, ['owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmLeads()
    {
        return $this->hasMany(CrmLead::class, ['owner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrm()
    {
        return $this->hasOne(CrmInstance::class, ['id' => 'crm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param $permissionName
     * @param array $params
     * @param bool $allowCaching
     * @return bool
     */
    public function can($permissionName, array $params = [], $allowCaching = true)
    {
        if ($allowCaching && empty($params) && isset($this->_access[$permissionName])) {
            return $this->_access[$permissionName];
        }
        if (($accessChecker = $this->getAccessChecker()) === null) {
            return false;
        }
        $access = $accessChecker->checkAccess($this->id, $permissionName, $params);
        if ($allowCaching && empty($params)) {
            $this->_access[$permissionName] = $access;
        }

        return $access;
    }

    /**
     * @return CrmDbManager|null
     */
    protected function getAccessChecker()
    {
        return Yii::$app->controller->module->authManager ?? null;
    }

    /**
     * @return string
     */
    public function getRbacRoleName()
    {
        switch ($this->role) {
            case self::ROLE_OWNER :
                return 'owner';
                break;
            case self::ROLE_ADMIN :
                return 'admin';
                break;
            case self::ROLE_MODERATOR :
                return 'moderator';
                break;
            case self::ROLE_CONTENT_MANAGER :
                return 'editor';
                break;
            case self::ROLE_MEMBER :
                return 'viewer';
                break;
            default :
                return null;
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (empty($this->crm_id)) {
            /* @var User $user*/
            $user = Yii::$app->user->identity;
            $this->crm_id = $user->crmMember->crm_id;
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert || array_key_exists('role', $changedAttributes)) {
            $rbacRoleName = $this->getRbacRoleName();
            if ($rbacRoleName !== null) {
                /** @var CrmDbManager $authManager */
                $authManager = Yii::$app->controller->module->authManager;
                $rbacRole = $authManager->getRole($rbacRoleName);
                if ($rbacRole !== null) {
                    $authManager->revokeAll($this->id);
                    $authManager->assign($rbacRole, $this->id);
                }
            }
        }
    }
}
