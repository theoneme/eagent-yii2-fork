<?php

namespace frontend\modules\crm\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "crm_instance".
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property integer $status
 *
 * @property CrmMember[] $crmMembers
 *
 * @mixin LinkableBehavior
 * @mixin ImageBehavior
 */
class CrmInstance extends ActiveRecord
{
    public const STATUS_DELETED = -50;
    public const STATUS_PAUSED = 0;
    public const STATUS_ACTIVE = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_instance';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'logo' => [
                'class' => ImageBehavior::class,
                'folder' => 'crm',
                'imageField' => 'logo'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'logo'], 'string', 'max' => 255],
            [['status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_PAUSED, self::STATUS_ACTIVE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('crm', 'ID'),
            'name' => Yii::t('crm', 'Title'),
            'status' => Yii::t('crm', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmMembers()
    {
        return $this->hasMany(CrmMember::class, ['crm_id' => 'id'])->indexBy('user_id');
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            (new CrmMember(['role' => CrmMember::ROLE_OWNER, 'crm_id' => $this->id, 'user_id' => Yii::$app->user->identity->getId()]))->save();
        }
    }
}
