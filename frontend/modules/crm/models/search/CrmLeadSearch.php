<?php

namespace frontend\modules\crm\models\search;

use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use frontend\modules\crm\models\CrmLead;

/**
 * CrmLeadSearch represents the model behind the search form of `frontend\modules\crm\models\CrmLead`.
 */
class CrmLeadSearch extends CrmLead
{
    public $ownerName;

    public $customerName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'opportunity_amount', 'status', 'type'], 'integer'],
            [['title', 'description', 'source'], 'safe'],
            [['ownerName', 'customerName'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'ownerName' => Yii::t('crm', 'Owner'),
            'customerName' => Yii::t('crm', 'Customer'),
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = CrmLead::find()->joinWith(['owner.user', 'customer'])->orderBy(['crm_lead.id' => SORT_DESC])->groupBy(['crm_lead.id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'crm_lead.id' => $this->id,
            'opportunity_amount' => $this->opportunity_amount,
            'crm_lead.status' => $this->status,
            'crm_lead.type' => $this->type,
            'crm_lead.owner_id' => $this->owner_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'currency_code', $this->currency_code]);

        /* @var User $user*/
        $user = Yii::$app->user->identity;
        if ($user->crmMember->can('editOthers')) {
            $query->andWhere(['crm_member.crm_id' => $user->crmMember->crm_id]);
        }
        else {
            $query->andWhere(['crm_lead.owner_id' => $user->crm_member_id]);
        }
        if (!empty($this->ownerName)) {
            $query->andWhere(['or',
                ['like', 'user.username', $this->ownerName],
                ['like', 'user.email', $this->ownerName],
                ['like', 'user.phone', $this->ownerName]
            ]);
        }
        if (!empty($this->customerName)) {
            $query->andWhere(['or',
                ['like', 'crm_customer.name', $this->customerName],
                ['like', 'crm_customer.email', $this->customerName],
                ['like', 'crm_customer.phone', $this->customerName]
            ]);
        }
        return $dataProvider;
    }
}
