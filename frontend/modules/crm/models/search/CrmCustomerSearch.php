<?php

namespace frontend\modules\crm\models\search;

use common\models\User;
use frontend\modules\crm\models\CrmCustomer;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class CrmCustomerSearch
 * @package frontend\modules\crm\models\search
 */
class CrmCustomerSearch extends CrmCustomer
{
    /**
     * @var string
     */
    public $ownerName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'type'], 'integer'],
            [['name', 'email', 'phone', 'info'], 'safe'],
//            [['lat', 'lng'], 'number'],
            [['ownerName'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'ownerName' => Yii::t('crm', 'Owner'),
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = CrmCustomer::find()->joinWith(['owner.user'])->orderBy(['crm_customer.id' => SORT_DESC])->groupBy(['crm_customer.id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'crm_customer.id' => $this->id,
//            'crm_customer.lat' => $this->lat,
//            'crm_customer.lng' => $this->lng,
            'crm_customer.status' => $this->status,
            'crm_customer.type' => $this->type,
        ]);

        /* @var User $user */
        $user = Yii::$app->user->identity;
        if ($user->crmMember->can('editOthers')) {
            $query->andWhere(['crm_member.crm_id' => $user->crmMember->crm_id]);
        } else {
            $query->andWhere(['crm_customer.owner_id' => $user->crm_member_id]);
        }

        $query->andFilterWhere(['like', 'crm_customer.name', $this->name])
            ->andFilterWhere(['like', 'crm_customer.email', $this->email])
            ->andFilterWhere(['like', 'crm_customer.phone', $this->phone])
            ->andFilterWhere(['like', 'crm_customer.info', $this->info]);

        if (!empty($this->ownerName)) {
            $query->andWhere(['or',
                ['like', 'user.username', $this->ownerName],
                ['like', 'user.email', $this->ownerName],
                ['like', 'user.phone', $this->ownerName]
            ]);
        }
        return $dataProvider;
    }
}
