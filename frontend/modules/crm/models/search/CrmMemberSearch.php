<?php

namespace frontend\modules\crm\models\search;

use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use frontend\modules\crm\models\CrmMember;

/**
 * CrmMemberSearch represents the model behind the search form of `frontend\modules\crm\models\CrmMember`.
 */
class CrmMemberSearch extends CrmMember
{
    public $username;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'role', 'status'], 'integer'],
            [['username'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'username' => Yii::t('crm', 'User'),
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        /* @var User $user*/
        $user = Yii::$app->user->identity;
        $this->crm_id = $user->crmMember->crm_id;

        $query = CrmMember::find()->joinWith(['user'])->orderBy(['crm_member.id' => SORT_DESC])->groupBy(['crm_member.id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'crm_member.id' => $this->id,
            'crm_id' => $this->crm_id,
            'role' => $this->role,
            'crm_member.status' => $this->status,
        ]);
        if (!empty($this->username)) {
            $query->andWhere(['or',
                ['like', 'user.username', $this->username],
                ['like', 'user.email', $this->username],
                ['like', 'user.phone', $this->username]
            ]);
        }

        return $dataProvider;
    }
}
