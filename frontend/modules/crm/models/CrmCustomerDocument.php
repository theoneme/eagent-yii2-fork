<?php

namespace frontend\modules\crm\models;

use common\behaviors\AttachmentBehavior;
use common\models\Attachment;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "crm_customer_document".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $type
 * @property int $issue_date
 * @property int $expiration_date
 * @property string $serial_number
 * @property string $description
 *
 * @property CrmCustomer $customer
 * @property Attachment[] $attachments
 *
 * @mixin AttachmentBehavior
 */
class CrmCustomerDocument extends ActiveRecord
{
    public const TYPE_PASSPORT = 0;
    public const TYPE_BIRTH_CERTIFICATE = 10;
    public const TYPE_MARRIAGE_CERTIFICATE = 20;
    public const TYPE_DIVORCE_CERTIFICATE = 30;
    public const TYPE_DRIVER_LICENSE = 40;
    public const TYPE_OTHER = 100;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_customer_document';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'attachment' => [
                'class' => AttachmentBehavior::class,
                'folder' => 'crm-customer-document',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['customer_id', 'type'], 'integer'],
            [['serial_number'], 'string', 'max' => 150],
            [['description'], 'string'],
            ['type', 'in', 'range' => [
                self::TYPE_PASSPORT,
                self::TYPE_BIRTH_CERTIFICATE,
                self::TYPE_MARRIAGE_CERTIFICATE,
                self::TYPE_DIVORCE_CERTIFICATE,
                self::TYPE_DRIVER_LICENSE,
                self::TYPE_OTHER,
            ]],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CrmCustomer::class, 'targetAttribute' => ['customer_id' => 'id']],
            [['issue_date', 'expiration_date'], 'date', 'format' => 'dd-mm-yyyy'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('crm', 'ID'),
            'customer_id' => Yii::t('crm', 'Customer'),
            'type' => Yii::t('crm', 'Type'),
            'serial_number' => Yii::t('crm', 'Serial Number'),
            'issue_date' => Yii::t('crm', 'Issue Date'),
            'expiration_date' => Yii::t('crm', 'Expiration Date'),
            'description' => Yii::t('crm', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CrmCustomer::class, ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['entity_id' => 'id'])->andOnCondition(['attachment.entity' => Attachment::ENTITY_CRM_CUSTOMER_DOCUMENT]);
    }

    /**
     *
     */
    public function afterFind()
    {
        if (!empty($this->issue_date)) {
            $this->issue_date = Yii::$app->formatter->asDate($this->issue_date, 'dd-MM-yyyy');
        }
        if (!empty($this->expiration_date)) {
            $this->expiration_date = Yii::$app->formatter->asDate($this->expiration_date, 'dd-MM-yyyy');
        }
        parent::afterFind();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!empty($this->issue_date)) {
            $this->issue_date = strtotime($this->issue_date);
        }
        if (!empty($this->expiration_date)) {
            $this->expiration_date = strtotime($this->expiration_date);
        }
        return parent::beforeSave($insert);
    }
}
