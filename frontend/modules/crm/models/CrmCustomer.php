<?php

namespace frontend\modules\crm\models;

use common\behaviors\ImageBehavior;
use common\behaviors\LinkableBehavior;
use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "crm_customer".
 *
 * @property int $id
 * @property int $owner_id
 * @property string $name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property string $phone
 * @property string $info
 * @property string $lat
 * @property string $lng
 * @property string $photo
 * @property string $country_resident
 * @property string $locale
 * @property string $communication_language
 * @property int $status
 * @property int $type
 * @property int $created_at
 * @property int $updated_at
 * @property int $family_status
 * @property int $birth_date
 *
 * @property CrmMember $owner
 * @property CrmLead[] $crmLeads
 * @property CrmCustomerContact[] $contacts
 * @property CrmCustomerDocument[] $documents
 * @property CrmCustomerAddress[] $addresses
 *
 * @mixin LinkableBehavior
 */
class CrmCustomer extends ActiveRecord
{
    public const STATUS_DELETED = -50;
    public const STATUS_PAUSED = 0;
    public const STATUS_ACTIVE = 10;

    public const TYPE_INDIVIDUAL = 0;
    public const TYPE_BUSINESS = 10;

    public const FAMILY_STATUS_SINGLE = 10;
    public const FAMILY_STATUS_MARRIED = 20;
    public const FAMILY_STATUS_DIVORCED = 30;
    public const FAMILY_STATUS_CIVIL = 40;
    public const FAMILY_STATUS_WIDOW = 50;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_customer';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'photo' => [
                'class' => ImageBehavior::class,
                'folder' => 'crm-customer',
                'imageField' => 'photo'
            ],
            'linkable' => [
                'class' => LinkableBehavior::class,
                'relations' => ['contacts', 'documents', 'addresses'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id'], 'required'],
            [['owner_id', 'status', 'type'], 'integer'],
            [['info'], 'string'],
//            [['lat', 'lng'], 'number'],
            [['name', 'last_name', 'middle_name', 'country_resident', 'email', 'phone', 'communication_language'], 'string', 'max' => 255],
            ['photo', 'string', 'max' => 155],
            [['locale'], 'string', 'max' => 5],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => CrmMember::class, 'targetAttribute' => ['owner_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_PAUSED, self::STATUS_ACTIVE]],
            ['family_status', 'in', 'range' => [self::FAMILY_STATUS_SINGLE, self::FAMILY_STATUS_MARRIED, self::FAMILY_STATUS_DIVORCED, self::FAMILY_STATUS_CIVIL, self::FAMILY_STATUS_WIDOW]],
            ['birth_date', 'date', 'format' => 'dd-mm-yyyy'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('crm', 'ID'),
            'owner_id' => Yii::t('crm', 'Owner'),
            'name' => Yii::t('crm', 'Name'),
            'email' => Yii::t('crm', 'Email'),
            'phone' => Yii::t('crm', 'Phone'),
            'info' => Yii::t('crm', 'Info'),
            'lat' => Yii::t('crm', 'Lat'),
            'lng' => Yii::t('crm', 'Lng'),
            'status' => Yii::t('crm', 'Status'),
            'type' => Yii::t('crm', 'Type'),
            'created_at' => Yii::t('crm', 'Created At'),
            'updated_at' => Yii::t('crm', 'Updated At'),
            'last_name' => Yii::t('crm', 'Last name'),
            'middle_name' => Yii::t('crm', 'Middle name'),
            'birth_date' => Yii::t('crm', 'Birth date'),
            'photo' => Yii::t('crm', 'Photo'),
            'family_status' => Yii::t('crm', 'Family status'),
            'country_resident' => Yii::t('crm', 'Country resident'),
            'locale' => Yii::t('crm', 'Site Language'),
            'communication_language' => Yii::t('crm', 'Communication Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(CrmMember::class, ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmLeads()
    {
        return $this->hasMany(CrmLead::class, ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(CrmCustomerContact::class, ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(CrmCustomerDocument::class, ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(CrmCustomerAddress::class, ['customer_id' => 'id']);
    }

    /**
     *
     */
    public function afterFind()
    {
        if (!empty($this->birth_date)) {
            $this->birth_date = Yii::$app->formatter->asDate($this->birth_date, 'dd-MM-yyyy');
        }
        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (empty($this->owner_id)) {
            /* @var User $user*/
            $user = Yii::$app->user->identity;
            $this->owner_id = $user->crm_member_id;
        }
        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!empty($this->birth_date)) {
            $this->birth_date = strtotime($this->birth_date);
        }
        return parent::beforeSave($insert);
    }

}
