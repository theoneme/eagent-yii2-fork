<?php

namespace frontend\modules\crm\models;

use common\mappers\AddressTranslationsMapper;
use common\models\AddressTranslation;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "crm_customer_address".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $lat
 * @property int $lng
 * @property string $type
 *
 * @property CrmCustomer $customer
 * @property AddressTranslation[] $addressTranslations
 */
class CrmCustomerAddress extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_customer_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['type'], 'string', 'max' => 150],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CrmCustomer::class, 'targetAttribute' => ['customer_id' => 'id']],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('crm', 'ID'),
            'customer_id' => Yii::t('crm', 'Customer'),
            'type' => Yii::t('crm', 'Type'),
            'value' => Yii::t('crm', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CrmCustomer::class, ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressTranslations()
    {
        return $this->hasMany(AddressTranslation::class, ['lat' => 'lat', 'lng' => 'lng'])
            ->from('address_translation crm_customer_address_translations')
            ->indexBy('locale');
    }
    public function getAddressText(){
        $addressTranslation = AddressTranslationsMapper::getMappedData($this->addressTranslations);
        return $addressTranslation['title'] ?? '';
    }
}
