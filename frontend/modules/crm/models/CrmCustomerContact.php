<?php

namespace frontend\modules\crm\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "crm_customer_contact".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $type
 * @property string $value
 *
 * @property CrmCustomer $customer
 */
class CrmCustomerContact extends ActiveRecord
{
    public const TYPE_EMAIL = 0;
    public const TYPE_PHONE = 10;
    public const TYPE_WEBSITE = 30;

    public const TYPE_SKYPE = 100;
    public const TYPE_WHATSAPP = 110;
    public const TYPE_VIBER = 120;
    public const TYPE_TELEGRAM = 130;

    public const TYPE_FACEBOOK = 200;
    public const TYPE_VK = 210;
    public const TYPE_TWITTER = 220;
    public const TYPE_GOOGLE = 230;
    public const TYPE_YOUTUBE = 240;
    public const TYPE_OK = 250;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_customer_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'value'], 'required'],
            [['customer_id', 'type'], 'integer'],
            [['value'], 'string', 'max' => 150],
            ['type', 'in', 'range' => [
                self::TYPE_EMAIL,
                self::TYPE_PHONE,
                self::TYPE_WEBSITE,
                self::TYPE_SKYPE,
                self::TYPE_WHATSAPP,
                self::TYPE_VIBER,
                self::TYPE_TELEGRAM,
                self::TYPE_FACEBOOK,
                self::TYPE_VK,
                self::TYPE_TWITTER,
                self::TYPE_GOOGLE,
                self::TYPE_YOUTUBE,
                self::TYPE_OK,
            ]],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CrmCustomer::class, 'targetAttribute' => ['customer_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('crm', 'ID'),
            'customer_id' => Yii::t('crm', 'Customer'),
            'type' => Yii::t('crm', 'Type'),
            'value' => Yii::t('crm', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CrmCustomer::class, ['id' => 'customer_id']);
    }
}
