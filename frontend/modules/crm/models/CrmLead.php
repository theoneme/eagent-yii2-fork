<?php

namespace frontend\modules\crm\models;

use common\models\Currency;
use common\models\Property;
use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "crm_lead".
 *
 * @property int $id
 * @property int $owner_id
 * @property int $customer_id
 * @property int $property_id
 * @property string $title
 * @property string $description
 * @property string $source
 * @property int $opportunity_amount
 * @property string $currency_code
 * @property int $status
 * @property int $type
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Currency $currencyCode
 * @property CrmCustomer $customer
 * @property CrmMember $owner
 */
class CrmLead extends ActiveRecord
{
    public const STATUS_DELETED = -50;
    public const STATUS_PAUSED = 0;
    public const STATUS_ACTIVE = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_lead';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['owner_id', 'customer_id'], 'required'],
            [['owner_id', 'customer_id', 'property_id', 'opportunity_amount', 'status', 'type', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['currency_code'], 'required'],
            [['title', 'source'], 'string', 'max' => 255],
            [['currency_code'], 'string', 'max' => 3],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CrmCustomer::class, 'targetAttribute' => ['customer_id' => 'id']],
            [['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => CrmMember::class, 'targetAttribute' => ['owner_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::class, 'targetAttribute' => ['property_id' => 'id']],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_PAUSED, self::STATUS_ACTIVE]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('crm', 'ID'),
            'owner_id' => Yii::t('crm', 'Owner'),
            'customer_id' => Yii::t('crm', 'Customer'),
            'property_id' => Yii::t('crm', 'Property object'),
            'title' => Yii::t('crm', 'Title'),
            'description' => Yii::t('crm', 'Description'),
            'source' => Yii::t('crm', 'Source'),
            'opportunity_amount' => Yii::t('crm', 'Opportunity Amount'),
            'currency_code' => Yii::t('crm', 'Currency'),
            'status' => Yii::t('crm', 'Status'),
            'type' => Yii::t('crm', 'Type'),
            'created_at' => Yii::t('crm', 'Created At'),
            'updated_at' => Yii::t('crm', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencyCode()
    {
        return $this->hasOne(Currency::class, ['code' => 'currency_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CrmCustomer::class, ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(CrmMember::class, ['id' => 'owner_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if (empty($this->owner_id)) {
            /* @var User $user*/
            $user = Yii::$app->user->identity;
            $this->owner_id = $user->crm_member_id;
        }
        return parent::beforeValidate();
    }
}
