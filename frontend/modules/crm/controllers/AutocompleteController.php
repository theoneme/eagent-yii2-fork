<?php

namespace frontend\modules\crm\controllers;

use common\mappers\TranslationsMapper;
use common\models\Property;
use common\models\Translation;
use common\models\User;
use frontend\modules\crm\models\CrmCustomer;
use frontend\modules\crm\models\CrmMember;
use Yii;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class AutocompleteController
 * @package frontend\modules\crm\controllers
 */
class AutocompleteController extends CrmBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['customers', 'members', 'users', 'properties'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actionMembers()
    {
        $query = urldecode(Yii::$app->request->post('query', ''));
        $condition = ['or',
            ['crm_member.id' => $query],
            ['like', 'user.username', $query],
        ];
        if (filter_var($query, FILTER_VALIDATE_EMAIL)) {
            $condition[] = ['like', 'user.email', $query];
        } else if (strlen($query) >= 8 && strlen($query) <= 15) {
            $condition[] = ['like', 'user.phone', $query];
        }
        $items = CrmMember::find()
            ->joinWith(['user'])
            ->where(['crm_id' => $this->currentMember->crm_id])
            ->andWhere($condition)
            ->groupBy('crm_member.id')
            ->limit(10)
            ->all();

        $result = array_map(function ($value) {
            return [
                'id' => $value->id,
                'value' => "{$value->user->username} (ID: {$value->id}; Email: {$value->user->email})",
                'phone' => $value->user->phone,
            ];
        }, $items);

        return $result;
    }

    /**
     * @return array
     */
    public function actionCustomers()
    {
        $crmCondition = $this->currentMember->can('editOthers')
            ? ['crm_member.crm_id' => $this->currentMember->crm_id]
            : ['crm_customer.owner_id' => $this->currentMember->id];

        $query = urldecode(Yii::$app->request->post('query', ''));
        $searchCondition = ['or',
            ['crm_customer.id' => $query],
            ['like', 'crm_customer.name', $query],
        ];
        if (filter_var($query, FILTER_VALIDATE_EMAIL)) {
            $searchCondition[] = ['like', 'crm_customer.email', $query];
        } else if (strlen($query) >= 8 && strlen($query) <= 15) {
            $searchCondition[] = ['like', 'crm_customer.phone', $query];
        }
        $items = CrmCustomer::find()
            ->joinWith(['owner'])
            ->where($crmCondition)
            ->andWhere($searchCondition)
            ->groupBy('crm_customer.id')
            ->limit(10)
            ->all();

        $result = array_map(function ($value) {
            return [
                'id' => $value->id,
                'value' => "{$value->name} (ID: {$value->id}; Email: {$value->email})",
                'phone' => $value->phone,
            ];
        }, $items);

        return $result;
    }

    /**
     * @return array
     */
    public function actionUsers()
    {
        $query = urldecode(Yii::$app->request->post('query', ''));
        $condition = ['or',
            ['user.id' => $query],
            ['like', 'user.username', $query],
        ];
        if (filter_var($query, FILTER_VALIDATE_EMAIL)) {
            $condition[] = ['like', 'user.email', $query];
        } else if (strlen($query) >= 8 && strlen($query) <= 15) {
            $condition[] = ['like', 'user.phone', $query];
        }
        $items = User::find()->where(['status' => User::STATUS_ACTIVE, 'is_company' => false])->andWhere($condition)->groupBy('user.id')->limit(10)->all();

        $result = array_map(function ($value) {
            return [
                'id' => $value->id,
                'value' => "{$value->username} (ID: {$value->id}; Email: {$value->email})",
                'phone' => $value->phone,
                'img' => !empty($value->avatar) ? $value->avatar : '/images/agent-no-image.png'
            ];
        }, $items);

        return $result;
    }

    /**
     * @return array
     */
    public function actionProperties()
    {
        $query = urldecode(Yii::$app->request->post('query', ''));
        $condition = ['or',
            ['property.id' => $query],
//            ['like', 'user.username', $query],
        ];
//        if (filter_var($query, FILTER_VALIDATE_EMAIL)){
//            $condition[] = ['like', 'user.email', $query];
//        }
//        else if (strlen($query) >= 8 && strlen($query) <= 15) {
//            $condition[] = ['like', 'user.phone', $query];
//        }
        $items = Property::find()->andWhere($condition)->groupBy('property.id')->limit(10)->all();

        $result = array_map(function ($value) {
            /* @var Property $value */
            $currentTranslation = TranslationsMapper::getMappedData($value->translations);
            return [
                'id' => $value->id,
                'value' => "{$currentTranslation[Translation::KEY_TITLE]} (ID: {$value->id};)",
//                'phone' => $value->phone,
//                'img' => !empty($value->avatar) ? $value->avatar : '/images/agent-no-image.png'
            ];
        }, $items);

        return $result;
    }
}
