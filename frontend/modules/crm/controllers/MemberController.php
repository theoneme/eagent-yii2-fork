<?php

namespace frontend\modules\crm\controllers;

use common\models\User;
use frontend\modules\crm\models\CrmMember;
use frontend\modules\crm\models\search\CrmMemberSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class MemberController
 * @package frontend\modules\crm\controllers
 */
class MemberController extends CrmBaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'leave' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all CrmMember models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CrmMemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currentMember' => $this->currentMember,
        ]);
    }

    /**
     * Creates a new CrmMember model.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CrmMember();
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $this->validateAjax($model);
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }
        $users = User::find()->where(['status' => User::STATUS_ACTIVE, 'is_company' => false])->orderBy(['user.id' => SORT_DESC])->groupBy('user.id')->limit(10)->all();
        return $this->render('create', [
            'model' => $model,
            'users' => $users
        ]);
    }

    /**
     * Updates an existing CrmMember model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var CrmMember $model */
        $model = $this->findModel($id, CrmMember::class);
        if (!(in_array($model->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $this->currentMember->can('manage')) && !(in_array($model->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $this->currentMember->can('manageAdmins'))) {
            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
            return $this->redirect(['index']);
        }
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $this->validateAjax($model);
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }
        $users = User::find()->where(['status' => User::STATUS_ACTIVE, 'is_company' => false])->orderBy(['user.id' => SORT_DESC])->groupBy('user.id')->limit(10)->all();

        return $this->render('update', [
            'model' => $model,
            'users' => $users
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        /** @var CrmMember $model */
        $model = $this->findModel($id, CrmMember::class);
        if (!(in_array($model->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $this->currentMember->can('manage')) && !(in_array($model->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $this->currentMember->can('manageAdmins'))) {
            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
        } else {
            $model->updateAttributes(['status' => CrmMember::STATUS_DELETED]);
        }

        return $this->redirect(['index']);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionLeave($id)
    {
        $model = $this->findModel($id, CrmMember::class, '/crm/site/index');
        if ($model->user_id === Yii::$app->user->identity->getId()) {
            $model->updateAttributes(['status' => CrmMember::STATUS_DELETED]);
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
        }

        return $this->redirect(['/crm/site/index']);
    }
}
