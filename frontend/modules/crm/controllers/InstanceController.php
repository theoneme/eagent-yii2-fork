<?php

namespace frontend\modules\crm\controllers;

use frontend\modules\crm\models\CrmInstance;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Class InstanceController
 * @package frontend\modules\crm\controllers
 */
class InstanceController extends CrmBaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Creates a new CrmCustomer model.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CrmInstance();
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $this->validateAjax($model);
            if ($model->save()) {
                //
                return $this->redirect(['/crm/site/settings']);
            }
        }

        return $this->redirect(['/crm/site/index']);
    }


    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, CrmInstance::class, '/crm/site/index');
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $this->validateAjax($model);
            if ($model->save()) {
                //
                return $this->redirect(['/crm/site/settings']);
            }
        }

        return $this->redirect(['/crm/site/index']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        /** @var CrmInstance $model */
        $model = $this->findModel($id, CrmInstance::class, '/crm/site/index');
        if (isset($model->crmMembers[Yii::$app->user->identity->getId()]) && $model->crmMembers[Yii::$app->user->identity->getId()]->can('manageCRM')) {
            $model->updateAttributes(['status' => CrmInstance::STATUS_DELETED]);
        } else {
            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
        }

        return $this->redirect(['/crm/site/index']);
    }
}
