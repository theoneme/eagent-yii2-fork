<?php

namespace frontend\modules\crm\controllers;

use common\models\User;
use frontend\modules\crm\models\CrmMember;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\ErrorAction;
use yii\web\JqueryAsset;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class CrmBaseController
 * @package frontend\modules\crm\controllers
 */
class CrmBaseController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'crm';
    /**
     * @var null|CrmMember $currentMember
     */
    public $currentMember;

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    // Не пускаем гостей
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                    // В эти экшены пускаем без выбора CRM
                    [
                        'allow' => true,
                        'controllers' => ['crm/site'],
                        'actions' => ['index', 'login'],
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['crm/instance'],
                        'actions' => ['create', 'update', 'delete'],
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['crm/member'],
                        'actions' => ['leave'],
                    ],
                    // Для остальных проверяем, что у юзера выбрана CRM
                    [
                        'allow' => false,
                        'matchCallback' => function ($rule, $action) {
                            /* @var User $user */
                            $user = Yii::$app->user->identity;
                            $result = empty($user->crm_member_id) || $user->crmMember === null;
                            if (!$result) {
                                $this->currentMember = $user->crmMember;
                            }
                            return $result;
                        },
                        'denyCallback' => function () {
                            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You must select or add new CRM to work with it'));
                            return $this->redirect(['/crm/site/index']);
                        },
                    ],
                    // Проверяем, что у него разрешен к ней доступ
                    [
                        'allow' => false,
                        'matchCallback' => function ($rule, $action) {
                            return !$this->currentMember->can('view', ['member' => $this->currentMember]);
                        },
                        'denyCallback' => function () {
                            Yii::$app->session->setFlash('warning', Yii::t('crm', 'This CRM does not exist or you no longer have access to it'));
                            return $this->redirect(['/crm/site/index']);
                        },
                    ],
                    // Проверяем права на создание сущностей
                    [
                        'allow' => false,
                        'controllers' => ['crm/lead', 'crm/customer'],
                        'actions' => ['create'],
                        'matchCallback' => function ($rule, $action) {
                            return !$this->currentMember->can('create');
                        },
                        'denyCallback' => function () {
                            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
                            return $this->redirect(['/crm/site/dashboard']);
                        },
                    ],
                    // Проверяем права на управление участниками
                    [
                        'allow' => false,
                        'controllers' => ['crm/member'],
                        'actions' => ['create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return !$this->currentMember->can('manage');
                        },
                        'denyCallback' => function () {
                            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
                            return $this->redirect(['/crm/site/dashboard']);
                        },
                    ],
                    // Проверяем права на управление CRM
                    [
                        'allow' => false,
                        'controllers' => ['crm/site'],
                        'actions' => ['settings'],
                        'matchCallback' => function ($rule, $action) {
                            return !$this->currentMember->can('manage');
                        },
                        'denyCallback' => function () {
                            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
                            return $this->redirect(['/crm/site/dashboard']);
                        },
                    ],
                    // Сюда могут добавлятся другие запрещающие правила, правда в дочерних контроллерах так и не понятно как это сделать
                    // Всё что не запрещено - разрешено
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }


    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            $this->disableAjaxAssets();
        } else {
            $result = $this->verifyLanguage();
            if ($result['redirect'] === true) {
                $this->redirect($result['url']);
                return false;
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * @return bool
     */
    protected function disableAjaxAssets()
    {
        $this->layout = false;

        Yii::$app->assetManager->bundles = [
            BootstrapPluginAsset::class => false,
            BootstrapAsset::class => false,
            JqueryAsset::class => false,
        ];

        return true;
    }

    /**
     * Performs ajax validation.
     * @param Model $model
     */
    protected function validateAjax(Model $model)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data = ActiveForm::validate($model);
            Yii::$app->response->send();
            Yii::$app->end();
        }
    }

    /**
     * Performs ajax validation.
     * @param Model $model
     */
    protected function validateWithRelationsAjax(Model $model)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data = $model->validateWithRelations();
            Yii::$app->response->send();
            Yii::$app->end();
        }
    }

    /**
     * @param $id
     * @param $modelClass
     * @param null $redirectUrl
     * @return mixed
     */
    protected function findModel($id, $modelClass, $redirectUrl = null)
    {
        if (($model = $modelClass::findOne($id)) !== null) {
            return $model;
        }
        Yii::$app->session->setFlash('error', Yii::t('crm', 'The requested page does not exist'));
        Yii::$app->response->redirect([$redirectUrl ?? "/crm/{$this->id}/index"]);
        Yii::$app->end();

        return null;
    }


    /**
     * @return array
     * @throws InvalidConfigException
     */
    protected function verifyLanguage()
    {
        $languageParam = Yii::$app->request->get('app_language');

        if (!Yii::$app->request->isAjax && !Yii::$app->request->isPost && in_array($languageParam, Yii::$app->params['supportedLocales'])) {
            $locale = array_search($languageParam, Yii::$app->params['supportedLocales'], true);
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'language',
                'value' => $locale,
                'expire' => time() + 60 * 60 * 24 * 30,
                'domain' => '.' . Yii::$app->params['domainName'],
            ]));
            Yii::$app->language = $locale;

            $requestUrl = explode('?', Yii::$app->request->getUrl())[0];
            $currentUrl = explode('?', Url::current())[0];
            $currentUrl = preg_replace("#^\/\/[\w-]+\." . preg_quote(Yii::$app->params['domainName']) . '(.*)#', '$1', $currentUrl);
            $currentUrl = (strpos($currentUrl, '/') === 0 ? '' : '/') . $currentUrl;
            if ($requestUrl !== $currentUrl && Yii::$app->request->cookies['language'] !== null) {
                return [
                    'redirect' => true,
                    'url' => Url::current()
                ];
            }
        }

        return [
            'redirect' => false
        ];
    }
}