<?php

namespace frontend\modules\crm\controllers;

use common\models\User;
use frontend\modules\crm\models\CrmInstance;
use frontend\modules\crm\models\CrmMember;
use Yii;

/**
 * Class SiteController
 * @package frontend\modules\crm\controllers
 */
class SiteController extends CrmBaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'crm-login';
        $crmMembers = CrmMember::find()->joinWith(['crm'])->where([
            'crm_member.user_id' => Yii::$app->user->identity->getId(),
            'crm_instance.status' => CrmInstance::STATUS_ACTIVE,
            'crm_member.status' => CrmMember::STATUS_ACTIVE,
        ])->all();
        return $this->render('index', ['crmMembers' => $crmMembers]);
    }

    /**
     * @return string
     */
    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

    /**
     * CrmInstance settings.
     * @return mixed
     */
    public function actionSettings()
    {
        $model = $this->currentMember->crm;
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
//            if (!empty($data['CrmMember'])) {
//                $model->attachBehavior('linkable', ['class' => LinkableBehavior::class, 'relations' => ['crmMembers']]);
//                foreach ($data['CrmMember'] as $memberData) {
//                    $member = $model->bind('crmMembers', $member['id'] ?? null);
//                    $member->attributes = $memberData;
//                }
//                $member = $model->bind('crmMembers', $this->currentMember->id, $this->currentMember->user_id);
//                $member->attributes = $this->currentMember->attributes;
//            }
            $this->validateAjax($model);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('crm', 'The CRM settings has been successfully updated'));
                //
            }
        }

        return $this->render('settings', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionLogin($id)
    {
        $member = CrmMember::findOne($id);
        if ($member !== null && $member->user_id === Yii::$app->user->identity->getId() && $member->can('view', ['member' => $member])) {
            /* @var User $user */
            $user = Yii::$app->user->identity;
            $user->updateAttributes(['crm_member_id' => $member->id]);
            return $this->redirect(['/crm/site/dashboard']);
        }
        Yii::$app->session->setFlash('warning', Yii::t('crm', 'This CRM does not exist or you no longer have access to it'));
        return $this->redirect(['/crm/site/index']);
    }

    /**
     * @return string
     */
    public function actionLogout()
    {
        /* @var User $user */
        $user = Yii::$app->user->identity;
        if (!empty($user->crm_member_id)) {
            $user->updateAttributes(['crm_member_id' => null]);
        }
        return $this->redirect(['/crm/site/index']);
    }
}
