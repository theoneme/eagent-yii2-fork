<?php

namespace frontend\modules\crm\controllers;

use frontend\modules\crm\models\CrmLead;
use frontend\modules\crm\models\CrmMember;
use frontend\modules\crm\models\search\CrmLeadSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class LeadController
 * @package frontend\modules\crm\controllers
 */
class LeadController extends CrmBaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all CrmLead models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CrmLeadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currentMember' => $this->currentMember
        ]);
    }

    /**
     * Creates a new CrmLead model.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CrmLead();
        $can = [
            'editOwn' => true,
            'editOther' => $this->currentMember->can('editOthers'),
        ];
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $this->validateAjax($model);
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'can' => $can
        ]);
    }

    /**
     * Updates an existing CrmLead model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        /** @var CrmLead $model */
        $model = $this->findModel($id, CrmLead::class);
        $can = [
            'editOwn' => $this->currentMember->can('edit', ['member_id' => $model->owner_id]),
            'editOther' => (in_array($model->owner->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $this->currentMember->can('editOthers')) || (in_array($model->owner->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $this->currentMember->can('manageAdmins')),
        ];
        if (!$can['editOwn'] && !$can['editOther']) {
            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
            return $this->redirect(['index']);
        }
        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            $this->validateAjax($model);
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'can' => $can
        ]);
    }

    /**
     * Deletes an existing CrmLead model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        /** @var CrmLead $model */
        $model = $this->findModel($id, CrmLead::class);
        $canEdit = $this->currentMember->can('edit', ['member_id' => $model->owner_id]) || (in_array($model->owner->role, [CrmMember::ROLE_MEMBER, CrmMember::ROLE_CONTENT_MANAGER]) && $this->currentMember->can('editOthers')) || (in_array($model->owner->role, [CrmMember::ROLE_MODERATOR, CrmMember::ROLE_ADMIN]) && $this->currentMember->can('manageAdmins'));
        if (!$canEdit) {
            Yii::$app->session->setFlash('warning', Yii::t('crm', 'You are not allowed to perform this action'));
        } else {
            $model->updateAttributes(['status' => CrmLead::STATUS_DELETED]);
        }

        return $this->redirect(['index']);
    }
}
