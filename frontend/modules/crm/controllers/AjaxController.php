<?php

namespace frontend\modules\crm\controllers;

use common\controllers\FrontEndController;
use frontend\modules\crm\models\CrmCustomerAddress;
use frontend\modules\crm\models\CrmCustomerContact;
use frontend\modules\crm\models\CrmCustomerDocument;
use Yii;
use yii\base\Module;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class AjaxController
 * @package frontend\modules\crm\controllers
 */
class AjaxController extends FrontEndController
{
    /**
     * AjaxController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct(string $id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-contact', 'render-document', 'render-address'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['render-contact', 'render-document', 'render-address'],
            ]
        ]);
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderContact($iterator)
    {
        return [
            'html' => $this->renderAjax('@frontend/modules/crm/views/customer/contact-partial', [
                'model' => new CrmCustomerContact(),
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderDocument($iterator)
    {
        return [
            'html' => $this->renderAjax('@frontend/modules/crm/views/customer/document-partial', [
                'model' => new CrmCustomerDocument(),
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderAddress($iterator)
    {
        return [
            'html' => $this->renderAjax('@frontend/modules/crm/views/customer/address-partial', [
                'model' => new CrmCustomerAddress(),
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }
}
