<?php

namespace frontend\modules\crm\components;

use Yii;
use yii\db\Query;
use yii\rbac\Assignment;
use yii\rbac\DbManager;
use yii\rbac\Item;
use yii\rbac\Role;

/**
 * Class CrmDbManager
 * @package frontend\modules\crm\components
 */
class CrmDbManager extends DbManager
{
    /**
     * @var string
     */
    public $assignmentTable = '{{%crm_auth_assignment}}';
    /**
     * @var array
     */
    private $_checkAccessAssignments = [];

    /**
     * {@inheritdoc}
     */
    public function checkAccess($memberId, $permissionName, $params = [])
    {
        if (isset($this->_checkAccessAssignments[(string)$memberId])) {
            $assignments = $this->_checkAccessAssignments[(string)$memberId];
        } else {
            $assignments = $this->getAssignments($memberId);
            $this->_checkAccessAssignments[(string)$memberId] = $assignments;
        }

        if ($this->hasNoAssignments($assignments)) {
            return false;
        }

        $this->loadFromCache();
        if ($this->items !== null) {
            return $this->checkAccessFromCache($memberId, $permissionName, $params, $assignments);
        }

        return $this->checkAccessRecursive($memberId, $permissionName, $params, $assignments);
    }

    /**
     * @param int|string $memberId
     * @param string $itemName
     * @param array $params
     * @param \yii\rbac\Assignment[] $assignments
     * @return bool
     */
    protected function checkAccessFromCache($memberId, $itemName, $params, $assignments)
    {
        if (!isset($this->items[$itemName])) {
            return false;
        }

        $item = $this->items[$itemName];

        Yii::debug($item instanceof Role ? "Checking role: $itemName" : "Checking permission: $itemName", __METHOD__);

        if (!$this->executeRule($memberId, $item, $params)) {
            return false;
        }

        if (isset($assignments[$itemName]) || in_array($itemName, $this->defaultRoles)) {
            return true;
        }

        if (!empty($this->parents[$itemName])) {
            foreach ($this->parents[$itemName] as $parent) {
                if ($this->checkAccessFromCache($memberId, $parent, $params, $assignments)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param int|string $memberId
     * @param string $itemName
     * @param array $params
     * @param \yii\rbac\Assignment[] $assignments
     * @return bool
     */
    protected function checkAccessRecursive($memberId, $itemName, $params, $assignments)
    {
        if (($item = $this->getItem($itemName)) === null) {
            return false;
        }

        Yii::debug($item instanceof Role ? "Checking role: $itemName" : "Checking permission: $itemName", __METHOD__);

        if (!$this->executeRule($memberId, $item, $params)) {
            return false;
        }

        if (isset($assignments[$itemName]) || in_array($itemName, $this->defaultRoles)) {
            return true;
        }

        $query = new Query();
        $parents = $query->select(['parent'])
            ->from($this->itemChildTable)
            ->where(['child' => $itemName])
            ->column($this->db);
        foreach ($parents as $parent) {
            if ($this->checkAccessRecursive($memberId, $parent, $params, $assignments)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $memberId
     * @return array|Role[]
     */
    public function getRolesByMember($memberId)
    {
        if ($this->isEmptyMemberId($memberId)) {
            return [];
        }

        $query = (new Query())->select('b.*')
            ->from(['a' => $this->assignmentTable, 'b' => $this->itemTable])
            ->where('{{a}}.[[item_name]]={{b}}.[[name]]')
            ->andWhere(['a.member_id' => (string)$memberId])
            ->andWhere(['b.type' => Item::TYPE_ROLE]);

        $roles = $this->getDefaultRoleInstances();
        foreach ($query->all($this->db) as $row) {
            $roles[$row['name']] = $this->populateItem($row);
        }

        return $roles;
    }

    /**
     * {@inheritdoc}
     */
    public function getPermissionsByMember($memberId)
    {
        if ($this->isEmptyMemberId($memberId)) {
            return [];
        }

        $directPermission = $this->getDirectPermissionsByMember($memberId);
        $inheritedPermission = $this->getInheritedPermissionsByMember($memberId);

        return array_merge($directPermission, $inheritedPermission);
    }

    /**
     * @param $memberId
     * @return array
     */
    protected function getDirectPermissionsByMember($memberId)
    {
        $query = (new Query())->select('b.*')
            ->from(['a' => $this->assignmentTable, 'b' => $this->itemTable])
            ->where('{{a}}.[[item_name]]={{b}}.[[name]]')
            ->andWhere(['a.member_id' => (string)$memberId])
            ->andWhere(['b.type' => Item::TYPE_PERMISSION]);

        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }

        return $permissions;
    }

    /**
     * @param $memberId
     * @return array
     */
    protected function getInheritedPermissionsByMember($memberId)
    {
        $query = (new Query())->select('item_name')
            ->from($this->assignmentTable)
            ->where(['member_id' => (string)$memberId]);

        $childrenList = $this->getChildrenList();
        $result = [];
        foreach ($query->column($this->db) as $roleName) {
            $this->getChildrenRecursive($roleName, $childrenList, $result);
        }

        if (empty($result)) {
            return [];
        }

        $query = (new Query())->from($this->itemTable)->where([
            'type' => Item::TYPE_PERMISSION,
            'name' => array_keys($result),
        ]);
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['name']] = $this->populateItem($row);
        }

        return $permissions;
    }

    /**
     * {@inheritdoc}
     */
    public function getAssignment($roleName, $memberId)
    {
        if ($this->isEmptyMemberId($memberId)) {
            return null;
        }

        $row = (new Query())->from($this->assignmentTable)
            ->where(['member_id' => (string)$memberId, 'item_name' => $roleName])
            ->one($this->db);

        if ($row === false) {
            return null;
        }

        return new Assignment([
            'userId' => $row['member_id'],
            'roleName' => $row['item_name'],
            'createdAt' => $row['created_at'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getAssignments($memberId)
    {
        if ($this->isEmptyMemberId($memberId)) {
            return [];
        }

        $query = (new Query())
            ->from($this->assignmentTable)
            ->where(['member_id' => (string)$memberId]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['item_name']] = new Assignment([
                'userId' => $row['member_id'],
                'roleName' => $row['item_name'],
                'createdAt' => $row['created_at'],
            ]);
        }

        return $assignments;
    }

    /**
     * {@inheritdoc}
     */
    public function assign($role, $memberId)
    {
        $assignment = new Assignment([
            'userId' => $memberId,
            'roleName' => $role->name,
            'createdAt' => time(),
        ]);

        $this->db->createCommand()
            ->insert($this->assignmentTable, [
                'member_id' => $assignment->userId,
                'item_name' => $assignment->roleName,
                'created_at' => $assignment->createdAt,
            ])->execute();

        unset($this->_checkAccessAssignments[(string)$memberId]);
        return $assignment;
    }

    /**
     * {@inheritdoc}
     */
    public function revoke($role, $memberId)
    {
        if ($this->isEmptyMemberId($memberId)) {
            return false;
        }

        unset($this->_checkAccessAssignments[(string)$memberId]);
        return $this->db->createCommand()
                ->delete($this->assignmentTable, ['member_id' => (string)$memberId, 'item_name' => $role->name])
                ->execute() > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function revokeAll($memberId)
    {
        if ($this->isEmptyMemberId($memberId)) {
            return false;
        }

        unset($this->_checkAccessAssignments[(string)$memberId]);
        return $this->db->createCommand()
                ->delete($this->assignmentTable, ['member_id' => (string)$memberId])
                ->execute() > 0;
    }

    /**
     * @param $roleName
     * @return array
     */
    public function getMemberIdsByRole($roleName)
    {
        if (empty($roleName)) {
            return [];
        }

        return (new Query())->select('[[member_id]]')
            ->from($this->assignmentTable)
            ->where(['item_name' => $roleName])->column($this->db);
    }

    /**
     * @param $memberId
     * @return bool
     */
    private function isEmptyMemberId($memberId)
    {
        return !isset($memberId) || $memberId === '';
    }
}
