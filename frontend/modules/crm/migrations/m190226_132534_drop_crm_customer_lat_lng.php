<?php

use frontend\modules\crm\models\CrmCustomer;
use frontend\modules\crm\models\CrmCustomerAddress;
use yii\db\Migration;

/**
 * Class m190226_132534_drop_crm_customer_lat_lng
 */
class m190226_132534_drop_crm_customer_lat_lng extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /* @var CrmCustomer[] $customers*/
        $customers = CrmCustomer::find()->where(['not',
            ['or',
                ['lat' => null],
                ['lat' => ''],
                ['lng' => null],
                ['lng' => ''],
            ]
        ])->all();
        foreach ($customers as $customer) {
            (new CrmCustomerAddress(['lat' => $customer->lat, 'lng' => $customer->lng, 'customer_id' => $customer->id]))->save();
        }
        $this->dropColumn('crm_customer', 'lat');
        $this->dropColumn('crm_customer', 'lng');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('crm_customer', 'lat', $this->decimal(10, 8)->null());
        $this->addColumn('crm_customer', 'lng', $this->decimal(11, 8)->null());
    }
}
