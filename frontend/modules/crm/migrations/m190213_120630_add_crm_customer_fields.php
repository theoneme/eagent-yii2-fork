<?php

use yii\db\Migration;

/**
 * Class m190213_120630_add_crm_customer_fields
 */
class m190213_120630_add_crm_customer_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crm_customer', 'last_name', $this->string());
        $this->addColumn('crm_customer', 'middle_name', $this->string());
        $this->addColumn('crm_customer', 'photo', $this->string(155));
        $this->addColumn('crm_customer', 'family_status', $this->integer());
        $this->addColumn('crm_customer', 'birth_date', $this->integer());
        $this->addColumn('crm_customer', 'country_resident', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crm_customer', 'last_name');
        $this->dropColumn('crm_customer', 'middle_name');
        $this->dropColumn('crm_customer', 'photo');
        $this->dropColumn('crm_customer', 'family_status');
        $this->dropColumn('crm_customer', 'birth_date');
        $this->dropColumn('crm_customer', 'country_resident');
    }
}
