<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crm_customer`.
 */
class m181030_125941_create_crm_customer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('crm_customer', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer(),
            'name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'info' => $this->text(),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
            'status' => $this->integer(),
            'type' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_crm_customer_owner_id', 'crm_customer', 'owner_id', 'crm_member', 'id', 'CASCADE');
        $this->createIndex('crm_customer_type_index', 'crm_customer', 'type');
        $this->createIndex('crm_customer_status_index', 'crm_customer', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('crm_customer');
    }
}
