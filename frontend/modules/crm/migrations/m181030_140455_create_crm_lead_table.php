<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crm_lead`.
 */
class m181030_140455_create_crm_lead_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('crm_lead', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer(),
            'customer_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'source' => $this->string(),
            'opportunity_amount' => $this->integer(),
            'currency_code' => $this->string(3)->notNull(),
            'status' => $this->integer(),
            'type' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk_crm_lead_owner_id', 'crm_lead', 'owner_id', 'crm_member', 'id', 'CASCADE');
        $this->addForeignKey('fk_crm_lead_customer_id', 'crm_lead', 'customer_id', 'crm_customer', 'id', 'CASCADE');
        $this->addForeignKey('fk_crm_lead_currency_code', 'crm_lead', 'currency_code', 'currency', 'code', 'CASCADE');
        $this->createIndex('crm_lead_type_index', 'crm_lead', 'type');
        $this->createIndex('crm_lead_status_index', 'crm_lead', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('crm_lead');
    }
}
