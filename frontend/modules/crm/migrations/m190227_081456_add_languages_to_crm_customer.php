<?php

use yii\db\Migration;

/**
 * Class m190227_081456_add_languages_to_crm_customer
 */
class m190227_081456_add_languages_to_crm_customer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crm_customer', 'locale', $this->string(5)->null());
        $this->addColumn('crm_customer', 'communication_language', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crm_customer', 'locale');
        $this->dropColumn('crm_customer', 'communication_language');
    }
}
