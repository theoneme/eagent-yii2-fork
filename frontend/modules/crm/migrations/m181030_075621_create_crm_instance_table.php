<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crm_instance`.
 */
class m181030_075621_create_crm_instance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('crm_instance', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'status' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('crm_instance');
    }
}
