<?php

use yii\db\Migration;

/**
 * Class m181213_113005_add_logo_to_crm_instance
 */
class m181213_113005_add_logo_to_crm_instance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crm_instance', 'logo', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crm_instance', 'logo');
    }
}
