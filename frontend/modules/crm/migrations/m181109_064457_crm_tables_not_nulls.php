<?php

use yii\db\Migration;

/**
 * Class m181109_064457_crm_tables_not_nulls
 */
class m181109_064457_crm_tables_not_nulls extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();

        $this->alterColumn('crm_member', 'user_id', $this->integer()->notNull());
        $this->alterColumn('crm_member', 'crm_id', $this->integer()->notNull());
        $this->alterColumn('crm_customer', 'owner_id', $this->integer()->notNull());
        $this->alterColumn('crm_lead', 'owner_id', $this->integer()->notNull());
        $this->alterColumn('crm_lead', 'customer_id', $this->integer()->notNull());

        Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();

        $this->alterColumn('crm_member', 'user_id', $this->integer());
        $this->alterColumn('crm_member', 'crm_id', $this->integer());
        $this->alterColumn('crm_customer', 'owner_id', $this->integer());
        $this->alterColumn('crm_lead', 'owner_id', $this->integer());
        $this->alterColumn('crm_lead', 'customer_id', $this->integer());

        Yii::$app->db->createCommand("SET foreign_key_checks = 0;")->execute();
    }
}
