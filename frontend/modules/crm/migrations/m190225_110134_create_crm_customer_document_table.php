<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_customer_document}}`.
 */
class m190225_110134_create_crm_customer_document_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_customer_document}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
        ]);
        $this->addForeignKey('fk_crm_customer_document_customer_id', 'crm_customer_document', 'customer_id', 'crm_customer', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_customer_document}}');
    }
}
