<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crm_member`.
 */
class m181030_075942_create_crm_member_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('crm_member', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'crm_id' => $this->integer(),
            'role' => $this->integer(),
            'status' => $this->integer(),
        ]);

        $this->addForeignKey('fk_crm_member_user_id', 'crm_member', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk_crm_member_crm_id', 'crm_member', 'crm_id', 'crm_instance', 'id', 'CASCADE');
        $this->createIndex('crm_member_role_index', 'crm_member', 'role');
        $this->createIndex('crm_member_status_index', 'crm_member', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('crm_member');
    }
}
