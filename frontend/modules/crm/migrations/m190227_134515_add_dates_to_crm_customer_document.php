<?php

use yii\db\Migration;

/**
 * Class m190227_134515_add_dates_to_crm_customer_document
 */
class m190227_134515_add_dates_to_crm_customer_document extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crm_customer_document', 'issue_date', $this->integer()->null());
        $this->addColumn('crm_customer_document', 'expiration_date', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crm_customer_document', 'issue_date');
        $this->dropColumn('crm_customer_document', 'expiration_date');
    }
}
