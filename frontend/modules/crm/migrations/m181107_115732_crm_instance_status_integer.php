<?php

use yii\db\Migration;

/**
 * Class m181107_115732_crm_instance_status_integer
 */
class m181107_115732_crm_instance_status_integer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('crm_instance', 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('crm_instance', 'status', $this->string());
    }
}
