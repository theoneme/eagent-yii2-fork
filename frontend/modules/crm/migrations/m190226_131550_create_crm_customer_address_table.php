<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_customer_address}}`.
 */
class m190226_131550_create_crm_customer_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_customer_address}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'type' => $this->string(150),
            'lat' => $this->decimal(10, 8)->null(),
            'lng' => $this->decimal(11, 8)->null(),
        ]);
        $this->addForeignKey('fk_crm_customer_address_customer_id', 'crm_customer_address', 'customer_id', 'crm_customer', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_customer_address}}');
    }
}
