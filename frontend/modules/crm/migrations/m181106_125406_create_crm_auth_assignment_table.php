<?php

use yii\db\Migration;

/**
 * Handles the creation of table `crm_auth_assignment`.
 */
class m181106_125406_create_crm_auth_assignment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('crm_auth_assignment', [
            'item_name' => $this->string(64)->notNull(),
            'member_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('', 'crm_auth_assignment', ['item_name', 'member_id']);
        $this->addForeignKey('fk_crm_aa_item_name', 'crm_auth_assignment', 'item_name', 'auth_item', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_crm_aa_member_id', 'crm_auth_assignment', 'member_id', 'crm_member', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('crm_auth_assignment');
    }
}
