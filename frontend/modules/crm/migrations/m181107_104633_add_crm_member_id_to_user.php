<?php

use yii\db\Migration;

/**
 * Class m181107_104633_add_crm_member_id_to_user
 */
class m181107_104633_add_crm_member_id_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'crm_member_id', $this->integer());
        $this->addForeignKey('fk_user_crm_member_id', 'user', 'crm_member_id', 'crm_member', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_crm_member_id', 'user');
        $this->dropColumn('user', 'crm_member_id');
    }
}
