<?php

use yii\db\Migration;

/**
 * Class m181214_135356_add_property_id_to_lead
 */
class m181214_135356_add_property_id_to_lead extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crm_lead', 'property_id', $this->integer());
        $this->addForeignKey('fk_crm_lead_property_id', 'crm_lead', 'property_id', 'property', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_crm_lead_property_id', 'crm_lead');
        $this->dropColumn('crm_lead', 'property_id');
    }
}
