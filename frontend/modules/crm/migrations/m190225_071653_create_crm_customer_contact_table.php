<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_customer_contact}}`.
 */
class m190225_071653_create_crm_customer_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_customer_contact}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'value' => $this->string(150)->notNull(),
        ]);
        $this->addForeignKey('fk_crm_customer_contact_customer_id', 'crm_customer_contact', 'customer_id', 'crm_customer', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_customer_contact}}');
    }
}
