<?php

use yii\db\Migration;

/**
 * Class m190227_132946_add_serial_number_to_crm_customer_document
 */
class m190227_132946_add_serial_number_to_crm_customer_document extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('crm_customer_document', 'serial_number', $this->string(150));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('crm_customer_document', 'serial_number');
    }
}
