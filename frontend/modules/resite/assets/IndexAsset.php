<?php

namespace frontend\modules\resite\assets;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class IndexAsset
 * @package frontend\modules\resite\assets
 */
class IndexAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/resite/web';
    public $css = [
        'css/site/resite.css'
    ];
    public $js = [
        'js/popper.min.js'
    ];
    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
        BootstrapPluginAsset::class,
    ];
}