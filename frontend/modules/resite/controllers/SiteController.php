<?php

namespace frontend\modules\resite\controllers;

use common\controllers\FrontEndController;
use Yii;
use yii\web\Response;

/**
 * Class SiteController
 * @package frontend\modules\resite\controllers
 */
class SiteController extends FrontEndController
{
    /**
     * @return mixed|string|Response
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $this->layout = 'main';

        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'sourcePath' => '@frontend/modules/instance/web',
            'css' => [
                'css/vendor/bootstrap.min.css',
            ]
        ];
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'sourcePath' => '@frontend/modules/instance/web',
            'js' => [
                'js/vendor/bootstrap.min.js',
            ]
        ];

        return $this->render('index');
    }
}