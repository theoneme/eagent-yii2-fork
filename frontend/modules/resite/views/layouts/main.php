<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var string $content
 */

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon"
          href=""
          type="image/png"/>
    <link rel="shortcut icon"
          href=""
          type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="main-block">
    <header>
        <div class="wrapper">
            <nav class="navbar navbar-expand-md">
                <a class="d-block text-right d-md-none w-100" href="#" data-toggle="collapse" data-target="#top-menu">
                    <div class="d-inline-block gamb-button rounded-circle">
                        <div class="bars">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                    </div>
                </a>
                <div class="d-md-flex justify-content-around collapse navbar-collapse" id="top-menu">
                    <ul class="no-list top-menu d-block mx-auto">
                        <li>
                            <a href="#we-offer">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                Наши преимущества
                            </a>
                        </li>
                        <li>
                            <a href="#about">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                О нас
                            </a>
                        </li>
                        <li>
                            <a href="#portfolio">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                Портфолио
                            </a>
                        </li>
                        <li>
                            <a href="#contact">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                Контакты
                            </a>
                        </li>
                        <li>
                            <a href="#tariff">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                Тарифы
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <?= $content ?>
    <div class="m-footer"></div>
</div>

<footer>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

