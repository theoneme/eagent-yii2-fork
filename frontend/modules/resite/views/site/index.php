<?php

use frontend\modules\resite\assets\IndexAsset;
use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\plugins\FontAwesomeAsset::register($this);
\frontend\assets\plugins\SlickAsset::register($this);
IndexAsset::register($this);

$bundle = $this->getAssetManager()->getBundle(IndexAsset::class);

?>

<section class="wrapper mx-auto d-block">
    <div class="slider px-3 px-sm-5">
        <div class="main-slide with-highlight">
            <div class="row align-items-center">

                <div class="col-12 col-lg-6 text-center text-lg-left pb-5 py-sm-5">
                    <h1 class="font-weight-bold">Новые заявки на <br/> покупку <span class="highlight overflow-hidden"
                                                                                     data-text="недвижимости"><span>недвижимости</span></span>
                    </h1>
                    <p class="text-medium mb-3 mb-lg-5">Комплексные решения по привлечению <br/>
                        новых клиентов для агентств недвижимости
                    </p>
                    <a href="#" class="d-inline-block button to-next mb-3 mr-3">
                        <span>Подробнее</span>
                        <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                        <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                    </a>
                    <a href="#contact" class="d-inline-block button orange">
                        <span>Оставить заявку</span>
                        <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                        <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                    </a>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="img text-center">
                        <?= Html::img("{$bundle->baseUrl}/images/slide3.png", [
                            'alt' => 'Новые заявки на покупку недвижимости',
                            'title' => 'Новые заявки на покупку недвижимости',
                            'class' => 'd-inline-block'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-slide with-highlight">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-left pb-5 py-sm-5">
                    <h2 class="h1-title font-weight-bold">Учет новых клиентов <br/> за счет внедрения <span
                                class="highlight overflow-hidden" data-text="CRM"><span>CRM</span></span></h2>
                    <p class="text-medium mb-3 mb-lg-5">Внедрение CRM настройка учета<br/>
                        клиентов. Автоматические рассылки
                    </p>
                    <a href="#" class="d-inline-block button to-next mb-3 mr-3">
                        <span>Подробнее</span>
                        <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                        <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                    </a>
                    <a href="#contact" class="d-inline-block button orange">
                        <span>Оставить заявку</span>
                        <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                        <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                    </a>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="img text-center">
                        <?= Html::img("{$bundle->baseUrl}/images/slide1.png", [
                            'alt' => 'Учет новых клиентов за счет внедрения CRM',
                            'title' => 'Учет новых клиентов за счет внедрения CRM',
                            'class' => 'd-inline-block'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-slide with-highlight">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-left pb-5 py-sm-5">
                    <h2 class="h1-title font-weight-bold">Увеличение количества <br/> клиентов и сделок в <span
                                class="highlight overflow-hidden" data-text="3 раза"><span>3 раза</span></span></h2>
                    <p class="text-medium mb-3 mb-lg-5">Мы гарантируем увеличение новых сделок<br/>
                        в 3 раза при прежних затратах на рекламу.
                    </p>
                    <a href="#we-offer"" class="d-inline-block button mb-3 mr-3">
                    <span>Подробнее</span>
                    <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                        <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                    </svg>
                    <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                        <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                    </svg>
                    </a>
                    <a href="#contact" class="d-inline-block button orange">
                        <span>Оставить заявку</span>
                        <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                        <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                            <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                        </svg>
                    </a>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="img text-center">
                        <?= Html::img("{$bundle->baseUrl}/images/slide2.png", [
                            'alt' => 'Увеличение количества клиентов и сделок в 3 раза',
                            'title' => 'Увеличение количества клиентов и сделок в 3 раза',
                            'class' => 'd-inline-block'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="we-offer with-highlight pb-5 wrapper mx-auto d-block" id="we-offer">
    <div>
        <svg class="w-100 bg-light-blue" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1680 72">
            <polygon fill="#fff" points="0,0 1680,72 1680,0"></polygon>
        </svg>
    </div>
    <div class="bg-light-blue">
        <div class="container-fluid">
            <h2 class="pt-5 pb-3 text-center font-weight-bold">Что мы <span class="highlight overflow-hidden"
                                                                            data-text="предлагаем"><span>предлагаем</span></span>
            </h2>
            <p class="text-center wrapper-small d-block mx-auto text-medium">
                Взять на себя решение всех вопросов по привлечению новых клиентов.
                Вы сможете больше заниматься вашей профессиональной деятельностью.
            </p>
            <div class="row py-5">
                <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                    <div class="img mb-4">
                        <?= Html::img("{$bundle->baseUrl}/images/offer1.png", [
                            'alt' => 'Создание сайта',
                            'title' => 'Создание сайта',
                            'class' => 'd-block mx-auto h-auto'
                        ]) ?>
                    </div>
                    <div class="title position-relative pb-2">
                        <h5 class="font-weight-bold">Создание сайта, лендинговых страниц их наполнение</h5>
                        <p>
                            Мы создадим удобные для пользователей сайты на которых они смогут оставить вам заявки.
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                    <div class="img mb-4">
                        <?= Html::img("{$bundle->baseUrl}/images/offer2.png", [
                            'alt' => 'Социальные сети',
                            'title' => 'Социальные сети',
                            'class' => 'd-block mx-auto h-auto'
                        ]) ?>
                    </div>
                    <div class="title position-relative pb-2">
                        <h5 class="font-weight-bold">Создание, наполнение и ведение страниц в социальных сетях</h5>
                        <p>Социальные сети помогут поднять имидж вашей компании и привлечь новых клиентов.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                    <div class="img mb-4">
                        <?= Html::img("{$bundle->baseUrl}/images/offer3.png", [
                            'alt' => 'Продвижение компании',
                            'title' => 'Продвижение компании',
                            'class' => 'd-block mx-auto h-auto'
                        ]) ?>
                    </div>
                    <div class="title position-relative pb-2">
                        <h5 class="font-weight-bold">Продвижение компании в Интернете и социальных сетях</h5>
                        <p>Настроим и будем вести рекламные кампании, которые эффективны для привлечения новых
                            покупателей на недвижимость </p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-3 offer-item text-center px-4 pb-5">
                    <div class="img mb-4">
                        <?= Html::img("{$bundle->baseUrl}/images/offer4.png", [
                            'alt' => 'Внедрение CRM',
                            'title' => 'Внедрение CRM',
                            'class' => 'd-block mx-auto h-auto'
                        ]) ?>
                    </div>
                    <div class="title position-relative pb-2">
                        <h5 class="font-weight-bold">
                            Внедрение CRM. Автоматизация работы с клиентами
                        </h5>
                        <p>
                            Внедрение CRM и других систем автоматизации поможет увеличить эффективность работы с
                            клиентами
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="about" class="with-highlight py-5 wrapper mx-auto d-block">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6">
                <?= Html::img("{$bundle->baseUrl}/images/slide4.png", [
                    'class' => 'd-block mx-auto mw-100'
                ]) ?>
            </div>
            <div class="col-12 col-lg-6">
                <h3 class="font-weight-bold">Мы возьмем на себя задачи
                    по привлечению <span class="highlight overflow-hidden" data-text="новых клиентов"><span>новых клиентов</span></span>
                </h3>

                <p class="mb-5">
                    В нашей компании работают специалисты различных профессий: программисты, специалисты по рекламе и
                    социальным сетям, копирайтеры,
                    Мы предлагаем решение всех вопросов по привлечению новых клиентов на покупку и продажу недвижимости.
                    Наша поддержка поможет вам сконцентрироваться на вашем бизнесе и больше уделить времени на работу с
                    клиентом, а все остальные задачи решим мы.
                </p>

                <a href="#" class="button orange">
                    <span>Оставить заявку</span>
                    <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                        <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                    </svg>
                    <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                        <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                    </svg>
                </a>

            </div>
        </div>
    </div>
</section>
<section id="portfolio" class="py-5 wrapper mx-auto d-block with-highlight">
    <div class="container-fluid">
        <h2 class="text-center font-weight-bold">
            Наше <span class="highlight overflow-hidden" data-text="портфолио"><span>портфолио</span></span>
        </h2>
        <p class="text-center text-medium pb-5">
            Praesent hendrerit, mi facilisis eleifend lobortis mi est hendrerit fringillaibus lorem condimentum
            fringilla dui enim et ante.
        </p>
        <div class="row">
            <div class="col-12 col-lg-6 pr-lg-1 pb-2 pb-lg-0">
                <div class="w-100 square position-relative square-big overflow-hidden rounded">
                    <div class="w-100 h-100 rounded"
                         style="background: url(<?= Url::to(["{$bundle->baseUrl}/images/portfolio4.png"]) ?>); background-size: cover;"></div>
                    <div class="square-over position-absolute bg-light-blue">
                        <div class="square-description p-5">
                            <h5 class="font-weight-bold">Lorem ipsum</h5>
                            <p class="text-tiny color-black">
                                Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 pl-lg-1">
                <div class="row">
                    <div class="col-12 col-sm-6 pr-sm-1">
                        <div class="pb-1">
                            <div class="w-100 square position-relative square-small overflow-hidden rounded">
                                <div class="w-100 h-100 rounded"
                                     style="background: url(<?= Url::to(["{$bundle->baseUrl}/images/portfolio3.png"]) ?>); background-size: cover;"></div>
                                <div class="square-over position-absolute bg-orange">
                                    <div class="square-description text-white p-5">
                                        <h5 class="font-weight-bold">Lorem ipsum</h5>
                                        <p class="text-tiny text-white">
                                            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pt-1">
                            <div class="w-100 square position-relative square-small overflow-hidden rounded">
                                <div class="w-100 h-100"
                                     style="background: url(<?= Url::to(["{$bundle->baseUrl}/images/portfolio1.png"]) ?>); background-size: cover;"></div>
                                <div class="square-over position-absolute bg-blue">
                                    <div class="square-description text-white p-5">
                                        <h5 class="font-weight-bold">Lorem ipsum</h5>
                                        <p class="text-tiny text-white">
                                            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 pl-sm-1">
                        <div class="pt-2 pt-sm-0">
                            <div class="w-100 square position-relative square-long overflow-hidden rounded">
                                <div class="w-100 h-100 rounded"
                                     style="background: url(<?= Url::to(["{$bundle->baseUrl}/images/portfolio2.png"]) ?>); background-size: cover;"></div>
                                <div class="square-over position-absolute bg-light-blue">
                                    <div class="square-description p-5">
                                        <h5 class="font-weight-bold">Lorem ipsum</h5>
                                        <p class="text-tiny color-black">
                                            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="with-highlight py-5 wrapper mx-auto d-block" id="contact">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6 pb-5 pb-lg-0">
                <h2 class="py-5 font-weight-bold">Наши <span class="highlight overflow-hidden"
                                                             data-text="контакты"><span>контакты</span></span></h2>
                <p class="text-medium">Адрес: <span class="color-black">г. Екатеринбургб, ул. ???, дом ??</span></p>
                <p class="text-medium">Телефон: <a href="tel:">+7 (000) 000-00-00</a></p>
            </div>
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <input type="text" class="form-control" name="" placeholder="Ваше имя">
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" name="" placeholder="Ваше телефон">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="" placeholder="Ваше email">
                </div>
                <div class="form-group pb-3">
                    <textarea class="form-control" placeholder="Ваше сообщение"></textarea>
                </div>
                <button class="d-inline-block button orange">
                    <span>Оставить заявку</span>
                    <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                        <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                    </svg>
                    <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                        <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                    </svg>
                </button>

            </div>
        </div>
    </div>
</section>

<section id="tariff" class="py-5 wrapper mx-auto d-block">
    <div>
        <svg class="w-100 bg-light-blue" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1680 72">
            <polygon fill="#fff" points="0,0 1680,72 1680,0"></polygon>
        </svg>
    </div>
    <div class="bg-light-blue py-5 pb-3">
        <div class="container-fluid px-xl-5">
            <div class="row">
                <div class="col-12 col-sm-6 col-lg-4 py-5 text-center text-sm-left">
                    <div class="w-100 d-flex flex-wrap flex-sm-nowrap">
                        <div class="mx-auto mr-sm-4 d-block">
                            <div class="tariff-price font-weight-bold d-flex"><span>12</span> <span
                                        class="currency">$</span></div>
                            <div class="text-tiny color-grey letter-spacing-02 pl-0 pl-sm-2">Monthly</div>
                        </div>
                        <div class="w-100">
                            <div class="tariff-title font-weight-bold mt-2 mb-4">Personal</div>
                            <p class="text-medium">Design solutions for any media. It’s a piece of cake.</p>
                            <ul class="no-list check color-grey letter-spacing-02 mb-5">
                                <li>Web & mobile</li>
                                <li>Free custom domain</li>
                                <li>Best hosting market</li>
                                <li>Outstanding support</li>
                            </ul>
                            <a href="#" class="button blue">
                                <span>Purchase</span>
                                <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 py-5 text-center text-sm-left">
                    <div class="w-100 d-flex flex-wrap flex-sm-nowrap">
                        <div class="mx-auto mr-sm-4 d-block">
                            <div class="tariff-price font-weight-bold d-flex"><span>31</span> <span
                                        class="currency">$</span></div>
                            <div class="text-tiny color-grey letter-spacing-02 pl-0 pl-sm-2">Monthly</div>
                        </div>
                        <div class="w-100">
                            <div class="tariff-title font-weight-bold mt-2 mb-4">Business</div>
                            <p class="text-medium">Design solutions for any media. It’s a piece of cake.</p>
                            <ul class="no-list check color-grey letter-spacing-02 mb-5">
                                <li>Web & mobile</li>
                                <li>Free custom domain</li>
                                <li>Best hosting market</li>
                                <li>Outstanding support</li>
                            </ul>
                            <a href="#" class="button blue">
                                <span>Purchase</span>
                                <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 py-5 text-center text-sm-left">
                    <div class="w-100 d-flex flex-wrap flex-sm-nowrap">
                        <div class="mx-auto mr-sm-4 d-block">
                            <div class="tariff-price font-weight-bold d-flex"><span>51</span> <span
                                        class="currency">$</span></div>
                            <div class="text-tiny color-grey letter-spacing-02 pl-0 pl-sm-2">Monthly</div>
                        </div>
                        <div class="w-100">
                            <div class="tariff-title font-weight-bold mt-2 mb-4">Enterprise</div>
                            <p class="text-medium">Design solutions for any media. It’s a piece of cake.</p>
                            <ul class="no-list check color-grey letter-spacing-02 mb-5">
                                <li>Web & mobile</li>
                                <li>Free custom domain</li>
                                <li>Best hosting market</li>
                                <li>Outstanding support</li>
                            </ul>
                            <a href="#" class="button orange">
                                <span>Purchase</span>
                                <svg class="arrow-prev" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                                <svg class="arrow-next" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                    <path d="M0,20 L40,20 M33,13 L39,20 L33,27"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<a href="#" id="go-top" class="go-top rounded-circle">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
        <path d="M20,40 L20,0 M13,7 L20,1 L26,7"></path>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
        <path d="M20,40 L20,0 M13,7 L20,1 L26,7"></path>
    </svg>
</a>

<?php $script = <<<JS
    $(".slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        centerMode: false,
        arrows: false,
        infinite: true
    });

    function appear(){
        $('.with-highlight:not(.appear)').each(function () {
            if ($(document).scrollTop() + $(window).height()*0.8 > $(this).offset().top) {
                $(this).addClass('appear');
            }
        });
    }

    function square() {
        $('.square-big').height($('.square-big').width());
        $('.square-small').height($('.square-small').width());
        $('.square-long').height($('.square-big').width());
    }

    $(document).ready(function () {
        appear();
        square();
    });

    $(window).scroll(function() {
       appear();
       let bTop = $('.go-top');
       if ($(document).scrollTop() > 200) {
           if (!bTop.hasClass('on')) {
               bTop.addClass('on');
           }
       } else {
           if (bTop.hasClass('on')) {
               bTop.removeClass('on');
           }
       }
    });

    $(window).resize(function () {
        square();
    });

    $(document.body).on('click', '#go-top', function() {
        $('html, body').animate({scrollTop: 0},1000);
        return false;
    });

    $('.slider').on('afterChange', function(){
        appear();
    });

    $('.slider').on('beforeChange', function(){
        $('.main-slide').removeClass('appear');
    });

    $(document.body).on('click' ,'.to-next', function () {
        $(".slider").slick('slickNext');
    });

    $('a[href*="#"]').click(function() {
        var el = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $(el).offset().top
        }, 1000);
        return false;
    });
JS;

$this->registerJs($script);
