<?php

namespace frontend\modules\resite;

use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package frontend\modules\instance
 */
class Module extends \yii\base\Module implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\resite\controllers';

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {

    }
}
