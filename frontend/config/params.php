<?php
return [
    'adminEmail' => 'admin@example.com',

    'priceChunks' => [
        [
            'range' => [
                'min' => 0,
                'max' => 50000
            ],
            'step' => 10000
        ],
        [
            'range' => [
                'min' => 50000,
                'max' => 100000
            ],
            'step' => 25000
        ],
        [
            'range' => [
                'min' => 100000,
                'max' => 200000
            ],
            'step' => 50000
        ],
        [
            'range' => [
                'min' => 200000,
                'max' => 500000
            ],
            'step' => 100000
        ],
        [
            'range' => [
                'min' => 500000,
                'max' => 1000000
            ],
            'step' => 150000
        ],
        [
            'range' => [
                'min' => 1000000,
                'max' => 10000000
            ],
            'step' => 250000
        ],
        [
            'range' => [
                'min' => 10000000,
                'max' => 100000000
            ],
            'step' => 1000000
        ]
    ],

    'propertyCatalogFilters' => [
        'property_area',
        'living_area',
        'kitchen_area',
        'balconies',
        'loggias',
        'condition',
        'bathrooms',
        'floor',
        'rooms',
        'bedrooms',
        'material',
        'floors',
        'year_built'
    ],

    'buildingCatalogFilters' => [
        'material',
        'floors',
        'building_type',
        'year_built',
        'building_phase',
        'building_state',
        'ready_year',
        'building_renovation_year'
    ]
];
