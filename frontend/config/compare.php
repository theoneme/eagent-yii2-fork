<?php
return [
    [
        'label' => Yii::t('main', 'Property Price'),
        'attribute' => 'price'
    ],
    [
        'label' => Yii::t('main', 'Property Name'),
        'attribute' => 'title'
    ],
    [
        'label' => Yii::t('main', 'Property Address'),
        'attribute' => 'address'
    ],
    [
        'label' => Yii::t('main', 'Property Category'),
        'attribute' => 'category'
    ],
    [
        'label' => Yii::t('main', 'Rooms'),
        'attribute' => 'rooms'
    ],
    [
        'label' => Yii::t('main', 'Bathrooms'),
        'attribute' => 'bathrooms'
    ],
    [
        'label' => Yii::t('main', 'Total Area'),
        'attribute' => 'property_area'
    ],
    [
        'label' => Yii::t('main', 'Living Area'),
        'attribute' => 'living_area'
    ],
    [
        'label' => Yii::t('main', 'Kitchen Area'),
        'attribute' => 'kitchen_area'
    ],
    [
        'label' => Yii::t('main', 'Floor'),
        'attribute' => 'floor'
    ],
    [
        'label' => Yii::t('main', 'Amenities'),
        'attribute' => 'amenities'
    ],
];