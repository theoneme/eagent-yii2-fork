<?php

use frontend\components\CompanyDbManager;
use frontend\components\CustomUrlManager;
use frontend\components\themes\ViewMode;
use frontend\modules\crm\components\CrmDbManager;
use frontend\widgets\IndexTabsWidget;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'crm',
        'instance',
        'estet',
        frontend\components\LanguageSelector::class,
    ],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru-RU',
    'name' => 'eAgent',
    'components' => [
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js',
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend'
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => ['auth/login'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
            'cookieParams' => [
                'path' => '/',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'except' => ['yii\web\HttpException:404', 'yii\i18n\PhpMessageSource::loadFallbackMessages'],
                ],
            ],
        ],
        'errorHandler' => [
//            'errorAction' => 'site/error',
            'errorView' => '@frontend/views/site/error.php',
        ],
        'urlManager' => [
//            'class' => CustomUrlManager::class,
            'ruleConfig' => [
                'class' => 'frontend\components\CustomUrlRule'
            ],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'sitemap.xml' => 'sitemap/default/index',
                [
                    'pattern' => 'sitemap-<action(list|part)>-<entity:[\w-]+>',
                    'route' => 'sitemap/default/<action>',
                    'suffix' => '.xml',
                ],

                'agency-landing' => 'resite/site/index',
                '<operation:(' . implode('|', IndexTabsWidget::getOperationList()) . ')>' => 'site/index',
                '/' => 'site/index',

                '<slug:(privacy-policy|terms-of-service)>' => 'page/view',
                '<slug:[\w-]+>-<action:(property)>' => 'property/<action>/show',
                '<slug:[\w-]+>-<action:(building)>' => 'building/<action>/show',
                '<category:[\w-]+>-for-<operation:(sale|rent)>-catalog' => 'property/catalog/index',
                '<operation:(secondary|new-construction)>-buildings-catalog' => 'building/catalog/index',

                'crm' => 'crm/site/index',
                'crm/<action:[\w-]+>' => 'crm/site/<action>',
//
//                '/<site_alias:[\w-]+>-site/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => 'instance/<controller>/<action>',
//                '/<site_alias:[\w-]+>-site/<controller:[\w-]+>/<action:[\w-]+>' => 'instance/<controller>/<action>',
//                '/<site_alias:[\w-]+>-site' => 'instance/site/index',

                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',

                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        'i18n' => [
            'translations' => [
                'main*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'main' => 'main.php',
                    ],
                ],
                'index*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'index' => 'index.php',
                    ],
                ],
                'property*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'property' => 'property.php',
                    ],
                ],
                'model*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'model' => 'model.php',
                    ],
                ],
                'catalog*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'catalog' => 'catalog.php',
                    ],
                ],
                'tariff*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'tariff' => 'tariff.php',
                    ],
                ],
                'agent*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'agent' => 'agent.php',
                    ],
                ],
                'labels*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'labels' => 'labels.php',
                    ],
                ],
                'building*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'notification' => 'building.php',
                    ],
                ],
                'wizard*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'wizard' => 'wizard.php',
                    ],
                ],
                'account*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'account' => 'account.php',
                    ],
                ],
                'landing*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'landing' => 'landing.php',
                    ],
                ],
            ],
        ],
        'view' => function (ViewMode $mode) {
            return Yii::createObject([
                'class' => \frontend\components\View::class,
                'enableMinify' => !YII_DEBUG,
                'concatCss' => true,
                'minifyCss' => true,
                'concatJs' => true,
                'minifyJs' => true,
                'webPath' => '@web',
                'basePath' => '@webroot',
                'minifyPath' => '@webroot/minify',
                'jsPosition' => [\yii\web\View::POS_END],
                'forceCharset' => 'UTF-8',
                'expandImports' => true,
                'compressOptions' => ['extra' => true],
                'isMobile' => $mode->isMobile(Yii::$app->request),
                'theme' => $mode->isMobile(Yii::$app->request) ? [
                    'pathMap' => [
                        '@frontend/views' => [
                            '@frontend/themes/mobile/views',
                            '@frontend/views',
                        ],
                        '@frontend/widgets' => [
                            '@frontend/themes/mobile/widgets',
                            '@frontend/widgets',
                        ]
                    ],
                ] : null,
            ]);
        },
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '211168917955-g3v64dqaff64ml86qogkh1nkg7jf0k8h.apps.googleusercontent.com',
                    'clientSecret' => 'dn0Lf_DdD3wRUwwdwu-VTOWq',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '202756367284434',
                    'clientSecret' => 'fbe45c0ab7cc1523bc50de3116287c40',
                    'attributeNames' => [
                        'name',
                        'id',
                        'picture',
                        'link',
                        'email'
                    ],
                ],
            ],
        ],
        'companyAuthManager' => [
            'class' => CompanyDbManager::class,
        ],
    ],
    'as viewMode' => [
        'class' => \frontend\components\themes\filters\ViewModeFilter::class,
        'expire' => 2592000 // 30 days
    ],
    'modules' => [
        'resite' => [
            'class' => 'frontend\modules\resite\Module',
        ],
        'estet' => [
            'class' => 'frontend\modules\estet\Module',
        ],
        'sitemap' => [
            'class' => 'frontend\modules\sitemap\Sitemap',
        ],
        'crm' => [
            'class' => 'frontend\modules\crm\Module',
            'components' => [
                'authManager' => [
                    'class' => CrmDbManager::class,
                ],
            ],
        ],
        'instance' => [
            'class' => 'frontend\modules\instance\Module',
        ],
    ],
    'params' => $params,
];
