<?php

namespace frontend\widgets;

use frontend\services\LocationService;
use Yii;
use yii\base\Widget;

/**
 * Class LocationWidget
 * @package frontend\widgets
 */
class LocationWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'location-widget';
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * LocationWidget constructor.
     * @param array $config
     * @param LocationService $locationService
     */
    public function __construct(array $config = [], LocationService $locationService)
    {
        parent::__construct($config);
        $this->_locationService = $locationService;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        return $this->render($this->template, [
            'locationData' => $locationData
        ]);
    }
}