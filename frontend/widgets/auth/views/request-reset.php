<?php

use frontend\models\auth\RequestResetForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

\frontend\assets\AuthAsset::register($this);

/**
 * @var RequestResetForm $requestResetForm
 */

?>

<div class="modal fade auth-modal new-modal" id="request-reset-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="auth-modal-title text-center">
                    <?= Yii::t('main', 'Reset Password') ?>
                </div>
                <div class="auth-modal-subtitle text-center">
                    <?= Yii::t('main', 'Enter the email you used in registration. A password reset link will be sent to your email') ?>
                </div>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'request-reset-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'action' => ['/auth/request-reset'],
                    'options' => [
                        'class' => 'formbot'
                    ],
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'errorOptions' => [
                            'tag' => 'label',
                            'class' => 'auth-modal-error text-left'
                        ]
                    ]
                ]); ?>
                <?= $form->field($requestResetForm, 'email', [
                    'inputOptions' => [
                        'placeholder' => Yii::t('main', 'Enter email address')
                    ]
                ]) ?>

                <div class="form-group text-center">
                    <?= Html::submitButton(Yii::t('main', 'Submit'), ['class' => 'btn btn-big width100 btn-green no-margin']) ?>
                </div>

                <?php ActiveForm::end(); ?>
                <div class="auth-modal-bottom text-center">
                    <a data-dismiss="modal" data-toggle="modal"
                       data-target="#login-modal"><?= Yii::t('main', 'Back to Sign In') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>