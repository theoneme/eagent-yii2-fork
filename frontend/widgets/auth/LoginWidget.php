<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\widgets\auth;

use common\interfaces\repositories\UserRepositoryInterface;
use common\models\auth\LoginForm;
use yii\base\Widget;

/**
 * Class LoginWidget
 * @package frontend\components
 */
class LoginWidget extends Widget
{
    /**
     * @var
     */
    private $_userRepository;
    /**
     * @var string
     */
    public $template = 'login';

    /**
     * LoginWidget constructor.
     * @param array $config
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository, array $config = [])
    {
        parent::__construct($config);
        $this->_userRepository = $userRepository;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $loginForm = new LoginForm($this->_userRepository);

        return $this->render($this->template, ['loginForm' => $loginForm]);
    }
}