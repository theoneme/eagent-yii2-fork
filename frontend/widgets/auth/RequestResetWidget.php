<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\widgets\auth;

use common\interfaces\repositories\UserRepositoryInterface;
use frontend\models\auth\RequestResetForm;
use yii\base\Widget;

/**
 * Class RequestResetWidget
 * @package frontend\widgets\auth
 */
class RequestResetWidget extends Widget
{
    /**
     * @var UserRepositoryInterface
     */
    private $_userRepository;

    /**
     * RequestResetWidget constructor.
     * @param array $config
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository, array $config = [])
    {
        parent::__construct($config);
        $this->_userRepository = $userRepository;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $requestResetForm = new RequestResetForm($this->_userRepository);

        return $this->render('request-reset', [
            'requestResetForm' => $requestResetForm
        ]);
    }
}