<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 02.08.2016
 * Time: 15:42
 */

namespace frontend\widgets\auth;

use common\services\entities\UserService;
use frontend\models\auth\SignupForm;
use yii\base\Widget;

/**
 * Class SignupWidget
 * @package frontend\components
 */
class SignupWidget extends Widget
{
    /**
     * @var UserService
     */
    private $_userService;
    /**
     * @var string
     */
    public $template = 'signup';

    /**
     * SignupWidget constructor.
     * @param array $config
     * @param UserService $userService
     */
    public function __construct(UserService $userService, array $config = [])
    {
        parent::__construct($config);
        $this->_userService = $userService;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $signupForm = new SignupForm($this->_userService);

        return $this->render($this->template, [
            'signupForm' => $signupForm,
        ]);
    }
}