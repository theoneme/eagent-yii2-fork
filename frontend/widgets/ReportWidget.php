<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 22:17
 */

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class ReportWidget
 * @package frontend\widgets
 */
class ReportWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('report-widget', [

        ]);
    }
}