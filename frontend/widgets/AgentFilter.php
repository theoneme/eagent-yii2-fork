<?php

namespace frontend\widgets;

use common\interfaces\FilterInterface;
use yii\base\Widget;

/**
 * Class AgentFilter
 * @package frontend\widgets
 */
class AgentFilter extends Widget
{
    /**
     * @var string
     */
    public $catalogView;
    /**
     * @var array
     */
    public $queryParams;
    /**
     * @var array
     */
    public $locationData;
    /**
     * @var array
     */
    public $filters;

    /**
     * PropertyFilter constructor.
     * @param FilterInterface $propertyFilterService
     * @param array $config
     */
    public function __construct(FilterInterface $propertyFilterService, array $config = [])
    {
        parent::__construct($config);

        $this->filters = [];
//        $this->filters = $propertyFilterService->buildFilterArray($this->queryParams);
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        $address = $this->locationData['address'];

        return $this->render('agent-filter', [
            'filters' => $this->filters,
//            'catalogView' => $this->catalogView,
            'address' => $address,
            'username' => $this->queryParams['username'] ?? '',
        ]);
    }
}