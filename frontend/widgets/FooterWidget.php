<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class FooterWidget
 * @package frontend\widgets
 */
class FooterWidget extends Widget
{
    /**
     * @var string
     */
    public $template = 'footer-widget';

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        return $this->render($this->template);
    }
}