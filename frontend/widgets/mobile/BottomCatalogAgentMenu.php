<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.03.2019
 * Time: 16:25
 */

namespace frontend\widgets\mobile;
use yii\base\Widget;

/**
 * Class BottomCatalogAgentMenu
 * @package frontend\widgets\mobile
 */

class BottomCatalogAgentMenu extends Widget
{
    /**
     * @var string
     */
    public $template = 'bottom-agent-catalog-menu';

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */

    public function run()
    {
        return $this->render($this->template);
    }
}