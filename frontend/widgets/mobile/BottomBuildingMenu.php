<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.03.2019
 * Time: 16:25
 */

namespace frontend\widgets\mobile;
use yii\base\Widget;

/**
 * Class BottomBuildingMenu
 * @package frontend\widgets\mobile
 */

class BottomBuildingMenu extends Widget
{

    public const ENTITY_BUILDING = 'building';

    /**
     * @var string
     */
    public $template = 'bottom-building-menu';
    /**
     * @var string
     */
    public $entity = self::ENTITY_BUILDING;
    /**
     * @var array
     */
    public $object;

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->object === null) {
            return null;
        }


        return $this->render($this->template, [
            'object' => $this->object,
            'entity' => $this->entity,
        ]);
    }
}