<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.03.2019
 * Time: 16:23
 */

namespace frontend\widgets\mobile;

use common\models\User;
use yii\base\Widget;

/**
 * Class BottomAgentMenu
 * @package frontend\widgets\mobile
 */

class BottomAgentMenu extends Widget
{

    public const ENTITY_AGENT = 'user';

    /**
     * @var string
     */
    public $template = 'bottom-agent-menu';
    /**
     * @var string
     */
    public $entity = self::ENTITY_AGENT;
    /**
     * @var array
     */
    public $object;

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->object === null) {
            return null;
        }


        return $this->render($this->template, [
            'object' => $this->object,
            'entity' => $this->entity,
        ]);
    }
}