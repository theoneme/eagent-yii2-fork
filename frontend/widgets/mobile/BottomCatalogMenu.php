<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:15
 */

namespace frontend\widgets\mobile;

use Yii;
use yii\base\Widget;

/**
 * Class BottomCatalogMenu
 * @package frontend\widgets\mobile
 */
class BottomCatalogMenu extends Widget
{
    /**
     * @var string
     */
    public $template = 'bottom-catalog-menu';

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        return $this->render($this->template);
    }
}