<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:15
 */

namespace frontend\widgets\mobile;

use common\models\Building;
use common\models\Property;
use Yii;
use yii\base\Widget;
use yii\helpers\Url;

/**
 * Class BottomPropertyMenu
 * @package frontend\widgets\mobile
 */
class BottomPropertyMenu extends Widget
{
    public const ENTITY_PROPERTY = 'property';
    public const ENTITY_BUILDING = 'building';

    /**
     * @var string
     */
    public $template = 'bottom-property-menu';
    /**
     * @var string
     */
    public $entity = self::ENTITY_PROPERTY;
    /**
     * @var array
     */
    public $object;

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if ($this->object === null) {
            return null;
        }

        $previousUrl = $this->entity === self::ENTITY_PROPERTY
            ? Url::to(['/property/catalog/index', 'operation' => Property::OPERATION_SALE, 'category' => 'flats'])
            : Url::to(['/building/catalog/index', 'operation' => Building::TYPE_DEFAULT_TEXT]);

        $referrer = Yii::$app->request->referrer;
        $parts = parse_url($referrer);
        if (array_key_exists('host', $parts) && strpos($parts['host'], 'eagent.me')) {
            $previousUrl = $referrer;
        }

        return $this->render($this->template, [
            'object' => $this->object,
            'entity' => $this->entity,
            'previousUrl' => $previousUrl
        ]);
    }
}