<?php

namespace frontend\widgets;

use common\components\CurrencyHelper;
use common\models\Property;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class IndexJobSearchWidget
 * @package frontend\widgets
 */
class IndexJobSearchWidget extends Widget
{
    public const OPERATION_SELL = 'sell';
    public const OPERATION_BUY = 'buy';
    public const OPERATION_RENT = 'rent';
    public const OPERATION_APPRAISAL = 'appraisal';

    /**
     * @var string
     */
    public $operation;

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $operation = !empty($this->operation) && in_array($this->operation, self::getOperationList()) ? $this->operation : self::OPERATION_SELL;
        $config = [
            self::OPERATION_SELL => [
                'title' => Yii::t('index', 'We will help you sell quickly and profitably.'),
                'subtitle' => Yii::t('index', 'Choose the best <a href="/agent/catalog/index">real estate agent</a> in your area or performers for the services you need:'),
                'items' => [
                    [
                        'img' => '/images/job-search/room.png',
                        'url' => 'https://ujobs.me/predprodazhnaya-podgotovka-kvartiry-5bcc6033c4612-job',
                        'title' => Yii::t('index', 'Presale job'),
                        'subtitle' => Yii::t('index', 'Minor repairs, cleaning and preparation for photo session'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/clean.png',
                        'url' => 'https://ujobs.me/kachestvennaya-uborka-kvartiry-5bcd7d2d29cef-job',
                        'title' => Yii::t('index', 'Cleaning'),
                        'subtitle' => Yii::t('index', 'Professional cleaning and preparation for photo session'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1000),
                        'class' => ''
                    ],
                    [
                        'img' => '/images/job-search/photosession.jpg',
                        'url' => 'https://ujobs.me/fotosemka-varianty-vybora-fotosemka-kvartiry-5bcc4416e51bc-job',
                        'title' => Yii::t('index', 'Photo session, video'),
                        'subtitle' => Yii::t('index', 'Photo and video session. Portfolio creation'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1000),
                        'class' => ''
                    ],
                    [
                        'img' => '/images/job-search/photoedit.png',
                        'url' => 'https://ujobs.me/retush-do-5-fotografiy-59d349a926c29-job',
                        'title' => Yii::t('index', 'Photo editing'),
                        'subtitle' => Yii::t('index', 'Photo retouching, photo cleaning, color correction'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 500),
                        'class' => ''
                    ],
                    [
                        'img' => '/images/job-search/search-engine.png',
                        'url' => 'https://ujobs.me/razmeshchenie-obyavleniy-na-saytakh-nedvizhimosti-5bcdac16c0ff1-job',
                        'title' => Yii::t('index', 'Publish on sites'),
                        'subtitle' => Yii::t('index', 'Publish announcements on dozens of real estate sites'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 2000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/social.png',
                        'url' => 'https://ujobs.me/nastroyka-reklamy-v-google-adwords-595b4e2edc0e0-job',
                        'title' => Yii::t('index', 'Advertising settings'),
                        'subtitle' => Yii::t('index', 'Advertising on Google and social networks'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1500),
                        'class' => ''
                    ],
                    [
                        'img' => '/images/job-search/search3.png',
                        'url' => 'https://ujobs.me/pomoshch-v-prodazhe-nedvizhimosti-pokazy-5bfd0b641d1fb-job',
                        'title' => Yii::t('index', 'Condo showcase'),
                        'subtitle' => Yii::t('index', 'Show condo, work with potential buyers'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/contract2.png',
                        'url' => 'https://ujobs.me/oformlenie-dogovora-kupli-prodazhi-nedvizhimosti-5bd1688576e82-job',
                        'title' => Yii::t('index', 'Conclusion of a treaty'),
                        'subtitle' => Yii::t('index', 'Treaty-making on sale of real estate'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/portfolio.png',
                        'url' => 'https://ujobs.me/podgotovka-dogovora-kupli-prodazhi-nedvizhimosti-5bcd9155cf329-job',
                        'title' => Yii::t('index', 'Transaction registration'),
                        'subtitle' => Yii::t('index', 'Preparation of legal documents and mortgages. Transaction registration'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/room-key.png',
                        'url' => 'https://ujobs.me/uslugi-rieltora-po-soprovozhdeniyu-prodazhi-5bfd1278782b1-job',
                        'title' => Yii::t('index', 'Sale condo'),
                        'subtitle' => Yii::t('index', 'Performing all the listed services before making a deal and receiving money'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 30000),
                        'class' => 'height'
                    ],
                ]
            ],
            self::OPERATION_BUY => [
                'title' => Yii::t('index', 'Buy real estate directly.'),
                'subtitle' => Yii::t('index', 'You only need to {link}, and we will help you make a deal.', [
                    'link' => Html::a(Yii::t('index', 'choose a property'), [
                        '/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_SALE
                    ])
                ]),
                'items' => [
                    [
                        'img' => '/images/job-search/sale.png',
                        'url' => 'https://ujobs.me/analiz-nedvizhimosti-5bd04f127f481-job',
                        'title' => Yii::t('index', 'Market analysis'),
                        'subtitle' => Yii::t('index', 'Search on all portals for the best deals'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1500),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/search.png',
                        'url' => 'https://ujobs.me/organizatsiya-prosmotrov-kvartir-vybrannykh-dlya-5bfd1fe4c9f67-job',
                        'title' => Yii::t('index', 'Property showcase'),
                        'subtitle' => Yii::t('index', 'Work with sellers. Condos showcase'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/mortgage.png',
                        'url' => 'https://ujobs.me/pomoshch-v-podbore-i-oformlenii-5bcd79ba88b3d-job',
                        'title' => Yii::t('index', 'Mortgage comparison'),
                        'subtitle' => Yii::t('index', 'Compare top mortgage rates and choose the most profitable. Registration'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 3000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/contract.png',
                        'url' => 'https://ujobs.me/podgotovka-dogovora-kupli-prodazhi-dogovora-na-5bfd27bedafb4-job',
                        'title' => Yii::t('index', 'Preparing contract'),
                        'subtitle' => Yii::t('index', 'Preparing sales contract and mortgage contract'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 2000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/house2.png',
                        'url' => 'https://ujobs.me/oformlenie-sdelki-kupli-prodazhi-nedvizhimosti-5bfd33b6b5182-job',
                        'title' => Yii::t('index', 'Transaction registration'),
                        'subtitle' => Yii::t('index', 'Preparation of all documents, transaction registration'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 20000),
                        'class' => 'height'
                    ],
                ]

            ],
            self::OPERATION_RENT => [
                'title' => Yii::t('index', 'Rent real estate directly.'),
                'subtitle' => Yii::t('index', 'You only need to {link}, and we will help you make a deal.', [
                    'link' => Html::a(Yii::t('index', 'choose a property'), [
                        '/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_RENT
                    ])
                ]),
                'items' => [
                    [
                        'img' => '/images/job-search/rent.png',
                        'url' => 'https://ujobs.me/arenda-nedvizhimosti-analiz-rynka-i-5bfd44ec10136-job ',
                        'title' => Yii::t('index', 'Market analysis'),
                        'subtitle' => Yii::t('index', 'Search on all portals for the best rental offers'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1500),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/real-estate.png',
                        'url' => 'https://ujobs.me/arenda-nedvizhimosti-analiz-rynka-i-5bfd44ec10136-job',
                        'title' => Yii::t('index', 'Property showcase'),
                        'subtitle' => Yii::t('index', 'Calls to the owners. Condos showcase'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/search2.png',
                        'url' => 'https://ujobs.me/podgotovka-dogovora-arendy-nedvizhimosti-5bcda77584641-job',
                        'title' => Yii::t('index', 'Preparing contract'),
                        'subtitle' => Yii::t('index', 'Preparation and execution of the lease agreement'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 2000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/house.png',
                        'url' => 'https://ujobs.me/podgotovka-dokumentov-oformlenie-sdelki-arendy-5bfd3d50905e9-job ',
                        'title' => Yii::t('index', 'Transaction registration'),
                        'subtitle' => Yii::t('index', 'Preparation of documents, lease transaction registration'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/moving-truck.png',
                        'url' => 'https://ujobs.me/organizatsiya-pereezda-perevozka-mebeli-i-5bfd4cf472cf5-job',
                        'title' => Yii::t('index', 'Moving'),
                        'subtitle' => Yii::t('index', 'Transportation of furniture and household equipment'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                ]
            ],
            self::OPERATION_APPRAISAL => [
                'title' => Yii::t('index', 'Get an estimate of your property for free.'),
                'subtitle' => Yii::t('index', 'You only need to <a href="#" data-toggle="modal" data-target="#contactModal">fill the form</a> and we will send you an estimate of your property for free.'),
                'items' => [
                    [
                        'img' => '/images/job-search/strategy.png',
                        'url' => 'https://ujobs.me/otsenka-nedvizhimosti-5bcd9993a2847-job',
                        'title' => Yii::t('index', 'Market analysis'),
                        'subtitle' => Yii::t('index', 'Market analysis. Determining the market value of real estate'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1500),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/text-lines.png',
                        'url' => 'https://ujobs.me/otsenka-nedvizhimosti-i-sostavlenie-ofitsialnogo-5bfe575a4611a-job ',
                        'title' => Yii::t('index', 'Preparation of the official report'),
                        'subtitle' => Yii::t('index', 'Survey company is drawing up an official report'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 15000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/mortgage2.png',
                        'url' => 'https://ujobs.me/pomoshch-v-podbore-i-oformlenii-5bcd79ba88b3d-job',
                        'title' => Yii::t('index', 'Mortgage comparison'),
                        'subtitle' => Yii::t('index', 'Compare top mortgage rates and choose the most profitable. Registration'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 2000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/mortgage-loan.png',
                        'url' => 'https://ujobs.me/refinansirovanie-ipoteki-5bcd8cba5840c-job ',
                        'title' => Yii::t('index', 'Mortgage Refinancing'),
                        'subtitle' => Yii::t('index', 'Reducing mortgage payments. Refinancing registration'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000),
                        'class' => 'height'
                    ],
                    [
                        'img' => '/images/job-search/property.png',
                        'url' => 'https://ujobs.me/oformlenie-sdelki-kupli-prodazhi-nedvizhimosti-5bfd33b6b5182-job',
                        'title' => Yii::t('index', 'Transaction registration'),
                        'subtitle' => Yii::t('index', 'Preparation of all documents, transaction registration'),
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 20000),
                        'class' => 'height'
                    ],
                ]
            ],
        ];

        $items = $config[$operation];
        return $this->render('index-job-search-widget', [
            'items' => $items,
        ]);
    }

    /**
     * @return array
     */
    public static function getOperationList()
    {
        return [
            self::OPERATION_SELL,
            self::OPERATION_BUY,
            self::OPERATION_RENT,
            self::OPERATION_APPRAISAL
        ];
    }
}