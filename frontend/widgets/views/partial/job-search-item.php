<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var array $model
 * @var View $this
 */

?>

<div class="help-radio step-option">
    <div>
        <a class="help-radio-img" href="<?= $model['url'] ?>"
           target="_blank"><?= Html::img($model['img'], ['alt' => $model['title'], 'title' => $model['title'], 'class' => $model['class']]) ?></a>
        <div class="titles-bottom text-left">
            <div class="help-radio-title">
                <?= $model['title'] ?>
            </div>
            <div class="help-radio-subtitle">
                <?= $model['subtitle'] ?>
            </div>
        </div>
        <div class="price-more">
            <div class="tb-price"><?= Yii::t('index', 'from {price}', ['price' => $model['price']]) ?></div>
            <a class="tb-more" href="<?= $model['url'] ?>" target="_blank"><?= Yii::t('index', 'Watch more'); ?></a>
        </div>
    </div>
</div>