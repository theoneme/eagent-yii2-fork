<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.12.2018
 * Time: 17:45
 */

use yii\helpers\Html;

/**
 * @var array $items
 */

\frontend\assets\JobSearchAsset::register($this);

?>

<div id="job-search-help">
    <div class="help-content block-content text-center">
        <div class="container-fluid-short">
            <h2 class="text-center">
                <span class="c-green"><?= Yii::t('index', 'New!') ?></span>
                <?= $items['title'] ?>
            </h2>
            <p class="text-center">
                <?= $items['subtitle'] ?>
                <?= Html::a(Yii::t('index', 'Watch more'), null, ['data-toggle' => 'modal', 'data-target' => '#jobSearchMoreModal']) ?>
            </p>
        </div>
        <div class="help-box">
            <div class="help-step top-step">
                <?php foreach ($items['items'] as $item) {
                    echo $this->render('partial/job-search-item', ['model' => $item]);
                } ?>
            </div>
        </div>
    </div>
</div>
