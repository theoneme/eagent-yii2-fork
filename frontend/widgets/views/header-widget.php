<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:18
 */

use common\helpers\UtilityHelper;
use common\models\Property;
use frontend\modules\crm\models\CrmMember;
use frontend\widgets\auth\LoginWidget;
use frontend\widgets\auth\RequestResetWidget;
use frontend\widgets\auth\SignupWidget;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var array $categorySlugs
 * @var array $categoryTree
 * @var array $user
 * @var CrmMember[] $crmMembers
 * @var boolean $isGuest
 * @var integer $currentProfile
 * @var integer $companyId
 * @var boolean $isCompany
 * @var string $balance
 */

?>

    <header>
        <div class="fixed-top">
            <div class="left-top-item text-left text-left trigger-cell">
                <div class="trigger-menu visible-xs">
                    <i></i>
                </div>
            </div>
            <div class="left-top-item text-left logo-cell">
                <div class="flex">
                    <div class="logo-img">
                        <?= Html::img(['/images/logo.jpg'], ['alt' => 'eagent', 'title' => 'eagent']) ?>
                    </div>
                    <div class="logo-text">
                        <a class="logo" href="/"><span>E</span>AGENT</a>
                        <div class="powered"><?= Yii::t('main', 'Powered by {who}', ['who' => 'uJobs']) ?></div>
                    </div>
                </div>
            </div>
            <nav class="left-top-item text-left top-menu">
                <div class="collapse navbar-collapse">
                    <ul class="nav nav-tabs no-list">
                        <li>
                            <?= Html::a(
                                Yii::t('main', 'Buy') . ' <i class="icon-down"></i>',
                                ['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'sale'],
                                ['data-menu' => 'menu-sell']
                            ) ?>
                        </li>
                        <li>
                            <?= Html::a(
                                Yii::t('main', 'Rent') . ' <i class="icon-down"></i>',
                                ['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'rent'],
                                ['data-menu' => 'menu-rent']
                            ) ?>
                        </li>
                        <li class="no-sub-menu">
                            <?= Html::a(Yii::t('main', 'Agents'), ['/agent/catalog/index'], ['data-menu' => 'menu-agent']) ?>
                        </li>
                        <li class="no-sub-menu">
                            <?= Html::a(
                                Yii::t('main', 'Requests'),
                                ['/request/catalog/index'],
                                ['data-menu' => 'menu-requests']
                            ) ?>
                        </li>
                    </ul>
                </div>
            </nav>
            <div class="right-top-item language text-right">
                <a class="toggle-language-link">
                    <i class="icon-earth-globe"></i>
                    <?= Yii::t('main', Yii::$app->params['languages'][Yii::$app->language]) ?>
                    / <?= Yii::$app->params['app_currency'] ?>
                </a>
            </div>

            <?php if ($isGuest === true) { ?>
                <div class="right-top-item call text-right">
                    <a href="#" class="dark-green" data-toggle="modal" data-target="#login-modal">
                        <i class="icon-man-user"></i>
                        <span class="fs-text">
                            <?= Yii::t('main', 'Sign In') ?>
                        </span>
                    </a>
                </div>
                <div class="right-top-item call text-right post-announcement">
                    <a href="#" class="btn btn-small btn-green" data-toggle="modal" data-target="#signup-modal">
                        <?= Yii::t('main', 'List your property') ?>
                    </a>
                </div>
            <?php } else { ?>

                <div class="right-top-item language text-right">
                    <?= Html::a($user['balance'], ['/user/wallet/index'], ['class' => 'c-green'])?>
                </div>
                <div class="right-top-item btn-profile text-right">
                    <div class="header-avatar text-center">
                        <?php if (!empty($user['avatar'])) {
                            echo Html::img($user['avatar'], ['alt' => $user['username'], 'title' => $user['username']]);
                        } else {
                            echo StringHelper::truncate($user['username'], 1, '');
                        } ?>
                    </div>
                    <div class="header-username">
                        <?= Html::encode(StringHelper::truncateWords($user['username'], 1, '')) ?>
                    </div>
                    <i class="icon-caret-down" aria-hidden="true"></i>
                </div>
            <?php } ?>
        </div>

        <div class="nav-head-profile" id="header-language">
            <div class="container-fluid">
                <a href="#" class="close-nav-head-profile">
                    <i class="icon-close"></i>
                </a>
                <div class="row nph-padding">
                    <div class="col-md-9 col-sm-8">
                        <div class="nhp-title"><?= Yii::t('main', 'Language') ?></div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <?php
                                $columnCount = ceil(count(Yii::$app->params['supportedLocales']) / 4);
                                $iterator = 0;
                                foreach (Yii::$app->params['supportedLocales'] as $locale => $code) {
                                    echo Html::a('<b>'
                                        . Yii::t('main', Yii::$app->params['languages'][$locale], [], $locale)
                                        . '</b>&nbsp;'
                                        . mb_strtolower(Yii::t('main', 'Language', [], $locale)), UtilityHelper::localeCurrentUrl($code), [
                                        'class' => $locale === Yii::$app->language ? 'active' : ''
                                    ]);
                                    if (++$iterator >= $columnCount) {
                                        echo '</div><div class="col-md-3 col-sm-6">';
                                        $iterator = 0;
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="nhp-title"><?= Yii::t('main', 'Currencies') ?></div>
                        <div class="row" style="margin-left: 0">
                            <?php
                            foreach (Yii::$app->params['currencies'] as $k => $v) {
                                echo Html::a('<b>'
                                    . Yii::t('main', Yii::$app->params['currencies'][$k]['title'])
                                    . '</b> - '
                                    . $v['sign'], Url::current(['app_currency' => strtolower($k)]), [
                                    'class' => $k === Yii::$app->params['app_currency'] ? 'active' : ''
                                ]);
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--**********************Profile menu*************************************-->
        <?php if ($isGuest === false) { ?>
            <div class="nav-head-profile" id="profile-drop">
                <div class="container-fluid">
                    <a href="" class="close-nav-head-profile">
                        <i class="icon-close"></i>
                    </a>
                    <div class="row nph-padding">
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12 small-padding">
                                    <a class="nhp-title" target="_blank"
                                       href="<?= Url::to(['/agent/agent/view', 'id' => $user['id']]) ?>">
                                        <?= Yii::t('main', 'View My Profile') ?>
                                    </a>
                                </div>
                                <div class="col-md-6 col-sm-6 small-padding">
                                    <div class="nhp-subtitle"><?= Yii::t('main', 'My Announcements') ?></div>
                                    <ul>
                                        <li>
                                            <i class="icon-clipboards"></i>
                                            <a target="_blank" href="<?= Url::to(['/property/manage/create']) ?>">
                                                <?= Yii::t('main', 'List your property') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <i class="icon-clipboards-loop"></i>
                                            <a target="_blank" href="<?= Url::to(['/property/list/index']) ?>">
                                                <?= Yii::t('main', 'My Properties') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-sm-6 small-padding">
                                    <div class="nhp-subtitle"><?= Yii::t('main', 'My Requests') ?></div>
                                    <ul>
                                        <li>
                                            <i class="icon-unknown"></i>
                                            <a target="_blank" href="<?= Url::to(['/request/manage/create']) ?>">
                                                <?= Yii::t('main', 'List your request') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <i class="icon-research"></i>
                                            <a target="_blank" href="<?= Url::to(['/request/list/index']) ?>">
                                                <?= Yii::t('main', 'My Requests') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12 small-padding">
                                    <a class="nhp-title"
                                       href="<?= Url::to(['/agent/agent/view', 'id' => $user['id']]) ?>">
                                        <?= $user['username'] ?>
                                    </a>
                                </div>
                                <div class="col-md-4 small-padding">
                                    <ul>
                                        <li>
                                            <i class="icon-settings"></i>
                                            <?php if ($isCompany === true) { ?>
                                                <a target="_blank"
                                                   href="<?= Url::to(['/company/manage/update', 'id' => $companyId]) ?>"><?= Yii::t('main', 'Company Settings') ?></a>
                                            <?php } else { ?>
                                                <a target="_blank"
                                                   href="<?= Url::to(['/user/manage/update']) ?>"><?= Yii::t('main', 'Profile Settings') ?></a>
                                            <?php } ?>
                                        </li>
                                        <li>
                                            <i class="icon-envelope"></i>
                                            <a href="#"><?= Yii::t('main', 'Messages') ?></a>
                                        </li>
                                        <li>
                                            <i class="icon-notification"></i>
                                            <a href="#"><?= Yii::t('main', 'Notifications') ?></a>
                                        </li>
                                        <li>
                                            <i class="icon-multiple-users-silhouette"></i>
                                            <a target="_blank"
                                               href="<?= Url::to(['/company/list/index']) ?>"><?= Yii::t('main', 'My Companies') ?></a>
                                        </li>
                                        <li>
                                            <i class="icon-logout"></i>
                                            <a href="<?= Url::to(['/auth/logout']) ?>"><?= Yii::t('main', 'Logout') ?></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4 small-padding">
                                    <div class="nhp-subtitle"><?= Yii::t('main', 'Useful Links') ?></div>
                                    <ul>
                                        <li>
                                            <i class="icon-piggy-bank"></i>
                                            <a target="_blank"
                                               href="<?= Url::to(['/page/tariff']) ?>"><?= Yii::t('main', 'Tariffs') ?></a>
                                        </li>
                                        <li>
                                            <i class="icon-thinking-head"></i>
                                            <a target="_blank" href="<?= Url::to(['/crm/site/index']) ?>">
                                            <span style='color: #00b22d; white-space: normal;'>
                                                <?= Yii::t('main', 'New!') ?>
                                            </span>
                                                <br>
                                                <?= Yii::t('main', 'Create CRM for free') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <i class="icon-collaboration"></i>
                                            <a target="_blank" href="<?= Url::to(['/company/manage/create']) ?>">
                                            <span style='color: #00b22d; white-space: normal;'>
                                                <?= Yii::t('main', 'New!') ?>
                                            </span>
                                                <br>
                                                <?= Yii::t('main', 'Create Company for free') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <?php if (!empty($crmMembers)) { ?>
                                    <div class="col-md-4 small-padding">
                                        <div class="nhp-subtitle"><?= Yii::t('main', 'My CRM') ?></div>
                                        <ul>
                                            <?php foreach ($crmMembers as $member) { ?>
                                                <li>
                                                    <?= $member->crm->logo
                                                        ? Html::img($member->crm->logo, [
                                                            'style' => 'height: 20px;border-radius: 50%;margin-left: -23px; display: inline-block;'
                                                        ])
                                                        : '<i class="icon-crm"></i>'
                                                    ?>
                                                    <?= Html::a($member->crm->name, [
                                                        '/crm/site/login',
                                                        'id' => $member->id
                                                    ], ['style' => 'display: inline-block;', 'target' => '_blank']) ?>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                                <?php if (!empty($profiles) && count($profiles) > 1) { ?>
                                    <div class="col-md-4 small-padding">
                                        <div class="nhp-subtitle"><?= Yii::t('main', 'My Profiles') ?></div>
                                        <ul>
                                            <?php foreach ($profiles as $profile) { ?>
                                                <li class="no-padding">
                                                    <?= $profile['companyLogo']
                                                        ? Html::img($profile['companyLogo'], ['class' => 'header-small-avatar'])
                                                        : '<i class="icon-crm"></i>'
                                                    ?>
                                                    <?= Html::a($profile['companyTitle'], [
                                                        '/company/ajax/profile-switch'
                                                    ], [
                                                        'style' => 'display: inline-block;',
                                                        'data-action' => 'identity-switcher',
                                                        'data-identity' => $profile['userId'],
                                                        'class' => $currentProfile === $profile['userId'] ? 'font-bold' : null
                                                    ]) ?>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!--**********************Profile menu*************************************-->

        <div class="sub-menu" id="menu-sell">
            <div class="sub-menu-container">
                <h3 class="sub-menu-title text-left"><?= Yii::t('main', 'Properties for sale') ?></h3>
                <div class="sub-menu-cols">
                    <?php foreach ($categoryTree as $category) { ?>
                        <div class="sub-menu-col">
                            <div class="sub-menu-block">
                                <h2 class="sub-menu-sub-title text-left">
                                    <?= Html::a($category['title'], [
                                        '/property/catalog/index',
                                        'category' => $category['slug'],
                                        'operation' => Property::OPERATION_SALE
                                    ]) ?>
                                </h2>
                                <ul class="no-list ul-submenu">
                                    <?php foreach ($category['children'] as $categoryChild) { ?>
                                        <li>
                                            <?= Html::a($categoryChild['title'], [
                                                '/property/catalog/index',
                                                'category' => $categoryChild['slug'],
                                                'operation' => Property::OPERATION_SALE
                                            ]) ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="sub-menu" id="menu-rent">
            <div class="sub-menu-container">
                <h3 class="sub-menu-title text-left"><?= Yii::t('main', 'Rental Property') ?></h3>
                <div class="sub-menu-cols">
                    <?php foreach ($categoryTree as $category) { ?>
                        <div class="sub-menu-col">
                            <div class="sub-menu-block">
                                <h2 class="sub-menu-sub-title text-left">
                                    <?= Html::a($category['title'], [
                                        '/property/catalog/index',
                                        'category' => $category['slug'],
                                        'operation' => Property::OPERATION_RENT
                                    ]) ?>
                                </h2>
                                <ul class="no-list ul-submenu">
                                    <?php foreach ($category['children'] as $categoryChild) { ?>
                                        <li>
                                            <?= Html::a($categoryChild['title'], [
                                                '/property/catalog/index',
                                                'category' => $categoryChild['slug'],
                                                'operation' => Property::OPERATION_RENT
                                            ]) ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <a class="global-create-link text-center" href="<?= Url::to(['/property/manage/create']) ?>">
            <i class="icon-edit"></i>
        </a>
    </header>

<?php if ($isGuest === true) { ?>
    <?= SignupWidget::widget() ?>
    <?= LoginWidget::widget() ?>
    <?= RequestResetWidget::widget() ?>
<?php } ?>

<?php $script = <<<JS
    $(document.body).on('mouseenter','.top-menu li:not(.no-sub-menu) a',function(){
        let idMenu = $(this).attr('data-menu');
        $('#'+idMenu).addClass('open');
        $('.fixed-top').addClass('white-top');
    });

    $(document.body).on('mouseleave','.top-menu li:not(.no-sub-menu) a',function(e){
        let xMouse = e.pageX,
            yMouse = e.pageY,
            subMenu = $('.sub-menu.open'),
            x1Menu = subMenu.offset().left,
            x2Menu = x1Menu + subMenu.width(),
            y1Menu = subMenu.offset().top,
            y2Menu = y1Menu + subMenu.height();
        
        if(xMouse < x1Menu || xMouse > x2Menu || yMouse < y1Menu || yMouse > y2Menu) {
            $('.sub-menu').removeClass('open');
            $('.fixed-top').removeClass('white-top');
        }
    });

    $(document.body).on('mouseleave','.sub-menu',function(){
        $('.sub-menu').removeClass('open');
        $('.fixed-top').removeClass('white-top');
    });
    
    function headerhide() {
        let width = 20,
            dataMenu = '',
            maxWidth = $('.fixed-top').width() - 90;
        
        $('.fixed-top .right-top-item,.fixed-top .left-top-item.logo-cell').each(function () {
            width += $(this).outerWidth(true);
        });
        $('.fixed-top .left-top-item.top-menu li').each(function () {
            if (maxWidth <= width + $(this).outerWidth()) {
                $(this).hide();
                dataMenu = $(this).find('a').attr('data-menu');
                $('.aside-menu').find('[data-id="'+dataMenu+'"]').removeClass('hide');
            } else {
                $(this).show();
                dataMenu = $(this).find('a').attr('data-menu');
                $('.aside-menu').find('[data-id="'+dataMenu+'"]').addClass('hide');
                width += $(this).outerWidth();
            }
        });
    }
    
    headerhide();
    $(window).resize(function() {
        headerhide();
    });
    function dropHeaderMenu(e, idMenu) {
        e.stopPropagation();
        if ($('.nav-head-profile.open').attr('id') != idMenu) {
            $('.nav-head-profile').removeClass('open');
        }
        $('#' + idMenu).toggleClass('open');
        
        if(!$('#' + idMenu).hasClass('open')){
            setTimeout(function() { $('.fixed-top').toggleClass('white-top'); }, 500);
        } else {
            $('.fixed-top').toggleClass('white-top');
        }
    }

    $(document.body).on('click', '.btn-profile', function (e) {
        dropHeaderMenu(e, 'profile-drop');
    });
    
    $(document.body).on('click', '.toggle-language-link', function (e) {
        dropHeaderMenu(e, 'header-language');
    });
    
    $(document.body).on('click', '.close-nav-head-profile', function () {
        $('.nav-head-profile').removeClass('open');
        return false
    });
     
    $(document.body).click(function (e) {
        if (!$(e.target).parents('.nav-head-profile').length && !$(e.target).hasClass('nav-head-profile')) {
            $('.nav-head-profile').removeClass('open');
        }
    });

    window.addEventListener('popstate', function(e){
        if (e.state) {
            location.reload();
        }
    });

    $(document.body).on('click','.animate-button',function(){
       $(this).addClass('go') 
    });
    
    $('[data-action="identity-switcher"]').on('click', function() {
        let href = $(this).attr('href'),
            id = $(this).data('identity');
        
        $.post(href, {identity: id}, function(response) {
            if(response.success === true) {
                window.location.reload();
            }
        });
        
        return false;
    });
JS;

$this->registerJs($script);
