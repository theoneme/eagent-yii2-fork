<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:16
 */

use common\components\CurrencyHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $catalogView
 * @var array $filters
 * @var string $sort
 * @var string $address
 * @var array $params
 * @var integer $gridSize
 */

?>

<?= Html::beginForm(Url::canonical(), 'get', [
    'id' => 'filter-form',
]); ?>
<?= Html::hiddenInput('sort', $sort, ['id' => 'filter-sort']) ?>
<?= Html::hiddenInput('box', $params['box'] ?? null, ['id' => 'filter-box']) ?>
<?= Html::hiddenInput('district', $params['district'] ?? null, ['id' => 'filter-district']) ?>
<?= Html::hiddenInput('microdistrict', $params['microdistrict'] ?? null, ['id' => 'filter-microdistrict']) ?>
<?= Html::hiddenInput('zoom', $params['zoom'] ?? null, ['id' => 'filter-zoom']) ?>
<div class="filters">
    <div class="loading-overlay hidden"></div>
    <div class="filter-search">
        <div class="input-group search text-center">
            <?= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
            <?= Html::input('search', 'city-helper', $address, [
                'class' => 'form-control input-grey',
                'id' => 'filter-city-helper',
                'placeholder' => Yii::t('catalog', 'Enter city, region or country')
            ]) ?>
            <a href="#" class="input-group-addon"><i class="icon-search"></i></a>
            <div class="district-form text-left hidden">
                <div class="district-inner">
                    <p class="">
                        <?= Yii::t('catalog', 'Start typing and select one of the options') ?>
                    </p>
                    <div class="autocomplete-loc-suggestions">

                    </div>
                </div>
                <div class="district-open">
                    <a class="flex space-between" href="#" data-action="load-city-parts" data-toggle="modal"
                       data-target="#city-parts">
                        <span><?= Yii::t('catalog', 'Districts and microdistricts') ?></span>
                        <i class="icon-next"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php $categoryFilter = ArrayHelper::remove($filters, 'category'); ?>
    <?php if ($categoryFilter) { ?>
        <div class="filter-item filter-types">
            <div class="filter-name" data-action="expand-filter">
                <div class="filter-types-icons">
                    <?php if (!empty($categoryFilter['checkedCircle'])) { ?>
                        <div class="filter-circle filter-circle-<?= $categoryFilter['checkedCircle'] ?>"></div>
                    <?php } ?>
                </div>
                <div class="filter-type-name"><?= $categoryFilter['checkedTitle'] ?></div>
            </div>
            <div class="filter-box" data-role="mobile-category-filter">
                <?php foreach ($categoryFilter['values'] as $key => $category) {
                    $checked = $categoryFilter['checked'] === $key;
                    $childChecked = !empty($category['children']) && array_key_exists($categoryFilter['checked'], $category['children']);
                    ?>
                    <div class="filter-type-group <?= $checked || $childChecked ? 'active' : '' ?>">
                        <div class="filter-type-group-name">
                            <?= Html::radio('category', $categoryFilter['checked'] === $key, [
                                'id' => "cat-{$key}",
                                'value' => $key,
                                'class' => 'radio-checkbox',
                            ]) ?>
                            <label for="<?= "cat-{$key}" ?>">
                                <span class="filter-circle filter-circle-<?= $category['circle'] ?>"></span>
                                <span class="ftgn-title"><?= $category['title'] ?></span>
                            </label>
                        </div>
                        <?php if (!empty($category['children'])) { ?>
                            <?php foreach ($category['children'] as $ckey => $cvalue) { ?>
                                <div class="filter-sub-types">
                                    <div class="filter-sub-type">
                                        <?= Html::radio('category', $categoryFilter['checked'] === $ckey, [
                                            'id' => "cat-{$ckey}",
                                            'value' => $ckey,
                                        ]) ?>
                                        <label for="<?= "cat-{$ckey}" ?>">
                                            <span class="ftgn-title"><?= $cvalue['title'] ?></span>
                                        </label>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php $priceFilter = ArrayHelper::remove($filters, 'price'); ?>
    <?php if ($priceFilter) { ?>
        <div class="filter-item filter-prices">
            <div class="filter-name" data-action="expand-filter">
                <div class="filter-type-name">
                    <span id="up-price"></span>
                    <span id="begin-price">
                            <?php if ($priceFilter['left']['checked'] !== null) { ?>
                                <?= CurrencyHelper::format(Yii::$app->params['app_currency'], $priceFilter['left']['checked']) ?>+
                            <?php } ?>
                        </span>
                    <span id="delimiter-price">
                            <?php if ($priceFilter['left']['checked'] === null && $priceFilter['right']['checked'] === null) { ?>
                                <?= Yii::t('catalog', 'Any Price') ?>
                            <?php } ?>
                        </span>
                    <span id="end-price">
                            <?php if ($priceFilter['right']['checked'] !== null) { ?>
                                <?= CurrencyHelper::format(Yii::$app->params['app_currency'], $priceFilter['right']['checked']) ?>
                            <?php } ?>
                        </span>
                </div>
            </div>
            <div class="filter-box">
                <div class="filter-price-diapason clearfix text-center">
                    <div class="col-md-5 col-sm-5 col-xs-5 no-padding">
                        <?= Html::input('number', 'price[min]', $priceFilter['left']['checked'], [
                            'placeholder' => Yii::t('catalog', 'Min'),
                            'class' => 'input-grey',
                            'id' => 'min-price-input'
                        ]) ?>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-2 no-padding">
                        &#8212;
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-5 no-padding">
                        <?= Html::input('number', 'price[max]', $priceFilter['right']['checked'], [
                            'placeholder' => Yii::t('catalog', 'Max'),
                            'class' => 'input-grey',
                            'id' => 'max-price-input'
                        ]) ?>
                    </div>
                </div>
                <div class="filter-column-left text-left">
                    <?php foreach ($priceFilter['left']['values'] as $value) { ?>
                        <div class="filter-choose" data-action="set-left-price"
                             data-value="<?= $value ?>"><?= CurrencyHelper::format(Yii::$app->params['app_currency'], $value) ?>
                            +
                        </div>
                    <?php } ?>
                </div>
                <div class="filter-column-right text-right">
                    <?php foreach ($priceFilter['right']['values'] as $value) { ?>
                        <?php if ($value) { ?>
                            <div class="filter-choose" data-action="set-right-price"
                                 data-value="<?= $value ?>"><?= CurrencyHelper::format(Yii::$app->params['app_currency'], $value) ?>
                                +
                            </div>
                        <?php } else { ?>
                            <div class="filter-choose" data-action="set-right-price"
                                 data-value=""><?= Yii::t('catalog', 'Any Price') ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php $roomFilter = ArrayHelper::remove($filters, 'rooms'); ?>
    <?php if ($roomFilter) { ?>
        <div class="filter-item filter-beds">
            <?= Html::hiddenInput('rooms[min]', $roomFilter['checked']['min']) ?>
            <div class="filter-name" data-action="expand-filter">
                <div class="filter-type-name">
                    <?= Yii::t('catalog', '<span >{rooms, plural, one{#+ rooms} other{#+ rooms}}</span>', [
                        'rooms' => $roomFilter['checked']['min'] ?? $roomFilter['values']['min']
                    ]) ?>
                </div>
            </div>
            <div class="filter-box">
                <div class="filter-column-left">
                    <?php $checked = (int)($roomFilter['checked']['min'] ?? $roomFilter['values']['min']); ?>
                    <!--                        --><?php //for ($i = (int)$roomFilter['values']['min']; $i < $roomFilter['values']['max']; $i++) { ?>
                    <?php for ($i = 1; $i < 6; $i++) { ?>
                        <?= Html::tag('div', "{$i}+", [
                            'class' => 'filter-choose ' . ($i === $checked ? 'active' : ''),
                            'data-action' => 'set-top-filter',
                            'data-value' => $i
                        ]) ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php $operationFilter = ArrayHelper::remove($filters, 'operation'); ?>
    <?php if ($operationFilter) { ?>
        <div class="filter-item filter-beds">
            <?= Html::hiddenInput('operation', $operationFilter['checked']) ?>
            <div class="filter-name" data-action="expand-filter">
                <div class="filter-type-name">
                    <?= $operationFilter['values'][$operationFilter['checked']]['title'] ?>
                </div>
            </div>
            <div class="filter-box">
                <div class="filter-column-left">
                    <?php $checked = ($operationFilter['checked'] ?? $operationFilter['values']); ?>
                    <?php foreach ($operationFilter['values'] as $key => $value) { ?>
                        <?= Html::tag('div', $value['title'], [
                            'class' => 'filter-choose ' . ($key === $checked ? 'active' : ''),
                            'data-action' => 'set-top-filter',
                            'data-value' => $key
                        ]) ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <!--        --><?php //if (!empty($filters)) { ?>
    <div class="filter-item filter-more">
        <div class="filter-name" data-action="show-more-filters">
            <div class="filter-type-name">
                <i class="icon-parameters"></i>
                <?= Yii::t('catalog', 'More') ?>
            </div>
        </div>
    </div>
    <!--        --><?php //} ?>

    <div class="filter-item filter-switch">
        <?= Html::a('<i class="icon-menu"></i>', '#', [
            'class' => 'switch-items ' . ($catalogView === 'grid' ? 'active' : ''),
            'data-view' => 'grid',
            'data-action' => 'change-catalog-view',
            'data-url' => Url::current([0 => '/property/catalog/index', 'box' => null, 'zoom' => null])
        ]) ?>
    </div>
    <div class="filter-item filter-switch filter-list">
        <?= Html::a('<i class="icon-list"></i>', '#', [
            'class' => 'switch-items ' . ($catalogView === 'list' ? 'active' : ''),
            'data-view' => 'list',
            'data-action' => 'change-catalog-view',
            'data-url' => Url::current([0 => '/property/catalog/index', 'box' => null, 'zoom' => null])
        ]) ?>
    </div>
    <div class="filter-item filter-switch">
        <?= Html::a('<i class="icon-gps"></i>', '#', [
            'class' => 'switch-items ' . ($catalogView === 'map' ? 'active' : ''),
            'data-view' => 'map',
            'data-action' => 'change-catalog-view',
//                'data-url' => Url::current(['box' => null, 'zoom' => null])
        ]) ?>
    </div>
</div>

<div class="clearfix">
    <div class="col-md-<?= $gridSize ?> col-sm-12 catalog-left">
        <div class="params-filter">
            <div class="params-filter-content">
                <div class="row">
                    <?php foreach ($filters as $key => $filter) { ?>
                        <div class="col-md-<?= $gridSize > 6 ? 4 : 6 ?> col-sm-6">
                            <?= $this->render("filter/{$filter['type']}", [
                                'filter' => $filter,
                                'key' => $key
                            ]) ?>
                        </div>
                    <?php } ?>
                    <div class="col-md-12">
                        <?= Html::submitInput(Yii::t('catalog', 'Show'), ['class' => 'btn btn-big btn-blue-white']) ?>
                        <?= Html::a(Yii::t('catalog', 'Reset'), '#', ['class' => 'btn btn-big btn-white-blue', 'data-action' => 'reset-filter']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= Html::endForm() ?>

<?= $this->render('filter/city-part-modal') ?>

<?php $script = <<<JS
    $('body').on('click', function (e) {
        if (!$(e.target).closest(".filter-search").length) {
            $('.district-form').addClass('hidden');
        }
        
        if (!$(e.target).closest(".filters").length) {
            $('.filter-item').removeClass('open');
        }
    })
JS;
$this->registerJs($script); ?>

