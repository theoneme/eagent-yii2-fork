<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:53
 */

use yii\helpers\Html;

/**
 * @var array $filter
 * @var \yii\widgets\ActiveForm $form
 * @var string $key
 */

?>

<div class="form-group">
    <label for="<?= $key ?>-min"><?= $filter['title'] ?></label>
    <div class="flex two-param">
        <?= Html::textInput("{$key}[min]", $filter['checked']['min'], [
            'placeholder' => Yii::t('catalog', 'From {min}', ['min' => $filter['values']['min']]),
            'type' => 'number',
            'min' => 0,
            'id' => "{$key}-min",
        ]) ?>
        <div class="dash">&#8211;</div>
        <?= Html::textInput("{$key}[max]", $filter['checked']['max'], [
            'placeholder' => Yii::t('catalog', 'To {max}', ['max' => $filter['values']['max']]),
            'type' => 'number',
            'min' => 0
        ]) ?>
    </div>
</div>