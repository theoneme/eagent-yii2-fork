<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.06.2019
 * Time: 11:37
 */

use yii\helpers\Html;

?>

<div id="city-parts" class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title text-center"><?= Yii::t('catalog', 'Districts and microdistricts') ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <h3>
                            <?= Yii::t('catalog', 'Districts') ?>
                        </h3>
                        <div class='modal-double-column' data-role="district-container">

                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3>
                            <?= Yii::t('catalog', 'Microdistricts') ?>
                        </h3>
                        <div class="col-district" data-role="microdistrict-container">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-left">
                    <?= Html::a(Yii::t('catalog', 'Choose'), '#', [
                        'data-action' => 'submit-city-parts',
                        'data-dismiss' => 'modal',
                        'class' => 'btn btn-small btn-blue-white'
                    ]) ?>
                    <?= Html::a(Yii::t('catalog', 'Clear'), '#', [
                        'data-action' => 'clear-city-parts',
                        'class' => 'clear-district'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>