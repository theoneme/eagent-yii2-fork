<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Property;

?>
<div class="container-fluid-short">
    <h2 class="text-center">
        <?= Yii::t('index', 'We are modern real estate portal') ?>
    </h2>
    <div class="text-center moz-subtitle"><?= Yii::t('index', 'We have combined technologies to make the process of renting real estate the easiest'); ?></div>
</div>
<div class="container">
    <div class="moz-blocks row">
        <div class="flex flex-v-center moz-item">
            <div class="col-md-6 col-md-push-6 moz-info moz-left">
                <div class="moz-title">
                    <div class="moz-green">
                        <?= Yii::t('index', 'To rent'); ?> —
                    </div>
                    <div>
                        <?= Yii::t('index', 'Property'); ?>
                    </div>
                </div>
                <p class="p-light-gray">
                    <?= Yii::t('index', 'You can not only place an advertisement for renting an apartment for free, but also use CRM systems for managing Agents and rent payments'); ?>
                </p>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_RENT]) ?>"
                   class="alias-pink">
                    <?= Yii::t('index', 'Place a rental announcement'); ?>
                    <i class="icon-right-arrow"></i>
                </a>
            </div>
            <div class="col-md-6 col-md-pull-6 moz-img">
                <?= Html::img("/images/moz2.png", ['alt' => Yii::t('index', 'To rent'), 'title' => Yii::t('index', 'To rent')]) ?>
            </div>

        </div>

        <div class="flex flex-v-center moz-item">
            <div class="col-md-6 moz-info moz-right"> <div class="moz-title">
                    <div class="moz-green">
                        <?= Yii::t('index', 'Rent'); ?> —
                    </div>
                    <div>
                        <?= Yii::t('index', 'Property'); ?>
                    </div>
                </div>
                <p class="p-light-gray">
                    <?= Yii::t('index', 'It is easy to choose the most suitable rental option on our website. We will help to conclude a contract at a favorable price'); ?>
                </p>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_RENT]) ?>"
                   class="alias-pink">
                    <?= Yii::t('index', 'Search condo or house for rent'); ?>
                    <i class="icon-right-arrow"></i>
                </a>
            </div>
            <div class="col-md-6 moz-img">
                <?= Html::img("/images/moz3.png", ['alt' => Yii::t('index', 'Rent'), 'title' => Yii::t('index', 'Rent')]) ?>
            </div>
        </div>
    </div>
</div>