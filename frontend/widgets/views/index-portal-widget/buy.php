<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Property;

?>
<div class="container-fluid-short">
    <h2 class="text-center">
        <?= Yii::t('index', 'We are modern real estate portal') ?>
    </h2>
    <div class="text-center moz-subtitle"><?= Yii::t('index', 'Technologies have made the process of selling and buying real estate simple and transparent'); ?></div>
</div>
<div class="container">
    <div class="moz-blocks row">
        <div class="flex flex-v-center moz-item">
            <div class="col-md-6 col-md-push-6 moz-info moz-left">
                <div class="moz-title">
                    <div class="moz-green">
                        <?= Yii::t('index', 'Buy'); ?> —
                    </div>
                    <div>
                        <?= Yii::t('index', 'Property'); ?>
                    </div>
                </div>
                <p class="p-light-gray">
                    <?= Yii::t('index', 'We have created a convenient search so that you can easily find the best offers for buying real estate. View them and offer your price'); ?>
                </p>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_SALE]) ?>"
                   class="alias-pink">
                    <?= Yii::t('index', 'Start choosing real estate'); ?>
                    <i class="icon-right-arrow"></i>
                </a>
            </div>
            <div class="col-md-6 col-md-pull-6 moz-img">
                <?= Html::img("/images/moz3.png", ['alt' => Yii::t('index', 'Buy'), 'title' => Yii::t('index', 'Buy')]) ?>
            </div>
        </div>
        <div class="flex flex-v-center moz-item">
            <div class="col-md-6 moz-info moz-right"><div class="moz-title">
                    <div class="moz-green">
                        <?= Yii::t('index', 'Sell'); ?> —
                    </div>
                    <div>
                        <?= Yii::t('index', 'Your property'); ?>
                    </div>
                </div>
                <p class="p-light-gray">
                    <?= Yii::t('index', 'Placing ads on the site will attract new buyers from around the world to your property. And our free CRM will allow you to work effectively with customers'); ?>
                </p>
                <a href="<?= Url::to(['/property/manage/create']) ?>" class="alias-pink">
                    <?= Yii::t('index', 'List your property for free'); ?>
                    <i class="icon-right-arrow"></i>
                </a>
            </div>
            <div class="col-md-6 moz-img">
                <?= Html::img("/images/moz2.png", ['alt' => Yii::t('index', 'Sell'), 'title' => Yii::t('index', 'Sell')]) ?>
            </div>
        </div>
    </div>
</div>