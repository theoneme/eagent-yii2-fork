<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 17:41
 */

use common\helpers\UtilityHelper;
use frontend\modules\crm\models\CrmMember;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var array $categorySlugs
 * @var array $user
 * @var CrmMember[] $crmMembers
 * @var boolean $isGuest
 * @var integer $currentProfile
 * @var integer $companyId
 * @var boolean $isCompany
 */

?>

    <div class="mobile-menu">
        <div class="mobile-menu-head pad20">
            <?php if ($isGuest === false) { ?>
                <div class="flex">
                    <a href="<?= Url::to(['/agent/agent/view', 'id' => $user['id']]) ?>">
                        <div class="mobile-menu-avatar text-center">
                            <?php if (!empty($user['thumb'])) {
                                echo Html::img($user['thumb'], ['alt' => $user['username'], 'title' => $user['username']]);
                            } else {
                                echo StringHelper::truncate($user['username'], 1, '');
                            } ?>
                        </div>
                    </a>
                    <div class="menu-infoprof">
                        <div class="mobile-menu-nick">
                            <div>
                                <a href="<?= Url::to(['/agent/agent/view', 'id' => $user['id']]) ?>">
                                    <?= $user['username'] ?>
                                </a>
                            </div>
                            <div class="mobile-balance">
                                <?= Html::a($user['balance'], ['/user/wallet/index'])?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <?= Html::a(Yii::t('main', 'Sign Up'), null, [
                    'class' => 'btn btn-small btn-green',
                    'data-toggle' => 'modal',
                    'data-target' => '#signup-modal'
                ]) ?>
            <?php } ?>
        </div>
        <div class="nav-subtitle"><?= Yii::t('main', 'Common') ?></div>
        <ul class="mobile-list text-left no-list">
            <li>
                <a href="<?= Url::to(['/request/catalog/index']) ?>">
                    <i class="icon-search1 mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Requests') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/page/tariff']) ?>">
                    <i class="icon-piggy-bank mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Tariffs') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/agent/catalog/index']) ?>">
                    <i class="icon-agent mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Agents') ?>
                </a>
            </li>
        </ul>

        <div class="nav-subtitle"><?= Yii::t('main', 'Sale') ?></div>
        <ul class="mobile-list text-left no-list">
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'sale']) ?>">
                    <i class="icon-skyline mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Flats') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => 'sale']) ?>">
                    <i class="icon-icon mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Houses') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['land'] ?? 'land', 'operation' => 'sale']) ?>">
                    <i class="icon-area mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Land') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['commercial-property'] ?? 'commercial-property', 'operation' => 'sale']) ?>">
                    <i class="icon-bank-building mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Commercial Property') ?>
                </a>
            </li>
        </ul>

        <div class="nav-subtitle"><?= Yii::t('main', 'Rent') ?></div>
        <ul class="mobile-list text-left no-list">
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'rent']) ?>">
                    <i class="icon-skyline mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Flats') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => 'rent']) ?>">
                    <i class="icon-icon mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Houses') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['land'] ?? 'land', 'operation' => 'rent']) ?>">
                    <i class="icon-area mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Land') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['commercial-property'] ?? 'commercial-property', 'operation' => 'rent']) ?>">
                    <i class="icon-bank-building mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Commercial Property') ?>
                </a>
            </li>
        </ul>

        <div class="nav-subtitle"><?= Yii::t('main', 'Buildings') ?></div>
        <ul class="mobile-list text-left no-list">
            <li>
                <a href="<?= Url::to(['/building/catalog/index', 'operation' => \common\models\Building::TYPE_DEFAULT_TEXT]) ?>">
                    <i class="icon-city mobile-menu-icon"></i>
                    <?= Yii::t('main', 'Secondary Buildings') ?>
                </a>
            </li>
            <li>
                <a href="<?= Url::to(['/building/catalog/index', 'operation' => \common\models\Building::TYPE_NEW_CONSTRUCTION_TEXT]) ?>">
                    <i class="icon-crane mobile-menu-icon"></i>
                    <?= Yii::t('main', 'New Constructions') ?>
                </a>
            </li>
        </ul>

        <?php if (!$isGuest) { ?>
            <div class="nav-subtitle"><?= Yii::t('main', 'My Announcements') ?></div>
            <ul class="mobile-list text-left no-list">
                <li>
                    <i class="icon-clipboards mobile-menu-icon"></i>
                    <a href="<?= Url::to(['/property/manage/create']) ?>">
                        <?= Yii::t('main', 'List your property') ?>
                    </a>
                </li>
                <li>
                    <i class="icon-clipboards-loop mobile-menu-icon"></i>
                    <a href="<?= Url::to(['/property/list/index']) ?>">
                        <?= Yii::t('main', 'My Properties') ?>
                    </a>
                </li>
            </ul>

            <div class="nav-subtitle"><?= Yii::t('main', 'My Requests') ?></div>
            <ul class="mobile-list text-left no-list">
                <li>
                    <i class="icon-unknown mobile-menu-icon"></i>
                    <a href="<?= Url::to(['/request/manage/create']) ?>">
                        <?= Yii::t('main', 'List your request') ?>
                    </a>
                </li>
                <li>
                    <i class="icon-research mobile-menu-icon"></i>
                    <a href="<?= Url::to(['/request/list/index']) ?>">
                        <?= Yii::t('main', 'My Requests') ?>
                    </a>
                </li>
            </ul>

            <div class="nav-subtitle"><?= Yii::t('main', 'My profile') ?></div>
            <ul class="mobile-list text-left no-list">
                <li>
                    <i class="icon-settings mobile-menu-icon"></i>
                    <a href="<?= Url::to(['/user/manage/update']) ?>"><?= Yii::t('main', 'Profile Settings') ?></a>
                </li>
                <li>
                    <i class="icon-multiple-users-silhouette mobile-menu-icon"></i>
                    <a href="<?= Url::to(['/company/list/index']) ?>"><?= Yii::t('main', 'My Companies') ?></a>
                </li>
                <li>
                    <i class="icon-logout mobile-menu-icon"></i>
                    <a href="<?= Url::to(['/auth/logout']) ?>"><?= Yii::t('main', 'Logout') ?></a>
                </li>
            </ul>

            <?php if (!empty($profiles) && count($profiles) > 1) { ?>
                <div class="nav-subtitle"><?= Yii::t('main', 'My Accounts') ?></div>
                <ul class="mobile-list text-left no-list">
                    <?php foreach ($profiles as $profile) { ?>
                        <li class="flex">
                            <div class="mobile-menu-other-account-avatar">
                                <?= $profile['companyLogo']
                                    ? Html::img($profile['companyLogo'])
                                    : '<i class="icon-man-user"></i>'
                                ?>
                            </div>
                            <div class="mobile-menu-nick">
                                <?= Html::a($profile['companyTitle'], [
                                    '/company/ajax/profile-switch'
                                ], [
                                    'style' => 'display: inline-block;',
                                    'data-action' => 'identity-switcher',
                                    'data-identity' => $profile['userId'],
                                    'class' => $currentProfile === $profile['userId'] ? 'font-bold' : null
                                ]) ?>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>

        <?php } ?>
        <div class="nav-subtitle"><?= Yii::t('main', 'Settings') ?></div>
        <ul class="mobile-list text-left no-list">
            <li>
                <i class="icon-earth-globe mobile-menu-icon"></i>
                <select name="app_language" class="app_language mobile-language">
                    <?php foreach (Yii::$app->params['supportedLocales'] as $locale => $code) {
                        echo Html::tag('option', Yii::t('main', Yii::$app->params['languages'][$locale], [], $locale), [
                            'value' => UtilityHelper::localeCurrentUrl($code),
                            'selected' => $locale === Yii::$app->language
                        ]);
                    } ?>
                </select>
            </li>
            <li>
                <i class="icon-dollar-bill mobile-menu-icon"></i>
                <select name="app_currency" class="app_currency mobile-language">
                    <?php foreach (Yii::$app->params['currencies'] as $k => $v) {
                        echo Html::tag('option', $v['sign'] . ' ' . $k, [
                            'value' => Url::current(['app_currency' => strtolower($k)]),
                            'selected' => $k === Yii::$app->params['app_currency']
                        ]);
                    } ?>
                </select>
            </li>
        </ul>
    </div>

<?php $script = <<<JS
    $(document).on('change', 'select[name=app_language], select[name=app_currency]', function() {
        window.location.href = $(this).val();
    });
JS;

$this->registerJs($script);