<h2 class="text-center"><?= Yii::t('index', 'Need help from local property sale experts?'); ?></h2>
<p class="text-center">
    <?= Yii::t('index', 'You can place an advertisement for the sale of real estate for free and indicate in the questionnaire that you need the help of an agent. We will select the one most effective for you'); ?>
</p>
<div class="text-center">
    <a class="btn btn-big btn-white-blue">
        <?= Yii::t('index', 'List your property for free'); ?>
    </a>
</div>