<h2 class="text-center"><?= Yii::t('index', 'Need help from local property rental agents?'); ?></h2>
<p class="text-center">
    <?= Yii::t('index', 'You can place a free application for rental property and we will select the best agents that will help you find the most profitable offers and help you issue a lease agreement'); ?>
</p>
<div class="text-center">
    <a class="btn btn-big btn-white-blue">
        <?= Yii::t('index', 'List your request for free'); ?>
    </a>
</div>