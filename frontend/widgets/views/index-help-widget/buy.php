<h2 class="text-center"><?= Yii::t('index', 'Need help from local property purchase experts?'); ?></h2>
<p class="text-center">
    <?= Yii::t('index', 'You can place a request for the purchase of real estate free of charge and we will select the best specialists who will help you to choose the best real estate offers and make a deal'); ?>
</p>
<div class="text-center">
    <a class="btn btn-big btn-white-blue">
        <?= Yii::t('index', 'List your request for free'); ?>
    </a>
</div>