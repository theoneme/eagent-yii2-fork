<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 21:57
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $properties
 * @var View $this
 */

?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 hidden-xs footer-column">
                <div class="footer-title">
                    <?= Yii::t('main', 'Properties for sale') ?>:
                </div>
                <ul class="footer-alias no-list">
                    <li>
                        <?= Html::a(
                            Yii::t('main', '{entity} for sale', [
                                'entity' => Yii::t('main', 'Flats')
                            ]),
                            ['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'sale'],
                            ['data-menu' => 'menu-new']
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            Yii::t('main', '{entity} for sale', [
                                'entity' => Yii::t('main', 'Houses')
                            ]),
                            ['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => 'sale'],
                            ['data-menu' => 'menu-new']
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            Yii::t('main', '{entity} for sale', [
                                'entity' => Yii::t('main', 'Commercial Property')
                            ]),
                            ['/property/catalog/index', 'category' => $categorySlugs['commercial_property'] ?? 'commercial_property', 'operation' => 'sale'],
                            ['data-menu' => 'menu-new']
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            Yii::t('main', 'New Constructions'),
                            ['/building/catalog/index', 'operation' => \common\models\Building::TYPE_NEW_CONSTRUCTION_TEXT],
                            ['data-menu' => 'menu-new']
                        ) ?>
                    </li>
                    <li>
                        <?= Html::a(
                            Yii::t('main', 'Secondary Buildings'),
                            ['/building/catalog/index', 'operation' => \common\models\Building::TYPE_DEFAULT_TEXT],
                            ['data-menu' => 'menu-new']
                        ) ?>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 footer-column">
                <div class="footer-title">
                    <?= Yii::t('main', 'Useful Links') ?>
                </div>
                <ul class="no-list footer-alias">
                    <li>
                        <h5>
                            <?= Html::a(Yii::t('main', 'Desktop version'), Url::current(['mode' => 'desktop']), [
                                'data-method' => 'post',
                            ]) ?>
                        </h5>
                    </li>
                    <li>
                        <h5>
                            <?= Html::a(Yii::t('main', 'Mobile version'), Url::current(['mode' => 'mobile']), [
                                'data-method' => 'post',
                            ]) ?>
                        </h5>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 hidden-xs footer-column">
                <div class="footer-title">
                    <?= Yii::t('main', 'Support') ?>
                </div>
                <ul class="no-list footer-alias">
                    <li>
                        <h5>
                            <a href="#">
                                <?= Yii::t('main', 'Contacts') ?>
                            </a>
                        </h5>
                    </li>
                    <li>
                        <h5>
                            <?= Html::a(Yii::t('main', 'Privacy policy'), ['/page/view', 'slug' => 'privacy-policy']) ?>
                        </h5>
                    </li>
                    <li>
                        <h5>
                            <?= Html::a(Yii::t('main', 'Terms of service'), ['/page/view', 'slug' => 'terms-of-service']) ?>
                        </h5>
                    </li>
                    <li>
                        <h5>
                            <?= Html::a(Yii::t('index', 'Worldwide Real Estate listings'), ['/location/list/locations']) ?>
                        </h5>
                    </li>
                    <li>
                        <h5>
                            <?= Html::a(
                                Yii::t('main', 'Tariffs'),
                                ['/page/tariff'],
                                ['data-menu' => 'menu-tariff']
                            ) ?>
                        </h5>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 footer-column">
                <div class="footer-title">
                    <?= Yii::t('main', 'Get in touch') ?>
                </div>
                <a class="footer-phone" href="tel:15188572903" title="Call Real Estate Company eAgent.me Inc">
                    <i class="icon-phone"></i>
                    +7 (912) 251-89-48
                </a>
                <a class="footer-mail" href="mailto:save@eagent.me" title="Call Real Estate Company eAgent.me Inc">
                    <i class="icon-envelope"></i>
                    info@eagent.me
                </a>
                <address>
                    <p>17100 Collins Ave Suite 215</p>
                    <p>Sunny Isles Beach, FL 33160</p>
                    <p>United States</p>
                </address>
                <div class="footer-title">
                    <?= Yii::t('main', 'Follow us') ?>
                </div>
                <div class="social">
                    <a class="text-center" href="https://www.facebook.com/eagent.me">
                        <i class="icon-facebook-logo"></i>
                    </a>
                    <a class="text-center" href="https://twitter.com/eagentme">
                        <i class="icon-twitter"></i>
                    </a>
                    <a class="text-center" href="https://plus.google.com/u/0/+EagentMe">
                        <i class="icon-google-plus"></i>
                    </a>
                    <a class="text-center" href="https://www.linkedin.com/company/eagent-me?trk=company_logo">
                        <i class="icon-linkedin"></i>
                    </a>
                    <a class="text-center" href="https://www.pinterest.com/eroomme/httpwwweagentme">
                        <i class="icon-pinterest"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="copyright text-center">&#169; 2018 eAgent.me, Inc</div>
    </div>
</footer>
