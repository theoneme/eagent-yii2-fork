<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.09.2018
 * Time: 19:04
 */

use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\plugins\AutoCompleteAsset::register($this);

?>

    <div class="modal fade modal-city" id="location-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?= Html::beginForm(['/location/ajax/set-location'], 'post', ['id' => 'city-form']) ?>
                    <div class="city-search input-group">
                        <?= Html::hiddenInput('location', null, ['id' => 'filter-city']) ?>
                        <?= Html::input('search', 'city-helper', null, [
                            'class' => 'form-control',
                            'id' => 'filter-city-helper',
                            'placeholder' => Yii::t('catalog', 'Address or ZIP code')
                        ]) ?>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="icon-search"></i></button>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                    <ul class="no-list city-switch">
                        <li class="active">
                            <a href="#cities" data-toggle="tab"> <?= Yii::t('index', 'Big cities') ?> </a>
                        </li>
                        <li>
                            <a href="#regions" data-toggle="tab"> <?= Yii::t('index', 'Regions') ?> </a>
                        </li>
                        <li>
                            <a href="#country" data-toggle="tab"> <?= Yii::t('index', 'Countries') ?> </a>
                        </li>
                    </ul>
                    <div class="tab-content tab-city">
                        <div class="city-content tab-pane fade in active" id="cities">
                            <div class="city-block">
                                <div class='modal-triple-column' data-role="big-city-container">

                                </div>
                            </div>
                        </div>
                        <div class="city-content tab-pane fade in" id="regions">
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    <div class="city-block">
                                        <div class='modal-double-column' data-role="region-container">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="city-block" data-role="city-container">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="city-content tab-pane fade in" id="country">
                            <div class="row">
                                <div class="col-md-4 col-sm-6">
                                    <div class="city-block" data-role="country-container">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="city-block" data-role="region-container">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="city-block" data-role="city-container">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="modal-footer text-center">
                <?= Html::submitButton(Yii::t('index', 'Remember'), ['class' => 'btn btn-white-blue', 'name' => 'contact-button']) ?>
            </div>-->
            </div>
        </div>
    </div>

<?php $countryUrl = Url::to(['/location/ajax/countries']);
$regionUrl = Url::to(['/location/ajax/regions']);
$cityUrl = Url::to(['/location/ajax/cities']);
$bigCityUrl = Url::to(['/location/ajax/big-cities']);
$setLocationUrl = Url::to(['/location/ajax/set-location']);
$citySearchUrl = Url::to(['/location/ajax/locations']);

$script = <<<JS
    $('#location-modal').LocationModal({
        countryUrl: '{$countryUrl}',
        regionUrl: '{$regionUrl}',
        cityUrl: '{$cityUrl}',
        bigCityUrl: '{$bigCityUrl}',
        setLocationUrl: '{$setLocationUrl}'
    });

    $('#city-form').on('submit', function() {
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
            if(result.success === true) {
                window.location.replace(result.location);
            }
        });
        
        return false;
    });

    new AutoComplete({
        selector: "#filter-city-helper",
        autoFocus: false,
        minChars: 3,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.get('{$citySearchUrl}', { request: request }, function(data) { 
                response(data);
            });     
        },
        renderItem: function (item, search){ 
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-title="' + item.title + '" data-slug="' + item.slug + '">'
                + item.title.replace(re, "<b>$1</b>")
                + '</div>';
        },
        onSelect: function(e, term, item) {
            $("#filter-city").val(item.getAttribute('data-slug'));
            $("#filter-city-helper").val(item.getAttribute('data-title')).prop('disabled', true);
            $('#city-form').submit();
        }
    });
JS;

$this->registerJs($script);
