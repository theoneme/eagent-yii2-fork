<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2018
 * Time: 13:26
 */

\frontend\assets\ReportAsset::register($this);

?>

<div id="report-modal-container" class="new-modal">

</div>

<?php $script = <<<JS
    $('#report-modal-container').ReportModal();
JS;

$this->registerJs($script);