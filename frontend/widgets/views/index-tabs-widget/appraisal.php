<?php
use yii\helpers\Html;

/* @var string $address
 * @var boolean $isGuest
 * */

?>
<h1 class="banner-title text-center">
    <?= Yii::t('index', 'To rent a property in {city}', [
        'city' => Html::a($address, '#', [
            'data-action' => 'change-location',
            'data-target' => '#location-modal',
            'data-toggle' => 'modal',
            'class' => 'banner-location-change'
        ])
    ]) ?>
</h1>
<div class="banner-subtitle text-center">
    <?= Yii::t('index', 'Free announcement placement. <br> Assistance in registration of the lease agreement') ?>
</div>
<div class="input-group search text-center" style="border: none">
    <?php if ($isGuest === true) { ?>
        <?= Html::a(Yii::t('index', 'List your property for free'), null, [
            'data-toggle' => 'modal',
            'data-target' => '#signup-modal',
            'class' => 'btn btn-big btn-green'
        ])?>
    <?php } else { ?>
        <?= Html::a(Yii::t('index', 'List your property for free'), ['/property/manage/create'], [
            'class' => 'btn btn-big btn-green'
        ])?>
    <?php } ?>
</div>