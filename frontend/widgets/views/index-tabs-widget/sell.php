<?php
use yii\helpers\Html;

/**
 * @var string $address
 * @var boolean $isGuest
 */
?>
<h1 class="banner-title text-center">
    <?= Yii::t('index', 'Sell property in {city}', [
        'city' => Html::a($address, '#', [
            'data-action' => 'change-location',
            'data-target' => '#location-modal',
            'data-toggle' => 'modal',
            'class' => 'banner-location-change'
        ])
    ]) ?>
</h1>
<div class="banner-subtitle text-center">
    <?= Yii::t('index', 'Free announcement placement. <br> Find a property buyer') ?>
</div>
<div class="input-group search text-center" style="border: none">
   <!-- <?/*= Html::input('text', 'search', null, [
        'placeholder' => Yii::t('index', 'Enter your home address'),
        'class' => 'form-control'
    ]) ?>
    <div class="input-group-addon">
        <a class="search-but" href="#"><?= Yii::t('index', 'Search') */?></a>
    </div>-->
    <?php if ($isGuest === true) { ?>
        <?= Html::a(Yii::t('index', 'List your property for free'), null, [
            'data-toggle' => 'modal',
            'data-target' => '#signup-modal',
            'class' => 'btn btn-big btn-green'
        ])?>
    <?php } else { ?>
        <?= Html::a(Yii::t('index', 'List your property for free'), ['/property/manage/create'], [
            'class' => 'btn btn-big btn-green'
        ])?>
    <?php } ?>
</div>