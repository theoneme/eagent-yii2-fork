<?php

use yii\helpers\Html;

/**
 * @var string $address
 * @var string $currentCity
 */

?>
    <h1 class="banner-title text-center">
        <?= Yii::t('index', 'Rent property in {city}', [
            'city' => Html::a($address, '#', [
                'data-action' => 'change-location',
                'data-target' => '#location-modal',
                'data-toggle' => 'modal',
                'class' => 'banner-location-change'
            ])
        ]) ?>
    </h1>
    <div class="banner-subtitle text-center">
        <?= Yii::t('index', 'User-friendly search. Verified <br>  Rental Property Listings') ?>
    </div>
<?= Html::beginForm(['/property/catalog/search'], 'get', ['id' => 'banner-city-form']) ?>
    <div class="input-group search">
        <?= Html::hiddenInput('operation', \common\models\Property::OPERATION_RENT) ?>
        <?= Html::hiddenInput('zoom', null, ['id' => 'banner-filter-zoom']) ?>
        <?= Html::hiddenInput('box', null, ['id' => 'banner-filter-box']) ?>
        <?= Html::hiddenInput('category', 'flats') ?>
        <?= Html::input('search', 'city-helper', null, [
            'class' => 'form-control',
            'id' => 'banner-filter-city-helper',
            'placeholder' => Yii::t('index', 'Enter city, region or country')
        ]) ?>
        <div class="input-group-addon">
            <button type="submit" class="search-but"><?= Yii::t('index', 'Search') ?></button>
        </div>
    </div>
<?= Html::endForm() ?>

<?php $script = <<<JS
    let autocomplete = new google.maps.places.Autocomplete(document.getElementById('banner-filter-city-helper'), {
        types: ['geocode']
    });
    autocomplete.addListener('place_changed', function() {
        let place = autocomplete.getPlace(),
            neLat = place.geometry.viewport.getNorthEast().lat(),
            neLng = place.geometry.viewport.getNorthEast().lng(),
            swLat = place.geometry.viewport.getSouthWest().lat(),
            swLng = place.geometry.viewport.getSouthWest().lng();
        
        let box = neLat + ',' + neLng + ';' + swLat + ',' + swLng;
        $('#banner-filter-box').val(box);
        $('#banner-filter-zoom').val(13);
    });
    
    $('#banner-city-form').on('submit', function() {
        $(this).find('input').filter(function() {console.log(this);
            return !this.value;
        }).attr('disabled', 'disabled');
        $('#banner-filter-city-helper').attr('disabled', 'disabled');
        
        return true;
    });
JS;

$this->registerJs($script);