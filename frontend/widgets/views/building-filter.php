<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:16
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $catalogView
 * @var array $filters
 * @var string $sort
 * @var string $address
 * @var array $params
 * @var integer $gridSize
 */

?>

<?= Html::beginForm(Url::canonical(), 'get', [
    'id' => 'filter-form',
]); ?>
<?= Html::hiddenInput('box', $params['box'] ?? null, ['id' => 'filter-box']) ?>
<?= Html::hiddenInput('district', $params['district'] ?? null, ['id' => 'filter-district']) ?>
<?= Html::hiddenInput('microdistrict', $params['microdistrict'] ?? null, ['id' => 'filter-microdistrict']) ?>
<?= Html::hiddenInput('zoom', $params['zoom'] ?? null, ['id' => 'filter-zoom']) ?>
<div class="filters">
    <div class="loading-overlay hidden"></div>
    <div class="filter-search">
        <div class="input-group search text-center">
            <?= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
            <?= Html::input('search', 'city-helper', $address, [
                'class' => 'form-control input-grey',
                'id' => 'filter-city-helper',
                'placeholder' => Yii::t('catalog', 'Enter city, region or country')
            ]) ?>
            <a href="#" class="input-group-addon"><i class="icon-search"></i></a>
            <div class="district-form text-left hidden">
                <div class="district-inner">
                    <p class="">
                        <?= Yii::t('catalog', 'Start typing and select one of the options') ?>
                    </p>
                    <div class="autocomplete-loc-suggestions">

                    </div>
                </div>
                <div class="district-open">
                    <a class="flex space-between" href="#" data-action="load-city-parts" data-toggle="modal"
                       data-target="#city-parts">
                        <span><?= Yii::t('catalog', 'Districts and microdistricts') ?></span>
                        <i class="icon-next"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <?php $typeFilter = ArrayHelper::remove($filters, 'operation'); ?>
    <?php if ($typeFilter) { ?>
        <div class="filter-item filter-types">
            <div class="filter-name" data-action="expand-filter">
                <div class="filter-types-icons">
                    <div class="filter-circle filter-circle-<?= $typeFilter['values'][$typeFilter['checked']]['circle'] ?>"></div>
                </div>
                <div class="filter-type-name"><?= $typeFilter['values'][$typeFilter['checked']]['title'] ?></div>
            </div>
            <div class="filter-box">
                <?php foreach ($typeFilter['values'] as $key => $category) { ?>
                    <div class="filter-type-group">
                        <div class="filter-type-group-name">
                            <?= Html::radio('operation', $typeFilter['checked'] === $key, [
                                'id' => "cat-{$key}",
                                'value' => $key,
                                'class' => 'radio-checkbox'
                            ]) ?>
                            <label for="<?= "cat-{$key}" ?>">
                                <span class="filter-circle filter-circle-<?= $category['circle'] ?>"></span>
                                <span class="ftgn-title"><?= $category['title'] ?></span>
                            </label>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <!--        --><?php //if (!empty($filters)) { ?>
    <div class="filter-item filter-more">
        <div class="filter-name" data-action="show-more-filters">
            <div class="filter-type-name">
                <i class="icon-filter-tool-black-shape"></i>
                <?= Yii::t('catalog', 'More') ?>
            </div>
        </div>
    </div>
    <!--        --><?php //} ?>

    <div class="filter-item filter-switch">
        <?= Html::a('<i class="icon-menu"></i>', '#', [
            'class' => 'switch-items ' . ($catalogView === 'grid' ? 'active' : ''),
            'data-view' => 'grid',
            'data-action' => 'change-catalog-view',
            'data-url' => Url::current([0 => '/building/catalog/index', 'box' => null, 'zoom' => null])
        ]) ?>
    </div>
    <div class="filter-item filter-switch">
        <?= Html::a('<i class="icon-list"></i>', '#', [
            'class' => 'switch-items ' . ($catalogView === 'list' ? 'active' : ''),
            'data-view' => 'list',
            'data-action' => 'change-catalog-view',
            'data-url' => Url::current([0 => '/building/catalog/index', 'box' => null, 'zoom' => null])
        ]) ?>
    </div>
    <div class="filter-item filter-switch">
        <?= Html::a('<i class="icon-gps"></i>', '#', [
            'class' => 'switch-items ' . ($catalogView === 'map' ? 'active' : ''),
            'data-view' => 'map',
            'data-action' => 'change-catalog-view',
//                'data-url' => Url::current(['box' => null, 'zoom' => null])
        ]) ?>
    </div>
</div>

<div class="clearfix">
    <div class="col-md-<?= $gridSize ?> col-sm-12 catalog-left">
        <div class="params-filter">
            <div class="params-filter-content">
                <div class="row">
                    <?php foreach ($filters as $key => $filter) { ?>
                        <div class="col-md-<?= $gridSize > 6 ? 4 : 6 ?> col-sm-6">
                            <?= $this->render("filter/{$filter['type']}", [
                                'filter' => $filter,
                                'key' => $key
                            ]) ?>
                        </div>
                    <?php } ?>
                    <div class="col-md-12">
                        <?= Html::submitInput(Yii::t('catalog', 'Show'), ['class' => 'btn btn-big btn-blue-white']) ?>
                        <?= Html::a(Yii::t('catalog', 'Reset'), '#', ['class' => 'btn btn-big btn-white-blue', 'data-action' => 'reset-filter']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= Html::endForm() ?>

<?= $this->render('filter/city-part-modal') ?>

<?php $script = <<<JS
    $('body').on('click', function (e) {
        if (!$(e.target).closest(".filter-search").length) {
            $('.district-form').addClass('d-none');
        }
        
        if (!$(e.target).closest(".filters").length) {
            $('.filter-item').removeClass('open');
        }
    })
JS;
$this->registerJs($script); ?>
