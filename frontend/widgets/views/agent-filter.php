<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:16
 */

use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\plugins\AutoCompleteAsset::register($this);

/**
 * @var string $catalogView
 * @var array $filters
 * @var string $username
 */

?>

<div class="agent-filter row">
    <?= Html::beginForm(['/agent/catalog/search'], 'get', [
        'id' => 'filter-form',
        'class' => 'col-md-12',
    ]); ?>
        <div class="form-group agent-filter-block">
            <?= Html::label(Yii::t('catalog', 'Location'), 'filter-city')?>

            <div class="input-group">
                <?= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
                <?= Html::input('search', 'city-helper', null, [
                    'class' => 'form-control',
                    'id' => 'filter-city-helper',
                    'placeholder' => Yii::t('catalog', 'Enter city, region or country')
                ]) ?>
                <a href="#" class="input-group-addon"><i class="icon-search"></i></a>
            </div>
        </div>
        <div class="form-group agent-filter-block">
            <?= Html::label(Yii::t('catalog', 'Name'), 'filter-name')?>
            <div class="input-group">
                <?= Html::input('search', 'username', $username, [
                    'class' => 'form-control',
                    'id' => 'filter-name',
                    'disabled' => true,
                    'placeholder' => Yii::t('catalog', 'Agent name')
                ]) ?>
                <a class="input-group-addon form-submit"><i class="icon-search"></i></a>
            </div>
        </div>
    <?= Html::endForm() ?>
</div>

<?php $baseUrl = Url::to(['/property/catalog/search']);
$catalogViewUrl = Url::to(['/catalog/view']);
$cityUrl = Url::to(['/location/ajax/locations']);

$script = <<<JS
    new AutoComplete({
        selector: "#filter-city-helper",
        autoFocus: false,
        minChars: 3,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.get('{$cityUrl}', { request: request }, function(data) { 
                response(data);
            });     
        },
        renderItem: function (item, search){ 
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-title="' + item.title + '" data-slug="' + item.slug + '">'
                + item.title.replace(re, "<b>$1</b>")
                + '</div>';
        },
        onSelect: function(e, term, item) {
            $("#filter-city").val(item.getAttribute('data-slug'));
            $("#filter-city-helper").val(item.getAttribute('data-title')).prop('disabled', true);
            $('#filter-form').submit();
        }
    });
JS;

$this->registerJs($script);