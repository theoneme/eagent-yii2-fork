<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class ServiceFilter
 * @package frontend\widgets
 */
class ServiceFilter extends Widget
{
    /**
     * @var string
     */
    public $catalogView;
    /**
     * @var array
     */
    public $queryParams;
    /**
     * @var array
     */
    public $locationData;
    /**
     * @var array
     */
    public $filters;

    /**
     * RequestFilter constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->filters = [];
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        return $this->render('property-filter', [
            'filters' => $this->filters,
            'catalogView' => $this->catalogView,
            'address' => $this->locationData['address']
        ]);
    }
}