<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 18.03.2019
 * Time: 13:56
 */

namespace frontend\widgets;
use Yii;
use yii\base\Widget;

/**
 * Class IndexPortalWidget
 * @package frontend\widgets
 */
class IndexPortalWidget extends Widget
{
    public const OPERATION_SELL = 'sell';
    public const OPERATION_BUY = 'buy';
    public const OPERATION_RENT = 'rent';
    //public const OPERATION_TO_RENT = 'toRent';

    /**
     * @var string
     */
    public $operation;

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $operation = !empty($this->operation) && in_array($this->operation, self::getOperationList()) ? $this->operation : self::OPERATION_BUY;
        $links = [
            self::OPERATION_BUY => [
                'id' => 'buy',
                'label' => Yii::t('index', 'Buy'),
            ],
            self::OPERATION_SELL => [
                'id' => 'sell',
                'label' => Yii::t('index', 'Sell'),
            ],
            self::OPERATION_RENT => [
                'id' => 'rent',
                'label' => Yii::t('index', 'Rent'),
            ],
            /*self::OPERATION_TO_RENT => [
                'id' => 'toRent',
                'label' => Yii::t('index', 'ToRent'),
            ],*/
        ];
        $contentTemplates = [
            self::OPERATION_BUY => 'buy',
            self::OPERATION_SELL => 'sell',
            self::OPERATION_RENT => 'rent',
            //self::OPERATION_TO_RENT => 'toRent',
        ];
        $content = $this->render("index-portal-widget/{$contentTemplates[$operation]}.php");

        return $this->render('index-portal-widget', [
            'links' => $links,
            'operation' => $operation,
            'content' => $content
        ]);
    }

    /**
     * @return array
     */
    public static function getOperationList()
    {
        return [
            self::OPERATION_SELL,
            self::OPERATION_BUY,
            self::OPERATION_RENT,
            //self::OPERATION_TO_RENT
        ];
    }
}