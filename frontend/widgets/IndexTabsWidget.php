<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;

/**
 * Class IndexTabsWidget
 * @package frontend\widgets
 */
class IndexTabsWidget extends Widget
{
    public const OPERATION_SELL = 'sell';
    public const OPERATION_BUY = 'buy';
    public const OPERATION_RENT = 'rent';
    public const OPERATION_APPRAISAL = 'appraisal';

    /**
     * @var string
     */
    public $operation;
    /**
     * @var string
     */
    public $address = '';

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $operation = !empty($this->operation) && in_array($this->operation, self::getOperationList()) ? $this->operation : self::OPERATION_BUY;
        $links = [
            self::OPERATION_BUY => [
                'id' => 'buy',
                'label' => Yii::t('index', 'Buy'),
            ],
            self::OPERATION_SELL => [
                'id' => 'sell',
                'label' => Yii::t('index', 'Sell'),
            ],
            self::OPERATION_RENT => [
                'id' => 'rent',
                'label' => Yii::t('index', 'Rent'),
            ],
            self::OPERATION_APPRAISAL => [
                'id' => 'estimate',
                'label' => Yii::t('index', 'Estimate'),
            ],
        ];
        $contentTemplates = [
            self::OPERATION_BUY => 'buy',
            self::OPERATION_SELL => 'sell',
            self::OPERATION_RENT => 'rent',
            self::OPERATION_APPRAISAL => 'appraisal',
        ];
        $currentCity = Yii::$app->request->get('app_city');
        $content = $this->render("index-tabs-widget/{$contentTemplates[$operation]}.php", [
            'address' => $this->address,
            'isGuest' => Yii::$app->user->isGuest,
            'currentCity' => $currentCity
        ]);

        return $this->render('index-tabs-widget', [
            'links' => $links,
            'operation' => $operation,
            'content' => $content
        ]);
    }

    /**
     * @return array
     */
    public static function getOperationList()
    {
        return [
            self::OPERATION_SELL,
            self::OPERATION_BUY,
            self::OPERATION_RENT,
            self::OPERATION_APPRAISAL
        ];
    }
}