<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 22:17
 */

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class RequestFilter
 * @package frontend\widgets
 */
class RequestFilter extends Widget
{
    /**
     * @var string
     */
    public $catalogView;
    /**
     * @var array
     */
    public $queryParams;
    /**
     * @var array
     */
    public $locationData;
    /**
     * @var array
     */
    public $filters;
    /**
     * @var int
     */
    public $gridSize = 8;
    /**
     * @var integer
     */
    public $resultCount;

    /**
     * RequestFilter constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->filters = [];
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        $address = $this->locationData['address'];

        return $this->render('property-filter', [
            'filters' => $this->filters,
            'catalogView' => $this->catalogView,
            'address' => $address,
            'sort' => null,
            'gridSize' => $this->gridSize,
            'resultCount' => $this->resultCount
        ]);
    }
}