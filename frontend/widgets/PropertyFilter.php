<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 22:17
 */

namespace frontend\widgets;

use common\interfaces\FilterInterface;
use common\interfaces\filters\PropertyFilterInterface;
use common\models\AttributeFilter;
use Yii;
use yii\base\Widget;

/**
 * Class PropertyFilter
 * @package frontend\widgets
 */
class PropertyFilter extends Widget
{
    /**
     * @var string
     */
    public $template = 'property-filter';
    /**
     * @var string
     */
    public $catalogView;
    /**
     * @var array
     */
    public $queryParams;
    /**
     * @var array
     */
    public $locationData;
    /**
     * @var array
     */
    public $filters;
    /**
     * @var int
     */
    public $gridSize = 8;
    /**
     * @var integer
     */
    public $resultCount;
    /**
     * @var FilterInterface
     */
    private $_filterService;

    /**
     * PropertyFilter constructor.
     * @param PropertyFilterInterface $propertyFilterService
     * @param array $config
     */
    public function __construct(PropertyFilterInterface $propertyFilterService, array $config = [])
    {
        parent::__construct($config);
        $this->_filterService = $propertyFilterService;
    }

    /**
     * Prepare filter array
     */
    public function build()
    {
        $this->filters = $this->_filterService->buildFilterArray($this->queryParams);
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        $address = $this->locationData['address'];

        return $this->render($this->template, [
            'filters' => $this->filters,
            'catalogView' => $this->catalogView,
            'address' => $address,
            'sort' => $this->getSort(),
            'params' => $this->queryParams,
            'gridSize' => $this->gridSize,
            'resultCount' => $this->resultCount
        ]);
    }

    /**
     * @return array
     */
    public function getSortData()
    {
        return [
            'values' => [
                'created_at:desc' => Yii::t('catalog', 'Newest first'),
                'default_price:asc' => Yii::t('catalog', 'Cheap first'),
                'default_price:desc' => Yii::t('catalog', 'Expensive first'),
            ],
            'title' => Yii::t('catalog', 'Sort by'),
            'checked' => $this->queryParams['sort'] ?? 'created_at:desc',
            'type' => AttributeFilter::TYPE_DROPDOWN
        ];
    }

    /**
     * @return mixed|null
     */
    protected function getSort()
    {
        return $this->queryParams['sort'] ?? null;
    }
}