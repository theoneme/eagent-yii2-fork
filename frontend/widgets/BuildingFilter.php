<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.08.2018
 * Time: 22:17
 */

namespace frontend\widgets;

use common\interfaces\FilterInterface;
use common\interfaces\filters\BuildingFilterInterface;
use yii\base\Widget;

/**
 * Class BuildingFilter
 * @package frontend\widgets
 */
class BuildingFilter extends Widget
{
    /**
     * @var string
     */
    public $template = 'building-filter';
    /**
     * @var string
     */
    public $catalogView;
    /**
     * @var array
     */
    public $queryParams;
    /**
     * @var array
     */
    public $locationData;
    /**
     * @var array
     */
    public $filters;
    /**
     * @var int
     */
    public $gridSize = 8;
    /**
     * @var integer
     */
    public $resultCount;
    /**
     * @var FilterInterface
     */
    private $_filterService;

    /**
     * BuildingFilter constructor.
     * @param BuildingFilterInterface $buildingFilterService
     * @param array $config
     */
    public function __construct(BuildingFilterInterface $buildingFilterService, array $config = [])
    {
        parent::__construct($config);
        $this->_filterService = $buildingFilterService;
    }

    /**
     * Prepare filter array
     */
    public function build()
    {
        $this->filters = $this->_filterService->buildFilterArray($this->queryParams);
    }

    /**
     * @return string
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\db\Exception
     */
    public function run()
    {
        $address = $this->locationData['address'];

        return $this->render($this->template, [
            'filters' => $this->filters,
            'catalogView' => $this->catalogView,
            'address' => $address,
            'params' => $this->queryParams,
            'gridSize' => $this->gridSize,
            'resultCount' => $this->resultCount
        ]);
    }
}