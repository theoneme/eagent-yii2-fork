<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 04.04.2017
 * Time: 18:15
 */

namespace frontend\widgets;

use common\components\CurrencyHelper;
use common\dto\advanced\UserAdvancedDTO;
use common\interfaces\repositories\UserRepositoryInterface;
use common\interfaces\repositories\UserWalletRepositoryInterface;
use common\models\User;
use common\services\CategoryTreeService;
use frontend\services\CompaniesByMemberService;
use Yii;
use yii\base\Widget;

/**
 * Class MobileMenu
 * @package frontend\widgets
 */
class MobileMenu extends Widget
{
    /**
     * @var CompaniesByMemberService
     */
    private $_companiesByMemberService;
    /**
     * @var UserRepositoryInterface
     */
    private $_userRepository;
    /**
     * @var UserWalletRepositoryInterface
     */
    private $_userWalletRepository;
    /**
     * @var string
     */
    public $template = 'mobile-menu';

    /**
     * HeaderWidget constructor.
     * @param array $config
     * @param UserRepositoryInterface $userRepository
     * @param CompaniesByMemberService $companiesByMemberService
     */
    public function __construct(array $config = [], UserRepositoryInterface $userRepository, UserWalletRepositoryInterface $userWalletRepository, CompaniesByMemberService $companiesByMemberService)
    {
        parent::__construct($config);

        $this->_companiesByMemberService = $companiesByMemberService;
        $this->_userRepository = $userRepository;
        $this->_userWalletRepository = $userWalletRepository;
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        if (array_key_exists('profiles', Yii::$app->params['runtime'])) {
            $params = Yii::$app->params['runtime']['profiles'];
        } else {
            $categorySlugs = array_map(
                function ($var) {
                    return $var[Yii::$app->language] ?? null;
                },
                Yii::$app->cacheLayer->getCategoryAliasCache('ru-RU')
            );

            /* @var CategoryTreeService $categoryTreeService*/
            $categoryTreeService = Yii::$container->get(CategoryTreeService::class);
            $categoryTree = $categoryTreeService->getTree();

            $isGuest = Yii::$app->user->isGuest;
            $params = [
                'categorySlugs' => $categorySlugs,
                'categoryTree' => $categoryTree,
                'isGuest' => $isGuest,
            ];

            /** @var User $user */
            $user = Yii::$app->user->identity;
            if ($user !== null) {
                $companyUserId = $user['company_user_id'];
                if ($companyUserId) {
                    $companyUser = $this->_userRepository->with(['company'])->findOneByCriteria(['user.id' => $user['company_user_id']]);
                    $companyDTO = new UserAdvancedDTO($companyUser);
                    $userDTO = new UserAdvancedDTO($user);

                    $userData = $userDTO->getData();
                    $companyData = $companyDTO->getData();
                    $params['user'] = $companyData;
                    $params['isCompany'] = true;
                    $params['companyId'] = $companyUser['company']['id'];
                    $params['profiles'][] = [
                        'companyLogo' => $userData['thumb'],
                        'companyTitle' => $userData['username'],
                        'userId' => null
                    ];
                } else {
                    $userDTO = new UserAdvancedDTO($user);
                    $userData = $userDTO->getData();
                    $params['user'] = $userData;
                    $params['isCompany'] = false;
                    $params['profiles'][] = [
                        'companyLogo' => $userData['thumb'],
                        'companyTitle' => $userData['username'],
                        'userId' => null
                    ];
                }

                $wallets = $this->_userWalletRepository->findManyByCriteria(['user_id' => $params['user']['id']], true);
                $params['user']['balance'] = CurrencyHelper::format(
                    Yii::$app->params['app_currency'],
                    array_sum(
                        array_map(function ($var) {
                            return CurrencyHelper::convert($var['currency_code'], Yii::$app->params['app_currency'], $var['balance']);
                        }, $wallets)
                    )
                );

                $params['crmMembers'] = Yii::$app->getModule('crm')->getCrmMembers();
                $companies = $this->_companiesByMemberService->getCompanies(['user_id' => $user['id']], [
//                    'limit' => 3,
                    'pagination' => false
                ]);
                $params['profiles'] = array_merge($params['profiles'], $companies['items']);
                $params['currentProfile'] = $user['company_user_id'];
            }

            Yii::$app->params['runtime']['profiles'] = $params;
        }

        return $this->render($this->template, $params);
    }
}