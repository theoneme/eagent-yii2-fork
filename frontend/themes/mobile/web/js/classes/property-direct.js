/**
 * Created by Devour on 28.08.2018.
 */

(function ($) {
    $.fn.PropertyDirect = function (options) {
        var DirectLoader = function (options, modalId) {
            this.options = $.extend({
                selector: '[data-action=load-modal-property]',
                mainSlider: '[data-role=main-slider]',
                similarSlider: '[data-role=similar-slider]',
                sideContactFormSelector: '#side-contact-agent-form',
                bottomContactFormSelector: '#bottom-contact-agent-form',
                fancyCaptionContainer: '#fancy-caption-container',
                fancyTitle: '#fancy-title',

                additionalModalSelector: '#dynamic-modal',
                sendMessageSelector: '[data-action=send-message]',
                showContactsSelector: '[data-action=show-contacts]',
                offerPriceSelector: '[data-action=offer-price]',
                requestShowcaseSelector: '[data-action=request-showcase]',
                requestMortgageSelector: '[data-action=request-mortgage]'
            }, options);

            this.additionalModal = $(this.options.additionalModalSelector);
            this.meta = {
                location: window.location.href,
                title: $('title'),
                description: $('meta[name=description]'),
                keywords: $('meta[name=keywords]')
            };
            this.metaData = {};
            this.state = {state: true};

            this.attachHandlers();
            this.initPlugins();
        };

        DirectLoader.prototype = {
            attachHandlers: function () {
                let that = this;
                $(document).on('click', this.options.sendMessageSelector
                    + ',' + this.options.showContactsSelector
                    + ',' + this.options.offerPriceSelector
                    + ',' + this.options.requestMortgageSelector
                    + ',' + this.options.requestShowcaseSelector, function () {
                    that.sendRequest($(this).attr('href'), 'get', {}, function (result) {
                        if (result.success === true) {
                            that.additionalModal.find('.modal-content').html(result.html);
                        }
                    });

                    return false;
                });
            },

            initPlugins: function() {
                let that = this;
                var rtlSlider = false;
                if($('body').hasClass('rtl')) {
                    rtlSlider = true;
                }
                $(this.options.mainSlider).slick({
                    dots: false,
                    arrows: true,
                    autoplay: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    variableWidth: true,
                    rtl: rtlSlider,
                    responsive: [{
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            adaptiveHeight: true,
                            variableWidth: false
                        }
                    }]
                });

                $(this.options.similarSlider).slick({
                    dots: false,
                    arrows: true,
                    autoplay: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    rtl: rtlSlider,
                    responsive: [{
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            adaptiveHeight: true,
                            variableWidth: false
                        }
                    }]
                });

                $('[data-fancybox="gallery"]').fancybox({
                    baseClass: 'fancybox-advanced',
                    hash: false,
                    buttons: [
                        'title',
                        'close'
                    ],
                    infobar: true,
                    beforeShow : function( instance, current ) {
                        $('.fancybox-caption').append($(that.options.fancyCaptionContainer + '> div'));
                    },
                    beforeClose : function() {
                        $(that.options.fancyCaptionContainer).append($('.fancybox-caption > div'));
                    },
                    thumbs : false,
                    mobile: {
                        infobar: false,
                        caption : false,
                        thumbs: false
                    },
                    btnTpl: {
                        title:'<p class="fancybox-button--title">' +
                        $(that.options.fancyTitle).text() +
                        '</p>',
                    },
                });

                $('.more-less').trigger('show');
            },

            sendRequest: function (address, method, data, callback) {
                $.ajax({
                    url: address,
                    type: method,
                    data: data,
                    success: function (result) {
                        if (callback) {
                            callback(result);
                        } else {

                        }
                    }
                });
            },
        };

        return new DirectLoader(options, this.attr('id'));
    }
})(jQuery);