<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 22.03.2019
 * Time: 12:52
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>

    <div class="menu-bottom-block menu-bottom-catalog">
        <ul class="no-list space-between flex menu-bottom">
            <li>
                <a href="#" id="menu"><i class="icon-bottom i-menu"></i><?= Yii::t('catalog', 'Menu') ?></a>
            </li>
            <li>
                <a href="#" id="filter"><i class="icon-bottom i-controls"></i><?= Yii::t('catalog', 'Filter') ?></a>
            </li>
            <li>
                <a href="<?= Url::to(['/property/manage/create']) ?>">
                    <i class="icon-bottom i-write-letter"></i>
                    <?= Yii::t('catalog', 'Advertisement' ) ?>
                </a>
            </li>
        </ul>
    </div>
