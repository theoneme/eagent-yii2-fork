<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.03.2019
 * Time: 17:50
 */

use common\models\Property;
use frontend\widgets\mobile\BottomPropertyMenu;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $object
 * @var string $entity
 * @var string $previousUrl
 */

?>

    <div class="menu-bottom-block menu-bottom-property">
        <ul class="no-list flex menu-bottom space-between">
            <li>
                <a href="<?= $previousUrl ?>">
                    <i class="icon-bottom i-right-arrow i-back"></i>
                    <?= Yii::t('property', 'Back') ?>
                </a>
            </li>
            <?php if ($entity === BottomPropertyMenu::ENTITY_PROPERTY) { ?>
                <li>
                    <?= Html::a('<i class="icon-bottom i-chat"></i>' . Yii::t('property', 'Send'), [
                        '/property/ajax/send-message',
                        'entity_id' => $object['id']
                    ], [
                        'data-action' => 'send-message',
                        'data-target' => '#dynamic-modal',
                        'data-toggle' => 'modal'
                    ]) ?>
                </li>
                <li>
                    <?= Html::a('<i class="icon-bottom i-phone"></i>' . Yii::t('property', 'Call'), [
                        '/property/ajax/show-contacts',
                        'entity_id' => $object['id'],
                        'owner_id' => $object['user_id']
                    ], [
                        'data-action' => 'show-contacts',
                        'data-target' => '#dynamic-modal',
                        'data-toggle' => 'modal'
                    ]) ?>
                </li>
            <?php } ?>
            <li>
                <a href="#"><i class="icon-bottom i-share"></i><?= Yii::t('property', 'Share') ?></a>
            </li>
            <?php if ($entity === BottomPropertyMenu::ENTITY_PROPERTY) { ?>
                <li>
                    <a href="#" id="bottom-menu-more">
                        <i class="icon-bottom i-more"></i>
                        <?= Yii::t('property', 'More') ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php if ($entity === BottomPropertyMenu::ENTITY_PROPERTY) { ?>
    <div class="more-menu-bg">
        <div class="more-menu">
            <!--<div class="more-menu-title text-center"><?//= Yii::t('catalog', 'More actions') ?></div>-->
            <ul class="more-ul no-list">
                <li>
                    <?= Html::a('<span class="more-menu-icon i-dollar-symbol"></span>'.Yii::t('property', 'Offer your price'), [
                        '/property/ajax/offer-price', 'entity_id' => $object['id']
                    ], [
                        'data-action' => 'offer-price',
                        'data-target' => '#dynamic-modal',
                        'data-toggle' => 'modal'
                    ]) ?>
                </li>
                <li>
                    <?= Html::a('<span class="more-menu-icon i-houses"></span>'.Yii::t('property', 'Request a showcase'), [
                        '/property/ajax/request-showcase', 'entity_id' => $object['id']
                    ], [
                        'data-action' => 'request-showcase',
                        'data-target' => '#dynamic-modal',
                        'data-toggle' => 'modal'
                    ]) ?>
                </li>
                <?php if ($object['type'] === Property::TYPE_SALE) { ?>
                    <li>
                        <?= Html::a('<span class="more-menu-icon i-mail"></span>'.Yii::t('property', 'Send Request'), [
                            '/property/ajax/request-mortgage', 'entity_id' => $object['id']
                        ], [
                            'data-action' => 'request-mortgage',
                            'data-target' => '#dynamic-modal',
                            'data-toggle' => 'modal'
                        ]) ?>
                    </li>
                <?php } ?>
                <li>
                    <a href="#">
                        <span class="more-menu-icon i-diskette"></span><?= Yii::t('property', 'Save') ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="more-menu-icon i-phone-book"></span><?= Yii::t('property', 'Contact owner (agent)') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/property/manage/create']) ?>" target="_blank">
                        <span class="more-menu-icon i-write-letter"></span><?= Yii::t('main', 'List your property' ) ?>
                    </a>
                </li>
            </ul>
            <a class="text-center btn-cancel" id="cancel-more" href="#"><?= Yii::t('catalog', 'Cancel') ?></a>
        </div>
    </div>
<?php } ?>

<?php $script = <<<JS
    $('.search-bottom').on('click', function() {
        $(this).addClass('open');
    });
    
    $(document.body).on('click', '#close-bsearch', function() {
        $('.search-bottom').removeClass('open');
        return false;
    });
    
    $(document.body).on('click', '#bottom-menu-more', function() {
        $('#dynamic-modal').modal('hide');
        $('.more-menu-bg').addClass('open');
        $('html').addClass('html-open');
        $('body').addClass('body-open');
        return false;
    });
    
    $(document.body).on('click', '#cancel-more', function() {
        $('.more-menu-bg').removeClass('open');
        $('html').removeClass('html-open');
        $('body').removeClass('body-open');
        return false;
    });
JS;

$this->registerJs($script);
