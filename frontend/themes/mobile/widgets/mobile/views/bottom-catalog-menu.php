<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.03.2019
 * Time: 17:50
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>

    <div class="menu-bottom-block menu-bottom-catalog">
        <ul class="no-list space-between flex menu-bottom">
            <li>
                <a href="#" id="menu"><i class="icon-bottom i-menu"></i><?= Yii::t('catalog', 'Menu') ?></a>
            </li>
            <li>
                <a id="bottom-menu-map" href="javascript:void(0)">
                    <div class="map-btn"><i class="icon-bottom i-map"></i><?= Yii::t('catalog', 'Map') ?></div>
                    <div class="list-btn"><i class="icon-bottom i-list"></i><?= Yii::t('catalog', 'List') ?></div>
                </a>
            </li>
            <li>
                <a href="#" id="filter">
                    <div class="mobile-count-filter text-center hidden">
                        5
                    </div>
                    <i class="icon-bottom i-controls"></i><?= Yii::t('catalog', 'Filter') ?>
                </a>
            </li>
            <li class="search-bottom">
                <div class="search-bottom-bg">
                    <a class="bottom-menu-arrow" href="#" id="close-bsearch">
                        <i class="icon-bottom i-right-arrow"></i>
                        <?= Yii::t('catalog', 'Close') ?>
                    </a>
                    <div class="input-group">
                        <?= Html::input('search', 'city-helper', null, [
                            'class' => 'form-control input-grey',
                            'id' => 'filter-city-helper',
                        ]) ?>
                        <a href="#" class="input-group-addon">
                            <i class="icon-bottom i-search"></i>
                        </a>
                    </div>
                    <div class="text-center text-bsearch"><?= Yii::t('catalog', 'Search') ?></div>

                    <div class="district-form text-left hidden">
                        <div class="district-inner">
                            <p class="">
                                <?= Yii::t('catalog', 'Start typing and select one of the options') ?>
                            </p>
                            <div class="autocomplete-loc-suggestions">

                            </div>
                        </div>
                        <div class="district-open">
                            <a class="flex space-between" href="#" data-action="load-city-parts" data-toggle="modal"
                               data-target="#city-parts">
                                <span><?= Yii::t('catalog', 'Districts and microdistricts') ?></span>
                                <i class="icon-next"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <a href="#" id="bottom-menu-more">
                    <i class="icon-bottom i-more"></i>
                    <?= Yii::t('property', 'More') ?>
                </a>
            </li>
        </ul>
    </div>

    <div class="more-menu-bg">
        <div class="more-menu">
            <!--<div class="more-menu-title text-center"><?//= Yii::t('catalog', 'More actions') ?></div>-->
            <ul class="more-ul no-list">
                <li>
                    <a href="#">
                        <span class="more-menu-icon i-diskette"></span>
                        <?= Yii::t('catalog', 'Save') ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="more-menu-icon i-share"></span>
                        <?= Yii::t('catalog', 'Share') ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="more-menu-icon i-list1"></span>
                        <?= Yii::t('catalog', 'Send Report') ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="more-menu-icon i-home"></span>
                        <?= Yii::t('catalog', 'Favorite') ?>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::to(['/property/manage/create']) ?>" target="_blank">
                        <span class="more-menu-icon i-write-letter"></span>
                        <?= Yii::t('main', 'List your property') ?>
                    </a>
                </li>
            </ul>
            <a class="text-center btn-cancel" id="cancel-more" href="#"><?= Yii::t('catalog', 'Cancel') ?></a>
        </div>
    </div>

<?php $script = <<<JS
    $('.search-bottom').on('click', function() {
        $(this).addClass('open');
    });

    $(document.body).on('click', '#close-bsearch', function() {
        $('.search-bottom').removeClass('open');
        return false;
    });
    
    $(document.body).on('click', '#bottom-menu-map', function() {
        $('.catalog-right').toggleClass('open');
        $('.map-btn').toggle();
        $('.list-btn').toggle();
        $('html').toggleClass('html-open');
        $('body').toggleClass('body-open');
        $(this).closest('li').toggleClass('active');
    });
    
    $(document.body).on('click', '#filter', function() {
        $('.filters').toggleClass('open');
        $('html').toggleClass('html-open');
        $('body').toggleClass('body-open');
        return false;
    });
    
    $(document.body).on('click', '#bottom-menu-more', function() {
        $('#dynamic-modal').modal('hide');
        $('.more-menu-bg').addClass('open');
        $('html').addClass('html-open');
        $('body').addClass('body-open');
        return false;
    });
    
    $(document.body).on('click', '#cancel-more', function() {
        $('.more-menu-bg').removeClass('open');
        $('html').removeClass('html-open');
        $('body').removeClass('body-open');
        return false;
    });
    
    $('body').on('click', function (e) {
        if (!$(e.target).closest(".search-bottom").length) {
            $('.district-form').addClass('hidden');
        }
        
        if (!$(e.target).closest(".filters").length) {
            $('.filter-item').removeClass('open');
        }
    })
JS;
$this->registerJs($script);
