<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 22.03.2019
 * Time: 13:00
 */

use common\models\Property;
use frontend\widgets\mobile\BottomBuildingMenu;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $object
 * @var string $entity
 */

?>

    <div class="menu-bottom-block menu-bottom-catalog">
        <ul class="no-list space-between flex menu-bottom">
            <li>
                <a href="#" id="menu"><i class="icon-bottom i-menu"></i><?= Yii::t('building', 'Menu') ?></a>
            </li>
            <?php if ($entity === BottomBuildingMenu::ENTITY_BUILDING) { ?>
                <li>
                    <?= Html::a('<i class="icon-bottom i-chat"></i>' . Yii::t('building', 'Send'), '#'/*,[
                        '/property/ajax/send-message',
                        'entity_id' => $object['id']
                    ], [
                        'data-action' => 'send-message',
                        'data-target' => '#dynamic-modal',
                        'data-toggle' => 'modal'
                    ]*/) ?>
                </li>
                <li>
                    <?= Html::a('<i class="icon-bottom i-phone"></i>' . Yii::t('building', 'Call'), '#'/*, [
                        '/property/ajax/show-contacts',
                        'entity_id' => $object['id'],
                    ], [
                        'data-action' => 'show-contacts',
                        'data-target' => '#dynamic-modal',
                        'data-toggle' => 'modal'
                    ]*/) ?>
                </li>
            <?php } ?>
            <li>
                <a href="#"><i class="icon-bottom i-share"></i><?= Yii::t('building', 'Share') ?></a>
            </li>
            <?php if ($entity === BottomBuildingMenu::ENTITY_BUILDING) { ?>
                <li>
                    <a href="#" id="bottom-menu-more">
                        <i class="icon-bottom i-more"></i>
                        <?= Yii::t('building', 'More') ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>

<?php if ($entity === BottomBuildingMenu::ENTITY_BUILDING) { ?>
    <div class="more-menu-bg">
        <div class="more-menu">
            <!--<div class="more-menu-title text-center"><?//= Yii::t('catalog', 'More actions') ?></div>-->
            <ul class="more-ul no-list">
                <li>
                    <a href="#">
                        <span class="more-menu-icon i-home"></span>
                        <?= Yii::t('catalog', 'Favorite') ?>
                    </a>
                </li>
                    <li>
                        <?= Html::a('<span class="more-menu-icon i-mail"></span>'.Yii::t('building', 'Send Request'),'#'/* [
                            '/property/ajax/request-mortgage',
                            'entity_id' => $object['id']
                        ], [
                            'data-action' => 'request-mortgage',
                            'data-target' => '#dynamic-modal',
                            'data-toggle' => 'modal'
                        ]*/) ?>
                    </li>
                <li>
                    <a href="<?= Url::to(['/property/manage/create']) ?>" target="_blank">
                        <span class="more-menu-icon i-write-letter"></span><?= Yii::t('main', 'List your property' ) ?>
                    </a>
                </li>
            </ul>
            <a class="text-center btn-cancel" id="cancel-more" href="#"><?= Yii::t('building', 'Cancel') ?></a>
        </div>
    </div>
<?php } ?>

<?php $script = <<<JS
   
    $(document.body).on('click', '#filter', function() {
        $('.filters').toggleClass('open');
        $('html').toggleClass('html-open');
        $('body').toggleClass('body-open');
        $(this).closest('li').toggleClass('active');
        return false;
    });
    
    $(document.body).on('click', '#bottom-menu-more', function() {
        $('#dynamic-modal').modal('hide');
        $('.more-menu-bg').addClass('open');
        $('html').addClass('html-open');
        $('body').addClass('body-open');
        return false;
    });
    
    $(document.body).on('click', '#cancel-more', function() {
        $('.more-menu-bg').removeClass('open');
        $('html').removeClass('html-open');
        $('body').removeClass('body-open');
        return false;
    });
JS;
$this->registerJs($script);