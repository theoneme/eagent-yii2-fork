<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:16
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $catalogView
 * @var array $filters
 * @var string $sort
 * @var string $address
 * @var array $params
 * @var integer $gridSize
 * @var null|integer $resultCount
 */

?>

<?= Html::beginForm(Url::canonical(), 'get', [
    'id' => 'filter-form',
]); ?>
<?= Html::hiddenInput('sort', $sort, ['id' => 'filter-sort']) ?>
<?= Html::hiddenInput('box', $params['box'] ?? null, ['id' => 'filter-box']) ?>
<?= Html::hiddenInput('zoom', $params['zoom'] ?? null, ['id' => 'filter-zoom']) ?>
<?= Html::hiddenInput('district', $params['district'] ?? null, ['id' => 'filter-district']) ?>
<?= Html::hiddenInput('microdistrict', $params['microdistrict'] ?? null, ['id' => 'filter-microdistrict']) ?>
<?= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
    <div class="filters">
        <div class="loading-overlay hidden"></div>

        <!--<div class="filter-search">
            <div class="input-group search text-center">
                <? /*= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
                <?= Html::input('search', 'city-helper', $address, [
                    'class' => 'form-control input-grey',
                    'id' => 'filter-city-helper',
                    'placeholder' => Yii::t('catalog', 'Enter city, region or country')
                ]) */ ?>
                <a href="#" class="input-group-addon"><i class="icon-search"></i></a>
            </div>
        </div>-->

        <?php $operationFilter = ArrayHelper::remove($filters, 'operation'); ?>
        <?php if ($operationFilter) { ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="filter-title">
                            <?= $operationFilter['title'] ?>
                        </div>
                    </div>
                    <div class="col-xs-8 flex">
                        <?php $checked = ($operationFilter['checked'] ?? $operationFilter['values']); ?>
                        <?php foreach ($operationFilter['values'] as $key => $value) { ?>
                            <div class="filter-choose width100">
                                <?= Html::radio('operation', $key === $checked, ['value' => $key, 'id' => $key]) ?>
                                <?= Html::tag('label', $value['title'], [
                                    'class' => 'text-center width100 ' . ($key === $checked ? 'active' : ''),
                                    'for' => $key,
                                ]) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php $categoryFilter = ArrayHelper::remove($filters, 'category'); ?>
        <?php if ($categoryFilter) { ?>
            <div class="form-group">
                <div class="filter-title"><?= Yii::t('catalog', 'Category') ?></div>
                <div>
                    <div class="filter-item filter-types">
                        <div class="filter-box" data-role="mobile-category-filter">
                            <?php foreach ($categoryFilter['values'] as $key => $category) {
                                $checked = $categoryFilter['checked'] === $key;
                                $childChecked = !empty($category['children']) && array_key_exists($categoryFilter['checked'], $category['children']);
                                ?>
                                <div class="filter-type-group <?= $checked || $childChecked ? 'active' : '' ?>">
                                    <div class="filter-type-group-name">
                                        <?= Html::radio('category', $categoryFilter['checked'] === $key, [
                                            'id' => "cat-{$key}",
                                            'value' => $key,
                                            'class' => 'radio-checkbox',
                                            'data' => [
                                                'role' => 'parent-category'
                                            ]
                                        ]) ?>
                                        <label for="<?= "cat-{$key}" ?>">
                                            <span class="filter-circle filter-circle-<?= $category['circle'] ?>"></span>
                                            <span class="ftgn-title"><?= $category['title'] ?></span>
                                        </label>
                                    </div>
                                    <?php if (!empty($category['children'])) { ?>
                                        <?php foreach ($category['children'] as $ckey => $cvalue) { ?>
                                            <div class="filter-sub-types">
                                                <div class="filter-sub-type">
                                                    <?= Html::radio('category', $categoryFilter['checked'] === $ckey, [
                                                        'id' => "cat-{$ckey}",
                                                        'value' => $ckey,
                                                    ]) ?>
                                                    <label for="<?= "cat-{$ckey}" ?>">
                                                        <span class="ftgn-title"><?= $cvalue['title'] ?></span>
                                                    </label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

        <div class="form-group">
            <div class="filter-title"><?= Yii::t('catalog', 'Catalog Mode') ?></div>
            <div>
                <div class="filter-mode">
                    <?= Html::a('<i class="icon-menu"></i>&nbsp;' . Yii::t('catalog', 'Default Mode'), '#', [
                        'class' => 'switch-items ' . ($catalogView === 'grid' ? 'font-bold' : ''),
                        'data-view' => 'grid',
                        'data-action' => 'change-catalog-view',
                        'data-url' => Url::current([0 => '/property/catalog/index', 'box' => null, 'zoom' => null])
                    ]) ?>
                </div>
                <div class="filter-mode">
                    <?= Html::a('<i class="icon-gps"></i>&nbsp;' . Yii::t('catalog', 'Search by map'), '#', [
                        'class' => 'switch-items ' . ($catalogView === 'map' ? 'font-bold' : ''),
                        'data-view' => 'map',
                        'data-action' => 'change-catalog-view',
                    ]) ?>
                </div>
            </div>
        </div>

        <!--<div class="form-group catalog-sort-wrap">
            <label for="catalog-sort"><? /*= $sortData['title'] ?></label>
            <select name="sort" id="catalog-sort">
                <option value="">
                    --&nbsp;<?= Yii::t('catalog', 'Select {attribute}', ['attribute' => Yii::t('catalog', 'Sort Type')]) ?>
                </option>
                <?php foreach ($sortData['values'] as $valueKey => $value) { ?>
                    <option value="<?= $valueKey ?>" <?= $valueKey === $sortData['checked'] ? 'selected' : '' ?>><?= $value ?></option>
                <?php }*/ ?>
            </select>
        </div>-->

        <?php $priceFilter = ArrayHelper::remove($filters, 'price'); ?>
        <?php if ($priceFilter) { ?>
            <div class="form-group filter-item">
                <label class="filter-title" for="min-price-input"><?= Yii::t('catalog', 'Price') ?></label>
                <div class="flex two-param">
                    <?= Html::input('number', 'price[min]', $priceFilter['left']['checked'], [
                        'placeholder' => Yii::t('catalog', 'Min'),
                        'class' => 'input-grey',
                        'id' => 'min-price-input'
                    ]) ?>
                    <div class="dash">
                        –
                    </div>
                    <?= Html::input('number', 'price[max]', $priceFilter['right']['checked'], [
                        'placeholder' => Yii::t('catalog', 'Max'),
                        'class' => 'input-grey',
                        'id' => 'max-price-input'
                    ]) ?>
                </div>
            </div>
        <?php } ?>
        <?php $bedroomFilter = ArrayHelper::remove($filters, 'bedrooms'); ?>
        <?php if ($bedroomFilter) { ?>
            <div class="form-group filter-item">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="filter-title">
                            <?= $bedroomFilter['title'] ?>
                        </div>
                    </div>
                    <div class="col-xs-8 flex">
                        <?php $checked = (int)($bedroomFilter['checked']['min'] ?? null); ?>
                        <?php for ($i = 1; $i < 6; $i++) { ?>
                            <div class="filter-choose width100">
                                <?= Html::radio('bedrooms[min]', $i === $checked, ['value' => $i, 'id' => "bedrooms-{$i}"]) ?>
                                <?= Html::tag('label', "{$i}+", [
                                    'class' => 'text-center width100 ' . ($i === $checked ? 'active' : ''),
                                    'for' => "bedrooms-{$i}",
                                ]) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php $roomFilter = ArrayHelper::remove($filters, 'rooms'); ?>
        <?php if ($roomFilter) { ?>
            <div class="form-group filter-item">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="filter-title">
                            <?= $roomFilter['title'] ?>
                        </div>
                    </div>
                    <div class="col-xs-8 flex">
                        <?php $checked = (int)($roomFilter['checked']['min'] ?? null); ?>
                        <?php for ($i = 1; $i < 6; $i++) { ?>
                            <div class="filter-choose width100">
                                <?= Html::radio('rooms[min]', $i === $checked, ['value' => $i, 'id' => "rooms-{$i}"]) ?>
                                <?= Html::tag('label', "{$i}+", [
                                    'class' => 'text-center width100 ' . ($i === $checked ? 'active' : ''),
                                    'for' => "rooms-{$i}",
                                ]) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php foreach ($filters as $key => $filter) { ?>
            <?= $this->render("filter/{$filter['type']}", [
                'filter' => $filter,
                'key' => $key
            ]) ?>
        <?php } ?>

        <div class="filter-ops flex space-between">
            <?= Html::a(Yii::t('catalog', 'Reset'), '#', ['class' => '', 'data-action' => 'reset-filter']) ?>
            <div class="mobile-results-count">
                <?= Yii::t('catalog', '{count, plural, one{# result} other{# results}}', [
                    'count' => $resultCount
                ]) ?>
            </div>
            <?= Html::submitInput(Yii::t('catalog', 'Show'), ['class' => '']) ?>
        </div>
    </div>

<?= Html::endForm() ?>

<?= $this->render('filter/city-part-modal') ?>

<?php $baseUrl = Url::to(['/property/catalog/search']);
$catalogViewUrl = Url::to(['/property/catalog/view']);
$cityUrl = Url::to(['/location/ajax/locations']);

$script = <<<JS
    $('[data-role=mobile-category-filter] input[data-role=parent-category]').on('change', function() { 
        if($(this).val()) {
            $('[data-role=mobile-category-filter] .filter-type-group').removeClass('active');
            $(this).closest('.filter-type-group').addClass('active');
        }
    });
JS;

$this->registerJs($script);