<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.09.2018
 * Time: 19:04
 */

use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\plugins\AutoCompleteAsset::register($this);

?>

    <div class="modal fade modal-city" id="location-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?= Html::beginForm(['/location/ajax/set-location'], 'post', ['id' => 'city-form']) ?>
                    <div class="city-search input-group form-group">
                        <?= Html::hiddenInput('location', null, ['id' => 'filter-city']) ?>
                        <?= Html::input('search', 'city-helper', null, [
                            'class' => 'form-control',
                            'id' => 'filter-city-helper',
                            'placeholder' => Yii::t('catalog', 'Address or ZIP code')
                        ]) ?>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="icon-search"></i></button>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                    <div class="tab-content tab-city">
                        <div class="city-content tab-pane fade in active" id="cities">
                            <p>
                                <strong><?= Yii::t('index', 'Big cities') ?></strong>
                            </p>
                            <div class="city-block">
                                <div class='modal-triple-column' data-role="big-city-container">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="modal-footer text-center">
                <?= Html::submitButton(Yii::t('index', 'Remember'), ['class' => 'btn btn-white-blue', 'name' => 'contact-button']) ?>
            </div>-->
            </div>
        </div>
    </div>

<?php $bigCityUrl = Url::to(['/location/ajax/big-cities']);
$setLocationUrl = Url::to(['/location/ajax/set-location']);
$citySearchUrl = Url::to(['/location/ajax/locations']);

$script = <<<JS
    $('#location-modal').LocationModal({
        bigCityUrl: '{$bigCityUrl}',
        setLocationUrl: '{$setLocationUrl}'
    });

    $('#city-form').on('submit', function() {
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
            if(result.success === true) {
                window.location.replace(result.location);
            }
        });
        
        return false;
    });

    new AutoComplete({
        selector: "#filter-city-helper",
        autoFocus: false,
        minChars: 3,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.get('{$citySearchUrl}', { request: request }, function(data) { 
                response(data);
            });     
        },
        renderItem: function (item, search){ 
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion" data-title="' + item.title + '" data-slug="' + item.slug + '">'
                + item.title.replace(re, "<b>$1</b>")
                + '</div>';
        },
        onSelect: function(e, term, item) {
            $("#filter-city").val(item.getAttribute('data-slug'));
            $("#filter-city-helper").val(item.getAttribute('data-title')).prop('disabled', true);
            $('#city-form').submit();
        }
    });
JS;

$this->registerJs($script);
