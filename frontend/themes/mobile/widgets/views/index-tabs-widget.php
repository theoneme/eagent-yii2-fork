<?php

use frontend\widgets\IndexTabsWidget;
use yii\helpers\Html;

/**
 * @var string $operation
 * @var array $links
 * @var string $content
 */
?>

<div class="tab-content banner-tabs">
    <div class="tab-pane fade in active" id="<?= $links[$operation]['id'] ?>">
        <?= $content ?>
    </div>
</div>

<ul class="banner-tabs-nav text-center no-list">
    <?php foreach ($links as $op => $link) {
        $active = $op === $operation ? 'active' : '';
        $arrow = in_array($op, [IndexTabsWidget::OPERATION_BUY, IndexTabsWidget::OPERATION_RENT]) ? 'arrow' : '';
        echo Html::tag('li',
            Html::a($link['label'], ['/site/index', 'operation' => $op === IndexTabsWidget::OPERATION_BUY ? null : $op]),
            [
                'class' => "$active $arrow"
            ]
        );
    } ?>
</ul>