<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 21:57
 */

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * @var View $this
 */

?>

<footer>
    <div class="footer-column text-center">
        <div class="footer-title">
            <?= Yii::t('main', 'Get in touch') ?>
        </div>
        <a class="footer-phone" href="tel:15188572903" title="Call Real Estate Company eAgent.me Inc">
            <i class="icon-phone"></i>
            +7 (912) 251-89-48
        </a>
        <a class="footer-mail" href="mailto:save@eagent.me" title="Call Real Estate Company eAgent.me Inc">
            <i class="icon-envelope"></i>
            info@eagent.me
        </a>
        <address>
            <p class="text-center">17100 Collins Ave Suite 215</p>
            <p class="text-center">Sunny Isles Beach, FL 33160</p>
            <p class="text-center">United States</p>
        </address>

        <div class="footer-title">
            <?= Yii::t('main', 'Useful Links') ?>
        </div>
        <ul class="no-list footer-alias">
            <li>
                <h5>
                    <?= Html::a(Yii::t('main', 'Desktop version'), Url::current(['mode' => 'desktop']), [
                        'data-method' => 'post',
                    ]) ?>
                </h5>
            </li>
            <li>
                <h5>
                    <?= Html::a(Yii::t('main', 'Mobile version'), Url::current(['mode' => 'mobile']), [
                        'data-method' => 'post',
                    ]) ?>
                </h5>
            </li>
        </ul>

        <div class="footer-title">
            <?= Yii::t('main', 'Follow us') ?>
        </div>
        <div class="social">
            <a class="text-center" href="https://www.facebook.com/eagent.me">
                <i class="icon-facebook-logo"></i>
            </a>
            <a class="text-center" href="https://twitter.com/eagentme">
                <i class="icon-twitter"></i>
            </a>
            <a class="text-center" href="https://plus.google.com/u/0/+EagentMe">
                <i class="icon-google-plus"></i>
            </a>
            <a class="text-center" href="https://www.linkedin.com/company/eagent-me?trk=company_logo">
                <i class="icon-linkedin"></i>
            </a>
            <a class="text-center" href="https://www.pinterest.com/eroomme/httpwwweagentme">
                <i class="icon-pinterest"></i>
            </a>
        </div>
    </div>
</footer>