<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:53
 */

/**
 * @var array $filter
 * @var \yii\widgets\ActiveForm $form
 * @var string $key
 */

?>

<div class="form-group">
    <div class="row">
        <div class="col-xs-4"><label class="filter-title" for="<?= $key ?>"><?= $filter['title'] ?></label></div>
        <div class="col-xs-8">
            <select class="width100" name="<?= $key ?>" id="<?= $key ?>">
                <option value="">
                    <?= Yii::t('catalog', 'Select {attribute}', ['attribute' => $filter['title']]) ?>
                </option>
                <?php foreach ($filter['values'] as $valueKey => $value) { ?>
                    <option value="<?= $valueKey ?>" <?= $valueKey === $filter['checked'] ? 'selected' : '' ?>><?= "{$value['title']} ({$value['count']})" ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
</div>