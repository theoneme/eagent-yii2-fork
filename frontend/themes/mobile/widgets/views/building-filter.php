<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.09.2018
 * Time: 18:16
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string $catalogView
 * @var array $filters
 * @var string $sort
 * @var string $address
 * @var array $params
 * @var integer $gridSize
 * @var integer $resultCount
 */

?>

<?= Html::beginForm(Url::canonical(), 'get', [
    'id' => 'filter-form',
]); ?>
<?= Html::hiddenInput('box', $params['box'] ?? null, ['id' => 'filter-box']) ?>
<?= Html::hiddenInput('zoom', $params['zoom'] ?? null, ['id' => 'filter-zoom']) ?>
<?= Html::hiddenInput('district', $params['district'] ?? null, ['id' => 'filter-district']) ?>
<?= Html::hiddenInput('microdistrict', $params['microdistrict'] ?? null, ['id' => 'filter-microdistrict']) ?>
<?= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
    <div class="filters">
        <div class="loading-overlay hidden"></div>
        <!--<div class="filter-search">
            <div class="input-group search text-center">
                <? /*= Html::hiddenInput('city', null, ['id' => 'filter-city']) ?>
                <?= Html::input('search', 'city-helper', $address, [
                    'class' => 'form-control input-grey',
                    'id' => 'filter-city-helper',
                    'placeholder' => Yii::t('catalog', 'Enter city, region or country')
                ]) */ ?>
                <a href="#" class="input-group-addon"><i class="icon-search"></i></a>
            </div>
        </div>-->
        <?php $operationFilter = ArrayHelper::remove($filters, 'operation'); ?>
        <?php if ($operationFilter) { ?>
            <div class="flex flex-wrap filter-row">
                <?php $checked = ($operationFilter['checked'] ?? $operationFilter['values']); ?>
                <?php foreach ($operationFilter['values'] as $key => $value) { ?>
                    <div class="form-group filter-col50">
                        <?= Html::radio('operation', $operationFilter['checked'] === $key, [
                            'id' => "cat-{$key}",
                            'value' => $key,
                            'class' => 'radio-checkbox'
                        ]) ?>
                        <label class="flex" for="<?= "cat-{$key}" ?>">
                            <span><?= $value['title'] ?></span>
                        </label>
                    </div>
                <?php } ?>
            </div>

        <?php } ?>
        <div class="form-group">
            <div class="filter-title"><?= Yii::t('catalog', 'Catalog Mode') ?></div>
            <div>
                <div class="filter-mode">
                    <?= Html::a('<i class="icon-menu"></i>&nbsp;' . Yii::t('catalog', 'Default Mode'), '#', [
                        'class' => 'switch-items ' . ($catalogView === 'grid' ? 'font-bold' : ''),
                        'data-view' => 'grid',
                        'data-action' => 'change-catalog-view',
                        'data-url' => Url::current([0 => '/building/catalog/index', 'box' => null, 'zoom' => null])
                    ]) ?>
                </div>
                <div class="filter-mode">
                    <?= Html::a('<i class="icon-gps"></i>&nbsp;' . Yii::t('catalog', 'Map Filter Mode'), '#', [
                        'class' => 'switch-items ' . ($catalogView === 'map' ? 'font-bold' : ''),
                        'data-view' => 'map',
                        'data-action' => 'change-catalog-view',
                    ]) ?>
                </div>
            </div>
        </div>

        <?php foreach ($filters as $key => $filter) { ?>
            <?= $this->render("filter/{$filter['type']}", [
                'filter' => $filter,
                'key' => $key
            ]) ?>
        <?php } ?>

        <div class="filter-ops flex space-between">
            <?= Html::a(Yii::t('catalog', 'Reset'), '#', ['class' => '', 'data-action' => 'reset-filter']) ?>
            <div class="mobile-results-count">
                <?= Yii::t('catalog', '{count, plural, one{# result} other{# results}}', [
                    'count' => $resultCount
                ]) ?>
            </div>
            <?= Html::submitInput(Yii::t('catalog', 'Show'), ['class' => '']) ?>
        </div>
    </div>

<?= Html::endForm() ?>

<?= $this->render('filter/city-part-modal') ?>