<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:18
 */

use frontend\modules\crm\models\CrmMember;
use frontend\widgets\auth\LoginWidget;
use frontend\widgets\auth\RequestResetWidget;
use frontend\widgets\auth\SignupWidget;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 * @var array $categorySlugs
 * @var array $user
 * @var CrmMember[] $crmMembers
 * @var boolean $isGuest
 * @var integer $currentProfile
 * @var integer $companyId
 * @var boolean $isCompany
 */

?>

    <header>
        <div class="fixed-top">
            <div class="left-top-item text-left text-left trigger-cell">
                <div class="trigger-menu">
                    <i></i>
                </div>
            </div>

            <div class="left-top-item text-left logo-cell text-center">
                <div class="logo-flex">
                    <div class="logo-img">
                        <?= Html::img(['/images/logo.jpg'], ['alt' => 'eagent', 'title' => 'eagent']) ?>
                    </div>
                    <div class="logo-text">
                        <a class="logo" href="/"><span>E</span>AGENT</a>
                        <div class="powered"><?= Yii::t('main', 'Powered by {who}', ['who' => 'uJobs']) ?></div>
                    </div>
                </div>
            </div>

            <?php if ($isGuest === true) { ?>
                <div class="right-top-item call text-right">
                    <a href="#" class="mobile-login" data-toggle="modal" data-target="#login-modal"> </a>
                </div>
            <?php } else { ?>
                <a href="<?= Url::to(['/agent/agent/view', 'id' => Yii::$app->user->getId()]) ?>"
                   class="right-top-item btn-profile text-right">
                    <div class="header-avatar text-center">
                        <?php if (!empty($user['thumb'])) {
                            echo Html::img($user['thumb'], ['alt' => $user['username'], 'title' => $user['username']]);
                        } else {
                            echo StringHelper::truncate($user['username'], 1, '');
                        } ?>
                    </div>
                    <div class="header-username">
                        <?= Html::encode(StringHelper::truncateWords($user['username'], 1, '')) ?>
                    </div>
                </a>
            <?php } ?>
        </div>
    </header>

<?php if ($isGuest === true) { ?>
    <?= SignupWidget::widget() ?>
    <?= LoginWidget::widget() ?>
    <?= RequestResetWidget::widget() ?>
<?php } ?>

<?php $script = <<<JS
    window.addEventListener('popstate', function(e) { 
        if (e.state) {
            location.reload();
        }
    });

    $(document.body).on('click','.animate-button',function(){
       $(this).addClass('go') 
    });
    
    $('[data-action="identity-switcher"]').on('click', function() {
        let href = $(this).attr('href'),
            id = $(this).data('identity');
        
        $.post(href, {identity: id}, function(response) {
            if(response.success === true) {
                window.location.reload();
            }
        });
        
        return false;
    });
JS;

$this->registerJs($script);
