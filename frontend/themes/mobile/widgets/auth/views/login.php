<?php

use common\models\auth\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

\frontend\themes\mobile\assets\AuthAsset::register($this);

/**
 * @var LoginForm $loginForm
 */

?>

    <div class="modal fade auth-modal new-modal" id="login-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="auth-modal-title text-center"><?= Yii::t('main', 'Log in') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/auth/login'],
                        'options' => [
                            'class' => 'formbot'
                        ],
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'auth-modal-error text-left'
                            ]
                        ]
                    ]); ?>
                    <?= $form->field($loginForm, 'login', [
                        'inputOptions' => [
                            'placeholder' => Yii::t('main', 'Email') . ' / ' . Yii::t('main', 'Phone')
                        ]
                    ]) ?>
                    <?= $form->field($loginForm, 'password', [
                        'inputOptions' => [
                            'placeholder' => Yii::t('main', 'Password')
                        ]
                    ])->passwordInput() ?>

                    <div class="flex space-between">
                        <div class="chover">
                            <?= $form->field($loginForm, 'rememberMe', [
                                'template' => '{input}',
                                'options' => [
                                    'tag' => false
                                ]
                            ])->checkbox(['id' => 'rem-check-box'], false) ?>
                            <label for="rem-check-box">
                                <span class="ctext"><?= Yii::t('main', 'Remember me') ?></span>
                            </label>
                        </div>
                        <div>
                            <?= Html::a(Yii::t('main', 'Forgot Password') . '?', null, [
                                'class' => 'forgot',
                                'data-dismiss' => 'modal',
                                'data-toggle' => 'modal',
                                'data-target' => '#request-reset-modal'
                            ]) ?>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('main', 'Log in'), ['class' => 'btn btn-big width100 btn-green no-margin']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <div class="auth-modal-or-line text-center">
                        <div class="auth-modal-or-text"><?= Yii::t('main', 'or') ?></div>
                    </div>
                    <div class="auth-modal-socials">
                        <?= Html::a('<i class="icon-facebook-logo" aria-hidden="true"></i>', 'https://eagent.me/auth/social?authclient=facebook', ['class' => 'auth-modal-facebook text-center']) ?>
                        <?= Html::a('<i class="icon-google-glass-logo" aria-hidden="true"></i>', 'https://eagent.me/auth/social?authclient=google', ['class' => 'auth-modal-google text-center']) ?>
                    </div>
                    <div class="auth-modal-bottom text-center">
                        <?= Yii::t('main', 'Not a member yet?') ?>
                        <a data-dismiss="modal" data-toggle="modal" data-target="#signup-modal">
                            <?= Yii::t('main', 'Register now') ?>
                        </a>
                        — <?= Yii::t('main', 'it\'s fun and easy!') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    $('#login-modal').on('hidden.bs.modal', function (e) {
		$('#login-form')[0].reset();
	});
JS;

$this->registerJs($script);