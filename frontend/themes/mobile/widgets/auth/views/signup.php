<?php

use frontend\models\auth\SignupForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

\frontend\themes\mobile\assets\AuthAsset::register($this);

/**
 * @var SignupForm $signupForm
 * @var \yii\web\View $this
 */
?>

    <div class="modal fade auth-modal new-modal" id="signup-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="auth-modal-title text-center">
                        <?= Yii::t('main', 'Welcome to {site}', ['site' => Yii::$app->name]) ?>
                    </div>
                    <div class="auth-modal-subtitle text-center"><?= Yii::t('main', 'Sell, Buy, Rent real estate directly') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'signup-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/auth/signup'],
                        'options' => [
                            'class' => 'formbot',
                            'data-step' => 1,
                            'data-final-step' => 1
                        ],
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'auth-modal-error text-left'
                            ]
                        ]
                    ]); ?>
                    <div class="steps" data-step="1">
                        <?= $form->field($signupForm, 'login', [
                            'inputOptions' => [
                                'placeholder' => Yii::t('main', 'Enter your email or phone')
                            ]
                        ]) ?>
                        <?= $form->field($signupForm, 'password', [
                            'inputOptions' => [
                                'placeholder' => Yii::t('main', 'Choose a Password')
                            ]
                        ])->passwordInput() ?>
                    </div>
                    <div class="text-center form-group">
                        <?= Html::submitButton(Yii::t('main', 'Sign Up'), ['class' => 'btn btn-big width100 btn-green no-margin']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <div class="auth-modal-warning text-warning">
                        <div class="auth-modal-fw-item">
                            <?= Yii::t('main', 'By joining I agree to {privacy} and {terms}, and receive emails from {site}.', [
                                'privacy' => Html::a(Yii::t('main', 'with privacy policy'),
                                    ['/privacy-policy'],
                                    ['class' => 'page-modal-toggle']
                                ),
                                'terms' => Html::a(Yii::t('main', 'with terms of service'),
                                    ['/terms-of-service'],
                                    ['class' => 'page-modal-toggle']
                                ),
                                'site' => Yii::$app->name
                            ]) ?>
                        </div>
                    </div>
                    <div class="auth-modal-or-line text-center">
                        <div class="auth-modal-or-text"><?= Yii::t('main', 'or') ?></div>
                    </div>
                    <div class="auth-modal-socials">
                        <?= Html::a('<i class="icon-facebook-logo" aria-hidden="true"></i>', 'https://eagent.me/auth/social?authclient=facebook', ['class' => 'auth-modal-facebook text-center']) ?>
                        <?= Html::a('<i class="icon-google-glass-logo" aria-hidden="true"></i>', 'https://eagent.me/auth/social?authclient=google', ['class' => 'auth-modal-google text-center']) ?>
                    </div>
                    <div class="auth-modal-bottom text-center">
                        <?= Yii::t('main', 'Already a member?') ?>
                        <a data-dismiss="modal" data-toggle="modal" data-target="#login-modal">
                            <?= Yii::t('main', 'Sign In') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $script = <<<JS
    let signupForm = $('#signup-form');

	$('#signup-modal').on('hidden.bs.modal', function (e) {
		signupForm[0].reset();
		signupForm.data('step', 1);
		$('#ModalSignup .steps').hide();
		$('#ModalSignup div[data-step=\"1\"]').show();
	});
JS;

$this->registerJs($script);