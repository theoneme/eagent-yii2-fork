<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.12.2018
 * Time: 18:24
 */

namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

/**
 * Class ForumAsset
 * @package frontend\themes\mobile\assets
 */

class ForumAsset extends \frontend\assets\ForumAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/forum.css',
    ];
    public $js = [
    ];
    public $depends = [
        CommonAsset::class
    ];
}