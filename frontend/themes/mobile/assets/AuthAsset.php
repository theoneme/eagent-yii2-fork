<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2017
 * Time: 18:04
 */

namespace frontend\themes\mobile\assets;

/**
 * Class AuthAsset
 * @package frontend\themes\mobile\assets
 */
class AuthAsset extends \frontend\assets\AuthAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;

    public $depends = [
        CommonAsset::class
    ];
}