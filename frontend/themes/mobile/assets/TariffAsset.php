<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 03.10.2018
 * Time: 11:35
 */

namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

/**
 * Class TariffAsset
 * @package frontend\themes\mobile\assets
 */
class TariffAsset extends \frontend\assets\TariffAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/tariff.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}