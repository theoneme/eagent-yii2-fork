<?php

namespace frontend\themes\mobile\assets;

use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\MoreLessAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\assets\plugins\TooltipAsset;

/**
 * Class PropertyDirectModalAsset
 * @package frontend\themes\mobile\assets
 */
class PropertyDirectModalAsset extends \frontend\assets\PropertyDirectModalAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $js = [
        'js/classes/property-direct.js',
    ];
    public $depends = [
        TooltipAsset::class,
        MoreLessAsset::class,
        SlickAsset::class,
        CommonAsset::class,
        FancyBoxAsset::class
    ];
}
