<?php

namespace frontend\themes\mobile\assets;

use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\MoreLessAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\assets\plugins\TooltipAsset;
use yii\web\AssetBundle;

/**
 * Class PropertyModalAsset
 * @package frontend\themes\mobile\assets
 */
class PropertyModalAsset extends \frontend\assets\PropertyModalAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/modal.css',
        'css/forms.css',
    ];
    public $js = [
        '/js/classes/mortgage-calculator.js',
        '/js/classes/modal-map.js',
    ];
    public $depends = [
        TooltipAsset::class,
        MoreLessAsset::class,
        SlickAsset::class,
        CommonAsset::class,
        FancyBoxAsset::class
    ];
}
