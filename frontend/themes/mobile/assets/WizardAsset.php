<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.09.2018
 * Time: 12:41
 */

namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

/**
 * Class WizardAsset
 * @package frontend\assets\TariffAsset
 */
class WizardAsset extends \frontend\assets\TariffAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/wizard.css',
    ];
    public $js = [
        '/js/classes/progress-calculator.js'
    ];
    public $depends = [
        CommonAsset::class
    ];
}