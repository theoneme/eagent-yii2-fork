<?php

namespace frontend\themes\mobile\assets;

use common\assets\GoogleAsset;
use yii\web\AssetBundle;

/**
 * Class CatalogAsset
 * @package frontend\themes\mobile\assets
 */
class CatalogAsset extends \frontend\assets\CatalogAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/catalog.css',
    ];
    public $js = [
        '/js/vendor/marker-cluster.js',
        '/js/classes/custom-marker.js',
        '/js/classes/catalog-map.js',
    ];
    public $depends = [
        CommonAsset::class,
        GoogleAsset::class
    ];
}
