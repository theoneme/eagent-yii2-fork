<?php

namespace frontend\themes\mobile\assets;

use common\assets\GoogleAsset;
use yii\web\AssetBundle;

/**
 * Class RequestAsset
 * @package frontend\themes\mobile\assets
 */
class RequestAsset extends AssetBundle
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/request.css',
    ];
    public $js = [
    ];
    public $depends = [
        CommonAsset::class,
        GoogleAsset::class
    ];
}
