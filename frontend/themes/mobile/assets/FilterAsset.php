<?php

namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

/**
 * Class FilterAsset
 * @package frontend\themes\mobile\assets
 */
class FilterAsset extends \frontend\assets\FilterAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/filter.css',
    ];
    public $js = [
        '/js/classes/filter-advanced.js',
    ];
    public $depends = [
        CommonAsset::class
    ];
}
