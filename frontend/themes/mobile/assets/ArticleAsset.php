<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 03.12.2018
 * Time: 16:19
 */


namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

/**
 * Class CatalogAsset
 * @package frontend\themes\mobile\assets
 */
class ArticleAsset extends \frontend\assets\ArticleAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/article.css'
    ];
    public $js = [
    ];
    public $depends = [
        CommonAsset::class
    ];
}
