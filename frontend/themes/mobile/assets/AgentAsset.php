<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.09.2018
 * Time: 15:01
 */

namespace frontend\themes\mobile\assets;

use yii\web\AssetBundle;

/**
 * Class AgentAsset
 * @package frontend\themes\mobile\assets
 */
class AgentAsset extends \frontend\assets\AgentAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/agent.css',
    ];
    public $js = [
        '/js/classes/more-less.js',
    ];
    public $depends = [
        CommonAsset::class
    ];
}