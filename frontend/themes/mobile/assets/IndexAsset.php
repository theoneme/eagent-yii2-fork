<?php

namespace frontend\themes\mobile\assets;

/**
 * Class IndexAsset
 * @package frontend\themes\mobile\assets
 */
class IndexAsset extends \frontend\assets\IndexAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;

    public $js = [
        '/js/classes/location-modal-loader.js',
    ];

    public $depends = [
        CommonAsset::class
    ];
}
