<?php

namespace frontend\themes\mobile\assets;

use airani\bootstrap\BootstrapRtlAsset;
use Yii;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;

/**
 * Class CommonAsset
 * @package frontend\assets
 */
class CommonAsset extends \frontend\assets\CommonAsset
{
    public $sourcePath = '@frontend/themes/mobile/web';
    public $basePath = null;
    public $baseUrl = null;
    public $css = [
        'css/main.css',
        '/css/vendor/animate-button.css',
        '/css/vendor/icon.css'
    ];
    public $js = [
        '/js/script.js',
    ];
}
