<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2018
 * Time: 14:30
 */

use frontend\themes\mobile\assets\CommonAsset;
use yii\helpers\Html;
use common\widgets\Alert;

/*
 * @var $this \yii\web\View
 * @var $content string
 * @var array $categorySlugs
 */

CommonAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= in_array(Yii::$app->language, ['hi-HI', 'ar-AR']) ? 'rtl' : '' ?>">
<?php $this->beginBody() ?>
<div class="alert-container">
    <?= Alert::widget() ?>
</div>
<?= \frontend\widgets\MobileMenu::widget() ?>
<div class="mainblock">
    <?= \frontend\widgets\HeaderWidget::widget() ?>
    <div class="all-content clearfix">
        <?= $content ?>
    </div>

    <div class="mfooter"></div>
</div>

<?= \yii\bootstrap\Modal::widget([
    'id' => 'dynamic-modal',
    'options' => ['class' => 'fade new-modal modal480']
]) ?>

<?= \frontend\widgets\FooterWidget::widget(['template' => 'catalog-footer-widget']) ?>

<?php $script = <<<JS
    history.replaceState({state: true}, 'test');

    $(document).on('show.bs.modal', '.modal:not(.home-modal)', function () {
        var zIndex = 1050 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    $(document).on('hidden.bs.modal', '.modal ', function () {
        if($('.modal:visible').length > 0){
            $('body').addClass('modal-open');
        }
    });
    
JS;

$this->registerJs($script); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

