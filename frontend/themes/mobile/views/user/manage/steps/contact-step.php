<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<div>
    <?= $form->field($userForm, 'phone', [
        'options' => [
            'class' => 'form-group ' . (empty($userForm['phone']) ? '' : 'form-group-disabled')
        ]
    ])->textInput(['disabled' => !empty($userForm['phone'])]) ?>
</div>
<div>
    <?= $form->field($userForm, 'email', [
        'options' => [
            'class' => 'form-group ' . (empty($userForm['email']) ? '' : 'form-group-disabled')
        ]
    ])->textInput(['disabled' => !empty($userForm['email'])]) ?>
</div>
<div>
    <?= $form->field($userForm, 'username')->textInput() ?>
</div>
