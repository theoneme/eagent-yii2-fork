<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */
?>


<div>
    <?= $form->field($userForm->meta, 'subtitle')->textarea([
        'placeholder' => Yii::t('wizard', 'Profile Short Description'),
        'rows' => 4
    ]) ?>
</div>
<div>
    <?= $form->field($userForm->meta, 'description')->textarea([
        'placeholder' => Yii::t('wizard', 'Profile Description'),
        'rows' => 8
    ]) ?>
</div>



