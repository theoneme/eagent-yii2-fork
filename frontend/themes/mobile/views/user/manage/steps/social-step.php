<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

use yii\helpers\Html;

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 * @var array $userDTO
 */
?>

<?php foreach (Yii::$app->params['enabledSocials'] as $social) { ?>
    <div>
        <p>
            <?php if (array_key_exists($social, $userDTO['socials'])) { ?>
                <?= Html::a('<i class=icon-minus-symbol"></i>&nbsp;' . Yii::t('wizard', 'Detach {provider} account', ['provider' => ucfirst($social)]), [
                    '/auth/detach', 'provider' => $social
                ], ['class' => 'btn btn-small btn-white-blue wh-normal']) ?>
            <?php } else { ?>
                <?= Html::a('<i class="icon-plus-black-symbol"></i>&nbsp;' . Yii::t('wizard', 'Attach {provider} account', [
                        'provider' => ucfirst($social)
                    ]), "https://eagent.me/auth/social?authclient={$social}", ['class' => 'btn btn-small btn-white-blue wh-normal']) ?>
            <?php } ?>
        </p>
    </div>
<?php } ?>



