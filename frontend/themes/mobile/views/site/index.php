<?php

use common\models\Property;
use frontend\forms\contact\ContactForm;
use frontend\themes\mobile\assets\IndexAsset;
use frontend\widgets\IndexHelpWidget;
use frontend\widgets\IndexPortalWidget;
use frontend\widgets\IndexTabsWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var array $properties
 * @var array $localBuildings
 * @var array $globalBuildings
 * @var array $crosslinkData
 * @var string $address
 * @var string $operation
 */

IndexAsset::register($this);
\frontend\assets\plugins\SlickAsset::register($this);

?>

    <section class="banner-main text-main block-content">
        <div class="banner-center">
            <?= IndexTabsWidget::widget(['operation' => $operation, 'address' => $address]) ?>
        </div>
    </section>
    <section class="block-content">
        <div class="container-fluid-short">
            <div class="top-hr"></div>
            <h2 class="text-center"><?= Yii::t('index', 'Find out how to buy an apartment and save up to 20% of the cost'); ?></h2>
            <p class="text-center">
                <?= Yii::t('index', 'You should to answer 5 questions and we will send you for FREE a strategy: how to buy an apartment and how to save money on your purchase'); ?>
            </p>
            <div class="text-center">
                <a class="btn btn-big btn-white-blue">
                    <?= Yii::t('main', 'Pass the test'); ?>
                </a>
            </div>
        </div>
    </section>

    <section class="block-content">
        <div class="container">
            <h2 class="text-center">
                <?= Yii::t('index', 'Popular searches for buying property in {city}', ['city' => $address]) ?>
            </h2>
            <div class="clearfix special-block mob-slider">
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <div class="special-item">
                        <div class="special-img width">
                            <?= Html::img('/images/index/flat.jpg', [
                                'alt' => Yii::t('index', 'Apartments for sale'),
                                'title' => Yii::t('index', 'Apartments for sale'),
                            ]) ?>
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <?= $address ?>
                            </div>
                            <?= Html::a(Yii::t('index', 'Apartments for sale'), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['flats'] ?? 'flats'
                            ], ['class' => 'special-type']) ?>
                            <?= Html::a(Yii::t('index', '{rooms, plural, one{#-room} other{#-rooms}} apartments', ['rooms' => 1]), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['flats'] ?? 'flats',
                                'rooms' => [
                                    'min' => 1
                                ]
                            ], ['class' => 'special-line']) ?>
                            <?= Html::a(Yii::t('index', '{rooms, plural, one{#-room} other{#-rooms}} apartments', ['rooms' => 2]), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['flats'] ?? 'flats',
                                'rooms' => [
                                    'min' => 2
                                ]
                            ], ['class' => 'special-line']) ?>
                            <?= Html::a(Yii::t('index', '{rooms, plural, one{#-room} other{#-rooms}} apartments', ['rooms' => 3]), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['flats'] ?? 'flats',
                                'rooms' => [
                                    'min' => 3
                                ]
                            ], ['class' => 'special-line']) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <div class="special-item">
                        <div class="special-img width">
                            <?= Html::img('/images/index/house.jpg', [
                                'alt' => Yii::t('index', 'Houses for sale'),
                                'title' => Yii::t('index', 'Houses for sale'),
                            ]) ?>
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <?= $address ?>
                            </div>
                            <?= Html::a(Yii::t('index', 'Houses for sale'), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['houses'] ?? 'houses'
                            ], ['class' => 'special-type']) ?>
                            <?= Html::a(Yii::t('index', '{rooms, plural, one{#-room} other{#-rooms}} houses', ['rooms' => 3]), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['houses'] ?? 'houses',
                                'rooms' => [
                                    'min' => 3
                                ]
                            ], ['class' => 'special-line']) ?>
                            <?= Html::a(Yii::t('index', '{rooms, plural, one{#-room} other{#-rooms}} houses', ['rooms' => 4]), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['houses'] ?? 'houses',
                                'rooms' => [
                                    'min' => 4
                                ]
                            ], ['class' => 'special-line']) ?>
                            <?= Html::a(Yii::t('index', '{rooms, plural, one{#-room} other{#-rooms}} houses', ['rooms' => 5]), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['houses'] ?? 'houses',
                                'rooms' => [
                                    'min' => 5
                                ]
                            ], ['class' => 'special-line']) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <div class="special-item">
                        <div class="special-img width">
                            <?= Html::img('/images/index/commercial.jpg', [
                                'alt' => Yii::t('index', 'Commercial property for sale'),
                                'title' => Yii::t('index', 'Commercial property for sale'),
                            ]) ?>
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <?= $address ?>
                            </div>
                            <?= Html::a(Yii::t('index', 'Commercial property for sale'), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['commercial-property'] ?? 'commercial-property'
                            ], ['class' => 'special-type']) ?>
                            <?= Html::a(Yii::t('index', 'Offices'), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['commercial-property'] ?? 'commercial-property',
                            ], ['class' => 'special-line']) ?>
                            <?= Html::a(Yii::t('index', 'Warehouses'), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['commercial-property'] ?? 'commercial-property',
                            ], ['class' => 'special-line']) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <div class="special-item">
                        <div class="special-img width">
                            <?= Html::img('/images/index/land.jpg', [
                                'alt' => Yii::t('index', 'Land for sale'),
                                'title' => Yii::t('index', 'Land for sale'),
                            ]) ?>
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <?= $address ?>
                            </div>
                            <?= Html::a(Yii::t('index', 'Land for sale'), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['land'] ?? 'land'
                            ], ['class' => 'special-type']) ?>
                            <?= Html::a(Yii::t('index', 'Commercial land'), [
                                '/property/catalog/index',
                                'operation' => Property::OPERATION_SALE,
                                'category' => $categorySlugs['land'] ?? 'land',
                            ], ['class' => 'special-line']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a class="btn btn-big btn-white-blue" href="#"><?= Yii::t('index', 'Real estate sale catalog') ?></a>
            </div>
        </div>
    </section>

<?php if (!empty($properties)) { ?>
    <section class="block-content">
        <div class="container">
            <h2 class="text-center">
                <?= Yii::t('index', 'New offers of real estate for sale in {city}', ['city' => $address]) ?>
            </h2>
            <div class="clearfix special-block mob-slider">
                <?php foreach ($properties as $property) { ?>
                    <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                        <div class="special-item">
                            <div class="special-img width">
                                <a href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                                   data-action="load-modal-property">
                                    <?= Html::img($property['image'], ['alt' => $property['title'], 'title' => $property['title']]) ?>
                                </a>
                            </div>
                            <div class="special-info">
                                <div class="special-text">
                                    <?= Yii::t('index', '{category}-{city}', ['category' => Yii::t('index', 'Apartments'), 'city' => $address]) ?>
                                </div>
                                <a class="special-type"
                                   href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                                   data-action="load-modal-property">
                                    <?= $property['title'] ?>
                                </a>
                                <div class="special-descr">
                                    <?= !empty($property['description'])
                                        ? \yii\helpers\StringHelper::truncate($property['description'], 85)
                                        : Yii::t('index', 'Description is missing') ?>
                                </div>
                                <div class="flex space-between">
                                    <div class="special-price"><?= $property['price'] ?></div>
                                    <a class="special-more"
                                       href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                                       data-action="load-modal-property">
                                        <?= Yii::t('index', 'Watch more'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="text-center">
                <a class="btn btn-big btn-white-blue"
                   href="<?= Url::to(['/property/catalog/index', 'category' => 'flats', 'operation' => Property::OPERATION_SALE]) ?>">
                    <?= Yii::t('index', 'More properties'); ?>
                </a>
            </div>
        </div>
    </section>
<?php } ?>

    <section class="block-content">
        <?= IndexPortalWidget::widget(['operation' => $operation]) ?>
    </section>
<?php if (!empty($localBuildings)) { ?>
    <section class="block-content">
        <div class="container">
            <h2 class="text-center">
                <?= Yii::t('index', 'Building offers for sale in {city}', ['city' => $address]) ?>
            </h2>
            <div class="clearfix special-block mob-slider">
                <?php foreach ($localBuildings as $building) { ?>
                    <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                        <div class="special-item">
                            <div class="special-img width">
                                <a href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']]) ?>"
                                   data-action="load-modal-property">
                                    <?= Html::img($building['image'], ['alt' => $building['title'], 'title' => $building['title']]) ?>
                                </a>
                            </div>
                            <div class="special-info">
                                <?php if (isset($building['attributes']['new_construction_builder'])) { ?>
                                    <div class="special-text">
                                        <?= Yii::t('index', 'Developer - {developer}', [
                                            'developer' => $building['attributes']['new_construction_builder']
                                        ]) ?>
                                    </div>
                                <?php } ?>
                                <a class="special-type"
                                   href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']]) ?>"
                                   data-action="load-modal-property">
                                    <?= \common\helpers\UtilityHelper::upperFirstLetter($building['name']) ?>
                                </a>
                                <?php if (isset($building['attributes']['ready_year'])) { ?>
                                    <div class="special-line line-grey">
                                        <?= Yii::t('index', 'Ready date - <span>{quarter}{year}</span>', [
                                            'quarter' => isset($building['attributes']['ready_quarter'])
                                                ? Yii::t('index', '{quarter} quarter', [
                                                    'quarter' => $building['attributes']['ready_quarter']
                                                ])
                                                : null,
                                            'year' => $building['attributes']['ready_year']]) ?>
                                    </div>
                                <?php } ?>
                                <?php if (isset($building['attributes']['new_construction_discount'])) { ?>
                                    <div class="special-line line-grey">
                                        <?= Yii::t('index', 'Discount - <span>{percent}%</span>', [
                                            'percent' => $building['attributes']['new_construction_discount']
                                        ]) ?>
                                    </div>
                                <?php } ?>

                                <div class="flex space-between">
                                    <?= Html::a(Yii::t('index', 'Watch more'), [
                                        '/building/building/show', 'slug' => $building['slug']
                                    ], ['class' => 'special-more', 'data-action' => 'load-modal-property']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>
    <section class="block-content">
        <?= IndexHelpWidget::widget(['operation' => $operation]) ?>
    </section>
<?php if (!empty($globalBuildings)) { ?>
    <section class="block-content">
        <div class="container">
            <h2 class="text-center">
                <?= Yii::t('index', 'Property offers in other cities') ?>
            </h2>
            <div class="clearfix special-block mob-slider">
                <?php foreach ($globalBuildings as $building) { ?>
                    <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                        <div class="special-item">
                            <div class="special-img width">
                                <a href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']]) ?>"
                                   data-action="load-modal-property">
                                    <?= Html::img($building['image'], ['alt' => $building['title'], 'title' => $building['title']]) ?>
                                </a>
                            </div>
                            <div class="special-info">
                                <?php if (isset($building['attributes']['new_construction_builder'])) { ?>
                                    <div class="special-text">
                                        <?= Yii::t('index', 'Developer - {developer}', [
                                            'developer' => $building['attributes']['new_construction_builder']
                                        ]) ?>
                                    </div>
                                <?php } ?>
                                <a class="special-type"
                                   href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']]) ?>"
                                   data-action="load-modal-property">
                                    <?= \common\helpers\UtilityHelper::upperFirstLetter($building['name']) ?>
                                </a>
                                <?php if (isset($building['attributes']['ready_year'])) { ?>
                                    <div class="special-line line-grey">
                                        <?= Yii::t('index', 'Ready date - <span>{quarter}{year}</span>', [
                                            'quarter' => isset($building['attributes']['ready_quarter'])
                                                ? Yii::t('index', '{quarter} quarter', [
                                                    'quarter' => $building['attributes']['ready_quarter']
                                                ])
                                                : null,
                                            'year' => $building['attributes']['ready_year']]) ?>
                                    </div>
                                <?php } ?>
                                <?php if (isset($building['attributes']['new_construction_discount'])) { ?>
                                    <div class="special-line line-grey">
                                        <?= Yii::t('index', 'Discount - <span>{percent}%</span>', [
                                            'percent' => $building['attributes']['new_construction_discount']
                                        ]) ?>
                                    </div>
                                <?php } ?>

                                <div class="flex space-between">
                                    <?= Html::a(Yii::t('index', 'Watch more'), [
                                        '/building/building/show', 'slug' => $building['slug']
                                    ], ['class' => 'special-more', 'data-action' => 'load-modal-property']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>
<?= \frontend\widgets\LocationWidget::widget() ?>

    <div class="modal fade contact-us new-modal modal480" id="contactModal" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-md" style="margin: 160px auto">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="modal-title text-center"><?= Yii::t('main', 'Get a free estimate of your property') ?></div>
                </div>
                <div class="modal-body">
                    <?= $this->render('@frontend/views/contact/form', ['model' => new ContactForm()]) ?>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
     /*$(document).on('click', '.job-search-toggle', function() {
        $(this).closest('.job-search-toggle-block').slideUp(400, function() {
            $('.help-content').slideDown(400);
        });
        
        return false;
    });*/
     
    $(document).on('show.bs.modal', '.modal:not(.home-modal)', function() {
        var zIndex = 1050 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    $(document).on('hidden.bs.modal', '.modal', function() {
        if ($('.modal:visible').length > 0) {
            $('body').addClass('modal-open');
        }
    });
    
    var rtlSlider = false;
    if($('body').hasClass('rtl')) {
           rtlSlider = true;
    }
    
    $(".mob-slider").slick({
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: false,
        speed: 300,
		centerMode: false,
        variableWidth: true,
        rtl: rtlSlider
	});
    
    $(window).on("scroll", function() {
        if ($(window).scrollTop() > 50) {
            $('.fixed-top').addClass('white-top');
        } else {
                $('.fixed-top').removeClass('white-top');
        }
    });
JS;
$this->registerJs($script);
