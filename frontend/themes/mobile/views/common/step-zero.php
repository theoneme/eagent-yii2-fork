<?php

use frontend\assets\plugins\MoreLessAdvancedAsset;
use frontend\forms\StepZeroForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

frontend\themes\mobile\assets\WizardAsset::register($this);
MoreLessAdvancedAsset::register($this);

/**
 * @var array $categories
 * @var string $type
 * @var StepZeroForm $model
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'List your property or request for Free') ?></h1>
        <div class="wizard-content">
            <div class="wizard-row">
                <div class="wizards-steps">
                    <?php $form = ActiveForm::begin([
                        'method' => 'get',
                        'action' => ['/property/manage/create'],
                        'fieldConfig' => [
                            'template' => "{input}{error}",
                        ],
                        'options' => [
                            'id' => 'step-zero-form',
                        ],
                    ]); ?>
                    <section id="step1">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Please choose type of your advertisement:') ?>
                        </div>
                        <div class="step-content">
                            <?= $form->field($model, 'type')->radioList($model->types, [
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    //                                            $chk = $checked ? 'checked' : '';
                                    $output = "
                                                <div class='chover'>" .
                                        Html::radio($name, false, [
                                            'id' => "type-$value",
                                            'value' => $label['value'],
                                            'class' => 'radio-checkbox property-type',
                                            'data-action' => Url::to($label['action']),
                                        ]) .
                                        Html::label($label['label'], "type-$value") .
                                        "</div>";
                                    return $output;
                                },
                            ]) ?>
                        </div>
                    </section>
                    <section id="step2" data-lvl="1">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Property category') ?>
                        </div>
                        <div class="step-content">
                            <?= $this->render('@frontend/views/common/category-input', [
                                'model' => $model,
                                'items' => $model->categories,
                                'form' => $form,
                                'field' => 'parent_category_id',
                                'index' => 1,
                                'createForm' => false
                            ]) ?>
                        </div>
                    </section>
                    <section id="step3" class="subtype-step" data-lvl="2">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Property subcategory') ?>
                        </div>
                        <div class="step-content"></div>
                    </section>

                    <section id="step4">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Language') ?>
                        </div>
                        <div class="step-content">
                            <div class="type-adv">
                                <?= $form->field($model, 'locale')->radioList($model->locales, [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        $chk = $checked ? 'checked' : '';
                                        $output = '<div class="chover">' .
                                            Html::radio($name, $chk, [
                                                'id' => "locale-$index",
                                                'value' => $value,
                                                'class' => 'radio-checkbox'
                                            ]) .
                                            Html::label(Yii::t('main', $label), "locale-$index") .
                                           '</div>';
                                        return $output;
                                    },
                                ]) ?>
                            </div>
                        </div>
                    </section>
                    <div class="text-center">
                        <input id="contact-agent" type="submit" class="btn btn-big btn-blue-white"
                               value="<?= Yii::t('wizard', 'Save and Continue') ?>">
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
<?php $renderAttributesRoute = Url::toRoute('/property/property/attributes');
$showLanguagesText = Yii::t('wizard', 'Show other languages');
$hideLanguagesText = Yii::t('wizard', 'Hide other languages');
$renderCategoriesRoute = Url::toRoute('/ajax/render-category-input');

$canonical = Url::canonical();
$script = <<<JS
    $('input.property-type').change(function() {
        $('#step-zero-form').attr('action', $(this).data('action'));
    });
    $('input.property-type:checked').trigger('change');

    $('.field-stepzeroform-locale').MoreLessAdvanced({
        withArrows: false,
        moreText: '{$showLanguagesText}',
        lessText: '{$hideLanguagesText}',
        containerSelector: '.field-stepzeroform-locale',
        itemSelector: '.chover',
    });
    
    $('.step-zero-category-input').on('change', function() {
        let lvl = $(this).data('lvl') + 1;
        let container = $('section[data-lvl="' + lvl + '"]');
        if (container.length) {
            let val = $(this).val();
            if (val) { 
                $.get('$renderCategoriesRoute', {
                    index: lvl,
                    parent: val
                }, function(result) {
                    if(result.success === true) {
                        container.find('.step-content').html(result.html);
                        container.addClass('active');
                    }
                });   
            } else {
                container.removeClass('active');
            }
        }
        
        return false;
    });
JS;
$this->registerJs($script);

?>