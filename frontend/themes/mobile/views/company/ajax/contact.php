<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.12.2018
 * Time: 14:26
 */

use yii\helpers\Json;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var \common\forms\ar\composite\ContactForm $contactForm
 * @var boolean $createForm
 * @var ActiveForm|null $form
 * @var boolean $createForm
 * @var integer $iterator
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = new ActiveForm([
        'id' => 'company-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]);
    ob_end_clean(); ?>
<?php } ?>

    <div>
        <div class="row" data-role="contact-item">
            <div class="col-xs-12">
                <?= $form->field($contactForm, "[{$iterator}]type")->dropDownList(\common\helpers\DataHelper::getContacts(), [
                    'prompt' => '-- ' . Yii::t('wizard', 'Select contact type')
                ])->label(false) ?>
            </div>
            <div class="col-xs-10">
                <?= $form->field($contactForm, "[{$iterator}]value")->textInput(['placeholder' => Yii::t('wizard', 'Enter contact info')])->label(false) ?>
            </div>
            <div class="col-xs-2">
                <?= Html::a('<i class="icon-close"></i>', '#', ['data-action' => 'remove-contact']) ?>
            </div>
        </div>
    </div>

<?php if ($createForm === true) { ?>
    <?php $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    var attributes = $attributes;
    $.each(attributes, function() {
        $("#company-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);
} ?>