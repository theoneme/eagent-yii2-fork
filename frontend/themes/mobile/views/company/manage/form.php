<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use common\forms\ar\CompanyForm;
use frontend\controllers\company\ManageController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\common\assets\GoogleAsset::register($this);
frontend\themes\mobile\assets\WizardAsset::register($this);

/**
 * @var CompanyForm $companyForm
 * @var array $route
 * @var integer $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'Post Company') ?></h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($route),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div>
                    {input}
                    {error}
            </div>",
            ],
            'options' => [
                'id' => 'company-form',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <div class="wizard-row">
                <div class="wizards-steps">
                    <?php foreach ($companyForm->steps as $key => $step) { ?>
                        <section id="step<?= $key ?>">
                            <div class="wizard-step-header">
                                <?= $step['title'] ?>
                                <span>
                                        <?= Yii::t('wizard', 'Step {first} from {total}', [
                                            'first' => $key,
                                            'total' => count($companyForm->steps)
                                        ]) ?>
                                    </span>
                            </div>
                            <div class="step-content">
                                <?php foreach ($step['config'] as $stepConfig) { ?>
                                    <?php if ($stepConfig['type'] === 'view') { ?>
                                        <?= $this->render("steps/{$stepConfig['value']}", [
                                            'form' => $form,
                                            'companyForm' => $companyForm,
                                            'step' => $step,
                                            'createForm' => false
                                        ]) ?>
                                    <?php } ?>

                                    <?php if ($stepConfig['type'] === 'constructor') { ?>
                                        <?php foreach ($stepConfig['groups'] as $group) { ?>
                                            <?php $attributes = $companyForm->dynamicForm->getAttributesByGroup($group) ?>
                                            <?= $this->render("partial/attributes-group", [
                                                'form' => $form,
                                                'attributes' => array_flip($attributes),
                                                'dynamicForm' => $companyForm->dynamicForm
                                            ]) ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </section>
                    <?php } ?>
                    <div class="text-center">
                        <?= Html::submitButton(Yii::t('wizard', 'Post Company'), [
                            'class' => 'btn btn-big btn-white-blue'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php $canonical = Url::canonical();
$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
$script = <<<JS
var calculatorFlag = false,
    isUpdate = {$isUpdate},
    validationPerformed = false,
    progressCalculator = new ProgressCalculator({
        formSelector: '#company-form'
    }),
    form = $('#company-form');

if(isUpdate) {
    form.yiiActiveForm('validate', true);
    progressCalculator.updateProgress();
} else {
    validationPerformed = true
}

form.on('submit', function() { 
    calculatorFlag = true;
    let hasToUpload = false;
    $.each($(".file-upload-input"), function() {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            hasToUpload = true;
            return false;
        }
    });

    if(hasToUpload === false) {
        if(validationPerformed === true) { 
             return true;
        } else { 
            validationPerformed = true;
            return false;
        }
    } else {
        return false;
    }
}).on("afterValidateAttribute", function(event, attribute, messages) {
    if(calculatorFlag === false) {
        progressCalculator.updateProgress();
    }
}).on("afterValidate", function(event, messages, errorAttributes) { 
    progressCalculator.updateProgress();
    calculatorFlag = false;
});

JS;
$this->registerJs($script);

?>