<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $companyForm
 * @var \yii\widgets\ActiveForm $form
 */
?>

<div>
    <?= $form->field($companyForm->meta, 'title')->textInput([
        'placeholder' => Yii::t('wizard', 'Company Title'),
    ]) ?>
</div>
<div>
    <?= $form->field($companyForm->meta, 'description')->textarea([
        'placeholder' => Yii::t('wizard', 'Company Description'),
        'rows' => 4
    ]) ?>
</div>



