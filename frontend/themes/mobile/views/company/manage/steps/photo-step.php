<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var array $step
 * @var \frontend\forms\ar\CompanyForm $companyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

    <p>
        <?= Yii::t('wizard', 'Upload Company Logo') ?>
    </p>
    <div class="file-input-container">
        <div>
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'file-upload-input',
                    'name' => 'uploaded_images',
                    'options' => ['multiple' => false, 'class' => 'file-upload-input'],
                    'pluginOptions' => [
                        'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                        'overwriteInitial' => true,
                        'initialPreview' => !empty($companyForm->_company->logo) ? $companyForm->logo : [],
                        'initialPreviewConfig' => !empty($companyForm->_company->logo) ? [[
                            'caption' => basename($companyForm->logo),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => 0
                        ]] : [],
                    ]
                ])
            ) ?>
        </div>
        <div class="images-container" data-field="logo">
            <?= $form->field($companyForm, 'logo', ['template' => '{input}'])->hiddenInput([
                'value' => FileInputHelper::buildOriginImagePath($companyForm->logo),
                'data-key' => 'image_init_0'
            ])->label(false) ?>
        </div>
    </div>
    <p>&nbsp;</p>

    <p>
        <?= Yii::t('wizard', 'Upload Company Banner') ?>
    </p>
    <div class="file-input-container">
        <div>
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'file-zupload-input',
                    'name' => 'uploaded_images',
                    'options' => ['multiple' => false, 'class' => 'file-upload-input'],
                    'pluginOptions' => [
                        'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                        'overwriteInitial' => true,
                        'initialPreview' => !empty($companyForm->_company->banner) ? $companyForm->banner : [],
                        'initialPreviewConfig' => !empty($companyForm->_company->banner) ? [[
                            'caption' => basename($companyForm->banner),
                            'url' => Url::toRoute('/image/delete'),
                            'key' => 0
                        ]] : [],
                    ]
                ])
            ) ?>
        </div>
        <div class="images-container" data-field="banner">
            <?= $form->field($companyForm, 'banner', ['template' => '{input}'])->hiddenInput([
                'value' => FileInputHelper::buildOriginImagePath($companyForm->banner),
                'data-key' => 'image_init_0'
            ])->label(false) ?>
        </div>
    </div>
    <p>&nbsp;</p>

<?php $script = <<<JS
    let hasFileUploadError = false;
    $(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').find('.images-container');
        imagesContainer.find('input').val(response.uploadedPath);
    }).on("filedeleted", function(event, key) {
        $(this).closest('.file-input-container').children('.images-container').find("input[data-key='" + key + "']").val('');
    }).on("filebatchuploadcomplete", function() {
        if (hasFileUploadError === false) { 
            $('#company-form').submit();
        } else { 
            hasFileUploadError = false;
        }
    }).on("fileuploaderror", function(event, data, msg) { 
        hasFileUploadError = true;
        $('#' + data.id).find('.kv-file-remove').click();
    });
JS;

$this->registerJs($script);
