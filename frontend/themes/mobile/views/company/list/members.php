<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

\frontend\themes\mobile\assets\RequestAsset::register($this);

/**
 * @var array $members
 * @var View $this
 * @var integer $companyId
 */

?>

<div class="catalog-wrap">
    <div class="mobile-request">
        <h1 class="text-left"><?= Yii::t('account', 'Company Members') ?></h1>


        <?php if (count($members['items']) > 0) { ?>
            <?php foreach ($members['items'] as $member) { ?>
                <div class="mobile-request-item flex space-between">
                    <div class="mobile-request-img">
                        <?= Html::img($member['avatar'], ['class' => 'list-image-preview']) ?>
                    </div>
                    <div class="mobile-request-item-info">
                        <div class="mobile-request-row">
                            <div class="mobile-request-id">
                                <a href="<?= Url::to(['/agent/agent/view', 'id' => $member['user_id']]) ?>">
                                    <?= $member['username'] ?>
                                </a>
                            </div>
                            <div class="mobile-request-title">
                                <?= \common\decorators\CompanyMemberRoleDecorator::decorate($member['role']) ?>
                            </div>
                        </div>
                        <div class="flex space-between">
                            <div class="mobile-request-price">
                                <?= \common\decorators\CompanyMemberStatusDecorator::decorate($member['status']) ?>
                            </div>
                            <div>
                                <a href="<?= Url::to(['/company/manage-member/update', 'id' => $member['id'], 'company_id' => $member['company_id']]) ?>">
                                    <i class="icon-pencil"></i>
                                </a>
                                <?= Html::a('<i class="iocn-delete-button"></i>', [
                                    '/company/manage-member/delete', 'id' => $member['id'], 'company_id' => $member['company_id']
                                ], [
                                    'data' => [
                                        'confirm' => Yii::t('account', 'Are you sure?'),
                                        'method' => 'post'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if ($members['pagination']) { ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $members['pagination'],
                ]); ?>
            <?php } ?>
        <?php } else { ?>
            <p><?= Yii::t('main', 'No results found') ?></p>
        <?php } ?>

        <ul class="flex no-list flex-wrap object-status-switcher">
            <li>
                <?= Html::a(Yii::t('account', 'Back to companies'), ['/company/list/index'], [
                    'title' => Yii::t('account', 'Back to companies'),
                    'class' => 'btn btn-success',
                ]) ?>
            </li>
            <li>
                <?= Html::a(Yii::t('account', 'Add Company Member'), ['/company/manage-member/create', 'company_id' => $companyId], [
                    'class' => 'btn btn-success'
                ]) ?>
            </li>
        </ul>
    </div>
</div>
