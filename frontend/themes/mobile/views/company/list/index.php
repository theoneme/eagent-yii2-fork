<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

\frontend\themes\mobile\assets\RequestAsset::register($this);

/**
 * @var array $companies
 * @var View $this
 */

?>

<div class="catalog-wrap">
    <div class="mobile-request">
        <h1 class="text-left"><?= Yii::t('account', 'My Companies') ?></h1>

        <div class='form-group'>
            <?= Html::a(Yii::t('account', 'Create Company'), ['/company/manage/create'], [
                'class' => 'btn btn-success'
            ]) ?>
        </div>

        <?php if (count($data['items']) > 0) { ?>
            <?php foreach ($data['items'] as $company) { ?>
                <div class="mobile-request-item flex space-between">
                    <div class="mobile-request-img">
                        <?= Html::img($company['companyLogo'], ['class' => 'list-image-preview']) ?>
                    </div>
                    <div class="mobile-request-item-info">
                        <div class="mobile-request-row">
                            <div class="mobile-request-id">
                                <?= $company['id'] ?>
                            </div>
                            <div class="mobile-request-title">
                                <a href="<?= Url::to(['/agent/agent/view', 'id' => $company['userId']]) ?>">
                                    <?= $company['companyTitle'] ?>
                                </a>
                            </div>
                        </div>
                        <div class="flex space-between">
                            <div class="mobile-request-price">
                                <?= \common\decorators\CompanyMemberRoleDecorator::decorate($company['role']) ?>
                            </div>
                            <div>
                                <a href="<?= Url::to(['/company/list/members', 'company_id' => $company['id']]) ?>"><i class="icon-multiple-users-silhouette"></i></a>
                                <a href="<?= Url::to(['/company/manage/update', 'id' => $company['id']]) ?>"><i class="icon-pencil"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if ($data['pagination']) { ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $data['pagination'],
                ]); ?>
            <?php } ?>
        <?php } else { ?>
            <p><?= Yii::t('main', 'No results found') ?></p>
        <?php } ?>
    </div>
</div>
