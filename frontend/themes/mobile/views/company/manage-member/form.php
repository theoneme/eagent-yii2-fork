<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\frontend\assets\CatalogAsset::register($this);
\frontend\themes\mobile\assets\WizardAsset::register($this);
\frontend\assets\plugins\SelectizeAsset::register($this);

/**
 * @var \common\forms\ar\CompanyMemberForm $companyMemberForm
 * @var array $selectizeData
 * @var array $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left">
            <?= Yii::t('wizard', 'Edit Company Member') ?>
        </h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($action),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div>
                    {label}
                    {input}
                    {error}
            </div>",
            ],
            'options' => [
                'id' => 'company-member-form',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <div class="wizard-row">
                    <div class="wizards-steps">
                        <section id="step1">
                            <div class="step-content">
                                    <div>
                                        <?= $form->field($companyMemberForm, 'role')->dropDownList(\common\decorators\CompanyMemberRoleDecorator::getRoleLabels(false)) ?>
                                    </div>

                                    <div>
                                        <?= $form->field($companyMemberForm, 'status')->dropDownList(\common\decorators\CompanyMemberStatusDecorator::getRoleLabels(false)) ?>
                                    </div>

                                    <div>
                                        <?= $form->field($companyMemberForm, 'user_id')->dropDownList($selectizeData, ['class' => 'user-autocomplete']) ?>
                                    </div>
                            </div>
                        </section>
                        <div class="text-center">
                            <?= Html::submitButton(Yii::t('wizard', 'Edit Company Member'), [
                                'class' => 'btn btn-big btn-white-blue'
                            ]) ?>
                        </div>
                    </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>

<?php $url = Url::to(['/user/ajax/users-selectize']);

$script = <<<JS
    $(".user-autocomplete").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.get("$url", {
                    request: query
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
        render: {
	        option: function(data, escape) { 
	            return '<div class="option custom-selectize-option flex flex-v-center" data-value="' + data.id + '">' +
                           '<img src="' + data.img + '">' +
                           '<span class="selectize-option-title">' + data.value + '</span>' + 
                       '</div>';
	        },
            item: function(data, escape) { 
	            return '<div class="item custom-selectize-item" data-value="' + data.value + '">' +
                          '<img src="' + data.img + '">' 
                          + data.value + 
                       '</div>';
	        }
	    },
    });
JS;

$this->registerJs($script);