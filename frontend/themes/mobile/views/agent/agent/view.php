<?php

use common\models\Property;
use common\models\Translation;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

frontend\themes\mobile\assets\AgentAsset::register($this);

/**
 * @var View $this
 * @var array $agent
 * @var array $properties
 * @var array $soldProperties
 * @var array $reviews
 * @var array $propertyMarkers
 * @var array $userMarker
 * @var boolean $showLoginModal
 * @var \frontend\forms\contact\ContactAgentForm $contactAgentForm
 */

?>

<?= \frontend\widgets\mobile\BottomAgentMenu::widget(['object' => $agent]) ?>

    <div class="page-wrap">
        <div class="agent-profile-content">
            <div class="catalog-left">
                <div class="agent-main-info" data-role="marker-data" data-property="<? //= $userMarker ?>">
                    <div class="agent-ph ph-big">
                        <?= Html::img($agent['avatar'], ['alt' => $agent['username'], 'title' => $agent['username']]) ?>
                    </div>
                    <div class="agent-main-right">
                        <h1 class="text-center">
                            <?= $agent['username'] ?>
                            <!--                            <div class="agent-status text-center">-->
                            <!--                                --><? //= Yii::t('main', 'Premium agent') ?>
                            <!--                            </div>-->
                        </h1>
                        <div class="agent-activity text-center">
                            <div><?= Yii::t('agent', 'All Activity') ?></div>
                            <div class="agent-activity-rate">
                                <i class="icon-favorite"></i>
                                <div class="rate-number">5
                                    <span>/5</span>
                                </div>
                                <?= !empty($agent['reviews']) ? Html::a(Yii::t('agent', '{count} Reviews', ['count' => count($agent['reviews'])])) : '' ?>
                            </div>
                            <div>
                                <?= !empty($agent['sales']) ? Yii::t('agent', '{count} Sales Last year', ['count' => count($agent['sales'])]) : '' ?>
                            </div>
                        </div>
                    </div>
                    <div class="contact-link">
                        <a href="#contact-us"
                           class="btn btn-small btn-blue-white width100"><?= Yii::t('main', 'Contact us') ?></a>
                    </div>
                </div>
                <?php if (array_key_exists(Translation::KEY_SUBTITLE, $agent['translations']) || array_key_exists(Translation::KEY_DESCRIPTION, $agent['translations'])) { ?>
                    <div class="agent-description">
                        <h2 class="bold-h2 text-left"><?= Yii::t('agent', 'About Us') ?></h2>
                        <div class="agent-sub-title">
                            <?= $agent['translations'][Translation::KEY_SUBTITLE] ?? '' ?>
                        </div>
                        <div class="hide-content hide-text">
                            <div>
                                <?= $agent['translations'][Translation::KEY_DESCRIPTION] ?? '' ?>
                            </div>
                        </div>
                        <a class="more-less" data-height="120">
                            <?= Yii::t('main', 'More') ?>
                            <i class="icon-down"></i>
                        </a>
                    </div>
                <?php } ?>
                <div class="our-sales hide">
                    <h2 class="bold-h2 text-left"><?= Yii::t('agent', 'Our Listings & Sales') ?></h2>
                    <div class="our-sales-map">

                    </div>
                </div>
                <div class="homes-list">
                    <h2 class="bold-h2 text-left">
                        <?= Yii::t('agent', 'Our active listings') ?>
                    </h2>
                    <div class="homes-list-count">(<?= $properties['pagination']->totalCount ?? 0 ?>)</div>
                    <div class="homes-table homes-active">
                        <div class="homes-row homes-header">
                            <div class="homes-title">
                                <?= Yii::t('agent', 'Property Address') ?>
                            </div>
                            <div class="homes-opts price">
                                <?= Yii::t('main', 'Price') ?>
                            </div>
                        </div>
                        <?php foreach ($properties['items'] as $property) {
                            echo $this->render('property-active', ['property' => $property,/* 'marker' => $propertyMarkers[$property['id']] ?? []*/]);
                        } ?>
                    </div>
                    <div class="flex space-between">
                        <?php if ($properties['pagination']) {
                            echo LinkPager::widget(['pagination' => $properties['pagination']]);
                        } ?>
                        <!--                        <a class="asCenter hidden-xs" href="#">View on a map</a>-->
                    </div>
                </div>

                <?php if (!empty($agent['sales'])) { ?>
                    <div class="homes-list">
                        <h2 class="bold-h2 text-left">
                            <?= Yii::t('agent', 'Our Past Sales') ?>
                        </h2>
                        <div class="homes-list-count">(0)</div>
                        <div class="homes-table homes-sold">
                            <div class="homes-row homes-header">
                                <div class="homes-title">
                                    <?= Yii::t('agent', 'Property Address') ?>
                                </div>
                                <div class="homes-opts represented">
                                    <?= Yii::t('agent', 'Represented') ?>
                                </div>
                            </div>
                            <?php foreach ($soldProperties['items'] as $property) {
                                echo $this->render('property-sold', ['property' => $property]);
                            } ?>
                        </div>
                        <?php if ($soldProperties['pagination']) {
                            echo LinkPager::widget(['pagination' => $soldProperties['pagination']]);
                        } ?>
                    </div>
                <?php } ?>

                <?php if (!empty($agent['reviews'])) { ?>
                <div class="reviews-list-block">
                    <div class="flex space-between review-header">
                        <h2 class="bold-h2 text-left">
                            <?= Yii::t('agent', 'Ratings & Reviews') ?>
                        </h2>
                        <a class="btn btn-small btn-white-blue asCenter" href="#">
                            <?= Yii::t('agent', 'Write a review') ?>
                        </a>
                    </div>
                    <div class="review-list">
                        <?php foreach ($agent['reviews'] as $review) {
                            echo $this->render('review-item', ['review' => $review]);
                        } ?>
                    </div>
                    <?php } ?>

                    <?php if (!empty($cities)) { ?>
                        <div class="service-areas">
                            <h2 class="bold-h2 text-left"><?= Yii::t('agent', 'Search property in cities') ?></h2>
                            <div class="service-areas-row">
                                <?php foreach ($cities as $city) { ?>
                                    <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                        'entity' => Yii::t('main', 'Condos'), 'city' => $city['title']
                                    ]), ['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                    <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                        'entity' => Yii::t('main', 'Houses'), 'city' => $city['title']
                                    ]), ['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                    <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                        'entity' => Yii::t('main', 'Land'), 'city' => $city['title']
                                    ]), ['/property/catalog/index', 'category' => $categorySlugs['land'] ?? 'land', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                    <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                        'entity' => Yii::t('main', 'Commercial Property'), 'city' => $city['title']
                                    ]), ['/property/catalog/index', 'category' => $categorySlugs['commercial-property'] ?? 'commercial-property', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div id="contact-us">
                    <div class="aside-profile-block">
                        <h3><?= Yii::t('main', 'Contact us') ?></h3>
                        <?php $form = ActiveForm::begin([
                            'action' => Url::to(['/contact/contact-agent']),
                            'id' => 'bottom-contact-agent-form',
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                        ]); ?>
                        <?= Html::hiddenInput('ContactAgentForm[agentId]', $agent['id']) ?>
                        <div>
                            <?= $form->field($contactAgentForm, 'name')->textInput([
                                'placeholder' => Yii::t('main', 'Your Name') . '*',
                                'id' => 'bottom-contact-agent-your-name',
                                'data-role' => 'name-field'
                            ])->label(false) ?>
                            <?= $form->field($contactAgentForm, 'phone')->textInput([
                                'placeholder' => Yii::t('main', 'Phone'),
                                'id' => 'bottom-contact-agent-phone',
                            ])->label(false) ?>
                            <?= $form->field($contactAgentForm, 'email')->textInput([
                                'placeholder' => Yii::t('main', 'Email') . '*',
                                'id' => 'bottom-contact-agent-email',
                                'data-role' => 'email-field'
                            ])->label(false) ?>
                            <?= $form->field($contactAgentForm, 'message')->textarea([
                                'value' => Yii::t('property', 'I am interested in {property}', ['property' => '...']),
                                'id' => 'bottom-contact-agent-message',
                            ])->label(false) ?>

                            <div class="animate-input text-center">
                                <input id="contact-agent-bottom" type="submit"
                                       class="btn btn-small btn-blue-white width100"
                                       value="<?= Yii::t('main', 'Contact Agent') ?>">
                                <label for="contact-agent-bottom" class="animate-button">
                                    <div class="btn-wrapper">
                                        <div class="btn-original"><?= Yii::t('main', 'Contact Agent') ?></div>
                                        <div class="btn-container">
                                            <div class="left-circle"></div>
                                            <div class="right-circle"></div>
                                            <div class="mask"></div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    <div class="aside-profile-block">
                        <h3>
                            <?= Yii::t('agent', 'Professional Information') ?>
                        </h3>
                        <div class="prof-table">
                            <?php if ($agent['email']) { ?>
                                <div class="prof-row">
                                    <div class="prof-property">
                                        <?= Yii::t('main', 'Email') ?>:
                                    </div>
                                    <div class="prof-descr">
                                        <?= $agent['email'] ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($agent['phone']) { ?>
                                <div class="prof-row">
                                    <div class="prof-property">
                                        <?= Yii::t('main', 'Phone') ?>:
                                    </div>
                                    <div class="prof-descr">
                                        <?= $agent['phone'] ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if (!empty($agent['address'])) { ?>
                                <div class="prof-row">
                                    <div class="prof-property">
                                        <?= Yii::t('main', 'Address') ?>:
                                    </div>
                                    <div class="prof-descr">
                                        <?= $agent['address'] ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $markerUserViewUrl = Url::to(['/agent/marker/view']);
$markerPropertyViewUrl = Url::to(['/property/marker/view']);
$markerUserIcon = Url::to(['/images/user-marker.png'], true);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');

$script = <<<JS
    let moreLess = $('.more-less');
    moreLess.MoreLess({
        moreText: '{$more}&nbsp;',
        lessText: '{$less}&nbsp;'
    });
    moreLess.trigger('show');
    
    /*let map = new CatalogMap({
        markerViewRoutes: {
            user: '{$markerUserViewUrl}',  
            property: '{$markerPropertyViewUrl}',  
        },
        markerIcons: {
            user: '{$markerUserIcon}',
            property: '{$markerPropertyIcon}'
        }
    });
    map.init();
    map.parseMarkers();*/
JS;

$this->registerJs($script);

if (Yii::$app->user->isGuest && $showLoginModal) {
    $this->registerJs("
        $('#login-modal').modal('show');
    ");
}