<?php

/* @var array $property*/
/* @var array $marker*/

use yii\helpers\Html;

?>
<div class="homes-row">
    <div class="homes-title">
        <div class="flex">
            <div class="homes-img">
                <?= Html::img($property['image'], ['alt' => $property['title'],'title' => $property['title']])?>
            </div>
            <div class="home-addr">
                <?= Html::a($property['title'], ['/property/property/show', 'slug' => $property['slug']], [
                    'data-role' => 'marker-data',
                    //'data-property' => $marker,
                ])?>
            </div>
        </div>
    </div>
    <div class="homes-opts">
        <?= $property['price']?>
    </div>
</div>
