<?php

use yii\helpers\Html;

/**
 * @var array $agent
 * @var integer $propertyCount
 * @var string $agentMarker
 */

?>
<div class="agent-item" data-role="marker-data"
     data-hover="show-marker"
     data-id="<?= $agent['id'] ?>"
     data-type="user">
    <div class="agent-list-info">
        <div class="agent-list-photo">
            <?= Html::img($agent['avatar'], ['alt' => $agent['username'], 'title' => $agent['username']]) ?>
        </div>
        <div class="agent-opt">
            <div class="agent-list-name">
                <?= Html::a($agent['username'], ['/agent/agent/view', 'id' => $agent['id']]) ?>
            </div>
            <div class="agent-list-phone">
                <?= $agent['phone'] ?>
            </div>
            <div class="agent-rate">
                <a href="#" class="agent-rate-alias">
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                </a>
            </div>
            <a href="#" class="agent-list-count">
                <?= !empty($agent['reviews']) ? '<p>' . Yii::t('agent', '{count} Reviews', ['count' => count($agent['reviews'])]) . '</p>' : '' ?>
                <?= !empty($agent['sales']) ? '<p>' . Yii::t('agent', '{count} Recent Sales', ['count' => count($agent['sales'])]) . '</p>' : '' ?>
                <?= $propertyCount ? '<p>' . Yii::t('agent', '{count, plural, one{# Listing} other{# Listings}}', ['count' => $propertyCount]) . '</p>' : '' ?>
            </a>
            <?php if (!empty($agent['lat']) && !empty($agent['lng'])) { ?>
                <div class="territory text-left">
                    <?php if (!empty($agent['addressData']['city'])) {
                        echo Yii::t('main', 'in city {city}', ['city' => $agent['addressData']['city']]);
                    } ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <?= Html::a('', ['/agent/agent/view', 'id' => $agent['id']], ['class' => 'go-page']) ?>
</div>