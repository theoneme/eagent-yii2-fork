<?php

use frontend\widgets\AgentFilter;
use yii\web\View;
use yii\widgets\LinkPager;

frontend\themes\mobile\assets\AgentAsset::register($this);

/**
 * @var array $agents
 * @var array $agentMarkers
 * @var array $seo
 * @var array $propertyMarkers
 * @var View $this
 * @var AgentFilter $filter
 * @var array $propertyCounts
 */

?>

<?= \frontend\widgets\mobile\BottomCatalogAgentMenu::widget() ?>

<div class="page-wrap">
    <div class="agent-profile-content">
        <div class="catalog-left">
            <div>
                <h1 class="text-left"><?= $seo['heading'] ?></h1>
            </div>
            <?= $filter->run() ?>
            <div class="agent-list">
                <?php foreach ($agents['items'] as $agent) {
                    echo $this->render('catalog-item', [
                        'agent' => $agent,
                        'agentMarker' => $agentMarkers[$agent['id']],
                        'propertyCount' => $propertyCounts[$agent['id']] ?? 0
                    ]);
                } ?>
            </div>
            <?php if ($agents['pagination']) {
                echo LinkPager::widget([
                    'pagination' => $agents['pagination'],
                    'maxButtonCount' => 5,
                ]);
            } ?>
            <div class="agent-list-descr">
                <?= $seo['content'] ?? '' ?>
            </div>
        </div>
    </div>
</div>