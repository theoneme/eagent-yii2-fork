<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use frontend\controllers\request\ManageController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

frontend\themes\mobile\assets\WizardAsset::register($this);

/**
 * @var \frontend\forms\ar\RequestForm $requestForm
 * @var array $route
 * @var integer $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'Post your request') ?></h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($route),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div>
                    {label}
                    {input}
                    {error}
            </div>",
            ],
            'options' => [
                'id' => 'property-form',
                'data-role' => 'edit-modal-form',
                'data-pj-container' => 'property-pjax',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <?= Html::activeHiddenInput($requestForm->category, 'category_id') ?>
            <?= Html::activeHiddenInput($requestForm, 'type') ?>
            <?= Html::activeHiddenInput($requestForm, 'locale') ?>
            <div class="wizard-row">
                <div class="wizards-steps">
                    <?php foreach ($requestForm->steps as $key => $step) { ?>
                        <section id="step<?= $key ?>">
                            <div class="wizard-step-header">
                                <?= $step['title'] ?>
                                <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($requestForm->steps)]) ?></span>
                            </div>
                            <div class="step-content">
                                <?php foreach ($step['config'] as $stepConfig) { ?>
                                    <?php if ($stepConfig['type'] === 'view') { ?>
                                        <?= $this->render("steps/{$stepConfig['value']}", [
                                            'form' => $form,
                                            'propertyForm' => $requestForm,
                                            'step' => $step,
                                            'createForm' => false
                                        ]) ?>
                                    <?php } ?>

                                    <?php if ($stepConfig['type'] === 'constructor') { ?>
                                        <?php foreach ($stepConfig['groups'] as $group) { ?>
                                            <?php $attributes = $requestForm->dynamicForm->getAttributesByGroup($group) ?>
                                            <?= $this->render("partial/attributes-group", [
                                                'form' => $form,
                                                'attributes' => array_flip($attributes),
                                                'dynamicForm' => $requestForm->dynamicForm
                                            ]) ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </section>
                    <?php } ?>
                    <div class="animate-input text-center">
                        <?= Html::submitInput(\Yii::t('wizard', 'Submit Request'), ['class' => 'btn btn-small btn-blue-white width100', 'id' => 'contact-agent']) ?>
                        <label for="contact-agent" class="animate-button">
                            <div class="btn-wrapper">
                                <div class="btn-original"><?= Yii::t('wizard', 'Submit Request') ?></div>
                                <div class="btn-container">
                                    <div class="left-circle"></div>
                                    <div class="right-circle"></div>
                                    <div class="mask"></div>
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php $renderAttributesRoute = Url::toRoute('/property/property/attributes');
$attachments = count($requestForm->attachment);
$canonical = Url::canonical();
$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
$script = <<<JS
var attachments = {$attachments},
    hasUploadError = false,
    isUpdate = {$isUpdate},
    calculatorFlag = false,
    validationPerformed = false,
    progressCalculator = new ProgressCalculator({
        formSelector: '#property-form'
    }),
    form = $('#property-form');

if(isUpdate) {
    form.yiiActiveForm('validate', true);
    progressCalculator.updateProgress();
} else {
    validationPerformed = true
}

form.on('submit', function() { 
    calculatorFlag = true;
    if ($("#file-upload-input").fileinput("getFilesCount") > 0) {
        $("#file-upload-input").fileinput("upload");
        return false;
    } else {
        if(validationPerformed === true) { 
             return true;
        } else { 
            validationPerformed = true;
            return false;
        }
    }
}).on("afterValidateAttribute", function(event, attribute, messages) {
    if(calculatorFlag === false) {
        progressCalculator.updateProgress();
    }
}).on("afterValidate", function(event, messages, errorAttributes) {
    progressCalculator.updateProgress();
    calculatorFlag = false;
});

$(".file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
    let response = data.response;
    $(".images-container").append("<input name=\'AttachmentForm[" + attachments + "][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
    attachments++;
}).on("filedeleted", function(event, key) {
    $(".images-container input[data-key=\'" + key + "\']").remove();
}).on("filebatchuploadcomplete", function() {
    if (hasUploadError === false) {
        $(this).closest("form").submit();
    } else {
        hasUploadError = false;
    }
}).on("fileuploaderror", function(event, data, msg) {
    hasUploadError = true;
    $('#' + data.id).find('.kv-file-remove').click();
});

JS;
$this->registerJs($script);

?>