<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

    <?= $form->field($propertyForm->meta, 'title')->textInput() ?>

    <?= $form->field($propertyForm->meta, 'description')->textarea([
        'placeholder' => Yii::t('wizard', 'Request Description'),
        'rows' => 8
    ]) ?>
