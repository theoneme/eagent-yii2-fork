<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */
?>

    <?= $form->field($propertyForm->price, 'price')->textInput(['type' => 'number']) ?>
    <?= $form->field($propertyForm->price, 'currency_code')->dropDownList(
        \common\helpers\DataHelper::getCurrencies()
    ) ?>
    <?= $form->field($propertyForm, 'contract_price')->radioList([
        1 => Yii::t('main', 'Yes'),
        0 => Yii::t('main', 'No')
    ], [
        'item' => function ($index, $label, $name, $checked, $value) {
            $chk = $checked ? 'checked' : '';
            $output = "<div class='chover radio-btn'>
            <input name='{$name}' id='form-contractprice-{$index}' class='radio-checkbox count-rooms' name='type' value='{$value}' {$chk} type='radio'>
            <label for='form-contractprice-{$index}'>{$label}</label>
        </div>";
            return $output;
        },
        'class' => 'flex'
    ])?>
