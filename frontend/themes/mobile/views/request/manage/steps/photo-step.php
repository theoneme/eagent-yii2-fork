<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>


<p>
    <?= Yii::t('wizard', 'You can upload up to {count} photos', ['count' => 20]) ?>
</p>
<div>
    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('wizard', 'Property Photos') ?>"
         data-content="<?= Yii::t('wizard', 'Upload Property Images') ?>">
        <?= FileInput::widget(
            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                'id' => 'file-upload-input',
                'name' => 'uploaded_images[]',
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                    'overwriteInitial' => false,
                    'initialPreview' => FileInputHelper::buildPreviews($propertyForm->attachment),
                    'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($propertyForm->attachment),
                ]
            ])
        ) ?>
    </div>
</div>
<div class="images-container">
    <?php foreach ($propertyForm->attachment as $key => $attachment) { ?>
        <?= $form->field($attachment, "[{$key}]content", ['template' => '{input}'])->hiddenInput([
            'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
            'data-key' => "image_init_{$key}"
        ])->label(false) ?>
    <?php } ?>
</div>
<p>&nbsp;</p>

