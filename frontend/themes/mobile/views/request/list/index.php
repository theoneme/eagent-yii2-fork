<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.11.2018
 * Time: 17:01
 */

use frontend\widgets\PropertyFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\plugins\AutoCompleteAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\themes\mobile\assets\RequestAsset::register($this);

/**
 * @var array $items
 * @var array $markers
 * @var array $tabs
 * @var View $this
 * @var PropertyFilter $itemFilter
 */

?>

<div class="catalog-wrap">
    <div class="catalog-left">
        <div class="mobile-request">
            <h1 class="text-left"><?= Yii::t('account', 'My Requests') ?></h1>
            <ul class="flex no-list flex-wrap object-status-switcher">
                <?php foreach ($tabs as $tab) { ?>
                    <li class="">
                        <a href="<?= Url::to(['/request/list/index', 'status' => $tab['status']]) ?>"
                           class="btn btn-default <?= $tab['active'] ? 'active' : '' ?>">
                            <small><?= $tab['title'] ?>&nbsp;(<?= $tab['count'] ?>)</small>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php if (count($items['items']) > 0) { ?>
                <?php foreach ($items['items'] as $item) { ?>
                    <div class="mobile-request-item flex space-between">
                        <div class="mobile-request-item-info">
                            <div class="mobile-request-row">
                                <div class="mobile-request-id"><?= $item['id'] ?></div>
                                <div class="mobile-request-title">
                                    <a data-role="marker-data"
                                       data-property="<?= $markers[$item['id']] ?>"
                                       href="#">
                                        <?= $item['title'] ?>
                                    </a>
                                </div>
                            </div>
                            <div class="flex space-between">
                                <div class="mobile-request-price"><?= $item['price'] ?></div>
                                <div>
                                    <a href="<?= Url::to(['/request/manage/update', 'id' => $item['id']]) ?>">
                                        <i class="icon-pencil"></i>
                                    </a>
                                    <?= Html::a('<i class="icon-delete-button"></i>', [
                                        '/request/manage/delete', 'id' => $item['id']
                                    ], ['data-confirm' => Yii::t('main', 'Do you really want to delete this item?')]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($items['pagination']) { ?>
                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $items['pagination'],
                    ]); ?>
                <?php } ?>
            <?php } else { ?>
                <p><?= Yii::t('main', 'No results found') ?></p>
            <?php } ?>
        </div>
    </div>
</div>

