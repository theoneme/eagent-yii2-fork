<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use frontend\assets\plugins\AutoCompleteAsset;
use frontend\themes\mobile\assets\FilterAsset;
use frontend\widgets\RequestFilter;
use yii\helpers\Url;
use yii\web\View;

\frontend\themes\mobile\assets\CatalogAsset::register($this);
frontend\themes\mobile\assets\RequestAsset::register($this);
AutoCompleteAsset::register($this);
FilterAsset::register($this);

/**
 * @var array $requests
 * @var array $requestMarkers
 * @var View $this
 * @var RequestFilter $requestFilter
 * @var string $lat
 * @var string $lng
 */

?>

<?= \frontend\widgets\mobile\BottomCatalogMenu::widget() ?>
<?= $requestFilter->run() ?>
<div class="catalog-wrap">
    <div class="catalog-left">
        <?php if (count($requests['items']) > 0) { ?>
            <div class="mobile-request">
                <?php foreach ($requests['items'] as $request) { ?>
                    <div class="mobile-request-item flex space-between" data-role="marker-data" data-property="<?= $requestMarkers[$request['id']] ?>">
                        <!--                            <div class="chover">-->
                        <!--                                <input id="house-->
                        <? //= $request['id'] ?><!--" class="radio-checkbox" name="id"-->
                        <!--                                       value="--><? //= $request['id'] ?><!--"-->
                        <!--                                       type="checkbox">-->
                        <!--                                <label for="house-->
                        <? //= $request['id'] ?><!--"></label>-->
                        <!--                            </div>-->
                        <div class="mobile-request-item-info">
                            <div class="mobile-request-row">
                                <!--                                    <div class="mobile-request-id" data-role="marker-data"-->
                                <!--                                         data-property="-->
                                <? //= $requestMarkers[$request['id']] ?><!--">-->
                                <!--                                        --><? //= $request['id'] ?>
                                <!--                                    </div>-->
                                <div class="mobile-request-title"><?= $request['title'] ?></div>
                            </div>

                            <div class="flex space-between">
                                <div><?= Yii::$app->formatter->asDatetime($request['created_at']) ?></div>
                                <div class="mobile-request-price"><?= $request['price'] ?></div>
                            </div>
                            <div class="flex space-between">
                                <a href="#" data-toggle="modal" data-target="#tariff-modal">
                                    <?= Yii::t('catalog', 'View Request') ?>
                                </a>
                                <div><?= Yii::t('catalog', 'Views') ?>: 0</div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <?php if ($requests['pagination']) { ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $requests['pagination'],
                ]); ?>
            <?php } ?>
        <?php } else { ?>
            <p><?= Yii::t('main', 'No results found') ?></p>
        <?php } ?>
    </div>
    <div class="catalog-right">
        <div class="map-over">
            <div id="map_catalog"></div>
        </div>
    </div>
</div>

<div class="modal fade new-modal modal480" id="tariff-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close not-age" data-dismiss="modal">×</button>
                <div class="modal-title text-center"><?= Yii::t('main', 'Attention!') ?></div>
            </div>
            <div class="modal-body">
                <div class="title-age text-left">
                    <p class="text-center">
                        <?= Yii::t('main', 'Request details are available only for agents, who bought our premium tariff') ?>
                    </p>
                </div>
                <div class="text-center">
                    <a class="btn btn-small btn-white-blue" href="<?= Url::to(['/page/tariff']) ?>">
                        <?= Yii::t('main', 'View Tariffs') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $markerPropertyIcon = Url::to(['/images/marker.png'], true);

$script = <<<JS
    let mapInitialized = false;
    let map = null;
    
    $(document).on('click', '#bottom-menu-map', function() { 
        if(mapInitialized === false) {
            map = new CatalogMap({
                lat: '{$lat}',
                lon: '{$lng}',
                markerViewRoutes: {
                    request: null,  
                },
                markerIcons: {
                    request: '{$markerPropertyIcon}'
                }
            });
            map.init();
            map.parseMarkers();
        }
    });
JS;
$this->registerJs($script);
