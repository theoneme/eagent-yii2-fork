<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2018
 * Time: 17:03
 */

use yii\helpers\Html;

/**
 * @var array $user
 * @var array $property
 */

?>

<div class="modal-header contact-header text-center">
    <button type="button" class="close-contact" data-dismiss="modal" aria-hidden="true">×</button>
    <div class="contact-head-avatar">
        <?= Html::a(Html::img($user['avatar']), ['/agent/agent/view', 'id' => $user['id']]) ?>
    </div>
    <?= Html::a($user['username'], ['/agent/agent/view', 'id' => $user['id']], [
        'class' => 'contact-head-name text-center'
    ]) ?>
    <div class="contact-header-date">
        <?= Yii::t('property', 'Member from {date}', [
            'date' => Yii::$app->formatter->asDatetime($user['created_at'])
        ]) ?>
    </div>
</div>
<div class="modal-body contact-body text-center">
    <?php if($user['phone']) { ?>
        <?= Html::a(Yii::t('property', 'Call up').'<div>'.$user['phone'].'</div>', "tel:{$user['phone']}", [
            'class' => 'btn btn-small btn-white-blue mix-phone-btn'
        ]) ?>
    <?php } ?>
    <?php if($user['email']) { ?>
        <?= Html::a($user['email'], "mailto:{$user['email']}", [
            'class' => 'contact-phone'
        ]) ?>
    <?php } ?>

    <p class="text-center">
        <?= Yii::t('property', 'Don`t forget to tell that you found this announcement on {site}', ['site' => Yii::$app->name]) ?>
    </p>
    <?= Html::a(Yii::t('property', 'Send Message'), ['/property/ajax/send-message', 'entity_id' => $property['id']], [
        'class' => 'btn btn-small btn-blue-white',
        'data-action' => 'send-message',
    ]) ?>
</div>



<?php
$script = <<<JS
    //$(".contact-header").closest('.modal-dialog').addClass('modal-sm');
JS;

$this->registerJs($script);

