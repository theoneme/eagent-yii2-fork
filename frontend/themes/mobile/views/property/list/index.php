<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use frontend\widgets\PropertyFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\plugins\AutoCompleteAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\assets\plugins\FancyBoxAsset::register($this);
\frontend\themes\mobile\assets\RequestAsset::register($this);

/**
 * @var array $properties
 * @var array $propertyMarkers
 * @var array $tabs
 * @var View $this
 * @var PropertyFilter $propertyFilter
 */

?>

<div class="catalog-wrap">
    <div class="catalog-left">
        <div class="mobile-request">
            <h1 class="text-left"><?= Yii::t('account', 'My Properties') ?></h1>
            <ul class="flex no-list flex-wrap object-status-switcher">
                <?php foreach ($tabs as $tab) { ?>
                    <li class="">
                        <a href="<?= Url::to(['/property/list/index', 'status' => $tab['status']]) ?>"
                           class="btn btn-default <?= $tab['active'] ? 'active' : '' ?>">
                            <small><?= $tab['title'] ?>&nbsp;(<?= $tab['count'] ?>)</small>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php if (count($properties['items']) > 0) { ?>
                <?php foreach ($properties['items'] as $property) { ?>
                    <div class="mobile-request-item flex space-between">
                        <div class="mobile-request-item-info">
                            <div class="mobile-request-row">
                                <div class="mobile-request-id">
                                    <?= $property['id'] ?>
                                </div>
                                <div class="mobile-request-title">
                                    <a data-role="marker-data"
                                       href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                                       data-property="<?= $propertyMarkers[$property['id']] ?>"
                                       data-action="load-modal-property">
                                        <?= $property['title'] ?>
                                    </a>
                                </div>
                            </div>
                            <div class="mobile-request-row">
                                <div class="flex space-between">
                                    <div><?= isset($property['attributes']['property_area']) ? "{$property['attributes']['property_area']} м²" : '-' ?></div>
                                    <div class="mobile-request-price"><?= $property['price'] ?></div>
                                </div>
                            </div>
                            <div class="flex space-between">
                                <div class="mobile-request-price">
                                    <?php if(count($property['images']) > 0) { ?>
                                        <?php foreach ($property['images'] as $key => $image) { ?>
                                            <?php if ($key === 0) { ?>
                                                <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($property['images']), $image, [
                                                    'data-fancybox' => "gallery-{$property['id']}",
                                                    'data-role' => 'fancy-gallery-item'
                                                ]) ?>
                                            <?php } else { ?>
                                                <?= Html::a(null, $image, [
                                                    'data-fancybox' => "gallery-{$property['id']}",
                                                    'class' => 'hidden',
                                                    'data-role' => 'fancy-gallery-item'
                                                ]) ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <i class="icon-camera"></i>&nbsp;<?= count($property['images']) ?>
                                    <?php } ?>
                                </div>
                                <div>
                                    <a href="<?= Url::to(['/property/manage/update', 'id' => $property['id']]) ?>">
                                        <i class="icon-pencil"></i></a>
                                    <?= Html::a('<i class="icon-delete-button"></i>', [
                                        '/property/manage/delete', 'id' => $property['id']
                                    ], ['data-confirm' => Yii::t('main', 'Do you really want to delete this item?')]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($properties['pagination']) { ?>
                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $properties['pagination'],
                    ]); ?>
                <?php } ?>
            <?php } else { ?>
                <p><?= Yii::t('main', 'No results found') ?></p>
            <?php } ?>

        </div>
    </div>
    </div>

<?php
$script = <<<JS
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false
    });
    
    // /* List houses */
    // $(document.body).on('click','#list-houses th',function() {
    //     if($(this).hasClass('active')){  
    //         $(this).toggleClass('up');
    //         $(this).toggleClass('down');
    //     } else {
    //         $('#list-houses th').removeClass('active');
    //         $(this).addClass('active');
    //     }
    // });
    //
    // $(document).on('change', '#list-houses input[type="checkbox"]', function() {
    //     $(this).closest('tr').toggleClass('check');
    // });
    
JS;

$this->registerJs($script);

