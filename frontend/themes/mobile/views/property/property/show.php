<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 20:22
 */

use common\assets\GoogleAsset;
use common\components\CurrencyHelper;
use common\helpers\GoogleStaticMapHelper;
use common\models\Property;
use frontend\assets\plugins\MortgageCalculatorAsset;
use frontend\forms\contact\ContactAgentForm;
use frontend\themes\mobile\assets\PropertyModalAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

GoogleAsset::register($this);
PropertyModalAsset::register($this);
MortgageCalculatorAsset::register($this);

/**
 * @var array $property
 * @var array $building
 * @var array $similarProperties
 * @var string $similarUrl
 * @var array $cheaperProperties
 * @var array $moreExpensiveProperties
 * @var array $biggerProperties
 * @var array $seo
 * @var array $realtors
 * @var View $this
 * @var ContactAgentForm $contactAgentForm
 */

?>
    <div class="catalog-right">
        <div class="map-over">
            <div id="big-map" data-lat="<?= $property['lat'] ?>"
                 data-lng="<?= $property['lng'] ?>" data-type="property"></div>
            <a class="close-map text-right">
                <i class="i-right-arrow"></i>
            </a>
        </div>
    </div>
    <div class="modal-body">
        <?php if (!empty($property['images'])) { ?>
            <div class="modal-slider" data-role="main-slider">
                <?php foreach ($property['images'] as $key => $image) { ?>
                    <div class="modal-slide">
                        <?= Html::a(Html::img($property['thumbnails'][$key], ['alt' => $seo['title'], 'title' => $seo['heading']]), $image, ['data-fancybox' => 'gallery']) ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="modal-info">
            <h1 class="text-left" id="fancy-title">
                <?= $property['title'] ?>
            </h1>
            <h3>
                <?php if (array_key_exists('bedrooms', $property['attributes'])) { ?>
                    <span class="like-ul">
                                    <?= Yii::t('property', '{bedrooms, plural, one{# bedroom} other{# bedrooms}}', [
                                        'bedrooms' => $property['attributes']['bedrooms']['value']
                                    ]) ?>
                                </span>
                <?php } ?>
                <?php if (array_key_exists('bathrooms', $property['attributes'])) { ?>
                    <span class="like-ul">
                                    <?= Yii::t('property', '{bathrooms, plural, one{# bathroom} other{# bathrooms}}', [
                                        'bathrooms' => $property['attributes']['bathrooms']['value']
                                    ]) ?>
                                </span>
                <?php } ?>
                <?php if (array_key_exists('property_area', $property['attributes'])) { ?>
                    <span class="like-ul">
                                    <?= Yii::t('main', '{area} m²', ['area' => $property['attributes']['property_area']['value']]) ?>
                                </span>
                <?php } ?>
            </h3>
            <?php if (!empty($building)) {
                echo Html::a(
                    Yii::t('property', 'Located in the building{title} at the address: {address}', [
                        'address' => $building['address'],
                        'title' => !empty($building['translations']['name']) ? Yii::t('seo', ' «{title}»', ['title' => $building['translations']['name']]) : '',
                    ]) . '.&nbsp;<span>' . Yii::t('property', 'View other properties in this building') . '</span>',
                    ['/building/building/show', 'slug' => $building['slug']],
                    ['class' => 'property-building-link', 'data-action' => 'load-modal-property']
                );
            } ?>

            <div class="flex space-between flex-v-center">
                <div class="modal-short">
                    <div class="mcs-price">
                        <?= $property['price'] ?>
                    </div>
                    <div class="market-date">
                        <div>
                            <?= Yii::t('property', 'Publish date: <strong>{date}</strong>', [
                                'date' => Yii::$app->formatter->asDatetime($property['created_at'])
                            ]) ?>
                        </div>
                    </div>
                </div>
                <?php if (!empty($property['lat']) && !empty($property['lng'])) {
                    $image = GoogleStaticMapHelper::getMapImage($property['lat'], $property['lng'], 120, 120, null, false);
                    echo Html::a('', '#', [
                        'class' => 'map-modal-icon',
                        'style' => "background-image:url('{$image}');",
                        'data-action' => 'big-gmap'
                    ]);
                } ?>
            </div>
            <div class="modal-home-descr">
                <div class="hide-content hide-text">
                    <p>
                        <?= $property['description'] ?>
                    </p>
                </div>
                <a class="more-less" data-height="200">
                    <?= Yii::t('main', 'More') ?>
                    <i class="icon-down"></i>
                </a>
            </div>
            <?php if (isset($property['translations']['advantage'])) { ?>
                <div class="advantage">
                    <h3 class="up-title text-left"><?= Yii::t('property', 'What i personally like in this property') ?></h3>
                    <div><?= $property['translations']['advantage'] ?></div>
                </div>
            <?php } ?>

            <div class="facts flex space-between">
                <div class="facts-title">
                    <?= Yii::t('property', 'Main Parameters') ?>
                </div>

                <?php if (array_key_exists('property_type', $property['attributes'])) { ?>
                    <div class="fact-item">
                        <div class="fi-icon">
                            <i class="icon-skyline"></i>
                        </div>
                        <div class="fact-info">
                            <div class="fact-title">
                                <?= Yii::t('property', 'Form of Ownership') ?>
                            </div>
                            <div class="fact-text">
                                <?= $property['attributes']['property_type']['value'] ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if (array_key_exists('year_built', $property['attributes'])) { ?>
                    <div class="fact-item">
                        <div class="fi-icon">
                            <i class="icon-calendar"></i>
                        </div>
                        <div class="fact-info">
                            <div class="fact-title">
                                <?= Yii::t('property', 'Year Built') ?>
                            </div>
                            <div class="fact-text">
                                <?= $property['attributes']['year_built']['value'] ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if (array_key_exists('heating', $property['attributes'])) { ?>
                    <div class="fact-item">
                        <div class="fi-icon">
                            <i class="icon-thermometer"></i>
                        </div>
                        <div class="fact-info">
                            <div class="fact-title">
                                <?= Yii::t('property', 'Heating') ?>
                            </div>
                            <div class="fact-text">
                                <?= $property['attributes']['heating']['value'] ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if (array_key_exists('cooling', $property['attributes'])) { ?>
                    <div class="fact-item">
                        <div class="fi-icon">
                            <i class="icon-snowflake"></i>
                        </div>
                        <div class="fact-info">
                            <div class="fact-title">
                                <?= Yii::t('property', 'Cooling') ?>
                            </div>
                            <div class="fact-text">
                                <?= $property['attributes']['cooling']['value'] ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if (array_key_exists('parking', $property['attributes'])) { ?>
                    <div class="fact-item">
                        <div class="fi-icon">
                            <i class="icon-parked-car"></i>
                        </div>
                        <div class="fact-info">
                            <div class="fact-title">
                                <?= Yii::t('property', 'Parking') ?>
                            </div>
                            <div class="fact-text">
                                <?= Yii::t('property', '{parking, plural, one{# space} other{# spaces}}', [
                                    'parking' => $property['attributes']['parking']['value']
                                ]) ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if (array_key_exists('property_area', $property['attributes'])) { ?>
                    <div class="fact-item">
                        <div class="fi-icon">
                            <i class="icon-dimensions"></i>
                        </div>
                        <div class="fact-info">
                            <div class="fact-title">
                                <?= Yii::t('property', 'Property Area') ?>
                            </div>
                            <div class="fact-text">
                                <?= $property['attributes']['property_area']['value'] ?>
                            </div>
                        </div>
                    </div>

                    <div class="fact-item">
                        <div class="fi-icon">
                            <i class="icon-blueprint"></i>
                        </div>
                        <div class="fact-info">
                            <div class="fact-title">
                                <?= Yii::t('property', 'Price / m²') ?>
                            </div>
                            <div class="fact-text">
                                <?= CurrencyHelper::convertAndFormat($property['currency_code'], Yii::$app->params['app_currency'], $property['source_price'] / (float)$property['attributes']['property_area']['value']) ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="fact-item">
                    <div class="fi-icon">
                        <i class="icon-hours"></i>
                    </div>
                    <div class="fact-info">
                        <div class="fact-title">
                            <?= Yii::t('property', 'Days on {site}', ['site' => Yii::$app->name]) ?>
                        </div>
                        <div class="fact-text">
                            <?= Yii::$app->formatter->asRelativeTime($property['created_at']) ?>
                        </div>
                    </div>
                </div>

                <div class="fact-item">
                    <div class="fi-icon">
                        <i class="icon-prices-of-houses"></i>
                    </div>
                    <div class="fact-info">
                        <div class="fact-title">
                            <?= Yii::t('property', 'Price') ?>
                        </div>
                        <div class="fact-text">
                            <?= $property['price'] ?>
                        </div>
                    </div>
                </div>

                <div class="fact-item">
                    <div class="fi-icon">
                        <i class="icon-home"></i>
                    </div>
                    <div class="fact-info">
                        <div class="fact-title">
                            <?= Yii::t('property', 'Saves') ?>
                        </div>
                        <div class="fact-text">
                            0
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-info-block hide-content hide-text">
                <div>
                    <h4 class="up-title text-left">
                        <?= Yii::t('property', 'Property Parameters') ?>
                    </h4>
                    <?php if (!empty($property['attributes'])) { ?>
                        <div class="modal-info-block-content">
                            <?php foreach ($property['attributes'] as $attribute) { ?>
                                <div class="info-block-item">
                                    <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <?= Yii::t('property', 'No attributes specified') ?>
                    <?php } ?>
                </div>
                <?php if (!empty($building) && !empty($building['attributes'])) { ?>
                    <p>&nbsp;</p>
                    <div>
                        <h4 class="up-title text-left">
                            <?= Yii::t('building', 'Building Parameters') ?>
                        </h4>
                        <div class="modal-info-block-content">
                            <?php foreach ($building['attributes'] as $attribute) { ?>
                                <div class="info-block-item">
                                    <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a class="col-md-6 more-less" data-height="300">
                <?= Yii::t('main', 'More') ?>
                <i class="icon-down"></i>
            </a>
            <div class="slide-down-list">
                <section class="params-block">
                    <div class="params-header" data-toggle="collapse" data-target="#tax-history">
                        <h2 class="text-left"><?= Yii::t('property', 'Price History') ?></h2>
                    </div>
                    <div class="collapse" id="tax-history">
                        <div class="params-content">
                            <ul class="no-list history-switch">
                                <li class="active">
                                    <a href="#priceH"
                                       data-toggle="tab"> <?= Yii::t('property', 'Price History') ?> </a>
                                </li>
                            </ul>
                            <div class="tab-content tab-history">
                                <div class="history-content tab-pane fade in active" id="priceH">
                                    <table class="table-history text-left">
                                        <thead>
                                        <tr>
                                            <th><?= Yii::t('property', 'Date') ?></th>
                                            <th><?= Yii::t('property', 'Event') ?></th>
                                            <th><?= Yii::t('property', 'Price') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= Yii::$app->formatter->asDatetime($property['created_at']) ?></td>
                                            <td><?= Yii::t('property', 'Property listed') ?></td>
                                            <td><?= $property['price'] ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php if ($property['type'] === Property::TYPE_SALE) { ?>
                    <section class="params-block">
                        <div class="params-header" data-toggle="collapse" data-target="#mortgages">
                            <h2 class="text-left"><?= Yii::t('property', 'Mortgage') ?></h2>
                        </div>
                        <div class="collapse" id="mortgages">
                            <div class="params-content">
                                <div class="row">
                                    <div class="col-md-5">
                                        <form class="calculator-form">
                                            <div class="form-group">
                                                <label for="mortgage-price"><?= Yii::t('property', 'Property Price') ?></label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><?= Yii::$app->params['app_currency'] ?></span>
                                                    <?= Html::input('number', 'mortgage-property-price', $property['raw_price'], [
                                                        'data-role' => 'mortgage-price',
                                                        'id' => 'mortgage-price',
                                                        'class' => 'form-control'
                                                    ]) ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mortgage-down-payment"><?= Yii::t('property', 'Down Payment') ?></label>
                                                <div class="clearfix">
                                                    <div class="col-xs-8 no-padding">
                                                        <div class="input-group">
                                                            <?= Html::input('number', 'mortgage-down-payment', (int)($property['raw_price'] / 2), [
                                                                'data-role' => 'mortgage-down-payment',
                                                                'id' => 'mortgage-down-payment',
                                                                'class' => 'form-control'
                                                            ]) ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 no-padding">
                                                        <div class="input-group">
                                                            <?= Html::input('number', 'mortgage-down-payment-percent', 50, [
                                                                'data-role' => 'mortgage-down-payment-percent',
                                                                'id' => 'mortgage-down-payment-percent',
                                                                'class' => 'form-control'
                                                            ]) ?>
                                                            <span class="input-group-addon">%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="mortgage-loan-term"><?= Yii::t('property', 'Loan Term') ?></label>
                                                <?= Html::dropDownList('mortgage-loan-term', null, [
                                                    5 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 5]),
                                                    10 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 10]),
                                                    15 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 15]),
                                                    20 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 20]),
                                                    30 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 30]),
                                                ], ['data-role' => 'mortgage-loan-term', 'id' => 'mortgage-loan-term']) ?>
                                            </div>
                                            <div class="form-group">
                                                <label for="mortgage-interest-rate"
                                                       class="flex space-between">
                                                    <?= Yii::t('property', 'Interest Rate') ?>
                                                </label>
                                                <div class="input-group">
                                                    <?= Html::input('number', 'mortgage-interest-rate', 6, [
                                                        'id' => 'mortgage-interest-rate',
                                                        'data-role' => 'mortgage-interest-rate',
                                                        'class' => 'form-control'
                                                    ]) ?>
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="chover">
                                                    <input id="mortgage-property-insurance"
                                                           data-role="mortgage-property-insurance"
                                                           class="radio-checkbox"
                                                           name="mortgage-property-insurance" value="1"
                                                           type="checkbox">
                                                    <label for="mortgage-property-insurance">
                                                        <?= Yii::t('property', 'Include Insurance') ?>
                                                    </label>

                                                    <div class="tooltip-ea" data-position="right"
                                                         data-width="360">
                                                        <div class="tooltip-modal">
                                                            <a href="#"
                                                               class="tooltip-close">&times;</a>
                                                            <div class="tooltip-body">
                                                                <p>
                                                                    <strong><?= Yii::t('property', 'Mortgage Insurance') ?></strong>
                                                                </p>
                                                                <p>
                                                                    <?= Yii::t('property', '-- Mortgage Insurance description --') ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="pay-sum text-center">
                                            <?= Yii::t('property', 'Monthly Payment') ?>:
                                            <span data-role="mortgage-payment"></span>
                                        </div>
                                        <div class="text-center">
                                            <?= Html::a(Yii::t('property', 'Send Request'), [
                                                '/property/ajax/request-mortgage', 'entity_id' => $property['id']
                                            ], [
                                                'class' => 'btn btn-small btn-blue-white',
                                                'data-action' => 'request-mortgage',
                                                'data-target' => '#dynamic-modal',
                                                'data-toggle' => 'modal'
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php } ?>
                <section class="params-block">
                    <div class="params-header" data-toggle="collapse" data-target="#views">
                        <h2 class="text-left"><?= Yii::t('property', 'Stats for this property') ?></h2>
                    </div>
                    <div class="collapse" id="views">
                        <div class="params-content">
                            <div class="stat-list row">
                                <div class="stat-block col-md-6 col-sm-6">
                                    <div class="stat-count text-center">
                                        0
                                    </div>
                                    <div class="stat-descr">
                                        <div>
                                            <strong><?= Yii::t('property', '{views, plural, one{view} other{views}}', ['views' => 0]) ?></strong>
                                        </div>
                                        <div>
                                            <?= Yii::t('property', 'in the last {days, plural, one{# day} other{# days}}', ['days' => 30]) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="stat-block col-md-6 col-sm-6">
                                    <div class="stat-count text-center">
                                        0
                                    </div>
                                    <div class="stat-descr">
                                        <div>
                                            <strong><?= Yii::t('property', '{saves, plural, one{user} other{users}} saved', ['saves' => 0]) ?></strong>
                                        </div>
                                        <div>
                                            <?= Yii::t('property', 'this property to their favorites in the last {days, plural, one{# day} other{# days}}', ['days' => 30]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php if ($property['lat'] && $property['lng']) { ?>
                    <section class="params-block">
                        <div class="params-header" data-toggle="collapse" data-target="#school" data-action="init-gmap">
                            <h2 class="text-left"><?= Yii::t('property', 'Property on map') ?></h2>
                        </div>
                        <div class="collapse" id="school">
                            <div id="modal-map" class="modal-property-map"
                                 data-lat="<?= $property['lat'] ?>"
                                 data-lng="<?= $property['lng'] ?>" data-type="property">
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </div>
            <p>&nbsp;</p>

            <? /*= $this->render('@frontend/views/agent/contact-agent-block', [
                'options' => [
                    'data-addon' => Url::to(['/property/ajax/quiz', 'entity_id' => $property['id']])
                ],
                'contactAgentForm' => $contactAgentForm,
                'realtors' => $realtors,
                'url' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                'title' => $property['title'],
            ]) */ ?>
            <div class="contact-agent-block">
                <h2 class="up-title"><?= Yii::t('property', 'Property listed by') ?></h2>
                <div class="agent-block">
                    <a href="<?= Url::to(['/agent/agent/view', 'id' => $property['user_id']]) ?>"
                       class="agent-photo">
                        <?= Html::img($property['user']['avatar'], ['alt' => $property['user']['username']]) ?>
                    </a>
                    <div class="agent-middle">
                        <div class="agent-name">
                            <?= Html::a($property['user']['username'], ['/agent/agent/view', 'id' => $property['user_id']])?>
                        </div>
                        <div class="agent-rate">
                            <a href="#" class="agent-rate-alias">
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                                <i class="icon-favorite"></i>
                            </a>
                            ( <a href="#"> 0 </a> )
                        </div>
                        <div class="agent-add-info">
                            <div>
                                <?= $property['user']['type'];?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="user-descr">
                    <p class="text-center">
                        <?= Html::a(Yii::t('property', 'Show Contacts'), [
                            '/property/ajax/show-contacts',
                            'entity_id' => $property['id'],
                            'owner_id' => $property['user_id']
                        ], [
                            'data-action' => 'show-contacts',
                            'data-target' => '#dynamic-modal',
                            'data-toggle' => 'modal'
                        ]) ?>
                    </p>
                </div>
            </div>
            <?= $this->render('@frontend/views/agent/contact-agent-block', [
                'blockTitle' => Yii::t('main', 'Do you have a questions? Contact us'),
                'options' => [
                    'data-addon' => $property['type'] === Property::TYPE_SALE
                        ? Url::to(['/property/ajax/quiz', 'entity_id' => $property['id']])
                        : null
                ],
                'contactAgentForm' => $contactAgentForm,
                'realtors' => $realtors,
                'url' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                'title' => $property['title'],
                'limit' => 0,
                'buttonConfig' => [
                    'entityId' => $property['id']
                ]
            ]) ?>
        </div>
        <div class="modal-home-footer">
            <div class="nearby-cols row">
                <div class="col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', '{rooms, plural, =0{} one{# room} other{# rooms}} Properties with similar area but cheaper price', [
                            'rooms' => $property['attributes']['rooms']['value'] ?? 0
                        ]) ?>
                    </div>
                    <?php if (count($cheaperProperties) > 0) { ?>
                        <ul class="no-list ul-nearby">
                            <?php foreach ($cheaperProperties as $cheaperProperty) { ?>
                                <li>
                                    <a href="<?= Url::to(['/property/property/show', 'slug' => $cheaperProperty['slug']]) ?>"
                                       data-action="load-modal-property">
                                        <?= $cheaperProperty['title'] ?>
                                        <b><?= $cheaperProperty['price'] ?></b>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <?= Yii::t('main', 'No results found') ?>
                    <?php } ?>
                </div>
                <div class="col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', '{rooms, plural, =0{} one{# room} other{# rooms}} Properties with similar area but higher price', [
                            'rooms' => $property['attributes']['rooms']['value'] ?? 0
                        ]) ?>
                    </div>
                    <?php if (count($moreExpensiveProperties) > 0) { ?>
                        <ul class="no-list ul-nearby">
                            <?php foreach ($moreExpensiveProperties as $moreExpensiveProperty) { ?>
                                <li>
                                    <a href="<?= Url::to(['/property/property/show', 'slug' => $moreExpensiveProperty['slug']]) ?>"
                                       data-action="load-modal-property">
                                        <?= $moreExpensiveProperty['title'] ?>
                                        <b><?= $moreExpensiveProperty['price'] ?></b>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <?= Yii::t('main', 'No results found') ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

<?php $more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');
$language = Yii::$app->language;
$currency = Yii::$app->params['app_currency'];
$price = $property['price'];

$script = <<<JS
    let moreLess = $('.more-less');

    moreLess.MoreLess({
        moreText: '{$more}&nbsp;',
        lessText: '{$less}&nbsp;'
    });
    
    /* Open calculator */
    $(document.body).on('click', '.calculator-btn', function() {
        $('.calculator-modal').toggleClass('open');
        
        return false;
    });
    
    /* Open sub menu more */
    $(document.body).on('click', '#blueMenuMore', function() {
        $('.blue-sub-menu').slideToggle(200);
    });
    
    $('.scroll-alias').on('click', function() {
        let anchor = $(this).attr('href');
        $('#property-modal').stop().animate({
            scrollTop: $(anchor).offset().top
        }, 500);
    });
JS;

$this->registerJs($script);

if ($property['type'] === Property::TYPE_SALE) {
    $script = <<<JS
        let mortgageCalculator = new MortgageCalculator({
            locale: '{$language}',
            currency: '{$currency}'
        });
        mortgageCalculator.calculate();
        mortgageCalculator.registerHandlers();
JS;

    $this->registerJs($script);
}

if ($property['lat'] && $property['lng']) {
    $markerPropertyIcon = Url::to(['/images/marker.png'], true);
    $script = <<<JS
        let mapInitialized = false;
        let mapInitializedBig = false;
        
        $('[data-action=init-gmap]').on('click', function() {
            if(mapInitialized === false) {
                let modalMap = new ModalMap({
                    markerIcons: {
                        property: '{$markerPropertyIcon}'
                    }
                });
                modalMap.init();
                modalMap.setMarker();
                
                mapInitialized = true;
            } 
        });
        
         $('[data-action=big-gmap]').on('click', function() {
            $('.catalog-right').show();
             
            if(mapInitializedBig === false) {
                let bigMap = new ModalMap({
                    markerIcons: {
                        property: '{$markerPropertyIcon}'
                    },
                    mapCanvasId: 'big-map'
                });
                bigMap.init();
                bigMap.setMarker();
                
                mapInitializedBig = true;
            } 
        });
        
        $(document.body).on('click', '.close-map', function() {
            $('.catalog-right').hide();
        });
JS;
    $this->registerJs($script);
}