<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.11.2018
 * Time: 16:36
 */

use frontend\forms\contact\ContactAgentForm;
use yii\web\View;

/**
 * @var array $property
 * @var array $building
 * @var array $similarProperties
 * @var string $similarUrl
 * @var array $cheaperProperties
 * @var array $moreExpensiveProperties
 * @var array $biggerProperties
 * @var array $seo
 * @var array $realtors
 * @var View $this
 * @var ContactAgentForm $contactAgentForm
 * @var string $catalogCategory
 */

frontend\themes\mobile\assets\PropertyDirectModalAsset::register($this);

?>

<?= \frontend\widgets\mobile\BottomPropertyMenu::widget([
    'object' => $property
]) ?>

    <div id="direct-container">
        <?= $this->render('show', [
            'property' => $property,
            'seo' => $seo,
            'realtors' => $realtors,
            'contactAgentForm' => $contactAgentForm,
            'building' => $building,
            'cheaperProperties' => $cheaperProperties,
            'moreExpensiveProperties' => $moreExpensiveProperties,
            'biggerProperties' => $biggerProperties
        ]) ?>
    </div>

<?php $script = <<<JS
    $('#direct-container').PropertyDirect();
JS;
$this->registerJs($script);