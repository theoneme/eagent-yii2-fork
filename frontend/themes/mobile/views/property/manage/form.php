<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use common\forms\ar\PropertyForm;
use common\models\Property;
use frontend\assets\CatalogAsset;
use frontend\assets\WizardAsset;
use frontend\controllers\property\ManageController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

frontend\themes\mobile\assets\WizardAsset::register($this);

/**
 * @var PropertyForm $propertyForm
 * @var array $route
 * @var integer $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'List your property') ?></h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($route),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div>
                    {label}
                    {input}
                    {error}
            </div>",
            ],
            'options' => [
                'id' => 'property-form',
                'data-role' => 'edit-modal-form',
                'data-pj-container' => 'property-pjax',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <?= Html::activeHiddenInput($propertyForm->category, 'category_id') ?>
            <?= Html::activeHiddenInput($propertyForm, 'type') ?>
            <?= Html::activeHiddenInput($propertyForm, 'locale') ?>
            <div class="wizard-row">
                <div class="wizards-steps">
                    <?php foreach ($propertyForm->steps as $key => $step) { ?>
                        <section id="step<?= $key ?>">
                            <div class="wizard-step-header">
                                <?= $step['title'] ?>
                                <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($propertyForm->steps)]) ?></span>
                            </div>
                            <div class="step-content">
                                <?php foreach ($step['config'] as $stepConfig) { ?>
                                    <?php if ($stepConfig['type'] === 'view') { ?>
                                        <?= $this->render("steps/{$stepConfig['value']}", [
                                            'form' => $form,
                                            'propertyForm' => $propertyForm,
                                            'step' => $step,
                                            'createForm' => false
                                        ]) ?>
                                    <?php } ?>

                                    <?php if ($stepConfig['type'] === 'constructor') { ?>
                                        <?php foreach ($stepConfig['groups'] as $group) { ?>
                                            <?php $attributes = $propertyForm->dynamicForm->getAttributesByGroup($group) ?>
                                            <?= $this->render("partial/attributes-group", [
                                                'form' => $form,
                                                'attributes' => array_flip($attributes),
                                                'dynamicForm' => $propertyForm->dynamicForm
                                            ]) ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </section>
                    <?php } ?>
                    <div class="text-center">
                        <input id="contact-agent" type="submit" class="btn btn-big btn-blue-white"
                               value="<?= Yii::t('property', 'Post Announcement') ?>">
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php $renderAttributesRoute = Url::toRoute('/property/property/attributes');
$canonical = Url::canonical();
$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
$script = <<<JS
var isUpdate = {$isUpdate},
    calculatorFlag = false,
    validationPerformed = false,
    progressCalculator = new ProgressCalculator({
        formSelector: '#property-form'
    }),
    form = $('#property-form');
if(isUpdate) {
    form.yiiActiveForm('validate', true);
    progressCalculator.updateProgress();
} else {
    validationPerformed = true
}

form.on('submit', function() { 
    calculatorFlag = true;
    let hasToUpload = false;
    $.each($(".file-upload-input"), function() {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            hasToUpload = true;
            return false;
        }
    });
    
    if(hasToUpload === false) {
        if(validationPerformed === true) { 
             return true;
        } else { 
            validationPerformed = true;
            return false;
        }
    } else {
        return false;
    }
}).on("afterValidateAttribute", function(event, attribute, messages) {
    if(calculatorFlag === false) {
        progressCalculator.updateProgress();
    }
}).on("afterValidate", function(event, messages, errorAttributes) {
    progressCalculator.updateProgress();
    calculatorFlag = false;
});

$("#categoryform-category_id, #propertyform-type").on("change", function(e) {
    let type = $('#propertyform-type').find('input:checked').val();
    let categoryId = $('#categoryform-category_id').find('input:checked').val();
    let params =  {
        category_id: categoryId,
        type: type,
    };
    let query = $.param(params);

    window.location.replace('{$canonical}' + '?' + query); 
});

JS;
$this->registerJs($script);

?>