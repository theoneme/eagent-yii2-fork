<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

use common\models\Translation;

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<?php foreach ($propertyForm->meta as $locale => $meta) { ?>
    <?php if ($locale === $propertyForm->locale) { ?>
        <?= $form->field($meta, "[{$locale}]" . Translation::KEY_ADDITIONAL_TITLE)->textInput([
            'placeholder' => Yii::t('wizard', 'Residential complex name')
        ])->label(Yii::t('wizard', 'Residential complex name')) ?>

        <?= $form->field($meta, "[{$locale}]" . Translation::KEY_ADDITIONAL_DESCRIPTION)->textarea([
            'placeholder' => Yii::t('wizard', 'Description of residential complex (Community)'),
            'rows' => 5
        ])->label(Yii::t('wizard', 'Description of residential complex (Community)')) ?>
    <?php } ?>
<?php } ?>
