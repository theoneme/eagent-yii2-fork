<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<?= \common\widgets\GmapsActiveInputWidget::widget([
    'model' => $propertyForm->geo,
    'form' => $form,
    'inputOptions' => ['class' => 'form-control'],
    'withMap' => true
]) ?>

<div class="form-group">
    <div id="google-map" style="height: 300px;">

    </div>
</div>

<?= $form->field($propertyForm, 'zip')->textInput(['type' => 'number']) ?>