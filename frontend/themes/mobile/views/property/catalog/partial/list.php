<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var array $sortData
 * @var array $seo
 * @var array $properties
 * @var integer $gridSize
 */

?>
    <div class="loading-overlay hidden"></div>

    <div>
        <h1 class="text-left"><?= $seo['heading'] ?></h1>
    </div>
<?php if (count($properties['items']) > 0) { ?>
    <div class="home-list">
        <?php foreach ($properties['items'] as $property) { ?>
            <div class="mobile-request-item flex space-between">
                <div class="mobile-request-item-info">
                    <div class="mobile-request-row">
                        <div class="mobile-request-id">
                            <?= $property['id'] ?>
                        </div>
                        <div class="mobile-request-title">
                            <a href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                               data-action="load-modal-property">
                                <?= $property['title'] ?>
                            </a>
                        </div>
                    </div>
                    <div class="mobile-request-row">
                        <div class="flex space-between">
                            <div><?= isset($property['attributes']['property_area']) ? "{$property['attributes']['property_area']} м²" : '-' ?></div>
                            <div class="mobile-request-price"><?= $property['price'] ?></div>
                        </div>
                    </div>
                    <div class="flex space-between">
                        <div class="mobile-request-price">
                            <?php if(count($property['images']) > 0) { ?>
                                <?php foreach ($property['images'] as $key => $image) { ?>
                                    <?php if ($key === 0) { ?>
                                        <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($property['images']), $image, [
                                            'data-fancybox' => "gallery-{$property['id']}",
                                            'data-role' => 'fancy-gallery-item'
                                        ]) ?>
                                    <?php } else { ?>
                                        <?= Html::a(null, $image, [
                                            'data-fancybox' => "gallery-{$property['id']}",
                                            'class' => 'hidden',
                                            'data-role' => 'fancy-gallery-item'
                                        ]) ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } else { ?>
                                <i class="icon-camera"></i>&nbsp;<?= count($property['images']) ?>
                            <?php } ?>
                        </div>
                        <div>
                            <a href="<?= Url::to(['/property/manage/update', 'id' => $property['id']]) ?>">
                                <i class="icon-pencil"></i>
                            </a>
                            <?= Html::a('<i class="icon-delete-button"></i>', [
                                '/property/manage/delete', 'id' => $property['id']
                            ], ['data-confirm' => Yii::t('main', 'Do you really want to delete this item?')]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php
    if ($properties['pagination']) {
        $properties['pagination']->route = '/property/catalog/index-ajax-short';

        echo LinkPager::widget([
            'pagination' => $properties['pagination'],
            'maxButtonCount' => 7,
            'linkOptions' => ['data-action' => 'switch-page'],
        ]);
    }
} else { ?>
    <p>
        <?= !empty($seo['emptyText']) ? $seo['emptyText'] : Yii::t('main', 'No results found'); ?>
    </p>
<?php } ?>