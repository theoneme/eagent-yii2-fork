<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use common\helpers\DataHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var array $sortData
 * @var array $seo
 * @var array $properties
 * @var integer $gridSize
 */

?>

<div class="loading-overlay hidden"></div>

<div class="form-group catalog-sort-wrap">
    <label for="catalog-sort"><?= $sortData['title'] ?></label>
    <select name="sort" id="catalog-sort">
        <option value="">
            --&nbsp;<?= Yii::t('catalog', 'Select {attribute}', ['attribute' => Yii::t('catalog', 'Sort Type')]) ?>
        </option>
        <?php foreach ($sortData['values'] as $valueKey => $value) { ?>
            <option value="<?= $valueKey ?>" <?= $valueKey === $sortData['checked'] ? 'selected' : '' ?>><?= $value ?></option>
        <?php } ?>
    </select>
</div>
<h1 class="text-left"><?= $seo['heading'] ?></h1>

<?php if (count($properties['items']) > 0) { ?>
    <div class="home-list">
        <?php foreach ($properties['items'] as $property) { ?>
            <div class="col-xs-6 home-item">
                <a class="home-item-inner"
                   href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                   data-action="load-modal-property">
                    <div class="home-img">
                        <?= Html::img($property['image'], ['alt' => $property['title'], 'title' => $property['title']]) ?>
                    </div>
                    <div class="home-wish"></div>
                    <div class="top-price text-center">
                        <div class="top-main-price"><?= $property['price'] ?></div>
                    </div>
                    <div class="home-info">
                        <h2 class="home-title"><?= $property['title'] ?></h2>
                        <div class="home-params">
                            <?php
                            $shown = 0;
                            $dotAttributes = DataHelper::getCatalogItemAttributes();
                            foreach ($dotAttributes as $key => $data) {
                                if (!empty($property['attributes'][$key])) {
                                    $shown++;
                                    echo Yii::t('catalog', $data['template'], [$data['param'] => $property['attributes'][$key]]) . '<span class="dot">·</span>';
                                }
                                if ($shown === 3){
                                    break;
                                }
                            } ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>
    <?php if ($properties['pagination']) {
        $properties['pagination']->route = '/property/catalog/index-ajax-short';

        echo LinkPager::widget([
            'pagination' => $properties['pagination'],
            'maxButtonCount' => 7,
            'linkOptions' => ['data-action' => 'switch-page'],
        ]);
    }
} else { ?>
<div class="home-list">
    <p>
        <?= !empty($seo['emptyText']) ? $seo['emptyText'] : Yii::t('main', 'No results found'); ?>
    </p>
</div>
    <div class="text-center margin-bottom30">
        <?php if (Yii::$app->user->isGuest) {
            echo Html::a(Yii::t('main', 'Post for free'), null, ['data-toggle' => 'modal', 'data-target' => '#signup-modal', 'class' => 'btn btn-big btn-white-blue']);
        } else {
            echo Html::a(Yii::t('main', 'Post for free'), ['/property/manage/create'], ['class' => 'btn btn-big btn-white-blue']);
        } ?>
    </div>
<?php } ?>
