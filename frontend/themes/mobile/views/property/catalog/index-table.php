<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:12
 */

use common\assets\GoogleAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use frontend\assets\plugins\FancyBoxAsset;
use frontend\themes\mobile\assets\CatalogAsset;
use frontend\themes\mobile\assets\FilterAsset;
use frontend\themes\mobile\assets\RequestAsset;
use frontend\widgets\PropertyFilter;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $properties
 * @var array $seo
 * @var array $sortData
 * @var View $this
 * @var PropertyFilter $propertyFilter
 * @var string $lat
 * @var string $lng
 */

CatalogAsset::register($this);
GoogleAsset::register($this);
AutoCompleteAsset::register($this);
FilterAsset::register($this);
RequestAsset::register($this);
FancyBoxAsset::register($this);

?>

<?= \frontend\widgets\mobile\BottomCatalogMenu::widget() ?>

    <div id="filter-component">
        <?= $propertyFilter->run() ?>
    </div>
    <div class="catalog-wrap clearfix">
        <div class="catalog-left" id="properties-list">
            <?= $this->render('partial/list', [
                'sortData' => $sortData,
                'seo' => $seo,
                'properties' => $properties
            ]) ?>
        </div>
        <div class="catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?php $baseUrl = Url::to(['/property/catalog/search']);
$catalogViewUrl = Url::to(['/property/catalog/view']);
$markerListUrl = Url::to(['/property/marker/catalog']);
$markerViewUrl = Url::to(['/property/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$complexUrl = Url::to(['/property/catalog/index-ajax']);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);
$polygon = json_encode($polygon);

$script = <<<JS
    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };

    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        submitOnChange: false,
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    
    let mapInitialized = false;
    let map = null;
    
    $(document).on('click', '#bottom-menu-map', function() { 
        if(mapInitialized === false) {
            map = new CatalogMap({
                lat: '{$lat}',
                lon: '{$lng}',
                mode: 'advanced',
                markerViewRoutes: {
                    property: '{$markerViewUrl}',  
                },
                markerIcons: {
                    property: '{$markerPropertyIcon}'
                },
                polygon: {$polygon}
            });
            map.init();
            let params = filter.buildQuery();
            
            map.fetchMarkers('{$markerListUrl}', params);
            mapInitialized = true;
        }
    });
    
    $(document).on('click', '#properties-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if(result.success === true) {
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({result: true}, 'Page', result.url);
            } 
        });
        
        return false;
    });
    
    let fn = function(result) {
        if (result.domain === false) {
            if(mapInitialized === true) {
                map.setMarkers(result.markers);
                map.setPolygon(result.polygon);
            }
            $('#properties-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $(document).on('submit', filter.options.filterSelector, function() {
         let params = filter.buildQuery();
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
         let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false
    });
JS;
$this->registerJs($script);