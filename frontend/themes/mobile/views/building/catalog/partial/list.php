<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var array $sortData
 * @var array $seo
 * @var array $buildings
 * @var integer $gridSize
 */

?>
    <div class="loading-overlay hidden"></div>

    <div>
        <h1 class="text-left"><?= $seo['heading'] ?></h1>
    </div>
<?php if (count($buildings['items']) > 0) { ?>
    <div class="home-list">
        <?php foreach ($buildings['items'] as $building) { ?>
            <div class="mobile-request-item flex space-between">
                <div class="mobile-request-item-info">
                    <div class="mobile-request-row">
                        <div class="mobile-request-id">
                            <?= $building['id'] ?>
                        </div>
                        <div class="mobile-request-title">
                            <a href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']]) ?>"
                               data-action="load-modal-building">
                                <?= $building['title'] ?>
                            </a>
                        </div>
                    </div>
                    <div class="flex space-between">
                        <div class="mobile-request-price">
                            <?php if(count($building['images']) > 0) { ?>
                                <?php foreach ($building['images'] as $key => $image) { ?>
                                    <?php if ($key === 0) { ?>
                                        <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($building['images']), $image, [
                                            'data-fancybox' => "gallery-{$building['id']}",
                                            'data-role' => 'fancy-gallery-item'
                                        ]) ?>
                                    <?php } else { ?>
                                        <?= Html::a(null, $image, [
                                            'data-fancybox' => "gallery-{$building['id']}",
                                            'class' => 'hidden',
                                            'data-role' => 'fancy-gallery-item'
                                        ]) ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } else { ?>
                                <i class="icon-camera"></i>&nbsp;<?= count($building['images']) ?>
                            <?php } ?>
                        </div>
                        <div>
                            <a href="<?= Url::to(['/building/manage/update', 'id' => $building['id']]) ?>">
                                <i class="icon-pencil"></i>
                            </a>
                            <?= Html::a('<i class="icon-delete-button"></i>', [
                                '/building/manage/delete', 'id' => $building['id']
                            ], ['data-confirm' => Yii::t('main', 'Do you really want to delete this item?')]) ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php
    if ($buildings['pagination']) {
        $buildings['pagination']->route = '/building/catalog/index-ajax-short';

        echo LinkPager::widget([
            'pagination' => $buildings['pagination'],
            'maxButtonCount' => 7,
            'linkOptions' => ['data-action' => 'switch-page'],
        ]);
    }
} else { ?>
    <p>
        <?= !empty($seo['emptyText']) ? $seo['emptyText'] : Yii::t('main', 'No results found'); ?>
    </p>
<?php } ?>