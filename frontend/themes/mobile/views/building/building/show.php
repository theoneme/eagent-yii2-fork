<?php

use frontend\forms\contact\ContactAgentForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $building
 * @var array $counts
 * @var array $seo
 * @var array $realtors
 * @var array $similar
 * @var View $this
 * @var ContactAgentForm $contactAgentForm
 */

\common\assets\GoogleAsset::register($this);
frontend\themes\mobile\assets\PropertyModalAsset::register($this);

?>


    <div class="modal-body">
        <?php if (!empty($building['images'])) { ?>
            <div class="modal-slider" data-role="main-slider">
                <div class="modal-slide">
                    <?= Html::a(Html::img(array_shift($building['thumbnails']), ['alt' => $seo['keywords'], 'title' => $seo['keywords']]), array_shift($building['images']), ['data-fancybox' => 'gallery']) ?>
                </div>
                <?php foreach ($building['images'] as $key => $image) { ?>
                    <div class="modal-slide">
                        <?= Html::a(Html::img($building['thumbnails'][$key], ['alt' => $seo['keywords'], 'title' => $seo['keywords']]), $image, ['data-fancybox' => 'gallery']) ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="modal-info">
            <h1 class="text-left" id="fancy-title">
                <?= $seo['heading'] ?>
            </h1>
            <div class="modal-home-descr">
                <div class="hide-content hide-text">
                    <p>
                        <?= $building['description'] ?>
                    </p>
                </div>
                <a class="more-less" data-height="200">
                    <?= Yii::t('main', 'More') ?>
                    <i class="icon-down"></i>
                </a>
            </div>
            <h3>
                <?= Yii::t('building', 'Condos in house{title}{address}', [
                    'address' => !empty($building['address']) ? Yii::t('seo', ' at {address}', ['address' => $building['address']]) : '',
                    'title' => !empty($building['translations']['name']) ? Yii::t('seo', ' «{title}»', ['title' => $building['translations']['name']]) : '',
                ]) ?>
            </h3>
            <div class="bedrooms-block">
                <ul class="no-list history-switch no-margin">
                    <?php
                    $active = true;
                    if (array_key_exists('sale', $counts)) {
                        echo Html::tag('li',
                            Html::a(
                                '<div class="count-block text-center cb-green">' . $counts['sale']['count'] . '</div> ' . Yii::t('building', 'For Sale'),
                                '#sale',
                                ['data-toggle' => 'tab']
                            ),
                            ['class' => $active ? 'active' : '']
                        );
                        $active = false;
                    }
                    if (array_key_exists('rent', $counts)) {
                        echo Html::tag('li',
                            Html::a(
                                '<div class="count-block text-center cb-green">' . $counts['rent']['count'] . '</div> ' . Yii::t('building', 'For Rent'),
                                '#rent',
                                ['data-toggle' => 'tab']
                            ),
                            ['class' => $active ? 'active' : '']
                        );
                        $active = false;
                    }
                    if (array_key_exists('off', $counts)) {
                        echo Html::tag('li',
                            Html::a(
                                '<div class="count-block text-center cb-green">' . $counts['off']['count'] . '</div> ' . Yii::t('building', 'Unavailable'),
                                '#off',
                                ['data-toggle' => 'tab']
                            ),
                            ['class' => $active ? 'active' : '']
                        );
                    } ?>
                </ul>
                <div class="tab-content tab-history">
                    <?php
                    $active = true;
                    if (array_key_exists('sale', $counts)) { ?>
                        <div class="history-content tab-pane fade in <?= $active ? 'active' : '' ?>"
                             id="sale">
                            <?php foreach ($counts['sale']['items'] as $rooms => $data) {
                                echo $this->render('@frontend/views/building/building/room-group', ['tab' => 'sale', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                            } ?>
                        </div>
                        <?php $active = false;
                    }
                    if (array_key_exists('rent', $counts)) { ?>
                        <div class="history-content tab-pane fade in <?= $active ? 'active' : '' ?>"
                             id="rent">
                            <?php foreach ($counts['rent']['items'] as $rooms => $data) {
                                echo $this->render('@frontend/views/building/building/room-group', ['tab' => 'rent', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                            } ?>
                        </div>
                        <?php $active = false;
                    }
                    if (array_key_exists('off', $counts)) { ?>
                        <div class="history-content tab-pane fade in <?= $active ? 'active' : '' ?>"
                             id="off">
                            <?php foreach ($counts['off']['items'] as $rooms => $data) {
                                echo $this->render('@frontend/views/building/building/room-group', ['tab' => 'off', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                            } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <?php if (!empty($building['attributes'])) { ?>
                <div>
                    <h4 class="up-title text-left">
                        <?= Yii::t('building', 'Building Parameters') ?>
                    </h4>
                    <div class="modal-info-block-content">
                        <?php foreach ($building['attributes'] as $attribute) { ?>
                            <div class="info-block-item">
                                <?= "{$attribute['title']}: {$attribute['value']}" ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <?php if (!empty($building['advancedAttributes'])) { ?>
                <?php foreach ($building['advancedAttributes'] as $attribute) { ?>
                    <div>
                        <h4 class="up-title text-left">
                            <?= $attribute['title'] ?>
                        </h4>
                        <div class="modal-info-block-content">
                            <?php foreach ($attribute['values'] as $value) { ?>
                                <div class="info-block-item">
                                    <i class="icon-down text-success"></i>&nbsp;<?= $value ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <?php if ($building['lat'] && $building['lng']) { ?>
                <section class="params-block">
                    <div class="params-header" data-toggle="collapse" data-target="#school" data-action="init-gmap">
                        <h2 class="text-left"><?= Yii::t('property', 'Building on map') ?></h2>
                    </div>
                    <div class="collapse" id="school">
                        <div id="modal-map" class="modal-property-map"
                             data-lat="<?= $building['lat'] ?>"
                             data-lng="<?= $building['lng'] ?>" data-type="property">
                        </div>
                    </div>
                </section>
            <?php } ?>
            <?php if (!empty($similar)) { ?>
                <div class="home-similar">
                    <h2 class="modal-h2">
                        <?= Yii::t('building', 'Similar Buildings') ?>
                    </h2>
                    <div class="similar-slider" data-role="similar-slider">
                        <?php foreach ($similar as $similarItem) {
                            echo $this->render('@frontend/views/building/building/similar', ['building' => $similarItem]);
                        } ?>
                    </div>
                </div>
            <?php } ?>
            <div>
                <?= $this->render('@frontend/views/agent/contact-agent-block', [
                    'contactAgentForm' => $contactAgentForm,
                    'realtors' => $realtors,
                    'slug' => $building['slug'],
                    'url' => Url::to(['/building/building/show', 'slug' => $building['slug']], true),
                    'title' => $building['title'],
                ]) ?>
            </div>
        </div>
        <div class="modal-home-footer">
            <div class="nearby-cols row">
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Nearby Cities') ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li><a href="#"><?= Yii::t('property', 'Moscow') ?></a></li>
                        <li><a href="#"><?= Yii::t('property', 'St. Peterburg') ?></a></li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Nearby Zip Codes') ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li><a href="#">33060</a></li>
                        <li><a href="#">33062</a></li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
            </div>
        </div>
    </div>

    <div id="fancy-caption-container"
         class="hidden <?= empty($building['attributes']) ? 'empty-fancy-container' : '' ?>">
        <div>
            <?php if (!empty($building['attributes'])) { ?>
                <h4 class="up-title text-left">
                    <?= Yii::t('building', 'Building Parameters') ?>
                </h4>
                <ul class="no-list fancybox-attributes">
                    <?php foreach ($building['attributes'] as $attribute) { ?>
                        <li>
                            <?= "{$attribute['title']}: {$attribute['value']}" ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>

            <?= $this->render('@frontend/views/agent/contact-agent-block', [
                'contactAgentForm' => $contactAgentForm,
                'realtors' => array_slice($realtors, 0, 1),
                'url' => Url::to(['/building/building/show', 'slug' => $building['slug']], true),
                'title' => $building['title'],
                'blockId' => 'fancy'
            ]) ?>
        </div>
    </div>

<?php $more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');
$propertiesUrl = Url::to(['/property/ajax/building-properties']);

$script = <<<JS
    var moreLess = $('.more-less');
    
    moreLess.MoreLess({
        moreText: '{$more}&nbsp;',
        lessText: '{$less}&nbsp;'
    }); 
    
    var fn = function() {
        $('.bedroom-group [data-page]').off('click').on('click', function() {
            let container = $(this).closest('.bedroom-group').find('[data-role=properties-container]'),
                href = $(this).attr('href');
            
            $.get(href, {}, function(result) {
                if(result.success === true) {
                    container.data('loaded', 1);
                    container.html(result.html);
                    fn();
                } 
            });
            
            return false;
        });
    };
    
    $('[data-action=load-properties]').on('click', function() {
        let buildingId = $(this).data('building'),
            operation = $(this).data('operation'),
            rooms = $(this).data('rooms'),
            that = $(this),
            container = that.closest('.bedroom-group').find('[data-role=properties-container]');
        
        if(container.data('loaded') !== 1) {
            $.get('{$propertiesUrl}', {operation: operation, building_id: buildingId, rooms: rooms}, function(result) {
                if(result.success === true) {
                    container.data('loaded', 1);
                    container.html(result.html);
                    fn();
                } 
            });
        }
    });
    
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false,
        infobar: false,
        buttons: [
            'close'
        ],
    });
JS;

$this->registerJs($script);

if ($building['lat'] && $building['lng']) {
    $markerPropertyIcon = Url::to(['/images/marker.png'], true);
    $script = <<<JS
        var mapInitialized = false;
        
        $('[data-action=init-gmap]').on('click', function() {
            if(mapInitialized === false) {
                var modalMap = new ModalMap({
                    markerIcons: {
                        property: '{$markerPropertyIcon}'
                    }
                });
                modalMap.init();
                modalMap.setMarker();
                
                mapInitialized = true;
            } 
        });
JS;
    $this->registerJs($script);
}