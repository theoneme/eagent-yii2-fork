<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.11.2018
 * Time: 16:36
 */

use frontend\forms\contact\ContactAgentForm;
use yii\web\View;

frontend\themes\mobile\assets\PropertyDirectModalAsset::register($this);

/**
 * @var array $building
 * @var array $seo
 * @var array $realtors
 * @var array $similar
 * @var array $counts
 * @var View $this
 * @var string $catalogCategory
 * @var ContactAgentForm $contactAgentForm
 */

?>

<?//= \frontend\widgets\mobile\BottomPropertyMenu::widget([
  //  'entity' => 'building',
  //  'object' => $building
//]) ?>


<?= \frontend\widgets\mobile\BottomBuildingMenu::widget(['entity' => 'building',
    'object' => $building]) ?>

    <div id="direct-container">
        <?= $this->render('show', [
            'building' => $building,
            'similar' => $similar, 'seo' => $seo,
            'realtors' => $realtors,
            'contactAgentForm' => $contactAgentForm,
            'counts' => $counts
        ]) ?>
    </div>

<?php $script = <<<JS
    $('#direct-container').PropertyDirect();
JS;
$this->registerJs($script);