<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 03.12.2018
 * Time: 15:27
 */

frontend\themes\mobile\assets\ArticleAsset::register($this);

?>

<div class="news-content">
    <div class="news-text-side"><h1 class="news-title text-left">
            Спорт, правильное питание.</h1>
        <div class="clearfix">
            <a class="alias-news-comment" href="#news-comments">
                0 комментариев </a>
            <div class="album-opt like-over pull-right" style="margin-top: 8px;">
                <i class="icon-thumbs-up-hand-symbol"></i>
                <span>0</span></div>
            <div class="album-opt pull-right" style="margin-top: 8px;">
                <i class="icon-eye"></i>
                <span>4</span></div>
        </div>
        <div class="news-img text-center">
            <img src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/10/page-5bd3d1096a6a2.jpg" alt=""></div>
        <div class="share-date">
            <div class="share">
                <div class="work-share text-center">
                    <a class="fb hint--bottom social-share text-center"
                       href="#"
                       title="Поделиться в Facebook"><i class="icon-facebook-logo" aria-hidden="true"></i></a>
                    <a class="in hint--bottom social-share text-center"
                            href="#"
                            title="Поделиться в Vk"><i class="icon-vk-social-network-logo" aria-hidden="true"></i></a><a
                            class="tw hint--bottom social-share text-center"
                            href="#"
                            title="Поделиться в Twitter"><i class="icon-twitter" aria-hidden="true"></i></a><a
                            class="g hint--bottom social-share text-center"
                            href="#"
                            title="Поделиться в Google+"><i class="icon-google-plus" aria-hidden="true"></i></a><a
                            class="in hint--bottom social-share text-center"
                            href="#"
                            title="Поделиться в LinkedIn"><i class="icon-linkedin" aria-hidden="true"></i></a><a
                            class="in hint--bottom email-share text-center" href="#" title="Отправить по email"
                            data-toggle="modal" data-target="#share-on-email"><i class="icon-open-mail-interface-symbol"
                                                                                 aria-hidden="true"></i></a>
                    <div class="modal fade" id="share-on-email" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog share-form">
                            <div class="modal-content">
                                <div class="modal-header">
                                    Поделиться этой ссылкой по email
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form id="email-share-form" action="/ajax/share-by-email" method="post">
                                        <input type="hidden" name="_csrf-frontend"
                                               value="ts0UHh8KvpNw8m0OTX3gPk5xWC3Wd7akkwRc1ETHZHDG9SxaT0TR5DacIE0sMYtEORsheo8ewcnMVRa4cp0WGw==">
                                        <div class="share-container">
                                            <div class="share-img">
                                                <img src="https://dwmh9kmdoyiua.cloudfront.net/cache/206x206/uploads/page/2018/10/page-5bd3d1096a6a2.jpg"
                                                     alt=""></div>
                                            <div class="share-info">
                                                <div class="share-item-title">Спорт, правильное питание.</div>
                                                <div class="share-by">
                                                    от PONOMARYOVA.MARIYA
                                                </div>
                                                <div class="share-link">
                                                    <span>Ссылка: </span>
                                                    https://ujobs.me/article/view/sport-pravilnoe-pitanie-5bd1f35e42e15?invited_by=36401
                                                </div>
                                            </div>
                                        </div>
                                        <div class="share-email">
                                            <input type="text" class="share-list" name="email"
                                                   placeholder="Введите email пользователя, с которым вы хотите поделиться этой ссылкой">
                                        </div>
                                        <div style="margin-top: 10px;">
                                            <div class="share-bottom">
                                                <div class="shares"></div>
                                                <button type="submit" class="email-share-submit btn-middle fright">
                                                    Отправить
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="spinner-bg">
                                    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                    <a class="link hint--bottom clipboard text-center" title="Копировать в буфер обмена"><i class="icon-link-symbol"
                                                                                                    aria-hidden="true"></i></a>
                    <div class="referral-link-block" style="display:none;">
                        <div class="your-link">Ваша ссылка</div>
                        <input type="text" id="clipboard-link" name="link"
                               value="https://ujobs.me/article/view/sport-pravilnoe-pitanie-5bd1f35e42e15?invited_by=36401">
                    </div>
                </div>
            </div>
            <div class="date-post">
                <div class="ujobs-circle-cell">
                    <img class="article-avatar"
                         src="https://dwmh9kmdoyiua.cloudfront.net/uploads/user/2018/10/ponomaryovamariya-5bd1ef7962f25.jpg"
                         alt=""></div>
                <div class="date-cell">
                    <div class="who-post">
                        <a href="/account/profile/show/49044">PONOMARYOVA.MARIYA</a></div>
                    <div class="date-news">25 окт. 2018 г.</div>
                </div>
            </div>
        </div>
        <div class="news-box"><p>Спорт полезен для организма. Это знают все с детства, но лишь немногие
                посвящают этому занятию свою жизнь. Для того, чтобы оставаться здоровым,
                красивым и стройным, чувствовать себя превосходно, не обязательно
                заниматься спортом профессионально. Достаточно несколько раз в неделю
                находить время на посещение тренажерного зала или бассейна, совершать
                пешие прогулки или кататься на велосипеде. Любая физическая активность –
                это возможность улучшить свое самочувствие, а также скорректировать
                параметры тела.</p></div>
        <div class="comment-news-box" id="news-comments">
            <div class="news-comtitle">
                    <div style="line-height: 50px;">
                        Комментарии
                    </div>
                    <div class="flex flex-end text-right">
                        <a class="btn btn-big btn-white-blue" data-id="9124" data-entity="page"><i
                                    class="icon-thumbs-up-hand-symbol"></i>&nbsp;Оценить</a>
                        <a class="btn btn-big btn-blue-white" data-id="49044" data-entity="profile"><i
                                    class="icon-rss-symbol"></i>&nbsp;Подписаться</a></div>
            </div>
            <div>
                Нет комментариев
            </div>
            <div class="news-comtitle">Отправить комментарий</div>
            <form id="comment-form" action="/comment/create" method="post">
                <input type="hidden" name="_csrf-frontend"
                       value="ts0UHh8KvpNw8m0OTX3gPk5xWC3Wd7akkwRc1ETHZHDG9SxaT0TR5DacIE0sMYtEORsheo8ewcnMVRa4cp0WGw==">
                <div class="form-group field-postcommentform-entityid"><input type="hidden"
                                                                              id="postcommentform-entityid"
                                                                              class="form-control"
                                                                              name="PostCommentForm[entityId]"
                                                                              value="9124">
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-postcommentform-comment required">
                    <label class="control-label" for="postcommentform-comment">Комментарий</label><textarea
                            id="postcommentform-comment" class="form-control" name="PostCommentForm[comment]"
                            aria-required="true"></textarea>
                    <div class="help-block"></div>
                </div>
                <div class="form-group field-postcommentform-name required">
                            <label class="control-label" for="postcommentform-name">Имя</label><input type="text"
                                                                                                      id="postcommentform-name"
                                                                                                      class="form-control"
                                                                                                      name="PostCommentForm[name]"
                                                                                                      value=""
                                                                                                      aria-required="true">
                            <div class="help-block"></div>
                </div>
                <div class="form-group field-postcommentform-email required">
                            <label class="control-label" for="postcommentform-email">Email</label><input type="text"
                                                                                                         id="postcommentform-email"
                                                                                                         class="form-control"
                                                                                                         name="PostCommentForm[email]"
                                                                                                         aria-required="true">
                            <div class="help-block"></div>
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-big btn-blue-white">Отправить комментарий</button>
                </div>
            </form>
        </div>
    </div>
    <div class="news-aside">
        <div class="news-aside-block">
            <div class="news-aside-title">Похожие статьи</div>
                <div class="related-post row" data-key="9211">
                    <div class="related-post-img">
                        <a href="#"><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                    alt="" title=""></a></div>
                    <a class="related-alias" href="#">ДО 12 000 ГРН. НА ВАШУ
                        КАРТУ ПОД 0,1% В ДЕНЬ ОБРАЩЕНИЯ</a>
                    <p>
                        До 12 000 грн. без справки о доходах и поручителей. Получите кредит онлайн...</p></div>
                <div class="related-post row" data-key="9204">
                    <div class="related-post-img">
                        <a href="#"><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/images/new/locale/ru-RU/no-image.png"
                                    alt="" title=""></a></div>
                    <a class="related-alias" href="#">Работа
                        для всех,просто и легко.</a>
                    <p>
                        Помощник руководителя Обязаности: Контроль за работой отдела Работа с документ...</p></div>
                <div class="related-post row" data-key="9194">
                    <div class="related-post-img">
                        <a href="#"><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/11/page-5bfebd0960f35.jpg"
                                    alt="" title=""></a></div>
                    <a class="related-alias" href="#">Оператор</a>
                    <p>
                        Обязанности: Принятие входящих звонковРабота с документамиПроведение собесед...</p></div>
                <div class="related-post row" data-key="9184">
                    <div class="related-post-img">
                        <a href="#"><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/images/new/locale/ru-RU/no-image.png"
                                    alt="" title=""></a></div>
                    <a class="related-alias" href="#">Художественное
                        граффити-оформление</a>
                    <p>
                        Нанесение различных рисунков на все возможные поверхности</p></div>
                <div class="related-post row" data-key="9174">
                    <div class="related-post-img">
                        <a href="#"><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/images/new/locale/ru-RU/no-image.png"
                                    alt="" title=""> </a></div>
                    <a class="related-alias"
                       href="#">Официальная работа в
                        интернете- реальность в которую я поверил</a>
                    <p>
                        Добрый день, предлагаю вам на обозрение вот такой вот сервис по трудоустрой...</p></div>
        </div>
    </div>
</div>