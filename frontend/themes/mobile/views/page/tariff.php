<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 03.10.2018
 * Time: 11:42
 */

use common\components\CurrencyHelper;

frontend\themes\mobile\assets\TariffAsset::register($this);

?>
<div class="tariff-wrap">
    <h2 class="text-center"><?= Yii::t('tariff', 'Tariffs for agents for receiving requests on property purchases'); ?></h2>
    <p class="text-center tariff-p"><?= Yii::t('tariff', 'You will receive requests (leads) from clients who are interested in buying property'); ?></p>

    <div class="tariff-block">
        <div class="tariff-box tariff-red">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Minimal'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '10']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority in agents catalog'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'No'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free customer management CRM'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority for your listings in catalog and similar propositions block'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', '{object} objects<br/>({count} hits)', ['object' => '2', 'count' => '100']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-agentava-na-poluchenie-5bb4aec79a0cc-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-blue">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Standard'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 20000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '50']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority in agents catalog'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free customer management CRM'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority for your listings in catalog and similar propositions block'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', '{object} objects<br/>({count} hits)', ['object' => '10', 'count' => '300']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-agentava-na-poluchenie-5bb4aec79a0cc-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-gray">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Advanced'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 50000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '200']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority in agents catalog'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free customer management CRM'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority for your listings in catalog and similar propositions block'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', '{object} objects<br/>({count} hits)', ['object' => '30', 'count' => '1000']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-agentava-na-poluchenie-5bb4aec79a0cc-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <h2 class="text-center"><?= Yii::t('tariff', 'Tariffs for agents for receiving requests on property sales'); ?></h2>
    <p class="text-center tariff-p"><?= Yii::t('tariff', 'You will receive requests (leads) from clients who are interested in selling property'); ?></p>

    <div class="tariff-block">
        <div class="tariff-box tariff-red">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Minimal'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 10000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '10']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority in agents catalog'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'No'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free customer management CRM'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority for your listings in catalog and similar propositions block'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', '{object} objects<br/>({count} hits)', ['object' => '2', 'count' => '100']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-agentov-na-poluchenie-5bb73a7456bf2-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-blue">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Standard'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 30000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '50']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority in agents catalog'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free customer management CRM'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority for your listings in catalog and similar propositions block'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', '{object} objects<br/>({count} hits)', ['object' => '10', 'count' => '300']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-agentov-na-poluchenie-5bb73a7456bf2-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-gray">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Advanced'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 100000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '200']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority in agents catalog'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free customer management CRM'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority for your listings in catalog and similar propositions block'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', '{object} objects<br/>({count} hits)', ['object' => '30', 'count' => '1000']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-agentov-na-poluchenie-5bb73a7456bf2-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <h2 class="text-center"><?= Yii::t('tariff', 'Tariffs for Banks for receiving requests on mortgage'); ?></h2>
    <p class="text-center tariff-p"><?= Yii::t('tariff', 'Grants access for receiving requests from clients which are interested in mortgage. <br> Also provides extra traffic for your site'); ?></p>
    <div class="tariff-block">
        <div class="tariff-box tariff-red">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Minimal'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 100000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '20']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority on receiving requests'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'No'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free CRM and support. Search for other propositions if needed'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', 'No'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Bank banner in the partners section on the main page'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '100']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-bankov-na-poluchenie-5bb73e5f8858d-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-blue">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Standard'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 200000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '50']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority on receiving requests'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'No'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free CRM and support. Search for other propositions if needed'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Bank banner in the partners section on the main page'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '300']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-bankov-na-poluchenie-5bb73e5f8858d-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-gray">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Advanced'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 500000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Requests for property purchase'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} per month', ['count' => '200']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Additional priority on receiving requests'); ?> —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free CRM and support. Search for other propositions if needed'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Bank banner in the partners section on the main page'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '1000']); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/tarify-dlya-bankov-na-poluchenie-5bb73e5f8858d-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <h2 class="text-center"><?= Yii::t('tariff', 'Banner advertising on the site'); ?></h2>
    <p class="text-center tariff-p"><?= Yii::t('tariff', 'Helps you to attract more visitors for your site'); ?></p>
    <div class="tariff-block">
        <div class="tariff-box tariff-red">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Minimal'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 10000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Object Card'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '200']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'On the main page below in the Partners section'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '200']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free CRM and support. Search for other propositions if needed'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/bannernaya-reklama-na-sayte-5bb74a791edd3-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-blue">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Standard'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 30000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Object Card'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '800']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'On the main page below in the Partners section'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '800']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free CRM and support. Search for other propositions if needed'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/bannernaya-reklama-na-sayte-5bb74a791edd3-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="tariff-box tariff-gray">
            <div class="tariff-head">
                <div class="tariff-title text-center"><?= Yii::t('tariff', 'Advanced'); ?></div>
                <div class="tariff-price text-center">
                    <span>
                        <?= Yii::t('tariff', '{price} per month', [
                            'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 100000)
                        ]) ?>
                    </span>
                </div>
            </div>
            <div class="tariff-body">
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Object Card'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '3 000']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'On the main page below in the Partners section'); ?> —
                    </div>
                    <?= Yii::t('tariff', '{count} hits per month', ['count' => '800']); ?>
                </div>
                <div class="tariff-item">
                    <div class="tariff-mobile-text">
                        <?= Yii::t('tariff', 'Free CRM and support. Search for other propositions if needed'); ?>
                        —
                    </div>
                    <?= Yii::t('tariff', 'Yes'); ?>
                </div>
                <div class="center-button text-center">
                    <a class="btn btn-small btn-blue-white" target="_blank" href="https://ujobs.me/bannernaya-reklama-na-sayte-5bb74a791edd3-job">
                        <?= Yii::t('tariff', 'Choose tariff'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>