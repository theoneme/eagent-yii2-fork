<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.12.2018
 * Time: 16:14
 */

namespace frontend\helpers;

use Yii;

/**
 * Class FilterDataHelper
 * @package frontend\helpers
 */
class FilterDataHelper
{
    /**
     * @return array
     */
    public static function getRoomsFilters()
    {
        return [
            1 => Yii::t('catalog', '{rooms, plural, one{#+ room} other{#+ rooms}}', ['rooms' => 1]),
            2 => Yii::t('catalog', '{rooms, plural, one{#+ room} other{#+ rooms}}', ['rooms' => 2]),
            3 => Yii::t('catalog', '{rooms, plural, one{#+ room} other{#+ rooms}}', ['rooms' => 3]),
            4 => Yii::t('catalog', '{rooms, plural, one{#+ room} other{#+ rooms}}', ['rooms' => 4]),
            5 => Yii::t('catalog', '{rooms, plural, one{#+ room} other{#+ rooms}}', ['rooms' => 5]),
        ];
    }
}