<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.12.2018
 * Time: 16:14
 */

namespace frontend\helpers;

/**
 * Class ViewToTemplateHelper
 * @package frontend\helpers
 */
class ViewToTemplateHelper
{
    /**
     * @param $cookieView
     * @return string
     */
    public static function getTemplate($cookieView)
    {
        switch ($cookieView) {
            case 'list':
                $template = 'index-table';
                break;
            case 'map':
                $template = 'index-map';
                break;
            case 'grid':
            default:
                $template = 'index';

        }

        return $template;
    }
}