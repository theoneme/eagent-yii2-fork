<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.12.2018
 * Time: 16:14
 */

namespace frontend\helpers;

use common\helpers\UtilityHelper;
use common\mappers\TranslationsMapper;
use common\models\Property;

/**
 * Class PropertySlugFinder
 * @package frontend\helpers
 */
class PropertySlugFinder
{
    public static function getSlug(Property $property)
    {
        $translations = TranslationsMapper::getMappedData($property['translations']);

        UtilityHelper::debug($translations);
    }
}