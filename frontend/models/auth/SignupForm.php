<?php

namespace frontend\models\auth;

use common\helpers\UtilityHelper;
use common\models\User;
use common\services\entities\UserService;
use common\validators\PhoneInputValidator;
use GeoIp2\Database\Reader;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Yii;
use yii\base\Model;

/**
 * Class SignupForm
 * @package frontend\models
 */
class SignupForm extends Model
{
    public const VALIDATE_AS_EMAIL = 0;
    public const VALIDATE_AS_PHONE = 10;

    public static $usernameRegexp = '/^[-a-zA-Zа-яА-ЯєЄіІїЇёЁ0-9_\.@+\(\)]+$/u';
    /**
     * Determines how login field must be validated
     * @var integer
     */
    public $validateAs;
    /**
     * @var string User phone
     */
    public $phone;
    /**
     * @var string User email
     */
    public $email;
    /**
     * @var string User login
     */
    public $login;
    /**
     * @var string Password
     */
    public $password;
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * SignupForm constructor.
     * @param UserService $userService
     * @param array $config
     */
    public function __construct(UserService $userService, array $config = [])
    {
        parent::__construct($config);
        $this->_userService = $userService;
    }

    /**
     * @return bool
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \MaxMind\Db\Reader\InvalidDatabaseException
     */
    public function beforeValidate()
    {
        if (filter_var($this->login, FILTER_VALIDATE_EMAIL)) {
            $this->validateAs = self::VALIDATE_AS_EMAIL;
        } else {
            $this->validateAs = self::VALIDATE_AS_PHONE;
            $this->login = '+' . preg_replace('/[^0-9]/', '', $this->login);
            $phoneUtil = PhoneNumberUtil::getInstance();
            $valid = false;
            try {
                $phone = $phoneUtil->parse($this->login, null);
                if ($phoneUtil->isValidNumber($phone)) {
                    $valid = true;
                }
            } catch (NumberParseException $e) {

            }

            if (!$valid) {
                $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-Country.mmdb');
                $ip = Yii::$app->request->getUserIP();
//				$ip = '91.246.224.1';
                if ($ip !== '127.0.0.1' && strlen($ip) > 0) {
                    $region = $geoIpReader->country($ip)->country->isoCode;
                    if ($region !== null) {
                        try {
                            $phone = $phoneUtil->parse(preg_replace('/[^0-9]/', '', $this->login), $region);
                            $this->login = $phoneUtil->format($phone, PhoneNumberFormat::E164);
                        } catch (NumberParseException $e) {

                        }
                    }
                }
            }
        }

        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['login', 'filter', 'filter' => 'trim'],
            ['login', 'required'],
            'emailPattern' => ['login', 'email', 'when' => function ($model) {
                return $model->validateAs === self::VALIDATE_AS_EMAIL;
            }],
            'emailUnique' => [
                'login',
                'unique',
                'targetClass' => User::class,
                'message' => Yii::t('main', 'This email address has already been taken'),
                'targetAttribute' => 'email',
                'when' => function ($model) {
                    return $model->validateAs === self::VALIDATE_AS_EMAIL;
                }
            ],

            // phone rules
            ['login',
                'string',
                'min' => 9,
                'max' => 15,
                'tooShort' => Yii::t('main', 'Phone must contain from {0} numbers', 9),
                'tooLong' => Yii::t('main', 'Phone must contain up to {0} numbers', 15),
                'when' => function ($model) {
                    return $model->validateAs === self::VALIDATE_AS_PHONE;
                }
            ],
            ['login',
                'unique',
                'targetClass' => User::class,
                'message' => Yii::t('main', 'This phone has already been taken'),
                'targetAttribute' => 'phone',
                'when' => function ($model) {
                    return $model->validateAs === self::VALIDATE_AS_PHONE;
                }
            ],
            ['login',
                PhoneInputValidator::class,
                'message' => Yii::t('main', 'Enter phone in full format, including county code. Example +7 (123) 456 78 90'),
                'when' => function ($model) {
                    return $model->validateAs === self::VALIDATE_AS_PHONE;
                }
            ],
            // password rules
            ['password', 'required', 'skipOnEmpty' => false],
            ['password', 'string', 'min' => 6, 'max' => 72],

            [['email', 'phone'], 'safe'], // one of them will be filled from login field
        ];
    }

    /**
     * @return bool
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->validateAs === self::VALIDATE_AS_EMAIL) {
            $this->email = $this->login;
            $this->phone = '';
        } else {
            $this->phone = $this->login;
            $this->email = '';
        }

        $insertId = $this->_userService->create([
            'UserForm' => [
                'username' => UtilityHelper::generateUsername($this->email, $this->phone),
                'email' => !empty($this->email) ? $this->email : null,
                'phone' => !empty($this->phone) ? $this->phone : null
            ],
            'PasswordForm' => [
                'password' => $this->password,
                'confirm' => $this->password
            ]
        ]);

        return $insertId !== null;
    }
}
