<?php

namespace frontend\models\auth;

use common\repositories\sql\UserRepository;
use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Class ResetPasswordForm
 * @package frontend\models
 */
class ResetPasswordForm extends Model
{
    /**
     * @var UserRepository
     */
    private $_userRepository;
    /**
     * @var string
     */
    public $token;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string
     */
    public $confirm;
    /**
     * @var \common\models\User
     */
    private $_user;

    /**
     * ResetPasswordForm constructor.
     * @param UserRepository $userRepository
     * @param $token
     * @param array $config
     */
    public function __construct(UserRepository $userRepository, $token, $config = [])
    {
        $this->_userRepository = $userRepository;
        $this->token = $token;

        parent::__construct($config);
    }

    /**
     * @throws \common\exceptions\RepositoryException
     */
    public function init()
    {
        if (empty($this->token) || !is_string($this->token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = $this->_userRepository->findOneByCriteria(['password_reset_token' => $this->token]);
        if ($this->_user === null) {
            throw new InvalidParamException('Wrong password reset token.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password', 'confirm'], 'required'],
            [['password', 'confirm'], 'string', 'min' => 6],
            ['confirm', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @return bool|mixed
     * @throws \ReflectionException
     * @throws \yii\base\Exception
     */
    public function resetPassword()
    {
        $result = $this->_userRepository->updateOneByCriteria(['id' => $this->_user->id], [
            'password_reset_token' => null,
            'updated_at' => time()
        ], UserRepository::MODE_LIGHT);

        return $result && $this->_userRepository->updateOneByCriteria(['id' => $this->_user->id], [
                'PasswordForm' => [
                    'password' => $this->password,
                    'confirm' => $this->password
                ]
            ]);
    }
}
