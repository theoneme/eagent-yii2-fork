<?php

namespace frontend\models\auth;

use common\interfaces\repositories\UserRepositoryInterface;
use common\models\User;
use common\repositories\sql\UserRepository;
use yii\base\Model;

/**
 * Class RequestResetForm
 * @package frontend\models
 */
class RequestResetForm extends Model
{
    /**
     * @var UserRepository
     */
    private $_userRepository;
    /**
     * @var User
     */
    public $_user;
    /**
     * @var string
     */
    public $email;

    /**
     * RequestResetForm constructor.
     * @param UserRepositoryInterface $userRepository
     * @param array $config
     */
    public function __construct(UserRepositoryInterface $userRepository, array $config = [])
    {
        parent::__construct($config);
        $this->_userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * @return bool
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function requestReset()
    {
        if (!User::isPasswordResetTokenValid($this->_user->password_reset_token)) {
            $token = $this->_user->generatePasswordResetToken();

            $result = $this->_userRepository->updateOneByCriteria(['id' => $this->_user->id], [
                'password_reset_token' => $token,
                'updated_at' => time()
            ], UserRepository::MODE_LIGHT);
            $this->_user->password_reset_token = $token;

            return $result;
        }

        return false;
    }
}
