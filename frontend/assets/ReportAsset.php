<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class ReportAsset
 * @package frontend\assets
 */
class ReportAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/report.css'
    ];
    public $js = [
        'js/classes/report-modal-loader.js',
    ];
    public $depends = [
        YiiAsset::class,
    ];
}