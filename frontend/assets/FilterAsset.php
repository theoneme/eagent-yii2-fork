<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class FilterAsset
 * @package frontend\assets
 */
class FilterAsset extends AssetBundle
{
    public $js = [
        'js/classes/filter-advanced.js',
    ];
    public $depends = [

    ];
}
