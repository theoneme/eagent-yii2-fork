<?php
/**
 * Created by PhpStorm.
 * User: Алёна
 * Date: 04.10.2018
 * Time: 13:54
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class HomesListAsset
 * @package frontend\assets
 */
class HomesListAsset extends AssetBundle
{
    public $css = [
        'css/list-homes.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}