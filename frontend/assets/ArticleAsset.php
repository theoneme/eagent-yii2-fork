<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 03.12.2018
 * Time: 16:19
 */


namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class CatalogAsset
 * @package frontend\assets
 */
class ArticleAsset extends AssetBundle
{
    public $css = [
        'css/article.css'
    ];
    public $js = [
    ];
    public $depends = [
        CommonAsset::class
    ];
}
