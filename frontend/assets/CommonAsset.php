<?php

namespace frontend\assets;

use airani\bootstrap\BootstrapRtlAsset;
use frontend\assets\plugins\FontAwesomeAsset;
use Yii;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class CommonAsset
 * @package frontend\assets
 */
class CommonAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/main.css',
        'css/vendor/animate-button.css'
    ];
    public $js = [
        'js/script.js',
        'js/classes/property-modal-loader.js',
    ];
    public $depends = [
        BootstrapAsset::class,
        FontAwesomeAsset::class,
        BootstrapPluginAsset::class,
        YiiAsset::class
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $language = Yii::$app->language;
        $rtlLanguages = ['ar-AR', 'he-IL'];

        if (in_array($language, $rtlLanguages)) {
            $this->css[] = 'css/rtl/main.css';
            $this->depends[] = BootstrapRtlAsset::class;
        }
    }
}
