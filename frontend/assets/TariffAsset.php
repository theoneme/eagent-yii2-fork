<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 03.10.2018
 * Time: 11:35
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class TariffAsset
 * @package frontend\assets
 */
class TariffAsset extends AssetBundle
{
    public $css = [
        'css/tariff.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}