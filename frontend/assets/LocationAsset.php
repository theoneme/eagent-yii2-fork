<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class LocataionAsset
 * @package frontend\assets
 */
class LocationAsset extends AssetBundle
{
    public $css = [
        'css/index.css',
        'css/locations.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}
