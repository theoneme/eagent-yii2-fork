<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ServiceAsset
 * @package frontend\assets
 */
class ServiceAsset extends AssetBundle
{
    public $css = [
        'css/service.css',
    ];

    public $depends = [
        CommonAsset::class
    ];
}