<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.09.2018
 * Time: 15:01
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AgentAsset
 * @package frontend\assets
 */
class AgentAsset extends AssetBundle
{
    public $css = [
        'css/agent.css',
    ];
    public $js = [
        'js/classes/more-less.js',
    ];
    public $depends = [
        CommonAsset::class
    ];
}