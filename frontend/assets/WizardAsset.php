<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.09.2018
 * Time: 12:41
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class WizardAsset
 * @package frontend\assets
 */
class WizardAsset extends AssetBundle
{
    public $css = [
        'css/wizard.css',
    ];
    public $js = [
        'js/classes/progress-calculator.js'
    ];
    public $depends = [
        CommonAsset::class
    ];
}