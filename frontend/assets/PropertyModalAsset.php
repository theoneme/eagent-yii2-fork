<?php

namespace frontend\assets;

use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\MoreLessAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\assets\plugins\TooltipAsset;
use yii\web\AssetBundle;

/**
 * Class PropertyModalAsset
 * @package frontend\assets
 */
class PropertyModalAsset extends AssetBundle
{
    public $css = [
        'css/modal.css',
        'css/forms.css',
    ];
    public $js = [
        'js/classes/modal-map.js',
    ];
    public $depends = [
        TooltipAsset::class,
        MoreLessAsset::class,
        SlickAsset::class,
        CommonAsset::class,
        FancyBoxAsset::class
    ];
}
