<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class JobSearchAsset
 * @package frontend\assets
 */
class JobSearchAsset extends AssetBundle
{
    public $css = [
        'css/vendor/job-search-help.css',
    ];
    public $depends = [
        JqueryAsset::class
    ];
}