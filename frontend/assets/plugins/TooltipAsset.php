<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets\plugins;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class TooltipAsset
 * @package frontend\assets\plugins
 */
class TooltipAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/tooltip.css',
    ];
    public $js = [
        'js/classes/tooltip.js',
    ];
    public $depends = [
        YiiAsset::class,
    ];
}