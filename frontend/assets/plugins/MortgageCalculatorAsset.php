<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets\plugins;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class MortgageCalculatorAsset
 * @package frontend\assets\plugins
 */
class MortgageCalculatorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/classes/mortgage-calculator.js'
    ];
    public $depends = [
        YiiAsset::class,
    ];
}