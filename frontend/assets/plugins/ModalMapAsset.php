<?php

namespace frontend\assets\plugins;

use common\assets\GoogleAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ModalMapAsset
 * @package frontend\assets\plugins
 */
class ModalMapAsset extends AssetBundle
{
    public $js = [
        'js/classes/modal-map.js',
    ];
    public $depends = [
        JqueryAsset::class,
        GoogleAsset::class
    ];
}
