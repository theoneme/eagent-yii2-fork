<?php

namespace frontend\assets\plugins;

use common\assets\GoogleAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class CatalogMapAsset
 * @package frontend\assets\plugins
 */
class CatalogMapAsset extends AssetBundle
{
    public $js = [
        'js/vendor/marker-cluster.js',
        'js/classes/custom-marker.js',
        'js/classes/catalog-map.js',
    ];
    public $depends = [
        JqueryAsset::class,
        GoogleAsset::class
    ];
}
