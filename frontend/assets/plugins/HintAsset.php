<?php

namespace frontend\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class HintAsset
 * @package frontend\assets
 */
class HintAsset extends AssetBundle
{
    public $css = [
        'css/hint.css',
    ];
    public $depends = [

    ];
}