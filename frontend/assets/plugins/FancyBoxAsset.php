<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets\plugins;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class FancyBoxAsset
 * @package frontend\assets
 */
class FancyBoxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/jquery.fancybox.min.css'
    ];
    public $js = [
        'js/vendor/jquery.fancybox.min.js',
    ];
    public $depends = [
        YiiAsset::class,
    ];
}