<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2017
 * Time: 18:04
 */

namespace frontend\assets\plugins;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class AutoCompleteAsset
 * @package frontend\assets
 */
class AutoCompleteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/vendor/auto-complete.css',
    ];
    public $js = [
        'js/classes/auto-complete.js'
    ];
    public $depends = [
        YiiAsset::class
    ];
}