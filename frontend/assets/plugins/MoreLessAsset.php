<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 11.11.2016
 * Time: 12:14
 */

namespace frontend\assets\plugins;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class MoreLessAsset
 * @package frontend\assets\plugins
 */
class MoreLessAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/classes/more-less.js'
    ];
    public $depends = [
        YiiAsset::class,
    ];
}