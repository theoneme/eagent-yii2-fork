<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 06.11.2018
 * Time: 13:12
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class ErrorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/error.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}