<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class ChildElementsAsset
 * @package frontend\assets
 */
class ChildElementsAsset extends AssetBundle
{
    public $css = [
        'css/child-elements.css',
    ];
    public $js = [
        'js/child-elements.js',
    ];
    public $depends = [
        JqueryAsset::class
    ];
}