<?php

namespace frontend\assets;

use common\assets\GoogleAsset;
use frontend\assets\plugins\CatalogMapAsset;
use yii\web\AssetBundle;

/**
 * Class CatalogAsset
 * @package frontend\assets
 */
class CatalogAsset extends AssetBundle
{
    public $css = [
        'css/catalog.css',
    ];
    public $js = [

    ];
    public $depends = [
        CommonAsset::class,
        GoogleAsset::class,
        CatalogMapAsset::class
    ];
}
