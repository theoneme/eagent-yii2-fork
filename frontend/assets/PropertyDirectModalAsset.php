<?php

namespace frontend\assets;

use frontend\assets\plugins\FancyBoxAsset;
use frontend\assets\plugins\MoreLessAsset;
use frontend\assets\plugins\SlickAsset;
use frontend\assets\plugins\TooltipAsset;
use yii\web\AssetBundle;

/**
 * Class PropertyDirectModalAsset
 * @package frontend\assets
 */
class PropertyDirectModalAsset extends AssetBundle
{
    public $js = [
        'js/classes/property-direct-modal-loader.js',
    ];
    public $depends = [
        TooltipAsset::class,
        MoreLessAsset::class,
        SlickAsset::class,
        CommonAsset::class,
        FancyBoxAsset::class
    ];
}
