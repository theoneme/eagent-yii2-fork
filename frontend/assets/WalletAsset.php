<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 29.05.2019
 * Time: 12:44
 */

namespace frontend\assets;
use yii\web\AssetBundle;

/**
 * Class WalletAsset
 * @package frontend\assets
 */

class WalletAsset extends AssetBundle
{
    public $css = [
        'css/wallet.css',
    ];
    public $js = [
    ];
    public $depends = [
        CommonAsset::class
    ];
}