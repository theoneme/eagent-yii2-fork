<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2017
 * Time: 18:04
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AuthAsset
 * @package frontend\assets
 */
class AuthAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/auth.css',
    ];
    public $depends = [
        CommonAsset::class
    ];
}