<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.12.2018
 * Time: 18:24
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ForumAsset
 * @package frontend\assets
 */

class ForumAsset extends AssetBundle
{
    public $css = [
        'css/forum.css',
    ];
    public $js = [
    ];
    public $depends = [
        CommonAsset::class
    ];
}