<?php
/**
 * Created by PhpStorm.
 * User: Единоличница млин
 * Date: 09.11.2018
 * Time: 14:51
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class FormHtmlAsset
 * @package frontend\assets
 */
class FormHtmlAsset extends AssetBundle
{
    public $css = [
        'css/form-steps.css',
    ];
    public $depends = [

    ];
}