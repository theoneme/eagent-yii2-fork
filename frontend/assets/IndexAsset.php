<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class CommonAsset
 * @package frontend\assets
 */
class IndexAsset extends AssetBundle
{
    public $css = [
        'css/index.css',
    ];
    public $js = [
        'js/classes/location-modal-loader.js',
    ];
    public $depends = [
        CommonAsset::class
    ];
}
