<?php

namespace frontend\rules;

use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Class CompanyAuthorRule
 * @package frontend\rules
 */
class CompanyAuthorRule extends Rule
{
    /**
     * @var string
     */
    public $name = 'companyAuthorRule';

    /**
     * @param int|string $userId
     * @param Item $item
     * @param array $params
     * @return bool
     */
    public function execute($userId, $item, $params)
    {
        return !empty($params['user_id']) && $params['user_id'] === $userId;
    }
}