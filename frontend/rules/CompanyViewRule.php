<?php

namespace frontend\rules;

use common\models\Company;
use common\models\CompanyMember;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * Class CompanyViewRule
 * @package frontend\rules
 */
class CompanyViewRule extends Rule
{
    /**
     * @var string
     */
    public $name = 'companyViewRule';

    /**
     * @param int|string $memberId
     * @param Item $item
     * @param array $params
     * @return bool
     */
    public function execute($memberId, $item, $params)
    {
        /* @var Company $company */
        /* @var CompanyMember $member */
        $company = ArrayHelper::getValue($params, 'company');
        $member = ArrayHelper::getValue($params, 'member');
        $asd = $member->status === CompanyMember::STATUS_ACTIVE && $company->status === Company::STATUS_ACTIVE;
        return true;
    }
}