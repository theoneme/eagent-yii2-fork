<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use common\forms\ar\PropertyForm;
use frontend\controllers\user\ManageController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\frontend\assets\CatalogAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\assets\WizardAsset::register($this);

/**
 * @var PropertyForm $userForm
 * @var array $route
 * @var array $userDTO
 * @var integer $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'Update Profile') ?></h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($route),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div class='row'>
                <div class='col-md-4'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {hint}
                    {input}
                    {error}
                </div>
            </div>",
            ],
            'options' => [
                'id' => 'user-form',
                'data-role' => 'edit-modal-form',
                'data-pj-container' => 'property-pjax',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <div class="row wizard-row">
                <div class="col-md-8 col-sm-12">
                    <div class="wizards-steps">
                        <?php foreach ($userForm->steps as $key => $step) { ?>
                            <section id="step<?= $key ?>">
                                <div class="wizard-step-header">
                                    <?= $step['title'] ?>
                                    <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($userForm->steps)]) ?></span>
                                </div>
                                <div class="row step-content">
                                    <div class="col-md-8">
                                        <?php foreach ($step['config'] as $stepConfig) { ?>
                                            <?php if ($stepConfig['type'] === 'view') { ?>
                                                <?= $this->render("steps/{$stepConfig['value']}", [
                                                    'form' => $form,
                                                    'userForm' => $userForm,
                                                    'step' => $step,
                                                    'createForm' => false,
                                                    'userDTO' => $userDTO
                                                ]) ?>
                                            <?php } ?>

                                            <?php if ($stepConfig['type'] === 'constructor') { ?>
                                                <?php foreach ($stepConfig['groups'] as $group) { ?>
                                                    <?php $attributes = $userForm->dynamicForm->getAttributesByGroup($group) ?>
                                                    <?= $this->render("partial/attributes-group", [
                                                        'form' => $form,
                                                        'attributes' => array_flip($attributes),
                                                        'dynamicForm' => $userForm->dynamicForm
                                                    ]) ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </section>
                        <?php } ?>
                        <div class="text-center">
                            <?= Html::a(Yii::t('wizard', 'Back'), Yii::$app->request->referrer, [
                                'class' => 'btn btn-big btn-white-blue'
                            ]) ?>
                            <?= Html::submitButton(Yii::t('wizard', 'Save Changes'), ['class' => 'btn btn-big btn-white-blue']) ?>
                        </div>
                    </div>
                </div>

                <nav class="col-md-4 visible-lg visible-md progress-menu-nav menu-wizard-aside">
                    <div class="nav" data-spy="affix" data-offset-top="300">
                        <div class="progress-steps">
                            <div class="progress-percent text-center">
                                0
                            </div>
                            <div class="progress-info">
                                <?= Yii::t('wizard', 'Fill your announcement with maximum amount of details, so it will be seen more often') ?>
                            </div>
                        </div>
                        <ul class="no-list progress-menu">
                            <?php foreach ($userForm->steps as $key => $step) { ?>
                                <li>
                                    <a href="#step<?= $key ?>">
                                        <?= $step['title'] ?>
                                        <div>

                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>

                        <div class="text-center">
                            <?= Html::submitButton(Yii::t('wizard', 'Save Changes'), ['class' => 'btn btn-big btn-white-blue']) ?>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php $renderAttributesRoute = Url::toRoute('/property/property/attributes');
$canonical = Url::canonical();
$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
$script = <<<JS
var calculatorFlag = false,
    isUpdate = {$isUpdate},
    validationPerformed = false,
    progressCalculator = new ProgressCalculator({
        formSelector: '#user-form'
    }),
    form = $('#user-form');

if(isUpdate) {
    form.yiiActiveForm('validate', true);
}
progressCalculator.updateProgress();

form.on('beforeSubmit', function() {
    calculatorFlag = true;
    if ($("#file-upload-input").fileinput("getFilesCount") > 0) {
        $("#file-upload-input").fileinput("upload");
        return false;
    } else {
        if(validationPerformed === true) {
             return true;
        } else {
            validationPerformed = true;
            return false;
        }
    }
}).on("afterValidateAttribute", function(event, attribute, messages) {
    if(calculatorFlag === false) {
        progressCalculator.updateProgress();
    }
}).on("afterValidate", function(event, messages, errorAttributes) { 
    progressCalculator.updateProgress();
    calculatorFlag = false;
});

let hasFileUploadError = false;
$('#file-upload-input').on('fileuploaded', function(event, data, previewId, index) {
    let response = data.response;
    $('#userform-avatar').val(response.uploadedPath).data('key', response.imageKey);
}).on('filedeleted', function(event, key) {
    $('#userform-avatar').val(null);
}).on("filebatchuploadcomplete", function() {
    if (hasFileUploadError === false) {
        form.submit();
    } else {
        hasFileUploadError = false;
    }
}).on("fileuploaderror", function(event, data, msg) {
    hasFileUploadError = true;
    $('#' + data.id).find('.kv-file-remove').click();
});
var tt = 'right';
if($('body').hasClass('rtl')){
    tt = 'left';
} 

$('.block-with-notes').popover({
    trigger: 'hover',
    placement: tt
});
$('body').scrollspy({
    target: '.progress-menu-nav',
    offset: 300
});

$(".progress-menu a").on('click', function(event) {
    if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800);
    }
});

JS;
$this->registerJs($script);

?>