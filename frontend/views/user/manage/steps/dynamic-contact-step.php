<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

    <div data-role="dynamic-contacts-container">
        <?php foreach ($userForm->contact as $key => $contact) { ?>
            <div class="block-with-notes"
                 data-toggle="popover" data-placement="right"
                 data-original-title="<?= Yii::t('wizard', 'Additional Contact') ?>"
                 data-content="<?= Yii::t('wizard', 'Set Your Additional Contact') ?>">
                <div class="row" data-role="contact-item">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <?= $form->field($contact, "[{$key}]type", ['template' => '{input}{error}'])
                            ->dropDownList(\common\helpers\DataHelper::getContacts(), [
                                'prompt' => '-- ' . Yii::t('wizard', 'Select contact type')
                            ])->label(false) ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-10">
                        <?= $form->field($contact, "[{$key}]value", ['template' => '{input}{error}'])
                            ->textInput(['placeholder' => Yii::t('wizard', 'Enter contact info')])
                            ->label(false) ?>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-2">
                        <?= Html::a('<i class="icon-close"></i>', '#', ['data-action' => 'remove-contact']) ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

<?= Html::a('<i class="icon-plus-black-symbol"></i>&nbsp;' . Yii::t('wizard', 'Add New Contact'), '#', [
    'data-action' => 'add-new-contact'
]) ?>

<?php $contactCount = count($userForm->contact);
$contactUrl = Url::to(['/user/ajax/render-contact']);
$script = <<<JS
    let iterator = {$contactCount};

    $('[data-action=add-new-contact]').on('click', function() {
        $.get('{$contactUrl}', {iterator: iterator}, function(result) {
            if(result.success === true) {
                $('[data-role=dynamic-contacts-container]').append(result.html);
            } 
            iterator++;
        });
        
        return false;
    });

    $(document).on('click', '[data-action=remove-contact]', function() {
        $(this).closest('[data-role=contact-item]').remove(); 
        
        return false;
    });
JS;

$this->registerJs($script);