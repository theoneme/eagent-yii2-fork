<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<p><?= Yii::t('wizard', 'If you wish to change your password, you can do it here') ?></p>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Password') ?>"
     data-content="<?= Yii::t('wizard', 'Set Password') ?>">
    <?= $form->field($userForm->password, 'password')->passwordInput()
    ?>
</div>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Password Confirmation') ?>"
     data-content="<?= Yii::t('wizard', 'Set Password Confirmation') ?>">
    <?= $form->field($userForm->password, 'confirm')->passwordInput()
    ?>
</div>
