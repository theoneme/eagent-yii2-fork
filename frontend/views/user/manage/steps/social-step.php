<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

use yii\helpers\Html;

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 * @var array $userDTO
 */
?>

<?php foreach (Yii::$app->params['enabledSocials'] as $social) { ?>
    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('wizard', 'Attach of detach {social} account', ['social' => $social]) ?>"
         data-content="<?= Yii::t('wizard', 'If you attach your social account to this profile then you may login with this social network on this site') ?>">
        <p>
            <?php if (array_key_exists($social, $userDTO['socials'])) { ?>
                <?= Html::a('<i class="icon-minus-symbol"></i>&nbsp;' . Yii::t('wizard', 'Detach {provider} account', ['provider' => ucfirst($social)]), [
                    '/auth/detach', 'provider' => $social
                ], ['class' => 'btn btn-small btn-white-blue']) ?>
            <?php } else { ?>
                <?= Html::a('<i class="icon-plus-black-symbol"></i>&nbsp;' . Yii::t('wizard', 'Attach {provider} account', [
                        'provider' => ucfirst($social)
                    ]), "https://eagent.me/auth/social?authclient={$social}", ['class' => 'btn btn-small btn-white-blue']) ?>
            <?php } ?>
        </p>
    </div>
<?php } ?>



