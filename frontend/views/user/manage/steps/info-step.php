<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

use common\helpers\UtilityHelper;

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */
?>


<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Profile Short Description') ?>"
     data-content="<?= Yii::t('wizard', 'Set Profile Short Description') ?>">
    <?= $form->field($userForm->meta, 'subtitle')->textarea([
        'placeholder' => Yii::t('wizard', 'Profile Short Description'),
        'rows' => 4
    ]) ?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Profile Description') ?>"
     data-content="<?= Yii::t('wizard', 'Set Profile Description') ?>">
    <?= $form->field($userForm->meta, 'description')->textarea([
        'placeholder' => Yii::t('wizard', 'Profile Description'),
        'rows' => 8
    ]) ?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Advertising on Site Allowed') ?>"
     data-content="<?= Yii::t('wizard', 'Set Permission for Advertising on Site') ?>">
    <?= $form->field($userForm, 'ads_allowed')->radioList([
        1 => Yii::t('main', 'Yes'),
        0 => Yii::t('main', 'No')
    ], [
        'item' => function ($index, $label, $name, $checked, $value){
            $chk = $checked ? 'checked' : '';
            $output = "<div class='chover radio-btn'>
            <input name='{$name}' id='userform-{$name}-{$index}' class='radio-checkbox' value='{$value}' {$chk} type='radio'>
            <label for='userform-{$name}-{$index}'>{$label}</label>
        </div>";
            return $output;
        },
        'class' => 'flex'
    ])?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Advertising on Partner Sites Allowed') ?>"
     data-content="<?= Yii::t('wizard', 'Set Permission for Advertising on Partner Sites') ?>">
    <?= $form->field($userForm, 'ads_allowed_partners')->radioList([
        1 => Yii::t('main', 'Yes'),
        0 => Yii::t('main', 'No')
    ], [
        'item' => function ($index, $label, $name, $checked, $value){
            $chk = $checked ? 'checked' : '';
            $output = "<div class='chover radio-btn'>
            <input name='{$name}' id='userform-{$name}-{$index}' class='radio-checkbox'' value='{$value}' {$chk} type='radio'>
            <label for='userform-{$name}-{$index}'>{$label}</label>
        </div>";
            return $output;
        },
        'class' => 'flex'
    ])?>
</div>