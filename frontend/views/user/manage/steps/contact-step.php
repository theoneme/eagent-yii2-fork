<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Phone') ?>"
     data-content="<?= Yii::t('wizard', 'Set Your Phone') ?>">
    <?= $form->field($userForm, 'phone', [
        'options' => [
            'class' => 'form-group ' . (empty($userForm['phone']) ? '' : 'form-group-disabled')
        ]
    ])->textInput(['disabled' => !empty($userForm['phone'])]) ?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Profile Description') ?>"
     data-content="<?= Yii::t('wizard', 'Set Profile Description') ?>">
    <?= $form->field($userForm, 'email', [
        'options' => [
            'class' => 'form-group ' . (empty($userForm['email']) ? '' : 'form-group-disabled')
        ]
    ])->textInput(['disabled' => !empty($userForm['email'])]) ?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Username') ?>"
     data-content="<?= Yii::t('wizard', 'Set Your Username') ?>">
    <?= $form->field($userForm, 'username')->textInput() ?>
</div>
