<?php

use common\components\CurrencyHelper;
use common\decorators\CurrencyLetterDecorator;
use common\decorators\CurrencyTwoLetterDecorator;
use common\decorators\WalletHistoryTemplateDecorator;
use frontend\assets\CatalogAsset;
use frontend\assets\WalletAsset;
use yii\helpers\StringHelper;

/**
 * @var array $wallets
 */

CatalogAsset::register($this);
WalletAsset::register($this);

?>

<div class="container balance-wrap">
    <h1><?= Yii::t('main', 'Payment Management')?></h1>
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12">
            <ul class="no-list ul-wallet">
                <?php
                $active = true;
                foreach ($wallets as $wallet) { ?>
                    <li class="<?= $active ? 'active' : ''?>">
                        <a href="#wallet-<?= $wallet['id']?>" data-toggle="tab" aria-expanded="false">
                            <div class="wallet-circle">
                                <div class="text-center">
                                    <?= CurrencyLetterDecorator::decorate($wallet['currency_code'])?>
                                </div>
                            </div>
                            <div class="wallet-title-info">
                                <div class="title-sum">
                                    <span class="money">
                                        <?= $wallet['balanceFormatted']?>
                                    </span>
                                </div>
                                <div class="title-number"><?= CurrencyTwoLetterDecorator::decorate($wallet['currency_code']) . $wallet['id']?></div>
                            </div>
                        </a>
                    </li>
                <?php
                    $active = false;
                } ?>
            </ul>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="tab-content">
                <?php
                $active = true;
                foreach ($wallets as $wallet) { ?>
                    <div id="wallet-<?= $wallet['id']?>" class="wallet-content tab-pane fade <?= $active ? 'active in' : ''?>">
                        <div class="wallet-title">
                            <?= Yii::t('main', '{code} wallet', ['code' => CurrencyTwoLetterDecorator::decorate($wallet['currency_code'])])?>
                        </div>
                        <div class="head-wallet">
                            <div class="wallet-circle">
                                <div class="text-center">
                                    <?= CurrencyLetterDecorator::decorate($wallet['currency_code'])?>
                                </div>
                            </div>
                            <div class="wallet-title-info">
                                <div class="title-sum">
                                    <span class="money">
                                        <?= $wallet['balanceFormatted']?>
                                    </span>
                                </div>
                                <div class="title-number"><?= CurrencyTwoLetterDecorator::decorate($wallet['currency_code']) . $wallet['id']?></div>
                            </div>
                        </div>
                        <div class="row sell-statistic-box">
                            <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                                <a href="#" class="stat-alias">
                                    <div class="sellstat-title text-center"><?= Yii::t('main', 'Income')?></div>
                                    <?= $wallet['totalIncome']?>
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                                <a href="#" class="stat-alias">
                                    <div class="sellstat-title text-center"><?= Yii::t('main', 'Costs')?></div>
                                    <?= $wallet['totalCosts']?>
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                                <a href="#" class="stat-alias">
                                    <div class="sellstat-title text-center"><?= Yii::t('main', 'Current balance')?></div>
                                    <?= $wallet['balanceFormatted']?>
                                </a>
                            </div>
                        </div>
                        <div class="wallet-btns">
<!--                            <div class="dropdown-block col-md-6">-->
<!--                                <button class="btn btn-big btn-white-blue dropdown-btn" type="button">-->
<!--                                    Запрос вывода денег <span class="caret"></span>-->
<!--                                </button>-->
<!--                                <div class="dropdown-info">-->
<!--                                    <div class="alert alert-info">-->
<!--                                        Пожалуйста, сперва заполните платежную информацию (<a-->
<!--                                                href="#">Ссылка</a>) в настройках Вашего-->
<!--                                        профиля-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="dropdown-block col-md-6">-->
<!--                                <button class="btn btn-big btn-white-blue dropdown-btn" type="button">-->
<!--                                    Перевод денег <span class="caret"></span>-->
<!--                                </button>-->
<!--                                <div class="dropdown-info dropdown-form">-->
<!--                                    <form id="" class="transfer-form" action="#" method="post">-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-sm-6">-->
<!--                                                <div class="form-group">-->
<!--                                                    <input type="text" class="amount-input" name="amount"-->
<!--                                                           placeholder="Сумма списания" style="width: auto" data-currency="USD">-->
<!--                                                </div>-->
<!--                                                <div class="form-group">-->
<!--                                                    <input type="text" class="ratio-input" name="ratio" disabled=""-->
<!--                                                           placeholder="Курс валют" style="width: auto"></div>-->
<!--                                                <div class="form-group">-->
<!--                                                    <input type="text" class="amount-to-input" name="amount_to"-->
<!--                                                           placeholder="Сумма зачисления" style="width: auto" data-currency="">-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-6">-->
<!--                                                <div class="form-group">-->
<!--                                                    <select class="form-control transfer-wallet-select" name="to_id"-->
<!--                                                            style="width: auto">-->
<!--                                                        <option data-currency="" value="">Выберите кошелёк</option>-->
<!--                                                        <option data-currency="EUR" value="100043879">EE100043879</option>-->
<!--                                                        <option data-currency="RUB" value="100000076">RR100000076</option>-->
<!--                                                    </select></div>-->
<!--                                                <div class="form-group">-->
<!--                                                    <button type="submit" class="btn btn-primary">Отправить</button>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!---->
<!---->
<!--                                    </form>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                        <div class="inbox row margin-30">
                            <div class="col-md-12">
                                <div class="all-messages">
                                    <div class="over-table-viewgigs">
                                        <table class="table-money">
                                            <tbody>
                                                <?php foreach ($wallet['histories'] as $i => $history) { ?>
                                                    <tr class="messages-row" data-key="<?= $i?>">
                                                        <td class="hidden-xs" width="50px">
                                                            <div class="wallet-avatar text-center">
                                                                <?= CurrencyLetterDecorator::decorate($wallet['currency_code'])?>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="sum">
                                                                <strong class="text-<?= $history['total'] > 0 ? 'success' : 'danger'?>"><?= $history['totalFormatted']?></strong>
                                                                <span class="doing"><?= WalletHistoryTemplateDecorator::decorate($history['template'], $history['customDataArray'])?></span>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="time text-right">
                                                                <?= Yii::$app->formatter->asDate($history['created_at'])?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $active = false;
                } ?>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<<JS
    $(document.body).on('click', '.dropdown-btn', function() {
        $(this).closest('.dropdown-block').toggleClass('open');
    });
JS;
$this->registerJs($script);
