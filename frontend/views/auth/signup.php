<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 17.10.2018
 * Time: 18:20
 */

/**
 * @var \frontend\models\auth\SignupForm $resetPasswordForm
 */

?>

<?php $script = <<<JS
    $('#signup-modal').modal('show');
JS;
$this->registerJs($script);
