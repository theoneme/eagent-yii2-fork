<?php

use frontend\models\auth\ResetPasswordForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var ResetPasswordForm $resetPasswordForm
 */

?>

    <div class="modal fade auth-modal in " id="reset-password-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="auth-modal-title"><?= Yii::t('main', 'Reset Password') ?></div>
                    <div class="auth-modal-subtitle"><?= Yii::t('main', 'Choose your new password') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'reset-password-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'action' => ['/auth/reset-password', 'token' => $resetPasswordForm->token],
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'errorOptions' => [
                                'tag' => 'label',
                                'class' => 'auth-modal-error text-left'
                            ]
                        ]
                    ]); ?>
                    <?= $form->field($resetPasswordForm, 'password')->passwordInput(['placeholder' => Yii::t('main', 'New Password')])->label(false) ?>

                    <?= $form->field($resetPasswordForm, 'confirm')->passwordInput(['placeholder' => Yii::t('main', 'Confirm Password')])->label(false) ?>

                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('main', 'Submit'), ['class' => 'btn btn-white-blue']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    $('#reset-password-modal').modal('show');
JS;
$this->registerJs($script);

