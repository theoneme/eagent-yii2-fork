<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use frontend\widgets\RequestFilter;
use yii\web\View;
use yii\helpers\Url;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\plugins\GoogleAddressAsset::register($this);
\frontend\assets\FilterAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);

/**
 * @var array $requests
 * @var array $requestMarkers
 * @var View $this
 * @var RequestFilter $requestFilter
 * @var string $lat
 * @var string $lng
 */

?>
<?= $requestFilter->run() ?>
    <div class="catalog-wrap">
        <div class="col-md-8 col-sm-12 catalog-left">
            <?php if (count($requests['items']) > 0) { ?>
                <div class="table-houses">
                    <table id="list-houses">
                        <thead>
                        <tr>
                            <th>
                                <div class="chover">
                                    <input id="allCheck" class="radio-checkbox" name="allCheck" type="checkbox">
                                    <label for="allCheck"></label>
                                </div>
                            </th>
                            <th class="up">ID</th>
                            <th class="up"><?= Yii::t('catalog', 'Title') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Publish date') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Price') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Request Showcase') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Views') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($requests['items'] as $request) { ?>
                            <tr>
                                <td>
                                    <div class="chover">
                                        <input id="house<?= $request['id'] ?>" class="radio-checkbox" name="id"
                                               value="<?= $request['id'] ?>"
                                               type="checkbox">
                                        <label for="house<?= $request['id'] ?>"></label>
                                    </div>
                                </td>
                                <td data-role="marker-data" data-property="<?= $requestMarkers[$request['id']] ?>">
                                    <?= $request['id'] ?>
                                </td>
                                <td><?= $request['title'] ?></td>
                                <td><?= Yii::$app->formatter->asDatetime($request['created_at']) ?> </td>
                                <td><?= $request['price'] ?> </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#tariff-modal">
                                        <?= Yii::t('catalog', 'View Request') ?>
                                    </a>
                                </td>
                                <td>0 </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <?php if ($requests['pagination']) { ?>
                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $requests['pagination'],
                    ]); ?>
                <?php } ?>
            <?php } else { ?>
                <p><?= Yii::t('main', 'No results found') ?></p>
            <?php } ?>

        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

    <div class="modal fade new-modal modal480" id="tariff-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close not-age" data-dismiss="modal">×</button>
                    <div class="modal-title text-center"><?= Yii::t('main', 'Attention!') ?></div>
                </div>
                <div class="modal-body">
                    <div class="title-age text-left">
                        <p class="text-center">
                            <?= Yii::t('main', 'Request details are available only for agents, who bought our premium tariff') ?>
                        </p>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-small btn-white-blue" href="<?= Url::to(['/page/tariff']) ?>">
                            <?= Yii::t('main', 'View Tariffs') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $markerPropertyIcon = Url::to(['/images/marker.png'], true);
$script = <<<JS
    // /* List houses */
    // $(document.body).on('click','#list-houses th',function() {
    //     if($(this).hasClass('active')){  
    //         $(this).toggleClass('up');
    //         $(this).toggleClass('down');
    //     } else {
    //         $('#list-houses th').removeClass('active');
    //         $(this).addClass('active');
    //     }
    // });
    //
    // $(document).on('change', '#list-houses input[type="checkbox"]', function() {
    //     $(this).closest('tr').toggleClass('check');
    // });
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        markerViewRoutes: {
            request: null,  
        },
        markerIcons: {
            request: '{$markerPropertyIcon}'
        }
    });
    map.init();
    map.parseMarkers();
JS;

$this->registerJs($script);
