<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Request Title') ?>"
     data-content="<?= Yii::t('wizard', 'Set Request Title') ?>">
    <?= $form->field($propertyForm->meta, 'title')->textInput() ?>
</div>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Request Description') ?>"
     data-content="<?= Yii::t('wizard', 'Set Request Description') ?>">
    <?= $form->field($propertyForm->meta, 'description')->textarea([
        'placeholder' => Yii::t('wizard', 'Request Description'),
        'rows' => 8
    ]) ?>
</div>
