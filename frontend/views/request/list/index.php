<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 14.11.2018
 * Time: 17:01
 */

use frontend\widgets\PropertyFilter;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\common\assets\GoogleAsset::register($this);

/**
 * @var array $items
 * @var array $markers
 * @var array $tabs
 * @var View $this
 * @var PropertyFilter $itemFilter
 */

?>

    <div class="catalog-wrap no-padding">
        <div class="col-md-8 col-sm-12 catalog-left">
            <h1 class="text-left"><?= Yii::t('account', 'My Requests') ?></h1>
            <ul class="flex no-list object-status-switcher">
                <?php foreach($tabs as $tab) { ?>
                    <li class="">
                        <a href="<?= Url::to(['/request/list/index', 'status' => $tab['status']]) ?>" class="btn btn-default <?= $tab['active'] ? 'active' : ''?>">
                            <small><?= $tab['title'] ?>&nbsp;(<?= $tab['count'] ?>)</small>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php if (count($items['items']) > 0) { ?>
                <div class="table-houses no-top-margin">
                    <table id="list-houses">
                        <thead>
                        <tr>
                            <th>
                                <div class="chover">
                                    <input id="allCheck" class="radio-checkbox" name="allCheck" type="checkbox">
                                    <label for="allCheck"></label>
                                </div>
                            </th>
                            <th class="up">ID</th>
                            <th class="up"><?= Yii::t('catalog', 'Title') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Price') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($items['items'] as $item) { ?>
                            <tr>
                                <td>
                                    <div class="chover">
                                        <input id="house<?= $item['id'] ?>" class="radio-checkbox" name="id[]"
                                               value="<?= $item['id'] ?>"
                                               type="checkbox"
                                               data-role="report-item">
                                        <label for="house<?= $item['id'] ?>"></label>
                                    </div>
                                </td>
                                <td><?= $item['id'] ?></td>
                                <td><a data-role="marker-data"
                                       data-property="<?= $markers[$item['id']] ?>"
                                       href="#">
                                        <?= $item['title'] ?>
                                    </a>
                                </td>
                                <td><?= $item['price'] ?></td>
                                <td>
                                    <a href="<?= Url::to(['/request/manage/update', 'id' => $item['id']]) ?>"><i class="icon-pencil"></i></a>
                                    <?= Html::a('<i class="icon-delete-button"></i>', [
                                        '/request/manage/delete', 'id' => $item['id']
                                    ], ['data-confirm' => Yii::t('main', 'Do you really want to delete this item?')]) ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <?php if ($items['pagination']) { ?>
                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $items['pagination'],
                    ]); ?>
                <?php } ?>
            <?php } else { ?>
                <p><?= Yii::t('main', 'No results found') ?></p>
            <?php } ?>

        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?php $markerPropertyIcon = Url::to(['/images/marker.png'], true);
$script = <<<JS
    // /* List houses */
    // $(document.body).on('click','#list-houses th',function() {
    //     if($(this).hasClass('active')){  
    //         $(this).toggleClass('up');
    //         $(this).toggleClass('down');
    //     } else {
    //         $('#list-houses th').removeClass('active');
    //         $(this).addClass('active');
    //     }
    // });
    //
    // $(document).on('change', '#list-houses input[type="checkbox"]', function() {
    //     $(this).closest('tr').toggleClass('check');
    // });
    
    let map = new CatalogMap({
        markerViewRoutes: {
            request: null,  
        },
        markerIcons: {
            request: '{$markerPropertyIcon}'
        }
    });
    map.init();
    map.parseMarkers();
JS;

$this->registerJs($script);

