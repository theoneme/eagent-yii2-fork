<?php

use frontend\assets\CatalogAsset;
use frontend\assets\plugins\MoreLessAdvancedAsset;
use frontend\assets\WizardAsset;
use frontend\forms\StepZeroForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

CatalogAsset::register($this);
WizardAsset::register($this);
MoreLessAdvancedAsset::register($this);

/**
 * @var array $categories
 * @var string $type
 * @var StepZeroForm $model
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'List your property or request for Free') ?></h1>
        <div class="wizard-content">
            <div class="wizard-row">
                <div class="wizards-steps">
                    <?php $form = ActiveForm::begin([
                        'method' => 'get',
                        'action' => ['/property/manage/create'],
                        'fieldConfig' => [
                            'template' => "{input}{error}",
                        ],
                        'options' => [
                            'id' => 'step-zero-form',
                        ],
                    ]); ?>
                    <section id="step1">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Please choose type of your advertisement:') ?>
                        </div>
                        <div class="row step-content">
                            <div class="col-md-8">
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('wizard', 'Choose what you want to do') ?>"
                                     data-content="<?= Yii::t('wizard', 'You can choose one of the following options: sell, buy, rent or rent out property') ?>">
                                    <?= $form->field($model, 'type')->radioList($model->types, [
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                            //                                            $chk = $checked ? 'checked' : '';
                                            $output = "
                                                <div class='chover'>" .
                                                Html::radio($name, false, [
                                                    'id' => "type-$value",
                                                    'value' => $label['value'],
                                                    'class' => 'radio-checkbox property-type',
                                                    'data-action' => Url::to($label['action']),
                                                ]) .
                                                Html::label($label['label'], "type-$value") .
                                                "</div>";
                                            return $output;
                                        },
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="step2" data-lvl="1">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Property category') ?>
                        </div>
                        <div class="row step-content">
                            <div class="col-md-8">
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('wizard', 'Specify the property category') ?>"
                                     data-content="<?= Yii::t('wizard', 'Select the category of property from the options') ?>">
                                    <?= $this->render('@frontend/views/common/category-input', [
                                        'model' => $model,
                                        'items' => $model->categories,
                                        'form' => $form,
                                        'field' => 'parent_category_id',
                                        'index' => 1,
                                        'createForm' => false
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="step3" class="subtype-step" data-lvl="2">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Property subcategory') ?>
                        </div>
                        <div class="row step-content">
                            <div class="col-md-8">
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('wizard', 'Specify the property subcategory') ?>"
                                     data-content="<?= Yii::t('wizard', 'Select the subcategory of property from the options') ?>">
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="step4">
                        <div class="wizard-step-header">
                            <?= Yii::t('wizard', 'Language') ?>
                        </div>
                        <div class="row step-content">
                            <div class="col-md-8">
                                <div class="block-with-notes"
                                     data-toggle="popover" data-placement="right"
                                     data-original-title="<?= Yii::t('wizard', 'Choose the main language in which the original advertisement will be placed') ?>"
                                     data-content="<?= Yii::t('wizard', 'We will also auto-translate into several popular languages to draw attention to your announcement') ?>">
                                    <div class="type-adv">
                                        <?= $form->field($model, 'locale')->radioList($model->locales, [
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                $chk = $checked ? 'checked' : '';
                                                $output = '<div class="chover">' .
                                                    Html::radio($name, $chk, [
                                                        'id' => "locale-$index",
                                                        'value' => $value,
                                                        'class' => 'radio-checkbox'
                                                    ]) .
                                                    Html::label(Yii::t('main', $label), "locale-$index") .
                                                    '</div>';
                                                return $output;
                                            },
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="text-center">
                        <input id="contact-agent" type="submit" class="btn btn-big btn-blue-white"
                               value="<?= Yii::t('wizard', 'Continue') ?>">
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
<?php
$renderAttributesRoute = Url::toRoute('/property/property/attributes');
$renderCategoriesRoute = Url::toRoute('/ajax/render-category-input');
$canonical = Url::canonical();
$showLanguagesText = Yii::t('wizard', 'Show other languages');
$hideLanguagesText = Yii::t('wizard', 'Hide other languages');

$script = <<<JS
    let tt = 'right';
    if($('body').hasClass('rtl')){
        tt = 'left';
    }

    $('.block-with-notes').popover({
        trigger: 'hover',
        placement: tt,
        html: true
    });

    $('input.property-type').change(function() {
        $('#step-zero-form').attr('action', $(this).data('action'));
    });
    $('input.property-type:checked').trigger('change');

    $('.field-stepzeroform-locale').MoreLessAdvanced({
        withArrows: false,
        moreText: '{$showLanguagesText}',
        lessText: '{$hideLanguagesText}',
        containerSelector: '.field-stepzeroform-locale',
        itemSelector: '.chover',
    });
    
    $('.step-zero-category-input').on('change', function() {
        let lvl = $(this).data('lvl') + 1;
        let container = $('section[data-lvl="' + lvl + '"]');
        if (container.length) {
            let val = $(this).val();
            if (val) { 
                $.get('$renderCategoriesRoute', {
                    index: lvl,
                    parent: val
                }, function(result) {
                    if(result.success === true) {
                        container.find('.block-with-notes').html(result.html);
                        container.addClass('active');
                    }
                });   
            } else {
                container.removeClass('active');
            }
        }
        
        return false;
    });
JS;
$this->registerJs($script);

?>