<?php

use frontend\forms\StepZeroForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/**
 * @var integer $index
 * @var array $items
 * @var ActiveForm $form
 * @var string $field
 * @var StepZeroForm $model
 * @var boolean $createForm
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "{input}{error}",
        ],
        'options' => [
            'id' => 'step-zero-form',
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

<?//= $form->field($model, $field)->dropDownList($items, [
//    'class' => 'category-dropdown',
//    'data-lvl' => $index
//]) ?>
<?= $form->field($model, $field)->radioList($items, [
    'item' => function ($i, $label, $name, $checked, $value) use ($index) {
        $chk = $checked ? 'checked' : '';
        $output = "
            <div class='chover'>" .
                Html::radio($name, $chk, [
                    'id' => "$name-$i",
                    'value' => $value,
                    'class' => 'radio-checkbox step-zero-category-input',
                    'data-lvl' => $index
                ]) .
                Html::label($label, "$name-$i") .
            "</div>";
        return $output;
    },
]) ?>

<?php if ($createForm === true) { ?>
    <?php
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
        var attributes = $attributes;
        $.each(attributes, function() {
            $("#step-zero-form").yiiActiveForm("add", this);
        });
JS;
    $this->registerJs($script);
} ?>