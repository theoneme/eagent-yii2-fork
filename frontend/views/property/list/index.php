<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use frontend\widgets\PropertyFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\assets\plugins\FancyBoxAsset::register($this);

/**
 * @var array $properties
 * @var array $propertyMarkers
 * @var array $tabs
 * @var View $this
 * @var PropertyFilter $propertyFilter
 */

?>

    <div class="catalog-wrap no-padding">
        <div class="col-md-8 col-sm-12 catalog-left">
            <h1 class="text-left"><?= Yii::t('account', 'My Properties') ?></h1>
            <ul class="flex no-list object-status-switcher">
                <?php foreach ($tabs as $tab) { ?>
                    <li class="">
                        <a href="<?= Url::to(['/property/list/index', 'status' => $tab['status']]) ?>"
                           class="btn btn-default <?= $tab['active'] ? 'active' : '' ?>">
                            <small><?= $tab['title'] ?>&nbsp;(<?= $tab['count'] ?>)</small>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <?php if (count($properties['items']) > 0) { ?>
                <div class="table-houses no-top-margin">
                    <table id="list-houses text-left">
                        <thead>
                        <tr>
                            <th>
                                <div class="chover">
                                    <input id="allCheck" class="radio-checkbox" name="allCheck" type="checkbox">
                                    <label for="allCheck"></label>
                                </div>
                            </th>
                            <th class="up">ID</th>
                            <th class="up"><?= Yii::t('catalog', 'Title') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Photos') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Area') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Price') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Action') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($properties['items'] as $property) { ?>
                            <tr>
                                <td>
                                    <div class="chover">
                                        <input id="house<?= $property['id'] ?>" class="radio-checkbox" name="id[]"
                                               value="<?= $property['id'] ?>"
                                               type="checkbox"
                                               data-role="report-item">
                                        <label for="house<?= $property['id'] ?>"></label>
                                    </div>
                                </td>
                                <td><?= $property['id'] ?></td>
                                <td>
                                    <a data-role="marker-data"
                                       href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                                       data-property="<?= $propertyMarkers[$property['id']] ?>"
                                       data-action="load-modal-property">
                                        <?= $property['title'] ?>
                                    </a>
                                </td>
                                <td>
                                    <?php if(count($property['images']) > 0) { ?>
                                        <?php foreach ($property['images'] as $key => $image) { ?>
                                            <?php if ($key === 0) { ?>
                                                <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($property['images']), $image, [
                                                    'data-fancybox' => "gallery-{$property['id']}",
                                                    'data-role' => 'fancy-gallery-item'
                                                ]) ?>
                                            <?php } else { ?>
                                                <?= Html::a(null, $image, [
                                                    'data-fancybox' => "gallery-{$property['id']}",
                                                    'class' => 'hidden',
                                                    'data-role' => 'fancy-gallery-item'
                                                ]) ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <i class="icon-camera"></i>&nbsp;<?= count($property['images']) ?>
                                    <?php } ?>
                                </td>
                                <td><?= isset($property['attributes']['property_area']) ? "{$property['attributes']['property_area']} м²" : '-' ?></td>
                                <td><?= $property['price'] ?></td>
                                <td>
                                    <a href="<?= Url::to(['/property/manage/update', 'id' => $property['id']]) ?>">
                                        <i class="icon-pencil"></i>
                                    </a>
                                    <?= Html::a('<i class="icon-delete-button"></i>', [
                                        '/property/manage/delete', 'id' => $property['id']
                                    ], ['data-confirm' => Yii::t('main', 'Do you really want to delete this item?')]) ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <?php if ($properties['pagination']) { ?>
                    <?= \yii\widgets\LinkPager::widget([
                        'pagination' => $properties['pagination'],
                    ]); ?>
                <?php } ?>
            <?php } else { ?>
                <p><?= Yii::t('main', 'No results found') ?></p>
            <?php } ?>

        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?php $markerListUrl = Url::to(['/property/marker/account']);
$markerViewUrl = Url::to(['/property/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$script = <<<JS
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false
    });
    
    // /* List houses */
    // $(document.body).on('click','#list-houses th',function() {
    //     if($(this).hasClass('active')){  
    //         $(this).toggleClass('up');
    //         $(this).toggleClass('down');
    //     } else {
    //         $('#list-houses th').removeClass('active');
    //         $(this).addClass('active');
    //     }
    // });
    //
    // $(document).on('change', '#list-houses input[type="checkbox"]', function() {
    //     $(this).closest('tr').toggleClass('check');
    // });
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        markerViewRoutes: {
            property: '{$markerViewUrl}',  
        },
        markerIcons: {
            property: '{$markerPropertyIcon}'
        }
    });
    map.init();
    map.parseMarkers();
JS;

$this->registerJs($script);

