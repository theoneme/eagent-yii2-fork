<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use common\forms\ar\BuildingForm;
use common\forms\ar\PropertyForm;
use frontend\assets\CatalogAsset;
use frontend\assets\WizardAsset;
use frontend\controllers\property\ManageController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

CatalogAsset::register($this);
WizardAsset::register($this);

/**
 * @var PropertyForm $propertyForm
 * @var BuildingForm $buildingForm
 * @var array $route
 * @var integer $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'List your property') ?></h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($route),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div class='row'>
                <div class='col-md-4'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {hint}
                    {input}
                    {error}
                </div>
            </div>",
            ],
            'options' => [
                'id' => 'property-form',
                'data-role' => 'edit-modal-form',
                'data-pj-container' => 'property-pjax',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <?= Html::activeHiddenInput($propertyForm->category, 'category_id') ?>
            <?= Html::activeHiddenInput($propertyForm, 'type') ?>
            <?= Html::activeHiddenInput($propertyForm, 'locale') ?>
            <div class="row wizard-row">
                <div class="col-md-9 col-sm-12">
                    <div class="wizards-steps">
                        <?php foreach ($propertyForm->steps as $key => $step) { ?>
                            <section id="step<?= $key ?>">
                                <div class="wizard-step-header">
                                    <?= $step['title'] ?>
                                    <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($propertyForm->steps)]) ?></span>
                                </div>
                                <div class="row step-content">
                                    <div class="col-md-8">
                                        <?php foreach ($step['config'] as $stepConfig) { ?>
                                            <?php if ($stepConfig['type'] === 'view') { ?>
                                                <?= $this->render("steps/{$stepConfig['value']}", [
                                                    'form' => $form,
                                                    'propertyForm' => $propertyForm,
                                                    'buildingForm' => $buildingForm,
                                                    'step' => $step,
                                                    'createForm' => false,
                                                    'action' => $action
                                                ]) ?>
                                            <?php } ?>

                                            <?php if ($stepConfig['type'] === 'constructor') { ?>
                                                <?php foreach ($stepConfig['groups'] as $group) { ?>
                                                    <?php $attributes = $propertyForm->dynamicForm->getAttributesByGroup($group) ?>
                                                    <?= $this->render("partial/attributes-group", [
                                                        'form' => $form,
                                                        'attributes' => array_flip($attributes),
                                                        'dynamicForm' => $propertyForm->dynamicForm,
                                                        'action' => $action
                                                    ]) ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </section>
                        <?php } ?>
                        <div class="text-center">
                            <?= Html::a(Yii::t('wizard', 'Back'), Yii::$app->request->referrer, [
                                'class' => 'btn btn-big btn-white-blue'
                            ]) ?>
                            <?php if ($action === \frontend\controllers\property\ManageController::ACTION_CREATE) { ?>
                                <?= Html::submitInput(Yii::t('wizard', 'Publish Listing'), [
                                    'class' => 'btn btn-big btn-blue-white'
                                ]) ?>
                            <?php } else { ?>
                                <?= Html::submitInput(Yii::t('wizard', 'Save Changes'), [
                                    'class' => 'btn btn-big btn-blue-white'
                                ]) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <nav class="col-md-3 visible-lg visible-md progress-menu-nav menu-wizard-aside">
                    <div class="nav" data-spy="affix" data-offset-top="300">
                        <div class="progress-steps">
                            <div class="progress-percent text-center">
                                0
                            </div>
                            <div class="progress-info">
                                <?= Yii::t('wizard', 'Fill your announcement with maximum amount of details, so it will be seen more often') ?>
                            </div>
                        </div>
                        <ul class="no-list progress-menu">
                            <?php foreach ($propertyForm->steps as $key => $step) { ?>
                                <li>
                                    <a href="#step<?= $key ?>">
                                        <?= $step['title'] ?>
                                        <div>

                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php $renderAttributesRoute = Url::toRoute('/property/property/attributes');
$canonical = Url::canonical();
$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
$script = <<<JS
var isUpdate = {$isUpdate},
    calculatorFlag = false,
    validationPerformed = false,
    progressCalculator = new ProgressCalculator({
        formSelector: '#property-form'
    }),
    form = $('#property-form'),
    tt = 'right',
    body = $(body);

if(body.hasClass('rtl')){
    tt = 'left';
} 
if(isUpdate) {
    form.yiiActiveForm('validate', true);
    progressCalculator.updateProgress();
} else {
    validationPerformed = true
}

form.on('submit', function() { 
    calculatorFlag = true;
    let hasToUpload = false;
    $.each($(".file-upload-input"), function() {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            hasToUpload = true;
            return false;
        }
    });
    
    if(hasToUpload === false) {
        if(validationPerformed === true) { 
             return true;
        } else { 
            validationPerformed = true;
            return false;
        }
    } else {
        return false;
    }
}).on("afterValidateAttribute", function(event, attribute, messages) {
    if(calculatorFlag === false) {
        progressCalculator.updateProgress();
    }
}).on("afterValidate", function(event, messages, errorAttributes) {
    progressCalculator.updateProgress();
    calculatorFlag = false;
});

$("#categoryform-category_id, #propertyform-type").on("change", function(e) {
    let type = $('#propertyform-type').find('input:checked').val();
    let categoryId = $('#categoryform-category_id').find('input:checked').val();
    // let propertyId = $('#property-id').val();
    // loadAttributes(categoryId, type, propertyId);
    let params =  {
        category_id: categoryId,
        type: type,
    };
    let query = $.param(params);

    window.location.replace('{$canonical}' + '?' + query); 
});

$('.block-with-notes').popover({
    trigger: 'hover',
    placement: tt,
    html: true
});

body.scrollspy({
    target: '.progress-menu-nav',
    offset: 300
});

$(".progress-menu a").on('click', function(event) {
    if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800);
    }
});

JS;
$this->registerJs($script);

?>