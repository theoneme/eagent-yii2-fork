<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2018
 * Time: 16:31
 */

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?//= $form->field($dynamicForm, $attribute)->checkboxList($dynamicForm['config']['values'][$attribute]) ?>

<?= $form->field($dynamicForm, $attribute)->checkboxList($dynamicForm['config']['values'][$attribute], [
    'item' => function ($index, $label, $name, $checked, $value) use ($attribute) {
        $chk = $checked ? 'checked' : '';
        $output = "<div class='chover big-radio-btn'>
                <input name='{$name}' id='propertyform-{$attribute}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='checkbox'>
                <label for='propertyform-{$attribute}-{$index}'>{$label}</label>
            </div>";
        return $output;
    },
]) ?>