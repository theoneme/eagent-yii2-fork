<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2018
 * Time: 16:31
 */

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?= $form->field($dynamicForm, $attribute)->radioList([
    1 => Yii::t('main', 'Yes'),
    0 => Yii::t('main', 'No')
], [
    'item' => function ($index, $label, $name, $checked, $value) use ($attribute){
        $chk = $checked ? 'checked' : '';
        $output = "<div class='chover radio-btn'>
            <input name='{$name}' id='propertyform-{$attribute}-{$index}' class='radio-checkbox count-rooms' value='{$value}' {$chk} type='radio'>
            <label for='propertyform-{$attribute}-{$index}'>{$label}</label>
        </div>";
        return $output;
    },
    'class' => 'flex'
])?>