<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2018
 * Time: 16:31
 */

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?= $form->field($dynamicForm, $attribute)->textInput([
    'type' => 'number',
    'step' => '0.01',
    'placeholder' => $dynamicForm['config']['notes'][$attribute]['title'] ?? null
]) ?>
