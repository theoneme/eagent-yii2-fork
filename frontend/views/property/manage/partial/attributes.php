<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.10.2018
 * Time: 18:08
 */

use yii\helpers\Json;
use yii\widgets\ActiveForm;

$createForm = $createForm ?? true;

/**
 * @var \common\forms\ar\DynamicForm $dynamicForm
 * @var boolean $createForm
 * @var ActiveForm|null $form
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = new ActiveForm([
        'id' => 'property-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "<div class='row'>
                    <div class='col-md-4'>
                        {label}
                    </div>
                    <div class='col-md-8'>
                        {input}
                        {error}
                    </div>
                </div>",
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

<?php foreach ($dynamicForm->attributes as $key => $attribute) { ?>
    <div class="block-with-notes"
         data-toggle="popover" data-placement="right"
         data-original-title="<?= Yii::t('wizard', 'Hint title') ?>"
         data-content="<?= Yii::t('wizard', 'Hint') ?>">
        <?= $this->render($dynamicForm->config['types'][$key], [
            'form' => $form,
            'dynamicForm' => $dynamicForm,
            'attribute' => $key
        ]) ?>
    </div>
<?php } ?>

<?php if ($createForm === true) { ?>
    <?php $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    var attributes = $attributes;
    $.each(attributes, function() {
        $("#property-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);
} ?>