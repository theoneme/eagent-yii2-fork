<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

$noteAddress = Yii::t('wizard', '<p>Start typing the address and choose from the options.</p><p><b>For Example:</b> 230 Wellington Street Traverse City, Michigan 49686, USA </p>');
$noteZip = Yii::t('wizard', '<p>Please enter the zip code of the region where the property is located.</p><p><b>For Example:</b> 10028 </p>');

?>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Set address of your property') ?>"
     data-content="<?= $noteAddress ?>">
    <?= \common\widgets\GmapsActiveInputWidget::widget([
        'model' => $propertyForm->geo,
        'form' => $form,
        'inputOptions' => ['class' => 'form-control'],
        'withMap' => true
    ]) ?>
</div>

<div class="form-group">
    <div id="google-map" style="height: 300px;">

    </div>
</div>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'ZIP Code') ?>"
     data-content="<?= $noteZip ?>">
    <?= $form->field($propertyForm, 'zip')->textInput(['type' => 'number']) ?>
</div>
