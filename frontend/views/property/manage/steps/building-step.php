<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

use common\forms\ar\BuildingForm;
use common\forms\ar\PropertyForm;
use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $step
 * @var PropertyForm $propertyForm
 * @var BuildingForm $buildingForm
 * @var \yii\widgets\ActiveForm $form
 */

$noteName = Yii::t('wizard', '<p>Specify building or complex name.</p><p><b>For Example:</b> Green tree </p>');
$noteDescription = Yii::t('wizard', '<p>Enter detailed description of building.</p><p><b>For Example:</b> Park Grove will is located minutes from everything that makes Coconut Grove the best place to live in Miami: the blue waters of Biscayne Bay, and year-round sunlight and warmth.The nearby Coconut Grove Sailing Club, the Coral Reef Yacht Club, and the Dinner Key Marina all offer access to the water and to the area’s vibrant boating culture. </p>');

?>

<?php foreach ($buildingForm->meta as $locale => $meta) { ?>
    <?php if ($locale === $propertyForm->locale) { ?>
        <div class="block-with-notes"
             data-toggle="popover" data-placement="right"
             data-original-title="<?= Yii::t('wizard', 'Building Title') ?>"
             data-content="<?= $noteName ?>">
            <?= $form->field($meta, "[{$locale}]name")->textInput(['data-role' => 'building-name']) ?>
        </div>

        <div class="block-with-notes"
             data-toggle="popover" data-placement="right"
             data-original-title="<?= Yii::t('wizard', 'Building Description') ?>"
             data-content="<?= $noteDescription ?>">
            <?= $form->field($meta, "[{$locale}]description")->textarea([
                'placeholder' => Yii::t('wizard', 'Building Description'),
                'rows' => 8,
                'data-role' => 'building-description'
            ]) ?>
        </div>
    <?php } ?>
<?php } ?>

    <div class="form-group" data-role="building-image-block">
        <p>
            <?= Yii::t('wizard', 'You can upload up to {count} photos', ['count' => 20]) ?>
        </p>
        <div class="block-with-notes form-group"
             data-toggle="popover" data-placement="right"
             data-original-title="<?= Yii::t('wizard', 'Building Photos') ?>"
             data-content="<?= Yii::t('wizard', 'Upload Building Images') ?>">
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'building-file-upload-input',
                    'name' => 'uploaded_images[]',
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                        'overwriteInitial' => false,
                        'initialPreview' => [],
                        'initialPreviewConfig' => [],
                    ]
                ])
            ) ?>
        </div>

        <p class='<?= empty($buildingForm->attachment) ? 'hidden' : '' ?>'
           data-role="building-prev-uploaded-title">
            <?= Yii::t('wizard', 'Previously uploaded building photos') ?>
        </p>

        <div class="flex" data-role="wizard-photo-small-container">
            <?php foreach ($buildingForm->attachment as $attachment) { ?>
                <div class="wizard-photo-small-preview">
                    <?= Html::img($attachment['content'], []) ?>
                </div>
            <?php } ?>
        </div>

        <div data-role="building-attachment-container" class="building-attachment-container">
            <?php foreach ($buildingForm->attachment as $key => $attachment) { ?>
                <?= $form->field($attachment, "[]content", ['template' => '{input}'])->hiddenInput([
                    'value' => FileInputHelper::buildOriginImagePath($attachment['content'])
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>

<?php $buildingParamsRoute = Url::to(['/building/ajax/building']);
$script = <<<JS
var hasBuildingUploadError = false;

$("#building-file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
    let response = data.response;
    $(".building-attachment-container").append("<input name=\'BuildingAttachmentForm[][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
}).on("filedeleted", function(event, key) {
    $(".building-attachment-container input[data-key=\'" + key + "\']").remove();
}).on("filebatchuploadcomplete", function() {
    if (hasBuildingUploadError === false) {
        $(this).closest("form").submit();
    } else {
        hasBuildingUploadError = false;
    }
}).on("fileuploaderror", function(event, data, msg) {
    hasBuildingUploadError = true;
    // $('#' + data.id).find('.kv-file-remove').click();
});

$("#gmaps-input-address").on("change", function() { 
    let lat = $('#gmaps-input-lat').val(),
        lng = $('#gmaps-input-lng').val();

    if(lat && lng) {
        $.get('{$buildingParamsRoute}', {lat: lat, lng: lng}, function(result) {
            if(result.success === true) {
                let data = result.data;
                $.each(data.attributes, function(key, value) {
                    let input = $('#dynamicform-' + key);
                    if(input.hasClass('selectize-multi')) {
                        let value1 = value.split(',');
                        $.each(value1, function(k, v) {
                            input[0].selectize.addOption({value: v});
                        });
                        input[0].selectize.setValue(value1, false);
                    } else {
                        input.val(value);
                    }
                });
                
                $('[data-role=building-image-block]').addClass('hidden');
                $('[data-role=building-description]').val(data.description).closest('.block-with-notes').addClass('hidden');
                $('[data-role=building-name]').val(data.name).closest('.block-with-notes').addClass('hidden');
            } else {
                $('[data-role=building-description]').val('').closest('.block-with-notes').removeClass('hidden');
                $('[data-role=building-name]').val('').closest('.block-with-notes').removeClass('hidden');
                $('[data-role=building-image-block]').removeClass('hidden');
            }
        });
    }
});
JS;

$this->registerJs($script);
