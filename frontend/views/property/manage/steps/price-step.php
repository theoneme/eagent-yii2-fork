<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

$notePrice = Yii::t('wizard', '<p>Specify property price.</p><p><b>For Example:</b> 300 000 </p>');
$noteCurrency = Yii::t('wizard', '<p>Select a currency from the list.</p><p><b>For Example:</b> Dollar </p>');

?>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Property Price') ?>"
     data-content="<?= $notePrice ?>">
    <?= $form->field($propertyForm->price, 'price')->widget(\yii\widgets\MaskedInput::class, [
        'clientOptions' => [
            'alias' => 'decimal',
            'groupSeparator' => ' ',
            'autoGroup' => true,
            'removeMaskOnSubmit' => true,
            'rightAlign' => false
        ],
    ]) ?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Currency') ?>"
     data-content="<?= $noteCurrency ?>">
    <?= $form->field($propertyForm->price, 'currency_code')->dropDownList(
        \common\helpers\DataHelper::getCurrencies()
    ) ?>
</div>

