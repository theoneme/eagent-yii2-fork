<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

$note = Yii::t('wizard', '<p>Write a detailed description of your house.</p><p><b>For Example:</b> The mansion has undergone a complete restoration. Every element has been rebuilt from the foundation up through the roof. </p>');

?>

<?php foreach ($propertyForm->meta as $locale => $meta) { ?>
    <?php if ($locale === $propertyForm->locale) { ?>
        <div class="block-with-notes"
             data-toggle="popover" data-placement="right"
             data-original-title="<?= Yii::t('wizard', 'Property Description') ?>"
             data-content="<?= $note ?>">
            <?= $form->field($meta, "[{$locale}]description")->textarea([
                'placeholder' => Yii::t('wizard', 'Property description'),
                'rows' => 8
            ]) ?>
        </div>
    <?php } ?>
<?php } ?>
