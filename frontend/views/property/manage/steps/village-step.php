<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

use common\models\Translation;

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

$noteName = Yii::t('wizard', '<p>Enter the name of the residential complex. Not required.</p><p><b>For Example:</b> Redwood </p>');
$noteDescription = Yii::t('wizard', '<p>Write a detailed description of residential complex (Community).</p><p><b>For Example:</b> An inhabited flat community with good infrastructure. Nearby is a pine forest. Also there are school, kindergarten, a sports complex, shops. </p>');

?>

<?php foreach ($propertyForm->meta as $locale => $meta) { ?>
    <?php if ($locale === $propertyForm->locale) { ?>
        <div class="block-with-notes"
             data-toggle="popover" data-placement="right"
             data-original-title="<?= Yii::t('wizard', 'Residential complex name') ?>"
             data-content="<?= $noteName ?>">
            <?= $form->field($meta, "[{$locale}]" . Translation::KEY_ADDITIONAL_TITLE)->textInput([
                'placeholder' => Yii::t('wizard', 'Residential complex name')
            ])->label(Yii::t('wizard', 'Residential complex name')) ?>
        </div>

        <div class="block-with-notes"
             data-toggle="popover" data-placement="right"
             data-original-title="<?= Yii::t('wizard', 'Description of residential complex (Community)') ?>"
             data-content="<?= $noteDescription ?>">
            <?= $form->field($meta, "[{$locale}]" . Translation::KEY_ADDITIONAL_DESCRIPTION)->textarea([
                'placeholder' => Yii::t('wizard', 'Description of residential complex (Community)'),
                'rows' => 5
            ])->label(Yii::t('wizard', 'Description of residential complex (Community)')) ?>
        </div>
    <?php } ?>
<?php } ?>
