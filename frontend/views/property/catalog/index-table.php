<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use frontend\widgets\PropertyFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\frontend\assets\FilterAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\assets\plugins\FancyBoxAsset::register($this);

/**
 * @var array $properties
 * @var array $propertyMarkers
 * @var View $this
 * @var PropertyFilter $propertyFilter
 * @var string $lat
 * @var string $lng
 * @var array $seo
 * @var array $sortData
 * @var integer $zoom
 * @var string $box
 */

?>

    <div id="filter-component">
        <?= $propertyFilter->run() ?>
    </div>
    <div class="catalog-wrap">
        <div class="col-md-8 col-sm-12 catalog-left" id="properties-list">
            <?= $this->render('partial/list', [
                'sortData' => $sortData,
                'seo' => $seo,
                'properties' => $properties
            ]) ?>
        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <?= Html::textInput('google-map-search', null, ['class' => 'form-control google-map-search hidden', 'id' => 'google-map-search']); ?>
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?= \frontend\widgets\ReportWidget::widget() ?>
<?php $baseUrl = Url::to(['/property/catalog/search']);
$catalogViewUrl = Url::to(['/property/catalog/view']);
$markerListUrl = Url::to(['/property/marker/catalog']);
$markerViewUrl = Url::to(['/property/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$complexUrl = Url::to(['/property/catalog/index-ajax']);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);
$polygon = json_encode($polygon);

$script = <<<JS
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false
    });

    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };
    
    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    let params = filter.buildQuery();
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        zoom: {$zoom},
        box: '{$box}',
        mode: 'advanced',
        markerViewRoutes: {
            property: '{$markerViewUrl}',
        },
        markerIcons: {
            property: '{$markerPropertyIcon}'
        },
        polygon: {$polygon}
    });
    map.init();
    map.fetchMarkers('{$markerListUrl}', params);
    
    let fn = function(result) {
        if (result.domain === false) {
            map.setPolygon(result.polygon);
            map.setMarkers(result.markers);
            $('#properties-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $('#' + map.options.mapCanvasId).on('mapDragEnd mapZoomChanged', function() {
        filter.setBox($(this).data('box'));
        filter.setZoom($(this).data('zoom'));
    
        filter.process();
    });
    
    $(document).on('submit', filter.options.filterSelector, function() {
        let params = filter.buildQuery();
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
        let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '#properties-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if (result.success === true) {
                map.clearMarkerCache();
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({
                    result: true
                }, 'Page', result.url);
            }
        });
    
        return false;
    })
JS;

$this->registerJs($script);

