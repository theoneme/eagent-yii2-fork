<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var array $sortData
 * @var array $seo
 * @var array $properties
 * @var integer $gridSize
 */

?>

    <div class="loading-overlay hidden"></div>

    <div class="flex space-between">
        <div>
            <h1 class="text-left"><?= $seo['heading'] ?></h1>
        </div>
        <div class="text-right">
            <div class="form-group catalog-sort-wrap">
                <label for="catalog-sort"><?= $sortData['title'] ?></label>
                <select name="sort" id="catalog-sort">
                    <option value="">
                        --&nbsp;<?= Yii::t('catalog', 'Select {attribute}', ['attribute' => Yii::t('catalog', 'Sort Type')]) ?>
                    </option>
                    <?php foreach ($sortData['values'] as $valueKey => $value) { ?>
                        <option value="<?= $valueKey ?>" <?= $valueKey === $sortData['checked'] ? 'selected' : '' ?>><?= $value ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
<?php if (count($properties['items']) > 0) { ?>
    <div class="table-houses">
        <table id="list-houses">
            <thead>
            <tr>
                <th>
                    <div class="chover">
                        <input id="allCheck" class="radio-checkbox" name="allCheck" type="checkbox">
                        <label for="allCheck"></label>
                    </div>
                </th>
                <th class="up">ID</th>
                <th class="up"><?= Yii::t('catalog', 'Address') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Photos') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Area') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Price') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Rooms') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Bedrooms') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($properties['items'] as $property) { ?>
                <tr>
                    <td>
                        <div class="chover">
                            <input id="house<?= $property['id'] ?>" class="radio-checkbox" name="id[]"
                                   value="<?= $property['id'] ?>"
                                   type="checkbox"
                                   data-role="report-item">
                            <label for="house<?= $property['id'] ?>"></label>
                        </div>
                    </td>
                    <td><?= $property['id'] ?></td>
                    <td><a href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                           data-hover="show-marker"
                           data-id="<?= $property['id']?>"
                           data-type="property"
                           data-action="load-modal-property"><?= $property['address'] ?></a></td>
                    <td>
                        <?php if (count($property['images']) > 0) { ?>
                            <?php foreach ($property['images'] as $key => $image) { ?>
                                <?php if ($key === 0) { ?>
                                    <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($property['images']), $image, [
                                        'data-fancybox' => "gallery-{$property['id']}",
                                        'data-role' => 'fancy-gallery-item'
                                    ]) ?>
                                <?php } else { ?>
                                    <?= Html::a(null, $image, [
                                        'data-fancybox' => "gallery-{$property['id']}",
                                        'class' => 'hidden',
                                        'data-role' => 'fancy-gallery-item'
                                    ]) ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } else { ?>
                            <i class="icon-camera"></i>&nbsp;<?= count($property['images']) ?>
                        <?php } ?>
                    </td>
                    <td><?= isset($property['attributes']['property_area']) ? "{$property['attributes']['property_area']} м²" : '-' ?></td>
                    <td><?= $property['price'] ?></td>
                    <td><?= $property['attributes']['rooms'] ?? '-' ?></td>
                    <td><?= $property['attributes']['bedrooms'] ?? '-' ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <?php if ($properties['pagination']) {
        $properties['pagination']->route = '/property/catalog/index-ajax-short';

        echo LinkPager::widget([
            'pagination' => $properties['pagination'],
            'linkOptions' => ['data-action' => 'switch-page'],
        ]); ?>
    <?php } ?>
<?php } else { ?>
    <p><?= Yii::t('main', 'No results found') ?></p>
<?php } ?>