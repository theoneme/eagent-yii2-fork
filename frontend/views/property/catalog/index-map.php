<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:12
 */

use frontend\assets\CatalogAsset;
use frontend\assets\FilterAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use frontend\widgets\PropertyFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $properties
 * @var array $seo
 * @var array $sortData
 * @var View $this
 * @var PropertyFilter $propertyFilter
 * @var string $lat
 * @var string $lng
 * @var integer $zoom
 * @var string $box
 */

CatalogAsset::register($this);
AutoCompleteAsset::register($this);
FilterAsset::register($this);

?>
    <div id="filter-component">
        <?= $propertyFilter->run() ?>
    </div>

    <div class="catalog-wrap clearfix">
        <div class="col-md-6 col-sm-12 catalog-left" id="properties-list">
            <?= $this->render('partial/map', [
                'sortData' => $sortData,
                'seo' => $seo,
                'properties' => $properties
            ]) ?>
        </div>
        <div class="col-md-6 hidden-xs catalog-right map-catalog">
            <div class="map-over">
                <?= Html::textInput('google-map-search', null, ['class' => 'form-control google-map-search hidden', 'id' => 'google-map-search']); ?>
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?php $baseUrl = Url::to(['/property/catalog/search']);
$catalogViewUrl = Url::to(['/property/catalog/view']);
$markerListUrl = Url::to(['/property/marker/catalog']);
$markerViewUrl = Url::to(['/property/marker/view']);
$complexUrl = Url::to(['/property/catalog/index-ajax']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);
$polygon = json_encode($polygon);

$script = <<<JS
    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };
    
    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    let params = filter.buildQuery();
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        zoom: {$zoom},
        box: '{$box}',
        mode: 'advanced',
        markerViewRoutes: {
            property: '{$markerViewUrl}',
        },
        markerIcons: {
            property: '{$markerPropertyIcon}'
        },
        polygon: {$polygon}
    });
    map.init();
    map.fetchMarkers('{$markerListUrl}', params);
    
    let fn = function(result) {
        if (result.domain === false) {
            map.setMarkers(result.markers);
            map.setPolygon(result.polygon);
            $('#properties-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $('#' + map.options.mapCanvasId).on('mapDragEnd mapZoomChanged', function() {
        filter.setBox($(this).data('box'));
        filter.setZoom($(this).data('zoom'));
    
        filter.process();
    });
    
    $(document).on('submit', filter.options.filterSelector, function() {
        let params = filter.buildQuery();
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
        let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
        $('.loading-overlay').removeClass('hidden');
        $.get('{$complexUrl}', params, function(result) {
            if (result.success === true) {
                fn(result);
            }
        });
    
        return false;
    });
    
    $(document).on('click', '#properties-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if (result.success === true) {
                map.clearMarkerCache();
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({
                    result: true
                }, 'Page', result.url);
            }
        });
    
        return false;
    })
JS;
$this->registerJs($script);