<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 24.09.2018
 * Time: 15:27
 */


use yii\helpers\Html;

\frontend\assets\PropertyModalAsset::register($this);

/**
 * @var array $property
 * @var array $seo
 */

?>

    <div class="home-modal-top">
        <div></div>
        <div>
            <button type="button" class="close-modal-home" data-dismiss="modal" aria-hidden="true">
                <span>×</span> <span class="menu-item-text"><?= Yii::t('property', 'Close') ?></span>
            </button>
        </div>
    </div>
    <div class="modal-body">
        <div class="home-address text-left">
            <div>
                <ul>
                    <li><?= $property['address'] ?></li>
                </ul>
            </div>
        </div>
        <?php if (!empty($property['images'])) { ?>
            <div class="modal-slider" data-role="main-slider">
                <?php foreach ($property['images'] as $key => $image) { ?>
                    <?php if ($key === 0) { ?>
                        <?= '<div class="modal-slide ms-one">' ?>
                        <?= Html::a(Html::img($property['thumbnails'][$key], ['alt' => $seo['keywords'], 'title' => $seo['keywords']]), $image, ['data-fancybox' => 'gallery']) ?>
                        <?= '</div><div class="modal-slide">' ?>
                    <?php } else { ?>
                        <?= Html::a(Html::img($property['thumbnails'][$key], ['alt' => $seo['keywords'], 'title' => $seo['keywords']]), $image, ['data-fancybox' => 'gallery']) ?>
                        <?php if ($key > 1 && $key % 2 !== 0) { ?>
                            <?= '</div><div class="modal-slide">' ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <?= '</div>' ?>
            </div>
        <?php } ?>
        <div class="modal-info">
            <div class="modal-cols row">
                <div class="col-md-8 col-sm-12">
                        <div>
                            <div class="building-mark">Building</div>
                            <h1 class="text-left">
                                <?= $property['title'] ?>
                            </h1>
                            <div class="building-address">Sunny Isles Beach, FL 33160</div>
                            <div class="bedrooms-block">
                                <ul class="no-list history-switch no-margin">
                                    <li class="active">
                                        <a href="#rent" data-toggle="tab"> <div class="count-block text-center cb-green">20</div> For Rent </a>
                                    </li>
                                    <li>
                                        <a href="#sale" data-toggle="tab"> <div class="count-block text-center cb-green">30</div> For Sale </a>
                                    </li>
                                    <li>
                                        <a href="#off" data-toggle="tab"> <div class="count-block text-center cb-gray">40</div> Off Market </a>
                                    </li>
                                </ul>
                                <div class="tab-content tab-history">
                                    <div class="history-content tab-pane fade in active" id="rent">
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#rentBedroom1">
                                                <div class="bedroom-title"><strong>1 BEDROOM</strong><span>$3,250 - $5,250</span></div>
                                            </div>
                                            <div class="collapse" id="rentBedroom1">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$3,250</a>
                                                    </div>
                                                    <div class="flat-marker"> Available now</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$4,900</a>
                                                    </div>
                                                    <div class="flat-marker"> Available now</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$5,250</a>
                                                    </div>
                                                    <div class="flat-marker"> Available now</div>
                                                    <div class="flat-square text-right">1,200 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1006</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#rentBedroom2">
                                                <div class="bedroom-title"><strong>2 BEDROOM</strong><span>$4,000 - $7,400</span></div>
                                            </div>
                                            <div class="collapse" id="rentBedroom2">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$4,000</a>
                                                    </div>
                                                    <div class="flat-marker"> Available now</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$7,000</a>
                                                    </div>
                                                    <div class="flat-marker"> Available now</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#rentBedroom3">
                                                <div class="bedroom-title"><strong>3 BEDROOM</strong><span>$7,400 - $10,000</span></div>
                                            </div>
                                            <div class="collapse" id="rentBedroom3">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$7,400</a>
                                                    </div>
                                                    <div class="flat-marker"> Available now</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$10,000</a>
                                                    </div>
                                                    <div class="flat-marker"> Available now</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="history-content tab-pane fade in" id="sale">
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#saleBedroom1">
                                                <div class="bedroom-title"><strong>1 BEDROOM</strong><span>$3,250 - $5,250</span></div>
                                            </div>
                                            <div class="collapse" id="saleBedroom1">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$3,250</a>
                                                    </div>
                                                    <div class="flat-marker red-mark"> For Sale</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$4,900</a>
                                                    </div>
                                                    <div class="flat-marker red-mark"> For Sale</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$5,250</a>
                                                    </div>
                                                    <div class="flat-marker red-mark"> Pending</div>
                                                    <div class="flat-square text-right">1,200 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1006</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#saleBedroom2">
                                                <div class="bedroom-title"><strong>2 BEDROOM</strong><span>$4,000 - $7,400</span></div>
                                            </div>
                                            <div class="collapse" id="saleBedroom2">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$4,000</a>
                                                    </div>
                                                    <div class="flat-marker red-mark"> For Sale</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$7,000</a>
                                                    </div>
                                                    <div class="flat-marker red-mark"> For Sale</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#saleBedroom3">
                                                <div class="bedroom-title"><strong>3 BEDROOM</strong><span>$7,400 - $10,000</span></div>
                                            </div>
                                            <div class="collapse" id="saleBedroom3">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$7,400</a>
                                                    </div>
                                                    <div class="flat-marker red-mark"> For Sale</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$10,000</a>
                                                    </div>
                                                    <div class="flat-marker red-mark"> Pending</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="history-content tab-pane fade in" id="off">

                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#offBedroom1">
                                                <div class="bedroom-title"><strong>1 BEDROOM</strong><span>$3,250 - $5,250</span></div>
                                            </div>
                                            <div class="collapse" id="offBedroom1">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$3,250</a>
                                                    </div>
                                                    <div class="flat-marker gray-mark"> Off Market</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$4,900</a>
                                                    </div>
                                                    <div class="flat-marker gray-mark"> Off Market</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$5,250</a>
                                                    </div>
                                                    <div class="flat-marker yellow-mark"> Recently Sold</div>
                                                    <div class="flat-square text-right">1,200 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1006</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#offBedroom2">
                                                <div class="bedroom-title"><strong>2 BEDROOM</strong><span>$4,000 - $7,400</span></div>
                                            </div>
                                            <div class="collapse" id="offBedroom2">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$4,000</a>
                                                    </div>
                                                    <div class="flat-marker gray-mark"> Off Market</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$7,000</a>
                                                    </div>
                                                    <div class="flat-marker gray-mark"> Off Market</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bedroom-group">
                                            <div class="params-header" data-toggle="collapse" data-target="#offBedroom3">
                                                <div class="bedroom-title"><strong>3 BEDROOM</strong><span>$7,400 - $10,000</span></div>
                                            </div>
                                            <div class="collapse" id="offBedroom3">
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$7,400</a>
                                                    </div>
                                                    <div class="flat-marker gray-mark"> Off Market</div>
                                                    <div class="flat-square text-right">1,168 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">1706</div>
                                                </div>
                                                <div class="flat-item">
                                                    <div class="flat-price">
                                                        <a href="#">$10,000</a>
                                                    </div>
                                                    <div class="flat-marker yellow-mark"> Recently Sold</div>
                                                    <div class="flat-square text-right">1,149 sqft</div>
                                                    <div class="flat-bath text-right">1.5 ba</div>
                                                    <div class="flat-unit text-right">2016</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="facts-building">
                            <h4 class="up-title text-left">
                                <?= Yii::t('property', 'Facts') ?>
                            </h4>
                            <ul class="no-list fact-building">
                                <li>Pets: Contact manager</li>
                                <li>Parking: Attached Garage</li>
                                <li>Year Built: 2006</li>
                            </ul>
                        </div>
                        <div class="facts-building">
                            <h4 class="up-title text-left">
                                <?= Yii::t('property', 'Amenities') ?>
                            </h4>
                            <ul class="no-list fact-building">
                                <li>Cooling: Central</li>
                                <li>Heating Type: Forced air</li>
                                <li>Elevator: Yes</li>
                                <li>Hot Tub/Spa: Yes</li>
                                <li>Tennis Court: Yes</li>
                            </ul>
                        </div>
                            <div class="slide-down-list">

                                <section class="params-block">
                                    <div class="params-header" data-toggle="collapse" data-target="#school">
                                        <h2 class="text-left">Nearby Schools in Pompano Beach</h2>
                                    </div>
                                    <div class="collapse" id="school">
                                        <div class="params-content">
                                            <div class="schools-head">
                                                <div class="school-head-rate">
                                                    <h4>GreatSchools Rating</h4>
                                                    <div class="tooltip-ea" data-position="right" data-width="360">
                                                        <div class="tooltip-modal">
                                                            <a href="" class="tooltip-close">&times;</a>
                                                            <div class="tooltip-body">
                                                                <p>
                                                                    Historically, GreatSchools ratings have been based
                                                                    solely on a comparison of standardized test results
                                                                    for all schools in a given state. As of September
                                                                    2017, the GreatSchools ratings also incorporate
                                                                    additional information, when available, such as
                                                                    college readiness, academic progress, advanced
                                                                    courses, equity, discipline and attendance data.
                                                                    GreatSchools ratings are designed to be a starting
                                                                    point to help parents compare schools, and should
                                                                    not be the only factor used in selecting the right
                                                                    school for your family.
                                                                    <a href="#">
                                                                        Learn more.
                                                                    </a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="school-head-grades text-right">
                                                    <h4>Grades</h4>
                                                </div>
                                                <div class="school-head-distance text-right">
                                                    <h4>Distance</h4>
                                                </div>
                                            </div>
                                            <ul class="no-list school-list">
                                                <li>
                                                    <div class="school-rate">
                                                        <div class="school-rate-circle text-center">
                                                            3
                                                        </div>
                                                        <div class="school-rate-text text-center">
                                                            out of 10
                                                        </div>
                                                    </div>
                                                    <div class="school-info">
                                                        <div class="school-title">
                                                            <a href="#">Pompano Beach Elementary</a>
                                                        </div>
                                                        <div class="school-grades text-right">
                                                            PK-5
                                                        </div>
                                                        <div class="school-distance text-right">
                                                            0.8 mi
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="school-rate">
                                                        <div class="school-rate-circle text-center">
                                                            4
                                                        </div>
                                                        <div class="school-rate-text text-center">
                                                            out of 10
                                                        </div>
                                                    </div>
                                                    <div class="school-info">
                                                        <div class="school-title">
                                                            <a href="#">Pompano Beach Middle</a>
                                                        </div>
                                                        <div class="school-grades text-right">
                                                            PK-5
                                                        </div>
                                                        <div class="school-distance text-right">
                                                            1.5 mi
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="school-rate">
                                                        <div class="school-rate-circle text-center">
                                                            9
                                                        </div>
                                                        <div class="school-rate-text text-center">
                                                            out of 10
                                                        </div>
                                                    </div>
                                                    <div class="school-info">
                                                        <div class="school-title">
                                                            <a href="#">Pompano Beach High</a>
                                                        </div>
                                                        <div class="school-grades text-right">
                                                            9-12
                                                        </div>
                                                        <div class="school-distance text-right">
                                                            0.8 mi
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <div class="school-descr">
                                                <p class="text-right">
                                                    <a href="#">More schools in Pompano Beach</a>
                                                </p>
                                                <p>
                                                    <strong> About the ratings: </strong> Historically, GreatSchools
                                                    ratings have been based solely on a comparison of standardized test
                                                    results for all schools in a given state. As of September 2017, the
                                                    GreatSchools ratings also incorporate additional information, when
                                                    available, such as college readiness, academic progress, advanced
                                                    courses, equity, discipline and attendance data. GreatSchools
                                                    ratings are designed to be a starting point to help parents compare
                                                    schools, and should not be the only factor used in selecting the
                                                    right school for your family.
                                                </p>
                                                <p>
                                                    <strong> Disclaimer: </strong> School attendance zone boundaries are
                                                    provided by a third party and subject to change. Check with the
                                                    applicable school district prior to making a decision based on these
                                                    boundaries.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                            <div class="home-similar">
                                <h2 class="modal-h2">Similar Homes for Sale</h2>
                                <div class="similar-slider" data-role="similar-slider">
                                    <div class="similar-slide">
                                        <a class="home-item-inner" href="" data-toggle="modal" data-target="#home">
                                            <div class="home-img">
                                                <img src="https://s3-us-west-2.amazonaws.com/eroom-property-images/default/0007/32/4eeabd25fe2256b37d80e3a2aa040321df603af4.jpeg"
                                                     alt="home" title="home">
                                            </div>
                                            <div class="home-wish"></div>
                                            <div class="top-price text-center">
                                                <div class="top-main-price">$2,371,000</div>
                                                <div class="top-save-price">save $71,130</div>
                                            </div>
                                            <div class="home-info">
                                                <h2 class="home-title">2 bedrooms Condo for sale in Ritz Carlton
                                                    Residences APT 603, Sunny Isles Beach Florida 33160</h2>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">2 bedrooms</div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">Condo</div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">2018</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="similar-slide">
                                        <a class="home-item-inner" href="" data-toggle="modal" data-target="#home">
                                            <div class="home-img">
                                                <img src="https://s3-us-west-2.amazonaws.com/eroom-property-images/default/0007/32/4eeabd25fe2256b37d80e3a2aa040321df603af4.jpeg"
                                                     alt="home" title="home">
                                            </div>
                                            <div class="home-wish"></div>
                                            <div class="top-price text-center">
                                                <div class="top-main-price">$2,371,000</div>
                                                <div class="top-save-price">save $71,130</div>
                                            </div>
                                            <div class="home-info">
                                                <h2 class="home-title">2 bedrooms Condo for sale in Ritz Carlton
                                                    Residences APT 603, Sunny Isles Beach Florida 33160</h2>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">2 bedrooms</div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">Condo</div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">2018</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="similar-slide">
                                        <a class="home-item-inner" href="" data-toggle="modal" data-target="#home">
                                            <div class="home-img">
                                                <img src="https://s3-us-west-2.amazonaws.com/eroom-property-images/default/0007/32/4eeabd25fe2256b37d80e3a2aa040321df603af4.jpeg"
                                                     alt="home" title="home">
                                            </div>
                                            <div class="home-wish"></div>
                                            <div class="top-price text-center">
                                                <div class="top-main-price">$2,371,000</div>
                                                <div class="top-save-price">save $71,130</div>
                                            </div>
                                            <div class="home-info">
                                                <h2 class="home-title">2 bedrooms Condo for sale in Ritz Carlton
                                                    Residences APT 603, Sunny Isles Beach Florida 33160</h2>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">2 bedrooms</div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">Condo</div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 home-param">2018</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="btn btn-big btn-white-blue"> See all similar listings </a>
                                </div>
                            </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="contact-agent-block visible-md visible-lg">
                        <h2 class="up-title text-left">
                            <?= Yii::t('main', 'Contact Agent') ?>
                        </h2>
                        <div class="chover">
                            <input id="agent1" class="radio-checkbox" name="agent" value="agent1" checked="checked"
                                   type="radio">
                            <label for="agent1">
                            </label>

                                <div class="agent-block">
                                    <div class="agent-photo">
                                        <img src="../images/veles.jpg" alt="agent">
                                    </div>
                                    <div class="agent-middle">
                                        <div class="agent-name">
                                            <a href="#"><?= Yii::t('property', 'Anastasia') ?></a>
                                        </div>
                                        <div class="agent-rate">
                                            <a href="#" class="agent-rate-alias">
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                            </a><!--ссылка на отзывы-->
                                            (<a href="#">0</a>)<!--ещё ссылка на отзывы-->
                                        </div>
                                        <div class="agent-add-info">
                                            <a class="count-sales" href="#">
                                                <!--ссылка на профиль-->
                                                <span class="text-center">0</span>
                                                <?= Yii::t('property', 'Recent sales') ?>
                                            </a>
                                            <div>
                                                <?= Yii::t('property', 'VELES') ?>
                                            </div>
                                            <div>
                                                (000) 000-00-00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="agent-status text-center">
                                        <?= Yii::t('main', 'Premium agent') ?>
                                    </div>
                                </div>
                        </div>

                        <div class="chover">
                            <input id="agent2" class="radio-checkbox" name="agent" value="agent2" checked=""
                                   type="radio">
                            <label for="agent2">
                            </label>

                                <div class="agent-block">
                                    <div class="agent-photo">
                                        <img src="../images/veles.jpg" alt="agent">
                                    </div>
                                    <div class="agent-middle">
                                        <div class="agent-name">
                                            <a href="#"><?= Yii::t('property', 'Anastasia') ?></a>
                                        </div>
                                        <div class="agent-rate">
                                            <a href="#" class="agent-rate-alias">
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                            </a><!--ссылка на отзывы-->
                                            (<a href="#">0</a>)<!--ещё ссылка на отзывы-->
                                        </div>
                                        <div class="agent-add-info">
                                            <a class="count-sales" href="#">
                                                <!--ссылка на профиль-->
                                                <span class="text-center">0</span>
                                                <?= Yii::t('property', 'Recent sales') ?>
                                            </a>
                                            <div>
                                                <?= Yii::t('property', 'VELES') ?>
                                            </div>
                                            <div>
                                                (000) 000-00-00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="agent-status text-center">
                                        <?= Yii::t('main', 'Premium agent') ?>
                                    </div>
                                </div>
                        </div>

                        <div class="chover">
                            <input id="agent3" class="radio-checkbox" name="agent" value="agent3" checked=""
                                   type="radio">
                            <label for="agent3">
                            </label>

                                <div class="agent-block">
                                    <div class="agent-photo">
                                        <img src="../images/veles.jpg" alt="agent">
                                    </div>
                                    <div class="agent-middle">
                                        <div class="agent-name">
                                            <a href="#"><?= Yii::t('property', 'Anastasia') ?></a>
                                        </div>
                                        <div class="agent-rate">
                                            <a href="#" class="agent-rate-alias">
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                                <i class="icon-favorite"></i>
                                            </a><!--ссылка на отзывы-->
                                            (<a href="#">0</a>)<!--ещё ссылка на отзывы-->
                                        </div>
                                        <div class="agent-add-info">
                                            <a class="count-sales" href="#">
                                                <!--ссылка на профиль-->
                                                <span class="text-center">0</span>
                                                <?= Yii::t('property', 'Recent sales') ?>
                                            </a>
                                            <div>
                                                <?= Yii::t('property', 'VELES') ?>
                                            </div>
                                            <div>
                                                (000) 000-00-00
                                            </div>
                                        </div>
                                    </div>
                                    <div class="agent-status text-center">
                                        <?= Yii::t('main', 'Premium agent') ?>
                                    </div>
                                </div>
                        </div>
                        <form>
                            <div class="form-group">
                                <?= Html::textInput('name', null, ['placeholder' => Yii::t('main', 'Your Name')]) ?>
                            </div>
                            <div class="form-group">
                                <?= Html::input('tel', 'phone', null, ['placeholder' => Yii::t('main', 'Phone')]) ?>
                            </div>
                            <div class="form-group">
                                <?= Html::input('email', 'email', null, ['placeholder' => Yii::t('main', 'Email')]) ?>
                            </div>
                            <div class="form-group">
                                <?= Html::textarea('message', Yii::t('property', 'I am interested in {property}', ['property' => $property['title']])) ?>
                            </div>
                            <div class="animate-input text-center">
                                <input id="contact-agent-bottom" type="submit" class="btn btn-small btn-blue-white width100" value="<?= Yii::t('main', 'Contact Agent') ?>">
                                <label for="contact-agent-bottom" class="animate-button">
                                    <div class="btn-wrapper">
                                        <div class="btn-original"><?= Yii::t('main', 'Contact Agent') ?></div>
                                        <div class="btn-container">
                                            <div class="left-circle"></div>
                                            <div class="right-circle"></div>
                                            <div class="mask"></div>
                                        </div>
                                    </div>
                                </label>
                            </div>
                        </form>
                        <div class="ca-temrs">
                            PH some text <a href="#"><?= Yii::t('main', 'Terms of use') ?></a>.
                        </div>
                        <!--                        <a href="#">Learn how to appear as the agent above</a>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-home-footer">
            <div class="nearby-cols row">
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Nearby Cities') ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li><a href="#"><?= Yii::t('property', 'Moscow') ?></a></li>
                        <li><a href="#"><?= Yii::t('property', 'St. Peterburg') ?></a></li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
                <!--                <div class="col-md-4">-->
                <!--                    <div class="nearby-title">-->
                <!--                        --><? //= Yii::t('property', 'Nearby Neighborhoods') ?>
                <!--                    </div>-->
                <!--                    <ul class="no-list ul-nearby">-->
                <!--                        <li><a href="#">Avalon Harbor</a></li>-->
                <!--                        <li><a href="#">Beach</a></li>-->
                <!--                        <li><a href="#">Collier City</a></li>-->
                <!--                        <li><a href="#">Cresthaven</a></li>-->
                <!--                        <li><a href="#">Garden Isles</a></li>-->
                <!--                    </ul>-->
                <!--                    --><? //= Html::a(Yii::t('main', 'More')) ?>
                <!--                </div>-->
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Nearby Zip Codes') ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li><a href="#">33060</a></li>
                        <li><a href="#">33062</a></li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Other topics in {city}', ['city' => 'Екатеринбурге']) ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li>
                            <a href="#"><?= Yii::t('property', 'Properties for sale in {index}', ['index' => 33062]) ?></a>
                        </li>
                        <li><a href="#"><?= Yii::t('property', 'Houses for sale in {index}', ['index' => 33062]) ?></a>
                        </li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
            </div>
            <div class="home-footer-info">
                <p>
                    SEO TEXT TEMPLATE
                </p>
                <p>
                    SEO TEXT TEMPLATE
                </p>
            </div>
        </div>
    </div>
