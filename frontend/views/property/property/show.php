<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 20:22
 */

use common\components\CurrencyHelper;
use common\models\Property;
use frontend\assets\plugins\MortgageCalculatorAsset;
use frontend\assets\PropertyModalAsset;
use frontend\forms\contact\ContactAgentForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

PropertyModalAsset::register($this);
MortgageCalculatorAsset::register($this);

/**
 * @var array $property
 * @var array $building
 * @var array $similarProperties
 * @var string $similarUrl
 * @var array $cheaperProperties
 * @var array $moreExpensiveProperties
 * @var array $biggerProperties
 * @var array $seo
 * @var array $realtors
 * @var View $this
 * @var ContactAgentForm $contactAgentForm
 */

?>

    <div class="home-modal-top">
        <ul class="blue-menu no-list">
            <li>
                <a class="blue-blue-btn scroll-alias" href="#bottom-contact-agent-form">
                    <?= Yii::t('main', 'Contact Agent') ?>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-heart"></i>
                    <span class="menu-item-text"><?= Yii::t('property', 'Save') ?></span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-envelope"></i>
                    <span class="menu-item-text"><?= Yii::t('property', 'Share') ?></span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-access-denied"></i>
                    <span class="menu-item-text"><?= Yii::t('property', 'Hide') ?></span>
                </a>
            </li>
            <li>
                <a href="#" id="blueMenuMore">
                    <span class="menu-item-text"><?= Yii::t('main', 'More') ?></span>
                    <i class="icon-caret-down"></i>
                </a>
                <div class="blue-sub-menu">
                    <ul class="no-list">
                        <li>
                            <a href="#">Print</a>
                        </li>
                        <li class="underline">
                            <a href="#">Get new listing in email</a>
                        </li>
                        <li>
                            <a href="#">Edit listing, photo and price</a>
                        </li>
                        <li>
                            <a href="#">Claim this listing</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <div>
            <button type="button" class="close-modal-home" data-dismiss="modal" aria-hidden="true">
                <span>×</span> <span class="menu-item-text"><?= Yii::t('property', 'Close') ?></span>
            </button>
        </div>
    </div>
    <div class="modal-body">
        <div class="flex">
            <div class="home-address text-left">
                <div>
                    <ul>
                        <li>ID: <?= $property['id'] ?></li>
                    </ul>
                </div>
            </div>
            <div class="home-address flex-grow text-right">
                <div>
                    <ul>
                        <li><?= $property['address'] ?></li>
                    </ul>
                </div>
            </div>
        </div>

        <?php if (!empty($property['images'])) { ?>
            <div class="modal-slider" data-role="main-slider">
                <div class="modal-slide ms-one">
                    <?= Html::a(Html::img(array_shift($property['thumbnails']), [
                        'alt' => $seo['title'],
                        'title' => $seo['heading']
                    ]), array_shift($property['images']), [
                        'data-fancybox' => 'gallery'
                    ]) ?>
                </div>
                <div class="modal-slide">
                    <?php foreach ($property['images'] as $key => $image) { ?>
                        <?php if ($key % 2 === 0 && $key > 0) { ?>
                            <?= '</div><div class="modal-slide">' ?>
                        <?php } ?>
                        <?= Html::a(Html::img($property['thumbnails'][$key], [
                            'alt' => $seo['title'],
                            'title' => $seo['heading']
                        ]), $image, [
                            'data-fancybox' => 'gallery'
                        ]) ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <div class="modal-info">
            <div class="modal-cols row">
                <div class="col-md-8 col-sm-12">
                    <div class="row">
                        <div class="modal-col-first col-md-8">
                            <h1 class="text-left" id="fancy-title">
                                <?= $seo['heading'] ?>
                            </h1>
                            <h3>
                                <?php if (array_key_exists('bedrooms', $property['attributes'])) { ?>
                                    <span class="like-ul">
                                    <?= Yii::t('property', '{bedrooms, plural, one{# bedroom} other{# bedrooms}}', [
                                        'bedrooms' => $property['attributes']['bedrooms']['value']
                                    ]) ?>
                                    </span>
                                <?php } ?>
                                <?php if (array_key_exists('rooms', $property['attributes'])) { ?>
                                    <span class="like-ul">
                                    <?= Yii::t('property', '{rooms, plural, one{# room} other{# rooms}}', [
                                        'rooms' => $property['attributes']['rooms']['value']
                                    ]) ?>
                                    </span>
                                <?php } ?>
                                <?php if (array_key_exists('bathrooms', $property['attributes'])) { ?>
                                    <span class="like-ul">
                                    <?= Yii::t('property', '{bathrooms, plural, one{# bathroom} other{# bathrooms}}', [
                                        'bathrooms' => $property['attributes']['bathrooms']['value']
                                    ]) ?>
                                    </span>
                                <?php } ?>
                                <?php if (array_key_exists('property_area', $property['attributes'])) { ?>
                                    <span class="like-ul">
                                    <?= Yii::t('property', '{area} m²', ['area' => $property['attributes']['property_area']['value']]) ?>
                                    </span>
                                <?php } ?>
                            </h3>
                            <?php if (!empty($building)) {
                                echo Html::a(
                                    Yii::t('property', 'Located in the building{title} at the address: {address}', [
                                        'address' => $building['address'],
                                        'title' => !empty($building['translations']['name']) ? Yii::t('seo', ' «{title}»', ['title' => $building['translations']['name']]) : '',
                                    ]) . '.&nbsp;<span>' . Yii::t('property', 'View other properties in this building') . '</span>',
                                    ['/building/building/show', 'slug' => $building['slug']],
                                    ['class' => 'property-building-link', 'data-action' => 'load-modal-property']
                                );
                            } ?>
                            <div class="modal-home-descr">
                                <div class="hide-content hide-text">
                                    <p>
                                        <?= $property['description'] ?>
                                    </p>
                                </div>
                                <a class="more-less" data-height="200">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                            <?php if (isset($property['translations']['advantage'])) { ?>
                                <div class="advantage">
                                    <h3 class="up-title text-left"><?= Yii::t('property', 'What i personally like in this property') ?></h3>
                                    <div><?= $property['translations']['advantage'] ?></div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="modal-col-second col-md-4">
                            <div class="mcs-price">
                                <?= $property['price'] ?>
                            </div>
                            <div class="market-date">
                                <div>
                                    <?= Yii::t('property', 'Publish date: <strong>{date}</strong>', [
                                        'date' => Yii::$app->formatter->asDatetime($property['created_at'])
                                    ]) ?>
                                </div>
                            </div>
                            <?php if ($property['type'] === Property::TYPE_SALE) { ?>
                                <div class="mortgage">
                                    <h4 class="up-title text-left">
                                        <?= Yii::t('property', 'Mortgage') ?></h4>
                                    <div class="mortgage-price" id="compact-mortgages">
                                        <div class="flex">
                                            <span data-role="mortgage-payment"></span>&nbsp;/&nbsp;
                                            <span><?= Yii::t('property', 'month') ?></span>
                                        </div>
                                        <div class="calculator">
                                            <a class="calculator-btn" href="#">
                                                <i class="icon-calculator"></i>
                                                <i class="icon-caret-down"></i>
                                            </a>
                                            <div class="calculator-modal">
                                                <h4 class="up-title text-center"><?= Yii::t('property', 'Monthly Payment') ?></h4>
                                                <div class="calc-sum text-center">
                                                    <span data-role="mortgage-payment"></span> <?= Yii::t('property', 'per month') ?>
                                                </div>
                                                <div class="calc-warning text-center">
                                                    <?= Yii::t('property', 'This is mortgage monthly payment with provided parameters') ?>
                                                </div>
                                                <div class="calc-preq text-center">
                                                    <p class="text-center">
                                                        <?= Yii::t('property', 'If you want to buy with property using mortgage, begin with sending request with button below') ?>
                                                    </p>
                                                    <?= Html::a(Yii::t('property', 'Send Request'), [
                                                        '/property/ajax/request-mortgage', 'entity_id' => $property['id']
                                                    ], [
                                                        'class' => 'btn btn-small btn-blue-white',
                                                        'data-action' => 'request-mortgage',
                                                        'data-target' => '#dynamic-modal',
                                                        'data-toggle' => 'modal'
                                                    ]) ?>
                                                </div>
                                                <div class="calculator-padding">
                                                    <form class="calculator-form">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <label for="compact-mortgage-price"><?= Yii::t('property', 'Property Price') ?></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><?= Yii::$app->params['app_currency'] ?></span>
                                                                        <?= Html::input('number', 'mortgage-property-price', $property['raw_price'], [
                                                                            'data-role' => 'mortgage-price',
                                                                            'id' => 'compact-mortgage-price',
                                                                            'class' => 'form-control'
                                                                        ]) ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label for="compact-mortgage-down-payment"><?= Yii::t('property', 'Down Payment') ?></label>
                                                                    <div class="clearfix">
                                                                        <div class="col-md-8 no-padding">
                                                                            <div class="input-group">
                                                                                <?= Html::input('number', 'mortgage-down-payment', (int)($property['raw_price'] / 2), [
                                                                                    'data-role' => 'mortgage-down-payment',
                                                                                    'id' => 'compact-mortgage-down-payment',
                                                                                    'class' => 'form-control'
                                                                                ]) ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4 no-padding">
                                                                            <div class="input-group">
                                                                                <?= Html::input('number', 'mortgage-down-payment-percent', 50, [
                                                                                    'data-role' => 'mortgage-down-payment-percent',
                                                                                    'id' => 'compact-mortgage-down-payment-percent',
                                                                                    'class' => 'form-control'
                                                                                ]) ?>
                                                                                <span class="input-group-addon">%</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <label for="compact-mortgage-loan-term"><?= Yii::t('property', 'Loan Term') ?></label>
                                                                    <?= Html::dropDownList('mortgage-loan-term', null, [
                                                                        5 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 5]),
                                                                        10 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 10]),
                                                                        15 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 15]),
                                                                        20 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 20]),
                                                                        30 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 30]),
                                                                    ], ['data-role' => 'mortgage-loan-term', 'id' => 'compact-mortgage-loan-term']) ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label for="compact-mortgage-interest-rate"><?= Yii::t('property', 'Interest Rate') ?></label>
                                                                    <div class="input-group">
                                                                        <?= Html::input('number', 'mortgage-interest-rate', 6, [
                                                                            'id' => 'compact-mortgage-interest-rate',
                                                                            'data-role' => 'mortgage-interest-rate',
                                                                            'class' => 'form-control'
                                                                        ]) ?>
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <div class="chover">
                                                                        <input id="compact-mortgage-property-insurance"
                                                                               data-role="mortgage-property-insurance"
                                                                               class="radio-checkbox"
                                                                               name="mortgage-property-insurance"
                                                                               value="1"
                                                                               type="checkbox">
                                                                        <label for="compact-mortgage-property-insurance">
                                                                            <?= Yii::t('property', 'Include Insurance') ?>
                                                                        </label>
                                                                        <div class="tooltip-ea"
                                                                             data-position="right"
                                                                             data-width="360">
                                                                            <div class="tooltip-modal">
                                                                                <a href="#"
                                                                                   class="tooltip-close">&times;</a>
                                                                                <div class="tooltip-body">
                                                                                    <p>
                                                                                        <strong><?= Yii::t('property', 'Mortgage Insurance') ?></strong>
                                                                                    </p>
                                                                                    <p>
                                                                                        <?= Yii::t('property', '-- Mortgage Insurance description --') ?>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?= Html::a('<div class="pre-qualified-icon"><i class="icon-dollar-bill"></i></div>&nbsp;'
                                    . Yii::t('property', 'Send Request'), [
                                    '/property/ajax/request-mortgage', 'entity_id' => $property['id']
                                ], [
                                    'class' => 'btn btn-small btn-white-blue',
                                    'data-action' => 'request-mortgage',
                                    'data-target' => '#dynamic-modal',
                                    'data-toggle' => 'modal'
                                ]) ?>
                            <?php } ?>
                        </div>

                        <div class="facts col-md-12">
                            <div class="facts-title">
                                <?= Yii::t('property', 'Main Parameters') ?>
                            </div>

                            <?php if (array_key_exists('property_type', $property['attributes'])) { ?>
                                <div class="fact-item col-md-4">
                                    <div class="fi-icon">
                                        <i class="icon-skyline"></i>
                                    </div>
                                    <div class="fact-info">
                                        <div class="fact-title">
                                            <?= Yii::t('property', 'Form of Ownership') ?>
                                        </div>
                                        <div class="fact-text">
                                            <?= $property['attributes']['property_type']['value'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (array_key_exists('year_built', $property['attributes'])) { ?>
                                <div class="fact-item col-md-4">
                                    <div class="fi-icon">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="fact-info">
                                        <div class="fact-title">
                                            <?= Yii::t('property', 'Year Built') ?>
                                        </div>
                                        <div class="fact-text">
                                            <?= $property['attributes']['year_built']['value'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (array_key_exists('heating', $property['attributes'])) { ?>
                                <div class="fact-item col-md-4">
                                    <div class="fi-icon">
                                        <i class="icon-thermometer"></i>
                                    </div>
                                    <div class="fact-info">
                                        <div class="fact-title">
                                            <?= Yii::t('property', 'Heating') ?>
                                        </div>
                                        <div class="fact-text">
                                            <?= $property['attributes']['heating']['value'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (array_key_exists('cooling', $property['attributes'])) { ?>
                                <div class="fact-item col-md-4">
                                    <div class="fi-icon">
                                        <i class="icon-snowflake"></i>
                                    </div>
                                    <div class="fact-info">
                                        <div class="fact-title">
                                            <?= Yii::t('property', 'Cooling') ?>
                                        </div>
                                        <div class="fact-text">
                                            <?= $property['attributes']['cooling']['value'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (array_key_exists('parking', $property['attributes'])) { ?>
                                <div class="fact-item col-md-4">
                                    <div class="fi-icon">
                                        <i class="icon-parked-car"></i>
                                    </div>
                                    <div class="fact-info">
                                        <div class="fact-title">
                                            <?= Yii::t('property', 'Parking') ?>
                                        </div>
                                        <div class="fact-text">
                                            <?= Yii::t('property', '{parking, plural, one{# space} other{# spaces}}', [
                                                'parking' => $property['attributes']['parking']['value']
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (array_key_exists('property_area', $property['attributes'])) { ?>
                                <div class="fact-item col-md-4">
                                    <div class="fi-icon">
                                        <i class="icon-dimensions"></i>
                                    </div>
                                    <div class="fact-info">
                                        <div class="fact-title">
                                            <?= Yii::t('property', 'Property Area') ?>
                                        </div>
                                        <div class="fact-text">
                                            <?= $property['attributes']['property_area']['value'] ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="fact-item col-md-4">
                                    <div class="fi-icon">
                                        <i class="icon-blueprint"></i>
                                    </div>
                                    <div class="fact-info">
                                        <div class="fact-title">
                                            <?= Yii::t('property', 'Price / m²') ?>
                                        </div>
                                        <div class="fact-text">
                                            <?= CurrencyHelper::convertAndFormat($property['currency_code'], Yii::$app->params['app_currency'], $property['source_price'] / (float)$property['attributes']['property_area']['value']) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="fact-item col-md-4">
                                <div class="fi-icon">
                                    <i class="icon-hours"></i>
                                </div>
                                <div class="fact-info">
                                    <div class="fact-title">
                                        <?= Yii::t('property', 'Days on {site}', ['site' => Yii::$app->name]) ?>
                                    </div>
                                    <div class="fact-text">
                                        <?= Yii::$app->formatter->asRelativeTime($property['created_at']) ?>
                                    </div>
                                </div>
                            </div>

                            <div class="fact-item col-md-4">
                                <div class="fi-icon">
                                    <i class="icon-prices-of-houses"></i>
                                </div>
                                <div class="fact-info">
                                    <div class="fact-title">
                                        <?= Yii::t('property', 'Price') ?>
                                    </div>
                                    <div class="fact-text">
                                        <?= $property['price'] ?>
                                    </div>
                                </div>
                            </div>

                            <div class="fact-item col-md-4">
                                <div class="fi-icon">
                                    <i class="icon-home"></i>
                                </div>
                                <div class="fact-info">
                                    <div class="fact-title">
                                        <?= Yii::t('property', 'Saves') ?>
                                    </div>
                                    <div class="fact-text">
                                        0
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-info-block col-md-12 hide-content hide-text">
                            <div>
                                <h4 class="up-title text-left">
                                    <?= Yii::t('property', 'Property Parameters') ?>
                                </h4>
                                <?php if (!empty($property['attributes'])) { ?>
                                    <div class="modal-info-block-content">
                                        <?php foreach ($property['attributes'] as $attribute) { ?>
                                            <div class="info-block-item">
                                                <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } else { ?>
                                    <?= Yii::t('property', 'No attributes specified') ?>
                                <?php } ?>
                            </div>
                            <?php if (!empty($building) && !empty($building['attributes'])) { ?>
                                <p>&nbsp;</p>
                                <div>
                                    <h4 class="up-title text-left">
                                        <?= Yii::t('building', 'Building Parameters') ?>
                                    </h4>
                                    <div class="modal-info-block-content">
                                        <?php foreach ($building['attributes'] as $attribute) { ?>
                                            <div class="info-block-item">
                                                <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <a class="col-md-6 more-less" data-height="300">
                            <?= Yii::t('main', 'More') ?>
                            <i class="icon-down"></i>
                        </a>
                        <div class="col-md-12">
                            <div class="slide-down-list">
                                <section class="params-block">
                                    <div class="params-header" data-toggle="collapse" data-target="#tax-history">
                                        <h2 class="text-left"><?= Yii::t('property', 'Price History') ?></h2>
                                    </div>
                                    <div class="collapse" id="tax-history">
                                        <div class="params-content">
                                            <ul class="no-list history-switch">
                                                <li class="active">
                                                    <a href="#priceH"
                                                       data-toggle="tab"> <?= Yii::t('property', 'Price History') ?> </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content tab-history">
                                                <div class="history-content tab-pane fade in active" id="priceH">
                                                    <table class="table-history text-left">
                                                        <thead>
                                                        <tr>
                                                            <th><?= Yii::t('property', 'Date') ?></th>
                                                            <th><?= Yii::t('property', 'Event') ?></th>
                                                            <th><?= Yii::t('property', 'Price') ?></th>
                                                            <th></th>
                                                            <th><?= Yii::$app->params['app_currency'] ?>/m²</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><?= Yii::$app->formatter->asDatetime($property['created_at']) ?></td>
                                                            <td><?= Yii::t('property', 'Property listed') ?></td>
                                                            <td><?= $property['price'] ?></td>
                                                            <td><span class="up">+0%</span></td>
                                                            <td><?= isset($property['attributes']['property_area'])
                                                                    ? (int)(CurrencyHelper::convertAndFormat($property['currency_code'], Yii::$app->params['app_currency'], $property['raw_price'] / (int)$property['attributes']['property_area']))
                                                                    : '-' ?>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <?php if ($property['type'] === Property::TYPE_SALE) { ?>
                                    <section class="params-block">
                                        <div class="params-header" data-toggle="collapse" data-target="#mortgages">
                                            <h2 class="text-left"><?= Yii::t('property', 'Mortgage') ?></h2>
                                        </div>
                                        <div class="collapse" id="mortgages">
                                            <div class="params-content">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <form class="calculator-form">
                                                            <div class="form-group">
                                                                <label for="mortgage-price"><?= Yii::t('property', 'Property Price') ?></label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><?= Yii::$app->params['app_currency'] ?></span>
                                                                    <?= Html::input('number', 'mortgage-property-price', $property['raw_price'], [
                                                                        'data-role' => 'mortgage-price',
                                                                        'id' => 'mortgage-price',
                                                                        'class' => 'form-control'
                                                                    ]) ?>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="mortgage-down-payment"><?= Yii::t('property', 'Down Payment') ?></label>
                                                                <div class="clearfix">
                                                                    <div class="col-md-8 no-padding">
                                                                        <div class="input-group">
                                                                            <?= Html::input('number', 'mortgage-down-payment', (int)($property['raw_price'] / 2), [
                                                                                'data-role' => 'mortgage-down-payment',
                                                                                'id' => 'mortgage-down-payment',
                                                                                'class' => 'form-control'
                                                                            ]) ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 no-padding">
                                                                        <div class="input-group">
                                                                            <?= Html::input('number', 'mortgage-down-payment-percent', 50, [
                                                                                'data-role' => 'mortgage-down-payment-percent',
                                                                                'id' => 'mortgage-down-payment-percent',
                                                                                'class' => 'form-control'
                                                                            ]) ?>
                                                                            <span class="input-group-addon">%</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="mortgage-loan-term"><?= Yii::t('property', 'Loan Term') ?></label>
                                                                <?= Html::dropDownList('mortgage-loan-term', null, [
                                                                    5 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 5]),
                                                                    10 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 10]),
                                                                    15 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 15]),
                                                                    20 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 20]),
                                                                    30 => Yii::t('property', '{years, plural, one{# year} other{# years}}', ['years' => 30]),
                                                                ], ['data-role' => 'mortgage-loan-term', 'id' => 'mortgage-loan-term']) ?>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="mortgage-interest-rate"
                                                                       class="flex space-between">
                                                                    <?= Yii::t('property', 'Interest Rate') ?>
                                                                </label>
                                                                <div class="input-group">
                                                                    <?= Html::input('number', 'mortgage-interest-rate', 6, [
                                                                        'id' => 'mortgage-interest-rate',
                                                                        'data-role' => 'mortgage-interest-rate',
                                                                        'class' => 'form-control'
                                                                    ]) ?>
                                                                    <span class="input-group-addon">%</span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="chover">
                                                                    <input id="mortgage-property-insurance"
                                                                           data-role="mortgage-property-insurance"
                                                                           class="radio-checkbox"
                                                                           name="mortgage-property-insurance" value="1"
                                                                           type="checkbox">
                                                                    <label for="mortgage-property-insurance">
                                                                        <?= Yii::t('property', 'Include Insurance') ?>
                                                                    </label>

                                                                    <div class="tooltip-ea" data-position="right"
                                                                         data-width="360">
                                                                        <div class="tooltip-modal">
                                                                            <a href="#"
                                                                               class="tooltip-close">&times;</a>
                                                                            <div class="tooltip-body">
                                                                                <p>
                                                                                    <strong><?= Yii::t('property', 'Mortgage Insurance') ?></strong>
                                                                                </p>
                                                                                <p>
                                                                                    <?= Yii::t('property', '-- Mortgage Insurance description --') ?>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="pay-sum text-center">
                                                            <?= Yii::t('property', 'Monthly Payment') ?>:
                                                            <span data-role="mortgage-payment"></span>
                                                        </div>
                                                        <div class="text-center">
                                                            <?= Html::a(Yii::t('property', 'Send Request'), [
                                                                '/property/ajax/request-mortgage', 'entity_id' => $property['id']
                                                            ], [
                                                                'class' => 'btn btn-small btn-blue-white',
                                                                'data-action' => 'request-mortgage',
                                                                'data-target' => '#dynamic-modal',
                                                                'data-toggle' => 'modal'
                                                            ]) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                <?php } ?>
                                <section class="params-block">
                                    <div class="params-header" data-toggle="collapse" data-target="#views">
                                        <h2 class="text-left"><?= Yii::t('property', 'Stats for this property') ?></h2>
                                    </div>
                                    <div class="collapse" id="views">
                                        <div class="params-content">
                                            <div class="stat-list row">
                                                <div class="stat-block col-md-6 col-sm-6">
                                                    <div class="stat-count text-center">
                                                        <?= $property['views_count'] ?>
                                                    </div>
                                                    <div class="stat-descr">
                                                        <div>
                                                            <strong><?= Yii::t('property', '{views, plural, one{view} other{views}}', ['views' => $property['views_count']]) ?></strong>
                                                        </div>
                                                        <div>
                                                            <?= Yii::t('property', 'in the last {days, plural, one{# day} other{# days}}', ['days' => 30]) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="stat-block col-md-6 col-sm-6">
                                                    <div class="stat-count text-center">
                                                        0
                                                    </div>
                                                    <div class="stat-descr">
                                                        <div>
                                                            <strong><?= Yii::t('property', '{saves, plural, one{user} other{users}} saved', ['saves' => 0]) ?></strong>
                                                        </div>
                                                        <div>
                                                            <?= Yii::t('property', 'this property to their favorites in the last {days, plural, one{# day} other{# days}}', ['days' => 30]) ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <?php if (!empty($property['video'])) { ?>
                                    <section class="params-block">
                                        <div class="params-header" data-toggle="collapse" data-target="#video"
                                             data-action="init-video">
                                            <h2 class="text-left">
                                                <?= Yii::t('property', 'Video tour') ?>
                                            </h2>
                                        </div>
                                        <div class="collapse" id="video">
                                            <div class="prop-video position-relative">
                                                <iframe height=480 class="w-100" src=''
                                                        data-src="<?= $property['video']['content'] ?>">
                                                </iframe>
                                            </div>
                                        </div>
                                    </section>
                                <?php } ?>
                                <?php if ($property['lat'] && $property['lng']) { ?>
                                    <section class="params-block">
                                        <div class="params-header" data-toggle="collapse" data-target="#school"
                                             data-action="init-gmap">
                                            <h2 class="text-left"><?= Yii::t('property', 'Property on map') ?></h2>
                                        </div>
                                        <div class="collapse" id="school">
                                            <div id="modal-map" class="modal-property-map"
                                                 data-lat="<?= $property['lat'] ?>"
                                                 data-lng="<?= $property['lng'] ?>" data-type="property">
                                            </div>
                                        </div>
                                    </section>
                                <?php } ?>
                            </div>
                            <div class="contact-agent-block">
                                <h2 class="up-title text-left">
                                    <?= Yii::t('main', 'Contact local expert') ?>
                                </h2>
                                <?php $form = ActiveForm::begin([
                                    'action' => Url::to(['/contact/contact-agent']),
                                    'id' => 'bottom-contact-agent-form',
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => false,
                                    'options' => [
                                        'data-addon' => $property['type'] === Property::TYPE_SALE
                                            ? Url::to(['/property/ajax/quiz', 'entity_id' => $property['id']])
                                            : null
                                    ]
                                ]); ?>
                                <?= $form->field($contactAgentForm, 'url')->hiddenInput([
                                    'id' => 'bottom-contact-agent-slug',
                                    'value' => $property['slug'],
                                ])->error(false)->label(false) ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php foreach ($realtors as $id => $realtor) { ?>
                                            <div class="chover">
                                                <?= Html::radio('ContactAgentForm[agentId]', 'agentId', [
                                                    'id' => "bottom-contact-agent-id-{$id}",
                                                    'class' => 'radio-checkbox',
                                                    'value' => $realtor['id']
                                                ]) ?>
                                                <label for="bottom-contact-agent-id-<?= $id ?>">
                                                </label>
                                                <?= $this->render('@frontend/views/agent/agent-block', ['realtor' => $realtor]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($contactAgentForm, 'name')->textInput([
                                            'placeholder' => Yii::t('main', 'Your Name') . '*',
                                            'id' => 'bottom-contact-agent-your-name',
                                            'data-role' => 'name-field'
                                        ])->label(false) ?>
                                        <?= $form->field($contactAgentForm, 'phone')->textInput([
                                            'placeholder' => Yii::t('main', 'Phone'),
                                            'id' => 'bottom-contact-agent-phone',
                                        ])->label(false) ?>
                                        <?= $form->field($contactAgentForm, 'email')->textInput([
                                            'placeholder' => Yii::t('main', 'Email') . '*',
                                            'id' => 'bottom-contact-agent-email',
                                            'data-role' => 'email-field'
                                        ])->label(false) ?>
                                        <?= $form->field($contactAgentForm, 'message')->textarea([
                                            'value' => Yii::t('property', 'I am interested in {property}', ['property' => $property['title']]),
                                            'id' => 'bottom-contact-agent-message',
                                        ])->label(false) ?>

                                        <div class="animate-input text-center">
                                            <input id="contact-agent-bottom" type="submit"
                                                   class="btn btn-small btn-blue-white width100"
                                                   value="<?= Yii::t('main', 'Contact Agent') ?>">
                                            <label for="contact-agent-bottom" class="animate-button">
                                                <div class="btn-wrapper">
                                                    <div class="btn-original"><?= Yii::t('main', 'Contact Agent') ?></div>
                                                    <div class="btn-container">
                                                        <div class="left-circle"></div>
                                                        <div class="right-circle"></div>
                                                        <div class="mask"></div>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php ActiveForm::end() ?>
                                <div class="ca-temrs">
                                    <?= Yii::t('property', 'By sending request you are automatically agree with {tos}', [
                                        'tos' => Html::a(Yii::t('property', 'Terms of Service'), '#')
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="contact-agent-block">
                        <h2 class="up-title"><?= Yii::t('property', 'Property listed by') ?></h2>
                        <div class="agent-block">
                            <a href="<?= Url::to(['/agent/agent/view', 'id' => $property['user_id']]) ?>"
                               class="agent-photo">
                                <?= Html::img($property['user']['avatar'], ['alt' => $property['user']['username']]) ?>
                            </a>
                            <div class="agent-middle">
                                <div class="agent-name">
                                    <a href="<?= Url::to(['/agent/agent/view', 'id' => $property['user_id']]) ?>"><?= $property['user']['username'] ?></a>
                                </div>
                                <div class="agent-rate">
                                    <a href="#" class="agent-rate-alias">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </a>
                                    <!--                                        ( <a href="#"> 0 </a> )-->
                                </div>
                                <div class="agent-add-info">
                                    <div>
                                        <?= $property['user']['type'];?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="user-descr">
                            <p class="text-center">
                                <?= Html::a(Yii::t('property', 'Show Contacts'), [
                                    '/property/ajax/show-contacts',
                                    'entity_id' => $property['id'],
                                    'owner_id' => $property['user_id']
                                ], [
                                    'data-action' => 'show-contacts',
                                    'data-target' => '#dynamic-modal',
                                    'data-toggle' => 'modal'
                                ]) ?>
                            </p>
                        </div>
                    </div>
                    <?= $this->render('@frontend/views/agent/contact-agent-block', [
                        'blockTitle' => Yii::t('main', 'Do you have a questions? Contact us'),
                        'options' => [
                            'data-addon' => $property['type'] === Property::TYPE_SALE
                                ? Url::to(['/property/ajax/quiz', 'entity_id' => $property['id']])
                                : null
                        ],
                        'contactAgentForm' => $contactAgentForm,
                        'realtors' => $realtors,
                        'url' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                        'title' => $property['title'],
                        'limit' => 0,
                        'buttonConfig' => [
                            'entityId' => $property['id'],
                            'extraButtons' => true
                        ]
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="modal-home-footer">
            <div class="nearby-cols row">
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', '{rooms, plural, =0{} one{# room} other{# rooms}} Properties with similar area but cheaper price', [
                            'rooms' => $property['attributes']['rooms']['value'] ?? 0
                        ]) ?>
                    </div>
                    <?php if (count($cheaperProperties) > 0) { ?>
                        <ul class="no-list ul-nearby">
                            <?php foreach ($cheaperProperties as $cheaperProperty) { ?>
                                <li>
                                    <a href="<?= Url::to(['/property/property/show', 'slug' => $cheaperProperty['slug']]) ?>"
                                       target="_blank">
                                        <?= $cheaperProperty['title'] ?>
                                        <b><?= $cheaperProperty['price'] ?></b>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <?= Yii::t('main', 'No results found') ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', '{rooms, plural, =0{} one{# room} other{# rooms}} Properties with similar area but higher price', [
                            'rooms' => $property['attributes']['rooms']['value'] ?? 0
                        ]) ?>
                    </div>
                    <?php if (count($moreExpensiveProperties) > 0) { ?>
                        <ul class="no-list ul-nearby">
                            <?php foreach ($moreExpensiveProperties as $moreExpensiveProperty) { ?>
                                <li>
                                    <a href="<?= Url::to(['/property/property/show', 'slug' => $moreExpensiveProperty['slug']]) ?>"
                                       target="_blank">
                                        <?= $moreExpensiveProperty['title'] ?>
                                        <b><?= $moreExpensiveProperty['price'] ?></b>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <?= Yii::t('main', 'No results found') ?>
                    <?php } ?>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Properties with bigger area for same price') ?>
                    </div>
                    <?php if (count($biggerProperties) > 0) { ?>
                        <ul class="no-list ul-nearby">
                            <?php foreach ($biggerProperties as $biggerProperty) { ?>
                                <li>
                                    <a href="<?= Url::to(['/property/property/show', 'slug' => $biggerProperty['slug']]) ?>"
                                       target="_blank">
                                        <?= $biggerProperty['title'] ?>
                                        <b><?= $biggerProperty['price'] ?></b>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <?= Yii::t('main', 'No results found') ?>
                    <?php } ?>
                </div>
            </div>
            <div class="home-footer-info">
                <!--                <p>-->
                <!--                    SEO TEXT TEMPLATE-->
                <!--                </p>-->
            </div>
        </div>
    </div>
    <div id="fancy-caption-container" class="hidden">
        <div>
            <h3><?= $property['price'] ?></h3>
            <h4 class="up-title text-left">
                <?= Yii::t('property', 'Property Parameters') ?>
            </h4>
            <ul class="no-list fancybox-attributes">
                <?php foreach ($property['attributes'] as $attribute) { ?>
                    <li>
                        <?= "{$attribute['title']}: {$attribute['value']}" ?>
                    </li>
                <?php } ?>
            </ul>
            <?= $this->render('@frontend/views/agent/contact-agent-block', [
                'propertyId' => $property['id'],
                'contactAgentForm' => $contactAgentForm,
                'realtors' => array_slice($realtors, 0, 1),
                'url' => Url::to(['/property/property/show', 'slug' => $property['slug']], true),
                'title' => $property['title'],
                'blockId' => 'fancy'
            ]) ?>
        </div>
    </div>

<?php $more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');
$language = Yii::$app->language;
$currency = Yii::$app->params['app_currency'];
$price = $property['price'];

$script = <<<JS
    var moreLess = $('.more-less');

    moreLess.MoreLess({
        moreText: '{$more}&nbsp;',
        lessText: '{$less}&nbsp;'
    });
    
    /* Open calculator */
    $(document.body).on('click', '.calculator-btn', function() {
        $('.calculator-modal').toggleClass('open');
        
        return false;
    });
    
    /* Open sub menu more */
    $(document.body).on('click', '#blueMenuMore', function() {
        $('.blue-sub-menu').slideToggle(200);
    });
    
    $('.scroll-alias').on('click', function() {
        var anchor = $(this).attr('href');
        $('#property-modal').stop().animate({
            scrollTop: $(anchor).offset().top
        }, 500);
    });
    
    $('[data-action=init-video]').on('click', function() {
        let iframe = $(this).closest('.params-block').find('iframe');
        iframe.attr('src', iframe.data('src'));
    });
JS;

$this->registerJs($script);

if ($property['type'] === Property::TYPE_SALE) {
    $script = <<<JS
        var mortgageCalculator = new MortgageCalculator({
            locale: '{$language}',
            currency: '{$currency}'
        });
        mortgageCalculator.calculate();
        var compactMortgageCalculator = new MortgageCalculator({
            selector: '#compact-mortgages',
            locale: '{$language}',
            currency: '{$currency}'
        });
        compactMortgageCalculator.calculate();
        mortgageCalculator.registerHandlers();
        compactMortgageCalculator.registerHandlers();
JS;

    $this->registerJs($script);
}

if ($property['lat'] && $property['lng']) {
    $markerPropertyIcon = Url::to(['/images/marker.png'], true);
    $script = <<<JS
        var mapInitialized = false;
        
        $('[data-action=init-gmap]').on('click', function() {
            if(mapInitialized === false) {
                var modalMap = new ModalMap({
                    markerIcons: {
                        property: '{$markerPropertyIcon}'
                    }
                });
                modalMap.init();
                modalMap.setMarker();
                
                mapInitialized = true;
            } 
        });
JS;
    $this->registerJs($script);
}