<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.11.2018
 * Time: 16:36
 */

use frontend\assets\CatalogAsset;
use frontend\forms\contact\ContactAgentForm;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\PropertyDirectModalAsset::register($this);
CatalogAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\frontend\assets\FilterAsset::register($this);

/**
 * @var array $property
 * @var array $building
 * @var array $similarProperties
 * @var string $similarUrl
 * @var array $cheaperProperties
 * @var array $moreExpensiveProperties
 * @var array $biggerProperties
 * @var array $seo
 * @var array $realtors
 * @var View $this
 * @var ContactAgentForm $contactAgentForm
 * @var string $catalogCategory
 */

?>

<div data-role="catalog-container">

</div>

<div class="modal fade home-modal" id='direct-modal' tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?= $this->render('show', [
                'property' => $property,
//                'similarProperties' => $similarProperties,
//                'similarUrl' => $similarUrl,
                'seo' => $seo,
                'realtors' => $realtors,
                'contactAgentForm' => $contactAgentForm,
                'building' => $building,
                'cheaperProperties' => $cheaperProperties,
                'moreExpensiveProperties' => $moreExpensiveProperties,
                'biggerProperties' => $biggerProperties
            ]) ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php $catalogUrl = Url::to(['/property/catalog/index', 'operation' => 'sale', 'category' => $catalogCategory, 'per-page' => 18]);
$script = <<<JS
    $('#direct-modal').PropertyDirectModal({
        catalogUrl: '{$catalogUrl}'
    });
    $('#direct-modal').modal('show');
JS;
$this->registerJs($script);