<?php

use yii\helpers\Html;
use common\components\CurrencyHelper;

/**
 * @var array $service
 */

?>

<div class="mob-work-item">
    <div class="mob-work-table">
        <a class="mob-work-img" data-pjax="0" href="<?= $service['url']?>">
            <?= Html::img($service['img'], [
                'alt' => $service['title'],
                'title' => $service['title'],
            ]) ?>
            <?= Html::img('/images/pro-icon.png', ['class' => 'tariff-icon']) ?>
        </a>
        <div class="mob-work-info">
            <?= Html::a($service['title'], $service['url'], ['class' => 'bf-descr', 'target' => '_blank']) ?>

            <div class="mob-block-bottom">
                <div class="by">
                    <?= Yii::t('main', 'by') ?>
                    <?= $service['username'] ?>
                </div>
                <?= Html::a(CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $service['price']), $service['url'], [
                    'class' => 'block-price',
                    'target' => '_blank'
                ]) ?>
            </div>
        </div>
    </div>
</div>