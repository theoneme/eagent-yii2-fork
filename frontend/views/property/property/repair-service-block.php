<?php

use frontend\assets\ServiceAsset;
use common\components\CurrencyHelper;
use yii\web\View;

/**
 * @var View $this
 * @var float $area
 */

ServiceAsset::register($this);

$services = [
    [
      'url' => 'https://ujobs.me/remont-kvartiry-pod-klyuch-so-5bb4c4eb15b91-job',
      'img' => '/images/service/1.jpg',
      'title' => 'Ремонт квартиры под ключ со стоимостью материалов в стиле Эко',
      'username' => 'uhome',
      'price' => 15000,
    ],
    [
        'url' => 'https://ujobs.me/remont-kvartiry-pod-klyuch-so-5bb4c24166e9c-job',
        'img' => '/images/service/2.jpg',
        'title' => 'Ремонт квартиры под ключ со стоимостью материалов в стиле Техно',
        'username' => 'uhome',
        'price' => 8000,
    ],
    [
        'url' => 'https://ujobs.me/remont-kvartiry-pod-klyuch-so-5bb4bbe29609d-job',
        'img' => '/images/service/3.jpg',
        'title' => 'Ремонт квартиры под ключ со стоимостью материалов в Скандинавском стиле',
        'username' => 'uhome',
        'price' => 14000,
    ]
];
?>

<div class="contact-agent-block repair-block text-center">
    <h2 class="up-title text-center"><?= Yii::t('property', 'Turnkey repair services') ?></h2>
    <div><?= Yii::t('property', '(work with materials cost)')?></div>
    <div class="repair-green">
        <?= Yii::t('property', 'Price from {min} to {max} per m²', [
            'min' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 8000),
            'max' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 15000)
        ])?>
    </div>
    <div><?= Yii::t('property', 'The estimated cost of repairing your condo will be')?>:</div>
    <div class="repair-green-big"><?= CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $area * 10000)?></div>
</div>

<div class="contact-agent-block">
    <?php foreach ($services as $service) {
        echo $this->render('service-item', ['service' => $service]);
    }?>
</div>