<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2018
 * Time: 19:20
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\contact\OfferPriceForm $priceForm
 * @var integer $entityId
 * @var array $currencies
 */

?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title text-center"><?= Yii::t('property', 'Offer your price') ?> </h4>
    </div>
    <div class="modal-body">
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/property/ajax/offer-price', 'entity_id' => $entityId]),
            'id' => 'offer-price-modal-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>
        <?= $form->field($priceForm, 'name')->textInput(['placeholder' => Yii::t('labels', 'Enter your name')]) ?>
        <?= $form->field($priceForm, 'phone')->textInput(['placeholder' => Yii::t('labels', 'Enter your phone')]) ?>
        <?= $form->field($priceForm, 'email')->textInput(['placeholder' => Yii::t('labels', 'Enter your email address')]) ?>
        <?= $form->field($priceForm, 'price')->textInput(['type' => 'number','placeholder' => Yii::t('labels', 'Enter offer price')]) ?>
        <?= $form->field($priceForm, 'currency_code')->dropDownList($currencies) ?>


        <div class="animate-input text-center">
            <input id="offer-price-modal-submit" type="submit" class="btn btn-small btn-blue-white width100"
                   value="<?= Yii::t('property', 'Send Request') ?>">
            <label for="offer-price-modal-submit" class="animate-button">
                <div class="btn-wrapper">
                    <div class="btn-original"><?= Yii::t('property', 'Send Request') ?></div>
                    <div class="btn-container">
                        <div class="left-circle"></div>
                        <div class="right-circle"></div>
                        <div class="mask"></div>
                    </div>
                </div>
            </label>
        </div>
        <?php ActiveForm::end() ?>
    </div>

<?php $script = <<<JS
    $('#offer-price-modal-form').on('beforeSubmit', function() { 
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.closest('.modal').modal('hide');
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);