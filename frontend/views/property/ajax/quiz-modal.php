<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2018
 * Time: 17:07
 */

use frontend\assets\FormHtmlAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\contact\QuizForm $quizForm
 * @var integer $entityId
 * @var array $intervals
 */

FormHtmlAsset::register($this);

?>

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    </div>
    <div class="modal-body">
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/property/ajax/quiz', 'entity_id' => $entityId]),
            'id' => 'quiz-modal-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'options' => [
                'data-steps' => 5,
                'data-step' => 1
            ]
        ]); ?>
        <?= Html::activeHiddenInput($quizForm, 'name') ?>
        <?= Html::activeHiddenInput($quizForm, 'email') ?>
        <div class="text-center quiz-step" data-step="1">
            <div class="question-step"><?= $quizForm->getAttributeLabel('buy') ?></div>
            <?= $form->field($quizForm, 'buy', ['template' => '{input}'])->radioList([
                1 => Yii::t('property', 'Yes'),
                0 => Yii::t('property', 'No'),
            ], [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $step = 'data-target-step=2';
                    if ($value === 0) {
                        $step = 'data-target-step=5';
                    }
                    $chk = $checked ? 'checked' : '';
                    $output = "<div class='chover'>
                    <input data-action='switch-step' {$step} name='{$name}' id='quizform-buy-{$index}' class='radio-checkbox' value='{$value}' {$chk} type='radio'>
                    <label class='btn btn-blue-white btn-big btn-radio' for='quizform-buy-{$index}'>{$label}</label>
                </div>";
                    return $output;
                }
            ])->label(false) ?>
        </div>
        <div class="text-center hidden quiz-step" data-step="2">
            <div class="question-step"><?= $quizForm->getAttributeLabel('interval') ?></div>
            <?= $form->field($quizForm, 'interval', ['template' => '{input}'])->radioList($intervals, [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $step = 'data-target-step=3';
                    $chk = $checked ? 'checked' : '';
                    $output = "<div class='chover'>
                    <input data-action='switch-step' {$step} name='{$name}' id='quizform-interval-{$index}' class='radio-checkbox' value='{$value}' {$chk} type='radio'>
                    <label class='btn btn-blue-white btn-big btn-radio' for='quizform-interval-{$index}'>{$label}</label>
                </div>";
                    return $output;
                }
            ])->label(false) ?>
        </div>
        <div class="text-center hidden quiz-step" data-step="3">
            <div class="question-step"><?= $quizForm->getAttributeLabel('mortgage') ?></div>
            <?= $form->field($quizForm, 'mortgage', ['template' => '{input}'])->radioList([
                1 => Yii::t('property', 'Yes'),
                0 => Yii::t('property', 'No'),
            ], [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $action = 'switch-step';
                    if ($value === 1) {
                        $action = 'show-input';
                    }
                    $output = "<div class='chover'>
                    <input data-action='{$action}' data-target-step='4' name='{$name}' id='quizform-mortgage-{$index}' class='radio-checkbox' value='{$value}' {$chk} type='radio'>
                    <label class='btn btn-blue-white btn-big btn-radio' for='quizform-mortgage-{$index}'>{$label}</label>
                </div>";
                    return $output;
                }
            ])->label(false) ?>
            <div class="form-group hidden">
                <div class="step-field" id="credit-amount">
                    <?= $form->field($quizForm, 'mortgageAmount')->textInput([
                        'type' => 'number',
                        'placeholder' => $quizForm->getAttributeLabel('mortgageAmount')
                    ])->label(false) ?>
                </div>
                <a href="#" data-action='switch-step' data-target-step='4' class="btn btn-blue-white btn-small">
                    <?= Yii::t('property', 'Next') ?>
                </a>
            </div>
        </div>
        <div class="text-center hidden quiz-step" data-step="4">
            <div class="question-step"><?= $quizForm->getAttributeLabel('secondary') ?></div>
            <?= $form->field($quizForm, 'secondary', ['template' => '{input}'])->radioList([
                1 => Yii::t('property', 'Yes'),
                0 => Yii::t('property', 'No'),
            ], [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $action = 'switch-step';
                    if ($value === 1) {
                        $action = 'show-input';
                    }
                    $output = "<div class='chover'>
                    <input data-action='{$action}' data-target-step='5' name='{$name}' id='quizform-secondary-{$index}' class='radio-checkbox' value='{$value}' {$chk} type='radio'>
                    <label class='btn btn-blue-white btn-big btn-radio' for='quizform-secondary-{$index}'>{$label}</label>
                </div>";
                    return $output;
                }
            ])->label(false) ?>
            <div class="form-group hidden">
                <div class="step-field">
                    <?= $form->field($quizForm, 'address')->textInput() ?>
                </div>
                <a href="#" data-action='switch-step' data-target-step='5' class="btn btn-blue-white btn-small">
                    <?= Yii::t('property', 'Next') ?>
                </a>
            </div>
        </div>
        <div class="text-center hidden quiz-step" data-step="5">
            <div class="question-step"><?= Yii::t('property', 'Thanks for your answers!') ?></div>
            <div class="form-group">
                <div class="chover">
                    <input class="btn btn-blue-white btn-big btn-radio" value="Ok" type="submit">
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>

<?php $script = <<<JS
    $("[data-action='show-input']").on("click", function() {
        $(this).closest('.quiz-step').find('.form-group.hidden').removeClass('hidden');
    });
    $("[data-action='switch-step']").on("click", function() {
        let targetStep = $(this).data('target-step');
        
        $(this).closest('.quiz-step').addClass('hidden');
        $('[data-step=' + targetStep +']').removeClass('hidden');
    });

    $('#quiz-modal-form').on('beforeSubmit', function() { console.log('asdf');
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.closest('.modal').modal('hide');
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);

