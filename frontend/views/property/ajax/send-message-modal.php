<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2018
 * Time: 17:07
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\contact\MessageForm $messageForm
 * @var integer $entityId
 */

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title text-center"><?= Yii::t('property', 'Send Message') ?></h4>
</div>
<div class="modal-body">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/property/ajax/send-message', 'entity_id' => $entityId]),
        'id' => 'send-message-modal-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    <?= $form->field($messageForm, 'name')->textInput(['placeholder' => Yii::t('labels', 'Enter your name')]) ?>
    <?= $form->field($messageForm, 'phone')->textInput(['placeholder' => Yii::t('labels', 'Enter your phone')]) ?>
    <?= $form->field($messageForm, 'email')->textInput(['placeholder' => Yii::t('labels', 'Enter your email address')]) ?>
    <?= $form->field($messageForm, 'message')->textarea(['rows' => 5]) ?>
    <div class="animate-input text-center">
        <input id="send-message-modal-submit" type="submit" class="btn btn-small btn-blue-white width100"
               value="<?= Yii::t('property', 'Send Request') ?>">
        <label for="send-message-modal-submit" class="animate-button">
            <div class="btn-wrapper">
                <div class="btn-original"><?= Yii::t('property', 'Send Request') ?></div>
                <div class="btn-container">
                    <div class="left-circle"></div>
                    <div class="right-circle"></div>
                    <div class="mask"></div>
                </div>
            </div>
        </label>
    </div>
    <?php ActiveForm::end() ?>
</div>

<?php $script = <<<JS
    $('#send-message-modal-form').on('beforeSubmit', function() { 
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.closest('.modal').modal('hide');
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);

