<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2018
 * Time: 17:06
 */

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title text-center"><?= Yii::t('main', 'Contact Agent') ?></h4>
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
            <label for="write-user-name"><?= Yii::t('property', 'First Name') ?></label>
            <input type="text" class="form-control" id="write-user-name">
        </div>
        <div class="form-group">
            <label for="write-user-phone"><?= Yii::t('property', 'Phone') ?></label>
            <input type="tel" class="form-control" id="write-user-phone">
        </div>
        <div class="form-group">
            <label for="write-user-email"><?= Yii::t('property', 'Email') ?></label>
            <input type="email" class="form-control" id="write-user-email">
        </div>
        <div class="form-group">
            <label for="write-user-message"><?= Yii::t('property', 'Your message') ?></label>
            <textarea class="form-control" id="write-user-message"></textarea>
        </div>
        <div class="animate-input text-center">
            <input id="write-user-submit" type="submit" class="btn btn-small btn-blue-white width100"
                   value="<?= Yii::t('property', 'Send Request') ?>">
            <label for="write-user-submit" class="animate-button">
                <div class="btn-wrapper">
                    <div class="btn-original"><?= Yii::t('main', 'Contact Agent') ?></div>
                    <div class="btn-container">
                        <div class="left-circle"></div>
                        <div class="right-circle"></div>
                        <div class="mask"></div>
                    </div>
                </div>
            </label>
        </div>
    </form>
</div>
