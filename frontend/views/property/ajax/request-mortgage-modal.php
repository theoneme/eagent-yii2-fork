<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 29.11.2018
 * Time: 17:01
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\forms\contact\MortgageForm;

/**
 * @var \frontend\forms\contact\MortgageForm $mortgageForm
 * @var integer $entityId
 * @var array $currencies
 */

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title text-center"><?= Yii::t('property', 'Send Request') ?></h4>
</div>
<div class="modal-body">
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/property/ajax/request-mortgage', 'entity_id' => $entityId]),
        'id' => 'request-mortgage-modal-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>
    <?= $form->field($mortgageForm, 'propertyName')->textInput() ?>
                <?= $form->field($mortgageForm, 'loanTerm')->textInput(['placeholder' => Yii::t('labels', 'How many years do you plan to repay the loan')]) ?>
                <?= $form->field($mortgageForm, 'name')->textInput(['placeholder' => Yii::t('labels', 'Enter your name')]) ?>
                <?= $form->field($mortgageForm, 'credit')->textInput() ?>
                <?= $form->field($mortgageForm, 'currencyCode')->dropDownList($currencies) ?>
                <?= $form->field($mortgageForm, 'age')->textInput(['placeholder' => Yii::t('labels', 'Enter your age')]) ?>
                <?= $form->field($mortgageForm, 'familyStatus')->dropDownList([
                    MortgageForm::STATUS_SINGLE => Yii::t('property', 'Single Person'),
                    MortgageForm::STATUS_MARRIED => Yii::t('property', 'Married'),
                    MortgageForm::STATUS_DIVORCED => Yii::t('property', 'Divorced'),
                ]) ?>

                <?= $form->field($mortgageForm, 'phone')->textInput(['placeholder' => Yii::t('labels', 'Enter your phone')]) ?>
                <?= $form->field($mortgageForm, 'email')->textInput(['placeholder' => Yii::t('labels', 'Enter your email address')]) ?>
        <div class="animate-input text-center">
            <input id="request-mortgage-modal-submit" type="submit" class="btn btn-small btn-blue-white width100"
                   value="<?= Yii::t('property', 'Send Request') ?>">
            <label for="request-mortgage-modal-submit" class="animate-button">
                <div class="btn-wrapper">
                    <div class="btn-original"><?= Yii::t('property', 'Send Request') ?></div>
                    <div class="btn-container">
                        <div class="left-circle"></div>
                        <div class="right-circle"></div>
                        <div class="mask"></div>
                    </div>
                </div>
            </label>
        </div>
    <?php ActiveForm::end() ?>
</div>

<?php $script = <<<JS
    $('#request-mortgage-modal-form').on('beforeSubmit', function() { 
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.closest('.modal').modal('hide');
             }
        });
        
        return false;
    });
JS;

$this->registerJs($script);