<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.03.2019
 * Time: 16:50
 */

use common\decorators\BuildingPropertyStatusDecorator;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\LinkPager;

/**
 * @var View $this
 * @var array $properties
 */

?>

<?php if (count($properties['items'])) { ?>
    <?php foreach ($properties['items'] as $property) { ?>
        <div class="flat-item">
            <div class="flat-title">
                <?= Html::a($property['title'], ['/property/property/show', 'slug' => $property['slug']], ['data-action' => 'load-modal-property']) ?>
            </div>
            <?= BuildingPropertyStatusDecorator::decorate($property['status']) ?>
            <div class="flat-price">
                <?= Html::a($property['price'], ['/property/property/show', 'slug' => $property['slug']]) ?>
            </div>
            <div class="flat-photos text-right">
                <?php if (count($property['images']) > 0) { ?>
                    <?php foreach ($property['images'] as $key => $image) { ?>
                        <?php if ($key === 0) { ?>
                            <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($property['images']), $image, [
                                'data-fancybox' => "gallery-{$property['id']}",
                                'data-role' => 'fancy-gallery-item'
                            ]) ?>
                        <?php } else { ?>
                            <?= Html::a(null, $image, [
                                'data-fancybox' => "gallery-{$property['id']}",
                                'class' => 'hidden',
                                'data-role' => 'fancy-gallery-item'
                            ]) ?>
                        <?php } ?>
                    <?php } ?>
                <?php } else { ?>
                    <i class="icon-camera"></i>&nbsp;<?= count($property['images']) ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php if ($properties['pagination']) {
        echo LinkPager::widget([
            'pagination' => $properties['pagination'],
            'linkOptions' => ['data-action' => 'switch-page'],
        ]);
    } ?>
<?php } else { ?>
    <p>
        <?= Yii::t('main', 'No results found'); ?>
    </p>
<?php } ?>
