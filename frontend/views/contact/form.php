<?php

use frontend\forms\contact\ContactForm;
use common\widgets\GmapsActiveInputWidget;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var ContactForm $model
 * @var View $this
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['/contact/contact-us']),
    'id' => 'contact-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
]); ?>
    <?= $form->field($model, 'name')->textInput([
        'placeholder' => Yii::t('labels', 'Enter your name') . '*',
        'id' => 'contact-form-your-name',
    ]) ?>
    <?= $form->field($model, 'phone')->textInput([
        'placeholder' => Yii::t('labels', 'Enter your phone'),
        'id' => 'contact-form-phone',
    ])?>
    <?= $form->field($model, 'email')->textInput([
        'placeholder' => Yii::t('labels', 'Enter your email address') . '*',
        'id' => 'contact-form-email',
    ])?>

    <div class="form-group">
    <label class="control-label" for="contact-form-email"><?=Yii::t('labels', 'Address of your property')?></label>
    <?= GmapsActiveInputWidget::widget([
        'model' => $model,
        'inputOptions' => [
            'placeholder' => Yii::t('labels', 'Enter your address') . '*',
            'class' => 'form-control'
        ]
    ])?>
    </div>
    <?/*= $form->field($model, 'floor')->textInput([
        'type' => 'number',
        'placeholder' => Yii::t('labels', 'Enter the floor'),
        'id' => 'contact-form-floor',
    ]) */?>
    <?= $form->field($model, 'rooms')->textInput([
        'type' => 'number',
        'placeholder' => Yii::t('labels', 'Enter the number of rooms'),
        'id' => 'contact-form-rooms',
    ]) ?>
    <?= $form->field($model, 'area')->textInput([
        'type' => 'number',
        'placeholder' => Yii::t('labels', 'Enter the area of your property'),
        'id' => 'contact-form-area',
    ])?>


    <div class="animate-input text-center">
        <input id="contact-us-submit" type="submit" class="btn btn-small btn-blue-white width100" value="<?= Yii::t('property', 'Send Request') ?>">
        <label for="contact-us-submit" class="animate-button">
            <div class="btn-wrapper">
                <div class="btn-original"><?= Yii::t('property', 'Send Request') ?></div>
                <div class="btn-container">
                    <div class="left-circle"></div>
                    <div class="right-circle"></div>
                    <div class="mask"></div>
                </div>
            </div>
        </label>
    </div>
<?php ActiveForm::end() ?>

<?php
$script = <<<JS
    $(document).on('submit', '#contact-form', function(){
        let form = $(this);
        $.post(form.attr('action'), 
            form.serialize(), 
            function(response) {
                if (response && response.success) {
                    form.closest('.modal').modal('hide');
                }
                else {
                    // ???
                }
            }
        );
        return false;
    });
JS;
$this->registerJs($script);