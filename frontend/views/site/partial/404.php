<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 06.11.2018
 * Time: 13:13
 */

?>

<div class="container box-404">
    <div class="brick"></div>
    <div class="number text-right">
        <div class="four"></div>
        <div class="zero">
            <div class="nail"></div>
        </div>
        <div class="four"></div>
    </div>
    <div class="info text-left">
        <h1 class="text-left"><?= Yii::t('main', 'Something is wrong'); ?></h1>
        <p><?= Yii::t('main', 'The page you are looking for was moved, removed, renamed or might never existed.'); ?></p>
        <a href="/" class="btn btn-blue-white btn-big"><?= Yii::t('main', 'Go Home') ?></a>
    </div>
</div>
