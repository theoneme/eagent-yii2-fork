<?php

use common\helpers\UtilityHelper;
use frontend\assets\CommonAsset;
use frontend\assets\ErrorAsset;
use yii\base\Action;
use yii\base\Controller;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var \yii\base\Exception $exception
 * @var $content string
 */

CommonAsset::register($this);
ErrorAsset::register($this);
$this->title = $exception->getMessage();
Yii::$app->controller = new Controller('site', Yii::$app);
Yii::$app->controller->action = new Action('index', Yii::$app->controller);

$statusCode = $exception->statusCode ?? 500;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/x-icon"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= \frontend\widgets\MobileMenu::widget() ?>
<div class="mainblock error-blue">
    <?= \frontend\widgets\HeaderWidget::widget() ?>
    <div class="all-content">
        <?php switch ($statusCode) {
            case 403:
                echo $this->render('partial/403', [
                    'name' => $exception->getMessage(),
                    'exception' => $exception
                ]);
                break;
            case 404:
                echo $this->render('partial/404', [
                    'name' => $exception->getMessage(),
                    'exception' => $exception
                ]);
                break;
            default:
                echo $this->render('partial/500', [
                    'name' => $exception->getMessage(),
                    'exception' => $exception
                ]);
                break;
        } ?>
        <div class="mfooter"></div>
    </div>
</div>

<div class="container-fluid">
    <div id="footer" class="box-404 text-center">
        <div class="worker"></div>
        <div class="tools"></div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
