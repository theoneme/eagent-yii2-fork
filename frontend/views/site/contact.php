<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\assets\CatalogAsset;

CatalogAsset::register($this);
$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid-left">
    <h1 class="text-left"> Контакты</h1>
    <p>
        Если у Вас есть вопросы, пожелания или предложения по работе с сервисом, Вы можете связаться с нами по электронной
        почте: <a href="mailto:support@eagent.me">support@eagent.me</a>. Или отправить нам сообщение через форму обратной
        связи:
    </p>
    <div class="row">
        <form id="contact-form" class="col-md-7" action="/contact" method="post" enctype="multipart/form-data">
            <div class="form-group field-contactform-name">
                <label class="control-label" for="contactform-name">Имя</label>
                <input type="text" id="contactform-name" class="form-control" name="ContactForm[name]">
                <p class="help-block help-block-error"></p>
            </div>
            <div class="form-group field-contactform-email required">
                <label class="control-label" for="contactform-email">Email</label>
                <input type="text" id="contactform-email" class="form-control" name="ContactForm[email]"
                       aria-required="true">
                <p class="help-block help-block-error"></p>
            </div>
            <div class="form-group field-contactform-description required">
                <label class="control-label" for="contactform-description">Описание</label><textarea
                        id="contactform-description" class="form-control" name="ContactForm[description]" rows="6"
                        aria-required="true"></textarea>
                <p class="help-block help-block-error"></p>
            </div>
            <input type="hidden" id="contactform-firstname" name="ContactForm[firstname]">
            <div class="form-group field-contactform-verifycode">
                <label class="control-label" for="contactform-verifycode">Код подтверждения</label>
                <div class="row">
                    <div class="col-lg-3">
                        <img id="contactform-verifycode-image" src="/site/captcha?v=5bec36a84f25d7.55650601" alt="">
                    </div>
                    <div class="col-lg-9">
                        <input type="text" id="contactform-verifycode" class="form-control" name="ContactForm[verifyCode]">
                    </div>
                </div>
                <p class="help-block help-block-error"></p>
            </div>
            <div class="form-group">
                <div class="animate-input text-center">
                    <input id="contact-agent" type="submit" class="btn btn-small btn-blue-white width100" value="<?= Yii::t('main', 'Contact us') ?>">
                    <label for="contact-agent" class="animate-button">
                        <div class="btn-wrapper">
                            <div class="btn-original"><?= Yii::t('main', 'Contact us') ?></div>
                            <div class="btn-container">
                                <div class="left-circle"></div>
                                <div class="right-circle"></div>
                                <div class="mask"></div>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
        </form>
        <div class="col-md-5">
            <p><strong>eAgent</strong></p>
            <p>Адрес</p>
            <p>Идентификационный номер: </p>
            <p>Номер лицензии: 00/000000/00</p>
            <p>Email: info@eagent.me</p>
            <p>Телефон: +7 (912) 251-89-48<br></p>
            <p><strong>Банковские реквизиты</strong></p>
            <p>Имя Получателя: eAgent</p>
            <p>Банк получателя: </p>
            <p>Банк посредник: </p>
            <p>Код банка: </p>
            <p>Адрес банка: </p>
            <p>Счет получателя: </p>
        </div>
    </div>
</div>