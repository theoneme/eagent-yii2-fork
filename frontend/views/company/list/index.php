<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use common\decorators\CompanyMemberRoleDecorator;
use common\models\CompanyMember;
use yii\helpers\Html;
use yii\web\View;

\frontend\assets\CatalogAsset::register($this);

/**
 * @var array $companies
 * @var View $this
 */

?>

<div class="catalog-wrap no-padding">
    <div class="col-md-12">
        <h1 class="text-left"><?= Yii::t('account', 'My Companies') ?></h1>

        <div class='form-group'>
            <?= Html::a(Yii::t('account', 'Create Company'), ['/company/manage/create'], [
                'class' => 'btn btn-success'
            ]) ?>
        </div>

        <?php if (count($data['items']) > 0) { ?>
            <div class="table-houses no-margin">
                <table id="list-houses text-left">
                    <thead>
                    <tr>
                        <th class="up">ID</th>
                        <th class="up"><?= Yii::t('account', 'Title') ?></th>
                        <th class="up"><?= Yii::t('account', 'Logo') ?></th>
                        <th class="up"><?= Yii::t('account', 'Role') ?></th>
                        <th class="up"><?= Yii::t('account', 'Action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($data['items'] as $company) { ?>
                        <tr>
                            <td><?= $company['id'] ?></td>
                            <td>
                                <?= Html::a($company['companyTitle'], ['/agent/agent/view', 'id' => $company['userId']]) ?>
                            </td>
                            <td>
                                <?= Html::img($company['companyLogo'], ['class' => 'list-image-preview']) ?>
                            </td>
                            <td><?= CompanyMemberRoleDecorator::decorate($company['role']) ?></td>
                            <td>
                                <?php
                                    echo Html::a('<i class="icon-multiple-users-silhouette"></i>', ['/company/list/members', 'company_id' => $company['id']]);
                                    if ($company['id'] === CompanyMember::ROLE_OWNER) {
                                        echo Html::a('<i class="icon-pencil"></i>', ['/company/manage/update', 'id' => $company['id']]);
                                    }
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

            <?php if ($data['pagination']) { ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $data['pagination'],
                ]); ?>
            <?php } ?>
        <?php } else { ?>
            <p><?= Yii::t('main', 'No results found') ?></p>
        <?php } ?>

    </div>
</div>
