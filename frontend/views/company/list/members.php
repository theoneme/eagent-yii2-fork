<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use common\decorators\CompanyMemberRoleDecorator;
use common\decorators\CompanyMemberStatusDecorator;
use common\models\CompanyMember;
use yii\helpers\Html;
use yii\web\View;

\frontend\assets\CatalogAsset::register($this);

/**
 * @var array $members
 * @var View $this
 * @var integer $companyId
 * @var CompanyMember $currentMember
 */

?>

<div class="catalog-wrap no-padding">
    <div class="col-md-12">
        <h1 class="text-left"><?= Yii::t('account', 'Company Members') ?></h1>

        <div class="form-group">
            <?= Html::a(Yii::t('account', 'Back to companies'), ['/company/list/index'], [
                'title' => Yii::t('account', 'Back to companies'),
                'class' => 'btn btn-success',
            ]) ?>

            <?php if ($currentMember->can('manageCompanyMembers')) {
                echo  Html::a(Yii::t('account', 'Add Company Member'), ['/company/manage-member/create', 'company_id' => $companyId], ['class' => 'btn btn-success']);
            } ?>
        </div>

        <?php if (count($members['items']) > 0) { ?>
            <div class="table-houses no-margin">
                <table id="list-houses text-left">
                    <thead>
                    <tr>
                        <th class="up"><?= Yii::t('account', 'Username') ?></th>
                        <th class="up"><?= Yii::t('account', 'Avatar') ?></th>
                        <th class="up"><?= Yii::t('account', 'Role') ?></th>
                        <th class="up"><?= Yii::t('account', 'Status') ?></th>
                        <th class="up"><?= Yii::t('account', 'Action') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($members['items'] as $member) { ?>
                        <tr>
                            <td>
                                <?= Html::a($member['username'], ['/agent/agent/view', 'id' => $member['user_id']]);?>
                            </td>
                            <td>
                                <?= Html::img($member['avatar'], ['class' => 'list-image-preview']) ?>
                            </td>
                            <td><?= CompanyMemberRoleDecorator::decorate($member['role']) ?></td>
                            <td><?= CompanyMemberStatusDecorator::decorate($member['status']) ?></td>
                            <td>
                                <?php if ($currentMember->can('manageCompanyMembers')) {
                                    echo Html::a('<i class="icon-pencil"></i>', ['/company/manage-member/update', 'id' => $member['id'], 'company_id' => $member['company_id']]);
                                    echo Html::a(
                                            '<i class="icon-delete-button"></i>',
                                            ['/company/manage-member/delete', 'id' => $member['id'], 'company_id' => $member['company_id']],
                                            ['data' =>  ['confirm' => Yii::t('account', 'Are you sure?'), 'method' => 'post']]
                                    );
                                }?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

            <?php if ($members['pagination']) { ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $members['pagination'],
                ]); ?>
            <?php } ?>
        <?php } else { ?>
            <p><?= Yii::t('main', 'No results found') ?></p>
        <?php } ?>

    </div>
</div>
