<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

use common\decorators\CompanyTypeDecorator;

/**
 * @var array $step
 * @var \frontend\forms\ar\UserForm $companyForm
 * @var \yii\widgets\ActiveForm $form
 */
?>

<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Company Title') ?>"
     data-content="<?= Yii::t('wizard', 'Set Company Title') ?>">
    <?= $form->field($companyForm->meta, 'title')->textInput([
        'placeholder' => Yii::t('wizard', 'Company Title'),
    ]) ?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Company Description') ?>"
     data-content="<?= Yii::t('wizard', 'Set Company Description') ?>">
    <?= $form->field($companyForm->meta, 'description')->textarea([
        'placeholder' => Yii::t('wizard', 'Company Description'),
        'rows' => 4
    ]) ?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Company Type') ?>"
     data-content="<?= Yii::t('wizard', 'Specify Company Type') ?>">
    <?= $form->field($companyForm, 'type')->radioList(CompanyTypeDecorator::getTypeLabels(), [
        'item' => function ($index, $label, $name, $checked, $value){
            $chk = $checked ? 'checked' : '';
            $output = "<div class='chover'>
            <input name='{$name}' id='userform-{$name}-{$index}' class='radio-checkbox' value='{$value}' {$chk} type='radio'>
            <label for='userform-{$name}-{$index}'>{$label}</label>
        </div>";
            return $output;
        },
    ])?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Advertising on Site Allowed') ?>"
     data-content="<?= Yii::t('wizard', 'Set Permission for Advertising on Site') ?>">
    <?= $form->field($companyForm, 'ads_allowed')->radioList([
        1 => Yii::t('main', 'Yes'),
        0 => Yii::t('main', 'No')
    ], [
        'item' => function ($index, $label, $name, $checked, $value){
            $chk = $checked ? 'checked' : '';
            $output = "<div class='chover radio-btn'>
            <input name='{$name}' id='userform-{$name}-{$index}' class='radio-checkbox' value='{$value}' {$chk} type='radio'>
            <label for='userform-{$name}-{$index}'>{$label}</label>
        </div>";
            return $output;
        },
        'class' => 'flex'
    ])?>
</div>
<div class="block-with-notes"
     data-toggle="popover" data-placement="right"
     data-original-title="<?= Yii::t('wizard', 'Advertising on Partner Sites Allowed') ?>"
     data-content="<?= Yii::t('wizard', 'Set Permission for Advertising on Partner Sites') ?>">
    <?= $form->field($companyForm, 'ads_allowed_partners')->radioList([
        1 => Yii::t('main', 'Yes'),
        0 => Yii::t('main', 'No')
    ], [
        'item' => function ($index, $label, $name, $checked, $value){
            $chk = $checked ? 'checked' : '';
            $output = "<div class='chover radio-btn'>
            <input name='{$name}' id='userform-{$name}-{$index}' class='radio-checkbox'' value='{$value}' {$chk} type='radio'>
            <label for='userform-{$name}-{$index}'>{$label}</label>
        </div>";
            return $output;
        },
        'class' => 'flex'
    ])?>
</div>



