<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use common\forms\ar\CompanyForm;
use frontend\controllers\company\ManageController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\frontend\assets\CatalogAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\assets\WizardAsset::register($this);

/**
 * @var CompanyForm $companyForm
 * @var array $route
 * @var integer $action
 */

?>

    <div class="wizard-wrap">
        <h1 class="text-left"><?= Yii::t('wizard', 'Post Company') ?></h1>
        <?php $form = ActiveForm::begin([
            'action' => Url::to($route),

            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => "<div class='row'>
                <div class='col-md-4'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {hint}
                    {input}
                    {error}
                </div>
            </div>",
            ],
            'options' => [
                'id' => 'company-form',
                'class' => 'form-wizard'
            ],
        ]); ?>
        <div class="wizard-content">
            <div class="row wizard-row">
                <div class="col-md-8 col-sm-12">
                    <div class="wizards-steps">
                        <?php foreach ($companyForm->steps as $key => $step) { ?>
                            <section id="step<?= $key ?>">
                                <div class="wizard-step-header">
                                    <?= $step['title'] ?>
                                    <span>
                                        <?= Yii::t('wizard', 'Step {first} from {total}', [
                                            'first' => $key,
                                            'total' => count($companyForm->steps)
                                        ]) ?>
                                    </span>
                                </div>
                                <div class="row step-content">
                                    <div class="col-md-8">
                                        <?php foreach ($step['config'] as $stepConfig) { ?>
                                            <?php if ($stepConfig['type'] === 'view') { ?>
                                                <?= $this->render("steps/{$stepConfig['value']}", [
                                                    'form' => $form,
                                                    'companyForm' => $companyForm,
                                                    'step' => $step,
                                                    'createForm' => false
                                                ]) ?>
                                            <?php } ?>

                                            <?php if ($stepConfig['type'] === 'constructor') { ?>
                                                <?php foreach ($stepConfig['groups'] as $group) { ?>
                                                    <?php $attributes = $companyForm->dynamicForm->getAttributesByGroup($group) ?>
                                                    <?= $this->render("partial/attributes-group", [
                                                        'form' => $form,
                                                        'attributes' => array_flip($attributes),
                                                        'dynamicForm' => $companyForm->dynamicForm
                                                    ]) ?>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </section>
                        <?php } ?>
                        <div class="text-center">
                            <?= Html::a(Yii::t('wizard', 'Back'), Yii::$app->request->referrer, [
                                'class' => 'btn btn-big btn-white-blue'
                            ]) ?>
                            <?php if ($action === \frontend\controllers\property\ManageController::ACTION_CREATE) { ?>
                                <?= Html::submitInput(Yii::t('wizard', 'Publish Company'), [
                                    'class' => 'btn btn-big btn-blue-white'
                                ]) ?>
                            <?php } else { ?>
                                <?= Html::submitInput(Yii::t('wizard', 'Save Changes'), [
                                    'class' => 'btn btn-big btn-blue-white'
                                ]) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <nav class="col-md-4 visible-lg visible-md progress-menu-nav menu-wizard-aside">
                    <div class="nav" data-spy="affix" data-offset-top="300">
                        <div class="progress-steps">
                            <div class="progress-percent text-center">
                                0
                            </div>
                            <div class="progress-info">
                                <?= Yii::t('wizard', 'Fill your announcement with maximum amount of details, so it will be seen more often') ?>
                            </div>
                        </div>
                        <ul class="no-list progress-menu">
                            <?php foreach ($companyForm->steps as $key => $step) { ?>
                                <li>
                                    <a href="#step<?= $key ?>">
                                        <?= $step['title'] ?>
                                        <div>

                                        </div>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
<?php $canonical = Url::canonical();
$isUpdate = (int)($action === ManageController::ACTION_UPDATE);
$script = <<<JS
var calculatorFlag = false,
    isUpdate = {$isUpdate},
    validationPerformed = false,
    progressCalculator = new ProgressCalculator({
        formSelector: '#company-form'
    }),
    form = $('#company-form');

if(isUpdate) {
    form.yiiActiveForm('validate', true);
    progressCalculator.updateProgress();
} else {
    validationPerformed = true
}

form.on('submit', function() { 
    calculatorFlag = true;
    let hasToUpload = false;
    $.each($(".file-upload-input"), function() {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            hasToUpload = true;
            return false;
        }
    });

    if(hasToUpload === false) {
        if(validationPerformed === true) { 
             return true;
        } else { 
            validationPerformed = true;
            return false;
        }
    } else {
        return false;
    }
}).on("afterValidateAttribute", function(event, attribute, messages) {
    if(calculatorFlag === false) {
        progressCalculator.updateProgress();
    }
}).on("afterValidate", function(event, messages, errorAttributes) { 
    progressCalculator.updateProgress();
    calculatorFlag = false;
});

var tt = 'right';
if($('body').hasClass('rtl')){
    tt = 'left';
}
$('.block-with-notes').popover({
    trigger: 'hover',
    placement: tt
});
$('body').scrollspy({
    target: '.progress-menu-nav',
    offset: 300
});
$(".progress-menu a").on('click', function(event) {
    if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800);
    }
});

JS;
$this->registerJs($script);

?>