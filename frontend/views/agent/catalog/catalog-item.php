<?php

use common\helpers\GoogleStaticMapHelper;
use yii\helpers\Html;

/**
 * @var array $agent
 * @var integer $propertyCount
 * @var string $agentMarker
 */

?>
<div class="agent-item" data-role="marker-data"
     data-hover="show-marker"
     data-id="<?= $agent['id']?>"
     data-type="user">
    <div class="agent-list-info">
        <div class="agent-list-photo">
            <?= Html::img($agent['avatar'], ['alt' => $agent['username'],'title' => $agent['username']])?>
        </div>
        <div class="agent-opt">
            <div class="agent-list-name">
                <?= Html::a($agent['username'],['/agent/agent/view', 'id' => $agent['id']])?>
            </div>
            <div class="agent-list-phone">
                <?= $agent['phone']?>
            </div>
            <div class="agent-rate">
                <a href="#" class="agent-rate-alias">
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                    <i class="icon-favorite"></i>
                </a>
            </div>
            <a href="#" class="agent-list-count">
                <?= !empty($agent['reviews']) ? Yii::t('agent', '{count} total reviews', ['count' => count($agent['reviews'])]) : ''?>
            </a>
            <?php if (!empty($agent['lat']) && !empty($agent['lng'])) { ?>
                <div class="territory text-left">
                    <?php if (!empty($agent['addressData']['city'])) {
                        echo Yii::t('main', 'in city {city}', ['city' => $agent['addressData']['city']]);
                    }?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="map-sale-over text-center">
        <div class="map-sale">
            <div class="agent-map-block">
                <?php if (!empty($agent['lat']) && !empty($agent['lng'])) {
                    $image = GoogleStaticMapHelper::getMapImage($agent['lat'], $agent['lng'], Yii::$app->params['mapImages']['catalog']['width'], Yii::$app->params['mapImages']['catalog']['height']);
                    echo Html::tag('div', '', ['class' => 'agent-map-img', 'style' => "background-image:url('{$image}');"]);
                } ?>
                <div class="trapezoid">
                    <?= !empty($agent['reviews']) ? '<p>' . Yii::t('agent', '{count} Reviews', ['count' => count($agent['reviews'])]) . '</p>' : ''?>
                    <?= !empty($agent['sales']) ? '<p>' . Yii::t('agent', '{count} Recent Sales', ['count' => count($agent['sales'])]) . '</p>' : ''?>
                    <?= $propertyCount ? '<p>' . Yii::t('agent', '{count, plural, one{# Listing} other{# Listings}}', ['count' => $propertyCount]) . '</p>' : ''?>
                </div>
            </div>
            <div class="agent-list-company text-right">
<!--                Coldwell Banker Residential Real Estate-->
            </div>
        </div>
    </div>
<!--    <div class="agent-list-review text-center">-->
<!--        <div class="table-review">-->
<!--            <div class="cell-review">-->
<!--                <a class="agent-list-date" href="#">-->
<!--                    Review 06/07/2018:-->
<!--                </a>-->
<!--                <p class="text-center">-->
<!--                    "My wife and I highly recommend Liza to any one who is looking to buy a home."-->
<!--                </p>-->
<!--            </div>-->
<!--        </div>-->
<!--        <a href="http://eagent.front/flats-for-sale-catalog" class="go-page"></a>-->
<!--    </div>-->
    <div class="agent-list-arrow">
        <?= Html::a('<i class="icon-next"></i>', ['/agent/agent/view', 'id' => $agent['id'], 'class' => 'text-center'])?>
    </div>
    <?= Html::a('',['/agent/agent/view', 'id' => $agent['id']], ['class' => 'go-page'])?>
</div>