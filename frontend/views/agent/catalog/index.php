<?php

use common\assets\GoogleAsset;
use frontend\widgets\AgentFilter;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\LinkPager;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\AgentAsset::register($this);
GoogleAsset::register($this);

/**
 * @var array $agents
 * @var array $agentMarkers
 * @var array $seo
 * @var array $propertyMarkers
 * @var View $this
 * @var AgentFilter $filter
 * @var array $propertyCounts
 */

?>

    <div class="page-wrap">
        <div class="agent-profile-content">
            <div class="row">
                <div class="col-md-8 col-sm-12 catalog-left">
                    <div>
                        <h1 class="text-left"><?= $seo['heading'] ?></h1>
                    </div>
                    <?= $filter->run() ?>
                    <div class="agent-list">
                        <?php foreach ($agents['items'] as $agent) {
                            echo $this->render('catalog-item', [
                                'agent' => $agent,
                                'agentMarker' => $agentMarkers[$agent['id']],
                                'propertyCount' => $propertyCounts[$agent['id']] ?? 0
                            ]);
                        } ?>
                    </div>
                    <?php if ($agents['pagination']) {
                        echo LinkPager::widget([
                            'pagination' => $agents['pagination'],
                        ]);
                    } ?>
                    <div class="col-md-12">
                        <div class="agent-list-descr">
                            <?= $seo['content'] ?? '' ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 hidden-xs hidden-sm catalog-right">
                    <div class="map-over">
                        <div id="map_catalog">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $markerUserViewUrl = Url::to(['/agent/marker/view']);
$markerUserIcon = Url::to(['/images/user-marker.png'], true);
$markerListUrl = Url::to(['/agent/marker/catalog']);

$script = <<<JS
    let map = new CatalogMap({
        mode: 'advanced',
        markerViewRoutes: {
            user: '{$markerUserViewUrl}',  
        },
        markerIcons: {
            user: '{$markerUserIcon}'
        }
    });
    map.init();
    
    map.fetchMarkers('{$markerListUrl}', {});
JS;

$this->registerJs($script);
