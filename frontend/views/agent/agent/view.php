<?php

use common\assets\GoogleAsset;
use common\models\Property;
use common\models\Translation;
use frontend\assets\AgentAsset;
use frontend\assets\CatalogAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

CatalogAsset::register($this);
AgentAsset::register($this);
GoogleAsset::register($this);

/**
 * @var View $this
 * @var array $agent
 * @var array $properties
 * @var array $soldProperties
 * @var array $reviews
 * @var array $propertyMarkers
 * @var array $userMarker
 * @var boolean $showLoginModal
 * @var \frontend\forms\contact\ContactAgentForm $contactAgentForm
 */

?>
    <div class="page-wrap">
        <div class="agent-profile-content">
            <div class="row">
                <div class="col-lg-9 col-md-12 catalog-left">
                    <div class="row">
                        <div class="col-lg-8 col-md-8">
                            <div class="agent-main-info" data-role="marker-data">
                                <div class="agent-ph ph-big">
                                    <?= Html::img($agent['avatar'], ['alt' => $agent['username'], 'title' => $agent['username']]) ?>
                                </div>
                                <div class="agent-main-right">
                                    <h1 class="text-center">
                                        <?= $agent['username'] ?>
                                        <!--                            <div class="agent-status text-center">-->
                                        <!--                                --><? //= Yii::t('main', 'Premium agent') ?>
                                        <!--                            </div>-->
                                    </h1>
                                    <div class="agent-activity">
                                        <div><?= Yii::t('agent', 'All Activity') ?></div>
                                        <div class="agent-activity-rate">
                                            <i class="icon-favorite"></i>
                                            <div class="rate-number">5
                                                <span>/5</span>
                                            </div>
                                            <?= !empty($agent['reviews']) ? Html::a(Yii::t('agent', '{count} Reviews', ['count' => count($agent['reviews'])])) : '' ?>
                                        </div>
                                        <div>
                                            <?= !empty($agent['sales']) ? Yii::t('agent', '{count} Sales Last year', ['count' => count($agent['sales'])]) : '' ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-link">
                                    <a href="#contact-us"
                                       class="btn btn-small btn-blue-white width100"><?= Yii::t('main', 'Contact us') ?></a>
                                </div>
                            </div>
                            <!--                <div class="our-members">-->
                            <!--                    <h2 class="bold-h2 text-left">Our members</h2>-->
                            <!--                    <div class="agent-members">-->
                            <!--                        <div class="agent-member text-center">-->
                            <!--                            <div class="agent-ph ph-middle">-->
                            <!--                                <img src="../images/veles.jpg" alt="agent">-->
                            <!--                            </div>-->
                            <!--                            <a class="agent-member-rate">-->
                            <!--                                <i class="icon-favorite"></i>-->
                            <!--                                5 (28 reviews)-->
                            <!--                            </a>-->
                            <!--                            <a href="#" class="agent-member-name">-->
                            <!--                                Beata Hill PA-->
                            <!--                            </a>-->
                            <!--                        </div>-->
                            <!--                        <div class="agent-member text-center">-->
                            <!--                            <div class="agent-ph ph-middle">-->
                            <!--                                <img src="../images/veles.jpg" alt="agent">-->
                            <!--                            </div>-->
                            <!--                            <a class="agent-member-rate">-->
                            <!--                                <i class="icon-favorite"></i>-->
                            <!--                                5 (4 reviews)-->
                            <!--                            </a>-->
                            <!--                            <a href="#" class="agent-member-name">-->
                            <!--                                Adam Chauser-->
                            <!--                            </a>-->
                            <!--                        </div>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <?php if (array_key_exists(Translation::KEY_SUBTITLE, $agent['translations']) || array_key_exists(Translation::KEY_DESCRIPTION, $agent['translations'])) { ?>
                                <div class="agent-description">
                                    <h2 class="bold-h2 text-left"><?= Yii::t('agent', 'About Us') ?></h2>
                                    <div class="agent-sub-title">
                                        <?= $agent['translations'][Translation::KEY_SUBTITLE] ?? '' ?>
                                    </div>
                                    <!--                    <div class="agent-specialty"> Specialties: Buyer's Agent, Listing Agent, Foreclosure, Short-Sale, Property Management, Landlord </div>-->
                                    <div class="hide-content hide-text">
                                        <div>
                                            <?= $agent['translations'][Translation::KEY_DESCRIPTION] ?? '' ?>
                                        </div>
                                    </div>
                                    <a class="more-less" data-height="120">
                                        <?= Yii::t('main', 'More') ?>
                                        <i class="icon-down"></i>
                                    </a>
                                </div>
                            <?php } ?>
                            <!--                <div class="premier-lenders">-->
                            <!--                    <h2 class="bold-h2 text-left">Premier Lenders</h2>-->
                            <!--                    <div class="tooltip-ea" data-position="right" data-width="310">-->
                            <!--                        <div class="tooltip-modal">-->
                            <!--                            <a href="" class="tooltip-close">&times;</a>-->
                            <!--                            <div class="tooltip-body">-->
                            <!--                                <p>-->
                            <!--                                    The premier lender and the agent have a financial relationship and pay to advertise on this page. Zillow and the agents do not recommend or endorse any of the premier lenders. For more information on Zillow advertising practices, see Zillow’s Terms of Use & Privacy.-->
                            <!--                                </p>-->
                            <!--                            </div>-->
                            <!--                        </div>-->
                            <!--                    </div>-->
                            <!--                    <div class="agent-block">-->
                            <!--                        <div class="agent-photo">-->
                            <!--                            <img src="../images/veles.jpg" alt="agent">-->
                            <!--                        </div>-->
                            <!--                        <div class="agent-middle">-->
                            <!--                            <div class="agent-name">-->
                            <!--                                <a href="#">Igor Shrayev</a>-->
                            <!--                            </div>-->
                            <!--                            <div class="agent-rate">-->
                            <!--                                <a href="#" class="agent-rate-alias">-->
                            <!--                                    <i class="icon-favorite"></i>-->
                            <!--                                    <i class="icon-favorite"></i>-->
                            <!--                                    <i class="icon-favorite"></i>-->
                            <!--                                    <i class="icon-favorite"></i>-->
                            <!--                                    <i class="icon-favorite"></i>-->
                            <!--                                </a>-->
                            <!--                                (<a href="#">18</a>)-->
                            <!--                            </div>-->
                            <!--                            <div class="agent-add-info">-->
                            <!--                                <div>-->
                            <!--                                    (954) 613-1367-->
                            <!--                                </div>-->
                            <!--                                <div>-->
                            <!--                                    NMLS# 379915-->
                            <!--                                </div>-->
                            <!--                                <div>-->
                            <!--                                    <a href="#">Lender Website</a>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <!--                        </div>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <div class="our-sales hide">
                                <h2 class="bold-h2 text-left"><?= Yii::t('agent', 'Our Listings & Sales') ?></h2>
                                <div class="our-sales-map">

                                </div>
                            </div>
                            <div class="homes-list" id="properties-list">
                                <?= $this->render('property-active', [
                                    'properties' => $properties
                                ]) ?>
                            </div>

                            <?php if (!empty($agent['sales'])) { ?>
                                <div class="homes-list">
                                    <h2 class="bold-h2 text-left">
                                        <?= Yii::t('agent', 'Our Past Sales') ?>
                                    </h2>
                                    <div class="homes-list-count">(0)</div>
                                    <div class="homes-table homes-sold">
                                        <div class="homes-row homes-header">
                                            <div class="homes-title">
                                                <?= Yii::t('agent', 'Property Address') ?>
                                            </div>
                                            <div class="homes-opts represented">
                                                <?= Yii::t('agent', 'Represented') ?>
                                            </div>
                                            <div class="homes-opts sold-date">
                                                <?= Yii::t('agent', 'Sold Date') ?>
                                            </div>
                                            <div class="homes-opts price">
                                                <?= Yii::t('main', 'Price') ?>
                                            </div>
                                        </div>
                                        <?php foreach ($soldProperties['items'] as $property) {
                                            echo $this->render('property-sold', ['property' => $property]);
                                        } ?>
                                    </div>
                                    <?php if ($soldProperties['pagination']) {
                                        echo LinkPager::widget(['pagination' => $soldProperties['pagination']]);
                                    } ?>
                                </div>
                            <?php } ?>

                            <?php if (!empty($agent['reviews'])) { ?>
                                <div class="reviews-list-block">
                                    <div class="flex space-between review-header">
                                        <h2 class="bold-h2 text-left">
                                            <?= Yii::t('agent', 'Ratings & Reviews') ?>
                                        </h2>
                                        <a class="btn btn-small btn-white-blue asCenter" href="#">
                                            <?= Yii::t('agent', 'Write a review') ?>
                                        </a>
                                    </div>
                                    <!--                                    <form class="row form-horizontal reviews-filter">-->
                                    <!--                                        <div class="col-md-6 col-sm-6">-->
                                    <!--                                            <div class="form-group">-->
                                    <!--                                                <label for="inputPassword3" class="col-md-3 col-sm-2 col-xs-3 control-label">Show:</label>-->
                                    <!--                                                <div class="col-md-9 col-sm-10 col-xs-9">-->
                                    <!--                                                    <select name="service" id="reviews-filters">-->
                                    <!--                                                        <option value="" selected="selected">All reviews (0)</option>-->
                                    <!--                                                        <option value="boughtHome">Helped me buy a home or lot/land (0)</option>-->
                                    <!--                                                        <option value="listedHome">Listed and sold a home or lot/land (0)</option>-->
                                    <!--                                                        <option value="showedHomes">Showed me homes or lots (0)</option>-->
                                    <!--                                                        <option value="boughtAndSoldHomes">Helped me buy and sell homes (0)</option>-->
                                    <!--                                                        <option value="helpedFindHomeToRent">Helped me find a home to rent (0)</option>-->
                                    <!--                                                        <option value="propertyManageHomeIOwn">Property manage a home I own (0)</option>-->
                                    <!--                                                        <option value="helpedFindTenantForRental">Helped me find tenant for rental (0)-->
                                    <!--                                                        </option>-->
                                    <!--                                                        <option value="consultedBuyingSelling">Consulted me on buying or selling a home-->
                                    <!--                                                            (0)-->
                                    <!--                                                        </option>-->
                                    <!--                                                    </select>-->
                                    <!--                                                </div>-->
                                    <!--                                            </div>-->
                                    <!--                                        </div>-->
                                    <!--                                        <div class="col-md-6 col-sm-6">-->
                                    <!--                                            <div class="form-group">-->
                                    <!--                                                <label for="inputPassword3" class="col-md-3 col-sm-2 col-xs-3 control-label">Sort by:</label>-->
                                    <!--                                                <div class="col-md-9 col-sm-10 col-xs-9">-->
                                    <!--                                                    <select name="sort" id="reviews-sorts">-->
                                    <!--                                                        <option value="1" selected="selected">Newest first</option>-->
                                    <!--                                                        <option value="2">Rating (high to low)</option>-->
                                    <!--                                                        <option value="3">Rating (low to high)</option>-->
                                    <!--                                                    </select>-->
                                    <!--                                                </div>-->
                                    <!--                                            </div>-->
                                    <!--                                        </div>-->
                                    <!--                                    </form>-->
                                    <div class="review-list">
                                        <?php foreach ($agent['reviews'] as $review) {
                                            echo $this->render('review-item', ['review' => $review]);
                                        } ?>
                                    </div>
                                    <!--                                    <ul class="pagination">-->
                                    <!--                                        <li class="prev"><a href="#" data-page="1">«</a></li>-->
                                    <!--                                        <li class="active"><a href="#" data-page="0">1</a></li>-->
                                    <!--                                        <li><a href="#" data-page="1">2</a></li>-->
                                    <!--                                        <li><a href="#" data-page="2">3</a></li>-->
                                    <!--                                        <li><a href="#" data-page="3">4</a></li>-->
                                    <!--                                        <li class="next"><a href="#">»</a></li>-->
                                    <!--                                    </ul>-->
                                </div>
                            <?php } ?>

                            <?php if (!empty($cities)) { ?>
                                <div class="service-areas">
                                    <h2 class="bold-h2 text-left"><?= Yii::t('agent', 'Search property in cities') ?></h2>
                                    <div class="row service-areas-row">
                                        <?php foreach ($cities as $city) { ?>
                                            <div class="col-md-6 col-sm-6">
                                                <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                                    'entity' => Yii::t('main', 'Condos'), 'city' => $city['title']
                                                ]), ['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                                <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                                    'entity' => Yii::t('main', 'Houses'), 'city' => $city['title']
                                                ]), ['/property/catalog/index', 'category' => $categorySlugs['houses'] ?? 'houses', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                                    'entity' => Yii::t('main', 'Land'), 'city' => $city['title']
                                                ]), ['/property/catalog/index', 'category' => $categorySlugs['land'] ?? 'land', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                                <?= Html::a(Yii::t('agent', '{entity} for sale in {city}', [
                                                    'entity' => Yii::t('main', 'Commercial Property'), 'city' => $city['title']
                                                ]), ['/property/catalog/index', 'category' => $categorySlugs['commercial-property'] ?? 'commercial-property', 'operation' => Property::OPERATION_SALE, 'app_city' => $city['slug']]) ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-4 col-md-4" id="contact-us">
                            <div class="aside-profile-block">
                                <h3><?= Yii::t('main', 'Contact us') ?></h3>
                                <?php $form = ActiveForm::begin([
                                    'action' => Url::to(['/contact/contact-agent']),
                                    'id' => 'bottom-contact-agent-form',
                                    'enableAjaxValidation' => true,
                                    'enableClientValidation' => false,
                                ]); ?>
                                <?= Html::hiddenInput('ContactAgentForm[agentId]', $agent['id']) ?>
                                <div>
                                    <?= $form->field($contactAgentForm, 'name')->textInput([
                                        'placeholder' => Yii::t('main', 'Your Name') . '*',
                                        'id' => 'bottom-contact-agent-your-name',
                                        'data-role' => 'name-field'
                                    ])->label(false) ?>
                                    <?= $form->field($contactAgentForm, 'phone')->textInput([
                                        'placeholder' => Yii::t('main', 'Phone'),
                                        'id' => 'bottom-contact-agent-phone',
                                    ])->label(false) ?>
                                    <?= $form->field($contactAgentForm, 'email')->textInput([
                                        'placeholder' => Yii::t('main', 'Email') . '*',
                                        'id' => 'bottom-contact-agent-email',
                                        'data-role' => 'email-field'
                                    ])->label(false) ?>
                                    <?= $form->field($contactAgentForm, 'message')->textarea([
                                        'value' => Yii::t('property', 'I am interested in {property}', ['property' => '...']),
                                        'id' => 'bottom-contact-agent-message',
                                    ])->label(false) ?>

                                    <div class="animate-input text-center">
                                        <input id="contact-agent-bottom" type="submit"
                                               class="btn btn-small btn-blue-white width100"
                                               value="<?= Yii::t('main', 'Contact Agent') ?>">
                                        <label for="contact-agent-bottom" class="animate-button">
                                            <div class="btn-wrapper">
                                                <div class="btn-original"><?= Yii::t('main', 'Contact Agent') ?></div>
                                                <div class="btn-container">
                                                    <div class="left-circle"></div>
                                                    <div class="right-circle"></div>
                                                    <div class="mask"></div>
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <?php ActiveForm::end() ?>
                            </div>
                            <div class="aside-profile-block">
                                <h3>
                                    <?= Yii::t('agent', 'Professional Information') ?>
                                </h3>
                                <div class="prof-table">
                                    <?php if ($agent['email']) { ?>
                                        <div class="prof-row">
                                            <div class="prof-property">
                                                <?= Yii::t('main', 'Email') ?>:
                                            </div>
                                            <div class="prof-descr">
                                                <?= $agent['email'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($agent['phone']) { ?>
                                        <div class="prof-row">
                                            <div class="prof-property">
                                                <?= Yii::t('main', 'Phone') ?>:
                                            </div>
                                            <div class="prof-descr">
                                                <?= $agent['phone'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (!empty($agent['address'])) { ?>
                                        <div class="prof-row">
                                            <div class="prof-property">
                                                <?= Yii::t('main', 'Address') ?>:
                                            </div>
                                            <div class="prof-descr">
                                                <?= $agent['address'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 visible-lg catalog-right">
                    <div class="map-over">
                        <div id="map_catalog">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $markerUserViewUrl = Url::to(['/agent/marker/view']);
$markerPropertyViewUrl = Url::to(['/property/marker/view']);
$markerUserIcon = Url::to(['/images/user-marker.png'], true);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$markerListUrl = Url::to(['/property/marker/account']);
$more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');
$agentId = $agent['id'];

$script = <<<JS
    let moreLess = $('.more-less');
    moreLess.MoreLess({
        moreText: '{$more}&nbsp;',
        lessText: '{$less}&nbsp;'
    });
    moreLess.trigger('show');
    
    let map = new CatalogMap({
        mode: 'advanced',
        markerViewRoutes: {
            user: '{$markerUserViewUrl}',  
            property: '{$markerPropertyViewUrl}',  
        },
        markerIcons: {
            user: '{$markerUserIcon}',
            property: '{$markerPropertyIcon}'
        }
    });
    map.init();
    map.fetchMarkers('{$markerListUrl}', {user: {$agentId}});
    
    $(document).on('click', '#properties-list [data-page]', function() { 
        $.get($(this).attr('href'), {}, function(result) {
            if(result.success === true) {
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({result: true}, 'Page', result.url);
            } 
        });
        
        return false;
    });
JS;

$this->registerJs($script);

if (Yii::$app->user->isGuest && $showLoginModal) {
    $this->registerJs("
        $('#login-modal').modal('show');
    ");
}