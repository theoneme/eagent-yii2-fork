<?php

?>
<div class="homes-row">
    <div class="homes-title">
        <div class="flex">
            <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
            <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
        </div>
    </div>
    <div class="homes-opts represented">
        Seller
    </div>
    <div class="homes-opts sold-date">
        09/21/2018
    </div>
    <div class="homes-opts price">
        $108,000
    </div>
</div>