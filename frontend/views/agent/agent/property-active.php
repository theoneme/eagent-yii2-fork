<?php

/**
 * @var array $property
 * @var array $marker
 */

use yii\helpers\Html;
use yii\widgets\LinkPager;

?>

<h2 class="bold-h2 text-left">
    <?= Yii::t('agent', 'Our active listings') ?>
</h2>
<div class="homes-list-count">(<?= $properties['pagination']->totalCount ?? 0 ?>)</div>
<div class="homes-table homes-active">
    <div class="homes-row homes-header">
        <div class="homes-title">
            <?= Yii::t('agent', 'Property Address') ?>
        </div>
        <div class="homes-opts bath">
            <?= Yii::t('agent', 'Bed / Bath') ?>
        </div>
        <div class="homes-opts price">
            <?= Yii::t('main', 'Price') ?>
        </div>
    </div>
    <?php foreach ($properties['items'] as $property) { ?>
        <div class="homes-row">
            <div class="homes-title">
                <div class="flex">
                    <div class="homes-img">
                        <?= Html::img($property['image'], ['alt' => $property['title'], 'title' => $property['title']]) ?>
                    </div>
                    <div class="home-addr">
                        <?= Html::a($property['title'], ['/property/property/show', 'slug' => $property['slug']], [
                            'data-action' => 'load-modal-property',
                            'data-hover' => 'show-marker',
                            'data-id' => $property['id'],
                            'data-type' => 'property',
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="homes-opts bath">
                <?php if (!array_key_exists('bedrooms', $property['attributes']) && !array_key_exists('bathrooms', $property['attributes'])) {
                    echo Yii::t('property', 'Not specified');
                } elseif (array_key_exists('bedrooms', $property['attributes']) && array_key_exists('bathrooms', $property['attributes'])) {
                    echo Yii::t('property', '{beds, plural, one{# bed} other{# beds}}', ['beds' => $property['attributes']['bedrooms']]) . ', ' . Yii::t('property', '{baths, plural, one{# bath} other{# bath}}', ['baths' => $property['attributes']['bathrooms']]);
                } elseif (array_key_exists('bedrooms', $property['attributes'])) {
                    echo Yii::t('property', '{beds, plural, one{# bed} other{# beds}}', ['beds' => $property['attributes']['bedrooms']]) . ', ' . Yii::t('property', 'Not specified');
                } else {
                    echo Yii::t('property', 'Not specified') . ', ', Yii::t('property', '{baths, plural, one{# bath} other{# bath}}', ['baths' => $property['attributes']['bathrooms']]);
                }
                ?>
            </div>
            <div class="homes-opts">
                <?= $property['price'] ?>
            </div>
        </div>
    <?php } ?>
</div>
<div class="flex space-between">
    <?php if ($properties['pagination']) {
        $properties['pagination']->route = '/agent/agent/view-ajax-short';

        echo LinkPager::widget(['pagination' => $properties['pagination']]);
    } ?>
</div>


