<?php

?>
<div class="review-item">
    <h3>
        <div class="agent-rate">
            <i class="icon-favorite"></i>
            <i class="icon-favorite"></i>
            <i class="icon-favorite"></i>
            <i class="icon-favorite"></i>
            <i class="icon-favorite"></i>
        </div>
        Highly likely to recommend
    </h3>
    <div class="review-text-info">
        09/07/2018 - zuser20170303100757770
    </div>
    <div class="review-text-info">
        Bought a Cooperative home in 2018 in Eastern Shores, North Miami Beach, FL.
    </div>
    <div class="review-components">
        <div class="review-component-item">
            <div class="review-component-title">
                Local knowledge:
            </div>
            <div class="agent-rate">
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
            </div>
        </div>
        <div class="review-component-item">
            <div class="review-component-title">
                Process expertise:
            </div>
            <div class="agent-rate">
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
            </div>
        </div>
        <div class="review-component-item">
            <div class="review-component-title">
                Responsiveness:
            </div>
            <div class="agent-rate">
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
            </div>
        </div>
        <div class="review-component-item">
            <div class="review-component-title">
                Negotiation skills:
            </div>
            <div class="agent-rate">
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
            </div>
        </div>
    </div>
    <div class="review-text">
        <div class="hide-content hide-text">
            <div>
                <p>Julio assisted me in the process of Selling My Home in Sunny Isles.</p>
                <p>He very well knows the current market conditions and have all the contacts needed in a Real Estate Transaction.</p>
                <p>Selling my vacation Home in Florida from Mexico City was made really easy by Julio.</p>
                <p>You can trust him 100% and will  highly recommend anyone to use his Services.</p>
                <p>Way to Go Julio !!</p>
            </div>
        </div>
        <a class="more-less" data-height="120">
            <?= Yii::t('main', 'More') ?>
            <i class="icon-down"></i>
        </a>
    </div>
</div>