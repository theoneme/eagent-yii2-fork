<?php

/* @var array $realtor */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="agent-block">
    <a href="<?= array_key_exists('id', $realtor) ? Url::to(['/agent/agent/view', 'id' => $realtor['id']]) : '#' ?>"
       class="agent-photo">
        <?= Html::img($realtor['avatar'], ['alt' => $realtor['username'], 'title' => $realtor['username']]) ?>
    </a>
    <div class="agent-middle">
        <div class="agent-name">
            <a href="<?= array_key_exists('id', $realtor) ? Url::to(['/agent/agent/view', 'id' => $realtor['id']]) : '#' ?>">
                <?= $realtor['username'] ?>
            </a>
        </div>
        <div class="agent-rate">
            <a href="<?= array_key_exists('id', $realtor) ? Url::to(['/agent/agent/view', 'id' => $realtor['id']]) : '#' ?>"
               class="agent-rate-alias">
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
                <i class="icon-favorite"></i>
            </a>
        </div>
    </div>
    <?php if (isset($realtor['premium']) && $realtor['premium'] === true) { ?>
        <div class="agent-status text-center">
            <?= Yii::t('main', 'Premium agent') ?>
        </div>
    <?php } ?>
</div>
