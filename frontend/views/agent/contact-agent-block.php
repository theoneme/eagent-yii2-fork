<?php

use frontend\forms\contact\ContactAgentForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var ContactAgentForm $contactAgentForm
 * @var array $realtors
 * @var string $url
 * @var array $title
 * @var integer $propertyId
 * @var string $blockTitle
 * @var integer $limit
 * @var array $buttonConfig
 * @var View $this
 */

$blockId = $blockId ?? 'side';
$blockSubtitle = $blockSubtitle ?? null;
$limit = $limit ?? null;

?>


<div class="contact-agent-block">
    <h2 class="up-title text-left">
        <?= $blockTitle ?? Yii::t('main', 'Do you have a questions? Contact local expert') ?>
    </h2>
    <?php if($blockSubtitle !== null) { ?>
        <h3 class="up-title text-left">
            <?= $blockSubtitle ?>
        </h3>
    <?php } ?>

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/contact/contact-agent']),
        'id' => "{$blockId}-contact-agent-form",
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => $options ?? []
    ]); ?>
    <?= $form->field($contactAgentForm, 'url')->hiddenInput([
        'id' => "{$blockId}-contact-agent-url",
        'value' => $url,
    ])->error(false)->label(false) ?>
    <?php foreach ($realtors as $id => $realtor) { ?>
        <?php if ($limit !== null && $id >= $limit) break; ?>
        <div class="chover">
            <?= Html::radio('ContactAgentForm[agentId]', 'agentId', [
                'id' => "{$blockId}-contact-agent-id-{$id}",
                'class' => 'radio-checkbox',
                'value' => $realtor['id']
            ]) ?>
            <label for="<?= $blockId ?>-contact-agent-id-<?= $id ?>">
            </label>
            <?= $this->render('@frontend/views/agent/agent-block', ['realtor' => $realtor]) ?>
        </div>
    <?php } ?>

    <?= $form->field($contactAgentForm, 'name')->textInput([
        'placeholder' => Yii::t('main', 'Your Name') . '*',
        'id' => "{$blockId}-contact-agent-your-name",
        'data-role' => 'name-field'
    ])->label(false) ?>
    <?= $form->field($contactAgentForm, 'phone')->textInput([
        'placeholder' => Yii::t('main', 'Phone'),
        'id' => "{$blockId}-contact-agent-phone",
    ])->label(false) ?>
    <?= $form->field($contactAgentForm, 'email')->textInput([
        'placeholder' => Yii::t('main', 'Email') . '*',
        'id' => "{$blockId}-contact-agent-email",
        'data-role' => 'email-field'
    ])->label(false) ?>
    <?= $form->field($contactAgentForm, 'message')->textarea([
        'value' => Yii::t('property', 'I am interested in {property}', ['property' => $title]),
        'id' => "{$blockId}-contact-agent-message",
    ])->label(false) ?>

    <div class="animate-input text-center">
        <input id="<?= $blockId ?>-contact-agent-widget" type="submit" class="btn btn-small btn-blue-white width100"
               value="<?= $buttonConfig['primaryTitle'] ?? Yii::t('property', 'Send Request') ?>">
        <label for="<?= $blockId ?>-contact-agent-widget" class="animate-button">
            <div class="btn-wrapper">
                <div class="btn-original"><?= $buttonConfig['primaryTitle'] ?? Yii::t('property', 'Send Request') ?></div>
                <div class="btn-container"
                     data-title="<?= $buttonConfig['primaryTitle'] ?? Yii::t('property', 'Send Request') ?>">
                    <div class="left-circle"></div>
                    <div class="right-circle"></div>
                    <div class="mask"></div>
                </div>
            </div>
        </label>
    </div>
    <?php ActiveForm::end() ?>
    <div class="ca-temrs">
        <?= Yii::t('property', 'By sending request you are automatically agree with {tos}', [
            'tos' => Html::a(Yii::t('property', 'Terms of Service'), '#')
        ]) ?>
    </div>
    <?php if (isset($buttonConfig['extraButtons'])) { ?>
        <div class="row wh-normal">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <?= Html::a(Yii::t('property', 'Offer your price'), ['/property/ajax/offer-price', 'entity_id' => $buttonConfig['entityId']], [
                    'class' => 'btn btn-small btn-white-blue',
                    'data-action' => 'offer-price',
                    'data-target' => '#dynamic-modal',
                    'data-toggle' => 'modal'
                ]) ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <?= Html::a(Yii::t('property', 'Request a showcase'), ['/property/ajax/request-showcase', 'entity_id' => $buttonConfig['entityId']], [
                    'class' => 'btn btn-small btn-white-blue',
                    'data-action' => 'request-showcase',
                    'data-target' => '#dynamic-modal',
                    'data-toggle' => 'modal'
                ]) ?>
            </div>
        </div>
    <?php } ?>
</div>