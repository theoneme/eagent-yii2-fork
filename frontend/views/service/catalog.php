<?php

use frontend\assets\CatalogAsset;
use frontend\assets\GoogleAsset;
use frontend\widgets\ServiceFilter;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\LinkPager;

CatalogAsset::register($this);
GoogleAsset::register($this);
\frontend\assets\AutoCompleteAsset::register($this);

/**
 * @var array $services
 * @var array $markers
 * @var View $this
 * @var ServiceFilter $filter
 */

?>
<?= $filter->run() ?>
    <div class="catalog-wrap">
        <div class="col-md-8 col-sm-12 catalog-left">
            <?php if (count($services['items']) > 0) { ?>
                <div class="table-houses">
                    <table id="list-houses">
                        <thead>
                        <tr>
<!--                            <th>-->
<!--                                <div class="chover">-->
<!--                                    <input id="allCheck" class="radio-checkbox" name="allCheck" type="checkbox">-->
<!--                                    <label for="allCheck"></label>-->
<!--                                </div>-->
<!--                            </th>-->
                            <th class="up">ID</th>
                            <th class="up"><?= Yii::t('catalog', 'Title') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Publish date') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Price') ?></th>
                            <th class="up"><?= Yii::t('catalog', 'Showcase') ?></th>
<!--                            <th class="up">--><?//= Yii::t('catalog', 'Views') ?><!--</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($services['items'] as $service) { ?>
                            <tr>
<!--                                <td>-->
<!--                                    <div class="chover">-->
<!--                                        <input id="house--><?//= $service['id'] ?><!--" class="radio-checkbox" name="id"-->
<!--                                               value="--><?//= $service['id'] ?><!--"-->
<!--                                               type="checkbox">-->
<!--                                        <label for="house--><?//= $service['id'] ?><!--"></label>-->
<!--                                    </div>-->
<!--                                </td>-->
                                <td>
                                    <?= $service['id'] ?>
                                </td>
                                <td><?= $service['title'] ?></td>
                                <td><?= Yii::$app->formatter->asDatetime($service['created_at']) ?> </td>
                                <td><?= Yii::t('main', 'from {price}', ['price' => $service['price']]) ?> </td>
                                <td>
                                    <?= Html::a(Yii::t('catalog', 'See more'), $service['url'], ['target' => '_blank'])?>
                                </td>
<!--                                <td>0 </td>-->
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <?php if ($services['pagination']) { ?>
                    <?= LinkPager::widget([
                        'pagination' => $services['pagination'],
                    ]); ?>
                <?php } ?>
            <?php } else { ?>
                <p>
                    <?= Yii::t('main', 'No results found') ?>
                </p>
            <?php } ?>

        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    // $('body').ListingMap();

    /* List houses */
    $(document.body).on('click','#list-houses th',function() {
        if($(this).hasClass('active')){  
            $(this).toggleClass('up');
            $(this).toggleClass('down');
        } else {
            $('#list-houses th').removeClass('active');
            $(this).addClass('active');
        }
    });
    
    // $(document).on('change', '#list-houses input[type="checkbox"]', function() {
    //     $(this).closest('tr').toggleClass('check');
    // });
JS;

$this->registerJs($script);
