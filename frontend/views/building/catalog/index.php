<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 11:12
 */

use common\assets\GoogleAsset;
use frontend\assets\CatalogAsset;
use frontend\assets\FilterAsset;
use frontend\assets\plugins\AutoCompleteAsset;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $buildings
 * @var array $seo
 * @var array $sortData
 * @var View $this
 * @var \frontend\widgets\BuildingFilter $buildingFilter
 * @var string $lat
 * @var string $lng
 */

CatalogAsset::register($this);
GoogleAsset::register($this);
AutoCompleteAsset::register($this);
FilterAsset::register($this);

?>
    <div id="filter-component">
        <?= $buildingFilter->run() ?>
    </div>
    <div class="catalog-wrap">
        <div class="col-md-8 col-sm-12 catalog-left" id="properties-list">
            <?= $this->render('partial/grid', [
                'seo' => $seo,
                'buildings' => $buildings
            ]) ?>
        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?php $baseUrl = Url::to(['/building/catalog/index']);
$catalogViewUrl = Url::to(['/building/catalog/view']);
$markerListUrl = Url::to(['/building/marker/catalog']);
$markerViewUrl = Url::to(['/building/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$complexUrl = Url::to(['/building/catalog/index-ajax']);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);
$polygon = json_encode($polygon);

$script = <<<JS
    let meta = {
        location: window.location.href,
        title: $('title'),
        description: $('meta[name=description]'),
        keywords: $('meta[name=keywords]')
    };

    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        mode: 'advanced',
        markerViewRoutes: {
            building: '{$markerViewUrl}',  
        },
        markerIcons: {
            building: '{$markerPropertyIcon}'
        },
        polygon: {$polygon},
    });
    map.init();
    let params = filter.buildQuery();
    
    map.fetchMarkers('{$markerListUrl}', params);
    
    $(document).on('click', '#properties-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if(result.success === true) {
                map.clearMarkerCache();
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({result: true}, 'Page', result.url);
            } 
        });
        
        return false;
    });
    
    let fn = function(result) {
        if (result.domain === false) {
            map.setMarkers(result.markers);
            map.setPolygon(result.polygon);
            $('#properties-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $(document).on('submit', filter.options.filterSelector, function() {
         let params = filter.buildQuery();
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
         let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $(document).on('mouseenter', '.sticker', function() {
        if (!$(this).hasClass('active')) {
            $(this).siblings().removeClass('active');
        }
        $(this).addClass('active');
    })
JS;
$this->registerJs($script);