<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use frontend\widgets\PropertyFilter;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\frontend\assets\FilterAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\assets\plugins\FancyBoxAsset::register($this);

/**
 * @var array $buildings
 * @var View $this
 * @var PropertyFilter $buildingFilter
 * @var string $lat
 * @var string $lng
 * @var array $seo
 * @var array $sortData
 */

?>

    <div id="filter-component">
        <?= $buildingFilter->run() ?>
    </div>
    <div class="catalog-wrap">
        <div class="col-md-8 col-sm-12 catalog-left" id="properties-list">
            <?= $this->render('partial/list', [
                'seo' => $seo,
                'buildings' => $buildings
            ]) ?>
        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?= \frontend\widgets\ReportWidget::widget() ?>
<?php $baseUrl = Url::to(['/building/catalog/search']);
$catalogViewUrl = Url::to(['/building/catalog/view']);
$markerListUrl = Url::to(['/building/marker/catalog']);
$markerViewUrl = Url::to(['/building/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$complexUrl = Url::to(['/building/catalog/index-ajax']);
$locationUrl = Url::to(['/location/ajax/locations']);
$cityPartsUrl = Url::to(['/location/ajax/city-parts']);
$polygon = json_encode($polygon);

$script = <<<JS
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false
    });

    $(document.body).on('click','#list-houses th',function() {
        if($(this).hasClass('active')){  
            $(this).toggleClass('up');
            $(this).toggleClass('down');
        } else {
            $('#list-houses th').removeClass('active');
            $(this).addClass('active');
        }
    });
    
    $(document).on('change', '#list-houses input[type="checkbox"]', function() {
        $(this).closest('tr').toggleClass('check');
    });
    
    let filter = new Filter({
        locationUrl: '{$locationUrl}',
        canonicalUrl: '{$baseUrl}',
        changeViewUrl: '{$catalogViewUrl}',
        cityPartsUrl: '{$cityPartsUrl}',
    });
    filter.registerHandlers();
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        mode: 'advanced',
        markerViewRoutes: {
            building: '{$markerViewUrl}',  
        },
        markerIcons: {
            building: '{$markerPropertyIcon}'
        },
        polygon: {$polygon},
    });
    map.init();
    let params = filter.buildQuery();
    
    map.fetchMarkers('{$markerListUrl}', params);
    
    $(document).on('click', '#properties-list [data-page]', function() {
        $.get($(this).attr('href'), {}, function(result) {
            if(result.success === true) {
                map.clearMarkerCache();
                $('#properties-list').html(result.catalog);
                window.scrollTo(0, 0);
                history.pushState({result: true}, 'Page', result.url);
            } 
        });
        
        return false;
    });
    
    let fn = function(result) {
        if (result.domain === false) {
            map.setMarkers(result.markers);
            map.setPolygon(result.polygon);
            $('#properties-list').html(result.catalog);
            $('#filter-component').html(result.filter);
            filter.registerHandlers();
    
            history.pushState({
                result: true
            }, result.seo.title, result.url);
            if (result.seo.title) {
                meta.title.html(result.seo.title);
            }
    
            if (result.seo.description) {
                meta.description.attr('content', result.seo.description);
            }
    
            if (result.seo.keywords) {
                meta.keywords.attr('content', result.seo.keywords);
            }
        } else {
            window.location.href = result.url;
        }
    };
    
    $(document).on('submit', filter.options.filterSelector, function() {
         let params = filter.buildQuery();
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
    
    $(document).on('click', '[data-action=reset-filter]', function() {
         let params = filter.buildQuery(['category', 'operation', 'box', 'zoom']);
         $('.loading-overlay').removeClass('hidden');
         $.get('{$complexUrl}', params, function(result) { 
             if(result.success === true) {
                fn(result);
             }
         });
         
         return false;
    });
JS;

$this->registerJs($script);

