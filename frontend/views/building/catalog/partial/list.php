<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var array $sortData
 * @var array $seo
 * @var array $buildings
 * @var integer $gridSize
 */

?>

    <div class="loading-overlay hidden"></div>

    <div>
        <h1 class="text-left"><?= $seo['heading'] ?></h1>
    </div>
<?php if (count($buildings['items']) > 0) { ?>
    <div class="table-houses">
        <table id="list-houses">
            <thead>
            <tr>
                <th>
                    <div class="chover">
                        <input id="allCheck" class="radio-checkbox" name="allCheck" type="checkbox">
                        <label for="allCheck"></label>
                    </div>
                </th>
                <th class="up">ID</th>
                <th class="up"><?= Yii::t('catalog', 'Title') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Photos') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Year Built') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($buildings['items'] as $building) { ?>
                <tr>
                    <td>
                        <div class="chover">
                            <input id="house<?= $building['id'] ?>" class="radio-checkbox" name="id[]"
                                   value="<?= $building['id'] ?>"
                                   type="checkbox"
                                   data-role="report-item">
                            <label for="house<?= $building['id'] ?>"></label>
                        </div>
                    </td>
                    <td><?= $building['id'] ?></td>
                    <td><a href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']]) ?>"
                           data-hover="show-marker"
                           data-id="<?= $building['id']?>"
                           data-type="building"
                           data-action="load-modal-property"><?= $building['title'] ?></a></td>
                    <td>
                        <?php if (count($building['images']) > 0) { ?>
                            <?php foreach ($building['images'] as $key => $image) { ?>
                                <?php if ($key === 0) { ?>
                                    <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($building['images']), $image, [
                                        'data-fancybox' => "gallery-{$building['id']}",
                                        'data-role' => 'fancy-gallery-item'
                                    ]) ?>
                                <?php } else { ?>
                                    <?= Html::a(null, $image, [
                                        'data-fancybox' => "gallery-{$building['id']}",
                                        'class' => 'hidden',
                                        'data-role' => 'fancy-gallery-item'
                                    ]) ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } else { ?>
                            <i class="icon-camera"></i>&nbsp;<?= count($building['images']) ?>
                        <?php } ?>
                    </td>
                    <td><?= $building['attributes']['year_built'] ?? '-' ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <?php if ($buildings['pagination']) {
        $buildings['pagination']->route = '/building/catalog/index-ajax-short';

        echo LinkPager::widget([
            'pagination' => $buildings['pagination'],
            'linkOptions' => ['data-action' => 'switch-page'],
        ]); ?>
    <?php } ?>
<?php } else { ?>
    <p><?= Yii::t('main', 'No results found') ?></p>
<?php } ?>