<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/**
 * @var array $seo
 * @var array $buildings
 * @var integer $gridSize
 */

?>

    <div class="loading-overlay hidden"></div>

    <div>
        <h1 class="text-left"><?= $seo['heading'] ?></h1>
    </div>
<?php if (count($buildings['items']) > 0) { ?>
    <div class="row home-list">
        <?php foreach ($buildings['items'] as $building) { ?>
            <div class="col-md-4 col-sm-6 col-xs-6 home-item">
                <div class="building-stickers">
                    <?php if (isset($building['attributes']['new_construction_discount'])) { ?>
                        <div class="sticker purple active">
                            <div class="stick-img">
                                <?= Html::img('/images/landing/percent.png') ?>
                            </div>
                            <div class="stick-text">
                                <?= Yii::t('landing', 'Discount {percent}%', [
                                    'percent' => $building['attributes']['new_construction_discount']
                                ]) ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (isset($building['attributes']['new_construction_mortgage'])) { ?>
                        <div class="sticker green">
                            <div class="stick-img">
                                <?= Html::img('/images/landing/mortgage.png') ?>
                            </div>
                            <div class="stick-text">
                                <?= Yii::t('landing', 'Mortgage {percent}%', [
                                    'percent' => $building['attributes']['new_construction_mortgage']
                                ]) ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (isset($building['attributes']['new_construction_installment'])) { ?>
                        <div class="sticker yellow">
                            <div class="stick-img">
                                <?= Html::img('/images/landing/discount.png') ?>
                            </div>
                            <div class="stick-text">
                                <?= $building['attributes']['new_construction_installment'] ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <a class="home-item-inner"
                   href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']]) ?>"
                   data-hover="show-marker"
                   data-id="<?= $building['id']?>"
                   data-type="building"
                   data-action="load-modal-property">
                    <div class="home-img">
                        <?= Html::img($building['image'], ['alt' => $building['title'], 'title' => $building['title']]) ?>
                    </div>
                    <div class="home-wish"></div>

                    <div class="home-info">
                        <h2 class="home-title">
                            <?= $building['title'] ?>
                        </h2>
                        <div class="home-params">
                            <?php $shown = 0;
                            foreach ($building['attributes'] as $key => $attribute) { ?>
                                <?php if ($key === 'year_built') {
                                    $shown++; ?>
                                    <?= $attribute ?>
                                    <span class="dot">·</span>
                                <?php } ?>
                                <?php if ($key === 'ready_year') {
                                    $shown++; ?>
                                    <?= $attribute ?>
                                    <span class="dot">·</span>
                                <?php } ?>
                                <?php if ($key === 'ready_quarter') {
                                    $shown++; ?>
                                    <?= Yii::t('catalog', '{quarter} quarter', ['quarter' => $attribute]) ?>
                                    <span class="dot">·</span>
                                <?php } ?>
                                <?php if ($shown === 3) break;
                            } ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>
    <?php
    if ($buildings['pagination']) {
        $buildings['pagination']->route = '/building/catalog/index-ajax-short';

        echo LinkPager::widget([
            'pagination' => $buildings['pagination'],
            'linkOptions' => ['data-action' => 'switch-page'],
        ]);
    }
} else { ?>
    <div class="home-list">
        <p>
            <?= !empty($seo['emptyText']) ? $seo['emptyText'] : Yii::t('main', 'No results found'); ?>
        </p>
    </div>
    <div class="text-center margin-bottom30">
        <?php if (Yii::$app->user->isGuest) {
            echo Html::a(Yii::t('main', 'Post for free'), null, ['data-toggle' => 'modal', 'data-target' => '#signup-modal', 'class' => 'btn btn-big btn-white-blue']);
        } else {
            echo Html::a(Yii::t('main', 'Post for free'), ['/building/manage/create'], ['class' => 'btn btn-big btn-white-blue']);
        } ?>
    </div>
<?php } ?>