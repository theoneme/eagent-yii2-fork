<?php

use common\decorators\BuildingSiteTransportDecorator;
use common\decorators\BuildingSiteTypeDecorator;
use common\helpers\DataHelper;
use common\models\Building;
use common\models\BuildingSite;
use frontend\assets\PropertyModalAsset;
use frontend\forms\contact\ContactAgentForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var array $building
 * @var array $counts
 * @var array $seo
 * @var array $realtors
 * @var array $similar
 * @var View $this
 * @var ContactAgentForm $contactAgentForm
 */

PropertyModalAsset::register($this);

?>

    <div class="home-modal-top">
        <div></div>
        <div>
            <button type="button" class="close-modal-home" data-dismiss="modal" aria-hidden="true">
                <span>×</span> <span class="menu-item-text">
                <?= Yii::t('main', 'Close') ?>
            </span>
            </button>
        </div>
    </div>
    <div class="modal-body">
        <div class="flex">
            <div class="home-address text-left">
                <div>
                    <ul>
                        <li>ID: <?= $building['id'] ?></li>
                    </ul>
                </div>
            </div>
            <div class="home-address flex-grow text-right">
                <div>
                    <ul>
                        <li><?= $building['address'] ?></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php if (!empty($building['images'])) { ?>
            <div class="modal-slider" data-role="main-slider">
                <div class="modal-slide ms-one">
                    <?= Html::a(Html::img(array_shift($building['thumbnails']), [
                        'alt' => $seo['keywords'],
                        'title' => $seo['keywords']
                    ]), array_shift($building['images']), [
                        'data-fancybox' => 'gallery'
                    ]) ?>
                </div>
                <div class="modal-slide">
                    <?php foreach ($building['images'] as $key => $image) { ?>
                        <?php if ($key % 2 === 0 && $key > 0) { ?>
                            <?= '</div><div class="modal-slide">' ?>
                        <?php } ?>
                        <?= Html::a(Html::img($building['thumbnails'][$key], [
                            'alt' => $seo['keywords'],
                            'title' => $seo['keywords']
                        ]), $image, [
                            'data-fancybox' => 'gallery'
                        ]) ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <div class="modal-info">
            <div class="modal-cols row">
                <div class="col-md-8 col-sm-12">
                    <div>
                        <!--                            <div class="building-mark">Building</div>-->
                        <h1 class="text-left" id="fancy-title">
                            <?= $seo['title'] ?>
                        </h1>
                        <!--                            <div class="building-address">Sunny Isles Beach, FL 33160</div>-->
                        <?php if (!empty($building['description'])) { ?>
                            <div class="modal-home-descr">
                                <div class="hide-content hide-text">
                                    <p>
                                        <?= $building['description'] ?>
                                    </p>
                                </div>
                                <a class="more-less" data-height="200">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <?php if (!empty($building['attributes'])) { ?>
                                <div>
                                    <h4 class="up-title text-left">
                                        <?= Yii::t('building', 'Building Parameters') ?>
                                    </h4>
                                    <div class="modal-info-block-content">
                                        <?php foreach ($building['attributes'] as $attribute) { ?>
                                            <div class="info-block-item">
                                                <?= "{$attribute['title']}: {$attribute['value']}" ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (isset($building['specialAttributes']['building_amenitie'])) { ?>
                                <div>
                                    <h4 class="up-title text-left">
                                        <?= Yii::t('building', 'Building Amenities') ?>
                                    </h4>
                                    <div class="modal-info-block-content">
                                        <?php foreach ($building['specialAttributes']['building_amenitie'] as $attribute) { ?>
                                            <div class="info-block-item">
                                                <i class="icon-tick text-success"></i>&nbsp;<?= $attribute ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                        <?php if(count($counts) > 0) { ?>
                            <h3>
                                <?= Yii::t('building', 'Condos in house{title}{address}', [
                                    'address' => !empty($building['address']) ? Yii::t('seo', ' at {address}', ['address' => $building['address']]) : '',
                                    'title' => !empty($building['translations']['name']) ? Yii::t('seo', ' «{title}»', ['title' => $building['translations']['name']]) : '',
                                ]) ?>
                            </h3>
                            <div class="bedrooms-block">
                                <ul class="no-list history-switch no-margin">
                                    <?php
                                    $active = true;
                                    if (array_key_exists('sale', $counts)) {
                                        echo Html::tag('li',
                                            Html::a(
                                                '<div class="count-block text-center cb-green">' . $counts['sale']['count'] . '</div> ' . Yii::t('building', 'For Sale'),
                                                '#sale',
                                                ['data-toggle' => 'tab']
                                            ),
                                            ['class' => $active ? 'active' : '']
                                        );
                                        $active = false;
                                    }
                                    if (array_key_exists('rent', $counts)) {
                                        echo Html::tag('li',
                                            Html::a(
                                                '<div class="count-block text-center cb-green">' . $counts['rent']['count'] . '</div> ' . Yii::t('building', 'For Rent'),
                                                '#rent',
                                                ['data-toggle' => 'tab']
                                            ),
                                            ['class' => $active ? 'active' : '']
                                        );
                                        $active = false;
                                    }
                                    if (array_key_exists('off', $counts)) {
                                        echo Html::tag('li',
                                            Html::a(
                                                '<div class="count-block text-center cb-green">' . $counts['off']['count'] . '</div> ' . Yii::t('building', 'Unavailable'),
                                                '#off',
                                                ['data-toggle' => 'tab']
                                            ),
                                            ['class' => $active ? 'active' : '']
                                        );
                                    } ?>
                                </ul>
                                <div class="tab-content tab-history">
                                    <?php
                                    $active = true;
                                    if (array_key_exists('sale', $counts)) { ?>
                                        <div class="history-content tab-pane fade in <?= $active ? 'active' : '' ?>"
                                             id="sale">
                                            <?php foreach ($counts['sale']['items'] as $rooms => $data) {
                                                echo $this->render('room-group', ['tab' => 'sale', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                                            } ?>
                                        </div>
                                        <?php $active = false;
                                    }
                                    if (array_key_exists('rent', $counts)) { ?>
                                        <div class="history-content tab-pane fade in <?= $active ? 'active' : '' ?>"
                                             id="rent">
                                            <?php foreach ($counts['rent']['items'] as $rooms => $data) {
                                                echo $this->render('room-group', ['tab' => 'rent', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                                            } ?>
                                        </div>
                                        <?php $active = false;
                                    }
                                    if (array_key_exists('off', $counts)) { ?>
                                        <div class="history-content tab-pane fade in <?= $active ? 'active' : '' ?>"
                                             id="off">
                                            <?php foreach ($counts['off']['items'] as $rooms => $data) {
                                                echo $this->render('room-group', ['tab' => 'off', 'rooms' => $rooms, 'data' => $data, 'buildingId' => $building['id']]);
                                            } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <?php if ($building['lat'] && $building['lng']) { ?>
                        <section class="params-block">
                            <div class="params-header" data-toggle="collapse" data-target="#school"
                                 data-action="init-gmap">
                                <h2 class="text-left"><?= Yii::t('property', 'Building on map') ?></h2>
                            </div>
                            <div class="collapse" id="school">
                                <div id="modal-map" class="modal-property-map"
                                     data-lat="<?= $building['lat'] ?>"
                                     data-lng="<?= $building['lng'] ?>" data-type="property">
                                </div>
                            </div>
                        </section>
                    <?php } ?>
                    <?php if (!empty($building['sites'])) { ?>
                        <section class="params-block">
                            <div class="params-header" data-toggle="collapse" data-target="#sites">
                                <h2 class="text-left"><?= Yii::t('building', 'Location') ?></h2>
                            </div>
                            <div class="collapse" id="sites">
                                <div class="params-content">
                                    <?php foreach ($building['sites'] as $site) { ?>
                                        <div class="building-site-item">
                                            <?= Html::img('/images/building-site/' . BuildingSiteTypeDecorator::getIcon($site['type'])) ?>
                                            <div class="building-site-text">
                                                <span class="building-site-name">
                                                    <?= Yii::t('building', '{type} {name}', [
                                                        'type' => BuildingSiteTypeDecorator::decorate($site['type']),
                                                        'name' => $site['type'] === BuildingSite::TYPE_CITY_CENTER ? '' : "\"{$site['name']}\"",
                                                    ]) ?>
                                                </span>
                                                <br>
                                                <?= Yii::t('building', '{time, plural, one{# minute} other{# minutes}} {transport}', [
                                                    'time' => $site['time'],
                                                    'transport' => BuildingSiteTransportDecorator::decorate($site['transport']),
                                                ]) ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </section>
                    <?php } ?>
                    <?php if (!empty($building['progress'])) {
                        $progressData = DataHelper::getBuildingProgressData($building['progress']);
                        ?>
                        <section class="params-block">
                            <div class="params-header" data-toggle="collapse" data-target="#progress">
                                <h2 class="text-left"><?= Yii::t('building', 'Construction Progress') ?></h2>
                            </div>
                            <div class="collapse" id="progress">
                                <div class="params-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= Html::dropDownList('progress_year', $progressData['firstYear'], $progressData['years'], ['class' => 'progress-year-select']) ?>
                                            <?= Html::dropDownList('progress_quarter', $progressData['firstQuarter'], $progressData['quarters'], ['class' => 'progress-quarter-select']) ?>
                                        </div>
                                        <div class="col-md-12">
                                            <?php foreach ($building['progress'] as $progressItem) { ?>
                                                <div class="building-progress-item clearfix"
                                                     data-id="<?= $progressItem['id'] ?>">
                                                    <?php foreach ($progressItem['images'] as $pkey => $image) {
                                                        echo Html::a(
                                                            Html::img($progressItem['thumbnails'][$pkey]),
                                                            $image,
                                                            [
                                                                'class' => 'col-md-4 col-xs-6 building-progress-fancybox',
                                                                'data-fancybox' => "progress-{$progressItem['year']}-{$progressItem['quarter']}"
                                                            ]
                                                        );
                                                    } ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php } ?>
                    <?php if (!empty($building['documents'])) { ?>
                        <section class="params-block">
                            <div class="params-header" data-toggle="collapse" data-target="#documents">
                                <h2 class="text-left"><?= Yii::t('building', 'Documentation') ?></h2>
                            </div>
                            <div class="collapse" id="documents">
                                <div class="params-content">
                                    <!--                                        <div class="hide-content hide-text">-->
                                    <?php foreach ($building['documents'] as $document) { ?>
                                        <div class="building-document-item">
                                            <?= Html::a($document['name'], $document['file'], ['target' => '_blank', 'download' => true]) ?>
                                        </div>
                                    <?php } ?>
                                    <!--                                        </div>-->
                                    <!--                                        <a class="more-less" data-height="300">-->
                                    <!--                                            --><? //= Yii::t('main', 'More') ?>
                                    <!--                                            <i class="icon-down"></i>-->
                                    <!--                                        </a>-->
                                </div>
                            </div>
                        </section>
                    <?php } ?>

                    <?php if (!empty($building['video'])) { ?>
                        <section class="params-block">
                            <div class="params-header" data-toggle="collapse" data-target="#video"
                                 data-action="init-video">
                                <h2 class="text-left">
                                    <?= Yii::t('property', 'Video tour') ?>
                                </h2>
                            </div>
                            <div class="collapse" id="video">
                                <div class="prop-video position-relative">
                                    <iframe height=480 class="w-100" src=''
                                            data-src="<?= $building['video']['content'] ?>">
                                    </iframe>
                                </div>
                            </div>
                        </section>
                    <?php } ?>

                    <div class="contact-agent-block">
                        <h2 class="up-title text-left">
                            <?= Yii::t('main', 'Contact local expert') ?>
                        </h2>
                        <?php $form = ActiveForm::begin([
                            'action' => Url::to(['/contact/contact-agent']),
                            'id' => 'bottom-contact-agent-form',
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => false,
                        ]); ?>
                        <?= $form->field($contactAgentForm, 'url')->hiddenInput([
                            'id' => 'bottom-contact-agent-slug',
                            'value' => $building['slug'],
                        ])->error(false)->label(false) ?>
                        <div class="row">
                            <div class="col-md-6">
                                <?php foreach ($realtors as $id => $realtor) { ?>
                                    <div class="chover">
                                        <?= Html::radio('ContactAgentForm[agentId]', 'agentId', [
                                            'id' => "bottom-contact-agent-id-{$id}",
                                            'class' => 'radio-checkbox',
                                            'value' => $realtor['id']
                                        ]) ?>
                                        <label for="bottom-contact-agent-id-<?= $id ?>">
                                        </label>
                                        <?= $this->render('@frontend/views/agent/agent-block', ['realtor' => $realtor]) ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($contactAgentForm, 'name')->textInput([
                                    'placeholder' => Yii::t('main', 'Your Name') . '*',
                                    'id' => 'bottom-contact-agent-your-name',
                                    'data-role' => 'name-field'
                                ])->label(false) ?>
                                <?= $form->field($contactAgentForm, 'phone')->textInput([
                                    'placeholder' => Yii::t('main', 'Phone'),
                                    'id' => 'bottom-contact-agent-phone',
                                ])->label(false) ?>
                                <?= $form->field($contactAgentForm, 'email')->textInput([
                                    'placeholder' => Yii::t('main', 'Email') . '*',
                                    'id' => 'bottom-contact-agent-email',
                                    'data-role' => 'email-field'
                                ])->label(false) ?>
                                <?= $form->field($contactAgentForm, 'message')->textarea([
                                    'value' => Yii::t('building', 'I am interested in property in building {building}', ['building' => $building['title']]),
                                    'id' => 'bottom-contact-agent-message',
                                ])->label(false) ?>

                                <div class="animate-input text-center">
                                    <input id="contact-agent-bottom" type="submit"
                                           class="btn btn-small btn-blue-white width100"
                                           value="<?= Yii::t('main', 'Contact Agent') ?>">
                                    <label for="contact-agent-bottom" class="animate-button">
                                        <div class="btn-wrapper">
                                            <div class="btn-original"><?= Yii::t('main', 'Contact Agent') ?></div>
                                            <div class="btn-container">
                                                <div class="left-circle"></div>
                                                <div class="right-circle"></div>
                                                <div class="mask"></div>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end() ?>
                        <div class="ca-temrs">
                            <?= Yii::t('property', 'By sending request you are automatically agree with {tos}', [
                                'tos' => Html::a(Yii::t('property', 'Terms of Service'), '#')
                            ]) ?>
                        </div>
                    </div>
                    <?php if (!empty($similar)) { ?>
                        <div class="home-similar">
                            <h2 class="modal-h2">
                                <?= Yii::t('building', 'Similar Buildings') ?>
                            </h2>
                            <div class="similar-slider" data-role="similar-slider">
                                <?php foreach ($similar as $similarItem) {
                                    echo $this->render('similar', ['building' => $similarItem]);
                                } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-4 col-sm-12">
                    <?= $this->render('@frontend/views/agent/contact-agent-block', [
                        'blockTitle' => $building['type'] === \common\models\Building::TYPE_NEW_CONSTRUCTION
                            ? Yii::t('main', 'Special proposition is available. Fill your contacts and we will send you details')
                            : Yii::t('main', 'Do you have a questions? Contact us'),
                        'blockSubtitle' => isset($building['specialAttributes']['new_construction_builder'])
                            ? Yii::t('property', 'Developer: {developer}', [
                                'developer' => $building['specialAttributes']['new_construction_builder']
                            ])
                            : null,
                        'contactAgentForm' => $contactAgentForm,
                        'realtors' => $realtors,
                        'slug' => $building['slug'],
                        'url' => Url::to(['/building/building/show', 'slug' => $building['slug']], true),
                        'title' => $building['title'],
                        'limit' => 0,
                        'buttonConfig' => [
                            'primaryTitle' => $building['type'] === \common\models\Building::TYPE_NEW_CONSTRUCTION
                                ? Yii::t('property', 'Get Special Offer')
                                : Yii::t('property', 'Send Request')
                        ]
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="modal-home-footer">
            <div class="nearby-cols row">
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Nearby Cities') ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li><a href="#"><?= Yii::t('property', 'Moscow') ?></a></li>
                        <li><a href="#"><?= Yii::t('property', 'St. Peterburg') ?></a></li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
                <!--                <div class="col-md-4">-->
                <!--                    <div class="nearby-title">-->
                <!--                        --><? //= Yii::t('property', 'Nearby Neighborhoods') ?>
                <!--                    </div>-->
                <!--                    <ul class="no-list ul-nearby">-->
                <!--                        <li><a href="#">Avalon Harbor</a></li>-->
                <!--                        <li><a href="#">Beach</a></li>-->
                <!--                        <li><a href="#">Collier City</a></li>-->
                <!--                        <li><a href="#">Cresthaven</a></li>-->
                <!--                        <li><a href="#">Garden Isles</a></li>-->
                <!--                    </ul>-->
                <!--                    --><? //= Html::a(Yii::t('main', 'More')) ?>
                <!--                </div>-->
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Nearby Zip Codes') ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li><a href="#">33060</a></li>
                        <li><a href="#">33062</a></li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 nearby-col">
                    <div class="nearby-title">
                        <?= Yii::t('property', 'Other topics in {city}', ['city' => 'Екатеринбурге']) ?>
                    </div>
                    <ul class="no-list ul-nearby">
                        <li>
                            <a href="#"><?= Yii::t('property', 'Properties for sale in {index}', ['index' => 33062]) ?></a>
                        </li>
                        <li><a href="#"><?= Yii::t('property', 'Houses for sale in {index}', ['index' => 33062]) ?></a>
                        </li>
                    </ul>
                    <?= Html::a(Yii::t('main', 'More')) ?>
                </div>
            </div>
            <div class="home-footer-info">
                <!--                <p>-->
                <!--                    SEO TEXT TEMPLATE-->
                <!--                </p>-->
                <!--                <p>-->
                <!--                    SEO TEXT TEMPLATE-->
                <!--                </p>-->
            </div>
        </div>
    </div>

    <div id="fancy-caption-container"
         class="hidden <?= empty($building['attributes']) ? 'empty-fancy-container' : '' ?>">
        <div>
            <?php if (!empty($building['attributes'])) { ?>
                <h4 class="up-title text-left">
                    <?= Yii::t('building', 'Building Parameters') ?>
                </h4>
                <ul class="no-list fancybox-attributes">
                    <?php foreach ($building['attributes'] as $attribute) { ?>
                        <li>
                            <?= "{$attribute['title']}: {$attribute['value']}" ?>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>

            <?= $this->render('@frontend/views/agent/contact-agent-block', [
                'blockTitle' => $building['type'] === \common\models\Building::TYPE_NEW_CONSTRUCTION
                    ? Yii::t('main', 'Special proposition is available. Selected agent will contact you:')
                    : null,
                'contactAgentForm' => $contactAgentForm,
                'realtors' => array_slice($realtors, 0, 1),
                'url' => Url::to(['/building/building/show', 'slug' => $building['slug']], true),
                'title' => $building['title'],
                'blockId' => 'fancy'
            ]) ?>
        </div>
    </div>

<?php $more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');
$propertiesUrl = Url::to(['/property/ajax/building-properties']);

$script = <<<JS
    var moreLess = $('.more-less');
    
    moreLess.MoreLess({
        moreText: '{$more}&nbsp;',
        lessText: '{$less}&nbsp;'
    }); 
    
    var tabFn = function() {
        $('.bedroom-group [data-page]').off('click').on('click', function() {
            let container = $(this).closest('.bedroom-group').find('[data-role=properties-container]'),
                href = $(this).attr('href');
            
            $.get(href, {}, function(result) {
                if(result.success === true) {
                    container.data('loaded', 1);
                    container.html(result.html);
                    tabFn();
                } 
            });
            
            return false;
        });
    };
    
    $('[data-action=load-properties]').on('click', function() {
        let buildingId = $(this).data('building'),
            operation = $(this).data('operation'),
            rooms = $(this).data('rooms'),
            that = $(this),
            container = that.closest('.bedroom-group').find('[data-role=properties-container]');
        
        if(container.data('loaded') !== 1) {
            $.get('{$propertiesUrl}', {operation: operation, building_id: buildingId, rooms: rooms}, function(result) {
                if(result.success === true) {
                    container.data('loaded', 1);
                    container.html(result.html);
                    tabFn();
                } 
            });
        }
    });    
    
    $('[data-action=init-video]').on('click', function() {
        let iframe = $(this).closest('.params-block').find('iframe');
        iframe.attr('src', iframe.data('src'));
    });
    
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false,
        infobar: false,
        buttons: [
            'close'
        ],
    });
    $('.building-progress-fancybox').fancybox({
        hash: false,
        // infobar: false,
        // buttons: [
        //     'close'
        // ],
    });
JS;

$this->registerJs($script);

if ($building['lat'] && $building['lng']) {
    $markerPropertyIcon = Url::to(['/images/marker.png'], true);
    $script = <<<JS
        var mapInitialized = false;
        
        $('[data-action=init-gmap]').on('click', function() {
            if(mapInitialized === false) {
                var modalMap = new ModalMap({
                    markerIcons: {
                        property: '{$markerPropertyIcon}'
                    }
                });
                modalMap.init();
                modalMap.setMarker();
                
                mapInitialized = true;
            } 
        });
JS;
    $this->registerJs($script);
}

if (!empty($building['progress'])) {
    $progressPeriods = Json::encode($progressData['periods']);
    $script = <<<JS
        let progressPeriods = $progressPeriods;
        $('.progress-quarter-select').on('change', function() {
            let year = $('.progress-year-select').val();
            let quarter = $(this).val();
            if (progressPeriods[year][quarter]) {
                $('.building-progress-item').removeClass('active');
                $('.building-progress-item[data-id=' + progressPeriods[year][quarter] + ']').addClass('active');
            }
            return false;
        });
        $('.progress-year-select').on('change', function() {
            let year = $(this).val();
            if (progressPeriods[year]) {
                $('.progress-quarter-select option').prop('disabled', true);
                $.each(progressPeriods[year], function(k){
                    $('.progress-quarter-select option[value=' + k + ']').prop('disabled', false);
                });
                $('.progress-quarter-select').val($('.progress-quarter-select option:not(:disabled)').first().val()).trigger('change');
            }
            return false;
        }).trigger('change');
JS;
    $this->registerJs($script);
}