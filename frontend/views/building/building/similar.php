<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var array $building
 * @var View $this
 */

?>

<div class="similar-slide">
    <a class="home-item-inner" href="<?= Url::to(['/building/building/show', 'slug' => $building['slug']])?>" data-action="load-modal-property">
        <div class="home-img">
            <?= Html::img($building['image'], ['alt' => $building['title'], 'title' => $building['title']])?>
        </div>
        <div class="home-wish"></div>
        <div class="home-info">
            <h2 class="home-title">
                <?= $building['title']?>
            </h2>
            <div class="row">
            </div>
        </div>
    </a>
</div>