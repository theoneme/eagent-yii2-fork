<?php

use yii\web\View;

/**
 * @var View $this
 * @var string $tab
 * @var integer $rooms
 * @var array $data
 * @var integer $buildingId
 */

$id = "{$tab}Room{$rooms}";
?>

<div class="bedroom-group">
    <div class="params-header" data-building="<?= $buildingId ?>" data-action="load-properties"
         data-operation="<?= $tab ?>" data-toggle="collapse" data-rooms="<?= $rooms?>" data-target="#<?= $id ?>">
        <div class="bedroom-title">
            <strong>
                <?= mb_strtoupper(Yii::t('main', '{count, plural, one{# room} other{# rooms}}', ['count' => $rooms])) ?>
            </strong>
            <span>
                (<?= Yii::t('main', '{count, plural, one{# condo} other{# condos}}', ['count' => $data['count']]) ?>)
            </span>
            <span>
                <?php if (array_key_exists('minPrice', $data)) { ?>
                    <?= \common\components\CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $data['minPrice']) ?>&nbsp;-&nbsp;
                    <?= \common\components\CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $data['maxPrice']) ?>
                <?php } ?>
            </span>
        </div>
    </div>
    <div data-role="properties-container" class="collapse" id="<?= $id ?>">

    </div>
</div>