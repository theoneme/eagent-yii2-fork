<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 16.11.2018
 * Time: 16:36
 */

use frontend\assets\CatalogAsset;
use frontend\forms\contact\ContactAgentForm;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\PropertyDirectModalAsset::register($this);
CatalogAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\frontend\assets\FilterAsset::register($this);

/**
 * @var array $building
 * @var array $seo
 * @var array $realtors
 * @var array $similar
 * @var array $counts
 * @var View $this
 * @var string $catalogCategory
 * @var ContactAgentForm $contactAgentForm
 */

?>

    <div data-role="catalog-container">

    </div>

    <div class="modal fade home-modal" id='direct-modal' tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <?= $this->render('show', [
                    'building' => $building,
                    'similar' => $similar, 'seo' => $seo,
                    'realtors' => $realtors,
                    'contactAgentForm' => $contactAgentForm,
                    'counts' => $counts
                ]) ?>
            </div>
        </div>
    </div>

<?php $catalogUrl = Url::to(['/property/catalog/index', 'operation' => 'sale', 'category' => $catalogCategory, 'per-page' => 15]);
$script = <<<JS
    $('#direct-modal').PropertyDirectModal({
        catalogUrl: '{$catalogUrl}'
    });
    $('#direct-modal').modal('show');
JS;
$this->registerJs($script);