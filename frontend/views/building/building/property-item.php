<?php

use common\decorators\BuildingPropertyStatusDecorator;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var array $property
 */

?>

<div class="flat-item">
    <div class="flat-title">
        <?= Html::a($property['title'], ['/property/property/show', 'slug' => $property['slug']], ['data-action' => 'load-modal-property'])?>
    </div>
    <?= BuildingPropertyStatusDecorator::decorate($property['status'])?>
    <div class="flat-price">
        <?= Html::a($property['price'], ['/property/property/show', 'slug' => $property['slug']])?>
    </div>
    <div class="flat-photos text-right">
        <?php if(count($property['images']) > 0) { ?>
            <?php foreach ($property['images'] as $key => $image) { ?>
                <?php if ($key === 0) { ?>
                    <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($property['images']), $image, [
                        'data-fancybox' => "gallery-{$property['id']}",
                        'data-role' => 'fancy-gallery-item'
                    ]) ?>
                <?php } else { ?>
                    <?= Html::a(null, $image, [
                        'data-fancybox' => "gallery-{$property['id']}",
                        'class' => 'hidden',
                        'data-role' => 'fancy-gallery-item'
                    ]) ?>
                <?php } ?>
            <?php } ?>
        <?php } else { ?>
            <i class="icon-camera"></i>&nbsp;<?= count($property['images']) ?>
        <?php } ?>
    </div>
</div>