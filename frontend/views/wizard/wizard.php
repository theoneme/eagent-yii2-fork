<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.09.2018
 * Time: 11:12
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\WizardAsset::register($this);
?>

<div class="wizard-wrap">
    <h1>Добавление объявления</h1>
    <div class="wizard-content">
        <div>
            <div class="sell-rent">
                <ul class="no-list flex">
                    <li><button class="grey-switch active">Продать</button></li>
                    <li><button class="grey-switch">Сдать</button></li>
                </ul><!--странная разметка это хак для того чтоб все браузеры выводили список без пробелов-->
            </div>
            <div class="type-adv">
                <div class="chover">
                    <input id="flat" class="radio-checkbox" name="type" value="flat" type="radio">
                    <label for="flat">Квартира</label>
                </div>
                <div class="chover">
                    <input id="room" class="radio-checkbox" name="type" value="room" type="radio">
                    <label for="room">Комната</label>
                </div>
                <div class="chover">
                    <input id="house" class="radio-checkbox" name="type" value="house" type="radio">
                    <label for="house">Дом, коттедж</label>
                </div>
                <div class="chover">
                    <input id="garaj" class="radio-checkbox" name="type" value="garaj" type="radio">
                    <label for="garaj">Гаражи</label>
                </div>
                <div class="chover">
                    <input id="home" class="radio-checkbox" name="type" value="home" type="radio">
                    <label for="home">Дачи</label>
                </div>
                <div class="chover">
                    <input id="commerc" class="radio-checkbox" name="type" value="commerc" type="radio">
                    <label for="commerc">Коммерческая</label>
                </div>
                <div class="chover">
                    <input id="earth" class="radio-checkbox" name="type" value="earth" type="radio">
                    <label for="earth">Земля</label>
                </div>
            </div>
        </div>
        <div class="row wizard-row">
            <div class="col-md-8 col-sm-12">
                <div class="sell-rent">
                    <ul class="no-list flex">
                        <li><button class="grey-switch active">Вторичка</button></li>
                        <li><button class="grey-switch">Новостройка</button></li>
                    </ul>
                </div>
                <div class="wizards-steps">
                    <form class="form-horizontal form-wizard" role="form">
                        <section id="step1">
                            <div class="wizard-step-header">
                                Местоположение
                                <span>шаг 1 из 7</span>
                            </div>
                            <div class="row step-content">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="city" class="col-md-4 control-label tex-left required">Населённый
                                            пункт</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="city"
                                                   placeholder="Введите название города">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="street" class="col-md-4 control-label tex-left required">Улица</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="street"
                                                   placeholder="Введите название улицы">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="number-home" class="col-md-4 control-label tex-left required">Номер дома</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="number-home"
                                                   placeholder="Например 8/16">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="district" class="col-md-4 control-label tex-left">Микрорайон</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="district"
                                                   placeholder="Введите название микрорайона">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="orient" class="col-md-4 control-label tex-left">Ориентир</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="orient"
                                                   placeholder="Например Центральный рынок">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 step-map">

                                </div>
                            </div>
                        </section>
                        <section id="step2">
                            <div class="wizard-step-header">
                                Информация о квартире
                                <span>шаг 2 из 7</span>
                            </div>
                            <div class="row step-content">
                                <div class="col-md-7">

                                    <div class="form-group">
                                        <label class="col-md-4 control-label tex-left required">Количество комнат</label>
                                        <div class="col-md-8">
                                            <div class="flex">
                                                <div class="chover radio-btn">
                                                    <input id="room1" class="radio-checkbox count-rooms" name="rooms"
                                                           type="radio">
                                                    <label for="room1">1</label>
                                                </div>
                                                <div class="chover radio-btn">
                                                    <input id="room2" class="radio-checkbox count-rooms" name="rooms"
                                                           type="radio">
                                                    <label for="room2">2</label>
                                                </div>
                                                <div class="chover radio-btn">
                                                    <input id="room3" class="radio-checkbox count-rooms" name="rooms"
                                                           type="radio">
                                                    <label for="room3">3</label>
                                                </div>
                                                <div class="chover radio-btn">
                                                    <input id="room4" class="radio-checkbox count-rooms" name="rooms"
                                                           type="radio">
                                                    <label for="room4">4</label>
                                                </div>
                                                <div class="chover radio-btn">
                                                    <input id="room5" class="radio-checkbox count-rooms" name="rooms"
                                                           type="radio">
                                                    <label for="room5">5+</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="square" class="col-md-4 control-label tex-left">Площадь м²</label>
                                        <div class="col-md-8">
                                            <div class="square flex">
                                                <input type="text" class="form-control" id="orient" placeholder="общая">
                                                <input type="text" class="form-control" id="orient" placeholder="жилая">
                                                <input type="text" class="form-control" id="orient" placeholder="кухня">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="floor" class="col-md-4 control-label tex-left required">Этаж</label>
                                        <div class="col-md-8">
                                            <div class="flex space-between floor">
                                                <input type="text" class="form-control" id="floor">
                                                <div>из</div>
                                                <input type="text" class="form-control" id="count-floors">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="type" class="col-md-4 control-label tex-left required">Тип квартиры</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="type">
                                                <option value="Выберите тип">Выберите тип</option>
                                                <option value="Бизнес класс">Бизнес класс</option>
                                                <option value="Гостинка">Гостинка</option>
                                                <option value="Студия">Студия</option>
                                                <option value="Типовая">Типовая</option>
                                                <option value="Улучшенной планировки">Улучшенной планировки</option>
                                                <option value="Эконом класс">Эконом класс</option>
                                                <option value="Элитная">Элитная</option>
                                                <option value="Двухуровневая">Двухуровневая</option>
                                                <option value="Индивидуальная">Индивидуальная</option>
                                                <option value="Ленинградская">Ленинградская</option>
                                                <option value="Малосемейка">Малосемейка</option>
                                                <option value="Новая">Новая</option>
                                                <option value="Пентагон">Пентагон</option>
                                                <option value="Пентзаус">Пентзаус</option>
                                                <option value="Полногабаритная">Полногабаритная</option>
                                                <option value="Другой">Другой</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="plan" class="col-md-4 control-label tex-left required">Планировка</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="plan">
                                                <option value="Выберите планировку">Выберите планировку</option>
                                                <option value="Изолированная">Изолированная</option>
                                                <option value="Свободная">Свободная</option>
                                                <option value="Смежная">Смежная</option>
                                                <option value="Смежно-изолированная">Смежно-изолированная</option>
                                                <option value="Иное">Иное</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="state" class="col-md-4 control-label tex-left required">Ремонт</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="state">
                                                <option value="Выберите состояние">Выберите состояние</option>
                                                <option value="В отличном состоянии">В отличном состоянии</option>
                                                <option value="Требует капитального ремонта">Требует капитального
                                                    ремонта
                                                </option>
                                                <option value="Требует косметического ремонта">Требует косметического
                                                    ремонта
                                                </option>
                                                <option value="Хорошее">Хорошее</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="san" class="col-md-4 control-label tex-left required">Санузел</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="san">
                                                <option value="Выберите cанузел">Выберите cанузел</option>
                                                <option value="Без удобств">Без удобств</option>
                                                <option value="Несколько">Несколько</option>
                                                <option value="Раздельный">Раздельный</option>
                                                <option value="Совмещённый">Совмещённый</option>
                                                <option value="Иное">Иное</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lodgy" class="col-md-4 control-label tex-left">Лоджий</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="lodgy">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="balcony" class="col-md-4 control-label tex-left">Балконов</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="balcony">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="step3">
                            <div class="wizard-step-header">
                                Цена и условия сделки
                                <span>шаг 3 из 7</span>
                            </div>
                            <div class="row step-content">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="price" class="col-md-4 control-label tex-left required">Цена, руб.</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="price">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label tex-left"></label>
                                        <div class="col-md-8">
                                            <div class="chover">
                                                <input id="discuss" class="radio-checkbox" name="discuss"
                                                       type="checkbox">
                                                <label for="discuss">Торг</label>
                                            </div>
                                            <div class="chover">
                                                <input id="clean-deal" class="radio-checkbox" name="clean-deal"
                                                       type="checkbox">
                                                <label for="clean-deal">Чистая продажа</label>
                                            </div>
                                            <div class="chover">
                                                <input id="mortgage" class="radio-checkbox" name="mortgage"
                                                       type="checkbox">
                                                <label for="mortgage">Ипотека</label>
                                            </div>
                                            <div class="chover">
                                                <input id="change" class="radio-checkbox" name="change" type="checkbox">
                                                <label for="change">Возможен обмен</label>
                                            </div>
                                            <div class="chover">
                                                <input id="money" class="radio-checkbox" name="money" type="checkbox">
                                                <label for="money">Внесен задаток</label>
                                            </div>
                                            <div class="chover">
                                                <input id="zalog" class="radio-checkbox" name="zalog" type="checkbox">
                                                <label for="zalog">Находится в залоге</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="owner" class="col-md-4 control-label tex-left required">Форма
                                            собственности</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="owner">
                                                <option value="Государственная">Государственная</option>
                                                <option value="Договор долевого участия">Договор долевого участия
                                                </option>
                                                <option value="Инвестиционная">Инвестиционная</option>
                                                <option value="Кооперативная">Кооперативная</option>
                                                <option value="Не оформлена">Не оформлена</option>
                                                <option value="Частная">Частная</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="step4">
                            <div class="wizard-step-header">
                                Информация о доме
                                <span>шаг 4 из 7</span>
                            </div>
                            <div class="row step-content">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="year" class="col-md-4 control-label tex-left">Год постройки</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="year">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="type-home" class="col-md-4 control-label tex-left required">Тип дома</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="type-home">
                                                <option value="Брежневка">Брежневка</option>
                                                <option value="Малоэтажка">Малоэтажка</option>
                                                <option value="Общежитие">Общежитие</option>
                                                <option value="Секционного типа">Секционного типа</option>
                                                <option value="Спецпроект">Спецпроект</option>
                                                <option value="Сталинка">Сталинка</option>
                                                <option value="Хрущёвка">Хрущёвка</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="material" class="col-md-4 control-label tex-left required">Материал
                                            дома</label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="material">
                                                <option value="Бетонные блоки">Бетонные блоки</option>
                                                <option value="Дерево">Дерево</option>
                                                <option value="Кирпич">Кирпич</option>
                                                <option value="Кирпич-монолит">Кирпич-монолит</option>
                                                <option value="Монолит">Монолит</option>
                                                <option value="Панель">Панель</option>
                                                <option value="Шлакоблоки">Шлакоблоки</option>
                                                <option value="Другое">Другое</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="step5">
                            <div class="wizard-step-header">
                                Фото и видео
                                <span>шаг 5 из 7</span>
                            </div>
                            <div class="row step-content">
                                <div class="col-md-12">
                                    <p>Вы можете загрузить в объявление не более 50 фотографий.</p>
                                    <div>
                                        место для вашего загрузчика фото.
                                    </div>
                                    <p>
                                        Какая-то инфа про размер фотографий
                                    </p>
                                    <div class="form-group">
                                        <label for="youtube" class="col-md-4 control-label tex-left youtube-load">Видео с</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="youtube">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="step6">
                            <div class="wizard-step-header">
                                Дополнительная информация
                                <span>шаг 6 из 7</span>
                            </div>
                            <div class="row step-content">
                                <div class="col-md-12">
                                    редактор текста
                                </div>
                            </div>
                        </section>
                        <section id="step7">
                            <div class="wizard-step-header">
                                Контактные данные
                                <span>шаг 7 из 7</span>
                            </div>
                            <div class="row step-content">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="name" class="col-md-4 control-label tex-left required">Имя</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label tex-left required">Телефон</label>
                                        <div class="col-md-8 form-phones">
                                            <div class="plus-phone-block">
                                                <div class="plus-phone">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control phone-input">
                                                        <span class="input-group-addon">&times;</span>
                                                    </div>
                                                    <div class="plus-btn text-center"><i class="icon-plus-black-symbol"></i></div>
                                                </div>
                                                <div class="plus-comment">
                                                    <a href="#" class="plus-comment-btn">Добавить комментарий</a>
                                                    <input type="text" class="form-control phone-comment"
                                                           placeholder="Например, звонить после 18:00">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mail" class="col-md-4 control-label tex-left">E-mail</label>
                                        <div class="col-md-8">
                                            <input type="email" class="form-control" id="mail" value="mail@mail.com">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="pre-view">
                            <a href="#">Предпросмотр объявления</a>
                        </div>
                        <div class="text-center">
                            <input type="submit" class="btn btn-big btn-white-blue" value="Разместить объявление">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4 visible-lg visible-md menu-wizard-aside">
                <div class="progress-steps">
                    <div class="progress-percent text-center">
                        66
                    </div>
                    <div class="progress-info">
                        Заполните Ваше объявление
                        как можно подробнее и его будут видеть чаще
                    </div>
                </div>
                <ul class="no-list progress-menu">
                    <li>
                        <a class="ok" href="#step1">
                            Местоположение
                            <div class="good-mark">
                                ✔
                            </div>
                            <div class="progress-error" id="step1Error">
                                1 ошибка
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="ok" href="#step2">
                            Информация о квартире
                            <div class="good-mark">
                                ✔
                            </div>
                            <div class="progress-error" id="step1Error">
                                1 ошибка
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="ok" href="#step3">
                            Цена и условия сделки
                            <div class="good-mark">
                                ✔
                            </div>
                            <div class="progress-error" id="step1Error">
                                1 ошибка
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="ok" href="#step4">
                            Информация о доме
                            <div class="good-mark">
                                ✔
                            </div>
                            <div class="progress-error" id="step1Error">
                                1 ошибка
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="ok" href="#step5">
                            Фото и видео
                            <div class="good-mark">
                                ✔
                            </div>
                            <div class="progress-error" id="step1Error">
                                1 ошибка
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="ok" href="#step6">
                            Дополнительная информация
                            <div class="good-mark">
                                ✔
                            </div>
                            <div class="progress-error" id="step1Error">
                                1 ошибка
                            </div>
                        </a>
                    </li>
                    <li>
                        <a class="step-error" href="#step7">
                            Контактные данные
                            <div class="good-mark">
                                ✔
                            </div>
                            <div class="progress-error" id="step1Error">
                                1 ошибка
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php

$script = <<<JS

    $(document).ready(function() {
        if($('.plus-phone-block').length===1){
            $('.plus-phone-block').find('.input-group-addon').hide();
        }
    });

    $(document.body).on('click','.plus-phone-block .input-group-addon',function() {
        $(this).closest('.plus-phone-block').remove();
        if($('.plus-phone-block').length===1){
            $('.plus-phone-block').find('.input-group-addon').hide();
        }
    });

    $(document.body).on('click','.plus-btn',function() {
        $(this).closest('.plus-phone-block').clone().appendTo('.form-phones');
        $('.plus-phone-block:last-child .phone-input').val('');
        $('.plus-phone-block:last-child .phone-comment').val('');
        $('.plus-phone-block').find('.input-group-addon').show();
    });
    
    $(document.body).on('click','.plus-comment-btn',function(e) {
        e.preventDefault();
        $('.phone-comment').hide();
        $(this).siblings('.phone-comment').show();
        $(this).hide();
    });
    
    $(document.body).on('click', function(e) {
        if (!$(e.target).hasClass('phone-comment') && !$(e.target).hasClass('plus-comment-btn')) {
            $('.phone-comment').each(function() {
                if(!$(this).val()){
                    $(this).siblings('.plus-comment-btn').show();
                    $(this).hide();
                }
            });
        }
    });
    
    
    //menu active
    $(function () {
        var sideMenu, sideMenuHeight, menuItems, scrollItems;
        var offsetMenuTop = $('.wizard-row').offset().top;
        var menuWidth = $('.menu-wizard-aside').outerWidth();
        var offsetMenuLeft = $('.menu-wizard-aside').offset().left;
    
        function reconstructScroller() {
            $(document).trigger("scroll");
            sideMenu = $(".progress-menu li"), sideMenuHeight = sideMenu.outerHeight() + offsetMenuTop, menuItems = sideMenu.find("a"), scrollItems = menuItems.map(function () {
                var item = $($(this).attr("href"));
                if (item.length) {
                    return item;
                }
            });
        }

        $(document.body).on('click', '.progress-menu li a', function () {
            elementClick = $(this).attr("href");
            destination = $(elementClick).offset().top - offsetMenuTop;
            $("body,html").animate({
                scrollTop: destination
            }, 800);
            return false;
        });
        
        $(window).resize(function() {
            $("body,html").scrollTop(100);
            offsetMenuTop = $('.wizard-row').offset().top;
            menuWidth = $('.menu-wizard-aside').outerWidth();
            offsetMenuLeft = $('.menu-wizard-aside').offset().left;
        });
        
        reconstructScroller();
        $(window).scroll(function () {
            var fromTop = $(this).scrollTop() + sideMenuHeight;
            var cur = scrollItems.map(function () {
                if ($(this).offset().top < fromTop) return this;
            });
            cur = cur[cur.length - 1];
            var id = cur && cur.length ? cur[0].id : "";
            $(".progress-menu li a").removeClass("selected");
            menuItems.parent().end().filter("[href*=\\'#" + id + "']").addClass("selected");
            
            if($(this).scrollTop() > offsetMenuTop){
                if(!$('.menu-wizard-aside').hasClass('menu-fixed')){
                    $('.menu-wizard-aside').addClass('menu-fixed');
                    $('.menu-wizard-aside').css('left',offsetMenuLeft);
                    $('.menu-wizard-aside').css('max-width',menuWidth);
                }   
            } else {
                $('.menu-wizard-aside').removeClass('menu-fixed');
                $('.menu-wizard-aside').css('left',0);
            }
        });
    });
JS;
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyB9nI8GFycVFBlWVH-qM2ovMQOB7zpKjbA');
$this->registerJs($script);
?>
