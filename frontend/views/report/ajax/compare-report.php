<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2018
 * Time: 15:56
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\components\CurrencyHelper;

/**
 * @var \frontend\forms\CompareReportForm $reportForm
 * @var array $properties
 */

?>

<div class="modal fade" id="report-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close not-age" data-dismiss="modal">×</button>
                <div class="modal-title text-center"><?= Yii::t('catalog', 'Make Report') ?></div>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['/report/manage/create-compare']),
                    'id' => 'report-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                ]); ?>
                <p><?= Yii::t('catalog', 'Selected Properties') ?>:</p>
                <table>
                    <?php foreach ($properties['items'] as $key => $property) { ?>
                        <tr class="report-item">
                            <td>
                                <?= Html::img($property['image'], ['alt' => $property['title'], 'title' => $property['title']]) ?>
                                <?= $form->field($reportForm, "id[{$key}]")->hiddenInput(['value' => $property['id']])->label(false)->error(false) ?>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <?= $property['title'] ?>
                                        </td>
                                        <td>
                                            <?= $property['price'] ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= $form->field($reportForm, "comments[{$key}]")->hiddenInput([
                                                'placeholder' => Yii::t('catalog', 'Add comment for this property')
                                            ])->label(false) ?>
                                            <?php if (array_key_exists($key, $reportForm['comments']) && !empty($reportForm['comments'][$key])) { ?>
                                                <?= Yii::t('catalog', 'Comment: {comment}', [
                                                    'comment' => $reportForm['comments'][$key]
                                                ]) ?>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?= $form->field($reportForm, "prices[{$key}]")->hiddenInput([
                                                'placeholder' => Yii::t('catalog', 'Possible price')
                                            ])->label(false) ?>
                                            <?php if (array_key_exists($key, $reportForm['prices']) && !empty($reportForm['prices'][$key])) { ?>
                                                <?= Yii::t('catalog', 'Possible price: {price}', [
                                                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $reportForm['prices'][$key])
                                                ]) ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    <?php } ?>
                </table>

                <?= $form->field($reportForm, 'message')->textarea(['rows' => 10]) ?>

                <div class="form-group text-center">
                    <?= Html::submitButton(Yii::t('catalog', 'Create Report'), ['class' => 'btn btn-small btn-blue-white']) ?>
                </div>
                <?= $form->field($reportForm, 'property_area')->hiddenInput()->label(false)->error(false) ?>
                <?= $form->field($reportForm, 'rooms')->hiddenInput()->label(false)->error(false) ?>
                <?= $form->field($reportForm, 'address')->hiddenInput()->label(false)->error(false) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>