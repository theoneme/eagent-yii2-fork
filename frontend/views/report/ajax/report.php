<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2018
 * Time: 15:56
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\ReportForm $reportForm
 * @var array $properties
 */

?>

    <div class="modal fade" id="report-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close not-age" data-dismiss="modal">×</button>
                    <div class="modal-title text-center"><?= Yii::t('catalog', 'Make Report') ?></div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['/report/manage/create']),
                        'id' => 'report-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                    ]); ?>
                    <p><?= Yii::t('catalog', 'Selected Properties') ?>:</p>
                    <table>
                        <?php foreach ($properties['items'] as $property) { ?>
                            <tr class="report-item">
                                <td>
                                    <?= Html::img($property['image'],['alt' => $property['title'], 'title' => $property['title']]) ?>
                                    <?= $form->field($reportForm, 'id[]')->hiddenInput(['value' => $property['id']])->label(false)->error(false) ?>
                                </td>
                                <td class="max-report-item"><?= $property['title'] ?></td>
                                <td><?= $property['price'] ?></td>
                            </tr>
                        <?php } ?>
                    </table>
<!--                    --><?//= $form->field($reportForm, 'toId')->hiddenInput()->label(false) ?>
<!---->
<!--                    <p>--><?//= Yii::t('catalog', 'Report Receiver') ?><!--</p>-->
<!--                    <div class="form-group">-->
<!--                        --><?//= Html::textInput('toid-autocomplete', null, ['class' => 'form-control', 'id' => 'receiver-autocomplete']) ?>
<!--                    </div>-->

                    <?= $form->field($reportForm, 'firstName')->textInput([
                            'placeholder' => Yii::t('labels', 'Enter client`s first name')
                    ])->label($reportForm->getAttributeLabel('firstName') . '*') ?>
                    <?= $form->field($reportForm, 'middleName')->textInput([
                        'placeholder' => Yii::t('labels', 'Enter client`s middle name')
                    ]) ?>
                    <?= $form->field($reportForm, 'lastName')->textInput([
                        'placeholder' => Yii::t('labels', 'Enter client`s last name')
                    ])->label($reportForm->getAttributeLabel('lastName') . '*') ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($reportForm, 'email')->textInput([
                                'placeholder' => Yii::t('labels', 'Enter client`s email address')
                            ])->label($reportForm->getAttributeLabel('Email') . '*') ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($reportForm, 'phone')->textInput([
                                'placeholder' => Yii::t('labels', 'Enter client`s phone')]) ?>
                        </div>
                    </div>


                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('catalog', 'Send Report'), ['class' => 'btn btn-small btn-blue-white']) ?>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>

<?php $userUrl = Url::to(['/user/ajax/users']);
$script = <<<JS
    new AutoComplete({
        selector: "#receiver-autocomplete",
        autoFocus: false,
        minChars: 2,
        source: function(request, response) {
            try { xhr.abort(); } catch(e){}
            xhr = $.get('{$userUrl}', { request: request }, function(data) { 
                response(data);
            });     
        },
        renderItem: function (item, search){
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            let re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return '<div class="autocomplete-suggestion flex flex-v-center"  data-id="' + item.id + '" data-username="' + item.username + '">'
                + '<div class="autocomplete-suggestion-column autocomplete-suggestion-image">'
                + '<img src="' + item.avatar + '"/>'
                + '</div>'
                + '<div class="autocomplete-suggestion-column">'
                + item.username.replace(re, "<b>$1</b>") + '&nbsp;(ID: ' + item.id + ')'
                + '</div>'
                + '</div>';
        },
        onSelect: function(e, term, item) { console.log(item, term);
            $("#reportform-toid").val(item.getAttribute('data-id'));
            $("#receiver-autocomplete").val(item.getAttribute('data-username'));
        }
    });
JS;

$this->registerJs($script);