<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 10.10.2018
 * Time: 15:56
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \frontend\forms\CompareReportForm $reportForm
 * @var array $properties
 */

?>

    <div class="modal fade" id="report-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close not-age" data-dismiss="modal">×</button>
                    <div class="modal-title text-center">
                        <?= Yii::t('catalog', 'Create Report') ?>
                    </div>
                    <div class="modal-subtitle text-center">
                        <?= Yii::t('catalog', 'Specify parameters of your property, so we can make an analyze.') ?>
                    </div>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['/report/ajax/pre-render-compare']),
                        'id' => 'report-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
//                        'method' => 'get',
//                        'options' => [
//                            'data' => [
//                                'no-ajax' => 1
//                            ]
//                        ]
                    ]); ?>

                    <?= $form->field($reportForm, 'lat')->hiddenInput(['id' => 'banner-filter-lat'])->label(false)->error(false) ?>
                    <?= $form->field($reportForm, 'lng')->hiddenInput(['id' => 'banner-filter-lng'])->label(false)->error(false) ?>
                    <?= $form->field($reportForm, 'address')->textInput(['id' => 'banner-filter-city-helper']) ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($reportForm, 'property_area')->textInput(['type' => 'number']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($reportForm, 'rooms')->textInput(['type' => 'number']) ?>
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('catalog', 'Create Report'), ['class' => 'btn btn-small btn-blue-white']) ?>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS
    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('banner-filter-city-helper'), {
        types: ['geocode']
    });
    autocomplete.addListener('place_changed', function() {
        let place = autocomplete.getPlace();
        $('#banner-filter-lat').val(place.geometry.location.lat());
        $('#banner-filter-lng').val(place.geometry.location.lng());
    });
JS;

$this->registerJs($script);