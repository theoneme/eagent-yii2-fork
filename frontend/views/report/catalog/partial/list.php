<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.01.2019
 * Time: 16:17
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var array $seo
 * @var array $properties
 * @var array $markers
 * @var integer $gridSize
 * @var \frontend\forms\PreCompareReportForm $reportForm
 */

?>

<div class="loading-overlay hidden"></div>

<div class="flex space-between">
    <div>
        <h1 class="text-left"><?= $seo['heading'] ?></h1>
    </div>
</div>
<?php if (count($properties['items']) > 0) { ?>
    <div class="table-houses">
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/report/ajax/render-compare']),
            'id' => 'report-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
        ]); ?>
        <table id="list-houses">
            <thead>
            <tr>
                <th>
                    <div class="chover">
                        <input id="allCheck" class="radio-checkbox" name="allCheck" type="checkbox">
                        <label for="allCheck"></label>
                    </div>
                </th>
                <th class="up">ID</th>
                <th class="up"><?= Yii::t('catalog', 'Address') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Photos') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Area') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Price') ?></th>
                <th class="up"><?= Yii::t('catalog', 'Rooms') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($properties['items'] as $key => $property) { ?>
                <tr>
                    <td>
                        <div class="chover">
                            <input id="house<?= $property['id'] ?>" class="radio-checkbox" name="id[]"
                                   value="<?= $property['id'] ?>"
                                   type="checkbox"
                                   checked>
                            <label for="house<?= $property['id'] ?>"></label>
                        </div>
                    </td>
                    <td>
                        <?= $property['id'] ?>
                        <?= $form->field($reportForm, "id[{$key}]")->hiddenInput(['value' => $property['id']])->label(false)->error(false) ?>
                    </td>
                    <td>
                        <a href="<?= Url::to(['/property/property/show', 'slug' => $property['slug']]) ?>"
                           data-hover="show-marker"
                           data-id="<?= $property['id'] ?>"
                           data-type="property"
                           data-action="load-modal-property"
                           data-role="marker-data"
                           data-property="<?= $markers[$property['id']] ?>">
                            <?= $property['address'] ?>
                        </a>
                        <?= $form->field($reportForm, "comments[{$key}]")->textInput([
                            'placeholder' => Yii::t('catalog', 'Add comment for this property')
                        ])->label(false) ?>
                    </td>
                    <td>
                        <?php if (count($property['images']) > 0) { ?>
                            <?php foreach ($property['images'] as $imageKey => $image) { ?>
                                <?php if ($imageKey === 0) { ?>
                                    <?= Html::a('<i class="icon-camera"></i>&nbsp;' . count($property['images']), $image, [
                                        'data-fancybox' => "gallery-{$property['id']}",
                                        'data-role' => 'fancy-gallery-item'
                                    ]) ?>
                                <?php } else { ?>
                                    <?= Html::a(null, $image, [
                                        'data-fancybox' => "gallery-{$property['id']}",
                                        'class' => 'hidden',
                                        'data-role' => 'fancy-gallery-item'
                                    ]) ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } else { ?>
                            <i class="icon-camera"></i>&nbsp;<?= count($property['images']) ?>
                        <?php } ?>
                    </td>
                    <td><?= isset($property['attributes']['property_area']) ? "{$property['attributes']['property_area']} м²" : '-' ?></td>
                    <td>
                        <?= $property['price'] ?>
                        <?= $form->field($reportForm, "prices[{$key}]")->textInput([
                            'placeholder' => Yii::t('catalog', 'Possible price')
                        ])->label(false) ?>
                    </td>
                    <td><?= $property['attributes']['rooms'] ?? '-' ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?= $form->field($reportForm, 'property_area')->hiddenInput()->label(false) ?>
        <?= $form->field($reportForm, 'rooms')->hiddenInput()->label(false) ?>
        <?= $form->field($reportForm, 'address')->hiddenInput()->label(false) ?>
        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('catalog', 'Create Report'), ['class' => 'btn btn-big btn-blue-white']) ?>
        </div>

        <?php ActiveForm::end() ?>
    </div>
<?php } ?>
