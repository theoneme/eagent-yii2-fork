<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 01.10.2018
 * Time: 11:16
 */

use yii\helpers\Url;
use yii\web\View;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\plugins\AutoCompleteAsset::register($this);
\frontend\assets\FilterAsset::register($this);
\common\assets\GoogleAsset::register($this);
\frontend\assets\plugins\FancyBoxAsset::register($this);

/**
 * @var array $properties
 * @var \frontend\forms\PreCompareReportForm $reportForm
 * @var array $markers
 * @var View $this
 * @var string $lat
 * @var string $lng
 * @var array $seo
 * @var integer $zoom
 * @var string $box
 */

?>

    <div class="catalog-wrap">
        <div class="col-md-8 col-sm-12 catalog-left" id="properties-list">
            <?= $this->render('partial/list', [
                'seo' => $seo,
                'properties' => $properties,
                'markers' => $markers,
                'reportForm' => $reportForm
            ]) ?>
        </div>
        <div class="col-md-4 hidden-xs catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?= \frontend\widgets\ReportWidget::widget() ?>
<?php $markerViewUrl = Url::to(['/property/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);
$reportUrl = Url::to(['/report/ajax/render-compare']);

$script = <<<JS
    $('[data-role=fancy-gallery-item]').fancybox({
        hash: false
    });
    
    let map = new CatalogMap({
        lat: '{$lat}',
        lon: '{$lng}',
        zoom: {$zoom},
        box: '{$box}',
        mode: 'advanced',
        markerViewRoutes: {
            property: '{$markerViewUrl}',
        },
        markerIcons: {
            property: '{$markerPropertyIcon}'
        }
    });
    map.init();
    map.parseMarkers();
    
    $('#report-form').on('beforeSubmit', function() {
         let params = $(this).serialize();
         $.post('{$reportUrl}', params, function(result) {
             if(result.success === true) {
                 let container = $('#report-modal-container');
                 container.html(result.html);
                 container.find('.modal').modal('show');
             }
         });
        
         return false;
    });
JS;

$this->registerJs($script);

