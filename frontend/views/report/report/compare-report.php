<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 11.10.2018
 * Time: 16:40
 */

use common\components\CurrencyHelper;
use yii\helpers\Html;
use yii\helpers\Url;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\HomesListAsset::register($this);
\frontend\assets\plugins\FancyBoxAsset::register($this);
\frontend\assets\plugins\GoogleAddressAsset::register($this);
\frontend\assets\plugins\MoreLessAsset::register($this);

/**
 * @var array $properties
 * @var array $report
 * @var array $propertyMarkers
 */

?>

    <div class="list-homes-wrap clearfix">
        <div class="col-md-8 catalog-left">
            <div class="lh-user-block">
                <div class="lh-user-photo">
                    <?= Html::img($report['from']['avatar'], ['alt' => $report['from']['username'], 'title' => $report['from']['username']]) ?>
                </div>
                <div class="lh-user-info">
                    <p>
                        <?= Yii::t('catalog', 'Report created {date}', ['date' => Yii::$app->formatter->asDatetime($report['created_at'])]) ?>
                    </p>
                    <p>
                        <strong><?= $report['from']['username'] ?></strong>
                    </p>
                    <?php if (!empty($report['from']['phone'])) { ?>
                        <p>
                            <strong><?= $report['from']['phone'] ?></strong>
                        </p>
                    <?php } ?>

                    <p>
                        <a href="mailto:<?= $report['from']['email'] ?>"><?= $report['from']['email'] ?></a>
                    </p>
                </div>
            </div>
            <div class="home-list">
                <p>
                    <?= Yii::t('catalog', 'We made analyze for your property located by address {address} with {rooms, plural, one{# room} other{# rooms}} and area {area, plural, one{# square meter} other{# square meters}}', [
                        'address' => $report['customData']['address'],
                        'rooms' => $report['customData']['rooms'],
                        'area' => $report['customData']['propertyArea']
                    ]) ?>
                </p>
                <p>
                    <?= Yii::t('catalog', 'List of similar properties:') ?>
                </p>

                <?php foreach ($properties['items'] as $key => $property) { ?>
                    <div class="list-home-item" data-role="marker-data"
                         data-property="<?= $propertyMarkers[$property['id']] ?>">
                        <div class="lh-main-block">
                            <div class="lh-title">
                                <div class="lh-title-text">
                                    <span class="counter"><?= $key + 1 ?>.</span> <?= $property['title'] ?>
                                </div>
                            </div>
                            <div class="lh-cols">
                                <?php if (!empty($property['images'])) { ?>
                                    <div class="lh-photos">
                                        <?php foreach ($property['images'] as $imageKey => $image) { ?>
                                            <?php if ($imageKey === 0) { ?>
                                                <?= '<div class="lh-big-photo">' ?>
                                                <?= Html::a(Html::img($property['thumbnails'][$imageKey]), $image, ['data-fancybox' => 'gallery-' . $key, 'alt' => $property['title'], 'title' => $property['title']]) ?>
                                                <?= '</div><div class="lh-small-photo">' ?>
                                            <?php } else { ?>
                                                <div class="lh-small-photo-item">
                                                    <?= Html::a(Html::img($property['thumbnails'][$imageKey]), $image, ['data-fancybox' => 'gallery-' . $key, 'alt' => $property['title'], 'title' => $property['title']]) ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                        <?= '</div>' ?>
                                    </div>
                                <?php } ?>
                                <div class="lh-params">
                                    <div class="lh-params-row">
                                        <div class="lh-params-title"><?= Yii::t('catalog', 'Price') ?>:</div>
                                        <div class="lh-params-text"><?= $property['price'] ?></div>
                                    </div>
                                    <?php if (!empty($report['customData']['prices'][$key])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title">
                                                <?= Yii::t('catalog', 'Possible price') ?>:
                                            </div>
                                            <div class="lh-params-text">
                                                <?= CurrencyHelper::convertAndFormat($report['customData']['currency'] ?? 'RUB', Yii::$app->params['app_currency'], $report['customData']['prices'][$key]) ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (array_key_exists('property_area', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Property Area') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['property_area'] ?>
                                                м²
                                            </div>
                                        </div>

                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Price per m²') ?></div>
                                            <div class="lh-params-text">
                                                <?= floor($property['raw_price'] / (int)$property['attributes']['property_area']) ?>
                                                <?= Yii::$app->params['app_currency'] ?> / м²
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('living_area', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Living Area') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['living_area'] ?>
                                                м²
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('kitchen_area', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Kitchen Area') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['kitchen_area'] ?>
                                                м²
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('rooms', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Number of Rooms') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['rooms'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('bedrooms', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Number of Bedrooms') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['bedrooms'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('balconies', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Number of Balconies') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['balconies'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('bathrooms', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Number of Bathrooms') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['bathrooms'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('floor', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Floor') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['floor'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('year_built', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Year Built') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['year_built'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('garages', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Number of Garages') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['garages'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (array_key_exists('loggias', $property['attributes'])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Number of Loggias') ?></div>
                                            <div class="lh-params-text">
                                                <?= $property['attributes']['loggias'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if (!empty($report['customData']['comments'][$key])) { ?>
                                        <div class="lh-params-row">
                                            <div class="lh-params-title"><?= Yii::t('catalog', 'Comment') ?>:</div>
                                            <div class="lh-params-text">
                                                <?= $report['customData']['comments'][$key] ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="lh-description">
                                <div class="lh-descr-title up-title text-left"><?= Yii::t('catalog', 'Description') ?></div>
                                <div class="hide-content hide-text">
                                    <div>
                                        <?= $property['description'] ?? Yii::t('catalog', 'Description is missing') ?>
                                    </div>
                                </div>
                                <a class="more-less" data-height="70">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div>
                    <p class="lh-descr-title up-title text-left"><?= Yii::t('catalog', 'Summary') ?></p>
                    <p><?= nl2br($report['customData']['message']) ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 catalog-right">
            <div class="map-over">
                <div id="map_catalog"></div>
            </div>
        </div>
    </div>

<?php $more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');
$markerViewUrl = Url::to(['/property/marker/view']);
$markerPropertyIcon = Url::to(['/images/marker.png'], true);

$script = <<<JS
    let moreLess = $('.more-less');
    moreLess.MoreLess({
        moreText: '{$more}&nbsp;',
        lessText: '{$less}&nbsp;'
    });
    moreLess.trigger('show');
    
    let countViewer = 0;
    $('.lh-photos').each(function() {
        countViewer++;
        let viewer = 'viewer' + countViewer;
        $('[data-fancybox="'+viewer+'"]').fancybox({
            buttons: [
                'close'
            ],
            infobar: false,
            caption: false
        });
    });
    
    let map = new CatalogMap({
        markerViewRoutes: {
            property: '{$markerViewUrl}',  
        },
        markerIcons: {
            property: '{$markerPropertyIcon}'
        }
    });
    map.init();
    map.parseMarkers();
JS;

$this->registerJs($script);
