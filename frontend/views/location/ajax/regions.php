<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 13:42
 */

use yii\helpers\Html;

/**
 * @var array $regions
 * @var array $locationData
 */

?>

<?php foreach ($regions as $letter => $items) { ?>
    <div class="city-group">
        <div class="city-letter"><?= $letter ?></div>
        <ul class="no-list city-list">
            <?php foreach ($items as $region) { ?>
                <li>
                    <?= Html::a($region['title'], '#', [
                        'data-slug' => $region['slug'],
                        'data-id' => $region['id'],
                        'data-role' => "{$region['entity']}-item",
                        'class' => $region['id'] === $locationData['region_id'] ? 'active' : ''
                    ]) ?>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
