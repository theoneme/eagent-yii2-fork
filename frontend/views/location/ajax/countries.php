<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 13:42
 */

use yii\helpers\Html;

/**
 * @var array $countries
 * @var array $locationData
 */

?>

<?php foreach ($countries as $letter => $items) { ?>
    <div class="city-group">
        <div class="city-letter"><?= $letter ?></div>
        <ul class="no-list city-list">
            <?php foreach ($items as $country) { ?>
                <li>
                    <?= Html::a($country['title'], '#', [
                        'data-slug' => $country['slug'],
                        'data-id' => $country['id'],
                        'data-role' => 'country-item',
                        'class' => $country['id'] === $locationData['country_id'] ? 'active' : ''
                    ]) ?>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
