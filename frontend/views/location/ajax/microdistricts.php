<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 13:42
 */

use yii\helpers\Html;

/**
 * @var array $microDistricts
 * @var string $currentMicroDistrict
 */

?>

<?php if (!empty($microDistricts)) { ?>
    <?php foreach ($microDistricts as $letter => $items) { ?>
        <div class="city-group">
            <div class="city-letter"><?= $letter ?></div>
            <ul class="no-list city-list">
                <?php foreach ($items as $microDistrict) { ?>
                    <li>
                        <?= Html::radio('city-part-helper', $microDistrict['slug'] === $currentMicroDistrict, [
                            'id' => "microdistrict-{$microDistrict['slug']}",
                            'value' => $microDistrict['slug'],
                            'class' => 'radio-checkbox',
                            'data-entity' => 'microdistrict'
                        ]) ?>
                        <label for="<?= "microdistrict-{$microDistrict['slug']}" ?>">
                            <span class="ftgn-title"><?= $microDistrict['title'] ?></span>
                        </label>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
<?php } else { ?>
    <p>
        <?= Yii::t('catalog', 'No microdistricts were found for this region') ?>
    </p>
<?php } ?>
