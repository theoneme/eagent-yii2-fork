<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 13:42
 */

use yii\helpers\Html;

/**
 * @var array $cities
 * @var array $locationData
 */

?>

<?php foreach ($cities as $letter => $items) { ?>
    <div class="city-group">
        <div class="city-letter"><?= $letter ?></div>
        <ul class="no-list city-list">
            <?php foreach ($items as $city) { ?>
                <li>
                    <?= Html::a($city['title'], '#', [
                        'data-slug' => $city['slug'],
                        'data-role' => "{$city['entity']}-item",
                        'class' => $city['id'] === $locationData['city_id'] ? 'active' : ''
                    ]) ?>
                </li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>
