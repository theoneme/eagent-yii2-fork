<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 04.12.2018
 * Time: 18:18
 */

use yii\helpers\Html;
use common\helpers\UrlAdvanced;

\frontend\assets\LocationAsset::register($this);

/**
 * @var array $data
 */

$this->title = Yii::t('index', 'Worldwide Real Estate listings');

?>

<section class="banner-main text-main block-content">
    <div class="banner-center">

        <div class="tab-content banner-tabs">
            <div class="tab-pane fade in active" id="sell">
                <h1 class="banner-title text-center">
                    <?= Yii::t('index', 'Worldwide Real Estate listings') ?>
                </h1>
                <div class="banner-subtitle text-center">
                    <?= Yii::t('index', 'On this service you can buy, sell or rent properties from all over the world') ?>
                </div>
                <div class="input-group search text-center">
                    <input type="text" class="form-control" name="search" placeholder="<?= Yii::t('index', 'Set country, region..') ?>">
                    <div class="input-group-addon">
                        <a class="search-but" href="#"><?= Yii::t('index', 'Search') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="block-content">
    <div class="container">
        <?php foreach ($data as $country) { ?>
            <h2 class="location-country">
                <?= Html::a($country['title'], UrlAdvanced::to(['/site/index', 'app_city' => "country-{$country['slug']}"])) ?>
            </h2>
            <?php foreach ($country['regions'] as $region) { ?>
                <div class="location-regions">
                    <h3 class="location-region">
                        <?= Html::a($region['title'], UrlAdvanced::to(['/site/index', 'app_city' => "region-{$region['slug']}"])) ?>
                    </h3>
                    <div class="alias-city clearfix">
                        <?php foreach ($region['cities'] as $city) { ?>
                            <h4 class="col-md-3">
                                <?= Html::a($city['title'], ['/site/index', 'app_city' => $city['slug']]) ?>
                            </h4>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</section>