<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2018
 * Time: 14:30
 */

use frontend\assets\CommonAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/*
 * @var $this \yii\web\View
 * @var $content string
 * @var array $categorySlugs
 */

CommonAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="<?= Url::to(['/images/favicon.png'], true) ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= Url::to(['/images/favicon.png'], true) ?>" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="<?= in_array(Yii::$app->language, ['he-IL', 'ar-AR']) ? 'rtl' : '' ?>">
<?php $this->beginBody() ?>
<div class="alert-container alert-index">
    <?= Alert::widget() ?>
</div>
<?= \frontend\widgets\MobileMenu::widget() ?>
<div class="mainblock">
    <?= \frontend\widgets\HeaderWidget::widget() ?>
    <div class="all-content clearfix">
        <?= $content ?>
    </div>
    <div class="mfooter"></div>
</div>

<?= \yii\bootstrap\Modal::widget([
    'id' => 'property-modal',
    'options' => ['class' => 'fade home-modal']
]) ?>

<?= \yii\bootstrap\Modal::widget([
    'id' => 'dynamic-modal',
    'options' => ['class' => 'fade']
]) ?>

<?= \frontend\widgets\FooterWidget::widget() ?>

<?php
$script = <<<JS
    $('#property-modal').PropertyModal();
JS;

$this->registerJs($script); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

