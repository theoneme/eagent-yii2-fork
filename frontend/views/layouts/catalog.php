<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 30.08.2018
 * Time: 14:30
 */

use frontend\assets\CommonAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/*
 * @var $this \yii\web\View
 * @var $content string
 * @var array $categorySlugs
 */

CommonAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="<?= Url::to(['/images/favicon.png'], true) ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= Url::to(['/images/favicon.png'], true) ?>" type="image/png"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?= in_array(Yii::$app->language, ['he-IL', 'ar-AR']) ? 'rtl' : '' ?>">
<?php $this->beginBody() ?>
<div class="alert-container alert-catalog">
    <?= Alert::widget() ?>
</div>
<?= \frontend\widgets\MobileMenu::widget() ?>
<div class="mainblock">
    <div class="page-with-aside">
        <aside class="hidden-xs">
            <ul class="no-list aside-menu">
                <li>
                    <a href="/" class="home">
                    <span class="aside-icon text-center">
                        <i class="icon-icon"></i>
                    </span>
                        <span class="open-title text-left">
                        <?= Yii::t('main', 'Home') ?>
                    </span>
                    </a>
                </li>
                <li>
                    <a href="#" data-action="show-more-filters">
                    <span class="aside-icon text-center">
                        <i class="icon-controls"></i>
                    </span>
                        <span class="hover-title">
                        <?= Yii::t('main', 'Filter') ?>
                    </span>
                        <span class="open-title text-left">
                        <?= Yii::t('main', 'Filter') ?>
                    </span>
                    </a>
                </li>
<!--                <li>-->
<!--                    <a href="#" data-text="Print">-->
<!--                    <span class="aside-icon text-center">-->
<!--                        <i class="icon-print"></i>-->
<!--                    </span>-->
<!--                        <span class="hover-title">-->
<!--                        --><?//= Yii::t('main', 'Print') ?>
<!--                    </span>-->
<!--                        <span class="open-title text-left">-->
<!--                        --><?//= Yii::t('main', 'Print') ?>
<!--                    </span>-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="#" data-text="Wish list">-->
<!--                    <span class="aside-icon text-center">-->
<!--                        <i class="icon-heart"></i>-->
<!--                    </span>-->
<!--                        <span class="hover-title">-->
<!--                        --><?//= Yii::t('main', 'Wish List') ?>
<!--                    </span>-->
<!--                        <span class="open-title text-left">-->
<!--                        --><?//= Yii::t('main', 'Wish List') ?>
<!--                    </span>-->
<!--                    </a>-->
<!--                </li>-->
                <?php if (!empty(Yii::$app->params['runtime']['catalogTemplate']) && Yii::$app->params['runtime']['catalogTemplate'] === 'index-table') {?>
                    <li>
                        <a href="<?= Url::to(['/report/ajax/render']) ?>" data-text="Report"
                           data-action="load-modal-report">
                        <span class="aside-icon text-center">
                            <i class="icon-flyers"></i>
                        </span>
                        <span class="hover-title">
                            <?= Yii::t('main', 'Report') ?>
                        </span>
                        <span class="open-title text-left">
                            <?= Yii::t('main', 'Report') ?>
                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?= Url::to(['/report/ajax/pre-render-compare']) ?>" data-text="Comparative report"
                           data-action="load-modal-report">
                        <span class="aside-icon text-center">
                            <i class="icon-flyers"></i>
                        </span>
                            <span class="hover-title">
                            <?= Yii::t('main', 'Comparative report') ?>
                        </span>
                            <span class="open-title text-left">
                            <?= Yii::t('main', 'Comparative report') ?>
                        </span>
                        </a>
                    </li>
                <?php }?>
                <li class="hide" data-id="menu-sell">
                    <!--<a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'sale']) ?>" data-menu='menu-sell'>-->
                    <a href="#" data-menu='menu-sell'>
                        <span class="aside-icon text-center">
                            <i class="icon-for-sale"></i>
                        </span>
                        <span class="hover-title">
                            <?= Yii::t('main', 'Sell') ?>
                        </span>
                        <span class="open-title text-left">
                            <?= Yii::t('main', 'Sell') ?>
                        </span>
                    </a>
                </li>
                <li class="hide" data-id="menu-rent">
                    <!--<a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'rent']) ?>" data-menu='menu-rent'>-->
                    <a href="#" data-menu='menu-rent'>
                        <span class="aside-icon text-center">
                            <i class="icon-rent"></i>
                        </span>
                        <span class="hover-title">
                            <?= Yii::t('main', 'Rent') ?>
                        </span>
                        <span class="open-title text-left">
                            <?= Yii::t('main', 'Rent') ?>
                        </span>
                    </a>
                </li>
                <li class="hide" data-id="menu-new">
                    <!--<a href="<?= Url::to(['/property/catalog/index', 'category' => $categorySlugs['flats'] ?? 'flats', 'operation' => 'sale']) ?>" data-menu='menu-new'>-->
                    <a href="#" data-menu='menu-new'>
                        <span class="aside-icon text-center">
                            <i class="icon-skyline"></i>
                        </span>
                        <span class="hover-title">
                            <?= Yii::t('main', 'New Constructions') ?>
                        </span>
                        <span class="open-title text-left">
                            <?= Yii::t('main', 'New Constructions') ?>
                        </span>
                    </a>
                </li>
                <li class="hide" data-id="menu-agent">
                    <a href="<?= Url::to(['/agent/catalog/index']) ?>">
                        <span class="aside-icon text-center">
                            <i class="icon-agent"></i>
                        </span>
                        <span class="hover-title">
                            <?= Yii::t('main', 'Agents') ?>
                        </span>
                        <span class="open-title text-left">
                            <?= Yii::t('main', 'Agents') ?>
                        </span>
                    </a>
                </li>
                <li class="hide" data-id="menu-tariff">
                    <a href="<?= Url::to(['/page/tariff']) ?>">
                        <span class="aside-icon text-center">
                            <i class="icon-piggy-bank"></i>
                        </span>
                        <span class="hover-title">
                            <?= Yii::t('main', 'Tariffs') ?>
                        </span>
                        <span class="open-title text-left">
                            <?= Yii::t('main', 'Tariffs') ?>
                        </span>
                    </a>
                </li>
                <li class="hide" data-id="menu-requests">
                    <a href="<?= Url::to(['/request/catalog/index']) ?>">
                        <span class="aside-icon text-center">
                            <i class="icon-search1"></i>
                        </span>
                        <span class="hover-title">
                            <?= Yii::t('main', 'Requests') ?>
                        </span>
                        <span class="open-title text-left">
                            <?= Yii::t('main', 'Requests') ?>
                        </span>
                    </a>
                </li>
            </ul>
        </aside>
        <?= \frontend\widgets\HeaderWidget::widget() ?>
        <div class="all-content clearfix">
            <?= $content ?>
        </div>

        <div class="mfooter"></div>
    </div>
</div>

<?= \yii\bootstrap\Modal::widget([
    'id' => 'property-modal',
    'options' => ['class' => 'fade home-modal'],
]) ?>

<?= \yii\bootstrap\Modal::widget([
    'id' => 'dynamic-modal',
    'options' => ['class' => 'fade new-modal modal480']
]) ?>

<?= \frontend\widgets\FooterWidget::widget(['template' => 'catalog-footer-widget']) ?>

<?php $script = <<<JS
    $('#property-modal').PropertyModal();

    $(document).on('show.bs.modal', '.modal:not(.home-modal)', function () {
        let zIndex = 1050 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
    $(document).on('hidden.bs.modal', '.modal', function () {
        if($('.modal:visible').length > 0){
            $('body').addClass('modal-open');
        }
    });
    
    $('aside').on('mouseenter', function(){
        $(this).addClass('open');
    }).on('mouseleave',function(){
        $(this).removeClass('open');
    });
JS;

$this->registerJs($script); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

