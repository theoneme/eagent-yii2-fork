<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 03.10.2018
 * Time: 11:42
 */

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\TariffAsset::register($this);

/**
 * @var array $page
 */

?>
<div class="container">
    <h1 class="text-center"><?= $page['title'] ?></h1>

    <div class="page-container">
        <p class="text-right">
            <?= Yii::$app->formatter->asDate($page['created_at']) ?>
        </p>
        <?= $page['description'] ?>
    </div>
</div>
