<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 27.09.2018
 * Time: 15:49
 */

use yii\helpers\Html;

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\AgentAsset::register($this);

?>

<div class="page-wrap">
    <div class="breadcrumbs">
        <ul class="no-list">
           <li>
               <a href="#">Florida</a>
           </li>
            <li>
                <a href="#">Sunny Isles Beach</a>
            </li>
            <li>
                <a href="#">Julio Clavijo</a>
            </li>
        </ul>
    </div>
    <div class="agent-profile-content">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="agent-main-info">
                    <div class="agent-ph ph-big">
                        <img src="../images/veles.jpg" alt="agent">
                    </div>
                    <div class="agent-main-right">
                        <h1 class="text-center">
                            Julio Clavijo
                            <div class="agent-status text-center">
                                <?= Yii::t('main', 'Premium agent') ?>
                            </div>
                        </h1>
                        <div class="agent-activity">
                            <div>All Activity</div>
                            <div class="agent-activity-rate">
                                <i class="icon-favorite"></i>
                                <div class="rate-number">5<span>/5</span></div>
                                <a href="#">89 Reviews</a>
                            </div>
                            <div>38 Sales Last 12mo</div>
                        </div>
                    </div>
                    <div class="contact-link">
                        <a href="#contact-us" class="btn btn-small btn-blue-white width100">Contact us</a>
                    </div>
                </div>
                <div class="our-members">
                    <h2 class="bold-h2 text-left">Our members</h2>
                    <div class="agent-members">
                        <div class="agent-member text-center">
                            <div class="agent-ph ph-middle">
                                <img src="../images/veles.jpg" alt="agent">
                            </div>
                            <a class="agent-member-rate">
                                <i class="icon-favorite"></i>
                                5 (28 reviews)
                            </a>
                            <a href="#" class="agent-member-name">
                                Beata Hill PA
                            </a>
                        </div>
                        <div class="agent-member text-center">
                            <div class="agent-ph ph-middle">
                                <img src="../images/veles.jpg" alt="agent">
                            </div>
                            <a class="agent-member-rate">
                                <i class="icon-favorite"></i>
                                5 (4 reviews)
                            </a>
                            <a href="#" class="agent-member-name">
                                Adam Chauser
                            </a>
                        </div>
                    </div>
                </div>
                <div class="agent-description">
                    <h2 class="bold-h2 text-left">About Us</h2>
                    <div class="agent-sub-title">Real Estate Professional (14 years experience)</div>
                    <div class="agent-specialty"> Specialties: Buyer's Agent, Listing Agent, Foreclosure, Short-Sale, Property Management, Landlord </div>
                    <div class="hide-content hide-text">
                        <div>
                            <p>
                                Having the right real estate agent means having an agent who is committed to helping you buy or sell your home with the highest level of expertise in your local market. This means also to help you in understanding each step of the buying or selling process. This commitment level has helped me build a remarkable track record of delivering results.
                            </p>
                            <p>
                                Our group was established with the goal of helping clients to increase and manage their personal wealth through the use of well-developed real estate investment strategies. Our group is comprised of agents fluent in  SPANISH, RUSSIAN, GERMAN,POLISH, HEBREW , ITALIAN, FRENCH AND PORTUGUESE . Every strategy is designed to suit the unique objectives of each individual or family and is created to provide both immediate and long term returns for this and future generations.
                            </p>
                            <p>
                                Whether you are an experienced investor or a first time buyer, I can help you in finding the property of your dreams. Please feel free to browse my website or let me guide you every step of the way by calling or emailing me to set up an appointment today.
                            </p>
                            <p>
                                I earned a bachelor's Degree in International Business Major at Florida International University in 2009 where I graduated with honors.
                            </p>
                            <p>
                                I have also been in the Top Seller Agent's list for years at Platinum and Gold Levels
                            </p>
                        </div>
                    </div>
                    <a class="more-less" data-height="120">
                        <?= Yii::t('main', 'More') ?>
                        <i class="icon-down"></i>
                    </a>
                </div>
                <div class="premier-lenders">
                    <h2 class="bold-h2 text-left">Premier Lenders</h2>
                    <div class="tooltip-ea" data-position="right" data-width="310">
                        <div class="tooltip-modal">
                            <a href="" class="tooltip-close">&times;</a>
                            <div class="tooltip-body">
                                <p>
                                    The premier lender and the agent have a financial relationship and pay to advertise on this page. Zillow and the agents do not recommend or endorse any of the premier lenders. For more information on Zillow advertising practices, see Zillow’s Terms of Use & Privacy.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="agent-block">
                        <div class="agent-photo">
                            <img src="../images/veles.jpg" alt="agent">
                        </div>
                        <div class="agent-middle">
                            <div class="agent-name">
                                <a href="#">Igor Shrayev</a>
                            </div>
                            <div class="agent-rate">
                                <a href="#" class="agent-rate-alias">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </a>
                                (<a href="#">18</a>)
                            </div>
                            <div class="agent-add-info">
                                <div>
                                    (954) 613-1367
                                </div>
                                <div>
                                    NMLS# 379915
                                </div>
                                <div>
                                    <a href="#">Lender Website</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-sales hide">
                    <h2 class="bold-h2 text-left">Our Listings & Sales</h2>
                    <div class="our-sales-map">

                    </div>
                </div>
                <div class="homes-list">
                    <h2 class="bold-h2 text-left">Our active listings</h2><div class="homes-list-count">(19)</div>
                    <div class="homes-table homes-active">
                        <div class="homes-row homes-header">
                            <div class="homes-title">
                                Property Address
                            </div>
                            <div class="homes-opts bath">
                                Bed / Bath
                            </div>
                            <div class="homes-opts price">
                                Price
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts bath">
                                2 Bed, 2 Bath
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts bath">
                                2 Bed, 2 Bath
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts bath">
                                2 Bed, 2 Bath
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts bath">
                                2 Bed, 2 Bath
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts bath">
                                2 Bed, 2 Bath
                            </div>
                            <div class="homes-opts">
                                $108,000
                            </div>
                        </div>
                    </div>
                    <div class="flex space-between">
                        <ul class="pagination">
                            <li class="prev"><a href="#" data-page="1">«</a></li>
                            <li class="active"><a href="#" data-page="0">1</a></li>
                            <li><a href="#" data-page="1">2</a></li>
                            <li><a href="#" data-page="2">3</a></li>
                            <li><a href="#" data-page="3">4</a></li>
                            <li class="next"><a href="#">»</a></li>
                        </ul>
                        <a class="asCenter hidden-xs" href="#">View on a map</a>
                    </div>
                </div>

                <div class="homes-list">
                    <h2 class="bold-h2 text-left">Our Past Sales</h2><div class="homes-list-count">(193 all-time)</div>
                    <div class="homes-table homes-sold">
                        <div class="homes-row homes-header">
                            <div class="homes-title">
                                Property Address
                            </div>
                            <div class="homes-opts represented">
                                Represented
                            </div>
                            <div class="homes-opts sold-date">
                                Sold Date
                            </div>
                            <div class="homes-opts price">
                                Price
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts represented">
                                Seller
                            </div>
                            <div class="homes-opts sold-date">
                                09/21/2018
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts represented">
                                Buyer
                            </div>
                            <div class="homes-opts sold-date">
                                09/21/2018
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts represented">
                                Seller
                            </div>
                            <div class="homes-opts sold-date">
                                09/21/2018
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts represented">
                                Buyer
                            </div>
                            <div class="homes-opts sold-date">
                                09/21/2018
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                        <div class="homes-row">
                            <div class="homes-title">
                                <div class="flex">
                                    <div class="homes-img"><img src="../images/home-ph1.jpeg" alt="home"></div>
                                    <div class="home-addr"><a href="#"> 1750 NE 191st St APT 421-3 Miami, FL 33179 </a></div>
                                </div>
                            </div>
                            <div class="homes-opts represented">
                                Seller
                            </div>
                            <div class="homes-opts sold-date">
                                09/21/2018
                            </div>
                            <div class="homes-opts price">
                                $108,000
                            </div>
                        </div>
                    </div>
                        <ul class="pagination">
                            <li class="prev"><a href="#" data-page="1">«</a></li>
                            <li class="active"><a href="#" data-page="0">1</a></li>
                            <li><a href="#" data-page="1">2</a></li>
                            <li><a href="#" data-page="2">3</a></li>
                            <li><a href="#" data-page="3">4</a></li>
                            <li class="next"><a href="#">»</a></li>
                        </ul>
                </div>

                <div class="reviews-list-block">
                    <div class="flex space-between review-header">
                        <h2 class="bold-h2 text-left">Ratings & Reviews</h2>
                        <a class="btn btn-small btn-white-blue asCenter" href="#">Write a review</a>
                    </div>
                    <form class="row form-horizontal reviews-filter">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-3 col-sm-2 col-xs-3 control-label">Show:</label>
                                <div class="col-md-9 col-sm-10 col-xs-9">
                                    <select name="service" id="reviews-filters">
                                        <option value="" selected="selected">All reviews (89)</option>
                                        <option value="boughtHome">Helped me buy a home or lot/land (53)</option>
                                        <option value="listedHome">Listed and sold a home or lot/land (13)</option>
                                        <option value="showedHomes">Showed me homes or lots (7)</option>
                                        <option value="boughtAndSoldHomes">Helped me buy and sell homes (5)</option>
                                        <option value="helpedFindHomeToRent">Helped me find a home to rent (4)</option>
                                        <option value="propertyManageHomeIOwn">Property manage a home I own (3)</option>
                                        <option value="helpedFindTenantForRental">Helped me find tenant for rental (2)
                                        </option>
                                        <option value="consultedBuyingSelling">Consulted me on buying or selling a home
                                            (2)
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label for="inputPassword3" class="col-md-3 col-sm-2 col-xs-3 control-label">Sort by:</label>
                                <div class="col-md-9 col-sm-10 col-xs-9">
                                    <select name="sort" id="reviews-sorts">
                                        <option value="1" selected="selected">Newest first</option>
                                        <option value="2">Rating (high to low)</option>
                                        <option value="3">Rating (low to high)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="review-list">
                        <div class="review-item">
                            <h3>
                                <div class="agent-rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                Highly likely to recommend
                            </h3>
                            <div class="review-text-info">
                                09/07/2018 - zuser20170303100757770
                            </div>
                            <div class="review-text-info">
                                Bought a Cooperative home in 2018 in Eastern Shores, North Miami Beach, FL.
                            </div>
                            <div class="review-components">
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Local knowledge:
                                    </div>
                                    <div class="agent-rate">
                                            <i class="icon-favorite"></i>
                                            <i class="icon-favorite"></i>
                                            <i class="icon-favorite"></i>
                                            <i class="icon-favorite"></i>
                                            <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Process expertise:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Responsiveness:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Negotiation skills:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="review-text">
                                <div class="hide-content hide-text">
                                    <div>
                                        <p>Julio assisted me in the process of Selling My Home in Sunny Isles.</p>
                                        <p>He very well knows the current market conditions and have all the contacts needed in a Real Estate Transaction.</p>
                                        <p>Selling my vacation Home in Florida from Mexico City was made really easy by Julio.</p>
                                        <p>You can trust him 100% and will  highly recommend anyone to use his Services.</p>
                                        <p>Way to Go Julio !!</p>
                                    </div>
                                </div>
                                <a class="more-less" data-height="120">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="review-item">
                            <h3>
                                <div class="agent-rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                Highly likely to recommend
                            </h3>
                            <div class="review-text-info">
                                09/07/2018 - zuser20170303100757770
                            </div>
                            <div class="review-text-info">
                                Bought a Cooperative home in 2018 in Eastern Shores, North Miami Beach, FL.
                            </div>
                            <div class="review-components">
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Local knowledge:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Process expertise:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Responsiveness:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Negotiation skills:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="review-text">
                                <div class="hide-content hide-text">
                                    <div>
                                        <p>Julio assisted me in the process of Selling My Home in Sunny Isles.</p>
                                        <p>He very well knows the current market conditions and have all the contacts needed in a Real Estate Transaction.</p>
                                    </div>
                                </div>
                                <a class="more-less" data-height="120">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="review-item">
                            <h3>
                                <div class="agent-rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                Highly likely to recommend
                            </h3>
                            <div class="review-text-info">
                                09/07/2018 - zuser20170303100757770
                            </div>
                            <div class="review-text-info">
                                Bought a Cooperative home in 2018 in Eastern Shores, North Miami Beach, FL.
                            </div>
                            <div class="review-components">
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Local knowledge:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Process expertise:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Responsiveness:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Negotiation skills:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="review-text">
                                <div class="hide-content hide-text">
                                    <div>
                                        <p>Julio assisted me in the process of Selling My Home in Sunny Isles.</p>
                                        <p>He very well knows the current market conditions and have all the contacts needed in a Real Estate Transaction.</p>
                                        <p>Selling my vacation Home in Florida from Mexico City was made really easy by Julio.</p>
                                        <p>You can trust him 100% and will  highly recommend anyone to use his Services.</p>
                                        <p>Way to Go Julio !!</p>
                                    </div>
                                </div>
                                <a class="more-less" data-height="120">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="review-item">
                            <h3>
                                <div class="agent-rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                Highly likely to recommend
                            </h3>
                            <div class="review-text-info">
                                09/07/2018 - zuser20170303100757770
                            </div>
                            <div class="review-text-info">
                                Bought a Cooperative home in 2018 in Eastern Shores, North Miami Beach, FL.
                            </div>
                            <div class="review-components">
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Local knowledge:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Process expertise:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Responsiveness:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Negotiation skills:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="review-text">
                                <div class="hide-content hide-text">
                                    <div>
                                        <p>Julio assisted me in the process of Selling My Home in Sunny Isles.</p>
                                        <p>He very well knows the current market conditions and have all the contacts needed in a Real Estate Transaction.</p>
                                        <p>Selling my vacation Home in Florida from Mexico City was made really easy by Julio.</p>
                                        <p>You can trust him 100% and will  highly recommend anyone to use his Services.</p>
                                        <p>Way to Go Julio !!</p>
                                    </div>
                                </div>
                                <a class="more-less" data-height="120">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="review-item">
                            <h3>
                                <div class="agent-rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                Highly likely to recommend
                            </h3>
                            <div class="review-text-info">
                                09/07/2018 - zuser20170303100757770
                            </div>
                            <div class="review-text-info">
                                Bought a Cooperative home in 2018 in Eastern Shores, North Miami Beach, FL.
                            </div>
                            <div class="review-components">
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Local knowledge:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Process expertise:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Responsiveness:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                                <div class="review-component-item">
                                    <div class="review-component-title">
                                        Negotiation skills:
                                    </div>
                                    <div class="agent-rate">
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                        <i class="icon-favorite"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="review-text">
                                <div class="hide-content hide-text">
                                    <div>
                                        <p>Julio assisted me in the process of Selling My Home in Sunny Isles.</p>
                                        <p>He very well knows the current market conditions and have all the contacts needed in a Real Estate Transaction.</p>
                                        <p>Selling my vacation Home in Florida from Mexico City was made really easy by Julio.</p>
                                        <p>You can trust him 100% and will  highly recommend anyone to use his Services.</p>
                                        <p>Way to Go Julio !!</p>
                                    </div>
                                </div>
                                <a class="more-less" data-height="120">
                                    <?= Yii::t('main', 'More') ?>
                                    <i class="icon-down"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <ul class="pagination">
                        <li class="prev"><a href="#" data-page="1">«</a></li>
                        <li class="active"><a href="#" data-page="0">1</a></li>
                        <li><a href="#" data-page="1">2</a></li>
                        <li><a href="#" data-page="2">3</a></li>
                        <li><a href="#" data-page="3">4</a></li>
                        <li class="next"><a href="#">»</a></li>
                    </ul>
                </div>

                <div class="service-areas">
                    <h2 class="bold-h2 text-left">Service Areas</h2>
                    <div class="row service-areas-row">
                        <div class="col-md-4 col-sm-4">
                            <a href="#">Aventura, FL</a>
                            <a href="#">Bal Harbour, FL</a>
                            <a href="#">Bay Harbor Islands, FL</a>
                            <a href="#">Biscayne Park, FL</a>
                            <a href="#">Broward county, FL</a>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <a href="#">Dania Beach, FL</a>
                            <a href="#">El Portal, FL</a>
                            <a href="#">Golden Beach, FL</a>
                            <a href="#">Golden Glades, FL</a>
                            <a href="#">Hallandale, FL</a>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <a href="#">Hollywood, FL</a>
                            <a href="#">Indian Creek, FL</a>
                            <a href="#">Ives Estates, FL</a>
                            <a href="#">Miami Beach, FL</a>
                            <a href="#">Miami Gardens, FL</a>
                        </div>
                    </div>
                    <a href="#">View all service areas</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-12" id="contact-us">
                <div class="aside-profile-block">
                    <h3>Contact Us</h3>
                    <form>
                        <div class="form-group">
                            <?= Html::textInput('name', null, ['placeholder' => Yii::t('main', 'Your Name')]) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::input('tel', 'phone', null, ['placeholder' => Yii::t('main', 'Phone')]) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::input('email', 'email', null, ['placeholder' => Yii::t('main', 'Email')]) ?>
                        </div>
                        <div class="form-group">
                            <?= Html::textarea('message') ?>
                        </div>
                        <div class="text-center">
                            <input id="contact-agent" type="submit" class="btn btn-small btn-blue-white" value="<?= Yii::t('main', 'Contact us') ?>">
                        </div>
                    </form>
                </div>
                <div class="aside-profile-block">
                    <h3>Professional Information</h3>
                    <div class="prof-table">
                        <div class="prof-row">
                            <div class="prof-property">
                                Broker address:
                            </div>
                            <div class="prof-descr">
                                <div>Optimar International Realty</div>
                                <div>18246 Collins Ave</div>
                                <div>Sunny Isles Beach, FL  33160</div>
                            </div>
                        </div>
                        <div class="prof-row">
                            <div class="prof-property">
                                Cell phone:
                            </div>
                            <div class="prof-descr">
                                (954) 302-7980
                            </div>
                        </div>
                        <div class="prof-row">
                            <div class="prof-property">
                                Websites:
                            </div>
                            <div class="prof-descr">
                                <a href="#">Website</a>, <a href="#">Facebook</a>
                            </div>
                        </div>
                        <div class="prof-row">
                            <div class="prof-property">
                                Screen name:
                            </div>
                            <div class="prof-descr">
                                Julio Clavijo
                            </div>
                        </div>
                        <div class="prof-row">
                            <div class="prof-property">
                                Member since:
                            </div>
                            <div class="prof-descr">
                                08/26/2010
                            </div>
                        </div>
                        <div class="prof-row">
                            <div class="prof-property">
                                Licenses:
                            </div>
                            <div class="prof-descr">
                                3102151 (Florida Real Estate License)
                            </div>
                        </div>
                        <div class="prof-row">
                            <div class="prof-property">
                                Languages:
                            </div>
                            <div class="prof-descr">
                                English, French, Polish, Mandarin, Russian, Italian, Portuguese, Cantonese, German, Spanish, Hebrew
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');

$script = <<<JS
    $(document).ready(function() {
        var moreLess = $('.more-less');
        moreLess.MoreLess({
            moreText: '{$more}&nbsp;',
            lessText: '{$less}&nbsp;'
        });
        $('.more-less').trigger('show');
    });
JS;

$this->registerJs($script);