<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 29.05.2019
 * Time: 12:42
 */

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\WalletAsset::register($this);

?>

<div class="container balance-wrap">
    <h1>Управление платежами</h1>
    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12">
            <ul class="no-list ul-wallet">
                <li class="">
                    <a href="#wallet-100066198" data-toggle="tab" aria-expanded="false">
                        <div class="wallet-circle">
                            <div class="text-center">
                                D
                            </div>
                        </div>
                        <div class="wallet-title-info">
                            <div class="title-sum">
                                <span class="money">
                                    $0
                                </span>
                            </div>
                            <div class="title-number">UD100066198</div>
                        </div>
                    </a></li>
                <li class="">
                    <a href="#wallet-100043879" data-toggle="tab" aria-expanded="false">
                        <div class="wallet-circle">
                            <div class="text-center">
                                E
                            </div>
                        </div>
                        <div class="wallet-title-info">
                            <div class="title-sum">
                                <span class="money">
                                    €0
                                </span>
                            </div>
                            <div class="title-number">EE100043879</div>
                        </div>
                    </a></li>
                <li class="active">
                    <a href="#wallet-100000076" data-toggle="tab" aria-expanded="true">
                        <div class="wallet-circle">
                            <div class="text-center">
                                R
                            </div>
                        </div>
                        <div class="wallet-title-info">
                            <div class="title-sum">
                                <span class="money">
                                    5&nbsp;500р.
                                </span>
                            </div>
                            <div class="title-number">RR100000076</div>
                        </div>
                    </a></li>
            </ul>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="tab-content">
                <div id="wallet-100066198" class="wallet-content tab-pane fade active in">
                    <div class="wallet-title">
                        UD кошелёк
                    </div>
                    <div class="head-wallet">
                        <div class="wallet-circle">
                            <div class="text-center">
                                D
                            </div>
                        </div>
                        <div class="wallet-title-info">
                            <div class="title-sum">
                                <span class="money">$0</span></div>
                            <div class="title-number">UD100066198</div>
                        </div>
                    </div>
                    <div class="row sell-statistic-box">
                        <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                            <a href="#" class="stat-alias">
                                <div class="sellstat-title text-center">Доходы</div>
                                $0 </a></div>
                        <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                            <a href="#" class="stat-alias">
                                <div class="sellstat-title text-center">Расходы</div>
                                $0 </a></div>
                        <div class="col-md-4 col-sm-4 col-xs-4 sell-statistic text-center">
                            <a href="#" class="stat-alias">
                                <div class="sellstat-title text-center">Текущий баланс</div>
                                $0 </a></div>
                    </div>
                    <div class="wallet-btns">
                        <div class="dropdown-block col-md-6">
                            <button class="btn btn-big btn-white-blue dropdown-btn" type="button">
                                Запрос вывода денег <span class="caret"></span>
                            </button>
                            <div class="dropdown-info">
                                <div class="alert alert-info">
                                        Пожалуйста, сперва заполните платежную информацию (<a
                                                href="#">Ссылка</a>) в настройках Вашего
                                        профиля
                                </div>
                            </div>
                        </div>
                        <div class="dropdown-block col-md-6">
                            <button class="btn btn-big btn-white-blue dropdown-btn" type="button">
                                Перевод денег <span class="caret"></span>
                            </button>
                            <div class="dropdown-info dropdown-form">
                                    <form id="" class="transfer-form" action="#" method="post">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="amount-input" name="amount"
                                                           placeholder="Сумма списания" style="width: auto" data-currency="USD">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" class="ratio-input" name="ratio" disabled=""
                                                           placeholder="Курс валют" style="width: auto"></div>
                                                <div class="form-group">
                                                    <input type="text" class="amount-to-input" name="amount_to"
                                                           placeholder="Сумма зачисления" style="width: auto" data-currency="">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <select class="form-control transfer-wallet-select" name="to_id"
                                                            style="width: auto">
                                                        <option data-currency="" value="">Выберите кошелёк</option>
                                                        <option data-currency="EUR" value="100043879">EE100043879</option>
                                                        <option data-currency="RUB" value="100000076">RR100000076</option>
                                                    </select></div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary">Отправить</button>
                                                </div>
                                            </div>
                                        </div>


                                    </form>
                            </div>
                        </div>
                    </div>
                    <div class="inbox row margin-30">
                        <div class="col-md-12">
                            <div class="all-messages">
                                <div class="over-table-viewgigs">
                                    <table class="table-money">
                                        <tbody>

                                        <tr class="messages-row" data-key="0">
                                            <td class="hidden-xs" width="50px">
                                                <div class="wallet-avatar text-center">
                                                    R
                                                </div>
                                            </td>
                                            <td>
                                                <div class="sum">
                                                    <strong class="text-success">+500р.</strong> <span class="doing">Возврат средств за отмененный заказ</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="time text-right">5 февр. 2018 г.</div>
                                            </td>
                                        </tr>
                                        <tr class="messages-row" data-key="1">
                                            <td class="hidden-xs" width="50px">
                                                <div class="wallet-avatar text-center">
                                                    R
                                                </div>
                                            </td>
                                            <td>
                                                <div class="sum">
                                                    <strong class="text-danger">-500р.</strong> <span class="doing">Оплата за покупку услуги (<a
                                                                href="#">Просмотр заказа</a>)</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="time text-right">5 февр. 2018 г.</div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="wallet-100043879" class="wallet-content tab-pane fade">
                </div>
                <div id="wallet-100000076" class="wallet-content tab-pane fade">
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$script = <<<JS
    $(document.body).on('click', '.dropdown-btn', function() {
        $(this).closest('.dropdown-block').toggleClass('open');
    });
JS;
$this->registerJs($script);
