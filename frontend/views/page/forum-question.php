<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 29.12.2018
 * Time: 14:42
 */

\frontend\assets\ForumAsset::register($this);
\frontend\assets\CatalogAsset::register($this);

?>

    <div class="page-wrap">
        <div class="row">
            <div class="col-lg-2">
                <nav class="navbar aside-forum text-left">
                    <div class="nav-head">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-forum">
                            Вопросы <i class="icon-down"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="nav-forum">
                        <ul class="no-list menu-forum">
                            <li>
                                <a href="#">Какой-то пункт</a>
                                <ul>
                                    <li><a href="">Специалист–специалисту</a></li>
                                    <li><a href="">Тенденции рынка</a></li>
                                </ul>
                            </li>
                            <li class="active">
                                <a href="">Вопросы</a>
                                <ul>
                                    <li><a href=""><span class="plus-question"></span>Задать вопрос</a></li>
                                    <li><a href="">Специалист–специалисту</a></li>
                                    <li><a href="">Тенденции рынка</a></li>
                                    <li><a href="">Городская недвижимость</a></li>
                                    <li><a href="">Коммерческая недвижимость</a></li>
                                    <li><a href="">Загородная недвижимость</a></li>
                                    <li><a href="">Зарубежная недвижимость</a></li>
                                    <li><a href="">Ипотека</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div></nav>
            </div>
            <div class="col-lg-7 col-md-8 col-sm-8">
                <div class="breadcrumbs hidden-xs">
                    <ul class="no-list">
                        <li>
                            <a href="#">Форум</a>
                        </li>
                        <li>
                            <a href="#">Вопросы</a>
                        </li>
                        <li>
                            <a href="#">Загородная недвижимость</a>
                        </li>
                    </ul>
                </div>
                <h1 class="text-left">Помогите купить</h1>
                <div class="question-block">
                    <div class="article-author">
                        <div class="article-author-avatar">
                            <img src="/images/veles-min.jpg" alt="" title="">
                        </div>
                        <div class="article-author-info">
                            <div class="article-author-name">Иннокентий</div>
                        </div>
                    </div>
                    <div class="article-info">
                        <div class="article-info-item">26 декабря 2018</div>
                        <div class="article-info-item"><i class="icon-eye"></i> 222</div>
                        <div class="article-info-item">
                            <a href="#advices"><i class="icon-comment-black-oval-bubble-shape"></i> 22</a>
                        </div>
                    </div>
                    <div class="question-text">
                        <p>
                            Добрый день. я нашла участок хочу купить. пытаюсь найти владельцев . казазала все справки какие только можно по кадастровому номеру. знаю фио. но дальше найти не могу. как быть что делать.может быть кто то может помочь
                        </p>
                    </div>
                    <ul class="no-list question-tags">
                        <li><a href="#">#Участок</a></li>
                        <li><a href="#">#Покупка</a></li>
                    </ul>
                </div>
                <div class="question-btns flex space-between">
                    <a href="#reply" class="reply-btn btn btn-small btn-blue-white">Ответить</a>
                    <div class="work-share text-center">
                        <a class="fb hint--bottom social-share text-center"
                           href="#"
                           title="Поделиться в Facebook"><i class="icon-facebook-logo" aria-hidden="true"></i></a>
                        <a class="in hint--bottom social-share text-center"
                           href="#"
                           title="Поделиться в Vk"><i class="icon-vk-social-network-logo" aria-hidden="true"></i></a><a
                            class="tw hint--bottom social-share text-center"
                            href="#"
                            title="Поделиться в Twitter"><i class="icon-twitter" aria-hidden="true"></i></a><a
                            class="g hint--bottom social-share text-center"
                            href="#"
                            title="Поделиться в Google+"><i class="icon-google-plus" aria-hidden="true"></i></a><a
                            class="in hint--bottom social-share text-center"
                            href="#"
                            title="Поделиться в LinkedIn"><i class="icon-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>

                <div class="advices" id="advices">
                    <div class="advice-top flex space-between">
                        <div class="advice-title">
                            Советы <span>3</span>
                        </div>
                        <a class="sub-advice" href="#" data-toggle="modal" data-target="#subscribe-advice"><i class="icon-envelope"></i> Подписаться на советы</a>
                    </div>
                    <div class="advice-list">
                        <div class="advice-item">
                            <div class="author-top-block flex space-between">
                                <div class="article-author">
                                    <div class="article-author-avatar">
                                        <img src="/images/veles-min.jpg" alt="" title="">
                                    </div>
                                    <div class="article-author-info">
                                        <div class="article-author-name">
                                            Алиса
                                            <span class="marker-best-advice">Лучший совет</span>
                                        </div>
                                        <div class="reply-date">сегодня в 9:02</div>
                                    </div>
                                </div>
                                <div class="advice-likes">
                                    <div class="album-opt like-over">
                                        <i class="icon-thumbs-up-hand-symbol"></i>
                                        <span>1</span>
                                        <div class="people-likes">
                                            <div class="people-likes-title">1 user likes this</div>
                                            <a href="#" data-pjax="0">
                                                <img src="https://graph.facebook.com/10216076646618700/picture?width=600&amp;height=600" alt="Agus Cano" title="Agus Cano">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="album-opt like-over">
                                        <i class="icon-thumbs-down-silhouette"></i>
                                        <span>1</span>
                                        <div class="people-likes">
                                            <div class="people-likes-title">1 user dislikes this</div>
                                            <a href="#" data-pjax="0">
                                                <img src="https://graph.facebook.com/10216076646618700/picture?width=600&amp;height=600" alt="Agus Cano" title="Agus Cano">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="advice-text">
                                <p>
                                    Добрый день. я нашла участок хочу купить. пытаюсь найти владельцев . казазала все справки какие только можно по кадастровому номеру. знаю фио. но дальше найти не могу. как быть что делать.может быть кто то может помочь
                                </p>
                            </div>
                        </div>
                        <div class="advice-item">
                            <div class="author-top-block flex space-between">
                                <div class="article-author">
                                    <div class="article-author-avatar">
                                        <img src="/images/veles-min.jpg" alt="" title="">
                                    </div>
                                    <div class="article-author-info">
                                        <div class="article-author-name">
                                            Алиса
                                        </div>
                                        <div class="reply-date">сегодня в 9:02</div>
                                    </div>
                                </div>
                                <div class="advice-likes">
                                    <div class="album-opt like-over">
                                        <i class="icon-thumbs-up-hand-symbol"></i>
                                        <span>1</span>
                                        <div class="people-likes">
                                            <div class="people-likes-title">1 user likes this</div>
                                            <a href="#" data-pjax="0">
                                                <img src="https://graph.facebook.com/10216076646618700/picture?width=600&amp;height=600" alt="Agus Cano" title="Agus Cano">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="album-opt like-over">
                                        <i class="icon-thumbs-down-silhouette"></i>
                                        <span>1</span>
                                        <div class="people-likes">
                                            <div class="people-likes-title">1 user dislikes this</div>
                                            <a href="#" data-pjax="0">
                                                <img src="https://graph.facebook.com/10216076646618700/picture?width=600&amp;height=600" alt="Agus Cano" title="Agus Cano">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="advice-text">
                                <p>
                                    Добрый день. я нашла участок хочу купить. пытаюсь найти владельцев . казазала все справки какие только можно по кадастровому номеру. знаю фио. но дальше найти не могу. как быть что делать.может быть кто то может помочь
                                </p>
                            </div>
                        </div>
                        <div class="advice-item">
                            <div class="author-top-block flex space-between">
                                <div class="article-author">
                                    <div class="article-author-avatar">
                                        <img src="/images/veles-min.jpg" alt="" title="">
                                    </div>
                                    <div class="article-author-info">
                                        <div class="article-author-name">
                                            Алиса
                                        </div>
                                        <div class="reply-date">сегодня в 9:02</div>
                                    </div>
                                </div>
                                <div class="advice-likes">
                                    <div class="album-opt like-over">
                                        <i class="icon-thumbs-up-hand-symbol"></i>
                                        <span>1</span>
                                        <div class="people-likes">
                                            <div class="people-likes-title">1 user likes this</div>
                                            <a href="#" data-pjax="0">
                                                <img src="https://graph.facebook.com/10216076646618700/picture?width=600&amp;height=600" alt="Agus Cano" title="Agus Cano">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="album-opt like-over">
                                        <i class="icon-thumbs-down-silhouette"></i>
                                        <span>1</span>
                                        <div class="people-likes">
                                            <div class="people-likes-title">1 user dislikes this</div>
                                            <a href="#" data-pjax="0">
                                                <img src="https://graph.facebook.com/10216076646618700/picture?width=600&amp;height=600" alt="Agus Cano" title="Agus Cano">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="advice-text">
                                <p>
                                    Добрый день. я нашла участок хочу купить. пытаюсь найти владельцев . казазала все справки какие только можно по кадастровому номеру. знаю фио. но дальше найти не могу. как быть что делать.может быть кто то может помочь
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="reply-form" id="reply">
                    <div class="article-author">
                        <div class="article-author-avatar">
                            <img src="/images/veles-min.jpg" alt="" title="">
                        </div>
                        <div class="article-author-info">
                            <div class="article-author-name">Иннокентий</div>
                        </div>
                    </div>
                    <div class="reply-warning">
                        <p>
                            Внимание! Материалы, частично или полностью скопированные с других источников, должны содержать ссылку на источник. В случае нарушения авторских прав третьих лиц вы можете быть привлечены к ответственности по ст. 146 УК России.
                        </p>
                        <a href="#" class="close-reply-warning">&times;</a>
                    </div>
                    <form>
                        <div class="form-group">
                            <label class="control-label" for="forum-your-message">Ваше сообщение</label>
                            <textarea type="text" id="forum-your-message" class="form-control" name="" placeholder="Введите ваше сообщение" aria-required="true"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="forum-your-name">Ваше имя</label>
                                    <input type="text" id="forum-your-name" class="form-control" name="" placeholder="Введите ваше имя" aria-required="true">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label" for="forum-your-email">Ваше email</label>
                                    <input type="text" id="forum-your-email" class="form-control" name="" placeholder="Введите ваше адрес электронной почты" aria-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="chover big-radio-btn">
                                <input name="" id="" class="radio-checkbox" value="" type="checkbox" checked="checked">
                                <label for="">Сообщать о новых комментариях на почту</label>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-big btn-blue-white" value="Ответить">
                        <!--<div class="text-center">
                            <label for="contact-us-submit" class="animate-button">
                                <div class="btn-wrapper">
                                    <div class="btn-original">Опубликовать</div>
                                    <div class="btn-container">
                                        <div class="left-circle"></div>
                                        <div class="right-circle"></div>
                                        <div class="mask"></div>
                                    </div>
                                </div>
                            </label>
                        </div>-->
                    </form>
                </div>

            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="news-aside-block">
                    <div class="news-aside-title">Сейчас обсуждают</div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                    alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                    alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                    alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                    alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                    src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                    alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php
$script = <<<JS
    $(document.body).on('click','ul.menu-forum > li',function() {
        $('ul.menu-forum > li').removeClass('active');
        $(this).addClass('active');
        return false;
    });
    $(document.body).on('click','.close-reply-warning',function() {
        $('.reply-warning').hide();
        return false;
    });
JS;
$this->registerJs($script);