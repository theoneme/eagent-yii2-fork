<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 21.02.2019
 * Time: 15:13
 */

use common\components\CurrencyHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\assets\plugins\SlickAsset;

\common\assets\GoogleAsset::register($this);
SlickAsset::register($this);

/**
 * @var \frontend\forms\landing\ContactForm $contactForm
 */

?>

    <section class="top-block">
        <header class="flex space-between">
            <a class="new-home"><?= Yii::t('landing', 'Estet') ?></a>
            <div class="flex">
                <a class="fix-call-link text-center visible-xs" href="tel:+73433021448"> <i class="icon-phone"></i> </a>
                <a class="header-butn head-fix-call hidden-xs" href="tel:+73433021448">+7(343) 302-14-48</a>
                <?= Html::a(Yii::t('landing', 'Get a special offer'), [
                    '/page/ajax/special-offer'
                ], [
                    'class' => 'header-butn butn butn-blue-white text-center',
                    'data-action' => 'get-special-offer',
                    'data-target' => '#dynamic-modal',
                    'data-toggle' => 'modal'
                ]) ?>

                <nav class="navbar text-right">
                    <div class="nav-head">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-forum">
                            <i class="icon-list-menu"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="nav-forum">
                        <ul class="no-list menu">
                            <li><a href="#special"><?= Yii::t('landing', 'Special offers') ?></a></li>
                            <li><a href="#best"><?= Yii::t('landing', 'Our advantages') ?></a></li>
                            <!--<li><a href="#video"><?//= Yii::t('landing', 'How we get discounts') ?></a></li>-->
                            <li><a href="#buildings"><?= Yii::t('landing', 'New buildings') ?></a></li>
                            <li><a href="#contact"><?= Yii::t('landing', 'Contacts') ?></a></li>
                            <li><a href="tel:+73433021448">+7(343) 302-14-48</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <a class="global-call-link text-center hidden-xs" href="#" data-target="#call" data-toggle="modal"> <i class="icon-phone"></i> </a>
        <div class="center-block text-center">
            <h1>
                <?= Yii::t('landing', 'We can help you save from {from} to {to} {currency} <br> when you buy a condo in the New Building in {city}', [
                    'from' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 200000),
                    'to' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 1000000),
                    'currency' => null,
                    'city' => 'Екатеринбурга'
                ]) ?>
            </h1>
            <h2>
                <?= Yii::t('landing', 'We select the best of the available offers of developers. <br> We get the maximum discount and installments') ?>
            </h2>
            <div class="center-btns">
                <?= Html::a(Yii::t('landing', 'Get a special offer'), [
                    '/page/ajax/special-offer'
                ], [
                    'class' => 'butn blue text-center',
                    'data-action' => 'get-special-offer',
                    'data-target' => '#dynamic-modal',
                    'data-toggle' => 'modal'
                ]) ?>

                <?= Html::a(Yii::t('landing', 'Get a free consultation'), [
                    '/page/ajax/consultation'
                ], [
                    'class' => 'butn white text-center',
                    'data-action' => 'get-consultation',
                    'data-target' => '#dynamic-modal',
                    'data-toggle' => 'modal'
                ]) ?>
            </div>
        </div>

        <div class="top-block-bottom">
            <div class="text-center text-bottom"> <?= Yii::t('landing', 'We select the offers of banks on the mortgage for free and make a deal') ?></div>
            <div class="banks flex space-between">
                <div class="bank-item"><img src="/images/landing/bank1.png" alt="bank" title="bank"></div>
                <div class="bank-item"><img src="/images/landing/bank2.png" alt="bank" title="bank"></div>
                <div class="bank-item"><img src="/images/landing/bank3.png" alt="bank" title="bank"></div>
                <div class="bank-item"><img src="/images/landing/bank4.png" alt="bank" title="bank"></div>
                <div class="bank-item"><img src="/images/landing/bank5.png" alt="bank" title="bank"></div>
                <div class="bank-item"><img src="/images/landing/bank6.png" alt="bank" title="bank"></div>
                <div class="bank-item"><img src="/images/landing/bank7.png" alt="bank" title="bank"></div>
            </div>
        </div>
    </section>

    <section id="best" class="wrapper-small text-center">
        <h2><?= Yii::t('landing', 'We work with all developers of {city}', ['city' => 'Екатеринбурга']) ?></h2>

        <div class="clearfix">
            <div class="col-sm-4">
                <div class="cols-title">
                    <?= Yii::t('landing', 'We get the maximum discount due to sales') ?>
                </div>
                <div class="cols-p">
                    <?= Yii::t('landing', 'Developers provide us with maximum discounts due to our sales.') ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="cols-title">
                    <?= Yii::t('landing', 'We provide actual offers from developers') ?>
                </div>
                <div class="cols-p">
                    <?= Yii::t('landing', 'We track and provide actual discounts, installments and other important information from developers.') ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="cols-title">
                    <?= Yii::t('landing', 'We select condos and register transaction for free') ?>
                </div>
                <div class="cols-p">
                    <?= Yii::t('landing', 'Developer pays us a fee for making a deal.') ?>
                </div>
            </div>
        </div>
    </section>

    <section id="special" class="gray-bg">
        <div class="wrapper-small">
            <h2 class="text-center">
                <?= Yii::t('landing', 'Examples of actual special offers') ?>
            </h2>
            <div id="mob-slider" class="clearfix special-block">
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img width">
                            <img src="/images/landing/special1.jpeg" alt="1-комнатная 34,2 м²" title="1-комнатная 34,2 м²">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    ЖК «Меридиан» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                1-комнатная 34,2 м²
                            </div>
                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 6]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite-half"></i>
                                </div>
                                <?= Yii::t('landing', 'Discount: <strong>{sum}</strong> {currency}', [
                                    'sum' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 210000),
                                    'currency' => null
                                ]) ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img width">
                            <img src="/images/landing/special2.png" alt="" title="">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    ЖК «Хрустальные ключи» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                Студия 21,06 м²
                            </div>

                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 6]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                <?= Yii::t('landing', 'Discount: <strong>{sum}</strong> {currency}', [
                                    'sum' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 270000),
                                    'currency' => null
                                ]) ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img width">
                            <img src="/images/landing/special3.png" alt="" title="">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    Квартал «Суходольский» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                2-комнатная 60,62 м²
                            </div>
                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 3]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite-half"></i>
                                </div>

                                <?= Yii::t('landing', 'Discount: <strong>{sum}</strong> {currency}', [
                                    'sum' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 200000),
                                    'currency' => null
                                ]) ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img">
                            <img src="/images/landing/special4.jpg" alt="" title="">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    ЖК «Луч» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                Студия 20,28 м²
                            </div>
                            <div class="special-price">
                            </div>
                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 5]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite-half"></i>
                                </div>
                                <?= Yii::t('landing', 'Discount: <strong>{sum}</strong> {currency}', [
                                    'sum' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 270000),
                                    'currency' => null
                                ]) ?>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img width">
                            <img src="/images/landing/special5.jpg" alt="" title="">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    ЖК «Луч» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                3-комнатная 80,86 м²
                            </div>

                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 6]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                <?= Yii::t('landing', 'Discount: <strong>{sum}</strong> {currency}', ['sum' => 270000, 'currency' => 'рублей']) ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img width">
                            <img src="/images/landing/special6.jpg" alt="" title="">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    ЖК «Бриз» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                2-комнатная 40,44 м²
                            </div>
                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 5]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                <?= Yii::t('landing', 'Discount: <strong>{sum}</strong> {currency}', [
                                    'sum' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 350000),
                                    'currency' => null
                                ]) ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img width">
                            <img src="/images/landing/special7.jpg" alt="" title="">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    Квартал «Суходольский» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                3-комнатная 77 м²
                            </div>

                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 4]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                </div>
                                <?= Yii::t('landing', 'Specials:') ?>
                                <?= Yii::t('landing', 'Interior') ?>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-6 special-over">
                    <a href="#" class="special-item">
                        <div class="special-img width">
                            <img src="/images/landing/special8.png" alt="" title="">
                        </div>
                        <div class="special-info">
                            <div class="special-text">
                                <p>
                                    ЖК «Бриз» - Екатеринбург
                                </p>
                            </div>
                            <div class="special-type">
                                1-комнатная 30,82 м²
                            </div>
                            <div class="special-price">
                                <?= Yii::t('landing', 'Installment: {month, plural, one{# month} other{# months}}', ['month' => 5]) ?>
                            </div>
                            <div class="special-price">
                                <div class="rate">
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite"></i>
                                    <i class="icon-favorite-half"></i>
                                </div>
                                <?= Yii::t('landing', 'Discount: <strong>{sum}</strong> {currency}', [
                                    'sum' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 200000),
                                    'currency' => null
                                ]) ?>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="text-center">
                <a class="butn butn-blue-white" href="#"><?= Yii::t('landing', 'Get all special offers') ?></a>
            </div>
        </div>
    </section>

    <section id="video" class="gray-bg hidden">
        <div class="wrapper-small">
            <h2 class="text-center">
                <?= Yii::t('landing', 'How do we get the best conditions for our clients?') ?>
            </h2>
            <h3 class="text-center">
                <?= Yii::t('landing', 'Due to the volume of sold condos and knowledge of the market situation, we get the best possible conditions') ?>
            </h3>
            <div class="review-slider">
                <div class="rev-slide">
                    <div class="rev-slide-info">
                        <div class="info-center">
                            <div class="rev-slide-title">
                                <?= Yii::t('landing', 'How to buy and save?'); ?>
                            </div>
                            <blockquote>
                                <?= Yii::t('landing', 'The developers give the best price when you sell more than {count} apartments per month, and you become an important partner...', ['count' => 10]); ?>
                            </blockquote>
                            <cite>
                                <?= Yii::t('landing', 'Artyom Kokoteev'); ?>,
                                <span>
                                <?= Yii::t('landing', 'CEO of the company "Estet"'); ?>
                            </span>
                            </cite>
                        </div>
                    </div>
                    <div class="rev-slide-video">
                        <a class="href-video" data-src="/video/review/<?= Yii::$app->language ?>/video.mp4"
                           data-toggle="modal" data-target="#video-modal">
                            <?= Html::img("/images/locale/" . Yii::$app->language . "/review.jpg", ['alt' => Yii::t('landing', 'How to buy and save?'), 'title' => Yii::t('landing', 'How to buy and save?')]) ?>
                            <div class="play text-center">
                                <i class="icon-play-button"></i>
                            </div>
                            <div class="slide-info-mobile">
                                <div class="rev-slide-title">
                                    <?= Yii::t('landing', 'How to buy and save?'); ?>
                                </div>
                                <blockquote>
                                    <?= Yii::t('landing', 'The developers give the best price when you sell more than {count} apartments per month, and you become an important partner...', ['count' => 10]); ?>
                                </blockquote>
                                <cite>
                                    <?= Yii::t('landing', 'Artyom Kokoteev'); ?>,
                                    <span>
                                    <?= Yii::t('landing', 'CEO of the company "Estet"'); ?>
                                </span>
                                </cite>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="buildings">
        <div class="wrapper-small">
            <h2 class="text-center">
                <?= Yii::t('landing', 'All complexes under construction in one place') ?>
            </h2>
            <h3 class="text-center">
                <?= Yii::t('landing', 'We work with all developers and <br/> we will provide relevant information on any building under construction.') ?>
            </h3>
        </div>
        <div class="flex flex-wrap building-block">
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building1.webp" alt="Мичуринский" title="Мичуринский">
                </div>
                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"><?= Yii::t('landing', 'Developer') ?> ЛСР. Недвижимость-Урал</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 8.25]) ?></div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Installment for half a year') ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Мичуринский"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building2.webp" alt="Космонавтов 11" title="Космонавтов 11">
                </div>

                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"><?= Yii::t('landing', 'Developer') ?> Группа Компаний ПИК</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 2]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 6.0]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Free taxi to the sales office') ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Космонавтов 11"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building3.webp" alt="Светлый" title="Светлый">
                </div>

                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"><?= Yii::t('landing', 'Developer') ?> ГК "ТЭН"</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 9.3]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text">
                                    <?= Yii::t('landing', 'Installment for {year, plural, one{# year} other{# years}}', ['year' => 5]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Светлый"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building4.webp" alt="Геометрия" title="Геометрия">
                </div>

                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"> ГК "ТЭН"</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 9.9]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text">
                                    <?= Yii::t('landing', 'Installment for {year, plural, one{# year} other{# years}}', ['year' => 1]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Геометрия"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building5.webp" alt="Шишимская горка" title="Шишимская горка">
                </div>

                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"><?= Yii::t('landing', 'Developer') ?> Брусника</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 7.8]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text">
                                    <?= Yii::t('landing', 'Installment for {year, plural, one{# year} other{# years}}', ['year' => 1]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Шишимская горка"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building6.webp" alt="" title="">
                </div>

                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder">Хрустальные ключи</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 8.25]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text">
                                    <?= Yii::t('landing', 'Installment for half a year') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Хрустальные ключи"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building7.webp" alt="Флагман" title="Флагман">
                </div>

                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"><?= Yii::t('landing', 'Developer') ?> ЛСР. Недвижимость-Урал</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 8.25]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text">
                                    <?= Yii::t('landing', 'Installment for half a year') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Флагман"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building8.webp" alt="Луч" title="Луч">
                </div>

                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"><?= Yii::t('landing', 'Developer') ?> ГК "ТЭН"</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 9.5]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text">
                                    <?= Yii::t('landing', 'Installment for {year, plural, one{# year} other{# years}}', ['year' => 1]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Луч"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
            <a href="<?= Url::to(['/page/ajax/presentation']) ?>" data-target="#dynamic-modal" data-toggle="modal"
               data-action="load-construction"
               class="building-item">
                <div class="building-img">
                    <img src="/images/landing/building9.webp" alt="Сказка" title="Сказка">
                </div>
                <div class="building-info">
                    <div class="building-top">
                        <div class="building-builder"><?= Yii::t('landing', 'Developer') ?> ГК "ТЭН"</div>
                        <div class="building-stickers">
                            <div class="sticker purple active">
                                <div class="stick-img">
                                    <img src="/images/landing/percent.png" alt="percent" title="percent">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Discount {percent}%', ['percent' => 3]) ?></div>
                            </div>
                            <div class="sticker green">
                                <div class="stick-img">
                                    <img src="/images/landing/mortgage.png" alt="mortgage" title="mortgage">
                                </div>
                                <div class="stick-text"><?= Yii::t('landing', 'Mortgage {percent}%', ['percent' => 9.3]) ?> </div>
                            </div>
                            <div class="sticker yellow">
                                <div class="stick-img">
                                    <img src="/images/landing/discount.png" alt="discount" title="discount">
                                </div>
                                <div class="stick-text">
                                    <?= Yii::t('landing', 'Installment for {year, plural, one{# year} other{# years}}', ['year' => 1]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="building-title text-center">
                        <?= Yii::t('landing', 'Residential complex') ?>
                        <div class="building-title-bold">"Сказка"</div>
                        <div class="butn white butn-building"><?= Yii::t('landing','See more')?></div>
                    </div>
                </div>
            </a>
        </div>
        <div class="text-center show-more">
            <?= Html::a(Yii::t('landing', 'Get a free presentation'), [
                '/page/ajax/get-presentation'
            ], [
                'class' => 'butn butn-blue-white',
                'data-action' => 'get-presentation',
                'data-target' => '#dynamic-modal',
                'data-toggle' => 'modal'
            ]) ?>
        </div>
    </section>

    <section class="border-top wrapper-small text-center">
        <h2><?= Yii::t('landing', '<strong> Estet </strong> company in numbers') ?></h2>

        <div class="clearfix">
            <div class="col-sm-4">
                <div class="cols-title">
                    <?= Yii::t('landing', 'We received discounts for buyers in the amount of {price} {currency}', [
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 2300000),
                        'currency' => null
                    ]) ?>
                </div>
                <div class="cols-p">
                    <?= Yii::t('landing', 'We have set more than 100 agreements on buying properties in new constructions') ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="cols-title">
                    <?= Yii::t('landing', 'Agreed mortgage loans totaling more than {price} {currency}', [
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000000000),
                        'currency' => null
                    ]) ?>
                </div>
                <div class="cols-p">
                    <?= Yii::t('landing', 'We approve {percent}% of requests for mortgage loans and select the most favorable rates', ['percent' => 98]) ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="cols-title">
                    <?= Yii::t('landing', 'More than {price} {currency} of commission returned to customers', [
                        'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], 5000000),
                        'currency' => null
                    ]) ?>
                </div>
                <div class="cols-p">
                    <?= Yii::t('landing', 'We return to buyers 50% of the remuneration received from the developer', ['percent' => 50]) ?>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="gray-bg">
        <div class="wrapper-small">
            <h2 class="text-center"><?= Yii::t('landing', 'Contact us') ?></h2>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['/page/ajax/contact-us']),
                'id' => 'contact-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
            ]); ?>
            <div class="clearfix">
                <div class="col-sm-6">
                    <?= $form->field($contactForm, 'name')->textInput() ?>
                    <?= $form->field($contactForm, 'phone')->textInput() ?>
                    <?= $form->field($contactForm, 'email')->textInput() ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($contactForm, 'message')->textarea(['rows' => 6]) ?>
                    <input type="submit" class="butn butn-form  butn-blue-white" value="<?= Yii::t('landing', 'Send Message') ?>">
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </section>

    <section class="">
        <div class="clearfix">
            <div class="col-lg-9 col-md-8 col-sm-6 no-padding">
                <div id="map" class="footer-map">

                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6 no-padding">
                <div class="contact">
                    <div class="contact-block">
                        <a href="tel:+73433021448" class="phone">+7(343) 302-14-48</a>
                    </div>
                    <div class="contact-block">
                        <p><?=Yii::t('landing','Address')?></p>
                        <address>г. Екатеринбург, улица Белинского 44д</address>
                    </div>
                    <div class="contact-block">
                        <p><?=Yii::t('landing','E-mail:')?></p>
                        <a href="mailto:info@eagent.me">info@eagent.me</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="wrapper-small text-center">
        <p>
            <?= Yii::t('landing', 'We draw your attention to the fact that this website is for informational purposes only and under no circumstances is not a public offer determined by the provisions of Article 437 (2) of the Civil Code of the Russian Federation.') ?>
            <?= Yii::t('landing', 'For detailed information on the availability and cost of the specified goods and (or) services, please contact the site manager using a special form of communication or by phone {phone} or by email {email}', [
                'phone' => Html::a('+7(343) 302-14-48', 'tel:+73433021448'),
                'email' => Html::a('info@eagent.me', 'mailto:info@eagent.me')
            ]) ?>
        </p>
    </footer>

    <div id="call" class="fade new-modal modal-call modal in" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="modal-title text-center">ESTET</div>
                </div>
                <div class="modal-body">
                    <div class="call-text">
                        <?= Yii::t('landing', 'Hello! We can call you back in 1 minute and answer all your questions!')?>
                    </div>
                    <form id="" action="" method="post">
                        <div class="row">
                            <div class="col-sm-4 call-item"><input type="text" name="" placeholder="+7(___)___-__-__"></div>
                            <div class="col-sm-4 call-item text-center"><input id="call-submit" type="submit" class="butn butn-blue-white" value="<?=Yii::t('landing', 'I am waiting for a call!')?>"></div>
                            <div id="timer" class="col-sm-4 col-xs-12 timer call-item"></div>
                        </div>
                        <div class="text-center choose-time-block">
                            <i class="icon-hours"></i> <a class="choose-time-link"><?= Yii::t('landing','Choose a convenient time to call')?></a>
                            <div class="time-hide hidden row">
                                <div class="col-sm-10 col-sm-offset-1">
                                    <input type="text" name="" placeholder="<?= Yii::t('landing','Enter the time when you what to receive a call')?>">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php $script = <<<JS

    function timer(){
        var minute = new Date().getTime() + 60000;
        $("#timer").countdown(minute, function(event) {
            $(this).html(event.strftime('%M:%S'));
        });
    }

    $('#call').on('shown.bs.modal', function (e) {
        timer();
    });

    $(document.body).on('click', '.choose-time-link', function(){
       $(this).siblings('.time-hide').removeClass('hidden'); 
    });
    
    $(document).on('mouseenter', '.sticker', function() {
        if (!$(this).hasClass('active')) {
            if ($(window).width() > 1400) {
                $(this).siblings().removeClass('active');
            }
            $(this).addClass('active');
        }
    });
    
    $(document).on('mouseleave', '.sticker', function() {
        if ($(window).width() <= 1400) {
            $(this).removeClass('active');
        }
    });
    
    function mobileMenu() {
        if ($(window).width() <= 1024) {
            if (!$('.top-block').hasClass('nav-show')) {
                $('.top-block').addClass('nav-show');
            }
        } else {
            if ($('.top-block').hasClass('nav-show')) {
                $('.top-block').removeClass('nav-show');
            }
        }
    }
    
    mobileMenu();
    $(window).resize(function() {
        if ($(window).width() <= 1400) {
            $('.sticker').removeClass('active');
        }
        mobileMenu();
    });
    
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 500) {
            if (!$('header').hasClass('fix')) {
                if (!$('.top-block').hasClass('nav-show')) {
                    $('.top-block').addClass('nav-show');
                }
                $('header').addClass('fix');
            }
        } else {
            if ($('header').hasClass('fix')) {
                if ($('.top-block').hasClass('nav-show')) {
                    if ($(window).width() > 1024) {
                        $('.top-block').removeClass('nav-show');
                    }
                }
                $('header').removeClass('fix');
            }
        }
    });
    
    if ($(window).width() <= 1400) {
        $('.sticker').removeClass('active');
    }
    $('a[href*="#"]').click(function() {
        var el = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $(el).offset().top
        }, 1000);
        return false;
    });
    
    let dynamicModal = $('#dynamic-modal');
    $(document).on('click', '[data-action=get-special-offer], [data-action=get-consultation], [data-action=get-presentation], [data-action=load-construction]', function () { 
        if($(this).attr('data-action')==='load-construction'){
            dynamicModal.find('.modal-dialog').addClass('modal-lg');
        } else if (dynamicModal.find('.modal-dialog').hasClass('modal-lg')){
            dynamicModal.find('.modal-dialog').removeClass('modal-lg');
        }
        
        $.get($(this).attr('href'), {}, function (result) {
            if (result.success === true) {
                dynamicModal.find('.modal-content').html(result.html);
            }
        });

        return false;
    });
    
    $('#contact-form').on('beforeSubmit', function() { 
        let that = $(this);
        $.post($(this).attr('action'), $(this).serialize(), function(result) {
             if(result.success === true) {
                 that.trigger('reset');
             }
        });
        
        return false;
    });
    
    let position = {lat: 55.828801, lng: 37.297655},
        map = new google.maps.Map(document.getElementById('map'), {zoom: 17, center: position}),
        infoWindow = new google.maps.InfoWindow({
            content: '<p>Бизнес центр Панорама. Адрес: Екатеринбург, ул Белинского 44Д, 2 этаж Офис 205</p>'
        }),
        marker = new google.maps.Marker({
            position: position, 
            map: map,
            title: 'Бизнес центр Панорама. Адрес: Екатеринбург, ул Белинского 44Д, 2 этаж Офис 205'
        });
    
    marker.addListener('click', function() {
      infoWindow.open(map, marker);
    });
    
    var slickDir = false;
    if($('body').hasClass('rtl')){
        slickDir = true;
    }
    
    if($(window).width()<=768){
        $("#mob-slider").slick({
            dots: false,
            arrows: false,
            infinite: true,
            autoplay: false,
            speed: 300,
            slidesToShow: 1,
            centerMode: false,
            variableWidth: true,
            rtl: slickDir
        });
    }
JS;
$this->registerJs($script);