<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 09.11.2018
 * Time: 13:43
 */

use frontend\assets\FormHtmlAsset;
FormHtmlAsset::register($this);

?>
<a class="btn btn-blue-white btn-big" href="#" data-toggle="modal" data-target="#details">Click</a>
<div id="details" class="modal-steps modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="text-center" id="screen1">
                        <div class="question-step">Вы планируете купить квартиру?</div>
                        <div class="form-group">
                            <div class="chover">
                                <input id="buy-yes" class="radio-checkbox" name="buy" value="Да" type="radio">
                                <label for="buy-yes" class="btn btn-blue-white btn-big btn-radio">Да</label>
                            </div>
                            <div class="chover">
                                <input id="buy-no" class="radio-checkbox" name="buy" value="Нет" type="radio">
                                <label for="buy-no" class="btn btn-blue-white btn-big btn-radio" data-dismiss="modal" aria-hidden="true">Нет</label>
                            </div>
                        </div>
                    </div>
                    <div class="text-center hide" id="screen2">
                        <div class="form-group">
                            <label class="question-step" for="month">В течении какого месяца вы планируете купить</label>
                            <select class="form-control" id="month">
                                <option value="Январь">Январь</option>
                                <option value="Февраль">Февраль</option>
                                <option value="Март">Март</option>
                                <option value="Апрель">Апрель</option>
                                <option value="Май">Май</option>
                                <option value="Июнь">Июнь</option>
                                <option value="Июль">Июль</option>
                                <option value="Август">Август</option>
                                <option value="Сентябрь">Сентябрь</option>
                                <option value="Октябрь">Октябрь</option>
                                <option value="Ноябрь">Ноябрь</option>
                                <option value="Декабрь">Декабрь</option>
                            </select>
                        </div>
                    </div>
                    <div class="text-center hide" id="screen3">
                        <div class="question-step">Вам нужен Ипотечный кредит?</div>
                        <div class="form-group">
                            <div class="chover">
                                <input id="loan-term-yes" class="radio-checkbox" name="loan-term" value="Да" type="radio">
                                <label for="loan-term-yes" class="btn btn-blue-white btn-big btn-radio">Да</label>
                            </div>
                            <div class="hide flex space-between step-field" id="credit-amount">
                                <input type="text" placeholder="Сумма кредита">
                                <a href="#" id="goStep4" class="btn btn-blue-white btn-small">Далее</a>
                            </div>
                            <div class="chover">
                                <input id="loan-term-no" class="radio-checkbox" name="loan-term" value="Нет" type="radio">
                                <label for="loan-term-no" class="btn btn-blue-white btn-big btn-radio">Нет</label>
                        </div>
                        </div>
                    </div>
                    <div class="text-center hide" id="screen4">
                        <div class="question-step">Вы планируете продавать вторичное жилье?</div>
                        <div class="form-group">
                            <div class="chover">
                                <input id="other-property-yes" class="radio-checkbox" name="loan-term" value="Да" type="radio">
                                <label for="other-property-yes" class="btn btn-blue-white btn-big btn-radio">Да</label>
                            </div>
                            <div class="hide flex space-between step-field" id="address">
                                <input type="text" placeholder="Введите адрес">
                                <a href="#" id="goStep5" class="btn btn-blue-white btn-small">Далее</a>
                            </div>
                            <div class="chover">
                                <input id="other-property-no" class="radio-checkbox" name="loan-term" value="Нет" type="radio">
                                <label for="other-property-no" class="btn btn-blue-white btn-big btn-radio">Нет</label>
                            </div>
                        </div>
                    </div>
                    <div class="text-center hide" id="screen5">
                        <div class="question-step">Спасибо за ответы</div>
                        <div class="form-group">
                            <div class="chover">
                                <input id="end" class="btn btn-blue-white btn-big btn-radio" value="Ок" type="submit" data-dismiss="modal" aria-hidden="true">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="contact-agent-block hide thanks-modal">
    <h2 class="up-title text-left">Thank You!</h2>
    <p class="green-p">Your request has been sent. An agent will connect with you shortly.</p>
</div>

<?php $script = <<<JS

    $(document.body).on('click', '#buy-yes', function () {
      $('div#screen1').toggleClass('hide').next().toggleClass('hide');
    });
    $(document.body).on('change', '#month', function () {
      $('div#screen2').toggleClass('hide').next().toggleClass('hide');
    });
    $(document.body).on('click', '#loan-term-no,#goStep4', function () {
      $('div#screen3').toggleClass('hide').next().toggleClass('hide');
      return false;
    });
    $(document.body).on('click', '#loan-term-yes', function () {
      $('#credit-amount').toggleClass('hide');
    });
    $(document.body).on('click', '#other-property-no,#goStep5', function () {
      $('div#screen4').toggleClass('hide').next().toggleClass('hide');
      return false;
    });
    $(document.body).on('click', '#other-property-yes', function () {
      $('#address').toggleClass('hide');
    });
    $(document.body).on('click', '#end', function () {
        $('.thanks-modal').toggleClass('hide');
        setTimeout(function() {   
            $('.thanks-modal').toggleClass('hide');
        }, 5000);
        return false;
    });
JS;

$this->registerJs($script); ?>