<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 04.10.2018
 * Time: 13:52
 */

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\HomesListAsset::register($this);
\frontend\assets\plugins\FancyBoxAsset::register($this);

?>

<div class="list-homes-wrap clearfix">
    <div class="col-md-8 catalog-left">
        <div class="lh-user-block">
            <div class="lh-user-photo">
                <img src="../images/veles.jpg" alt="user">
            </div>
            <div class="lh-user-info">
                <p>
                    Подборка подготовлена 21.09.2018
                </p>
                <p>
                    <strong>Имя агента</strong>
                </p>
                <p>
                    Агенство
                </p>
                <p>
                    <strong>+7 (978) 366 55 55</strong>
                </p>
                <p>
                    <a href="mailto:">mail@gmail.com</a>
                </p>
            </div>
        </div>
        <div class="home-list">
            <div class="list-home-item">
                <div class="lh-main-block">
                    <div class="lh-title">
                        <div class="lh-title-text">
                            <span class="counter">1.</span> 4 комнаты по адресу ул. Кирова, д. 5
                        </div>
                        <div class="chover green-check">
                            <input id="home1" class="radio-checkbox" name="home1" type="checkbox">
                            <label for="home1">Записаться на просмотр</label>
                        </div>
                    </div>
                    <div class="lh-cols">
                        <div class="lh-photos">
                            <div class="lh-big-photo">
                                <a href="../images/home-ph1.jpeg" data-fancybox="viewer1">
                                    <img src="../images/home-ph1.jpeg" alt="home">
                                </a>
                            </div>
                            <div class="lh-small-photo">
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph3.jpeg" data-fancybox="viewer1">
                                        <img src="../images/home-ph3.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph4.jpeg" data-fancybox="viewer1">
                                        <img src="../images/home-ph4.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph5.jpeg" data-fancybox="viewer1">
                                        <img src="../images/home-ph5.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph1.jpeg" data-fancybox="viewer1">
                                        <img src="../images/home-ph1.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph3.jpeg" data-fancybox="viewer1">
                                        <img src="../images/home-ph3.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph4.jpeg" data-fancybox="viewer1">
                                        <img src="../images/home-ph4.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph5.jpeg" data-fancybox="viewer1">
                                        <img src="../images/home-ph5.jpeg" alt="home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="lh-params">
                            <div class="lh-params-row">
                                <div class="lh-params-title">Цена:</div>
                                <div class="lh-params-text">1 200 300 р</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Общая площадь:</div>
                                <div class="lh-params-text">100 м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Этаж / Этажей в доме:</div>
                                <div class="lh-params-text">4/5</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Цена за м²:</div>
                                <div class="lh-params-text">1 200/1 р/м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Жилая площадь:</div>
                                <div class="lh-params-text">80 м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Количество комнат:</div>
                                <div class="lh-params-text">4</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Балкон</div>
                                <div class="lh-params-text">1</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Лоджия</div>
                                <div class="lh-params-text">1</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Туалет</div>
                                <div class="lh-params-text">2</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Ванные</div>
                                <div class="lh-params-text">2</div>
                            </div>
                        </div>
                    </div>
                    <div class="lh-description">
                        <div class="lh-descr-title up-title text-left">Описание</div>
                        <div class="hide-content hide-text">
                            <div>
                                <p>
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                </p>
                                <p>
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                </p>
                            </div>
                        </div>
                        <a class="more-less" data-height="70">
                            <?= Yii::t('main', 'More') ?>
                            <i class="icon-down"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="list-home-item">
                <div class="lh-main-block">
                    <div class="lh-title">
                        <div class="lh-title-text">
                            <span class="counter">1.</span> 4 комнаты по адресу ул. Кирова, д. 5
                        </div>
                        <div class="chover green-check">
                            <input id="home2" class="radio-checkbox" name="home2" type="checkbox">
                            <label for="home2">Записаться на просмотр</label>
                        </div>
                    </div>
                    <div class="lh-cols">
                        <div class="lh-photos">
                            <div class="lh-big-photo">
                                <a href="../images/home-ph1.jpeg" data-fancybox="viewer2">
                                    <img src="../images/home-ph1.jpeg" alt="home">
                                </a>
                            </div>
                            <div class="lh-small-photo">
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph3.jpeg" data-fancybox="viewer2">
                                        <img src="../images/home-ph3.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph4.jpeg" data-fancybox="viewer2">
                                        <img src="../images/home-ph4.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph5.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph5.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph1.jpeg" data-fancybox="viewer2">
                                        <img src="../images/home-ph1.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph3.jpeg" data-fancybox="viewer2">
                                        <img src="../images/home-ph3.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph4.jpeg" data-fancybox="viewer2">
                                        <img src="../images/home-ph4.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph5.jpeg" data-fancybox="viewer2">
                                        <img src="../images/home-ph5.jpeg" alt="home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="lh-params">
                            <div class="lh-params-row">
                                <div class="lh-params-title">Цена:</div>
                                <div class="lh-params-text">1 200 300 р</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Общая площадь:</div>
                                <div class="lh-params-text">100 м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Этаж / Этажей в доме:</div>
                                <div class="lh-params-text">4/5</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Цена за м²:</div>
                                <div class="lh-params-text">1 200/1 р/м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Жилая площадь:</div>
                                <div class="lh-params-text">80 м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Количество комнат:</div>
                                <div class="lh-params-text">4</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Балкон</div>
                                <div class="lh-params-text">1</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Лоджия</div>
                                <div class="lh-params-text">1</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Туалет</div>
                                <div class="lh-params-text">2</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Ванные</div>
                                <div class="lh-params-text">2</div>
                            </div>
                        </div>
                    </div>
                    <div class="lh-description">
                        <div class="lh-descr-title up-title text-left">Описание</div>
                        <div class="hide-content hide-text">
                            <div>
                                <p>
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                </p>
                                <p>
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                </p>
                            </div>
                        </div>
                        <a class="more-less" data-height="70">
                            <?= Yii::t('main', 'More') ?>
                            <i class="icon-down"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="list-home-item">
                <div class="lh-main-block">
                    <div class="lh-title">
                        <div class="lh-title-text">
                            <span class="counter">1.</span> 4 комнаты по адресу ул. Кирова, д. 5
                        </div>
                        <div class="chover green-check">
                            <input id="home3" class="radio-checkbox" name="home3" type="checkbox">
                            <label for="home3">Записаться на просмотр</label>
                        </div>
                    </div>
                    <div class="lh-cols">
                        <div class="lh-photos">
                            <div class="lh-big-photo">
                                <a href="../images/home-ph1.jpeg" data-fancybox="viewer3">
                                    <img src="../images/home-ph1.jpeg" alt="home">
                                </a>
                            </div>
                            <div class="lh-small-photo">
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph3.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph3.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph4.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph4.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph5.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph5.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph1.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph1.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph3.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph3.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph4.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph4.jpeg" alt="home">
                                    </a>
                                </div>
                                <div class="lh-small-photo-item">
                                    <a href="../images/home-ph5.jpeg" data-fancybox="viewer3">
                                        <img src="../images/home-ph5.jpeg" alt="home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="lh-params">
                            <div class="lh-params-row">
                                <div class="lh-params-title">Цена:</div>
                                <div class="lh-params-text">1 200 300 р</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Общая площадь:</div>
                                <div class="lh-params-text">100 м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Этаж / Этажей в доме:</div>
                                <div class="lh-params-text">4/5</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Цена за м²:</div>
                                <div class="lh-params-text">1 200/1 р/м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Жилая площадь:</div>
                                <div class="lh-params-text">80 м²</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Количество комнат:</div>
                                <div class="lh-params-text">4</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Балкон</div>
                                <div class="lh-params-text">1</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Лоджия</div>
                                <div class="lh-params-text">1</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Туалет</div>
                                <div class="lh-params-text">2</div>
                            </div>
                            <div class="lh-params-row">
                                <div class="lh-params-title">Ванные</div>
                                <div class="lh-params-text">2</div>
                            </div>
                        </div>
                    </div>
                    <div class="lh-description">
                        <div class="lh-descr-title up-title text-left">Описание</div>
                        <div class="hide-content hide-text">
                            <div>
                                <p>
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                </p>
                                <p>
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                    text text text text text text text text text text
                                </p>
                            </div>
                        </div>
                        <a class="more-less" data-height="70">
                            <?= Yii::t('main', 'More') ?>
                            <i class="icon-down"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a class="btn btn-big btn-white-blue" href="#"> Отправить Агенту выбранные варианты </a>
        </div>
    </div>
    <div class="col-md-4 col-sm-12 catalog-right">
        <div class="map-over">
            <div id="map_catalog"></div>
        </div>
    </div>
</div>

<?php
$more = Yii::t('main', 'More');
$less = Yii::t('main', 'Less');

$script = <<<JS
    $(document).ready(function() {
        var moreLess = $('.more-less');
        moreLess.MoreLess({
            moreText: '{$more}&nbsp;',
            lessText: '{$less}&nbsp;'
        });
        $('.more-less').trigger('show');
        
        let countViewer = 0;
        $('.lh-photos').each(function() {
            countViewer++;
            let viewer = 'viewer' + countViewer;
            $('[data-fancybox="'+viewer+'"]').fancybox({
                buttons: [
                    'close'
                ],
                infobar: false,
                caption: false
            });
        })
    });

    
    
JS;
$this->registerJsFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyB9nI8GFycVFBlWVH-qM2ovMQOB7zpKjbA');
$this->registerJs($script);
