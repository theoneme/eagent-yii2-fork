<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 26.12.2018
 * Time: 18:20
 */


\frontend\assets\ForumAsset::register($this);
\frontend\assets\CatalogAsset::register($this);

?>

<div class="page-wrap">
    <div class="row">
        <div class="col-lg-2">
            <nav class="navbar aside-forum text-left">
                <div class="nav-head">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-forum">
                        Вопросы <i class="icon-down"></i>
                    </button>
                </div>
            <div class="collapse navbar-collapse" id="nav-forum">
                <ul class="no-list menu-forum">
                    <li>
                        <a href="#">Какой-то пункт</a>
                        <ul>
                            <li><a href="">Специалист–специалисту</a></li>
                            <li><a href="">Тенденции рынка</a></li>
                        </ul>
                    </li>
                    <li class="active">
                        <a href="">Вопросы</a>
                        <ul>
                            <li><a href=""><span class="plus-question"></span>Задать вопрос</a></li>
                            <li><a href="">Специалист–специалисту</a></li>
                            <li><a href="">Тенденции рынка</a></li>
                            <li><a href="">Городская недвижимость</a></li>
                            <li><a href="">Коммерческая недвижимость</a></li>
                            <li><a href="">Загородная недвижимость</a></li>
                            <li><a href="">Зарубежная недвижимость</a></li>
                            <li><a href="">Ипотека</a></li>
                        </ul>
                    </li>
                </ul>
            </div></nav>
        </div>
        <div class="col-lg-7 col-md-8 col-sm-8">
            <div class="breadcrumbs hidden-xs">
                <ul class="no-list">
                    <li>
                        <a href="#">Форум</a>
                    </li>
                    <li>
                        <a href="#">Вопросы</a>
                    </li>
                </ul>
            </div>
            <h1 class="text-left">Вопросы риелтору</h1>
            <div class="flex space-between form-top">
                <a class="ask btn btn-small btn-blue-white" href="#">+ Задать вопрос</a>
                <ul class="no-list forum-top-alias">
                    <li><a href="#">Новое</a></li>
                    <li><a href="#">Популярное</a></li>
                    <li><a href="#">Обсуждаемое</a></li>
                </ul>
            </div>
            <div class="forum-list">
                <article class="forum-list-item">
                    <div class="nav-theme">
                        <a href="#">Вопросы риелтору</a>
                        <a href="#">Городская недвижимость</a>
                    </div>
                    <a class="article-title" href="#">Сделка через нотариуса. С какого момента я стану собственником?</a>
                    <p>
                        Купил квартиру у 2 собственников (ьаьа и дочь) через нотариуса. В договоре написана стандартная фраза, что акта передачи квартиры не будет. Договор сейчас сдан для в...
                    </p>
                    <div class="article-author">
                        <div class="article-author-avatar">
                            <img src="/images/veles-min.jpg" alt="" title="">
                        </div>
                        <div class="article-author-info">Иннокентий</div>
                    </div>
                    <div class="article-info">
                        <div class="article-info-item">26 декабря 2018</div>
                        <div class="article-info-item"><i class="icon-eye"></i> 222</div>
                        <div class="article-info-item">
                            <a href="#"><i class="icon-comment-black-oval-bubble-shape"></i> 22</a>
                        </div>
                    </div>
                </article>
                <article class="forum-list-item">
                    <div class="nav-theme">
                        <a href="#">Вопросы риелтору</a>
                        <a href="#">Городская недвижимость</a>
                    </div>
                    <a class="article-title" href="#">Сделка через нотариуса. С какого момента я стану собственником?</a>
                    <p>
                        Купил квартиру у 2 собственников (ьаьа и дочь) через нотариуса. В договоре написана стандартная фраза, что акта передачи квартиры не будет. Договор сейчас сдан для в...
                    </p>
                    <div class="article-author">
                        <div class="article-author-avatar">
                            <img src="/images/veles-min.jpg" alt="" title="">
                        </div>
                        <div class="article-author-info">Иннокентий</div>
                    </div>
                    <div class="article-info">
                        <div class="article-info-item">26 декабря 2018</div>
                        <div class="article-info-item"><i class="icon-eye"></i> 222</div>
                        <div class="article-info-item">
                            <a href="#"><i class="icon-comment-black-oval-bubble-shape"></i> 22</a>
                        </div>
                    </div>
                </article>
                <article class="forum-list-item">
                    <div class="nav-theme">
                        <a href="#">Вопросы риелтору</a>
                        <a href="#">Городская недвижимость</a>
                    </div>
                    <a class="article-title" href="#">Сделка через нотариуса. С какого момента я стану собственником?</a>
                    <p>
                        Купил квартиру у 2 собственников (ьаьа и дочь) через нотариуса. В договоре написана стандартная фраза, что акта передачи квартиры не будет. Договор сейчас сдан для в...
                    </p>
                    <div class="article-author">
                        <div class="article-author-avatar">
                            <img src="/images/veles-min.jpg" alt="" title="">
                        </div>
                        <div class="article-author-info">Иннокентий</div>
                    </div>
                    <div class="article-info">
                        <div class="article-info-item">26 декабря 2018</div>
                        <div class="article-info-item"><i class="icon-eye"></i> 222</div>
                        <div class="article-info-item">
                            <a href="#"><i class="icon-comment-black-oval-bubble-shape"></i> 22</a>
                        </div>
                    </div>
                </article>
                <article class="forum-list-item">
                    <div class="nav-theme">
                        <a href="#">Вопросы риелтору</a>
                        <a href="#">Городская недвижимость</a>
                    </div>
                    <a class="article-title" href="#">Сделка через нотариуса. С какого момента я стану собственником?</a>
                    <p>
                        Купил квартиру у 2 собственников (ьаьа и дочь) через нотариуса. В договоре написана стандартная фраза, что акта передачи квартиры не будет. Договор сейчас сдан для в...
                    </p>
                    <div class="article-author">
                        <div class="article-author-avatar">
                            <img src="/images/veles-min.jpg" alt="" title="">
                        </div>
                        <div class="article-author-info">Иннокентий</div>
                    </div>
                    <div class="article-info">
                        <div class="article-info-item">26 декабря 2018</div>
                        <div class="article-info-item"><i class="icon-eye"></i> 222</div>
                        <div class="article-info-item">
                            <a href="#"><i class="icon-comment-black-oval-bubble-shape"></i> 22</a>
                        </div>
                    </div>
                </article>
                <article class="forum-list-item">
                    <div class="nav-theme">
                        <a href="#">Вопросы риелтору</a>
                        <a href="#">Городская недвижимость</a>
                    </div>
                    <a class="article-title" href="#">Сделка через нотариуса. С какого момента я стану собственником?</a>
                    <p>
                        Купил квартиру у 2 собственников (ьаьа и дочь) через нотариуса. В договоре написана стандартная фраза, что акта передачи квартиры не будет. Договор сейчас сдан для в...
                    </p>
                    <div class="article-author">
                        <div class="article-author-avatar">
                            <img src="/images/veles-min.jpg" alt="" title="">
                        </div>
                        <div class="article-author-info">Иннокентий</div>
                    </div>
                    <div class="article-info">
                        <div class="article-info-item">26 декабря 2018</div>
                        <div class="article-info-item"><i class="icon-eye"></i> 222</div>
                        <div class="article-info-item">
                            <a href="#"><i class="icon-comment-black-oval-bubble-shape"></i> 22</a>
                        </div>
                    </div>
                </article>
            </div>
            <ul class="pagination">
                <li class="prev disabled"><span>«</span></li>
                <li class="active"><a href="#" data-page="0">1</a></li>
                <li><a href="#" data-page="1">2</a></li>
                <li class="next"><a href="#" data-page="1">»</a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4">
            <div class="news-aside-block">
                <div class="news-aside-title">Сейчас обсуждают</div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                        src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                        alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                        src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                        alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                        src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                        alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                        src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                        alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                    <div class="related-post row" data-key="9211">
                        <div class="related-post-img">
                            <a href=""><img
                                        src="https://dwmh9kmdoyiua.cloudfront.net/uploads/page/2018/12/page-5c03be300293e.jpg"
                                        alt="" title=""></a></div>
                        <a class="related-alias" href="#">
                            Как поделить деньги?
                        </a>
                        <p>
                            Да причем тут сумма комиссионных? Риэлтор -профи должен поступить так. Уважаемые клиенты-вы должны -150тыс за услуги риэлтора ,130тыс ..
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php
$script = <<<JS
    $(document.body).on('click','ul.menu-forum > li',function() {
        $('ul.menu-forum > li').removeClass('active');
        $(this).addClass('active');
        return false;
    });
JS;
$this->registerJs($script);