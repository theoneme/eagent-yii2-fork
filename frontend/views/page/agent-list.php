<?php
/**
 * Created by PhpStorm.
 * User: Алёнка
 * Date: 25.09.2018
 * Time: 14:45
 */

\frontend\assets\CatalogAsset::register($this);
\frontend\assets\AgentAsset::register($this);

?>
<div class="page-wrap">
    <div>
        <div class="city-top">Florida</div>
        <h1>Real Estate Agents in Florida</h1>
    </div>
    <div class="agent-filter row">
        <form class="col-md-12">
            <div class="form-group agent-filter-block">
                <label for="location">Location</label>
                <div class="input-group">
                    <input type="search" class="form-control" id="location" placeholder="Neighborhood/City/Zip">
                    <span class="input-group-addon"><i class="icon-search"></i></span>
                </div>
            </div>
            <div class="form-group agent-filter-block">
                <label for="name">Name</label>

                <div class="input-group">
                    <input type="search" class="form-control" id="name" placeholder="Agent name">
                    <span class="input-group-addon"><i class="icon-search"></i></span>
                </div>
            </div>
        </form>
    </div>
    <div class="agent-list">
        <div class="agent-top">
            <div class="agent-list-info">
                Reviews in FL <div class="tooltip-ea" data-position="right" data-width="310">
                    <div class="tooltip-modal">
                        <a href="" class="tooltip-close">&times;</a>
                        <div class="tooltip-body">
                            <p>
                                Agents are ordered using an algorithm that weighs different types of activity in the region, including reviews, sales in the last 12 months, and listings.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map-sale-over"></div>
            <div class="agent-list-review text-center">
                Client review
            </div>
        </div>
        <div class="agent-item">
            <div class="agent-list-info">
                <div class="agent-list-photo">
                    <img src="../images/veles.jpg" alt="agent">
                </div>
                <div class="agent-opt">
                    <div class="agent-list-name">
                        <a href="#">Heather Lefebvre &amp; Team OceanAreaLiving</a>
                    </div>
                    <div class="agent-list-phone">
                        (954) 951-4915
                    </div>
                    <div class="agent-rate">
                        <a href="#" class="agent-rate-alias">
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                        </a>
                    </div>
                    <a href="#" class="agent-list-count">21 total reviews (21 local)</a>
                </div>
            </div>
            <div class="map-sale-over text-center">
                <div class="map-sale">
                    <div class="territory text-left">
                        In FL:
                    </div>
                    <div class="agent-map-block">
                        <div class="agent-map-img" style="background-image:url('../images/maps/fl.png');"></div>
                        <div class="trapezoid">
                            <p>21 Local Reviews</p>
                            <p>No Recent Sales</p>
                            <p>No Listings</p>
                        </div>
                    </div>
                    <div class="agent-list-company text-right">
                        Coldwell Banker Residential Real Estate
                    </div>
                </div>
            </div>
            <div class="agent-list-review text-center">
                <div class="table-review">
                    <div class="cell-review">
                        <a class="agent-list-date" href="#">
                            Review 06/07/2018:
                        </a>
                        <p class="text-center">
                            "My wife and I highly recommend Liza to any one who is looking to buy a home."
                        </p>
                    </div>
                </div>
                <a href="http://eagent.front/flats-for-sale-catalog" class="go-page"></a>
            </div>
            <div class="agent-list-arrow">
                <a href="#">
                    <i class="icon-next"></i>
                </a>
            </div>
            <a href="/" class="go-page"></a>
        </div>
        <div class="agent-item">
            <div class="agent-list-info">
                <div class="agent-list-photo">
                    <img src="../images/veles.jpg" alt="agent">
                </div>
                <div class="agent-opt">
                    <div class="agent-list-name">
                        <a href="#">Heather Lefebvre &amp; Team OceanAreaLiving</a>
                    </div>
                    <div class="agent-list-phone">
                        (954) 951-4915
                    </div>
                    <div class="agent-rate">
                        <a href="#" class="agent-rate-alias">
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                        </a>
                    </div>
                    <a href="#" class="agent-list-count">21 total reviews (21 local)</a>
                </div>
            </div>
            <div class="map-sale-over text-center">
                <div class="map-sale">
                    <div class="territory text-left">
                        in FL:
                    </div>
                    <div class="agent-map-block">
                        <div class="agent-map-img" style="background-image:url('../images/maps/fl.png');"></div>
                        <div class="trapezoid">
                            <p>21 Local Reviews</p>
                            <p>No Recent Sales</p>
                            <p>No Listings</p>
                        </div>
                    </div>
                    <div class="agent-list-company text-right">
                        Coldwell Banker Residential Real Estate
                    </div>
                </div>
            </div>
            <div class="agent-list-review text-center">
                <div class="table-review">
                    <div class="cell-review">
                        <a class="agent-list-date text-center" href="#">
                            Review 06/07/2018:
                        </a>
                        <p class="text-center">
                            "My wife and I highly recommend Liza to any one who is looking to buy a home."
                        </p>
                    </div>
                </div>
                <a href="http://eagent.front/flats-for-sale-catalog" class="go-page"></a>
            </div>
            <div class="agent-list-arrow">
                <a href="#">
                    <i class="icon-next"></i>
                </a>
            </div>
            <a href="/" class="go-page"></a>
        </div>
        <div class="agent-item">
            <div class="agent-list-info">
                <div class="agent-list-photo">
                    <img src="../images/veles.jpg" alt="agent">
                </div>
                <div class="agent-opt">
                    <div class="agent-list-name">
                        <a href="#">Heather Lefebvre &amp; Team OceanAreaLiving</a>
                    </div>
                    <div class="agent-list-phone">
                        (954) 951-4915
                    </div>
                    <div class="agent-rate">
                        <a href="#" class="agent-rate-alias">
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                            <i class="icon-favorite"></i>
                        </a>
                    </div>
                    <a href="#" class="agent-list-count">21 total reviews (21 local)</a>
                </div>
            </div>
            <div class="map-sale-over text-center">
                <div class="map-sale">
                    <div class="territory text-left">
                        in FL:
                    </div>
                    <div class="agent-map-block">
                        <div class="agent-map-img" style="background-image:url('../images/maps/fl.png');"></div>
                        <div class="trapezoid">
                            <p>21 Local Reviews</p>
                            <p>No Recent Sales</p>
                            <p>No Listings</p>
                        </div>
                    </div>
                    <div class="agent-list-company text-right">
                        Coldwell Banker Residential Real Estate
                    </div>
                </div>
            </div>
            <div class="agent-list-review text-center">
                <div class="table-review">
                    <div class="cell-review">
                        <a class="agent-list-date text-center" href="#">
                            Review 06/07/2018:
                        </a>
                        <p class="text-center">
                            "My wife and I highly recommend Liza to any one who is looking to buy a home."
                        </p>
                    </div>
                </div>
                <a href="http://eagent.front/flats-for-sale-catalog" class="go-page"></a>
            </div>
            <div class="agent-list-arrow">
                <a href="#">
                    <i class="icon-next"></i>
                </a>
            </div>
            <a href="/" class="go-page"></a>
        </div>
    </div>
    <ul class="pagination">
        <li class="prev"><a href="#" data-page="1">«</a></li>
        <li class="active"><a href="#" data-page="0">1</a></li>
        <li><a href="#" data-page="1">2</a></li>
        <li><a href="#" data-page="2">3</a></li>
        <li><a href="#" data-page="3">4</a></li>
        <li class="next"><a href="#">»</a></li>
    </ul>
    <div class="col-md-12">
        <div class="agent-list-descr">
            <p>
                Whether you are looking to rent, buy or sell your home, Zillow's directory of local real estate agents and brokers in Florida connects you with professionals who can help meet your needs. Because the Florida real estate market is unique, it's important to choose a real estate agent or broker with local expertise to guide you through the process of renting, buying or selling your next home. Our directory helps you find real estate professionals who specialize in buying, selling, foreclosures, or relocation - among many other options. Alternatively, you could work with a local agent or real estate broker who provides an entire suite of buying and selling services.
            </p>
            <p>
                No matter what type of real estate needs you have, finding the local real estate professional you want to work with is the first step. The Florida real estate directory lets you view and compare real estate agents, read reviews, see an agent's current listings and past sales, and contact agents directly from their profile pages on Zillow.
            </p>
            <p>
                Zillow is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with the best local professionals who can help.
            </p>
            <p>
                Are you a real estate agent?
            </p>
            <p>
                Check out the extensive resources you can find in our Premier Agent® Resource Center, covering everything from business plan templates to complete guides on real estate marketing. Not a Premier Agent yet? Find out how real estate advertising on Zillow and Trulia can help you get more leads.
            </p>
        </div>
    </div>
</div>