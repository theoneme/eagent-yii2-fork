<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\catalog;

use common\models\Property;
use common\services\entities\CategoryService;
use common\services\entities\CityService;
use frontend\services\LocationService;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

/**
 * Class PropertyCatalogUrlService
 * @package frontend\services\catalog
 */
class PropertyCatalogUrlService
{
    /**
     * @var CategoryService
     */
    private $_categoryService;
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * CatalogSearchService constructor.
     * @param CategoryService $categoryService
     * @param CityService $cityService
     * @param LocationService $locationService
     */
    public function __construct(CategoryService $categoryService, CityService $cityService, LocationService $locationService)
    {
        $this->_categoryService = $categoryService;
        $this->_cityService = $cityService;
        $this->_locationService = $locationService;
    }

    /**
     * @param $queryParams
     * @param array $baseRoute
     * @return string
     */
    public function getUrl($queryParams, $baseRoute = ['/property/catalog/index'])
    {
        if (array_key_exists('category_id', $queryParams) && !array_key_exists('category', $queryParams)) {
            $category = $this->_categoryService->getOne(['category.id' => $queryParams['category_id']]);

            if ($category === null) {
                throw new BadRequestHttpException('Wrong category passed');
            }
            unset($queryParams['category_id']);
            $queryParams['category'] = $category['slug'];
        }

        if (array_key_exists('type', $queryParams) && !array_key_exists('operation', $queryParams)) {
            $operation = (int)$queryParams['type'] === Property::TYPE_RENT ? Property::OPERATION_RENT : Property::OPERATION_SALE;
            unset($queryParams['type']);
            $queryParams['operation'] = $operation;
        }

        if (array_key_exists('city', $queryParams)) {
            $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
            if ($queryParams['city'] !== $locationData['slug']) {
                $queryParams['app_city'] = $queryParams['city'];
                unset($queryParams['city']);
            }
        }

        return Url::to(array_merge($baseRoute, $queryParams));
    }
}