<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\catalog;

use common\helpers\SpatialHelper;
use common\interfaces\filters\PropertyFilterInterface;
use common\mappers\CatalogSortMapper;
use common\models\Property;
use common\repositories\elastic\PropertyRepository;
use common\services\entities\PropertyService;
use common\services\PropertyMarkerService;
use common\services\seo\CatalogSeoService;
use frontend\mappers\map\GooglePolygonMapper;
use frontend\services\LocationService;
use frontend\widgets\PropertyFilter;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PropertyCatalogDataService
 * @package frontend\services\catalog
 */
class PropertyCatalogDataService
{
    public const MODE_INDEX = 0;
    public const MODE_INDEX_AJAX = 10;

    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var LocationService
     */
    private $_locationService;
    /**
     * @var PropertyFilterInterface
     */
    private $_propertyFilterService;
    /**
     * @var CatalogSeoService
     */
    private $_catalogSeoService;
    /**
     * @var PropertyRepository
     */
    private $_elasticPropertyRepository;
    /**
     * @var LocationToFilterService
     */
    private $_locationToFilterService;

    /**
     * PropertyCatalogDataService constructor.
     * @param PropertyService $propertyService
     * @param LocationService $locationService
     * @param PropertyFilterInterface $propertyFilterService
     * @param PropertyRepository $elasticPropertyRepository
     * @param LocationToFilterService $locationToFilterService
     * @param CatalogSeoService $catalogSeoService
     */
    public function __construct(PropertyService $propertyService,
                                LocationService $locationService,
                                PropertyFilterInterface $propertyFilterService,
                                PropertyRepository $elasticPropertyRepository,
                                LocationToFilterService $locationToFilterService,
                                CatalogSeoService $catalogSeoService)
    {
        $this->_propertyService = $propertyService;
        $this->_propertyFilterService = $propertyFilterService;
        $this->_locationService = $locationService;
        $this->_catalogSeoService = $catalogSeoService;
        $this->_elasticPropertyRepository = $elasticPropertyRepository;
        $this->_locationToFilterService = $locationToFilterService;
    }

    /**
     * @param $queryParams
     * @param $category
     * @param $view
     * @param int $mode
     * @return array
     */
    public function getData($queryParams, $category, $view, $mode = self::MODE_INDEX)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $params = $this->extractParams($queryParams, $category);
        $polygon = ArrayHelper::remove($params, 'polygon');
        if ($polygon !== null) {
            $params['polygon'] = SpatialHelper::simplifyPolygon($polygon, Yii::$app->params['polygonCoef']);
            $polygon = GooglePolygonMapper::getMappedData($polygon);
        }

        $properties = $this->getItems($params);

        if ($mode === self::MODE_INDEX_AJAX) {
            $markerService = new PropertyMarkerService($this->_elasticPropertyRepository);
            $markers = $markerService->getMarkers($params);
        }
        $propertyFilter = $this->getFilter($params, $locationData, $properties, $view);
        $sortData = $propertyFilter->getSortData();

        $seo = $this->getSeo($params, $locationData, $category, $propertyFilter->filters, $properties);

        return [
            'lat' => $locationData['lat'],
            'lng' => $locationData['lng'],
            'zoom' => array_key_exists('zoom', $params) ? (int)$params['zoom'] : 11,
            'box' => $params['box'] ?? null,
            'polygon' => $polygon,
            'properties' => $properties,
            'seo' => $seo,
            'propertyFilter' => $propertyFilter,
            'sortData' => $sortData,
            'markers' => $markers ?? []
        ];
    }

    /**
     * @param array $params
     * @param array $categoryItem
     * @return array
     */
    protected function extractParams(array $params, array $categoryItem)
    {
        $params['status'] = Property::STATUS_ACTIVE;
        $params['ads_allowed'] = true;
        $params = $this->_locationToFilterService->getFilters($params);
        $params['per-page'] = $params['per-page'] ?? 30;
        $params['root'] = $categoryItem['root'];
        $params['lft'] = $categoryItem['lft'];
        $params['rgt'] = $categoryItem['rgt'];

        return $params;
    }

    /**
     * @param array $params
     * @param array $locationData
     * @param array $categoryItem
     * @param array $filters
     * @param array $properties
     * @return array
     */
    protected function getSeo(array $params, array $locationData, array $categoryItem, array $filters, array $properties)
    {
        $params['category'] = $categoryItem['title'];
        $params['count'] = $properties['pagination']->totalCount;
        $params['attributes'] = $filters;
        $params['location_slug'] = $locationData['slug'];
        $params['location_entity'] = $locationData['entity'];
        $params['minPrice'] = $filters['price']['left']['min'];
        $params['maxPrice'] = $filters['price']['right']['max'];

        $seo = $this->_catalogSeoService->getSeo(CatalogSeoService::TEMPLATE_CATEGORY, $params);
        $seo['image'] = end($properties['items'])['image'] ?? null;
        reset($properties['items']);

        return $seo;
    }

    /**
     * @param $params
     * @return array
     */
    protected function getItems(array $params)
    {
        $orderBy = ['id' => SORT_DESC];

        if (array_key_exists('sort', $params)) {
            $mappedSortData = CatalogSortMapper::getMappedData($params['sort']);

            switch ($mappedSortData['field']) {
                case 'default_price':
                    $orderBy = ['default_price' => $mappedSortData['order']];
                    break;
            }
        }

        if (!array_key_exists('db-sql', $params)) {
            $this->_propertyService->setRepository($this->_elasticPropertyRepository);
        }

        return $this->_propertyService->getMany($params, [
            'pagination' => true,
            'perPage' => $params['per-page'],
            'orderBy' => $orderBy
        ]);
    }

    /**
     * @param array $params
     * @param array $locationData
     * @param array $properties
     * @param string $view
     * @return PropertyFilter
     */
    protected function getFilter(array $params, array $locationData, array $properties, string $view)
    {
        $propertyFilter = new PropertyFilter($this->_propertyFilterService, [
            'catalogView' => $view,
            'queryParams' => $params,
            'locationData' => $locationData,
            'gridSize' => $view === 'map' ? 6 : 8,
            'resultCount' => $properties['pagination']->totalCount
        ]);
        $propertyFilter->build();

        return $propertyFilter;
    }
}