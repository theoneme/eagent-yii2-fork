<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\catalog;

use common\models\Building;
use common\services\entities\CityService;
use frontend\services\LocationService;
use Yii;
use yii\helpers\Url;

/**
 * Class BuildingCatalogUrlService
 * @package frontend\services\catalog
 */
class BuildingCatalogUrlService
{
    /**
     * @var CityService
     */
    private $_cityService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * BuildingCatalogUrlService constructor.
     * @param CityService $cityService
     * @param LocationService $locationService
     */
    public function __construct(CityService $cityService, LocationService $locationService)
    {
        $this->_cityService = $cityService;
        $this->_locationService = $locationService;
    }

    /**
     * @param $queryParams
     * @param array $baseRoute
     * @return string
     */
    public function getUrl($queryParams, $baseRoute = ['/building/catalog/index'])
    {
        if (array_key_exists('type', $queryParams) && !array_key_exists('operation', $queryParams)) {
            $operation = (int)$queryParams['type'] === Building::TYPE_DEFAULT ? Building::TYPE_DEFAULT_TEXT : Building::TYPE_NEW_CONSTRUCTION_TEXT;
            unset($queryParams['type']);
            $queryParams['operation'] = $operation;
        }

        if (array_key_exists('city', $queryParams)) {
            $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
            if ($queryParams['city'] !== $locationData['slug']) {
                $queryParams['app_city'] = $queryParams['city'];
                unset($queryParams['city']);
            }
        }

        return Url::to(array_merge($baseRoute, $queryParams));
    }
}