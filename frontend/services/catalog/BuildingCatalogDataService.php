<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\catalog;

use common\helpers\SpatialHelper;
use common\interfaces\filters\BuildingFilterInterface;
use common\mappers\CatalogSortMapper;
use common\models\Building;
use common\repositories\elastic\BuildingRepository;
use common\services\BuildingMarkerService;
use common\services\entities\BuildingService;
use common\services\seo\CatalogSeoService;
use frontend\mappers\map\GooglePolygonMapper;
use frontend\services\LocationService;
use frontend\widgets\BuildingFilter;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingCatalogDataService
 * @package frontend\services\catalog
 */
class BuildingCatalogDataService
{
    public const MODE_INDEX = 0;
    public const MODE_INDEX_AJAX = 10;

    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var LocationService
     */
    private $_locationService;
    /**
     * @var BuildingFilterInterface
     */
    private $_buildingFilterService;
    /**
     * @var CatalogSeoService
     */
    private $_catalogSeoService;
    /**
     * @var BuildingRepository
     */
    private $_elasticBuildingRepository;
    /**
     * @var LocationToFilterService
     */
    private $_locationToFilterService;

    /**
     * BuildingCatalogDataService constructor.
     * @param BuildingService $buildingService
     * @param LocationService $locationService
     * @param BuildingFilterInterface $buildingFilterService
     * @param BuildingRepository $elasticBuildingRepository
     * @param LocationToFilterService $locationToFilterService
     * @param CatalogSeoService $catalogSeoService
     */
    public function __construct(BuildingService $buildingService,
                                LocationService $locationService,
                                BuildingFilterInterface $buildingFilterService,
                                BuildingRepository $elasticBuildingRepository,
                                LocationToFilterService $locationToFilterService,
                                CatalogSeoService $catalogSeoService)
    {
        $this->_buildingService = $buildingService;
        $this->_buildingFilterService = $buildingFilterService;
        $this->_locationService = $locationService;
        $this->_catalogSeoService = $catalogSeoService;
        $this->_elasticBuildingRepository = $elasticBuildingRepository;
        $this->_locationToFilterService = $locationToFilterService;
    }

    /**
     * @param $queryParams
     * @param $view
     * @param int $mode
     * @return array
     */
    public function getData($queryParams, $view, $mode = self::MODE_INDEX)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);
        $params = $this->extractParams($queryParams);
        $polygon = ArrayHelper::remove($params, 'polygon');
        if ($polygon !== null) {
            $params['polygon'] = SpatialHelper::simplifyPolygon($polygon, Yii::$app->params['polygonCoef']);
            $polygon = GooglePolygonMapper::getMappedData($polygon);
        }

        if ($mode === self::MODE_INDEX_AJAX) {
            $markerService = new BuildingMarkerService($this->_elasticBuildingRepository);
            $markers = $markerService->getMarkers($params);
        }
        $buildings = $this->getItems($params);
        $buildingFilter = $this->getFilter($params, $locationData, $buildings, $view);

        $seo = $this->getSeo($params, $locationData, $buildingFilter->filters, $buildings);

        return [
            'lat' => $locationData['lat'],
            'lng' => $locationData['lng'],
            'zoom' => array_key_exists('zoom', $params) ? (int)$params['zoom'] : 11,
            'box' => $params['box'] ?? null,
            'polygon' => $polygon,
            'buildings' => $buildings,
            'seo' => $seo,
            'buildingFilter' => $buildingFilter,
            'markers' => $markers ?? []
        ];
    }

    /**
     * @param array $params
     * @return array
     */
    protected function extractParams(array $params)
    {
        $params['status'] = Building::STATUS_ACTIVE;
        $params['ads_allowed'] = true;
        $params = $this->_locationToFilterService->getFilters($params);
        $params['per-page'] = $params['per-page'] ?? 30;

        return $params;
    }

    /**
     * @param array $params
     * @param array $locationData
     * @param array $filters
     * @param array $buildings
     * @return array
     */
    protected function getSeo(array $params, array $locationData, array $filters, array $buildings)
    {
        $params['count'] = $buildings['pagination']->totalCount;
        $params['attributes'] = $filters;
        $params['location_slug'] = $locationData['slug'];
        $params['location_entity'] = $locationData['entity'];
        $params['buildingType'] = Building::TYPE_DEFAULT;
        if (array_key_exists('operation', $params)) {
            $params['buildingType'] = $params['operation'] === Building::TYPE_NEW_CONSTRUCTION_TEXT
                ? Building::TYPE_NEW_CONSTRUCTION
                : Building::TYPE_DEFAULT;
        }

        $seo = $this->_catalogSeoService->getSeo(CatalogSeoService::TEMPLATE_BUILDING, $params);
        $seo['image'] = end($buildings['items'])['image'] ?? null;
        reset($buildings['items']);

        return $seo;
    }

    /**
     * @param $params
     * @return array
     */
    protected function getItems(array $params)
    {
        $orderBy = ['id' => SORT_DESC];

        if (array_key_exists('sort', $params)) {
            $mappedSortData = CatalogSortMapper::getMappedData($params['sort']);

            switch ($mappedSortData['field']) {
                case 'default_price':
                    $orderBy = ['default_price' => $mappedSortData['order']];
                    break;
            }
        }

        if (!array_key_exists('db-sql', $params)) {
            $this->_buildingService->setRepository($this->_elasticBuildingRepository);
        }

        return $this->_buildingService->getMany($params, [
            'pagination' => true,
            'perPage' => $params['per-page'],
            'orderBy' => $orderBy
        ]);
    }

    /**
     * @param array $params
     * @param array $locationData
     * @param array $properties
     * @param string $view
     * @return BuildingFilter
     */
    protected function getFilter(array $params, array $locationData, array $properties, string $view)
    {
        $buildingFilter = new BuildingFilter($this->_buildingFilterService, [
            'catalogView' => $view,
            'queryParams' => $params,
            'locationData' => $locationData,
            'gridSize' => $view === 'map' ? 6 : 8,
            'resultCount' => $properties['pagination']->totalCount
        ]);
        $buildingFilter->build();

        return $buildingFilter;
    }
}