<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\catalog;

use common\mappers\LocationBoxFilterMapper;
use common\mappers\LocationFilterMapper;
use common\services\entities\DistrictService;
use common\services\entities\MicroDistrictService;
use frontend\services\LocationService;
use Yii;

/**
 * Class LocationToFilterService
 * @package frontend\services\catalog
 */
class LocationToFilterService
{
    /**
     * @var DistrictService
     */
    private $_districtService;
    /**
     * @var MicroDistrictService
     */
    private $_microDistrictService;
    /**
     * @var LocationService
     */
    private $_locationService;

    /**
     * LocationToFilterService constructor.
     * @param DistrictService $districtService
     * @param MicroDistrictService $microDistrictService
     */
    public function __construct(DistrictService $districtService, MicroDistrictService $microDistrictService, LocationService $locationService)
    {
        $this->_districtService = $districtService;
        $this->_microDistrictService = $microDistrictService;
        $this->_locationService = $locationService;
    }

    /**
     * @param $params
     * @return array
     */
    public function getFilters($params)
    {
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        if (array_key_exists('box', $params)) {
            unset($params['district'], $params['microdistrict']);
            return array_merge($params, LocationBoxFilterMapper::getMappedData($params['box']));
        }

        if (array_key_exists('microdistrict', $params)) {
            $microDistrict = $this->_microDistrictService->getOne(['slug' => $params['microdistrict'], 'city_id' => $locationData['city_id']]);
            if ($microDistrict !== null) {
                return array_merge($params, [
                    'region_id' => $locationData['region_id'],
                    'polygon' => $microDistrict['polygonArray']['coordinates'][0],
                    'micro_district_title' => $microDistrict['title']
                ]);
            }
        }

        if (array_key_exists('district', $params)) {
            $district = $this->_districtService->getOne(['slug' => $params['district'], 'city_id' => $locationData['city_id']]);
            if ($district !== null) {
                return array_merge($params, [
                    'region_id' => $locationData['region_id'],
                    'polygon' => $district['polygonArray']['coordinates'][0],
                    'district_title' => $district['title']
                ]);
            }
        }

        $params = array_merge($params, LocationFilterMapper::getMappedData($locationData));

        return $params;
    }
}