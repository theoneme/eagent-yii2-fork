<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services;

use common\interfaces\filters\PropertyFilterInterface;
use common\interfaces\RepositoryInterface;
use common\models\Attribute;
use common\models\Property;
use common\repositories\sql\AttributeFilterRepository;
use common\services\CategoryTreeService;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use frontend\mappers\PropertyFilterMapper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BasePropertyFilterService
 * @package frontend\services
 */
abstract class BasePropertyFilterService implements PropertyFilterInterface
{
    /**
     * @var AttributeService
     */
    protected $_attributeService;
    /**
     * @var AttributeValueService
     */
    protected $_attributeValueService;
    /**
     * @var AttributeFilterRepository
     */
    protected $_attributeFilterRepository;
    /**
     * @var CategoryTreeService
     */
    protected $_categoryTreeService;
    /**
     * @var array
     */
    protected $_propertyOperationToType = [
        Property::OPERATION_SALE => Property::TYPE_SALE,
        Property::OPERATION_RENT => Property::TYPE_RENT
    ];

    /**
     * BasePropertyFilterService constructor.
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeFilterRepository $attributeFilterRepository
     * @param CategoryTreeService $categoryTreeService
     */
    public function __construct(AttributeService $attributeService,
                                AttributeValueService $attributeValueService,
                                AttributeFilterRepository $attributeFilterRepository,
                                CategoryTreeService $categoryTreeService
    )
    {
        $this->_attributeService = $attributeService;
        $this->_attributeValueService = $attributeValueService;
        $this->_attributeFilterRepository = $attributeFilterRepository;
        $this->_categoryTreeService = $categoryTreeService;
    }

    /**
     * @param $params
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     */
    public function buildFilterArray($params)
    {
        $availableAttributes = Yii::$app->params['propertyCatalogFilters'];
        $attributes = $this->_attributeService->getMany(['alias' => $availableAttributes], ['indexBy' => 'id'])['items'];
        $attributeFilters = $this->_attributeFilterRepository
            ->indexBy('attribute_id')
            ->findManyByCriteria(['attribute_id' => array_keys($attributes)], true);
        $categories = $this->_categoryTreeService->getTree();
        $usedAttributes = $this->getAttributeValues($params, $attributes, $attributeFilters);

        $prices = $this->getPriceRange($params);
        if (empty($usedAttributes) && empty($prices)) {
            return [];
        }

        $formatted = $this->formatAttributes($usedAttributes);
        $valuesToTranslate = [];
        if (!empty($attributes)) {
            $valuesToTranslate = array_filter(array_map(function ($value) use ($attributes) {
                if ($attributes[$value['attribute_id']]['type'] === Attribute::TYPE_STRING) {
                    return $value['value_alias'];
                }
                return null;
            }, $usedAttributes));
        }

        $attributeValues = [];
        if (!empty($valuesToTranslate)) {
            $attributeValues = $this->_attributeValueService->getMany(['alias' => $valuesToTranslate], ['indexBy' => 'alias'])['items'];
        }

        $data = PropertyFilterMapper::getMappedData([
            'attributes' => $attributes,
            'attributeValues' => $attributeValues,
            'attributeFilters' => $attributeFilters,
            'formatted' => $formatted,
            'params' => $params,
            'categories' => $categories,
            'prices' => $prices
        ]);

        return $data;
    }

    /**
     * @param $params
     * @return array|bool
     */
    abstract protected function getPriceRange($params);

    /**
     * @param $params
     * @param $availableAttributes
     * @param $attributeFilters
     * @return mixed
     */
    abstract protected function getAttributeValues($params, $availableAttributes, $attributeFilters);

    /**
     * @param $params
     * @return mixed
     */
    abstract protected function applyDefaultParams($params);

    /**
     * @param $query
     * @param $availableAttributes
     * @param $params
     * @return mixed
     */
    protected function applyQueryParams($query, $availableAttributes, $params)
    {
        $paramsToApply = array_intersect_key($params, $availableAttributes);
        foreach ($paramsToApply as $key => $param) {
            if (array_key_exists('min', (array)$param) || array_key_exists('max', (array)$param)) {
                $query = $this->applyRangeAttributeQuery($query, $key, $param);
            } else {
                $query = $this->applyAttributeQuery($query, $key, $param);
            }
        }

        return $query;
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return RepositoryInterface
     */
    protected function applyAttributeQuery($query, $key, $param)
    {
        $relationAlias = 'property_attribute';

        $condition = [
            "{$relationAlias}.entity_alias" => $key,
            "{$relationAlias}.value_alias" => $param
        ];

        return $query->relationExists($relationAlias, 'property_id', $condition);
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return RepositoryInterface
     */
    protected function applyRangeAttributeQuery($query, $key, $param)
    {
        $relationAlias = 'property_attribute';

        $rangeCondition = ['and', [
            "{$relationAlias}.entity_alias" => $key
        ]];

        if (array_key_exists('min', $param)) {
            $rangeCondition[] = ['>=', "{$relationAlias}.value_number", (float)$param['min']];
        }
        if (array_key_exists('max', $param)) {
            $rangeCondition[] = ['<=', "{$relationAlias}.value_number", (float)$param['max']];
        }

        return $query->relationExists($relationAlias, 'property_id', $rangeCondition);
    }

    /**
     * @param $usedAttributes
     * @return array
     */
    protected function formatAttributes($usedAttributes)
    {
        $formatted = ArrayHelper::map($usedAttributes, 'value_alias', function ($value) {
            return [
                'count' => $value['count']
            ];
        }, 'attribute_id');

        return $formatted;
    }
}