<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\elasticsearch;

use common\components\CurrencyHelper;
use common\models\AttributeFilter;
use common\repositories\elastic\PropertyRepository;
use common\repositories\sql\AttributeFilterRepository;
use common\services\CategoryTreeService;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use frontend\helpers\FilterConditionHelper;
use frontend\mappers\filter\FilterElasticCountMapper;
use frontend\mappers\filter\FilterElasticMinMaxMapper;
use frontend\services\BasePropertyFilterService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class PropertyFilterService
 * @package frontend\services\elasticsearch
 */
class PropertyFilterService extends BasePropertyFilterService
{
    /**
     * @var PropertyRepository
     */
    protected $_elasticPropertyRepository;

    /**
     * PropertyFilterService constructor.
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeFilterRepository $attributeFilterRepository
     * @param CategoryTreeService $categoryTreeService
     * @param PropertyRepository $elasticPropertyRepository
     */
    public function __construct(AttributeService $attributeService,
                                AttributeValueService $attributeValueService,
                                AttributeFilterRepository $attributeFilterRepository,
                                CategoryTreeService $categoryTreeService,
                                PropertyRepository $elasticPropertyRepository
    )
    {
        parent::__construct($attributeService, $attributeValueService, $attributeFilterRepository, $categoryTreeService);
        $this->_elasticPropertyRepository = $elasticPropertyRepository;
    }

    /**
     * @param $params
     * @return array|bool
     */
    protected function getPriceRange($params)
    {
        $priceQuery = $this->_elasticPropertyRepository
            ->aggregate($this->buildMinMaxPriceAggregation());
        $criteria = $this->applyDefaultParams($params);
        $priceResult = $priceQuery->limit(0)->findManyByCriteria($criteria, true);

        if (is_array($priceResult) && array_key_exists('aggregations', $priceResult)) {
            $prices['min'] = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency'], $priceResult['aggregations']['minPrice']['value']);
            $prices['max'] = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency'], $priceResult['aggregations']['maxPrice']['value']);
        } else {
            $prices['min'] = 0;
            $prices['max'] = 0;
        }

        return $prices;
    }

    /**
     * @param $params
     * @param $availableAttributes
     * @return array
     */
    protected function getAttributeValues($params, $availableAttributes, $attributeFilters)
    {
        $notRangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] !== AttributeFilter::TYPE_RANGE;
        });
        $rangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] === AttributeFilter::TYPE_RANGE;
        });

        $usedAttributesQuery = $this->_elasticPropertyRepository;
        $aggregations = $this->buildMinMaxAttributeAggregation($rangeFilters);
        $criteria = $this->applyDefaultParams($params);
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);
        $numericValues = $usedAttributesQuery->limit(0)->aggregate($aggregations)->findManyByCriteria($criteria, true);
        $values = FilterElasticMinMaxMapper::getMappedData($numericValues);

        $usedAttributesQuery = $this->_elasticPropertyRepository;
        $aggregations = $this->buildCountAttributeAggregation($notRangeFilters);
        $criteria = $this->applyDefaultParams($params);
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);
        $stringValues = $usedAttributesQuery->limit(0)->aggregate($aggregations)->findManyByCriteria($criteria, true);
        $values = array_merge($values, FilterElasticCountMapper::getMappedData($stringValues));

        return $values;
    }

    /**
     * @param $params
     * @return array
     */
    protected function applyDefaultParams($params)
    {
        $filterConditionHelper = new FilterConditionHelper();
        $criteria = $filterConditionHelper->process($params, ['box', 'category', 'status', 'region_id', 'country_id', 'radius', 'polygon', 'price']);
        $propertyOperation = ArrayHelper::remove($params, 'operation');
        if ($propertyOperation !== null && in_array($propertyOperation, $this->_propertyOperationToType)) {
            $criteria = ['and', $criteria, ['type' => $this->_propertyOperationToType[$propertyOperation]]];
        }

        return $criteria;
    }

    /**
     * @param string $field
     * @return array
     */
    protected function buildMinMaxPriceAggregation($field = 'default_price')
    {
        return [
            'maxPrice' => [
                'max' => [
                    'field' => $field
                ],
            ],
            'minPrice' => [
                'min' => [
                    'field' => $field
                ]
            ]
        ];
    }

    /**
     * @param $attributes
     * @return array
     */
    protected function buildMinMaxAttributeAggregation($attributes)
    {
        $aggregations = [];

        foreach ($attributes as $key => $attribute) {
            $aggregations[$key] = [
                'nested' => [
                    'path' => 'property_attribute',
                ],
                'aggs' => [
                    'property_attribute' => [
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'term' => [
                                            'property_attribute.attribute_id' => $key
                                        ]
                                    ],
                                ]
                            ]
                        ],
                        'aggs' => [
                            'minValue' => [
                                'max' => [
                                    'field' => 'property_attribute.value_number'
                                ],
                            ],
                            'maxValue' => [
                                'min' => [
                                    'field' => 'property_attribute.value_number'
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        return $aggregations;
    }

    /**
     * @param $attributes
     * @param int $limit
     * @return array
     */
    protected function buildCountAttributeAggregation($attributes, $limit = 25)
    {
        $aggregations = [];

        foreach ($attributes as $key => $attribute) {
            $aggregations[$key] = [
                'nested' => [
                    'path' => 'property_attribute',
                ],
                'aggs' => [
                    'property_attribute' => [
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'term' => [
                                            'property_attribute.attribute_id' => $key
                                        ]
                                    ],
                                ]
                            ]
                        ],
                        'aggs' => [
                            'values' => [
                                'terms' => [
                                    'field' => 'property_attribute.value_alias',
                                    'size' => $limit
                                ],
                            ]
                        ]
                    ]
                ]
            ];
        }

        return $aggregations;
    }
}