<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\elasticsearch;

use common\interfaces\repositories\BuildingRepositoryInterface;
use common\models\AttributeFilter;
use common\repositories\elastic\BuildingRepository;
use common\repositories\sql\AttributeFilterRepository;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use frontend\helpers\FilterConditionHelper;
use frontend\mappers\filter\FilterElasticCountMapper;
use frontend\mappers\filter\FilterElasticMinMaxMapper;
use frontend\services\BaseBuildingFilterService;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingFilterService
 * @package frontend\services\elasticsearch
 */
class BuildingFilterService extends BaseBuildingFilterService
{
    /**
     * @var BuildingRepositoryInterface
     */
    protected $_elasticBuildingRepository;

    /**
     * BuildingFilterService constructor.
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeFilterRepository $attributeFilterRepository
     * @param BuildingRepository $elasticBuildingRepository
     */
    public function __construct(AttributeService $attributeService,
                                AttributeValueService $attributeValueService,
                                AttributeFilterRepository $attributeFilterRepository,
                                BuildingRepository $elasticBuildingRepository
    )
    {
        parent::__construct($attributeService, $attributeValueService, $attributeFilterRepository);
        $this->_elasticBuildingRepository = $elasticBuildingRepository;
    }

    /**
     * @param $params
     * @param $availableAttributes
     * @param $attributeFilters
     * @return array|mixed
     */
    protected function getAttributeValues($params, $availableAttributes, $attributeFilters)
    {
        $notRangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] !== AttributeFilter::TYPE_RANGE;
        });
        $rangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] === AttributeFilter::TYPE_RANGE;
        });

        $usedAttributesQuery = $this->_elasticBuildingRepository;
        $aggregations = $this->buildMinMaxAttributeAggregation($rangeFilters);
        $criteria = $this->applyDefaultParams($params);
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);
        $numericValues = $usedAttributesQuery->limit(0)->aggregate($aggregations)->findManyByCriteria($criteria, true);
        $values = FilterElasticMinMaxMapper::getMappedData($numericValues, 'building_attribute');

        $usedAttributesQuery = $this->_elasticBuildingRepository;
        $aggregations = $this->buildCountAttributeAggregation($notRangeFilters);
        $criteria = $this->applyDefaultParams($params);
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);
        $stringValues = $usedAttributesQuery->limit(0)->aggregate($aggregations)->findManyByCriteria($criteria, true);
        $values = array_merge($values, FilterElasticCountMapper::getMappedData($stringValues, 'building_attribute'));

        return $values;
    }

    /**
     * @param $params
     * @return array
     */
    protected function applyDefaultParams($params)
    {
        $filterConditionHelper = new FilterConditionHelper();
        $criteria = $filterConditionHelper->process($params, ['box', 'status', 'region_id', 'country_id', 'radius', 'polygon']);
        $buildingOperation = ArrayHelper::remove($params, 'operation');
        if ($buildingOperation !== null && in_array($buildingOperation, $this->_buildingTypeToType)) {
            $criteria = ['and', $criteria, ['type' => $this->_buildingTypeToType[$buildingOperation]]];
        }

        return $criteria;
    }

    /**
     * @param $attributes
     * @return array
     */
    protected function buildMinMaxAttributeAggregation($attributes)
    {
        $aggregations = [];

        foreach ($attributes as $key => $attribute) {
            $aggregations[$key] = [
                'nested' => [
                    'path' => 'building_attribute',
                ],
                'aggs' => [
                    'building_attribute' => [
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'term' => [
                                            'building_attribute.attribute_id' => $key
                                        ]
                                    ],
                                ]
                            ]
                        ],
                        'aggs' => [
                            'minValue' => [
                                'max' => [
                                    'field' => 'building_attribute.value_number'
                                ],
                            ],
                            'maxValue' => [
                                'min' => [
                                    'field' => 'building_attribute.value_number'
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        return $aggregations;
    }

    /**
     * @param $attributes
     * @param int $limit
     * @return array
     */
    protected function buildCountAttributeAggregation($attributes, $limit = 25)
    {
        $aggregations = [];

        foreach ($attributes as $key => $attribute) {
            $aggregations[$key] = [
                'nested' => [
                    'path' => 'building_attribute',
                ],
                'aggs' => [
                    'building_attribute' => [
                        'filter' => [
                            'bool' => [
                                'must' => [
                                    [
                                        'term' => [
                                            'building_attribute.attribute_id' => $key
                                        ]
                                    ],
                                ]
                            ]
                        ],
                        'aggs' => [
                            'values' => [
                                'terms' => [
                                    'field' => 'building_attribute.value_alias',
                                    'size' => $limit
                                ],
                            ]
                        ]
                    ]
                ]
            ];
        }

        return $aggregations;
    }
}