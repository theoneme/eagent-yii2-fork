<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services;

use common\services\entities\CompanyMemberService;
use common\services\entities\CompanyService;
use frontend\mappers\CompanyMemberMapper;

/**
 * Class CompaniesByMemberService
 * @package frontend\services
 */
class CompaniesByMemberService
{
    /**
     * @var CompanyService
     */
    private $_companyService;
    /**
     * @var CompanyMemberService
     */
    private $_companyMemberService;

    /**
     * CompaniesByMemberService constructor.
     * @param CompanyService $companyService
     * @param CompanyMemberService $companyMemberService
     */
    public function __construct(CompanyService $companyService, CompanyMemberService $companyMemberService)
    {
        $this->_companyService = $companyService;
        $this->_companyMemberService = $companyMemberService;
    }

    /**
     * @param $condition
     * @param $params
     * @return array
     */
    public function getCompanies($condition, $params)
    {
        $data = [
            'items' => [],
            'pagination' => []
        ];

        $members = $this->_companyMemberService->getMany($condition, $params);
        if (empty($members['items'])) {
            return $data;
        }
        $companyIDs = array_unique(array_column($members['items'], 'company_id'));
        $companies = $this->_companyService->getMany(['id' => $companyIDs], [
            'indexBy' => 'id'
        ]);

        $data['items'] = CompanyMemberMapper::getMappedData(['members' => $members['items'], 'companies' => $companies['items']]);
        $data['pagination'] = $members['pagination'] ?? [];

        return $data;
    }
}