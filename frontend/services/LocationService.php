<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services;

use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\RegionService;
use GeoIp2\Database\Reader;
use Yii;

/**
 * Class LocationService
 * @package frontend\services
 */
class LocationService
{
    /**
     * @var CountryService
     */
    private $_countryService;
    /**
     * @var RegionService
     */
    private $_regionService;
    /**
     * @var CityService
     */
    private $_cityService;

    /**
     * LocationService constructor.
     * @param CountryService $countryService
     * @param RegionService $regionService
     * @param CityService $cityService
     */
    public function __construct(CountryService $countryService, RegionService $regionService, CityService $cityService)
    {
        $this->_countryService = $countryService;
        $this->_regionService = $regionService;
        $this->_cityService = $cityService;
    }

    /**
     * @param $slug
     * @param $entity
     * @return array|null
     */
    public function getLocationObject($entity, $slug)
    {
        switch ($entity) {
            case 'country':
                $object = $this->_countryService->getOne(['country.slug' => $slug]);

                $city = $this->_cityService->getOne(['city.country_id' => $object]);
                $object['lat'] = $city['lat'];
                $object['lng'] = $city['lng'];

                break;
            case 'region':
                $object = $this->_regionService->getOne(['region.slug' => $slug]);

                $city = $this->_cityService->getOne(['city.region_id' => $object]);
                $object['lat'] = $city['lat'];
                $object['lng'] = $city['lng'];

                break;
            case 'city':
            default:
                $object = $this->_cityService->getOne(['city.slug' => $slug]);
        }

        return $object;
    }

    /**
     * @param array $locationData
     * @param bool $defaultIfMissing
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getLocationData($locationData, $defaultIfMissing = true)
    {
        if (!empty($locationData) && array_key_exists('slug', $locationData)) {
            $locationData['region_id'] = $locationData['region_id'] ?? null;
            $locationData['city_id'] = $locationData['city_id'] ?? null;

            if (isset($locationData['object'])) {
                $locationItem = $locationData['object'];
            } else {
                $locationItem = $this->getLocationObject($locationData['entity'], $locationData['slug']);

                $locationData['object'] = $locationItem;
                Yii::$app->params['runtime']['location']['object'] = $locationItem;
            }

            if (!array_key_exists('address', $locationData) || empty($locationData['address'])) {
                $locationData['address'] = $locationItem['title']; //TODO придумать вариант получше
            }

            return $locationData;
        }

        if ($defaultIfMissing === true) {
            $locationData = $this->getDefaultLocationData();
        }

        return $locationData;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getDefaultLocationData()
    {
        $locationData = [];

        $ip = Yii::$app->request->getUserIP();
        if ($ip !== '127.0.0.1') {
            $geoIpReader = new Reader(\Yii::getAlias('@common/lib/maxmind') . '/GeoLite2-City.mmdb');
            try {
                $geoCity = $geoIpReader->city($ip);
            } catch (\Exception $e) {
                $geoCity = null;
            }
            if (!empty($geoCity) && !empty($geoCity->location) && !empty($geoCity->location->latitude) && !empty($geoCity->location->longitude)) {
                $cities = $this->_cityService->getMany([
                    'lat' => $geoCity->location->latitude,
                    'lng' => $geoCity->location->longitude,
                ], ['limit' => 1]);
                if (!empty($cities['items'])) {
                    $city = $cities['items'][0];
                }
            }
        }

        $city = $city ?? $this->_cityService->getOne(['city.slug' => 'london']);
        if ($city !== null) {
            $locationData['lat'] = $city['lat'];
            $locationData['lng'] = $city['lng'];
            $locationData['address'] = $city['title'];
            $locationData['entity'] = 'city';
//            $locationData['entity_id'] = $city['id'];
            $locationData['country_id'] = $city['country_id'];
            $locationData['region_id'] = $city['region_id'];
            $locationData['city_id'] = $city['id'];
            $locationData['slug'] = $city['slug'];
        }

        return $locationData;
    }
}