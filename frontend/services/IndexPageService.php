<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services;

use common\interfaces\repositories\CategoryRepositoryInterface;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\mappers\LocationFilterMapper;
use common\models\Building;
use common\models\Category;
use common\models\Property;
use common\models\Translation;
use common\repositories\elastic\PropertyRepository;
use common\services\entities\AttributeValueService;
use common\services\entities\BuildingService;
use common\services\entities\PropertyService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class IndexPageService
 * @package frontend\services
 */
class IndexPageService
{
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;
    /**
     * @var LocationService
     */
    private $_locationService;
    /**
     * @var PropertyRepositoryInterface
     */
    private $_propertyRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;

    /**
     * IndexPageService constructor.
     * @param LocationService $locationService
     * @param PropertyService $propertyService
     * @param BuildingService $buildingService
     * @param AttributeValueService $attributeValueService
     * @param PropertyRepositoryInterface $propertyRepository
     * @param CategoryRepositoryInterface $categoryRepository
     * @param array $config
     */
    public function __construct(LocationService $locationService,
                                PropertyService $propertyService,
                                BuildingService $buildingService,
                                AttributeValueService $attributeValueService,
                                PropertyRepository $propertyRepository,
                                CategoryRepositoryInterface $categoryRepository,
                                array $config = [])
    {
        $this->_propertyService = $propertyService;
        $this->_buildingService = $buildingService;
        $this->_attributeValueService = $attributeValueService;
        $this->_locationService = $locationService;
        $this->_propertyRepository = $propertyRepository;
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getData()
    {
        /* @var Category $category */
        $category = $this->_categoryRepository->select(['id', 'root', 'lft', 'rgt'], true)->findOneByCriteria(['id' => 2], true);
        $locationData = $this->_locationService->getLocationData(Yii::$app->params['runtime']['location']);

        $params = ['status' => Property::STATUS_ACTIVE, 'operation' => Property::OPERATION_SALE];
        $params = array_merge($params, LocationFilterMapper::getMappedData($locationData));
        $params['ads_allowed'] = true;
        $lat = ArrayHelper::remove($params, 'lat');
        $lng = ArrayHelper::remove($params, 'lng');
        $radius = ArrayHelper::remove($params, 'radius');
        unset($params['operation']);
        $criteria = ['and', $params, ['type' => Property::TYPE_SALE]];
        if ($lat && $lng) {
            $criteria = ['and', $criteria, ['radius', $lat, $lng, $radius]];
        }

        if ($category !== null) {
            $criteria = ['and', $criteria, ['category.root' => $category['root']], ['>=', 'category.lft', $category['lft']], ['<=', 'category.rgt', $category['rgt']]];
        }

        $criteria = ['and', $criteria, ['translation.key' => Translation::KEY_DESCRIPTION, 'translation.locale' => Yii::$app->language]];
        $propertyIDs = $this->_propertyRepository->select(['id'], true)->joinWith(['category'], false)
            ->limit(8)
            ->orderBy(['id' => SORT_DESC])
            ->findColumnByCriteria($criteria, 'id');

        if (!empty($propertyIDs)) {
            $properties = $this->_propertyService->getMany(['id' => $propertyIDs]);
        } else {
            $properties['items'] = [];
        }

        $localBuildings = $this->_buildingService->getMany([
            'type' => Building::TYPE_NEW_CONSTRUCTION,
            'lat' => $locationData['lat'],
            'lng' => $locationData['lng'],
            'region_id' => $locationData['region_id'],
            'ads_allowed' => true
        ], ['limit' => 4, 'orderBy' => ['id' => SORT_DESC]]);
        $excludes = array_column($localBuildings['items'], 'id');

        $globalBuildings = $this->_buildingService->getMany([
            'type' => Building::TYPE_NEW_CONSTRUCTION,
            'ads_allowed' => true,
        ], ['limit' => 4, 'orderBy' => ['id' => SORT_DESC], 'exclude' => $excludes]);

        $merged = array_merge($localBuildings['items'], $globalBuildings['items']);

        // кусок говна чтобы притянуть транслейшены застройщиков
        $developers = array_filter(array_map(function ($building) {
            return isset($building['attributes']['new_construction_builder']) ? str_replace(',', '-', $building['attributes']['new_construction_builder']) : null;
        }, $merged));
        $developers = $this->_attributeValueService->getMany([
            'alias' => $developers,
            'attribute_id' => 41
        ], ['indexBy' => 'alias']);

        $localBuildings = $this->fillDevelopers($localBuildings, $developers);
        $globalBuildings = $this->fillDevelopers($globalBuildings, $developers);

        return [
            'properties' => $properties['items'],
            'localBuildings' => $localBuildings['items'],
            'globalBuildings' => $globalBuildings['items']
        ];
    }

    /**
     * @param $buildings
     * @param $developers
     * @return mixed
     */
    private function fillDevelopers($buildings, $developers)
    {
        foreach ($buildings['items'] as $key => &$building) {
            if (isset($building['attributes']['new_construction_builder'])) {
                $developer = str_replace(',', '-', $building['attributes']['new_construction_builder']);
                $building['attributes']['new_construction_builder'] = $developers['items'][$developer]['translations']['title'] ?? $building['attributes']['new_construction_builder'];
            }
        }

        return $buildings;
    }
}