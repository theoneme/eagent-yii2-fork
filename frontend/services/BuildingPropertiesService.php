<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services;

use common\models\Property;
use common\models\PropertyAttribute;
use common\repositories\sql\PropertyAttributeRepository;
use common\repositories\sql\PropertyRepository;
use frontend\mappers\BuildingPropertiesMapper;

/**
 * Class BuildingPropertiesService
 * @package frontend\services
 */
class BuildingPropertiesService
{
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var PropertyAttributeRepository
     */
    private $_propertyAttributeRepository;

    /**
     * BuildingPropertiesService constructor.
     * @param PropertyAttributeRepository $propertyAttributeRepository
     * @param PropertyRepository $propertyRepository
     */
    public function __construct(PropertyAttributeRepository $propertyAttributeRepository, PropertyRepository $propertyRepository)
    {
        $this->_propertyRepository = $propertyRepository;
        $this->_propertyAttributeRepository = $propertyAttributeRepository;
    }

    /**
     * @param $buildingId
     * @return mixed
     */
    public function getCounts($buildingId)
    {
        $countCriteria = [
            'building_id' => $buildingId,
            'status' => Property::STATUS_ACTIVE
        ];

        $noRoomsCounts = $this->_propertyRepository
            ->groupBy(['type', 'status'])
            ->orderBy(['type' => SORT_ASC])
            ->select(['type', 'status', 'count' => 'count(property.id)', 'minPrice' => 'min(property.default_price)', 'maxPrice' => 'max(property.default_price)'], true)
            ->findManyByCriteria([
                'and', $countCriteria, ['not exists',
                    PropertyAttribute::find()->where('property.id = property_attribute.property_id')->andWhere(['entity_alias' => 'rooms'])
                ]
            ], true);
        $counts = $this->_propertyAttributeRepository
            ->joinWith(['property' => function ($query) {
                return $query->select('id, type, category_id, status, building_id');
            }], false)
            ->groupBy(['type', 'status', 'value_alias'])
            ->orderBy(['value_alias' => SORT_ASC])
            ->select(['value_alias', 'type', 'status', 'count' => 'count(property_attribute.property_id)', 'minPrice' => 'min(property.default_price)', 'maxPrice' => 'max(property.default_price)'], true)
            ->findManyByCriteria(array_merge($countCriteria, [
//                'category_id' => 2,
                'entity_alias' => 'rooms',
            ]), true);

        if (!empty($noRoomsCounts)) {
            foreach ($noRoomsCounts as $count) {
                $count['value_alias'] = 1;
                $counts[] = $count;
            }
        }
        $mappedCounts = BuildingPropertiesMapper::getMappedData([
            'counts' => $counts
        ]);

        return $mappedCounts;
    }
}