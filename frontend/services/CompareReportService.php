<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services;

use common\components\CurrencyHelper;
use Yii;

/**
 * Class CompareReportService
 * @package frontend\services
 */
class CompareReportService
{
    /**
     * @param $properties
     * @param $attributes
     * @return null|string
     */
    public function getMessage($properties, $attributes)
    {
        $message = null;

        if (!empty($properties)) {
            $message = Yii::t('catalog', 'We have analyzed listings in internet that match your request. {count, plural, one{# property was analyzed} other{# properties were analyzed}}', [
                    'count' => count($properties['items'])
                ]) . PHP_EOL;

            $minPriceSqM = 91919191919119;
            $pricesSqM = [];

            foreach ($properties['items'] as $key => $property) {
                if (array_key_exists('property_area', $property['attributes'])) {
                    if(array_key_exists($key, $attributes['prices']) && !empty($attributes['prices'][$key])) {
                        $priceSqM = (int)($attributes['prices'][$key] / $property['attributes']['property_area']);
                    } else {
                        $priceSqM = (int)($property['raw_price'] / $property['attributes']['property_area']);
                    }

                    $pricesSqM[] = $priceSqM;
                    if ($priceSqM < $minPriceSqM) {
                        $minPriceSqM = $priceSqM;
                    }
                }
            }

            $avgPriceSqM = array_sum($pricesSqM) / count($pricesSqM);

            $message .= Yii::t('catalog', 'Minimum price per square meter {price}', [
                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $minPriceSqM)
                ]) . PHP_EOL;
            $message .= Yii::t('catalog', 'Average price per square meter {price}', [
                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $avgPriceSqM)
                ]) . PHP_EOL;
            $message .= Yii::t('catalog', 'Recommended price for fast sale {price}', [
                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $minPriceSqM * (integer)$attributes['property_area'])
                ]) . PHP_EOL;
            $message .= Yii::t('catalog', 'Recommended price {price}', [
                    'price' => CurrencyHelper::convertAndFormat('RUB', Yii::$app->params['app_currency'], $avgPriceSqM * (integer)$attributes['property_area'])
                ]) . PHP_EOL;
        }

        return $message;
    }
}