<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services;

use common\interfaces\filters\BuildingFilterInterface;
use common\interfaces\RepositoryInterface;
use common\models\Attribute;
use common\models\Building;
use common\repositories\sql\AttributeFilterRepository;
use common\services\CategoryTreeService;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use frontend\mappers\BuildingFilterMapper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BasePropertyFilterService
 * @package frontend\services
 */
abstract class BaseBuildingFilterService implements BuildingFilterInterface
{
    /**
     * @var AttributeService
     */
    protected $_attributeService;
    /**
     * @var AttributeValueService
     */
    protected $_attributeValueService;
    /**
     * @var AttributeFilterRepository
     */
    protected $_attributeFilterRepository;
    /**
     * @var array
     */
    protected $_buildingTypeToType = [
        'all' => null,
        Building::TYPE_DEFAULT_TEXT => Building::TYPE_DEFAULT,
        Building::TYPE_NEW_CONSTRUCTION_TEXT => Building::TYPE_NEW_CONSTRUCTION
    ];

    /**
     * BasePropertyFilterService constructor.
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeFilterRepository $attributeFilterRepository
     * @param CategoryTreeService $categoryTreeService
     */
    public function __construct(AttributeService $attributeService,
                                AttributeValueService $attributeValueService,
                                AttributeFilterRepository $attributeFilterRepository
    )
    {
        $this->_attributeService = $attributeService;
        $this->_attributeValueService = $attributeValueService;
        $this->_attributeFilterRepository = $attributeFilterRepository;
    }

    /**
     * @param $params
     * @return array
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     */
    public function buildFilterArray($params)
    {
        $availableAttributes = Yii::$app->params['buildingCatalogFilters'];
        $attributes = $this->_attributeService->getMany(['alias' => $availableAttributes], ['indexBy' => 'id'])['items'];
        $attributeFilters = $this->_attributeFilterRepository
            ->indexBy('attribute_id')
            ->findManyByCriteria(['attribute_id' => array_keys($attributes)], true);
        $usedAttributes = $this->getAttributeValues($params, $attributes, $attributeFilters);

        $formatted = $this->formatAttributes($usedAttributes);
        $valuesToTranslate = [];
        if (!empty($attributes)) {
            $valuesToTranslate = array_filter(array_map(function ($value) use ($attributes) {
                if ($attributes[$value['attribute_id']]['type'] === Attribute::TYPE_STRING) {
                    return $value['value_alias'];
                }
                return null;
            }, $usedAttributes));
        }

        $attributeValues = [];
        if (!empty($valuesToTranslate)) {
            $attributeValues = $this->_attributeValueService->getMany(['alias' => $valuesToTranslate], ['indexBy' => 'alias'])['items'];
        }

        $data = BuildingFilterMapper::getMappedData([
            'attributes' => $attributes,
            'attributeValues' => $attributeValues,
            'attributeFilters' => $attributeFilters,
            'formatted' => $formatted,
            'params' => $params,
        ]);

        return $data;
    }

    /**
     * @param $params
     * @param $availableAttributes
     * @param $attributeFilters
     * @return mixed
     */
    abstract protected function getAttributeValues($params, $availableAttributes, $attributeFilters);

    /**
     * @param $params
     * @return mixed
     */
    abstract protected function applyDefaultParams($params);

    /**
     * @param $query
     * @param $availableAttributes
     * @param $params
     * @return mixed
     */
    protected function applyQueryParams($query, $availableAttributes, $params)
    {
        $paramsToApply = array_intersect_key($params, $availableAttributes);
        foreach ($paramsToApply as $key => $param) {
            if (array_key_exists('min', (array)$param) || array_key_exists('max', (array)$param)) {
                $query = $this->applyRangeAttributeQuery($query, $key, $param);
            } else {
                $query = $this->applyAttributeQuery($query, $key, $param);
            }
        }

        return $query;
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyAttributeQuery($query, $key, $param)
    {
        $relationAlias = 'building_attribute';

        $condition = [
            "{$relationAlias}.entity_alias" => $key,
            "{$relationAlias}.value_alias" => $param
        ];

        return $query->relationExists($relationAlias, 'building_id', $condition);
    }

    /**
     * @param RepositoryInterface $query
     * @param $key
     * @param $param
     * @return mixed
     */
    protected function applyRangeAttributeQuery($query, $key, $param)
    {
        $relationAlias = 'building_attribute';

        $rangeCondition = ['and', [
            "{$relationAlias}.entity_alias" => $key
        ]];

        if (array_key_exists('min', $param)) {
            $rangeCondition[] = ['>=', "{$relationAlias}.value_number", (float)$param['min']];
        }
        if (array_key_exists('max', $param)) {
            $rangeCondition[] = ['<=', "{$relationAlias}.value_number", (float)$param['max']];
        }

        return $query->relationExists($relationAlias, 'building_id', $rangeCondition);
    }

    /**
     * @param $usedAttributes
     * @return array
     */
    protected function formatAttributes($usedAttributes)
    {
        $formatted = ArrayHelper::map($usedAttributes, 'value_alias', function ($value) {
            return [
                'count' => $value['count']
            ];
        }, 'attribute_id');

        return $formatted;
    }
}