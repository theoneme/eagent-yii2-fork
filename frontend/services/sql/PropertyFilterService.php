<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\sql;

use common\components\CurrencyHelper;
use common\models\AttributeFilter;
use common\repositories\sql\AttributeFilterRepository;
use common\repositories\sql\AttributeValueRepository;
use common\repositories\sql\PropertyRepository;
use common\services\CategoryTreeService;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use frontend\helpers\FilterConditionHelper;
use frontend\mappers\filter\FilterSqlMinMaxMapper;
use frontend\services\BasePropertyFilterService;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class PropertyFilterService
 * @package frontend\services\sql
 */
class PropertyFilterService extends BasePropertyFilterService
{
    /**
     * @var AttributeValueRepository
     */
    protected $_attributeValueRepository;
    /**
     * @var PropertyRepository
     */
    protected $_propertyRepository;

    /**
     * PropertyFilterService constructor.
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeFilterRepository $attributeFilterRepository
     * @param CategoryTreeService $categoryTreeService
     * @param AttributeValueRepository $attributeValueRepository
     * @param PropertyRepository $propertyRepository
     */
    public function __construct(AttributeService $attributeService,
                                AttributeValueService $attributeValueService,
                                AttributeFilterRepository $attributeFilterRepository,
                                CategoryTreeService $categoryTreeService,
                                AttributeValueRepository $attributeValueRepository,
                                PropertyRepository $propertyRepository
    )
    {
        parent::__construct($attributeService, $attributeValueService, $attributeFilterRepository, $categoryTreeService);
        $this->_attributeValueRepository = $attributeValueRepository;
        $this->_propertyRepository = $propertyRepository;
    }

    /**
     * @param $params
     * @return array|bool
     */
    protected function getPriceRange($params)
    {
        $priceQuery = $this->_propertyRepository
            ->joinWith(['category'], false)
            ->select([
                'min' => new Expression('MIN(default_price)'),
                'max' => new Expression('MAX(default_price)'),
            ], true);

        $criteria = $this->applyDefaultParams($params);
//        $priceQuery = $this->applyQueryParams($priceQuery, $availableAttributes, $params);
        $prices = $priceQuery->findOneByCriteria($criteria, true);
        $prices['min'] = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency'], $prices['min']);
        $prices['max'] = CurrencyHelper::convert('RUB', Yii::$app->params['app_currency'], $prices['max']);

        return $prices;
    }

    /**
     * @param $params
     * @param $availableAttributes
     * @param $attributeFilters
     * @return array
     */
    protected function getAttributeValues($params, $availableAttributes, $attributeFilters)
    {
        $notRangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] !== AttributeFilter::TYPE_RANGE;
        });
        $rangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] === AttributeFilter::TYPE_RANGE;
        });

        $usedAttributesQuery = $this->_attributeValueRepository
            ->select(['attribute_value.attribute_id, min(property_attribute.value_number) as min, max(property_attribute.value_number) as max'], true)
            ->joinWith(['propertyAttributes.property.category'], false)
            ->groupBy(['attribute_value.attribute_id']);
        $criteria = $this->applyDefaultParams($params);
        $criteria = ['and', $criteria, ['attribute_value.attribute_id' => array_keys($rangeFilters)]];
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);
        $numericValues = $usedAttributesQuery->findManyByCriteria($criteria, true);
        $values = FilterSqlMinMaxMapper::getMappedData($numericValues);

        $usedAttributesQuery = $this->_attributeValueRepository
            ->select(['attribute_value.alias as value_alias, attribute_value.attribute_id, count(property_attribute.id) as count'], true)
            ->joinWith(['propertyAttributes.property.category'], false)
            ->groupBy(['attribute_value.attribute_id', 'attribute_value.id'])
            ->having('count > 2')
            ->orderBy(['count' => SORT_DESC]);
        $criteria = $this->applyDefaultParams($params);
        $criteria = ['and', $criteria, ['attribute_value.attribute_id' => array_keys($notRangeFilters)]];
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);

        return array_merge($values, $usedAttributesQuery->findManyByCriteria($criteria, true));
    }

    /**
     * @param $params
     * @return array
     */
    protected function applyDefaultParams($params)
    {
        $filterConditionHelper = new FilterConditionHelper();
        $criteria = $filterConditionHelper->process($params, ['box', 'category', 'status', 'region_id', 'country_id', 'radius', 'polygon', 'price'], 'property');
        $propertyOperation = ArrayHelper::remove($params, 'operation');
        if ($propertyOperation !== null && in_array($propertyOperation, $this->_propertyOperationToType)) {
            $criteria = ['and', $criteria, ['property.type' => $this->_propertyOperationToType[$propertyOperation]]];
        }

        return $criteria;
    }
}