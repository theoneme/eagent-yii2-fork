<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 12.09.2018
 * Time: 14:00
 */

namespace frontend\services\sql;

use common\models\AttributeFilter;
use common\repositories\sql\AttributeFilterRepository;
use common\repositories\sql\AttributeValueRepository;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use frontend\helpers\FilterConditionHelper;
use frontend\mappers\filter\FilterSqlMinMaxMapper;
use frontend\services\BaseBuildingFilterService;
use yii\helpers\ArrayHelper;

/**
 * Class BuildingFilterService
 * @package frontend\services\sql
 */
class BuildingFilterService extends BaseBuildingFilterService
{
    /**
     * @var AttributeValueRepository
     */
    protected $_attributeValueRepository;

    /**
     * BuildingFilterService constructor.
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeFilterRepository $attributeFilterRepository
     * @param AttributeValueRepository $attributeValueRepository
     */
    public function __construct(AttributeService $attributeService,
                                AttributeValueService $attributeValueService,
                                AttributeFilterRepository $attributeFilterRepository,
                                AttributeValueRepository $attributeValueRepository
    )
    {
        parent::__construct($attributeService, $attributeValueService, $attributeFilterRepository);
        $this->_attributeValueRepository = $attributeValueRepository;
    }

    /**
     * @param $params
     * @param $availableAttributes
     * @param $attributeFilters
     * @return array
     */
    protected function getAttributeValues($params, $availableAttributes, $attributeFilters)
    {
        $notRangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] !== AttributeFilter::TYPE_RANGE;
        });
        $rangeFilters = array_filter($attributeFilters, function ($value) {
            return (int)$value['type'] === AttributeFilter::TYPE_RANGE;
        });

        $usedAttributesQuery = $this->_attributeValueRepository
            ->select(['attribute_value.attribute_id, min(building_attribute.value_number) as min, max(building_attribute.value_number) as max'], true)
            ->joinWith(['buildingAttributes.building'], false)
            ->groupBy(['attribute_value.attribute_id']);
        $criteria = $this->applyDefaultParams($params);
        $criteria = ['and', $criteria, ['attribute_value.attribute_id' => array_keys($rangeFilters)]];
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);
        $numericValues = $usedAttributesQuery->findManyByCriteria($criteria, true);
        $values = FilterSqlMinMaxMapper::getMappedData($numericValues);

        $usedAttributesQuery = $this->_attributeValueRepository
            ->select(['attribute_value.alias as value_alias, attribute_value.attribute_id, count(building_attribute.id) as count'], true)
            ->joinWith(['buildingAttributes.building'], false)
            ->groupBy(['attribute_value.attribute_id', 'attribute_value.id'])
            ->having('count > 2')
            ->orderBy(['count' => SORT_DESC]);
        $criteria = $this->applyDefaultParams($params);
        $criteria = ['and', $criteria, ['attribute_value.attribute_id' => array_keys($notRangeFilters)]];
        $usedAttributesQuery = $this->applyQueryParams($usedAttributesQuery, $availableAttributes, $params);

        return array_merge($values, $usedAttributesQuery->findManyByCriteria($criteria, true));
    }

    /**
     * @param $params
     * @return array
     */
    protected function applyDefaultParams($params)
    {
        $filterConditionHelper = new FilterConditionHelper();
        $criteria = $filterConditionHelper->process($params, ['box', 'status', 'region_id', 'country_id', 'radius', 'polygon'], 'building');
        $buildingOperation = ArrayHelper::remove($params, 'operation');
        if ($buildingOperation !== null && in_array($buildingOperation, $this->_buildingTypeToType)) {
            $criteria = ['and', $criteria, ['building.type' => $this->_buildingTypeToType[$buildingOperation]]];
        }

        return $criteria;
    }
}