<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 27.02.2019
 * Time: 11:28
 */

namespace frontend\filters;

use common\repositories\sql\CompanyMemberRepository;
use yii\web\Request;

/**
 * Class CompanyAccessFilter
 * @package frontend\filters
 */
class CompanyAccessRule extends \yii\filters\AccessRule
{
    /**
     * @var bool
     */
    public $allow = true;
    /**
     * @var array
     */
    public $roles = ['@'];
    /**
     * @var string
     */
    public $companyField = 'id';
    /**
     * @var CompanyMemberRepository
     */
    private $_companyMemberRepository;

    /**
     * CompanyAccessFilter constructor.
     * @param array $config
     * @param CompanyMemberRepository $companyMemberRepository
     */
    public function __construct(CompanyMemberRepository $companyMemberRepository, array $config = [])
    {
        parent::__construct($config);

        $this->_companyMemberRepository = $companyMemberRepository;
    }

    /**
     * @param \yii\base\Action $action
     * @param false|\yii\web\User $user
     * @param \yii\web\Request $request
     * @return bool|null
     */
    public function allows($action, $user, $request)
    {
        $success = parent::allows($action, $user, $request);
        if ($success !== true) {
            return $success;
        }

        return $this->hasAccess($request, $user);
    }

    /**
     * @param Request $request
     * @param $user
     * @return bool
     */
    private function hasAccess($request, $user)
    {
        $id = $request->get($this->companyField);

        $existsInCompany = $this->_companyMemberRepository->existsByCriteria(['company_id' => $id, 'user_id' => $user->id]);
        return $existsInCompany;
    }
}