<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms;

use common\helpers\UtilityHelper;
use common\models\City;
use common\models\Translation;
use common\services\entities\CityService;
use common\services\entities\ReportService;
use Yii;
use yii\base\Model;
use yii\web\Cookie;
use yii\web\CookieCollection;

/**
 * Class ReportForm
 * @package frontend\forms
 */
class ReportForm extends Model
{
    /**
     * @var array
     */
    public $id;
    /**
     * @var integer
     */
    public $firstName;
    /**
     * @var integer
     */
    public $lastName;
    /**
     * @var integer
     */
    public $middleName;
    /**
     * @var integer
     */
    public $phone;
    /**
     * @var string
     */
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'firstName', 'lastName', 'email'], 'required'],
            [['email'], 'email'],
            [['firstName', 'lastName', 'middleName'], 'string', 'max' => 35],
            [['phone'], 'string', 'max' => 15],
            [['id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'firstName' => Yii::t('labels', 'First Name'),
            'lastName' => Yii::t('labels', 'Last Name'),
            'middleName' => Yii::t('labels', 'Middle Name'),
            'phone' => Yii::t('labels', 'Phone'),
        ];
    }
}
