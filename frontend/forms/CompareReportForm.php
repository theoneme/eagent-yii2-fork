<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms;

use Yii;
use yii\base\Model;

/**
 * Class CompareReportForm
 * @package frontend\forms
 */
class CompareReportForm extends Model
{
    /**
     * @var array
     */
    public $id;
    /**
     * @var array
     */
    public $comments;
    /**
     * @var array
     */
    public $prices;
    /**
     * @var string
     */
    public $message;
    /**
     * @var integer
     */
    public $property_area;
    /**
     * @var integer
     */
    public $rooms;
    /**
     * @var string
     */
    public $address;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'safe'],
            [['comments', 'prices'], 'safe'],
            [['message', 'address'], 'string'],
            [['property_area', 'rooms'], 'number']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'message' => Yii::t('wizard', 'Summary')
        ];
    }
}
