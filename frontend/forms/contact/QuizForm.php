<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\contact;

use common\models\Currency;
use Yii;
use yii\base\Model;

/**
 * Class QuizForm
 * @package frontend\forms\contact
 */
class QuizForm extends Model
{
    public $email;
    public $name;
    public $buy;
    public $interval;
    public $mortgage;
    public $mortgageAmount;
    public $secondary;
    public $address;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mortgage', 'buy', 'secondary'], 'boolean'],
            [['interval', 'mortgageAmount'], 'integer'],
            [['name', 'email', 'address'], 'string', 'max' => 55]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'buy' => Yii::t('labels', 'Do you plan to buy condo?'),
            'interval' => Yii::t('labels', 'When do you plan on buying a property'),
            'mortgage' => Yii::t('labels', 'Do you need mortgage?'),
            'mortgageAmount' => Yii::t('labels', 'Credit Amount'),
            'address' => Yii::t('labels', 'Address'),
            'secondary' => Yii::t('labels', 'Do you plan to sell secondary property?'),
            'name' => Yii::t('labels', 'Name')
        ];
    }
}
