<?php

namespace frontend\forms\contact;

use Yii;

/**
 * Class ContactForm
 * @package frontend\forms
 */
class ContactForm extends BaseContactForm
{
    /**
     * @var integer
     */
    public $phone;
    /**
     * @var string
     */
    public $lat;
    /**
     * @var string
     */
    public $lng;
    /**
     * @var string
     */
    public $address;
    ///**
     //* @var string
     //*/
   // public $floor;
    /**
     * @var string
     */
    public $rooms;
    /**
     * @var string
     */
    public $area;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['address'], 'required'],
            [['address'], 'string', 'max' => 255],
            //[['floor', 'rooms', 'area', 'lat', 'lng'], 'number'],
            [['phone'], 'string', 'max' => 15],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'phone' => Yii::t('labels', 'Your Phone'),
            'address' => Yii::t('labels', 'Address of your property'),
            //'floor' => Yii::t('labels', 'Floor'),
            'rooms' => Yii::t('labels', 'Rooms'),
            'area' => Yii::t('labels', 'Area'),
        ]);
    }
}
