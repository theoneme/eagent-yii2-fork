<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\contact;

use common\models\Currency;
use Yii;

/**
 * Class MortgageForm
 * @package frontend\forms\contact
 */
class MortgageForm extends BaseContactForm
{
    public const STATUS_SINGLE = 0;
    public const STATUS_MARRIED = 10;
    public const STATUS_DIVORCED = 20;

    /**
     * @var string
     */
    public $propertyName;
    /**
     * @var integer
     */
    public $credit;
    /**
     * @var integer
     */
    public $currencyCode;
    /**
     * @var integer
     */
    public $loanTerm;
    /**
     * @var integer
     */
    public $age;
    /**
     * @var string
     */
    public $familyStatus;
    /**
     * @var integer
     */
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['propertyName', 'credit', 'loanTerm', 'familyStatus', 'age'], 'required'],
            [['phone', 'familyStatus'], 'string', 'max' => 15],
            [['propertyName'], 'string', 'max' => 85],
            [['currencyCode'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currencyCode' => 'code']],
            ['familyStatus', 'in', 'range' => [
                self::STATUS_DIVORCED,
                self::STATUS_MARRIED,
                self::STATUS_SINGLE
            ]]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'propertyName' => Yii::t('labels', 'I want to get approval from the bank to the property'),
            'credit' => Yii::t('labels', 'Credit Amount'),
            'loanTerm' => Yii::t('labels', 'Loan Term (years)'),
            'age' => Yii::t('labels', 'Your Age'),
            'familyStatus' => Yii::t('labels', 'Your Family Status'),
            'phone' => Yii::t('labels', 'Your Phone'),
            'currencyCode' => Yii::t('labels', 'Currency'),
        ]);
    }

    /**
     * @param $status
     * @return string
     */
    public function getFamilyStatus($status)
    {
        switch($status) {
            case self::STATUS_DIVORCED:
                $familyStatus = Yii::t('property', 'Single');
                break;
            case self::STATUS_MARRIED:
                $familyStatus = Yii::t('property', 'Married');
                break;
            default:
                $familyStatus = Yii::t('property', 'Single');
        }

        return $familyStatus;
    }
}
