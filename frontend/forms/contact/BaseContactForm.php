<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\contact;

use Yii;
use yii\base\Model;

/**
 * Class BaseContactForm
 * @package frontend\forms\contact
 */
class BaseContactForm extends Model
{
    /**
     * @var array
     */
    public $name;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $message;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            [['email'], 'email'],
            [['message'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('labels', 'Your Name'),
            'message' => Yii::t('labels', 'Your Message'),
            'email' => Yii::t('labels', 'Your Email'),
        ];
    }
}
