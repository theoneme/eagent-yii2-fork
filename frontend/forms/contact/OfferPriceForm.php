<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\contact;

use common\models\Currency;
use Yii;

/**
 * Class OfferPriceForm
 * @package frontend\forms\contact
 */
class OfferPriceForm extends BaseContactForm
{
    /**
     * @var integer
     */
    public $price;
    /**
     * @var string
     */
    public $currency_code;
    /**
     * @var integer
     */
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['price'], 'integer'],
            [['price', 'currency_code'], 'required'],
            [['phone'], 'string', 'max' => 15],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'price' => Yii::t('labels', 'Your Price'),
            'currency_code' => Yii::t('labels', 'Currency'),
            'phone' => Yii::t('labels', 'Your Phone'),
        ]);
    }
}
