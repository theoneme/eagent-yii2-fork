<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\contact;

use Yii;

/**
 * Class MessageForm
 * @package frontend\forms\contact
 */
class MessageForm extends BaseContactForm
{
    /**
     * @var integer
     */
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['phone'], 'string', 'max' => 15],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'phone' => Yii::t('labels', 'Your Phone'),
        ]);
    }
}
