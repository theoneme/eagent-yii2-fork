<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\contact;

use Yii;

/**
 * Class ContactAgentForm
 * @package frontend\forms
 */
class ContactAgentForm extends BaseContactForm
{
    /**
     * @var integer
     */
    public $phone;
    /**
     * @var string
     */
    public $url;
    /**
     * @var integer
     */
    public $agentId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['agentId'], 'integer'],
//            [['url'], 'required'],
            [['url'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'phone' => Yii::t('labels', 'Phone'),
        ]);
    }
}
