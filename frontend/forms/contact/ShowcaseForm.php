<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\contact;

use Yii;

/**
 * Class ShowcaseForm
 * @package frontend\forms\contact
 */
class ShowcaseForm extends BaseContactForm
{
    /**
     * @var string
     */
    public $date;
    /**
     * @var string
     */
    public $time;
    /**
     * @var string
     */
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['date'], 'required'],
            [['phone', 'date'], 'string', 'max' => 15],
            [['time'], 'string', 'max' => 75],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'date' => Yii::t('labels', 'Date'),
            'time' => Yii::t('labels', 'Time'),
            'phone' => Yii::t('labels', 'Your Phone'),
        ]);
    }
}
