<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\forms\ar;

use common\forms\ar\RequestForm as BaseRequestForm;
use common\helpers\Auth;
use common\models\Property;
use Yii;

/**
 * Class RequestForm
 * @package frontend\forms\ar
 */
class RequestForm extends BaseRequestForm
{
    /**
     * @var
     */
    public $steps;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'defaultStatus' => ['status', 'default', 'value' => Property::STATUS_REQUIRES_MODERATION]
        ]);
    }

    /**
     * @param array $data
     * @param null $formName
     * @param array $exceptions
     * @return bool
     */
    public function load($data, $formName = null, $exceptions = [])
    {
        $success = parent::load($data, $formName, $exceptions);
        $this->user_id = Auth::user()->getCurrentId();
        $this->locale = $this->locale ?? Yii::$app->language;

        return $success;
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}