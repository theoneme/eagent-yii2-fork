<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\forms\ar;

use common\forms\ar\UserForm as BaseUserForm;

/**
 * Class UserForm
 * @package frontend\forms\ar
 */
class UserForm extends BaseUserForm
{
    /**
     * @var array
     */
    public $steps;

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}