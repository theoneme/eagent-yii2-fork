<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\forms\ar;

use common\forms\ar\composite\CategoryForm;
use common\forms\ar\composite\GeoForm;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\composite\PriceForm;
use common\forms\ar\PropertyForm as BasePropertyForm;
use common\helpers\Auth;
use common\models\Property;
use Yii;

/**
 * Class PropertyForm
 * @package frontend\forms\ar
 */
class PropertyForm extends BasePropertyForm
{
    /**
     * @var
     */
    public $steps;

    /**
     * PropertyForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->geo = new GeoForm(['rules' => [
            ['address', 'required']
        ]]);
        $this->meta = array_map(function () {
            return new MetaForm();
        }, Yii::$app->params['languages']);
        $this->price = new PriceForm();
        $this->category = new CategoryForm();
        $this->attachment = [];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'defaultStatus' => ['status', 'default', 'value' => Property::STATUS_REQUIRES_MODERATION]
        ]);
    }

    /**
     * @param array $data
     * @param null $formName
     * @param array $exceptions
     * @return bool
     */
    public function load($data, $formName = null, $exceptions = [])
    {
        $success = parent::load($data, $formName);
        $this->user_id = Auth::user()->getCurrentId();
        $this->locale = $this->locale ?? Yii::$app->language;

        return $success;
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}