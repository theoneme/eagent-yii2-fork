<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace frontend\forms\ar;

/**
 * Class CompanyForm
 * @package frontend\forms\ar
 */
class CompanyForm extends \common\forms\ar\CompanyForm
{
    /**
     * @var array
     */
    public $steps;

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}