<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms;

use Yii;
use yii\base\Model;

/**
 * Class PreCompareReportForm
 * @package frontend\forms
 */
class PreCompareReportForm extends Model
{
    /**
     * @var number
     */
    public $property_area;
    /**
     * @var number
     */
    public $lat;
    /**
     * @var number
     */
    public $lng;
    /**
     * @var number
     */
    public $rooms;
    /**
     * @var string
     */
    public $address;
    /**
     * @var string
     */
    public $category;
    /**
     * @var array
     */
    public $comments;
    /**
     * @var array
     */
    public $prices;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'property_area', 'rooms'], 'required'],
            [['property_area', 'rooms', 'lat', 'lng'], 'number'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'address' => Yii::t('wizard', 'Property Address'),
            'property_area' => Yii::t('wizard', 'Property Total Area'),
            'rooms' => Yii::t('wizard', 'Property Number of Rooms')
        ];
    }
}
