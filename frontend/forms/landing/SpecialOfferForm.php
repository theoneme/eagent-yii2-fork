<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms\landing;

use frontend\forms\contact\BaseContactForm;
use Yii;

/**
 * Class SpecialOfferForm
 * @package frontend\forms\landing
 */
class SpecialOfferForm extends BaseContactForm
{
    /**
     * @var string
     */
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['phone'], 'string'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'phone' => Yii::t('labels', 'Phone'),
        ]);
    }
}
