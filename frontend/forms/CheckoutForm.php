<?php
namespace frontend\forms;

use common\models\Currency;
use common\models\Payment;
use Yii;
use yii\base\Model;

/**
 * Class ReportForm
 * @package frontend\forms
 */
class CheckoutForm extends Model
{
    /**
     * @var string
     */
    public $type;
    /**
     * @var integer
     */
    public $total;
    /**
     * @var integer
     */
    public $currency_code;
    /**
     * @var integer
     */
    public $entity_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            ['type', 'in', 'range' => [Payment::TYPE_REPLENISH, Payment::TYPE_TARIFF]],
            [['total'], 'required', 'when' => function ($model) {
                return $model->type === Payment::TYPE_REPLENISH;
            }],
            [['entity_id'], 'required', 'when' => function ($model) {
                return $model->type === Payment::TYPE_TARIFF;
            }],
            [['total', 'entity_id'], 'integer'],
            [['currency_code'], 'string', 'max' => 3],
            [['currency_code'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::class, 'targetAttribute' => ['currency_code' => 'code']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'firstName' => Yii::t('labels', 'First Name'),
            'lastName' => Yii::t('labels', 'Last Name'),
            'middleName' => Yii::t('labels', 'Middle Name'),
            'phone' => Yii::t('labels', 'Phone'),
        ];
    }

    /**
     * @return integer
     */
    public function getTotal(){
        switch ($this->type) {
            case Payment::TYPE_REPLENISH:
                return $this->total;
                break;
            case Payment::TYPE_TARIFF:
                // Вычислить из тарифа цену
                break;
        }
    }
}
