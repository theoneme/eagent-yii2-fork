<?php

namespace frontend\forms;

use common\helpers\DataHelper;
use common\models\Category;
use common\models\Property;
use common\models\Request;
use Yii;
use yii\base\Model;

/**
 * Class StepZeroForm
 * @package frontend\forms
 */
class StepZeroForm extends Model
{
    /**
     * @var integer
     */
    public $parent_category_id;
    /**
     * @var integer
     */
    public $category_id;
    /**
     * @var integer
     */
    public $type;
    /**
     * @var string
     */
    public $locale;
    /**
     * @var array
     */
    public $categories;
    /**
     * @var array
     */
    public $locales;
    /**
     * @var array
     */
    public $types;

    /**
     * LocationForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->categories = DataHelper::getCategories();
        $this->locales = DataHelper::getLocales(true);
        $this->types = [
            'p_sale' => [
                'label' => Yii::t('wizard', 'I want to list my property for sale'),
                'action' => ['/property/manage/create'],
                'value' => Property::TYPE_SALE
            ],
            'p_rent' => [
                'label' => Yii::t('wizard', 'I want to list my property for rent'),
                'action' => ['/property/manage/create'],
                'value' => Property::TYPE_RENT
            ],
            'r_sale' => [
                'label' => Yii::t('wizard', 'I want to place a request to purchase a property'),
                'action' => ['/request/manage/create'],
                'value' => Request::TYPE_SALE
            ],
            'r_rent' => [
                'label' => Yii::t('wizard', 'I want to place a request to rent a my property'),
                'action' => ['/request/manage/create'],
                'value' => Request::TYPE_RENT
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_category_id', 'category_id', 'type', 'locale'], 'required'],
            [['parent_category_id'], 'in', 'range' => array_keys($this->categories)],
            [['type'], 'in', 'range' => [Property::TYPE_SALE, Property::TYPE_RENT, Request::TYPE_SALE, Request::TYPE_RENT]],
            [['locale'], 'in', 'range' => array_keys(Yii::$app->params['supportedLocales'])],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('wizard', 'Please choose type of your advertisement:'),
            'locale' => Yii::t('wizard', 'Language'),
            'parent_category_id' => Yii::t('wizard', 'Property type'),
            'category_id' => Yii::t('wizard', 'Property subtype'),
        ];
    }
}
