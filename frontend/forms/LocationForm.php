<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 06.06.2018
 * Time: 16:46
 */

namespace frontend\forms;

use common\models\City;
use common\models\Country;
use common\models\Region;
use Yii;
use yii\base\Model;
use yii\web\Cookie;
use yii\web\CookieCollection;

/**
 * Class LocationForm
 * @package frontend\forms
 */
class LocationForm extends Model
{
    /**
     * @var string
     */
    public $entity;
    /**
     * @var string
     */
    public $slug;
    /**
     * @var array
     */
    public $object;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slug', 'entity'], 'required'],
            [['slug'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['slug' => 'slug'], 'when' => function ($model) {
                return $model->entity === 'country';
            }],
            [['slug'], 'exist', 'skipOnError' => true, 'targetClass' => Region::class, 'targetAttribute' => ['slug' => 'slug'], 'when' => function ($model) {
                return $model->entity === 'region';
            }],
            [['slug'], 'exist', 'skipOnError' => true, 'targetClass' => City::class, 'targetAttribute' => ['slug' => 'slug'], 'when' => function ($model) {
                return $model->entity === 'city';
            }],
        ];
    }

    /**
     * @param $input
     * @param CookieCollection $cookies
     * @return bool
     * @throws \ReflectionException
     */
    public function saveLocation($input, CookieCollection $cookies)
    {
        $formName = null;
        if (!array_key_exists((new \ReflectionClass($this))->getShortName(), $input)) {
            $formName = '';
        }

        $this->load($input, $formName);

        if (!$this->validate()) {
            return false;
        }

        switch ($this->entity) {
            case 'country':
                $locationData['country_id'] = $this->object['id'];

                break;
            case 'region':
                $locationData['region_id'] = $this->object['id'];
                $locationData['country_id'] = $this->object['country_id'];

                break;
            case 'city':
            default:
                $locationData['city_id'] = $this->object['id'];
                $locationData['region_id'] = $this->object['region_id'];
                $locationData['country_id'] = $this->object['country_id'];
        }

        $locationData['created_at'] = time();
        $locationData['entity'] = $this->entity;
        $locationData['lat'] = $this->object['lat'] ?? null;
        $locationData['lng'] = $this->object['lng'] ?? null;
        $locationData['slug'] = $this->object['slug'];

        $cookies->add(new Cookie([
            'name' => 'location',
            'value' => $locationData,
            'domain' => '.' . Yii::$app->params['domainName'],
            'expire' => time() + 36000,
        ]));

        Yii::$app->params['runtime']['location'] = $locationData;

        return true;
    }
}
