<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace backend\forms\ar;

use common\forms\ar\PropertyForm as BasePropertyForm;
use common\models\Property;

/**
 * Class PropertyForm
 * @package backend\forms\ar
 */
class PropertyForm extends BasePropertyForm
{
    /**
     * @var array
     */
    public $steps;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status', 'type'], 'integer'],
            [['status'], 'required'],
            [['status'], 'in', 'range' => [
                Property::STATUS_DELETED,
                Property::STATUS_ACTIVE,
                Property::STATUS_REQUIRES_MODERATION,
                Property::STATUS_REQUIRES_MODIFICATION
            ]],
            [['user_id'], 'required']
        ]);
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}