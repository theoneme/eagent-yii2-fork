<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace backend\forms\ar;

use common\forms\ar\RequestForm as BaseRequestForm;
use common\models\Request;

/**
 * Class RequestForm
 * @package backend\forms\ar
 */
class RequestForm extends BaseRequestForm
{
    /**
     * @var
     */
    public $steps;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status', 'type'], 'integer'],
            [['status'], 'required'],
            [['status'], 'in', 'range' => [
                Request::STATUS_DELETED,
                Request::STATUS_ACTIVE,
                Request::STATUS_REQUIRES_MODERATION,
                Request::STATUS_REQUIRES_MODIFICATION
            ]],
            [['user_id'], 'required']
        ]);
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}