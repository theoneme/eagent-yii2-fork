<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace backend\forms\ar;
use common\models\Company;

/**
 * Class CompanyForm
 * @package backend\forms\ar
 */
class CompanyForm extends \common\forms\ar\CompanyForm
{
    /**
     * @var array
     */
    public $steps;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['status'], 'integer'],
            [['status'], 'required'],
            [['status'], 'in', 'range' => [
                Company::STATUS_DELETED,
                Company::STATUS_ACTIVE,
                Company::STATUS_REQUIRES_MODERATION,
                Company::STATUS_REQUIRES_MODIFICATION
            ]]
        ]);
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}