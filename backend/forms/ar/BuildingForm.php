<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 05.12.2016
 * Time: 13:48
 */

namespace backend\forms\ar;

use common\forms\ar\composite\BuildingMetaForm;
use common\forms\ar\composite\GeoForm;
use common\models\Building;
use Yii;

/**
 * Class BuildingForm
 * @package backend\forms\ar
 */
class BuildingForm extends \common\forms\ar\BuildingForm
{
    /**
     * @var
     */
    public $steps;


    /**
     * BuildingForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->geo = new GeoForm(['rules' => [
            [['lat', 'lng'], 'unique', 'targetClass' => Building::class, 'when' => function ($model) {
                return $model->lat !== (float)$this->_building->lat && $model->lat !== (float)$this->_building->lng;
            }],
            ['address', 'required']
        ]]);
        $this->meta = array_map(function () {
            return new BuildingMetaForm();
        }, Yii::$app->params['languages']);
        $this->attachment = [];
        $this->phases = [];
        $this->progress = [];
        $this->sites = [];
        $this->documents = [];
    }

    /**
     * @param $config
     */
    public function buildLayout($config)
    {
        $this->steps = $config;
    }
}