<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.10.2018
 * Time: 16:34
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

\common\assets\GoogleAsset::register($this);

/**
 * @var \backend\models\search\CompanySearch $search
 * @var \common\data\MyArrayDataProvider $dataProvider
 */

?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Companies list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <p>
            <?= Html::a(Yii::t('main', 'Create Company'), ['/company/company/create'], [
                'title' => Yii::t('main', 'Create Company'),
                'class' => 'btn btn-success',
                'data-action' => 'load-edit-modal'
            ]) ?>
        </p>
        <?php Pjax::begin(['id' => 'company-pjax', 'timeout' => 6000]) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'company-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Имя',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'],
                            ['/company/company/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['company']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                'created_at:datetime',
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'value' => function ($model) {
                        /* @var array $model */
                        return \common\decorators\CompanyStatusDecorator::decorate($model['status']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'filter' => \common\decorators\UserStatusDecorator::getStatusLabels(false)
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{members} {update} {delete}',
                    'buttons' => [
                        'members' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, [
                                'title' => 'Company Members',
                                'data-pjax' => 0
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Company',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'members':
                                $url = Url::toRoute(["/company/company-member/index", 'company_id' => $model['id']]);
                                break;
                            case 'update':
                                $url = Url::toRoute(["/company/company/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/company/company/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
