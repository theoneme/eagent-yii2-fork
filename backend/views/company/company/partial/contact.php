<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 21.12.2018
 * Time: 16:33
 */

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/**
 * @var \common\forms\ar\composite\ContactForm $contactForm
 * @var boolean $createForm
 * @var ActiveForm|null $form
 * @var boolean $createForm
 * @var integer $iterator
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = new ActiveForm([
        'id' => 'company-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]);
    ob_end_clean(); ?>
<?php } ?>

    <div class="row" data-role="contact-item">
        <div class="col-md-5">
            <?= $form->field($contactForm, "[{$iterator}]type")->dropDownList(\common\helpers\DataHelper::getContacts(), [
                'prompt' => '-- ' . Yii::t('wizard', 'Select contact type')
            ])->label(false) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($contactForm, "[{$iterator}]value")->textInput(['placeholder' => Yii::t('wizard', 'Enter contact info')])->label(false) ?>
        </div>
        <div class="col-md-1">
            <?= Html::a('<i class="fa fa-close"></i>', '#', ['data-action' => 'remove-contact']) ?>
        </div>
    </div>

<?php if ($createForm === true) { ?>
    <?php $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
    var attributes = $attributes;
    $.each(attributes, function() {
        $("#company-form").yiiActiveForm("add", this);
    });
JS;
    $this->registerJs($script);
} ?>