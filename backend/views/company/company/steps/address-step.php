<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \common\forms\ar\CompanyForm $companyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>


<?= \common\widgets\GmapsActiveInputWidget::widget([
    'model' => $companyForm->geo,
    'form' => $form,
    'inputOptions' => ['class' => 'form-control'],
    'withMap' => true
]) ?>

<div id="google-map" style="height: 300px;">

</div>


