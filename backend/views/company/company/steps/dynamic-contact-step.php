<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $step
 * @var \frontend\forms\ar\CompanyForm $companyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

    <div data-role="dynamic-contacts-container">
        <?php foreach ($companyForm->contact as $key => $contact) { ?>
            <div class="row" data-role="contact-item">
                <div class="col-md-5">
                    <?= $form->field($contact, "[{$key}]type", ['template' => '{input}{error}'])
                        ->dropDownList(\common\helpers\DataHelper::getContacts(), [
                            'prompt' => '-- ' . Yii::t('wizard', 'Select contact type')
                        ])->label(false) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($contact, "[{$key}]value", ['template' => '{input}{error}'])
                        ->textInput(['placeholder' => Yii::t('wizard', 'Enter contact info')])
                        ->label(false) ?>
                </div>
                <div class="col-md-1">
                    <?= Html::a('<i class="fa fa-close"></i>', '#', ['data-action' => 'remove-contact']) ?>
                </div>
            </div>

        <?php } ?>
    </div>

<?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('wizard', 'Add New Contact'), '#', [
    'data-action' => 'add-new-contact'
]) ?>

<?php $contactCount = count($companyForm->contact);
$contactUrl = Url::to(['/company/company/render-contact']);
$script = <<<JS
    var iterator = {$contactCount};

    $('[data-action=add-new-contact]').on('click', function() {
        $.get('{$contactUrl}', {iterator: iterator}, function(result) {
            if(result.success === true) {
                $('[data-role=dynamic-contacts-container').append(result.html);
            } 
            iterator++;
        });
        
        return false;
    });

    $(document).on('click', '[data-action=remove-contact]', function() {
        $(this).closest('[data-role=contact-item]').remove(); 
        
        return false;
    });
JS;

$this->registerJs($script);