<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

/**
 * @var array $step
 * @var \common\forms\ar\CompanyForm $companyForm
 * @var \yii\widgets\ActiveForm $form
 */
?>

<?= $form->field($companyForm->meta, 'title')->textInput([
    'placeholder' => Yii::t('wizard', 'Company Title'),
]) ?>

<?= $form->field($companyForm->meta, 'description')->textarea([
    'placeholder' => Yii::t('wizard', 'Company Description'),
    'rows' => 4
]) ?>
<?= $form->field($companyForm, 'ads_allowed', ['template' => "<div class='row'>
    <div class='col-md-3'>
        {label}
    </div>
    <div class='col-md-9'>
        {input}
        Yes
        {error}
    </div>
</div>"])->checkbox([], false) ?>
<?= $form->field($companyForm, 'ads_allowed_partners', ['template' => "<div class='row'>
    <div class='col-md-3'>
        {label}
    </div>
    <div class='col-md-9'>
        {input}
        Yes
        {error}
    </div>
</div>"])->checkbox([], false) ?>