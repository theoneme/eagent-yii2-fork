<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use common\forms\ar\CompanyForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\common\assets\GoogleAsset::register($this);

/**
 * @var CompanyForm $companyForm
 * @var array $action
 * @var array $companyDTO
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'id' => 'company-form',
        'enctype' => 'multipart/form-data',
        'data-pjax' => false,
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'company-pjax'
    ],
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
]); ?>

<?php if ($companyForm->_company->isNewRecord === false) { ?>
    <p>
        <span class="label label-default">
            Ссылка на сайте: <?= Html::a('Открыть', Yii::$app->frontUrlManager->createUrl([
                '/agent/agent/view', 'id' => $companyDTO['user_id']
            ]), [
                'target' => '_blank'
            ]) ?>
        </span>
    </p>
<?php } ?>

<?= $form->field($companyForm, 'status')->dropDownList(\common\decorators\CompanyStatusDecorator::getStatusLabels(false), [
    'prompt' => '-- ' . Yii::t('main', 'Select Status')
]) ?>

<?php foreach ($companyForm->steps as $key => $step) { ?>
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title"><?= $step['title'] ?></h3>
            <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($companyForm->steps)]) ?></span>
        </div>
        <div class="box-body">
            <?php foreach ($step['config'] as $stepConfig) { ?>
                <?php if ($stepConfig['type'] === 'view') { ?>
                    <?= $this->render("steps/{$stepConfig['value']}", [
                        'form' => $form,
                        'companyForm' => $companyForm,
                        'step' => $step,
                        'createForm' => false,
                    ]) ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>

    <div class="form-group row">
        <div class="col-lg-offset-3 col-lg-9">
            <?= Html::submitButton(Yii::t('labels', 'Update'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?php $script = <<<JS
var form = $('#company-form');

form.on('submit', function() { 
    let hasToUpload = false;
    
    $.each($(".file-upload-input"), function() {
        if ($(this).fileinput("getFilesCount") > 0) {
            $(this).fileinput("upload");
            hasToUpload = true;
            return false;
        }
    });
    
    if(hasToUpload === true) {
        return false;
    }
    
    return true;
});

JS;
$this->registerJs($script);

?>