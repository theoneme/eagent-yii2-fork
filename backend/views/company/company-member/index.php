<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.10.2018
 * Time: 16:34
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

\common\assets\GoogleAsset::register($this);

/**
 * @var \common\models\search\CompanyMemberSearch $search
 */

?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Company members list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <div class="form-group">
            <?= Html::a(Yii::t('main', 'Back to companies'), ['/company/company/index'], [
                'title' => Yii::t('main', 'Back to companies'),
                'class' => 'btn btn-success',
            ]) ?>

            <?= Html::a(Yii::t('main', 'Add company member'), ['/company/company-member/create', 'company_id' => $companyId], [
                'title' => Yii::t('main', 'Add company member'),
                'class' => 'btn btn-success',
                'data-action' => 'load-edit-modal'
            ]) ?>
        </div>

        <?php Pjax::begin(['id' => 'company-pjax', 'timeout' => 6000]) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'company-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'username',
                    'header' => 'Имя',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['username'],
                            ['/user/update', 'id' => $model['user_id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['user']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'role',
                    'header' => 'Роль',
                    'value' => function ($model) {
                        /* @var array $model */
                        return \common\decorators\CompanyMemberRoleDecorator::decorate($model['role']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'filter' => \common\decorators\UserStatusDecorator::getStatusLabels(false)
                ],
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'value' => function ($model) {
                        /* @var array $model */
                        return \common\decorators\CompanyMemberStatusDecorator::decorate($model['status']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'filter' => \common\decorators\UserStatusDecorator::getStatusLabels(false)
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Company Member',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/company/company-member/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/company/company-member/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
