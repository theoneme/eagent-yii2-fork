<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.10.2018
 * Time: 10:20
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

\backend\assets\SelectizeAsset::register($this);

/**
 * @var \common\forms\ar\CompanyMemberForm $companyMemberForm
 * @var array $selectizeData
 * @var array $action
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'id' => 'company-form',
        'enctype' => 'multipart/form-data',
        'data-pjax' => false,
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'company-pjax'
    ],
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
]); ?>

<?= $form->field($companyMemberForm, 'role')->dropDownList(\common\decorators\CompanyMemberRoleDecorator::getRoleLabels(false)) ?>
<?= $form->field($companyMemberForm, 'status')->dropDownList(\common\decorators\CompanyMemberStatusDecorator::getRoleLabels(false)) ?>
<?= $form->field($companyMemberForm, 'user_id')->dropDownList($selectizeData, ['class' => 'user-autocomplete']) ?>

    <div class="form-group row">
        <div class="col-lg-offset-3 col-lg-9">
            <?= Html::submitButton(Yii::t('labels', 'Update'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>

<?php $url = Url::to(['/ajax/users']);

$script = <<<JS
    $(".user-autocomplete").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "value",
        searchField: ["value"],
        maxOptions: 10,
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.get("$url", {
                    request: query
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        },
        render: {
	        option: function(data, escape) { 
	            return '<div class="option custom-selectize-option flex flex-v-center" data-value="' + data.id + '">' +
                           '<img src="' + data.img + '">' +
                           '<span class="selectize-option-title">' + data.value + '</span>' + 
                       '</div>';
	        },
            item: function(data, escape) { 
	            return '<div class="item custom-selectize-item" data-value="' + data.value + '">' +
                          '<img src="' + data.img + '">' 
                          + data.value + 
                       '</div>';
	        }
	    },
    });
JS;

$this->registerJs($script);