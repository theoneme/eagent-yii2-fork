<?php

use backend\assets\SelectizeAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \common\forms\ar\CityForm $cityForm
 * @var array $action
 */

SelectizeAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'property-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'property-pjax'
    ],
]); ?>

<?= $form->field($cityForm, 'is_big')->checkbox() ?>
<?= $form->field($cityForm, 'slug')->textInput() ?>
<?= $form->field($cityForm, 'country_id')
    ->dropDownList(!empty($dto) ? [$cityForm['country_id'] => $dto['country']['title']] : [], ['class' => '']) ?>
<?= $form->field($cityForm, 'region_id')
    ->dropDownList(!empty($dto) ? [$cityForm['region_id'] => $dto['region']['title']] : [], ['class' => '']) ?>

    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Translations</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык.
            </div>
            <?php foreach ($cityForm->meta as $locale => $meta) { ?>
                <div class="row">
                    <div class="col-md-1">
                        <span class="label label-default"><?= $locale ?></span>
                    </div>
                    <div class="col-md-11">
                        <?= $form->field($meta, "[{$locale}]title")->textInput(['data-role' => 'city-translation-input']) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Geo</div>
        <div class="panel-body">
            <div class="alert alert-danger">После того как заполнили поля выше - обязательно клацаем Get coordinates
            </div>
            <div class="row">
                <div class="col-md-5">
                    <?= $form->field($cityForm->geo, 'lat')->textInput() ?>
                </div>
                <div class="col-md-5">
                    <?= $form->field($cityForm->geo, 'lng')->textInput() ?>
                </div>
                <div class="col-md-2">
                    <?= Html::button('Get coordinates', ['data-action' => 'get-city-coordinates', 'class' => 'btn btn-default btn-xs']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php $regionUrl = Url::to(['/ajax/regions']);
$countryUrl = Url::to(['/ajax/countries']);

$script = <<<JS
    $("#cityform-region_id").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "label",
        searchField: ["label"],
        "load": function(query, callback) {
            if (!query.length) {
                return callback();
            }
            $.get('$regionUrl', {query: query}, function(data) { 
                callback(data);
            }).fail(function() {
                callback();
            });
        }
    });

    $("#cityform-country_id").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "label",
        searchField: ["label"],
        "load": function(query, callback) {
            if (!query.length) {
                return callback();
            }
            $.get('$countryUrl', {query: query}, function(data) { 
                callback(data);
            }).fail(function() {
                callback();
            });
        }
    });
    
    var geocoder = new google.maps.Geocoder();
    $('[data-action=get-city-coordinates]').on('click', function() {
        let country = $('#cityform-country_id').find('option:selected').text(); 
        let region = $('#cityform-region_id').find('option:selected').text(); 
        let city = null;
        $.each($('[data-role=city-translation-input'), function() {
            if($(this).val()) {
                city = $(this).val();
            }
        });
        
        let address = country + ' ' + region + ' ' + city;
        geocoder.geocode( { 'address': address}, function(results, status) { 
            if (status == 'OK') { 
                $('#lightgeoform-lat').val(results[0].geometry.location.lat());
                $('#lightgeoform-lng').val(results[0].geometry.location.lng());
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    });
JS;

$this->registerJs($script);
