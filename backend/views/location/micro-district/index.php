<?php

use backend\models\search\MicroDistrictSearch;
use common\assets\GoogleAsset;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var MicroDistrictSearch $search
 */

GoogleAsset::register($this);

?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">District list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'property-pjax', 'timeout' => 6000]) ?>

        <div class="form-group">
            <?= Html::a('Create micro district', ['/location/micro-district/create'], [
                'data-action' => 'load-edit-modal',
                'title' => 'Create micro district',
                'class' => 'btn btn-success',
                'data-pjax' => 0
            ]) ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'request',
                    'header' => 'Микрорайон',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'] ?? '',
                            ['/location/micro-district/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['micro district']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'district_title',
                    'header' => 'Район',
                    'value' => function ($model) {
                        /** @var array $model */
                        return $model['district_id'] ? Html::a($model['district']['title'],
                            ['/location/district/update', 'id' => $model['district_id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['district']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        ) : '';
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'city_title',
                    'header' => 'Город',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['city']['title'],
                            ['/location/city/update', 'id' => $model['city_id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['city']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Region',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/location/micro-district/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/location/micro-district/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
