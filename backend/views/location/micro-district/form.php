<?php

use backend\assets\SelectizeAsset;
use common\forms\ar\MicroDistrictForm;
use common\widgets\GmapsActivePolygonWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var MicroDistrictForm $model
 * @var array $action
 */

SelectizeAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'property-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'property-pjax'
    ],
]); ?>

<?= $form->field($model, 'slug')->textInput() ?>
<?= $form->field($model, 'city_id')->dropDownList(!empty($dto['city_id']) ? [$model['city_id'] => $dto['city']['title']] : [], ['class' => '']) ?>
<?= $form->field($model, 'district_id')->dropDownList(!empty($dto['district_id']) ? [$model['district_id'] => $dto['district']['title']] : [], ['class' => '']) ?>

    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Translations</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык.
            </div>
            <?php foreach ($model->meta as $locale => $meta) { ?>
                <div class="row">
                    <div class="col-md-1">
                        <span class="label label-default"><?= $locale ?></span>
                    </div>
                    <div class="col-md-11">
                        <?= $form->field($meta, "[{$locale}]title")->textInput(['data-role' => 'district-translation-input']) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Импорт полигона</div>
        <div class="panel-body">
            <p>
                Пытается импортировать полигон с Open Street Map. Нажимать после того, как заполнили город и название района(на русском)
            </p>
            <?= Html::button(Yii::t('labels', 'Import'), ['class' => 'btn btn-block btn-danger import-polygon']) ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Полигон</div>
        <div class="panel-body">
            <?= GmapsActivePolygonWidget::widget(['models' => $model->geo, 'formName' => 'LightGeoForm'])?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$cityUrl = Url::to(['/ajax/cities']);
$districtUrl = Url::to(['/ajax/districts']);
$polygonUrl = Url::to(['/ajax/polygon-import']);

$script = <<<JS
    $("#microdistrictform-city_id").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "label",
        searchField: ["label"],
        "load": function(query, callback) {
            if (!query.length) {
                return callback();
            }
            $.get('$cityUrl', {query: query}, function(data) { 
                callback(data);
            }).fail(function() {
                callback();
            });
        }
    });

    $("#microdistrictform-district_id").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "label",
        searchField: ["label"],
        "load": function(query, callback) {
            if (!query.length) {
                return callback();
            }
            $.get('$districtUrl', {query: query}, function(data) { 
                callback(data);
            }).fail(function() {
                callback();
            });
        }
    });
    
    $('.import-polygon').on('click', function(){
        let city_id = $("#microdistrictform-city_id").val();
        let district = $("#metaform-ru-ru-title").val();
        $.get('$polygonUrl', 
            {district: district, city_id: city_id}, 
            function(response) { 
                if (response.success) {
                    polygonInput.options.startPoints = response.points;
                    polygonInput.clearPolygon();
                    polygonInput.initPolygon();
                    polygonInput.appendPolygonInputs(response.points);
                }
                else {
                    alert(response.message);
                }
            }
        );
        return false;
    });
JS;

$this->registerJs($script);
