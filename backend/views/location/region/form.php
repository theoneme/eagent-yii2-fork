<?php

use backend\assets\SelectizeAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \common\forms\ar\RegionForm $regionForm
 * @var array $action
 */

SelectizeAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'property-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'property-pjax'
    ],
]); ?>

<?= $form->field($regionForm, 'code')->textInput() ?>
<?= $form->field($regionForm, 'slug')->textInput() ?>
<?= $form->field($regionForm, 'country_id')
    ->dropDownList(!empty($dto) ? [$regionForm['country_id'] => $dto['country']['title']] : [], ['class' => '']) ?>

    <div class="panel panel-default">
        <div class="panel-heading">Translations</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык. Slug - как регион будет показываться в URL
            </div>
            <?php foreach ($regionForm->meta as $locale => $meta) { ?>
                <div class="row">
                    <div class="col-md-1">
                        <span class="label label-default"><?= $locale ?></span>
                    </div>
                    <div class="col-md-11">
                        <?= $form->field($meta, "[{$locale}]title") ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php $url = Url::to(['/ajax/countries']);

$script = <<<JS
    $("#regionform-country_id").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "id",
        labelField: "label",
        searchField: ["label"],
        "load": function(query, callback) {
            if (!query.length) {
                return callback();
            }
            $.get('$url', {query: query}, function(data) { 
                callback(data);
            }).fail(function() {
                callback();
            });
        }
    });
JS;

$this->registerJs($script);
