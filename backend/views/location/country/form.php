<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \common\forms\ar\CountryForm $countryForm
 * @var array $action
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'property-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'property-pjax'
    ],
]); ?>

<?= $form->field($countryForm, 'code')->textInput() ?>
<?= $form->field($countryForm, 'slug')->textInput() ?>

<div class="panel panel-default">
    <div class="panel-heading">Translations</div>
    <div class="panel-body">
        <div class="alert alert-danger">Надо заполнить хотя бы 1 язык. Slug - как страна будет показываться в URL
        </div>
        <?php foreach ($countryForm->meta as $locale => $meta) { ?>
            <div class="row">
                <div class="col-md-1">
                    <span class="label label-default"><?= $locale ?></span>
                </div>
                <div class="col-md-11">
                    <?= $form->field($meta, "[{$locale}]title") ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


<div class="form-group">
    <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
</div>

<?php ActiveForm::end(); ?>
