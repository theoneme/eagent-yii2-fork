<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2018
 * Time: 16:31
 */

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?= $form->field($dynamicForm, $attribute, ['template' => "<div class='row'>
                    <div class='col-md-3'>
                        {label}
                    </div>
                    <div class='col-md-9'>
                        {input}
                        Yes
                        {error}
                    </div>
                </div>"])->checkbox([], false) ?>
