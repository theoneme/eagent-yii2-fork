<?php

use common\forms\ar\BuildingForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var array $step
 * @var BuildingForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 * @var View $this
 */

?>


<div data-role="dynamic-relation-container" data-relation="site">
    <?php foreach ($propertyForm->sites as $key => $site) {
        echo $this->render('site-partial', [
            'model' => $site,
            'form' => $form,
            'key' => $key,
            'createForm' => false
        ]);
    } ?>
</div>
<?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('wizard', 'Add New Site'), '#', [
    'data-action' => 'add-new-relation',
    'data-relation' => 'site'
]) ?>