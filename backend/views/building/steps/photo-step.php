<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

use backend\forms\ar\BuildingForm;
use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;

/**
 * @var array $step
 * @var BuildingForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

    <div class="form-group">
        <p>
            <?= Yii::t('wizard', 'You can upload up to {count} photos', ['count' => 20]) ?>
        </p>
        <div>
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'file-upload-input',
                    'name' => 'uploaded_images[]',
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                        'overwriteInitial' => false,
                        'initialPreview' => FileInputHelper::buildPreviews($propertyForm->attachment),
                        'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($propertyForm->attachment),
                    ]
                ])
            ) ?>
        </div>
        <div class="images-container">
            <?php foreach ($propertyForm->attachment as $key => $attachment) { ?>
                <?= $form->field($attachment, "[]content", ['template' => '{input}'])->hiddenInput([
                    'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                    'data-key' => "image_init_{$key}"
                ])->label(false) ?>
            <?php } ?>
        </div>

    </div>

    <div class="form-group">
        <div class="block-with-notes"
             data-toggle="popover" data-placement="right"
             data-original-title="<?= Yii::t('wizard', 'Video Tour') ?>"
             data-content="<?= Yii::t('wizard', 'Youtube Video Tour') ?>">
            <?= $form->field($propertyForm->video, 'content')->textInput([
                'placeholder' => Yii::t('wizard', 'Youtube link on video tour for this building'),
            ]) ?>
        </div>
    </div>

<?php $attachments = count($propertyForm->attachment);
$script = <<<JS
var hasUploadError = false;

$("#file-upload-input").on("fileuploaded", function(event, data, previewId, index) {
    let response = data.response;
    $(".images-container").append("<input name=\'BuildingAttachmentForm[][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");
}).on("filedeleted", function(event, key) {
    $(".images-container input[data-key=\'" + key + "\']").remove();
}).on("filebatchuploadcomplete", function() {
    if (hasUploadError === false) {
        $(this).closest("form").submit();
    } else {
        hasUploadError = false;
    }
}).on("fileuploaderror", function(event, data, msg) {
    hasUploadError = true;
    $('#' + data.id).find('.kv-file-remove').click();
});
JS;

$this->registerJs($script);

