<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

use backend\forms\ar\BuildingForm;

/**
 * @var array $step
 * @var BuildingForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<?php foreach ($propertyForm->meta as $locale => $meta) { ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($meta, "[{$locale}]name")->textInput([
                'placeholder' => Yii::t('wizard', 'Building name'),
                'rows' => 3
            ])->label($meta->getAttributeLabel('name') . "({$locale})") ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($meta, "[{$locale}]description")->textarea([
                'placeholder' => Yii::t('wizard', 'Building description'),
                'rows' => 3
            ])->label($meta->getAttributeLabel('description') . "({$locale})") ?>
        </div>
    </div>
<?php } ?>
