<?php

use common\forms\ar\composite\BuildingProgressForm;
use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var integer $key
 * @var BuildingProgressForm $model
 * @var ActiveForm $form
 * @var boolean $createForm
 * @var View $this
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
        ],
        'options' => [
            'id' => 'building-form',
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

<div class="row dynamic-relation-item" data-role="relation-item">
    <?= Html::a('<i class="fa fa-close"></i>', '#', ['class' => 'dynamic-relation-remove', 'data-action' => 'remove-relation']) ?>
    <?= Html::activeHiddenInput($model, "[{$key}]id")?>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]year")->textInput([
            'placeholder' => Yii::t('model', 'Year')
        ])->label($model->getAttributeLabel('year')) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]quarter")->dropDownList([1 => 1, 2 => 2, 3 => 3, 4 => 4])->label($model->getAttributeLabel('quarter')) ?>
    </div>
    <div class='col-md-12 file-input-container'>
        <?= FileInput::widget(
            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                'id' => "progress-upload-input-$key",
                'name' => 'uploaded_images[]',
                'options' => [
                    'multiple' => true,
                    'class' => 'file-upload-input progress-upload-input'
                ],
                'pluginOptions' => [
                    'minFileCount' => 1,
                    'overwriteInitial' => false,
                    'initialPreview' => FileInputHelper::buildPreviews($model->attachments),
                    'initialPreviewConfig' => FileInputHelper::buildPreviewsConfig($model->attachments),
                ]
            ])
        ) ?>
        <div class="images-container" data-group="<?= $key?>">
            <?php foreach ($model->attachments as $k => $attachment) { ?>
                <?= $form->field($model, "[{$key}][BuildingProgressAttachmentForm][{$k}]content", ['template' => '{input}'])->hiddenInput([
                    'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                    'data-key' => "image_init_{$k}"
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>
</div>

<?php if ($createForm === true) { ?>
    <?php
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
        var attributes = $attributes;
        $.each(attributes, function() {
            $("#building-form").yiiActiveForm("add", this);
        });
JS;
    $this->registerJs($script);
}

//$count = count($model->attachments);
//$script = <<<JS
//    attachments[$iterator] = $count;
//JS;
//$this->registerJs($script);
