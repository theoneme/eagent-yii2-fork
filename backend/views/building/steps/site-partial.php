<?php

use common\decorators\BuildingSiteTransportDecorator;
use common\decorators\BuildingSiteTypeDecorator;
use common\forms\ar\composite\BuildingSiteForm;
use common\widgets\GmapsActiveInputWidget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var integer $key
 * @var BuildingSiteForm $model
 * @var ActiveForm $form
 * @var boolean $createForm
 * @var View $this
 */
?>

<?php if ($createForm === true) { ?>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
        ],
        'options' => [
            'id' => 'building-form',
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

<div class="row dynamic-relation-item" data-role="relation-item">
    <?= Html::a('<i class="fa fa-close"></i>', '#', ['class' => 'dynamic-relation-remove', 'data-action' => 'remove-relation']) ?>
    <?= Html::activeHiddenInput($model, "[{$key}]id")?>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]type")->dropDownList(BuildingSiteTypeDecorator::getLabels()) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]name")->textInput([
            'placeholder' => Yii::t('wizard', 'Title')
        ])->label($model->getAttributeLabel('name')) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, "[{$key}]transport")->dropDownList(BuildingSiteTransportDecorator::getLabels()) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, "[{$key}]time")->textInput([
            'placeholder' => Yii::t('wizard', 'Time'),
        ])->label($model->getAttributeLabel('time')) ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, "[{$key}]distance")->textInput([
            'placeholder' => Yii::t('wizard', 'Distance'),
        ])->label($model->getAttributeLabel('distance')) ?>
    </div>
    <?= GmapsActiveInputWidget::widget([
        'model' => $model,
        'form' => $form,
        'withMap' => true,
        'addressAttribute' => "[{$key}]address",
        'latAttribute' => "[{$key}]lat",
        'lngAttribute' => "[{$key}]lng",
        'widgetId' => $key,
        'inputOptions' => [
            'label' => Yii::t('wizard', 'Address'),
            'value' => ''
        ],
        'fieldOptions' => [
            'template' => "
                <div class='col-md-4 control-label'>
                    {label}
                </div>
                <div class='col-md-8'>
                    {input}
                    {error}
                </div>
                <div class='col-md-12'>
                    <div id='google-map{$key}' style='height: 240px;'></div>
                </div>
            ",
        ]
    ]) ?>
</div>

<?php if ($createForm === true) { ?>
    <?php
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
        var attributes = $attributes;
        $.each(attributes, function() {
            $("#building-form").yiiActiveForm("add", this);
        });
JS;
    $this->registerJs($script);
} ?>