<?php

use common\forms\ar\BuildingForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var array $step
 * @var BuildingForm $propertyForm
 * @var ActiveForm $form
 * @var View $this
 */

?>


<div data-role="dynamic-relation-container" data-relation="progress">
    <?php foreach ($propertyForm->progress as $key => $progressItem) {
        echo $this->render('progress-partial', [
            'model' => $progressItem,
            'form' => $form,
            'key' => $key,
            'createForm' => false
        ]);
    } ?>
</div>
<?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('wizard', 'Add New Progress'), '#', [
    'data-action' => 'add-new-relation',
    'data-relation' => 'progress'
]) ?>

<?php
$script = <<<JS
    // var attachments = {};
    $(document).off("fileuploaded", ".progress-upload-input").on("fileuploaded", ".progress-upload-input", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        let group = imagesContainer.data('group');
        // let id = attachments[group] ? attachments[group] : 0;
        imagesContainer.append("<input class='image-source' name='BuildingProgressForm[" + group + "][BuildingProgressAttachmentForm][][content]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
        // attachments[group] = id + 1;
    }).off("filedeleted", ".progress-upload-input").on("filedeleted", ".progress-upload-input", function(event, key) {
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        imagesContainer.find("input[data-key='" + key + "']").remove();
    }).off("filebatchuploadcomplete", ".progress-upload-input").on("filebatchuploadcomplete", ".progress-upload-input", function(event, files, extra) {
        $('#building-form').submit();
    });
JS;
$this->registerJs($script);

