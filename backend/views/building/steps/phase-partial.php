<?php

use common\decorators\BuildingPhaseStatusDecorator;
use common\forms\ar\composite\BuildingPhaseForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var integer $key
 * @var BuildingPhaseForm $model
 * @var ActiveForm $form
 * @var boolean $createForm
 * @var View $this
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
        ],
        'options' => [
            'id' => 'building-form',
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

<div class="row dynamic-relation-item" data-role="relation-item">
    <?= Html::a('<i class="fa fa-close"></i>', '#', ['class' => 'dynamic-relation-remove', 'data-action' => 'remove-relation']) ?>
    <?= Html::activeHiddenInput($model, "[{$key}]id")?>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]year")->textInput([
            'placeholder' => Yii::t('model', 'Year')
        ])->label($model->getAttributeLabel('year')) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]quarter")->dropDownList([1 => 1, 2 => 2, 3 => 3, 4 => 4])->label($model->getAttributeLabel('quarter')) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]status")->dropDownList(BuildingPhaseStatusDecorator::getStatusLabels(false)) ?>
    </div>
</div>

<?php if ($createForm === true) { ?>
    <?php
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
        var attributes = $attributes;
        $.each(attributes, function() {
            $("#building-form").yiiActiveForm("add", this);
        });
JS;
    $this->registerJs($script);
} ?>