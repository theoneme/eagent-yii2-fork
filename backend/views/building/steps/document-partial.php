<?php

use common\decorators\BuildingDocumentTypeDecorator;
use common\forms\ar\composite\BuildingDocumentForm;
use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var integer $key
 * @var BuildingDocumentForm $model
 * @var ActiveForm $form
 * @var boolean $createForm
 * @var View $this
 */

?>

<?php if ($createForm === true) { ?>
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
        ],
        'options' => [
            'id' => 'building-form',
        ],
    ]);
    ob_end_clean(); ?>
<?php } ?>

<div class="row dynamic-relation-item" data-role="relation-item">
    <?= Html::a('<i class="fa fa-close"></i>', '#', ['class' => 'dynamic-relation-remove', 'data-action' => 'remove-relation']) ?>
    <?= Html::activeHiddenInput($model, "[{$key}]id")?>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]type")->dropDownList(BuildingDocumentTypeDecorator::getLabels()) ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, "[{$key}]name")->textInput([
            'placeholder' => Yii::t('wizard', 'Title'),
        ])->label($model->getAttributeLabel('name')) ?>
    </div>
    <div class='col-md-12 file-input-container'>
        <?= FileInput::widget(
            ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                'id' => "document-upload-input-$key",
                'name' => 'uploaded_images',
                'options' => [
                    'multiple' => false,
                    'class' => 'file-upload-input document-upload-input',
                ],
                'pluginOptions' => [
                    'overwriteInitial' => true,
                    'initialPreview' => !empty($model->file) ? Yii::$app->mediaLayer->getThumb($model->file) : [],
                    'initialPreviewConfig' => !empty($model->file) ? [[
                        'caption' => basename($model->file),
                        'url' => Url::toRoute('/image/delete'),
                        'key' => 0
                    ]] : [],
                ]
            ])
        ) ?>
        <div class="images-container" data-group="<?= $key?>">
            <?= $form->field($model, "[{$key}]file", ['template' => '{input}'])->hiddenInput([
                'value' => FileInputHelper::buildOriginImagePath($model->file),
                'data-key' => 'image_init_0',
            ])->label(false) ?>
        </div>
    </div>
</div>

<?php if ($createForm === true) { ?>
    <?php
    $attributes = Json::htmlEncode($form->attributes);
    $script = <<<JS
        var attributes = $attributes;
        $.each(attributes, function() {
            $("#building-form").yiiActiveForm("add", this);
        });
JS;
    $this->registerJs($script);
} ?>