<?php

use common\forms\ar\BuildingForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var array $step
 * @var BuildingForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 * @var View $this
 */

?>

<div data-role="dynamic-relation-container" data-relation="document">
    <?php foreach ($propertyForm->documents as $key => $document) {
        echo $this->render('document-partial', [
            'model' => $document,
            'form' => $form,
            'key' => $key,
            'createForm' => false
        ]);
    } ?>
</div>
<?= Html::a('<i class="fa fa-plus"></i>&nbsp;' . Yii::t('wizard', 'Add New Document'), '#', [
    'data-action' => 'add-new-relation',
    'data-relation' => 'document'
]) ?>

<?php
$script = <<<JS
    var attachments = {};
    $(document).off("fileuploaded", ".document-upload-input").on("fileuploaded", ".document-upload-input", function(event, data, previewId, index) {
        let response = data.response;
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        imagesContainer.find('input').val(response.uploadedPath).data('key', response.imageKey);
    }).off("filedeleted", ".document-upload-input").on("filedeleted", ".document-upload-input", function(event, key) {
        let imagesContainer = $(this).closest('.file-input-container').children('.images-container');
        imagesContainer.find('input').val(null);
    }).off("filebatchuploadcomplete", ".document-upload-input").on("filebatchuploadcomplete", ".document-upload-input", function(event, files, extra) {
        $('#building-form').submit();
    });
JS;
$this->registerJs($script);

