<?php

use common\decorators\PropertyStatusDecorator;
use common\models\Building;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \common\forms\ar\BuildingForm $buildingForm
 * @var array $categories
 * @var array $action
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'building-form',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'building-pjax'
    ],
]); ?>

<?php if ($buildingForm->_building->isNewRecord === false) { ?>
    <p>
        <span class="label label-default">
            Ссылка на профиль: <?= Html::a('Открыть', Yii::$app->frontUrlManager->createUrl([
                '/property/building/show', 'slug' => $buildingForm->_building->slug
            ]), [
                'target' => '_blank'
            ]) ?>
        </span>
    </p>
<?php } ?>

    <div>
        <?= $form->field($buildingForm, 'type', ['template' => '{input}'])->radioList([
            Building::TYPE_DEFAULT => Yii::t('main', 'Old Construction'),
            Building::TYPE_NEW_CONSTRUCTION => Yii::t('main', 'New Construction')
        ], [
            'item' => function ($index, $label, $name, $checked, $value) {
                $chk = $checked ? 'checked' : '';
                $active = $checked ? 'active' : '';
                $output = "<div class='chover'>
                                <input name='{$name}' id='buildingform-type-{$index}' class='radio-checkbox hidden' name='type' value='{$value}' {$chk} type='radio'>
                                <label class='btn btn-default {$active}' for='buildingform-type-{$index}'>{$label}</label>
                            </div>";
                return $output;
            },
            'class' => 'flex'
        ]) ?>
    </div>

    <div>
        <?= $form->field($buildingForm, 'status')->dropDownList(PropertyStatusDecorator::getStatusLabels(false), [
            'prompt' => '-- ' . Yii::t('main', 'Select Status')
        ]) ?>
    </div>
    <div>
        <?= $form->field($buildingForm, 'locale')->dropDownList(Yii::$app->params['languages'])->label(Yii::t('main', 'Language')) ?>
    </div>
<?php foreach ($buildingForm->steps as $key => $step) { ?>
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title"><?= $step['title'] ?></h3>
            <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($buildingForm->steps)]) ?></span>
        </div>
        <div class="box-body">
            <?php foreach ($step['config'] as $stepConfig) { ?>
                <?php if ($stepConfig['type'] === 'view') { ?>
                    <?= $this->render("steps/{$stepConfig['value']}", [
                        'form' => $form,
                        'propertyForm' => $buildingForm,
                        'step' => $step,
                        'createForm' => false
                    ]) ?>
                <?php } ?>

                <?php if ($stepConfig['type'] === 'constructor') { ?>
                    <?php foreach ($stepConfig['groups'] as $group) { ?>
                        <?php $attributes = $buildingForm->dynamicForm->getAttributesByGroup($group) ?>
                        <?= $this->render("partial/attributes-group", [
                            'form' => $form,
                            'attributes' => array_flip($attributes),
                            'dynamicForm' => $buildingForm->dynamicForm
                        ]) ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$canonical = Url::current(['id' => null, 'type' => null]);
$id = $buildingForm->_building['id'] ?? 'null';

$phaseCount = count($buildingForm->phases);
$progressCount = count($buildingForm->progress);
$siteCount = count($buildingForm->sites);
$documentCount = count($buildingForm->documents);
$phaseUrl = Url::to(['/ajax/render-building-phase']);
$progressUrl = Url::to(['/ajax/render-building-progress']);
$siteUrl = Url::to(['/ajax/render-building-site']);
$documentUrl = Url::to(['/ajax/render-building-document']);
$script = <<<JS
    var relations = {
        'phase' : {
            'count' : $phaseCount,
            'url' : '$phaseUrl'
        },
        'progress' : {
            'count' : $progressCount,
            'url' : '$progressUrl'
        },
        'site' : {
            'count' : $siteCount,
            'url' : '$siteUrl'
        },
        'document' : {
            'count' : $documentCount,
            'url' : '$documentUrl'
        },
    };

    $('[data-action=add-new-relation]').on('click', function() {
        let relation = $(this).data('relation');
        if (relations[relation]) {  
            $.get(relations[relation]['url'], {iterator: relations[relation]['count']}, function(result) {
                if(result.success === true) {
                    $('[data-role=dynamic-relation-container][data-relation=' + relation + ']').append(result.html);
                } 
                relations[relation]['count']++;
            });
        }
        
        return false;
    });

    $(document).off('click', '[data-action=remove-relation]').on('click', '[data-action=remove-relation]', function() {
        $(this).closest('[data-role=relation-item]').remove(); 
        return false;
    });
    
    $('#building-form').on('submit', function() {
        let hasToUpload = false;
        $.each($(".file-upload-input"), function() {
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                hasToUpload = true;
                return false;
            }
        });
        
        return !hasToUpload;
    });

    $("#buildingform-type").on("change", function(e) { 
        let type = $('#buildingform-type').find('input:checked').val();
        let params =  {
            type: type,
            id: {$id}
        };
        let query = $.param(params);
        
        $('#edit-modal').find('.modal-body').load('{$canonical}' + '?' + query);
        
        return false;
    });
JS;

$this->registerJs($script);

