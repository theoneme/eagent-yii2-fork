<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.10.2018
 * Time: 16:34
 */

use common\models\Building;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use common\decorators\PropertyStatusDecorator;

/**
 * @var \common\models\search\BuildingSearch $search
 * @var integer $filterStatus
 */

\common\assets\GoogleAsset::register($this);

?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Buildings list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'building-pjax', 'timeout' => 6000]) ?>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <?= Html::a('Create building', ['/building/create'], [
                        'data-action' => 'load-edit-modal',
                        'title' => 'Create building',
                        'class' => 'btn btn-success',
                        'data-pjax' => 0
                    ]) ?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h4 class="box-title">Поиск по адресу (в радиусе 10км от точки)</h4>
                    </div>
                    <div class="box-body">
                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]); ?>
                        <div class="row">
                            <div class="col-md-8">
                                <?php if(is_array($search['status'])) { ?>
                                    <?= Html::activeCheckboxList($search, 'status', PropertyStatusDecorator::getStatusLabels(false), ['class' => 'hidden']) ?>
                                <?php } else { ?>
                                    <?= Html::activeHiddenInput($search, 'status') ?>
                                <?php } ?>
                                <?= Html::activeHiddenInput($search, 'id') ?>
                                <?= Html::hiddenInput('BuildingSearch[operation]', $search['type'] === Building::TYPE_DEFAULT ? Building::TYPE_DEFAULT_TEXT : Building::TYPE_NEW_CONSTRUCTION_TEXT) ?>
                                <?= \common\widgets\GmapsActiveInputWidget::widget([
                                    'inputOptions' => [
                                        'class' => 'form-control',
                                        'required' => 'required',
                                    ],
                                    'fieldOptions' => [
                                        'template' => '{input}{error}',
                                    ],
                                    'form' => $form,
                                    'model' => $search,
                                    'widgetId' => 'asdf'
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= Html::submitInput('Отфильтровать', ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>

                        <?php ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{summary}\n{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Имя',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'],
                            ['/building/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['building']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'value' => function ($model) {
                        return \common\decorators\PropertyStatusDecorator::decorate($model['status']);
                    },

                    'format' => 'html',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'filter' => false,
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{generate} {update} {delete}',
                    'buttons' => [
                        'generate' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-dashboard"></span>', $url, [
                                'title' => 'Regenerate Seo',
                                'data-pjax' => 0,
                                'data-action' => 'generate-seo'
                            ]);
                        },
                        'update' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Building',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model) {
                        switch ($action) {
                            case 'generate':
                                $url = Url::toRoute(["/building/{$action}", 'id' => $model['id']]);
                                break;
                            case 'update':
                                $url = Url::toRoute(["/building/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/building/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>

<?php $script = <<<JS
    $('#gmaps-input-address').on('change', function() {
        if($(this).val() === '') {
            $('#gmaps-input-lat').val('');
            $('#gmaps-input-lng').lng('');
        } 
    });
JS;

$this->registerJs($script);