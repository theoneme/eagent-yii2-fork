<?php

/**
 * @var $this yii\web\View
 * @var array $counts
 */

$this->title = Yii::$app->name;
?>

<section class="content-header">
    <h1>
        <?= $this->title ?>
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Всего пользователей</span>
                    <span class="info-box-number"><?= $counts['totalUsers'] ?></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Всего объектов</span>
                    <span class="info-box-number"><?= $counts['totalProperties'] ?></span>
                </div>
            </div>
        </div>

        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Всего заявок</span>
                    <span class="info-box-number"><?= $counts['totalRequests'] ?></span>
                </div>
            </div>
        </div>

        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Всего зданий</span>
                    <span class="info-box-number"><?= $counts['totalBuildings'] ?></span>
                </div>
            </div>
        </div>
    </div>
</section>
