<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.02.2019
 * Time: 15:34
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var ActiveDataProvider $provider */
/* @var integer $month */
/* @var integer $year */

$this->title = Yii::t('main', 'Stats');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="setting-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>

        <div class="box-body">
            <div>
                <?php \yii\widgets\Pjax::begin(['id' => 'stat-pjax']); ?>
                <?= Html::beginForm(['/stat/index'], 'get', ['id' => 'stat-form', 'data-pjax' => '']) ?>
                <div>
                    <div style="float:right" class="col-md-3">
                        <label>Год</label>
                        <?= Html::dropDownList('year', Yii::$app->request->get('year', $year), [
                            array_combine(range(date('Y'), 2016), range(date('Y'), 2016))
                        ], [
                            'class' => 'form-control stat-trigger'
                        ]) ?>
                    </div>
                    <div style="float:right" class="col-md-3">
                        <label>Месяц</label>
                        <?= Html::dropDownList('month', Yii::$app->request->get('month', 3), [
                            array_combine(range(1, 12), range(1, 12))
                        ], [
                            'class' => 'form-control stat-trigger'
                        ]) ?>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $provider,
                        'id' => 'users',
                        'columns' => [
                            [
                                'header' => 'День',
                                'value' => function ($model) use ($year, $month) {
                                    return Yii::$app->formatter->asDate("{$year}-{$month}-{$model['id']}", 'long');
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Юзеры',
                                'value' => function ($model) {
                                    return $model['currentDayUsers'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Зарегались сами',
                                'value' => function ($model) {
                                    return $model['currentDayHandUsers'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Объекты',
                                'value' => function ($model) {
                                    return $model['currentDayProperties'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Разместили руками',
                                'value' => function ($model) {
                                    return $model['currentDayHandProperties'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Объекты продажа',
                                'value' => function ($model) {
                                    return $model['currentDaySaleProperties'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Объекты аренда',
                                'value' => function ($model) {
                                    return $model['currentDayRentProperties'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Заявки продажа',
                                'value' => function ($model) {
                                    return $model['currentDaySaleRequests'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                            [
                                'header' => 'Заявки аренда',
                                'value' => function ($model) {
                                    return $model['currentDayRentRequests'];
                                },
                                'headerOptions' => ['style' => 'font-size: 12px;'],
                                'contentOptions' => ['style' => 'font-size: 15px;']
                            ],
                        ],
                    ]); ?>
                    <?= Html::endForm() ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>

<?php $this->registerJs('
    $(document).on("change", ".stat-trigger", function() {
        $("#stat-form").submit();
    });
'); ?>