<?php

use common\decorators\PropertyStatusDecorator;
use common\models\Property;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \backend\forms\ar\PropertyForm $propertyForm
 * @var \backend\forms\ar\BuildingForm $buildingForm
 * @var array $categories
 * @var array $subcategories
 * @var array $propertyData
 * @var array $action
 * @var integer $parentCategoryId
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'property-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'property-pjax'
    ],
]); ?>

<?php if ($propertyForm->_property->isNewRecord === false) { ?>
    <p>
        <span class="label label-default">
            Ссылка на сайте: <?= Html::a('Открыть', Yii::$app->frontUrlManager->createUrl([
                '/property/property/show', 'slug' => $propertyData['slug']
            ]), [
                'target' => '_blank'
            ]) ?>
        </span>
    </p>
<?php } ?>

    <div>
        <div>
            <?= $form->field($propertyForm, 'type', ['template' => '{input}'])->radioList([
                Property::TYPE_SALE => Yii::t('main', 'For Sale'),
                Property::TYPE_RENT => Yii::t('main', 'For Rent')
            ], [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $active = $checked ? 'active' : '';
                    $output = "<div class='chover'>
                                <input name='{$name}' id='propertyform-type-{$index}' class='radio-checkbox hidden type-input' value='{$value}' {$chk} type='radio'>
                                <label class='btn btn-default {$active}' for='propertyform-type-{$index}'>{$label}</label>
                            </div>";
                    return $output;
                },
                'class' => 'flex'
            ]) ?>
        </div>
        <div>
            <div class="form-group">
                <div class="flex">
                    <?php foreach ($categories as $id => $category) {
                        $chk = $id === $parentCategoryId ? 'checked' : '';
                        $active = $id === $parentCategoryId ? 'active' : '';
                        echo "<div class='chover'>
                                <input name='category_id' id='parent-category-id-{$id}' class='radio-checkbox hidden category-input' value='{$id}' {$chk} type='radio'>
                                <label class='btn btn-default {$active}' for='parent-category-id-{$id}'>{$category}</label>
                            </div>";
                    }?>
                </div>
            </div>
        </div>
        <div>
            <?= $form->field($propertyForm->category, 'category_id', ['template' => '{input}{error}'])->radioList($subcategories, [
                'item' => function ($index, $label, $name, $checked, $value) {
                    $chk = $checked ? 'checked' : '';
                    $active = $checked ? 'active' : '';
                    $output = "<div class='chover'>
                                <input name='{$name}' id='propertyform-category-id-{$index}' class='radio-checkbox hidden subcategory-input' value='{$value}' {$chk} type='radio'>
                                <label class='btn btn-default {$active}' for='propertyform-category-id-{$index}'>{$label}</label>
                            </div>";
                    return $output;
                },
                'class' => 'flex',
                'style' => 'flex-wrap: wrap;'
            ]) ?>
        </div>
        <?= $form->field($propertyForm, 'locale')->dropDownList(Yii::$app->params['languages'])->label(Yii::t('main', 'Language')) ?>
        <?= $form->field($propertyForm, 'user_id')->textInput(['type' => 'number']) ?>
        <?= $form->field($propertyForm, 'status')->dropDownList(PropertyStatusDecorator::getStatusLabels(false), [
            'prompt' => '-- ' . Yii::t('main', 'Select Status')
        ]) ?>
    </div>

<?php foreach ($propertyForm->steps as $key => $step) { ?>
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title"><?= $step['title'] ?></h3>
            <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($propertyForm->steps)]) ?></span>
        </div>
        <div class="box-body">
            <?php foreach ($step['config'] as $stepConfig) { ?>
                <?php if ($stepConfig['type'] === 'view') { ?>
                    <?= $this->render("steps/{$stepConfig['value']}", [
                        'form' => $form,
                        'propertyForm' => $propertyForm,
                        'step' => $step,
                        'createForm' => false,
                        'buildingForm' => $buildingForm
                    ]) ?>
                <?php } ?>

                <?php if ($stepConfig['type'] === 'constructor') { ?>
                    <?php foreach ($stepConfig['groups'] as $group) { ?>
                        <?php $attributes = $propertyForm->dynamicForm->getAttributesByGroup($group) ?>
                        <?= $this->render("partial/attributes-group", [
                            'form' => $form,
                            'attributes' => array_flip($attributes),
                            'dynamicForm' => $propertyForm->dynamicForm
                        ]) ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$renderAttributesRoute = Url::toRoute('/property/attributes');
$canonical = Url::current(['id' => null, 'type' => null, 'category_id' => null]);
$id = $propertyForm->_property['id'] ?? 'null';
//$attachments = count($propertyForm->attachment);
$script = <<<JS
    $('#property-form').on('submit', function() {
        let hasToUpload = false;
        $.each($(".file-upload-input"), function() {
            if ($(this).fileinput("getFilesCount") > 0) {
                $(this).fileinput("upload");
                hasToUpload = true;
                return false;
            }
        });
        
        return !hasToUpload;
    });
    
    function loadAttributes(categoryId, type, entityId) {
        $.get('$renderAttributesRoute', {
            category_id: categoryId,
            type: type,
            entity_id: entityId
        }, function(data) {
            $("#attributes-info").html(data);
        }, "html");
    }
    
    function reloadModal(categoryId, type) {
        let params =  {
            category_id: categoryId,
            type: type,
            id: {$id}
        };
        let query = $.param(params);
        $('#edit-modal').find('.modal-body').load('{$canonical}' + '?' + query);
    }
    
    $(".category-input, .subcategory-input").on("change", function(e) {
        let type = $('.type-input:checked').val();
        let categoryId = $(this).val();
        reloadModal(categoryId, type);
        return false;
    });
    $(".type-input").on("change", function(e) {
        let type = $(this).val();
        let categoryId = $('.subcategory-input:checked').val();
        if (!categoryId) {
            categoryId = $('.category-input:checked').val();
        }
        reloadModal(categoryId, type);
        return false;
    });
JS;

$this->registerJs($script);

