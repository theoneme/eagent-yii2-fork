<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */
?>


<?= $form->field($propertyForm->price, 'price')->widget(\yii\widgets\MaskedInput::class, [
    'clientOptions' => [
        'alias' => 'decimal',
        'groupSeparator' => ' ',
        'autoGroup' => true,
        'removeMaskOnSubmit' => true,
        'rightAlign' => false
    ],
]) ?>
<?= $form->field($propertyForm->price, 'currency_code')->dropDownList(
    \common\helpers\DataHelper::getCurrencies()
) ?>

