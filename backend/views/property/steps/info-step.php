<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \frontend\forms\ar\PropertyForm $propertyForm
 * @var \yii\widgets\ActiveForm $form
 */

?>


<?php foreach ($propertyForm->meta as $locale => $meta) { ?>
    <?= $form->field($meta, "[{$locale}]description")->textarea([
        'placeholder' => Yii::t('wizard', 'Property description'),
        'rows' => 3
    ])->label($meta->getAttributeLabel('description') . "({$locale})") ?>
<?php } ?>
