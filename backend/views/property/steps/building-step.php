<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var array $step
 * @var \backend\forms\ar\PropertyForm $propertyForm
 * @var \backend\forms\ar\BuildingForm $buildingForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<?php foreach ($buildingForm->meta as $locale => $meta) { ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($meta, "[{$locale}]description")->textarea([
                'placeholder' => Yii::t('wizard', 'Building description'),
                'rows' => 3,
                'data-role' => 'building-description'
            ])->label($meta->getAttributeLabel('description') . "({$locale})") ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($meta, "[{$locale}]name")->textarea([
                'placeholder' => Yii::t('wizard', 'Building name'),
                'rows' => 3,
                'data-role' => 'building-name'
            ])->label($meta->getAttributeLabel('name') . "({$locale})") ?>
        </div>
    </div>
<?php } ?>

    <div class="form-group" data-role="building-image-block">
        <p>
            <?= Yii::t('wizard', 'You can upload up to {count} photos', ['count' => 20]) ?>
        </p>

        <div class="form-group">
            <?= FileInput::widget(
                ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
                    'id' => 'building-file-upload-input',
                    'name' => 'uploaded_images[]',
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                        'overwriteInitial' => false,
                        'initialPreview' => [],
                        'initialPreviewConfig' => [],
                    ]
                ])
            ) ?>
        </div>

        <p class='<?= empty($buildingForm->attachment) ? 'hidden' : '' ?>'
           data-role="building-prev-uploaded-title">
            <?= Yii::t('wizard', 'Previously uploaded building photos') ?>
        </p>

        <div class="flex" data-role="wizard-photo-small-container">
            <?php foreach ($buildingForm->attachment as $key => $attachment) { ?>
                <div class="wizard-photo-small-preview">
                    <div class="wizard-building-photo-remove">
                        <?= Html::a('<i class="fa fa-close"></i>', '#', ['data-action' => 'remove-preview', 'data-key' => $key]) ?>
                    </div>
                    <?= Html::img($attachment['content'], []) ?>
                </div>
            <?php } ?>
        </div>

        <div data-role="building-attachment-container" class="building-attachment-container">
            <?php foreach ($buildingForm->attachment as $key => $attachment) { ?>
                <?= $form->field($attachment, "[]content", ['template' => '{input}'])->hiddenInput([
                    'value' => FileInputHelper::buildOriginImagePath($attachment['content']),
                    'data-key' => $key
                ])->label(false) ?>
            <?php } ?>
        </div>
    </div>

<?php $buildingParamsRoute = Url::to(['/ajax/building']);
$script = <<<JS
var hasBuildingUploadError = false;

$("#building-file-upload-input").on("fileuploaded", function(event, data) {
    let response = data.response;
    $(".building-attachment-container").append("<input name=\'BuildingAttachmentForm[][content]\' data-key=\'" + response.imageKey + "\' type=\'hidden\' value=\'" + response.uploadedPath + "\'>");

}).on("filedeleted", function(event, key) {
    $(".building-attachment-container input[data-key=\'" + key + "\']").remove();
}).on("filebatchuploadcomplete", function() {
    if (hasBuildingUploadError === false) {
        $(this).closest("form").submit();
    } else {
        hasBuildingUploadError = false;
    }
}).on("fileuploaderror", function(event, data, msg) {
    hasBuildingUploadError = true;
    $('#' + data.id).find('.kv-file-remove').click();
});

$('[data-action=remove-preview').on('click', function() {
    let key = $(this).data('key');
    $(this).closest('.wizard-photo-small-preview').remove();
    $('[data-role=building-attachment-container]').find('[data-key=' + key + ']').remove();
    
    return false;
});

$("#gmaps-input-address").on("change", function() { 
    let lat = $('#gmaps-input-lat').val(),
        lng = $('#gmaps-input-lng').val();

    if(lat && lng) {
        $.get('{$buildingParamsRoute}', {lat: lat, lng: lng}, function(result) {
            if(result.success === true) {
                $('[data-role=building-attachment-container]').html('');
                $('[data-role=wizard-photo-small-container]').html('');
                
                let data = result.data;
                $.each(data.attributes, function(key, value) {
                    let input = $('#dynamicform-' + key);
                    if(input.hasClass('selectize-multi')) {
                        let value1 = value.split(',');
                        $.each(value1, function(k, v) {
                            input[0].selectize.addOption({value: v});
                        });
                        input[0].selectize.setValue(value1, false);
                    } else {
                        input.val(value);
                    }
                });
                $.each(data.images, function(key, value) {
                   $('[data-role=building-attachment-container]').append('<input data-key = "' + key + '" type=hidden name="BuildingAttachmentForm[][content]" value="' + value + '">');
                   $('[data-role=wizard-photo-small-container]').append('<div class="wizard-photo-small-preview"><div class="wizard-building-photo-remove"><a href="#" data-action="remove-preview" data-key="' + key + '" <i class="fa fa-close"></i></a></div><img src="' + value + '"></div>');
                });
                
                if(data.images.length > 0) {
                    $('[data-role=building-prev-uploaded-name]').removeClass('hidden');
                } else {
                    $('[data-role=building-prev-uploaded-name]').addClass('hidden');
                }
                
                $('[data-role=building-description]').val(data.description);
                $('[data-role=building-name]').val(data.name);
            }
        });
    }
});
JS;

$this->registerJs($script);
