<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2018
 * Time: 16:31
 */

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>

<?= $form->field($dynamicForm, $attribute)->checkboxList($dynamicForm['config']['values'][$attribute]) ?>
