<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 31.08.2017
 * Time: 12:57
 */

use common\forms\ar\DynamicForm;
use backend\assets\SelectizeAsset;
use yii\helpers\Url;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

SelectizeAsset::register($this);

?>

<?= $form->field($dynamicForm, $attribute)->textInput(['class' => 'selectize-multi']) ?>

<?php $attributeNameParts = explode('_', $attribute);
$url = Url::to(['/ajax/attribute-values']);
$addTagMessage = Yii::t('wizard', 'Add {attribute}', ['attribute' => $dynamicForm->getAttributeLabel($attribute)]);

$script = <<<JS
    $("#dynamicform-{$attribute}").selectize({
        inputClass: 'form-control selectize-input', 
        valueField: "value",
        labelField: "value",
        searchField: ["value"],
        plugins: ["remove_button"],
        persist: false,
        create: true,
        maxOptions: 10,
        render: {
            option_create: function(data, escape) {
                return "<div class=\"create\">$addTagMessage&nbsp;<strong>" + escape(data.input) + "</strong>&hellip;</div>";
            }
        },
        "load": function(query, callback) {
            if (!query.length)
                return callback();
            $.get("$url", {
                    attribute: {$attributeNameParts[1]},              
                    query: encodeURIComponent(query)
                },
                function(data) {
                    callback(data);
                }).fail(function() {
                callback();
            });
        }
    })
JS;

$this->registerJs($script);