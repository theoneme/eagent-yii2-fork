<?php

use backend\helpers\LeftMenuHelper;
use common\controllers\BackEndController;
use common\helpers\Auth;
use common\models\Building;
use common\models\Property;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var BackEndController $controller */
$controller = Yii::$app->controller;

$request = Yii::$app->request;

?>

<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php echo Html::img(Auth::user()->avatar, ['class' => 'img-circle']) ?>
            </div>
            <div class="pull-left info">
                <?php if (!Yii::$app->user->isGuest) { ?>
                    <p><?= Auth::user()->username ?></p>
                    <a href="#"><i class="circle text-success"></i> Online</a>
                <?php } ?>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
                'items' => [
                    [
                        'label' => Yii::t('main', 'Menu Yii2'),
                        'options' => ['class' => 'header']
                    ],
                    [
                        'label' => Yii::t('main', 'Dashboard'),
                        'icon' => 'dashboard',
                        'url' => Url::toRoute('/site/index'),
                        'active' => $controller->id === 'site',
                        'visible' => $controller->isAllowed('site', 'index'),
                    ],
                    [
                        'label' => Yii::t('main', 'Stats'),
                        'icon' => 'dashboard',
                        'url' => Url::toRoute('/stat/index'),
                        'active' => $controller->id === 'stat',
                        'visible' => $controller->isAllowed('stat', 'index'),
                    ],
                    [
                        'label' => Yii::t('main', 'System'),
                        'url' => '#',
                        'icon' => 'list-alt',
                        'active' => in_array($controller->id, ['gii', 'debug', 'setting', 'test']),
                        'items' => [
                            [
                                'label' => 'Gii',
                                'icon' => 'file-code-o', 'url' => ['/gii'],
                                'visible' => $controller->isAllowed('gii', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Debug'),
                                'icon' => 'dashboard', 'url' => ['/debug'],
                                'visible' => $controller->isAllowed('debug', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Clear cache'),
                                'icon' => 'folder',
                                'url' => Url::toRoute('/site/clear-cache'),
                                'visible' => $controller->isAllowed('site', 'clear-cache')
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('main', 'Attributes'),
                        'url' => '#',
                        'icon' => 'list-alt',
                        'active' => in_array($controller->id, ['attribute/attribute', 'attribute/attribute-value']),
                        'items' => [
                            [
                                'label' => Yii::t('main', 'Attributes'),
                                'icon' => 'file-code-o',
                                'url' => ['/attribute/attribute/index'],
                                'visible' => $controller->isAllowed('attribute', 'index'),
                                'active' => $controller->id === 'attribute/attribute' && $controller->action->id === 'index',
                            ],
                            [
                                'label' => Yii::t('main', 'Moderation'),
                                'icon' => 'file-code-o',
                                'url' => ['/attribute/attribute/moderation'],
                                'visible' => $controller->isAllowed('attribute', 'moderation'),
                                'active' => $controller->id === 'attribute/attribute' && $controller->action->id === 'moderation',
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('main', 'Locations'),
                        'url' => '#',
                        'icon' => 'list-alt',
                        'active' => in_array($controller->id, ['location/country', 'location/region', 'location/city', 'location/district', 'location/micro-district']),
                        'items' => [
                            [
                                'label' => Yii::t('main', 'Countries'),
                                'icon' => 'file-code-o', 'url' => ['/location/country/index'],
                                'visible' => $controller->isAllowed('country', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Regions'),
                                'icon' => 'file-code-o', 'url' => ['/location/region/index'],
                                'visible' => $controller->isAllowed('region', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Cities'),
                                'icon' => 'file-code-o', 'url' => ['/location/city/index'],
                                'visible' => $controller->isAllowed('city', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Districts'),
                                'icon' => 'file-code-o', 'url' => ['/location/district/index'],
                                'visible' => $controller->isAllowed('district', 'index'),
                            ],
                            [
                                'label' => Yii::t('main', 'Micro Districts'),
                                'icon' => 'file-code-o', 'url' => ['/location/micro-district/index'],
                                'visible' => $controller->isAllowed('micro-district', 'index'),
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('main', 'Categories'),
                        'icon' => 'list-alt',
                        'url' => Url::toRoute('/category/index'),
                        'active' => $controller->id === 'category' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('category', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Users'),
                        'icon' => 'user',
                        'url' => Url::toRoute('/user/index'),
                        'active' => $controller->id === 'user' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('admin', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Companies'),
                        'icon' => 'user',
                        'url' => Url::toRoute('/company/company/index'),
                        'active' => in_array($controller->id, ['company/company', 'company/company-member']) && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('company', 'index')
                    ],
//                    [
//                        'label' => Yii::t('main', 'Properties'),
//                        'icon' => 'user',
//                        'url' => Url::toRoute('/property/index'),
//                        'active' => $controller->id === 'property' && $controller->action->id === 'index',
//                        'visible' => $controller->isAllowed('admin', 'index')
//                    ],
                    [
                        'label' => Yii::t('main', 'Properties'),
                        'url' => '#',
                        'icon' => 'list-alt',
                        'active' => $controller->id === 'property',
                        'items' => [
                            [
                                'label' => Yii::t('main', 'Sale'),
                                'icon' => 'list-alt',
                                'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, null, Property::OPERATION_SALE),
                                'visible' => $controller->isAllowed('property', 'index'),
                                'items' => [
                                    [
                                        'label' => Yii::t('main', 'Pending'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/property/index', 'PropertySearch' => [
                                            'status' => [
                                                Property::STATUS_REQUIRES_MODERATION,
                                                Property::STATUS_REQUIRES_MODIFICATION
                                            ],
                                            'operation' => Property::OPERATION_SALE,
                                            'category_id' => 2
                                        ]]),
                                        'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, [
                                                Property::STATUS_REQUIRES_MODERATION,
                                                Property::STATUS_REQUIRES_MODIFICATION
                                            ], Property::OPERATION_SALE),
                                        'visible' => $controller->isAllowed('property', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Active'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/property/index', 'PropertySearch' => [
                                            'status' => Property::STATUS_ACTIVE,
                                            'operation' => Property::OPERATION_SALE,
                                            'category_id' => 2
                                        ]]),
                                        'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, Property::STATUS_ACTIVE, Property::OPERATION_SALE),
                                        'visible' => $controller->isAllowed('property', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Disabled'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/property/index', 'PropertySearch' => [
                                            'status' => [
                                                Property::STATUS_DELETED,
                                                Property::STATUS_PAUSED
                                            ],
                                            'operation' => Property::OPERATION_SALE,
                                            'category_id' => 2
                                        ]]),
                                        'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, [
                                                Property::STATUS_DELETED,
                                                Property::STATUS_PAUSED
                                            ], Property::OPERATION_SALE),
                                        'visible' => $controller->isAllowed('property', 'index')
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('main', 'Rent'),
                                'icon' => 'user',
                                'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, null, Property::OPERATION_RENT),
                                'visible' => $controller->isAllowed('property', 'index'),
                                'items' => [
                                    [
                                        'label' => Yii::t('main', 'Pending'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/property/index', 'PropertySearch' => [
                                            'status' => [
                                                Property::STATUS_REQUIRES_MODERATION,
                                                Property::STATUS_REQUIRES_MODIFICATION
                                            ],
                                            'operation' => Property::OPERATION_RENT,
                                            'category_id' => 2
                                        ]]),
                                        'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, [
                                                Property::STATUS_REQUIRES_MODERATION,
                                                Property::STATUS_REQUIRES_MODIFICATION
                                            ], Property::OPERATION_RENT),
                                        'visible' => $controller->isAllowed('property', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Active'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/property/index', 'PropertySearch' => [
                                            'status' => Property::STATUS_ACTIVE,
                                            'operation' => Property::OPERATION_RENT,
                                            'category_id' => 2
                                        ]]),
                                        'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, Property::STATUS_ACTIVE, Property::OPERATION_RENT),
                                        'visible' => $controller->isAllowed('property', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Disabled'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/property/index', 'PropertySearch' => [
                                            'status' => [
                                                Property::STATUS_DELETED,
                                                Property::STATUS_PAUSED
                                            ],
                                            'operation' => Property::OPERATION_RENT,
                                            'category_id' => 2
                                        ]]),
                                        'active' => $controller->id === 'property' && $controller->action->id === 'index' && LeftMenuHelper::isPropertyMenuActive($request, [
                                                Property::STATUS_DELETED,
                                                Property::STATUS_PAUSED
                                            ], Property::OPERATION_RENT),
                                        'visible' => $controller->isAllowed('property', 'index')
                                    ],
                                ]
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('main', 'Buildings'),
                        'url' => '#',
                        'icon' => 'list-alt',
                        'active' => $controller->id === 'building',
                        'items' => [
                            [
                                'label' => Yii::t('main', 'Secondary'),
                                'icon' => 'list-alt',
                                'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, null, Building::TYPE_DEFAULT_TEXT),
                                'visible' => $controller->isAllowed('building', 'index'),
                                'items' => [
                                    [
                                        'label' => Yii::t('main', 'Pending'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/building/index', 'BuildingSearch' => [
                                            'status' => [
                                                Building::STATUS_REQUIRES_MODERATION,
                                                Building::STATUS_REQUIRES_MODIFICATION
                                            ],
                                            'operation' => Building::TYPE_DEFAULT_TEXT
                                        ]]),
                                        'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, [
                                                Building::STATUS_REQUIRES_MODERATION,
                                                Building::STATUS_REQUIRES_MODIFICATION
                                            ], Building::TYPE_DEFAULT_TEXT),
                                        'visible' => $controller->isAllowed('building', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Active'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/building/index', 'BuildingSearch' => [
                                            'status' => Building::STATUS_ACTIVE,
                                            'operation' => Building::TYPE_DEFAULT_TEXT
                                        ]]),
                                        'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, Building::STATUS_ACTIVE, Building::TYPE_DEFAULT_TEXT),
                                        'visible' => $controller->isAllowed('building', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Disabled'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/building/index', 'BuildingSearch' => [
                                            'status' => [
                                                Building::STATUS_DELETED,
                                                Building::STATUS_PAUSED
                                            ],
                                            'operation' => Building::TYPE_DEFAULT_TEXT
                                        ]]),
                                        'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, [
                                                Building::STATUS_DELETED,
                                                Building::STATUS_PAUSED
                                            ], Building::TYPE_DEFAULT_TEXT),
                                        'visible' => $controller->isAllowed('building', 'index')
                                    ],
                                ]
                            ],
                            [
                                'label' => Yii::t('main', 'New Constructions'),
                                'icon' => 'user',
                                'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, null, Building::TYPE_NEW_CONSTRUCTION_TEXT),
                                'visible' => $controller->isAllowed('building', 'index'),
                                'items' => [
                                    [
                                        'label' => Yii::t('main', 'Pending'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/building/index', 'BuildingSearch' => [
                                            'status' => [
                                                Building::STATUS_REQUIRES_MODERATION,
                                                Building::STATUS_REQUIRES_MODIFICATION
                                            ],
                                            'operation' => Building::TYPE_NEW_CONSTRUCTION_TEXT
                                        ]]),
                                        'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, [
                                                Building::STATUS_REQUIRES_MODERATION,
                                                Building::STATUS_REQUIRES_MODIFICATION
                                            ], Building::TYPE_NEW_CONSTRUCTION_TEXT),
                                        'visible' => $controller->isAllowed('building', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Active'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/building/index', 'BuildingSearch' => [
                                            'status' => Building::STATUS_ACTIVE,
                                            'operation' => Building::TYPE_NEW_CONSTRUCTION_TEXT
                                        ]]),
                                        'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, Building::STATUS_ACTIVE, Building::TYPE_NEW_CONSTRUCTION_TEXT),
                                        'visible' => $controller->isAllowed('building', 'index')
                                    ],
                                    [
                                        'label' => Yii::t('main', 'Disabled'),
                                        'icon' => 'list-alt',
                                        'url' => Url::to(['/building/index', 'BuildingSearch' => [
                                            'status' => [
                                                Building::STATUS_DELETED,
                                                Building::STATUS_PAUSED
                                            ],
                                            'operation' => Building::TYPE_NEW_CONSTRUCTION_TEXT
                                        ]]),
                                        'active' => $controller->id === 'building' && $controller->action->id === 'index' && LeftMenuHelper::isBuildingMenuActive($request, [
                                                Building::STATUS_DELETED,
                                                Building::STATUS_PAUSED
                                            ], Building::TYPE_NEW_CONSTRUCTION_TEXT),
                                        'visible' => $controller->isAllowed('building', 'index')
                                    ],
                                ]
                            ],
                        ],
                    ],
                    [
                        'label' => Yii::t('main', 'Pages'),
                        'icon' => 'user',
                        'url' => Url::toRoute('/page/index'),
                        'active' => $controller->id === 'page' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('admin', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Requests'),
                        'icon' => 'user',
                        'url' => Url::toRoute('/request/index'),
                        'active' => $controller->id === 'request' && $controller->action->id === 'index',
                        'visible' => $controller->isAllowed('admin', 'index')
                    ],
                    [
                        'label' => Yii::t('main', 'Logout'),
                        'url' => ['/auth/logout'],
                        'icon' => 'user',
                        'template' => '<a href="{url}" data-method="post">{icon} {label}</a>',
                        'visible' => !Yii::$app->user->isGuest
                    ],
                ],
            ]
        ) ?>
    </section>
</aside>
