<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;


/* @var $this \yii\web\View */
/* @var $content string */


backend\assets\AppAsset::register($this);
dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Eagent - <?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <?php $this->head() ?>
</head>
<body class="skin-purple sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">
    <?= $this->render('header.php') ?>
    <?= $this->render('left.php') ?>
    <?= $this->render('content.php', ['content' => $content]) ?>
</div>
<?php
Modal::begin([
    'headerOptions' => ['id' => 'edit-modal-header'],
    'id' => 'edit-modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    //'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

Modal::end();
?>
<div class="crop-bg" style="display: none;">
    <div class="crop-container"></div>
    <div class="crop-actions">
        <i class="fa fa-times fa-lg crop-close" aria-hidden="true"></i>
        <button class="btn btn-primary crop-rotate-left">
            <i class="fa fa-rotate-left fa-lg" aria-hidden="true"></i>
        </button>
        <button class="btn btn-primary crop-rotate-right">
            <i class="fa fa-rotate-right fa-lg" aria-hidden="true"></i>
        </button>
        <button class="btn btn-primary crop-confirm">Сохранить</button>
    </div>
</div>

<?php $script = <<<JS
    $('#edit-modal').EditModal();

    function googleLoad() {
        $(document).trigger('google-loaded');
    }
JS;

$this->registerJs($script);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
