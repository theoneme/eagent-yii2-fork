<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 18.10.2018
 * Time: 16:34
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var \common\models\search\PageSearch $search
 */

\common\assets\GoogleAsset::register($this);

?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Page list</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->

    <div class="box-body">
        <?php Pjax::begin(['id' => 'property-pjax', 'timeout' => 6000]) ?>

        <div class="form-group">
            <?= Html::a('Create page', ['/page/create'], [
                'data-action' => 'load-edit-modal',
                'title' => 'Create page',
                'class' => 'btn btn-success',
                'data-pjax' => 0
            ]) ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Страница',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['title'],
                            ['/page/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['page']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => 'Update Region',
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::toRoute(["/page/{$action}", 'id' => $model['id']]);
                                break;
                            default:
                                $url = Url::toRoute(["/page/{$action}", 'id' => $model['id']]);
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
