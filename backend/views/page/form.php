<?php

use backend\assets\SelectizeAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \common\forms\ar\PageForm $pageForm
 * @var array $action
 */

SelectizeAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'property-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'property-pjax'
    ],
]); ?>

    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Translations</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык. Slug - как страница будет показываться в URL
            </div>
            <?php foreach ($pageForm->meta as $locale => $meta) { ?>
                <div class="row">
                    <div class="col-md-1">
                        <span class="label label-default"><?= $locale ?></span>
                    </div>
                    <div class="col-md-7">
                        <?= $form->field($meta, "[{$locale}]title")->textInput(['data-role' => 'page-translation-input']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($meta, "[{$locale}]slug") ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-11 col-md-offset-1">
                        <?= $form->field($meta, "[{$locale}]description", ['template' => '{input}'])->textarea(['placeholder' => 'content']) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>
