<?php

use common\decorators\AttributeTypeDecorator;
use common\forms\ar\AttributeForm;
use backend\assets\SelectizeAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var AttributeForm $model
 * @var array $action
 */

SelectizeAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'action' => $action,

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'attribute-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'attribute-pjax'
    ],
]); ?>

    <?= $form->field($model, 'alias')->textInput() ?>
    <?= $form->field($model, 'type')->dropDownList(AttributeTypeDecorator::getTypeLabels()) ?>

    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Название</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык.</div>
            <?php foreach ($model->meta as $locale => $meta) { ?>
                <div class="row">
                    <div class="col-md-1">
                        <span class="label label-default">
                            <?= Yii::$app->params['languages'][$locale] ?>
                        </span>
                    </div>
                    <div class="col-md-11">
                        <?= $form->field($meta, "[{$locale}]title", ['template' => '{label}{input}{error}'])->textInput(['data-role' => 'attribute-translation-input'])->label(false) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$script = <<<JS

JS;

$this->registerJs($script);
