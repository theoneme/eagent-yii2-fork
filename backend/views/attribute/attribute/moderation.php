<?php

use common\data\MyArrayDataProvider;
use common\decorators\AttributeTypeDecorator;
use common\models\search\AttributeSearch;
use common\models\Translation;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var AttributeSearch $search
 * @var MyArrayDataProvider $dataProvider
 * @var View $this
 * @var array $values
 */

$this->title = Yii::t('main', 'Attributes');
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $this->title?></h3>
    </div>

    <div class="box-body">
        <?php Pjax::begin(['id' => 'attribute-pjax', 'timeout' => 6000]) ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'alias',
                    'header' => 'Alias',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Название',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['translations'][Translation::KEY_TITLE],
                            ['/attribute/attribute/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['attribute']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'type',
                    'header' => 'Тип',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'value' => function ($model) {
                        return AttributeTypeDecorator::decorate($model['type']);
                    },
                    'format' => 'html',
                    'filter' => AttributeTypeDecorator::getTypeLabels(),
//                    'filterInputOptions' => [
//                        'value' => $filterStatus,
//                        'class' => 'form-control'
//                    ]
                ],
                [
                    'header' => 'Значения',
                    'contentOptions' => ['style' => 'font-size: 14px; white-space: pre-wrap;'],
                    'value' => function ($model) use ($values) {
                        return implode(' | ',
                            array_map(
                                function ($value) {
                                    return Html::a(
                                        $value['translations'][Translation::KEY_TITLE] ?? str_replace('-', '.', $value['alias']),
                                        ["/attribute/attribute-value/update", 'id' => $value['id']],
                                        [
                                            'title' => Yii::t('main', 'Update {0}', ['attribute value']),
                                            'data-pjax' => 0,
                                            'data-action' => 'load-edit-modal'
                                        ]
                                    );
                                },
                                $values[$model['id']] ?? []
                            )
                        );
                    },
                    'format' => 'raw',
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
