<?php

use common\data\MyArrayDataProvider;
use common\decorators\AttributeTypeDecorator;
use common\models\search\AttributeSearch;
use common\models\Translation;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var AttributeSearch $search
 * @var MyArrayDataProvider $dataProvider
 * @var View $this
 */

$this->title = Yii::t('main', 'Attributes');
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $this->title?></h3>
    </div>

    <div class="box-body">
        <?php Pjax::begin(['id' => 'attribute-pjax', 'timeout' => 6000]) ?>

        <div class="form-group">
            <?= Html::a(Yii::t('main', 'Add attribute'), ['/attribute/attribute/create'], [
                'data-action' => 'load-edit-modal',
                'title' => Yii::t('main', 'Add attribute'),
                'class' => 'btn btn-success',
                'data-pjax' => 0
            ]) ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'alias',
                    'header' => 'Alias',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Название',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['translations'][Translation::KEY_TITLE],
                            ['/attribute/attribute/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['attribute']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'type',
                    'header' => 'Тип',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'value' => function ($model) {
                        return AttributeTypeDecorator::decorate($model['type']);
                    },
                    'format' => 'html',
                    'filter' => AttributeTypeDecorator::getTypeLabels(),
//                    'filterInputOptions' => [
//                        'value' => $filterStatus,
//                        'class' => 'form-control'
//                    ]
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{values} {update}',
                    'options' => ['style' => 'width: 80px'],
                    'contentOptions' => ['style' => 'width: 80px; white-space: normal; text-align: center;'],
                    'buttons' => [
                        'values' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => 'Значения аттрибута',
                                'data-pjax' => 0
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('main', 'Update {0}', ['attribute']),
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'values':
                                $url = Url::toRoute(["/attribute/attribute-value/index", 'AttributeValueSearch[attribute_id]' => $model['id']]);
                                break;
                            case 'update':
                                $url = Url::to(['/attribute/attribute/update', 'id' => $model['id']]);
                                break;
                            default:
                                $url = '';
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
    <!-- /.box-body -->
</div>
