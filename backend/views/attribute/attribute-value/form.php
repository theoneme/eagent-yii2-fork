<?php

use backend\models\MergeAttributeValueForm;
use common\decorators\AttributeValueStatusDecorator;
use common\forms\ar\AttributeValueForm;
use backend\assets\SelectizeAsset;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var AttributeValueForm $model
 * @var array $action
 * @var integer|null $id
 * @var MergeAttributeValueForm|null $mergeModel
 */

SelectizeAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'action' => $action,

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'attribute-value-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'attribute-pjax'
    ],
]); ?>

<!--    --><?//= $form->field($model, "attribute_id", ['template' => '{input}'])->hiddenInput(); ?>
    <?= $form->field($model, 'alias')->textInput() ?>
    <?= $form->field($model, 'status')->radioList(AttributeValueStatusDecorator::getStatusLabels(false)) ?>

    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Название</div>
        <div class="panel-body">
            <div class="alert alert-danger">Надо заполнить хотя бы 1 язык.</div>
            <?php foreach ($model->meta as $locale => $meta) { ?>
                <div class="row">
                    <div class="col-md-1">
                        <span class="label label-default">
                            <?= Yii::$app->params['languages'][$locale] ?>
                        </span>
                    </div>
                    <div class="col-md-11">
                        <?= $form->field($meta, "[{$locale}]title", ['template' => '{label}{input}{error}'])->textInput(['data-role' => 'attribute-value-translation-input'])->label(false) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

    <?php if (!empty($id)) { ?>
        <div class="form-group">
            <?= Html::a(
                Yii::t('main', 'Delete'),
                ['/attribute/attribute-value/delete', 'id' => $id],
                ['class' => 'btn btn-block btn-danger', 'data-confirm' => 'Вы уверены что хотите удалить данное значение?']
            ) ?>
        </div>
    <?php } ?>

<?php ActiveForm::end(); ?>

<?php if (!empty($mergeModel)) {
    $form = ActiveForm::begin([
        'action' => ['/attribute/attribute-value/merge'],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => [
            'id' =>  'attribute-merge-form',
            'data-role' => 'edit-modal-form',
            'data-pj-container' => 'attribute-pjax'
        ]
    ]); ?>

    <div class="panel panel-default" data-role="translations-panel">
        <div class="panel-heading">Объединить с другим значением</div>
        <div class="panel-body">
            <?= Html::activeHiddenInput($mergeModel, 'fromId', ['value' => $id]) ?>
            <?= $form->field($mergeModel, 'toId')->dropDownList(
                [],
                ['id' => 'merge-target-select', 'class' => '']
            )->label('Выберите значение с которым хотите объединить') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('main', 'Merge'), ['class' => 'btn btn-block btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    $attrValueUrl = Url::to(['/ajax/attribute-values', 'attribute' => $model->attribute_id]);
    $script = <<<JS
        $("#merge-target-select").selectize({
            inputClass: 'form-control selectize-input', 
            dropdownClass: 'selectize-dropdown drop-up', 
            valueField: "id",
            labelField: "value",
            searchField: ["value"],
            "load": function(query, callback) {
                if (!query.length) {
                    return callback();
                }
                $.get('$attrValueUrl', {query: query}, function(data) { 
                    callback(data);
                }).fail(function() {
                    callback();
                });
            }
        });
JS;

    $this->registerJs($script);
} ?>
