<?php

use common\data\MyArrayDataProvider;
use common\decorators\AttributeValueStatusDecorator;
use common\dto\AttributeDTO;
use common\models\search\AttributeValueSearch;
use common\models\Translation;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var AttributeValueSearch $search
 * @var MyArrayDataProvider $dataProvider
 * @var AttributeDTO $attribute
 */

$this->title = $attribute
    ? Yii::t('main', '"{attribute}" attribute values', ['attribute' => $attribute['title']])
    : Yii::t('main', 'Attribute values');
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?= $this->title?></h3>
    </div>

    <div class="box-body">
        <?php Pjax::begin(['id' => 'attribute-pjax', 'timeout' => 6000]) ?>

        <?php if ($attribute) { ?>
            <div class="form-group">
                <?= Html::a(Yii::t('main', 'Add attribute value'), ['/attribute/attribute-value/create', 'attributeId' => $attribute['id']], [
                    'data-action' => 'load-edit-modal',
                    'title' => Yii::t('main', 'Add attribute value'),
                    'class' => 'btn btn-success',
                    'data-pjax' => 0
                ]) ?>
            </div>
        <?php }?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'id' => 'user-grid',
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 72px; white-space: normal; font-size: 12px;'],
                    'options' => ['style' => 'width: 72px']
                ],
                [
                    'attribute' => 'alias',
                    'header' => 'Alias',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                ],
                [
                    'attribute' => 'title',
                    'header' => 'Значение',
                    'value' => function ($model) {
                        /** @var array $model */
                        return Html::a($model['translations'][Translation::KEY_TITLE] ?? str_replace('-', '.', $model['alias']),
                            ['/attribute/attribute-value/update', 'id' => $model['id']],
                            [
                                'title' => Yii::t('main', 'Update {0}', ['attribute']),
                                'data-action' => 'load-edit-modal',
                                'data-pjax' => 0
                            ]
                        );
                    },
                    'format' => 'raw',
                ],
                [
                    'attribute' => 'status',
                    'header' => 'Статус',
                    'contentOptions' => ['style' => 'width: 100px; white-space: normal; text-align: center;'],
                    'options' => ['style' => 'width: 100px'],
                    'value' => function ($model) {
                        return AttributeValueStatusDecorator::decorate($model['status']);
                    },
                    'format' => 'html',
                    'filter' => AttributeValueStatusDecorator::getStatusLabels(false),
//                    'filterInputOptions' => [
//                        'value' => $filterStatus,
//                        'class' => 'form-control'
//                    ]
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{update} {delete}',
                    'options' => ['style' => 'width: 60px'],
                    'contentOptions' => ['style' => 'width: 60px; white-space: normal; text-align: center;'],
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('main', 'Update {0}', ['attribute value']),
                                'data-pjax' => 0,
                                'data-action' => 'load-edit-modal'
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        switch ($action) {
                            case 'update':
                                $url = Url::to(['/attribute/attribute-value/update', 'id' => $model['id']]);
                                break;
                            case 'delete':
                                $url = Url::to(['/attribute/attribute-value/delete', 'id' => $model['id']]);
                                break;
                            default:
                                $url = '';
                                break;
                        }
                        return $url;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
</div>
