<?php

use backend\assets\SelectizeAsset;
use common\forms\ar\CategoryForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var CategoryForm $model
 * @var $create bool
 */

SelectizeAsset::register($this);

?>

<?php Pjax::begin(['id' => 'edit-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>
    <?php $form = ActiveForm::begin([
        'action' => ['/category/item', 'id' => $model->_category->id, 'parent_id' => $model->parent_id],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "<div class='row'>
            <div class='col-md-3'>
                {label}
            </div>
            <div class='col-md-9'>
                {input}
                {error}
            </div>
        </div>",
        ],
        'options' => [
            'id' => 'category-form',
            'data-pjax' => false,
//            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
        <div class="kv-detail-heading">
            <div class="kv-detail-crumbs">
                <span class="kv-crumb-active"><?= $create ? 'Добавление' : 'Редактирование' ?> категории</span>
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary" title="" data-original-title="Сохранить">
                        <i class="glyphicon glyphicon-floppy-disk"></i>
                    </button>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="category-form">
            <div class="panel panel-default" data-role="translations-panel">
                <div class="panel-heading">Переводы</div>
                <div class="panel-body">
                    <div class="alert alert-danger">Надо заполнить хотя бы 1 язык. Slug - как категория будет отображаться в URL</div>
                    <?php foreach ($model->meta as $locale => $meta) { ?>
                        <div class="row">
                            <div class="col-md-1">
                                <span class="label label-default"><?= $locale ?></span>
                            </div>
                            <div class="col-md-7">
                                <?= $form->field($meta, "[{$locale}]title")->textInput(['data-role' => 'category-translation-input']) ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($meta, "[{$locale}]slug") ?>
                            </div>
                            <div class="col-md-7 col-md-offset-1">
                                <?= $form->field($meta, "[{$locale}]add_title")->textInput(['data-role' => 'category-translation-input'])->label('Название в единственном числе') ?>
                            </div>
                        </div>
                        <hr>
                    <?php } ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('labels', 'Save'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
            </div>
        </div>
    <?php
    ActiveForm::end();
Pjax::end();
?>
