<?php

use common\models\Category;
use kartik\tree\TreeViewAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $models Category[] */

$this->title = 'Категории';
TreeViewAsset::register($this);
?>
    <div class="category-index">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
<!--                <div class="pull-right">-->
<!--                    --><?//= Html::a(Yii::t('app', 'Flush cache'), '#', ['data-action' => 'flush-cache']) ?>
<!--                </div>-->
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-control kv-tree-wrapper">
                            <div class="kv-header-container" style="border: 1px solid #d2d6de">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="kv-heading-container">
                                            <?= $this->title?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="kv-search-sm kv-search-container">
                                            <span class="close kv-search-clear" title="Очистить результаты поиска">×</span>
                                            <input class="form-control input-sm kv-search-input" name="kv-tree-search" placeholder="Поиск..." type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="w0-tree" class="kv-tree-container" style="height:600px; border: 1px solid #d2d6de">
                                <ul class="kv-tree">
                                    <?php Pjax::begin(['id' => 'tree-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>
                                    <?php foreach ($models as $model) {
                                        echo $this->render('tree-item', ['model' => $model]);
                                    } ?>
                                    <?php Pjax::end(); ?>
                                </ul>
                            </div>
                            <div class="kv-footer-container">
                                <div id="w0-toolbar" class="kv-toolbar-container" role="toolbar">
                                    <div class="btn-group-sm btn-group" role="group">
                                        <button data-url="<?= Url::to(['/category/item']) ?>" type="button" class="btn btn-default kv-toolbar-btn kv-create-root hint--top" title="" data-hint="Добавить новое дерево">
                                            <span class="kv-icon-10 fa fa-tree"></span>
                                        </button>
                                        <button type="button" class="btn btn-default kv-toolbar-btn kv-create hint--top" title="" data-hint="Добавить новую категорию">
                                            <span class="kv-icon-10 fa fa-plus"></span>
                                        </button>
                                        <button type="button" class="btn btn-default kv-toolbar-btn kv-remove hint--top" title="" data-hint="Удалить">
                                            <span class="kv-icon-10 fa fa-trash"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div id="w0-detail" class="kv-detail-container">
                            <div class="kv-node-message">Выберите элемент дерева для редактирования</div>
                            <div class="edit-form-container">
                                <?php Pjax::begin(['id' => 'edit-pjax-container', 'linkSelector' => false, 'formSelector' => false]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$script = <<<JS
    // Скрипты для работы с деревом (слева)
    $(document).on('click', '.kv-node-expand, .kv-node-collapse', function(){
        $(this).closest('.kv-parent').toggleClass('kv-collapsed');
        return false;
    });
    $(document).on('click', '.kv-node-detail', function() {
        $('.new-tree-node').remove();
        $('.kv-node-detail').removeClass('kv-focussed');
        $(this).addClass('kv-focussed');
        $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
 	    $.pjax.reload({container: "#edit-pjax-container", url: $(this).closest('li').data('url'), replace: false});
    });
    $(document).on('click', '.kv-create-root', function() {
        $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
        renderNewNode('Новое дерево', 'tree', $('#tree-pjax-container'));
 	    $.pjax.reload({container: "#edit-pjax-container", url: $(this).data('url'), replace: false});
    });
    $(document).on('click', '.kv-create', function() {
        let li = $('.kv-focussed').closest('li');
        if (li.length) {
            $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
            renderNewNode('Новая категория', 'file', li.children('ul'));
            $.pjax.reload({container: "#edit-pjax-container", url: li.data('child-url'), replace: false});
        }
        else {
            alertCall('warning', 'Выберите элемент дерева к которому хотите добавить дочерний элемент');
        }
    });
    $(document).on('click', '.kv-remove', function() {
        let li = $('.kv-focussed').closest('li');
        if (li.length) {
            if (confirm('Вместе с элементом дерева будут удалены все дочерние элементы. Вы уверены что хотите удалить"' + $.trim($('.kv-focussed .kv-node-label').text()) + '"?')){
                $.post(li.data('delete-url'), {},
                    function(data) {
                        $('.kv-detail-container').removeClass('kv-editing kv-loading');
                        $.pjax.reload({container: "#tree-pjax-container"});
                    }
                );
            }
        }
        else {
            alertCall('warning', 'Выберите элемент дерева который хотите удалить');
        }
    });
    $('#edit-pjax-container').on('pjax:success', function(event, data, status, xhr, options) {
        $('.kv-detail-container').removeClass('kv-loading').addClass('kv-editing');
    }).on('pjax:error', function (event) {
		event.preventDefault();
	});
    
    function renderNewNode(text, icon, prependTo) {
        prependTo.prepend(
            '<li class="new-tree-node">' +
                '<div class="kv-tree-list" tabindex="-1">' +
                    '<div class="kv-node-detail" tabindex="-1">' +
                        '<span class="text-info kv-node-icon kv-icon-child">' +
                            '<span class="fa fa-' + icon +'"></span>' +
                        '</span>' +
                        '<span class="kv-node-label">' +
                            text +
                        '</span>' +
                    '</div>' +
                '</div>' +
           '</li>'
        );
        $('.kv-node-detail').removeClass('kv-focussed');
        $('.new-tree-node .kv-node-detail').addClass('kv-focussed');
        $('.new-tree-node').parents('.kv-parent').removeClass('kv-collapsed');
    }
    
    // Скрипты для работы с формой (справа)
    // let actionAfterUpload = 'submit-form';
    $(document).on('beforeSubmit', '#edit-pjax-container form', function () {
        // let returnValue = true;
        // $(".file-upload-input").each(function(index){
        //     if ($(this).fileinput("getFilesCount") > 0) {
        //         actionAfterUpload = 'submit-form';
        //         $(this).fileinput("upload");
        //         returnValue = false;
        //         return false;
        //     }
        // });
        // if (!returnValue) {
        //     return false;
        // }
        let self = $(this);
        let formData = new FormData($(this)[0]);
        
        $('.kv-detail-container').removeClass('kv-editing').addClass('kv-loading');
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    $('.kv-detail-container').removeClass('kv-editing kv-loading');
                    $('#tree-pjax-container').one('pjax:success', function(event, data, status, xhr, options) {
                        let treeItem = $('#' + result.id);
                        treeItem.parents('.kv-parent').removeClass('kv-collapsed');
                        treeItem.find('.kv-node-detail').first().addClass('kv-focussed');
                        $(this).animate({
                            scrollTop: treeItem.position().top - $(this).position().top
                        }, 800);
                    });
                    $.pjax.reload({container: "#tree-pjax-container"});
                    alertCall('success', 'Элемент дерева успешно сохранен');
                }
                else {
                    alertCall('error', 'Ошибка при сохранении');
                }
            }
        });
        return false;
    });
    
    $(document).on("afterValidate", '#edit-pjax-container form', function (event, messages, errorAttributes) {
        if (errorAttributes.length) {
            let firstError = $(this).find(".has-error").first();
            let firstErrorTab = firstError.closest('.tab-pane');
            if (firstErrorTab.length) {
                $('a[href="#' + firstErrorTab.attr('id') + '"]').tab('show');
            }
            $("html, body").animate({scrollTop: firstError.offset().top}, 800);
        }
    });
    
    // $(document).on("fileuploaded", ".file-upload-input", function(event, data, previewId, index) {
    //     let response = data.response;
    //     let imagesContainer = $(this).closest('.file-input-container').children('.images-container');   
    //     let id = imagesContainer.attr('data-index');
    //     let model = imagesContainer.data('model');
    //     imagesContainer.find("input").remove();
    //     imagesContainer.append("<input class='image-source' name='" + model + id + "[image]' data-key='" + response.imageKey + "' type='hidden' value='" + response.uploadedPath + "'>");
    // }).on("filedeleted", function(event, key) {
    //     $(".images-container input[data-key='" + key + "']").remove();
    // }).on("filebatchuploadcomplete", function(event, files, extra) {
    //     if (actionAfterUpload === 'submit-item') {
    //         $(event.target).closest('.element-form').find('.submit-button').trigger('click');
    //     }
    //     else {
    //         $(event.target).closest('form').submit();
    //     }
    // });
JS;
$this->registerJs($script);
