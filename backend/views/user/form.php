<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \common\forms\ar\UserForm $userForm
 * @var array $userDTO
 * @var array $action
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'id' => 'user-form',
        'enctype' => 'multipart/form-data',
        'data-pjax' => false,
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'user-pjax'
    ],
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
]); ?>

<?php if ($userForm->_user->isNewRecord === false) { ?>
    <p>
        <span class="label label-default">
            Ссылка на сайте: <?= Html::a('Открыть', Yii::$app->frontUrlManager->createUrl([
                '/agent/agent/view', 'id' => $userDTO['id']
            ]), [
                'target' => '_blank'
            ]) ?>
        </span>
    </p>
<?php } ?>

<?php foreach ($userForm->steps as $key => $step) { ?>
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title"><?= $step['title'] ?></h3>
            <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($userForm->steps)]) ?></span>
        </div>
        <div class="box-body">
            <?php foreach ($step['config'] as $stepConfig) { ?>
                <?php if ($stepConfig['type'] === 'view') { ?>
                    <?= $this->render("steps/{$stepConfig['value']}", [
                        'form' => $form,
                        'userForm' => $userForm,
                        'step' => $step,
                        'createForm' => false,
                        'userDTO' => $userDTO
                    ]) ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<div class="form-group row">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('labels', 'Update'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $script = <<<JS
var form = $('#user-form'),
    hasFileUploadError = false;

form.on('submit', function() { 
    if ($("#file-upload-input").fileinput("getFilesCount") > 0) {
        $("#file-upload-input").fileinput("upload");
        return false;
    } else {
        return true;
    }
});

$('#file-upload-input').on('fileuploaded', function(event, data) {
    let response = data.response;
    $('#userform-avatar').val(response.uploadedPath).data('key', response.imageKey);
}).on('filedeleted', function(event, key) {
    $('#userform-avatar').val(null);
}).on("filebatchuploadcomplete", function() {
    if (hasFileUploadError === false) {
        form.submit();
    } else {
        hasFileUploadError = false;
    }
}).on("fileuploaderror", function(event, data) {
    hasFileUploadError = true;
    $('#' + data.id).find('.kv-file-remove').click();
});

JS;
$this->registerJs($script);

?>
