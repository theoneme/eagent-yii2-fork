<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

use common\helpers\FileInputHelper;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * @var array $step
 * @var \backend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<p>
    <?= Yii::t('wizard', 'Upload Your Profile Photo') ?>
</p>
<div>
    <?= FileInput::widget(
        ArrayHelper::merge(FileInputHelper::getDefaultConfig(), [
            'id' => 'file-upload-input',
            'name' => 'uploaded_images',
            'options' => ['multiple' => false],
            'pluginOptions' => [
                'dropZoneTitle' => Yii::t('wizard', 'Drag & drop photos here &hellip;'),
                'overwriteInitial' => true,
                'initialPreview' => !empty($userForm->_user->avatar) ? $userForm->avatar : [],
                'initialPreviewConfig' => !empty($userForm->_user->avatar) ? [[
                    'caption' => basename($userForm->avatar),
                    'url' => Url::toRoute('/image/delete'),
                    'key' => 0
                ]] : [],
            ]
        ])
    ) ?>
</div>
<div class="images-container">
    <?= $form->field($userForm, 'avatar', ['template' => '{input}'])->hiddenInput([
        'value' => FileInputHelper::buildOriginImagePath($userForm->avatar),
        'data-key' => 'image_init_0'
    ])->label(false) ?>
</div>
<p>&nbsp;</p>

