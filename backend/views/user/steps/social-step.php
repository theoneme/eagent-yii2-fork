<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

use yii\helpers\Html;

/**
 * @var array $step
 * @var \backend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 * @var array $userDTO
 */
?>

<?php foreach ($userDTO['socials'] as $provider => $social) { ?>
    <p>
        <b>
            <?= $provider ?>&nbsp;
        </b>
        <?= Html::a($social['url'], $social['url'], [
            'target' => '_blank'
        ]) ?>
    </p>
<?php } ?>