<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:32
 */

/**
 * @var array $step
 * @var \backend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<?= $form->field($userForm, 'phone')->textInput() ?>
<?= $form->field($userForm, 'email')->textInput() ?>
<?= $form->field($userForm, 'username')->textInput() ?>