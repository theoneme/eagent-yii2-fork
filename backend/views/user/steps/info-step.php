<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 08.11.2018
 * Time: 17:28
 */

/**
 * @var array $step
 * @var \backend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */
?>


<?= $form->field($userForm->meta, 'subtitle')->textarea([
    'placeholder' => Yii::t('wizard', 'Profile Short Description'),
    'rows' => 4
]) ?>
<?= $form->field($userForm->meta, 'description')->textarea([
    'placeholder' => Yii::t('wizard', 'Profile Description'),
    'rows' => 8
]) ?>
<?= $form->field($userForm, 'ads_allowed', ['template' => "<div class='row'>
    <div class='col-md-3'>
        {label}
    </div>
    <div class='col-md-9'>
        {input}
        Yes
        {error}
    </div>
</div>"])->checkbox([], false) ?>
<?= $form->field($userForm, 'ads_allowed_partners', ['template' => "<div class='row'>
    <div class='col-md-3'>
        {label}
    </div>
    <div class='col-md-9'>
        {input}
        Yes
        {error}
    </div>
</div>"])->checkbox([], false) ?>