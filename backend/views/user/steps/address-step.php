<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 05.11.2018
 * Time: 14:12
 */

/**
 * @var array $step
 * @var \backend\forms\ar\UserForm $userForm
 * @var \yii\widgets\ActiveForm $form
 */

?>

<?= \common\widgets\GmapsActiveInputWidget::widget([
    'model' => $userForm->geo,
    'form' => $form,
    'inputOptions' => ['class' => 'form-control'],
    'withMap' => true
]) ?>

<div id="google-map" style="height: 300px;">

</div>


