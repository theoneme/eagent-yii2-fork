<?php

use common\decorators\RequestStatusDecorator;
use common\models\Request;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var \common\forms\ar\RequestForm $requestForm
 * @var array $withdrawalTypesData
 * @var ActiveDataProvider $languageDataProvider
 * @var array $categories
 * @var array $subcategories
 * @var array $action
 * @var integer $parentCategoryId
 */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::to($action),

    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "<div class='row'>
                <div class='col-md-3'>
                    {label}
                </div>
                <div class='col-md-9'>
                    {input}
                    {error}
                </div>
            </div>",
    ],
    'options' => [
        'id' => 'request-form',
        'enctype' => 'multipart/form-data',
        'data-role' => 'edit-modal-form',
        'data-pj-container' => 'property-pjax'
    ],
]); ?>

    <div>
        <?= $form->field($requestForm, 'type', ['template' => '{input}'])->radioList([
            Request::TYPE_SALE => Yii::t('main', 'Buy'),
            Request::TYPE_RENT => Yii::t('main', 'Rent')
        ], [
            'item' => function ($index, $label, $name, $checked, $value) {
                $chk = $checked ? 'checked' : '';
                $active = $checked ? 'active' : '';
                $output = "
                    <div class='chover'>
                        <input name='{$name}' id='requestform-type-{$index}' class='radio-checkbox hidden type-input' value='{$value}' {$chk} type='radio'>
                        <label class='btn btn-default {$active}' for='requestform-type-{$index}'>{$label}</label>
                    </div>
                ";
                return $output;
            },
            'class' => 'flex'
        ]) ?>
    </div>
    <div>
        <div class="form-group">
            <div class="flex">
                <?php foreach ($categories as $id => $category) {
                    $chk = $id === $parentCategoryId ? 'checked' : '';
                    $active = $id === $parentCategoryId ? 'active' : '';
                    echo "<div class='chover'>
                                <input name='category_id' id='parent-category-id-{$id}' class='radio-checkbox hidden category-input' value='{$id}' {$chk} type='radio'>
                                <label class='btn btn-default {$active}' for='parent-category-id-{$id}'>{$category}</label>
                            </div>";
                }?>
            </div>
        </div>
    </div>
    <div>
        <?= $form->field($requestForm->category, 'category_id', ['template' => '{input}{error}'])->radioList($subcategories, [
            'item' => function ($index, $label, $name, $checked, $value) {
                $chk = $checked ? 'checked' : '';
                $active = $checked ? 'active' : '';
                $output = "<div class='chover'>
                                <input name='{$name}' id='requestform-category-id-{$index}' class='radio-checkbox hidden subcategory-input' value='{$value}' {$chk} type='radio'>
                                <label class='btn btn-default {$active}' for='requestform-category-id-{$index}'>{$label}</label>
                            </div>";
                return $output;
            },
            'class' => 'flex',
            'style' => 'flex-wrap: wrap;'
        ]) ?>
    </div>
    <div>
        <?= $form->field($requestForm, 'locale')->dropDownList(Yii::$app->params['languages'])->label(Yii::t('main', 'Language')) ?>
    </div>
    <div>
        <?= $form->field($requestForm, 'status')
            ->dropDownList(RequestStatusDecorator::getStatusLabels(false), [
                'prompt' => '-- ' . Yii::t('main', 'Select Status')
            ]) ?>
    </div>

<?php foreach ($requestForm->steps as $key => $step) { ?>
    <div class="box box-danger">
        <div class="box-header">
            <h3 class="box-title"><?= $step['title'] ?></h3>
            <span><?= Yii::t('wizard', 'Step {first} from {total}', ['first' => $key, 'total' => count($requestForm->steps)]) ?></span>
        </div>
        <div class="box-body">
            <?php foreach ($step['config'] as $stepConfig) { ?>
                <?php if ($stepConfig['type'] === 'view') { ?>
                    <?= $this->render("steps/{$stepConfig['value']}", [
                        'form' => $form,
                        'propertyForm' => $requestForm,
                        'step' => $step,
                        'createForm' => false
                    ]) ?>
                <?php } ?>

                <?php if ($stepConfig['type'] === 'constructor') { ?>
                    <?php foreach ($stepConfig['groups'] as $group) { ?>
                        <?php $attributes = $requestForm->dynamicForm->getAttributesByGroup($group) ?>
                        <?= $this->render("partial/attributes-group", [
                            'form' => $form,
                            'attributes' => array_flip($attributes),
                            'dynamicForm' => $requestForm->dynamicForm
                        ]) ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('labels', 'Create'), ['class' => 'btn btn-block btn-success', 'id' => 'submit-button']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php
$renderAttributesRoute = Url::toRoute('/property/attributes');
$canonical = Url::current(['id' => null, 'type' => null, 'category_id' => null]);
$id = $requestForm->_request['id'] ?? 'null';
$script = <<<JS
    $('#request-form').on('submit', function() {
        if ($("#file-upload-input").fileinput("getFilesCount") > 0) { 
            $("#file-upload-input").fileinput("upload");
            return false;
        } else {
            return true;
        }
    });
    
    function reloadModal(categoryId, type) {
        let params =  {
            category_id: categoryId,
            type: type,
            id: {$id}
        };
        let query = $.param(params);
        $('#edit-modal').find('.modal-body').load('{$canonical}' + '?' + query);
    }
    
    $(".category-input, .subcategory-input").on("change", function(e) {
        let type = $('.type-input:checked').val();
        let categoryId = $(this).val();
        reloadModal(categoryId, type);
        return false;
    });
    $(".type-input").on("change", function(e) {
        let type = $(this).val();
        let categoryId = $('.subcategory-input:checked').val();
        if (!categoryId) {
            categoryId = $('.category-input:checked').val();
        }
        reloadModal(categoryId, type);
        return false;
    });
JS;

$this->registerJs($script);

