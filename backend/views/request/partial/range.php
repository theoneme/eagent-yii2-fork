<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 26.10.2018
 * Time: 16:31
 */

use common\forms\ar\DynamicForm;

/**
 * @var DynamicForm $dynamicForm
 * @var integer $attribute
 */

?>
<div class="row">
    <div class="col-md-4">
        <?= $dynamicForm->getAttributeLabel($attribute) ?>
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($dynamicForm, "{$attribute}[min]")->textInput()->label(Yii::t('wizard', 'From')) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($dynamicForm, "{$attribute}[max]")->textInput()->label(Yii::t('wizard', 'To')) ?>
            </div>
        </div>
    </div>
</div>


