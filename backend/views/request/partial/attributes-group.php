<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.10.2018
 * Time: 18:08
 */

use yii\widgets\ActiveForm;

/**
 * @var \common\forms\ar\DynamicForm $dynamicForm
 * @var ActiveForm|null $form
 * @var array $attributes
 */

?>

<?php foreach ($attributes as $key => $attribute) { ?>
        <?= $this->render($dynamicForm->config['types'][$key], [
            'form' => $form,
            'dynamicForm' => $dynamicForm,
            'attribute' => $key
        ]) ?>
<?php } ?>
