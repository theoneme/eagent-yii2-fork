<?php

return [
    '"{attribute}" attribute values' => 'Значения атрибута "{attribute}"',
    'Active' => 'Активен',
    'Add attribute' => 'Добавить аттрибут',
    'Add attribute value' => 'Добавить значение атрибута',
    'Add company member' => '',
    'Attribute values' => 'Знаения атрибутов',
    'Attributes' => 'Атрибуты',
    'Back to companies' => 'Назад к компаниям',
    'Buildings' => 'Здания',
    'Buy' => 'Покупка',
    'Cities' => 'Города',
    'Clear cache' => 'Очистить кэш',
    'Companies' => 'Компании',
    'Countries' => 'Страны',
    'Create Company' => 'Создать компанию',
    'Dashboard' => 'Сводка',
    'Debug' => 'Отладка',
    'Delete' => 'Удалить',
    'Disabled' => 'Отключено',
    'For Rent' => 'В аренду',
    'For Sale' => 'На продажу',
    'Language' => 'Язык',
    'Locations' => 'Локации',
    'Logout' => 'Выход',
    'Menu Yii2' => '',
    'Merge' => 'Объединить',
    'Moderation' => 'Модерация',
    'New Construction' => 'Новостройка',
    'New Constructions' => 'Новостройки',
    'Old Construction' => 'Вторичное здание',
    'Pages' => 'Страницы',
    'Pending' => 'Ожидает',
    'Properties' => 'Объекты',
    'Regions' => 'Регионы',
    'Rent' => 'Аренда',
    'Requests' => 'Заявки',
    'Sale' => 'Продажа',
    'Save' => 'Сохранить',
    'Secondary' => 'Вторичное',
    'Select Status' => 'Выберите статус',
    'Stats' => 'Статистика',
    'System' => 'Система',
    'Update {0}' => 'Редактирование {0}',
    'Users' => 'Пользователи',
    'Categories' => 'Категории',
    'Districts' => 'Районы',
    'Micro Districts' => 'Микрорайоны',
];
