<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 10:25
 */

return [
    'color' => null,
    'interactive' => true,
    'sourcePath' => ['backend'],
    'messagePath' => 'backend/messages',
    'languages' => array_diff(array_keys(Yii::$app->params['supportedLocales']), ['en-GB']),
    'translator' => 'Yii::t',
    'sort' => true,
    'overwrite' => true,
    'removeUnused' => true,
    'markUnused' => true,
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/BaseYii.php',
    ],
    'only' => [
        '*.php',
    ],
    'format' => 'php',
    'db' => 'db',
    'sourceMessageTable' => '{{%source_message}}',
    'messageTable' => '{{%message}}',
    'catalog' => 'messages',
    'ignoreCategories' => ['agent', 'catalog', 'index', 'property', 'seo', 'tariff', 'yii', 'labels', 'wizard'],
];
