<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 22.03.2019
 * Time: 17:15
 */

namespace backend\helpers;

use yii\web\Request;

/**
 * Class LeftMenuHelper
 * @package backend\helpers
 */
class LeftMenuHelper
{
    /**
     * @param Request $request
     * @param $statuses
     * @param $operation
     * @return bool
     */
    public static function isPropertyMenuActive($request, $statuses, $operation)
    {
        $searchParams = $request->get('PropertySearch');

        $type = $searchParams['operation'];

        if ($statuses !== null) {
            $intersect = array_intersect((array)$searchParams['status'], (array)$statuses);
            $result = !empty($intersect) && $operation === $type;
        } else {
            $result = $operation === $type;
        }

        return $result;
    }

    /**
     * @param Request $request
     * @param $statuses
     * @param $operation
     * @return bool
     */
    public static function isBuildingMenuActive($request, $statuses, $operation)
    {
        $searchParams = $request->get('BuildingSearch');

        $type = $searchParams['operation'];

        if ($statuses !== null) {
            $intersect = array_intersect((array)$searchParams['status'], (array)$statuses);
            $result = !empty($intersect) && $operation === $type;
        } else {
            $result = $operation === $type;
        }

        return $result;
    }
}
