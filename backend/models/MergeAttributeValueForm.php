<?php

namespace backend\models;

use common\dto\AttributeValueDTO;
use common\interfaces\RepositoryInterface;
use common\models\AttributeValue;
use common\repositories\sql\AttributeValueRepository;
use common\repositories\sql\BuildingAttributeRepository;
use common\repositories\sql\PropertyAttributeRepository;
use common\services\entities\AttributeValueService;
use Yii;
use yii\base\Model;

/**
 * Class MergeAttributeValueForm
 * @package common\modules\attribute\models\backend
 */
class MergeAttributeValueForm extends Model
{
    /**
     * @var integer
     */
    public $fromId;
    /**
     * @var integer
     */
    public $toId;
    /**
     * @var RepositoryInterface
     */
    protected $_attributeValueRepository;
    /**
     * @var AttributeValueService
     */
    protected $_attributeValueService;
    /**
     * @var RepositoryInterface
     */
    protected $_propertyAttributeRepository;
    /**
     * @var RepositoryInterface
     */
    protected $_buildingAttributeRepository;

    /**
     * MergeAttributeValueForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->_attributeValueRepository = Yii::$container->get(AttributeValueRepository::class);
        $this->_attributeValueService = Yii::$container->get(AttributeValueService::class);
        $this->_propertyAttributeRepository = Yii::$container->get(PropertyAttributeRepository::class);
        $this->_buildingAttributeRepository = Yii::$container->get(BuildingAttributeRepository::class);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fromId', 'toId'], 'required'],
            [['fromId', 'toId'], 'integer'],
            [['fromId'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['fromId' => 'id']],
            [['toId'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::class, 'targetAttribute' => ['toId' => 'id']],
            ['fromId', 'compare', 'compareAttribute' => 'toId', 'operator' => '!==', 'type' => 'number'],
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        /**
         * @var AttributeValueDTO $fromValue
         * @var AttributeValueDTO $toValue
         */
        $fromValue = $this->_attributeValueService->getOne(['attribute_value.id' => $this->fromId]);
        $toValue = $this->_attributeValueService->getOne(['attribute_value.id' => $this->toId]);
        if ($fromValue['attribute_id'] !== $toValue['attribute_id']) {
            return false;
        }

        $this->_propertyAttributeRepository->updateManyByCriteria(
            ['value' => $fromValue['id']],
            ['value' => $toValue['id'], 'value_alias' => $toValue['alias'], 'value_number' => $toValue['title']]
        );
        $this->_buildingAttributeRepository->updateManyByCriteria(
            ['value' => $fromValue['id']],
            ['value' => $toValue['id'], 'value_alias' => $toValue['alias'], 'value_number' => $toValue['title']]
        );

        $this->_attributeValueRepository->deleteOneByCriteria(['id' => $this->fromId]);

        return true;
    }
}
