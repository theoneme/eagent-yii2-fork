<?php

namespace backend\models\search;

/**
 * Class BuildingSearch
 * @package backend\models\search
 */
class BuildingSearch extends \common\models\search\BuildingSearch
{
    /**
     * @var string
     */
    public $address;
    /**
     * @var float
     */
    public $lat;
    /**
     * @var float
     */
    public $lng;
}
