<?php

namespace backend\models\search;

use common\models\Translation;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class CompanySearch
 * @package backend\models\search
 */
class CompanySearch extends \common\models\search\CompanySearch
{
    /**
     * @var string
     */
    public $title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['title', 'string']
        ]);
    }

    /**
     * @return mixed
     */
    protected function initQuery()
    {
        $query = $this->_companyRepository
            ->joinWith(['companyMembers', 'translations'])
            ->with(['user.contacts', 'addressTranslations' => function ($query) {
                /** @var ActiveQuery $query */
                $query->andOnCondition(['company_address_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }])
            ->groupBy('company.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        $title = $params['CompanySearch']['title'] ?? null;
        if ($title) {
            $criteria = ['and', ['and', ['like', 'company_translations.value', $title], ['company_translations.key' => Translation::KEY_TITLE]], $criteria];
        }

        return $criteria;
    }
}
