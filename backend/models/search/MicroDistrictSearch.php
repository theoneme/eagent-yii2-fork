<?php
namespace backend\models\search;

use common\interfaces\repositories\DistrictRepositoryInterface;
use common\interfaces\RepositoryInterface;
use common\models\search\MicroDistrictSearch as BaseMicroDistrictSearch;
use common\models\Translation;

/**
 * Class CitySearch
 * @package backend\models\search
 */
class MicroDistrictSearch extends BaseMicroDistrictSearch
{
    /**
     * @var string
     */
    public $request;
    /**
     * @var string
     */
    public $city_title;
    /**
     * @var string
     */
    public $district_title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['id'], 'each', 'rule' => ['integer'], 'when' => function ($model) {
                return is_array($model->id);
            }],
            [['id'], 'integer', 'when' => function ($model) {
                return !is_array($model->id);
            }],
            [['request', 'city_title', 'district_title'], 'string']
        ]);
    }

    /**
     * @param array $params
     * @return DistrictRepositoryInterface|RepositoryInterface
     */
    protected function initQuery(array $params = [])
    {
        $params['request'] = $this->request;
        $query = parent::initQuery($params);

        if (!empty($this->city_title)) {
            $query->joinWith(['city.translations']);
        } else {
            $query->with(['city.translations']);
        }

        if (!empty($this->district_title)) {
            $query->joinWith(['district.translations']);
        } else {
            $query->with(['district.translations']);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        $params['request'] = $this->request;
        $criteria = parent::improveCriteria($criteria, $params);

        if (!empty($this->city_title)) {
            $criteria = ['and', $criteria, ['like', 'city_translations.value', "{$this->city_title}%", false], ['city_translations.key' => Translation::KEY_TITLE]];
        }
        if (!empty($this->district_title)) {
            $criteria = ['and', $criteria, ['like', 'district_translations.value', "{$this->district_title}%", false], ['district_translations.key' => Translation::KEY_TITLE]];
        }

        return $criteria;
    }
}
