<?php

namespace backend\models\search;

use Yii;

/**
 * Class PropertySearch
 * @package backend\models\search
 */
class PropertySearch extends \common\models\search\PropertySearch
{
    /**
     * @var string
     */
    public $address;
    /**
     * @var float
     */
    public $lat;
    /**
     * @var float
     */
    public $lng;

    /**
     * @param array $params
     * @return mixed
     */
    protected function initQuery($params = [])
    {
        $query = $this->_propertyRepository
            ->with(['translations' => function ($query) {
                $query->andOnCondition(['property_translations.locale' => [Yii::$app->language, 'en-GB', 'ru-RU']]);
            }, 'propertyAttributes', 'attachments', 'addressTranslations' => function ($query) {
                $query->andOnCondition(['property_address_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }, 'user.company.translations', 'category.translations'])
            ->groupBy('property.id');
        if (!empty($params['root']) && !empty($params['lft']) && !empty($params['rgt'])) {
            $query->joinWith(['category']);
        }

        return $query;
    }
}
