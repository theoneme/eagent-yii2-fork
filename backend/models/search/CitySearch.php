<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 23.08.2018
 * Time: 17:28
 */

namespace backend\models\search;

use common\models\search\CitySearch as BaseCitySearch;
use common\models\Translation;

/**
 * Class CitySearch
 * @package backend\models\search
 */
class CitySearch extends BaseCitySearch
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var string
     */
    public $region_title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['title', 'region_title'], 'string']
        ]);
    }

    /**
     * @param array $params
     * @return \common\interfaces\repositories\CityRepositoryInterface|\common\interfaces\RepositoryInterface
     */
    protected function initQuery($params = [])
    {
        $query = $this->_cityRepository
            ->groupBy('city.id');

        if (!empty($this->title)) {
            $query->joinWith(['translations']);
        } else {
            $query->with(['translations']);
        }

        if (!empty($this->region_title)) {
            $query->joinWith(['region.translations']);
        } else {
            $query->with(['region.translations']);
        }

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        if (!empty($this->title)) {
            $criteria = ['and', $criteria, ['like', 'city_translations.value', "{$this->title}%", false], ['city_translations.key' => Translation::KEY_TITLE]];
        }
        if (!empty($this->region_title)) {
            $criteria = ['and', $criteria, ['like', 'region_translations.value', "{$this->region_title}%", false], ['region_translations.key' => Translation::KEY_TITLE]];
        }

        return $criteria;
    }
}
