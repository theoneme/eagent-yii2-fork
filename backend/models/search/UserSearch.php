<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 24.08.2018
 * Time: 18:30
 */

namespace backend\models\search;

use common\models\search\UserSearch as BaseUserSearch;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class UserSearch
 * @package backend\models\search
 */
class UserSearch extends BaseUserSearch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => [self::TYPE_DEFAULT, self::TYPE_REALTOR]],
            [['username', 'phone', 'email'], 'string'],
            [['status', 'id'], 'integer'],
            ['is_company', 'boolean'],
        ];
    }

    /**
     * @return mixed
     */
    protected function initQuery()
    {
        $query = $this->_userRepository
            ->with(['translations', 'addressTranslations' => function (ActiveQuery $query) {
                return $query->andWhere(['user_address_translations.locale' => [Yii::$app->language, 'en-GB']]);
            }])
            ->groupBy('user.id');

        return $query;
    }

    /**
     * @param $criteria
     * @param $params
     * @return mixed
     */
    protected function improveCriteria($criteria, $params)
    {
        $username = ArrayHelper::remove($criteria, 'username');
        $email = ArrayHelper::remove($criteria, 'email');
        $phone = ArrayHelper::remove($criteria, 'phone');

        if ($username) {
            $criteria = ['and',
                ['like', 'username', $username],
                $criteria
            ];
        }

        if ($email) {
            $criteria = ['and',
                ['like', 'email', $email],
                $criteria
            ];
        }
        if ($phone) {
            $criteria = ['and',
                ['like', 'phone', $phone],
                $criteria
            ];
        }

        return $criteria;
    }
}
