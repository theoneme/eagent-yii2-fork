<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class PostFormAsset
 * @package backend\assets
 */
class FileInputAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/file-input-addons.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
