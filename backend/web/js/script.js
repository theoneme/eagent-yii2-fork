(function ($) {
    $.fn.EditModal = function (options) {
        var EditModalLoader = function (options, modalId) {
            this.options = $.extend({
                modalTriggerSelector: '[data-action=load-edit-modal]',
                modalFormSelector: '[data-role=edit-modal-form]',
            }, options);

            this.modal = $('#' + modalId);
            this.meta = {
                location: window.location.href,
                title: $('title'),
                description: $('meta[name=description]'),
                keywords: $('meta[name=keywords]')
            };

            this.attachHandlers();
        };

        EditModalLoader.prototype = {
            attachHandlers: function () {
                let that = this;

                $(document).on('click', that.options.modalTriggerSelector, function () {
                   that.modal
                        .find('.modal-body')
                        .load($(this).attr('href'))
                        .closest('.modal')
                        .modal('show')
                        .find('#edit-modal-header')
                        .html('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3>' + $(this).attr("title") + '</h3>');

                    return false;
                });

                $(document).on('submit', this.options.modalFormSelector, function () {
                    let self = $(this),
                        formData = new FormData($(this)[0]),
                        pjaxContainer = $(this).data('pj-container');

                    that.sendRequest($(this).attr('action'), 'post', formData, function(result) {
                        if (result.success === true) {
                            if(pjaxContainer != undefined && $('#' + pjaxContainer).length) {
                                $.pjax.reload({container: '#' + pjaxContainer});
                            }
                            self.closest('.modal').modal('hide');
                        }
                    });

                    return false;
                });
            },

            sendRequest: function (address, method, data, callback) {
                $.ajax({
                    url: address,
                    type: method,
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (callback) {
                            callback(result);
                        } else {

                        }
                    }
                });
            },
        };

        return new EditModalLoader(options, this.attr('id'));
    }
})(jQuery);