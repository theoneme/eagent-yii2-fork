function loadAttributes(url, categoryId, type, entityId) {
    $.get('$renderAttributesRoute', {
        category_id: categoryId,
        type: type,
        entity_id: entityId
    }, function(data) {
        $("#attributes-info").html(data);
    }, "html");
}

$("#categoryform-category_id, #propertyform-type").on("change", function(e) {
    let type = $('#propertyform-type').val();
    let categoryId = $('#categoryform-category_id').find('option:selected').val();
    let propertyId = $('#property-id').val();
    loadAttributes(categoryId, type, propertyId);
});