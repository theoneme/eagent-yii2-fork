let alertTypes = {
    'success': 'success',
    'info': 'info',
    'warning': 'warning',
    'error': 'danger',
    'danger': 'danger'
};

let alertIcons = {
    'success': 'check',
    'info': 'info',
    'warning': 'warning',
    'error': 'ban',
    'danger': 'ban'
};
function alertCall(type, text) {
    let alertContainer = $('section.content > .alert');
    if (alertContainer.length) {
        alertContainer.slideUp(400, function() {
            alertContainer.remove();
            alertCall(type, text);
        });
    }
    else {
        $('section.content').prepend(
            '<div class="alert-' + alertTypes[type] + ' alert fade in">' +
            '   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
            '   <i class="icon fa fa-' +  alertIcons[type] + '"></i>' + text +
            '</div>'
        );
        alertContainer.slideDown();
    }
}