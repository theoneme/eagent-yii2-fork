(function ($) {
    $.fn.GoogleAddressInput = function (options) {
        var Input = function (options, modalId) {
            this.options = $.extend({
                inputSelector: '#' + modalId,
                latSelector: '#gmaps-input-lat',
                longSelector: '#gmaps-input-lng',
                addressSelector: '#gmaps-input-address',
                language: 'ru',
                apiKey: null,
                withMap: false,
                mapSelector: '#google-map',
                startLat: 56.83200970,
                startLng: 60.59915200
            }, options);

            this.autocomplete = null;
            this.map = null;
            this.marker = null;
            this.geocoder = null;

            this.attachListeners();
        };

        Input.prototype = {
            attachListeners: function () {
                let that = this;
                // google.maps.event.addDomListener(window, 'load', function () {
                that.autocomplete = new google.maps.places.Autocomplete(
                    $(that.options.inputSelector).get(0), {
                        types: ['address']
                    }
                );
                that.geocoder = new google.maps.Geocoder;

                that.autocomplete.addListener('place_changed', $.proxy(that.fillAddress, that));
                if (that.options.withMap === true) {
                    let uluru = {lat: that.options.startLat, lng: that.options.startLng};
                    that.map = new google.maps.Map($(that.options.mapSelector)[0], {
                        zoom: 13,
                        center: uluru,
                        disableDefaultUI: true
                    });
                    that.marker = new google.maps.Marker({position: uluru, map: that.map});

                    google.maps.event.addListener(that.map, 'click', function (event) {
                        let result = {lat: event.latLng.lat(), lng: event.latLng.lng()};
                        that.marker.setPosition(result);

                        that.geocoder.geocode({'location': result}, function (results, status) {
                            if (status === 'OK') {
                                if (results[0]) {
                                    $(that.options.addressSelector).val(results[0].formatted_address);
                                }
                            } else {
                                $(that.options.addressSelector).val('');
                            }
                            $(that.options.latSelector).val(result.lat);
                            $(that.options.longSelector).val(result.lng);
                        });
                    });
                }
                // });
            },

            fillAddress: function () {
                let place = this.autocomplete.getPlace();
                let that = this;
                if (typeof place.address_components !== "undefined") {
                    let fullAddress = [];
                    for (let i = 0; i < place.address_components.length; i++) {
                        let addressPart = place.address_components[i].long_name;
                        if (addressPart.length) {
                            let addressType = place.address_components[i].types[0];
                            switch (addressType) {
                                case 'route':
                                    fullAddress[0] = addressPart;
                                    break;
                                case 'street_number':
                                    fullAddress[1] = addressPart;
                                    break;
                                case 'locality':
                                    fullAddress[2] = addressPart;
                                    break;
                                case 'administrative_area_level_1':
                                    fullAddress[3] = addressPart;
                                    break;
                                case 'country':
                                    fullAddress[4] = addressPart;
                                    break;
                            }
                        }
                    }
                    fullAddress = fullAddress.filter(String);
                    $(this.options.latSelector).val(place.geometry.location.lat());
                    $(this.options.longSelector).val(place.geometry.location.lng());
                    $(this.options.addressSelector).val(fullAddress.join(', ')).change();

                    if (that.options.withMap === true) {
                        let latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
                        that.marker.setPosition(latlng);
                        that.map.setCenter(that.marker.getPosition());
                        that.map.setZoom(13);
                    }
                } else {
                    $(this.options.latSelector).val('');
                    $(this.options.longSelector).val('');
                    $(this.options.addressSelector).val('').change();
                }
            }
        };

        return new Input(options, this.attr('id'));
    }

})(jQuery);