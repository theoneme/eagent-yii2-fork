<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\auth\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Class AuthController
 * @package backend\controllers
 */
class AuthController extends BackEndController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin()
    {
        $this->layout = 'main-login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        /** @var LoginForm $loginForm */
        $loginForm = Yii::createObject(LoginForm::class);
        $loginForm->withRoles = true;
        $input = Yii::$app->request->post();
        $loginForm->load($input);

        if ($loginForm->validate() && $loginForm->login()) {
            return $this->goBack();
        }

        return $this->render('login', [
            'model' => $loginForm,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
