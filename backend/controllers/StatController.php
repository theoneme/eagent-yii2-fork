<?php
/**
 * Created by PhpStorm.
 * User: ПК
 * Date: 21.02.2019
 * Time: 15:34
 */

namespace backend\controllers;

use common\controllers\BackEndController;
use common\models\Property;
use common\models\Request;
use common\repositories\sql\PropertyRepository;
use common\repositories\sql\RequestRepository;
use common\repositories\sql\UserRepository;
use Yii;
use yii\base\Module;
use yii\data\ArrayDataProvider;

/**
 * Class StatController
 * @package backend\controllers
 */
class StatController extends BackEndController
{
    /**
     * @var UserRepository
     */
    private $_userRepository;
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var RequestRepository
     */
    private $_requestRepository;

    /**
     * StatController constructor.
     * @param string $id
     * @param Module $module
     * @param RequestRepository $requestRepository
     * @param UserRepository $userRepository
     * @param PropertyRepository $propertyRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, RequestRepository $requestRepository, UserRepository $userRepository, PropertyRepository $propertyRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_userRepository = $userRepository;
        $this->_propertyRepository = $propertyRepository;
        $this->_requestRepository = $requestRepository;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = [];
        $now = new \DateTime('now');
        $month = Yii::$app->request->get('month') ?: $now->format('m');
        $year = Yii::$app->request->get('year') ? Yii::$app->request->get('year') : $now->format('Y');
        $day = ((Yii::$app->request->get('month') == $now->format('m')
                && Yii::$app->request->get('year') == $now->format('Y'))
            || Yii::$app->request->get('month') == null)
            ? $now->format('d')
            : cal_days_in_month(CAL_GREGORIAN, $month, $year);

        for ($i = 1; $i < $day + 1; $i++) {
            $currentDayUsers = $this->_userRepository->select(['id'], true)->countByCriteria([
                'and',
                [
                    'between',
                    'created_at',
                    strtotime("{$i}-{$month}-{$year} 00:00:00"),
                    strtotime("{$i}-{$month}-{$year} 23:59:59")
                ],
                ['is_company' => false]
            ], [], true);

            $currentDayParsedUsers = $this->_userRepository->select(['user.id'], true)->joinWith(['import'], false, 'INNER JOIN')
                ->countByCriteria([
                    'and',
                    [
                        'between',
                        'created_at',
                        strtotime("{$i}-{$month}-{$year} 00:00:00"),
                        strtotime("{$i}-{$month}-{$year} 23:59:59")
                    ],
                    ['is_company' => false]
                ], [], true);

            $currentDayProperties = $this->_propertyRepository->select(['id'], true)->countByCriteria([
                'between',
                'created_at',
                strtotime("{$i}-{$month}-{$year} 00:00:00"),
                strtotime("{$i}-{$month}-{$year} 23:59:59")
            ], [], true);

            $currentDayParsedProperties = $this->_propertyRepository->select(['property.id'], true)->joinWith(['import'], false, 'INNER JOIN')
                ->countByCriteria([
                    'and',
                    [
                        'between',
                        'created_at',
                        strtotime("{$i}-{$month}-{$year} 00:00:00"),
                        strtotime("{$i}-{$month}-{$year} 23:59:59")
                    ],
                ], [], true);

            $currentDaySaleProperties = $this->_propertyRepository->select(['id'], true)->countByCriteria([
                'and',
                [
                    'between',
                    'created_at',
                    strtotime("{$i}-{$month}-{$year} 00:00:00"),
                    strtotime("{$i}-{$month}-{$year} 23:59:59")
                ],
                ['type' => Property::TYPE_SALE]
            ], [], true);

            $currentDayRentProperties = $this->_propertyRepository->select(['id'], true)->countByCriteria([
                'and',
                [
                    'between',
                    'created_at',
                    strtotime("{$i}-{$month}-{$year} 00:00:00"),
                    strtotime("{$i}-{$month}-{$year} 23:59:59")
                ],
                ['type' => Property::TYPE_RENT]
            ], [], true);

            $currentDaySaleRequests = $this->_requestRepository->select(['id'], true)->countByCriteria([
                'and',
                [
                    'between',
                    'created_at',
                    strtotime("{$i}-{$month}-{$year} 00:00:00"),
                    strtotime("{$i}-{$month}-{$year} 23:59:59")
                ],
                ['type' => Request::TYPE_SALE]
            ], [], true);

            $currentDayRentRequests = $this->_requestRepository->select(['id'], true)->countByCriteria([
                'and',
                [
                    'between',
                    'created_at',
                    strtotime("{$i}-{$month}-{$year} 00:00:00"),
                    strtotime("{$i}-{$month}-{$year} 23:59:59")
                ],
                ['type' => Request::TYPE_RENT]
            ], [], true);

            $data[] = [
                'id' => $i,
                'currentDayUsers' => $currentDayUsers,
                'currentDayHandUsers' => $currentDayUsers - $currentDayParsedUsers,
                'currentDayProperties' => $currentDayProperties,
                'currentDayHandProperties' => $currentDayProperties - $currentDayParsedProperties,
                'currentDaySaleProperties' => $currentDaySaleProperties,
                'currentDayRentProperties' => $currentDayRentProperties,
                'currentDaySaleRequests' => $currentDaySaleRequests,
                'currentDayRentRequests' => $currentDayRentRequests,
            ];
        }
//
        $data = array_reverse($data);

        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => ['id'],
            ],
            'pagination' => [
                'pageSize' => 35,
            ],
        ]);

        return $this->render('index', [
            'provider' => $provider,
            'month' => $month,
            'year' => $year
        ]);
    }
}
