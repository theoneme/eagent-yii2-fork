<?php

namespace backend\controllers;

use backend\forms\ar\BuildingForm;
use backend\models\search\BuildingSearch;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\helpers\FormHelper;
use common\helpers\UtilityHelper;
use common\mappers\AddressTranslationsMapper;
use common\mappers\TranslationsMapper;
use common\models\Building;
use common\models\Translation;
use common\repositories\sql\BuildingRepository;
use common\services\elasticsearch\BuildingElasticConverter;
use common\services\entities\BuildingService;
use common\services\seo\BuildingSeoService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class BuildingController
 * @package backend\controllers
 */
class BuildingController extends BackEndController
{
    /**
     * @var BuildingRepository
     */
    private $_buildingRepository;
    /**
     * @var BuildingService
     */
    private $_buildingService;

    /**
     * BuildingController constructor.
     * @param string $id
     * @param Module $module
     * @param BuildingService $buildingService
     * @param BuildingRepository $buildingRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, BuildingService $buildingService, BuildingRepository $buildingRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_buildingRepository = $buildingRepository;
        $this->_buildingService = $buildingService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'attributes', 'delete', 'generate'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new BuildingSearch($this->_buildingRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $queryParams = Yii::$app->request->queryParams;
        $params = $this->extractParams($queryParams);
        $properties = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $properties['items'],
            'totalCount' => $properties['pagination']->totalCount ?? count($properties['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        $search->address = $params['address'] ?? null;
        $search->lat = $params['lat'] ?? null;
        $search->lng = $params['lng'] ?? null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionSecondary()
    {
        $search = new BuildingSearch($this->_buildingRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $params = Yii::$app->request->queryParams;
        $params['type'] = Building::TYPE_DEFAULT;
        $buildings = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $buildings['items'],
            'totalCount' => $buildings['pagination']->totalCount ?? count($buildings['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionNew()
    {
        $search = new BuildingSearch($this->_buildingRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $params = Yii::$app->request->queryParams;
        $params['type'] = Building::TYPE_NEW_CONSTRUCTION;
        $buildings = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $buildings['items'],
            'totalCount' => $buildings['pagination']->totalCount ?? count($buildings['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $buildingForm = new BuildingForm();
        $building = $this->_buildingRepository->findOneByCriteria(['building.id' => $id]);
        $buildingForm->_building = $building;
        $buildingDTO = $this->_buildingService->getOne(['building.id' => $id]);
        $buildingDTO['type'] = Yii::$app->request->get('type', $buildingDTO['type']);
        $buildingForm->prepareUpdate($buildingDTO);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $buildingForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($buildingForm);
            }

            if ($buildingForm->validate()) {
                $buildingForm->bindData();
                $buildingForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_BUILDING, $buildingForm->type);
        $buildingForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'buildingForm' => $buildingForm,
            'action' => ['/building/update', 'id' => $id]
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $buildingForm = new BuildingForm();
        $buildingForm->_building = new Building();
        $input = Yii::$app->request->post();
        $buildingForm->load($input);

        if (!empty($input)) {
            $buildingForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($buildingForm);
            }

            if ($buildingForm->validate()) {
                $buildingForm->bindData();
                $result = $buildingForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => $result];
            }
        } else {
            $buildingForm->load([
                'BuildingForm' => [
                    'type' => Yii::$app->request->get('type')
                        ? (int)Yii::$app->request->get('type')
                        : Building::TYPE_DEFAULT
                ]
            ]); // проинициализировать DynamicForm
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_BUILDING, $buildingForm->type);
        $buildingForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'buildingForm' => $buildingForm,
            'action' => ['/building/create']
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var Building $building */
        $building = $this->_buildingRepository->findOneByCriteria(['building.id' => $id]);

        if ($building !== null) {
            $building->updateAttributes(['status' => Building::STATUS_DELETED, 'updated_at' => time()]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionGenerate($id)
    {
        /** @var Building $building */
        $building = $this->_buildingRepository->findOneByCriteria(['building.id' => $id]);

        if ($building !== null) {
            $translations = TranslationsMapper::getMappedData($building->translations);
            $addressTranslation = AddressTranslationsMapper::getMappedData($building->addressTranslations);
            $seoParams = ArrayHelper::map($building->buildingAttributes, 'entity_alias', 'value_alias');
            $seoParams['address'] = $addressTranslation['title'] ?? '';
            $seoParams['title'] = $translations[Translation::KEY_NAME] ?? null;

            $seoService = Yii::$container->get(BuildingSeoService::class);
            $seo = $seoService->getSeo(BuildingSeoService::TEMPLATE_BUILDING, $seoParams, $building->locale);
            if (!empty($addressTranslation) && !empty($seo['heading'])) {
                Translation::deleteAll(['key' => Translation::KEY_TITLE, 'entity' => Translation::ENTITY_BUILDING, 'entity_id' => $building['id']]);
                $titleTranslation = new Translation([
                    'key' => Translation::KEY_TITLE,
                    'entity' => Translation::ENTITY_BUILDING,
                    'entity_id' => $building->id,
                    'locale' => $building->locale,
                    'value' => $seo['heading']
                ]);
                if ($titleTranslation->save()) {
                    $newSlug = UtilityHelper::generateSlug($seo['heading'], 7, 75, $building->getPrimaryKey());
                    $building->updateAttributes([
                        'slug' => $newSlug,
                        'updated_at' => time()
                    ]);

                    $building = $this->_buildingRepository->findOneByCriteria(['building.id' => $id]);

                    $converter = new BuildingElasticConverter($building);
                    $converter->process(true);
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $queryParams
     * @return array
     */
    protected function extractParams($queryParams)
    {
        $params = $queryParams;

        if (array_key_exists('BuildingSearch', $params)) {
            $propertySearch = $params['BuildingSearch'];
            unset($params['BuildingSearch']);

            $params = array_filter(array_merge($params, $propertySearch));
        }

        return $params;
    }
}
