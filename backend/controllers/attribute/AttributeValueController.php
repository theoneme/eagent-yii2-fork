<?php

namespace backend\controllers\attribute;

use backend\models\MergeAttributeValueForm;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\AttributeValueForm;
use common\models\AttributeValue;
use common\models\search\AttributeValueSearch;
use common\repositories\sql\AttributeValueRepository;
use common\repositories\sql\BuildingAttributeRepository;
use common\repositories\sql\PropertyAttributeRepository;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AttributeValueController
 * @package backend\controllers
 */
class AttributeValueController extends BackEndController
{
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var AttributeValueRepository
     */
    private $_attributeValueRepository;
    /**
     * @var PropertyAttributeRepository
     */
    protected $_propertyAttributeRepository;
    /**
     * @var BuildingAttributeRepository
     */
    protected $_buildingAttributeRepository;

    /**
     * AttributeValueController constructor.
     * @param string $id
     * @param Module $module
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeValueRepository $attributeValueRepository
     * @param PropertyAttributeRepository $propertyAttributeRepository
     * @param BuildingAttributeRepository $buildingAttributeRepository
     * @param array $config
     */
    public function __construct(
        string $id,
        Module $module,
        AttributeService $attributeService,
        AttributeValueService $attributeValueService,
        AttributeValueRepository $attributeValueRepository,
        PropertyAttributeRepository $propertyAttributeRepository,
        BuildingAttributeRepository $buildingAttributeRepository,
        array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_attributeValueService = $attributeValueService;
        $this->_attributeService = $attributeService;
        $this->_attributeValueRepository = $attributeValueRepository;
        $this->_propertyAttributeRepository = $propertyAttributeRepository;
        $this->_buildingAttributeRepository = $buildingAttributeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete', 'merge'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['merge'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new AttributeValueSearch($this->_attributeValueRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search(Yii::$app->request->get());

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        $attribute = !empty($search->attribute_id) ? $this->_attributeService->getOne(['attribute.id' => $search->attribute_id]) : null;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
            'attribute' => $attribute,
        ]);
    }

    /**
     * @param $attributeId
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate($attributeId)
    {
        $form = new AttributeValueForm(['_attributeValue' => new AttributeValue()]);
        $form->attribute_id = $attributeId;
        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($form);
            }

            if ($form->validate()) {
                $form->bindData();
                $form->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $form,
            'action' => ['/attribute/attribute-value/create', 'attributeId' => $attributeId]
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $form = new AttributeValueForm();
        /* @var AttributeValue $model */
        $model = $this->_attributeValueRepository->findOneByCriteria(['attribute_value.id' => $id]);
        $form->_attributeValue = $model;
        $dto = $this->_attributeValueService->getOne(['attribute_value.id' => $id]);
        $form->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($form);
            }

            if ($form->validate()) {
                $form->bindData();
                $form->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $form,
            'action' => ['/attribute/attribute-value/update', 'id' => $id],
            'id' => $id,
            'mergeModel' => new MergeAttributeValueForm()
        ]);
    }


    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_attributeValueService->delete(['attribute_value.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return array|mixed
     */
    public function actionMerge()
    {
        $model = new MergeAttributeValueForm();
        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $model->load($input);
            if (array_key_exists('ajax', $input)) {
                return ActiveForm::validate($model);
            }

            if ($model->save()) {
                return ['success' => true];
            }
        }

        return ['success' => false];
    }
}
