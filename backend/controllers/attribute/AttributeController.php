<?php

namespace backend\controllers\attribute;

use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\AttributeForm;
use common\models\Attribute;
use common\models\AttributeValue;
use common\models\search\AttributeSearch;
use common\repositories\sql\AttributeRepository;
use common\services\entities\AttributeService;
use common\services\entities\AttributeValueService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class AttributeValueController
 * @package backend\controllers
 */
class AttributeController extends BackEndController
{
    /**
     * @var AttributeValueService
     */
    private $_attributeValueService;
    /**
     * @var AttributeService
     */
    private $_attributeService;
    /**
     * @var AttributeValueService
     */
    private $_attributeRepository;

    /**
     * AttributeController constructor.
     * @param string $id
     * @param Module $module
     * @param AttributeService $attributeService
     * @param AttributeValueService $attributeValueService
     * @param AttributeRepository $attributeRepository
     * @param array $config
     */
    public function __construct(
        string $id,
        Module $module,
        AttributeService $attributeService,
        AttributeValueService $attributeValueService,
        AttributeRepository $attributeRepository,
        array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_attributeValueService = $attributeValueService;
        $this->_attributeService = $attributeService;
        $this->_attributeRepository = $attributeRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'moderation', 'update', 'create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new AttributeSearch($this->_attributeRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search(Yii::$app->request->get());

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionModeration()
    {
        $search = new AttributeSearch($this->_attributeRepository, [
            'pagination' => true,
            'perPage' => 5,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search(Yii::$app->request->get());

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 5
            ]
        ]);

        $values = $this->_attributeValueService->getMany(['attribute_id' => array_column($items['items'], 'id'), 'status' => AttributeValue::STATUS_PENDING]);
        $values = ArrayHelper::map($values['items'], 'id', function ($var) {
            return $var;
        }, 'attribute_id');

        return $this->render('moderation', [
            'dataProvider' => $dataProvider,
            'search' => $search,
            'values' => $values,
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $form = new AttributeForm(['_attribute' => new Attribute()]);
        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($form);
            }

            if ($form->validate()) {
                $form->bindData();
                $form->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $form,
            'action' => ['/attribute/attribute/create']
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $form = new AttributeForm();
        $model = $this->_attributeRepository->findOneByCriteria(['attribute.id' => $id]);
        $form->_attribute = $model;
        $dto = $this->_attributeService->getOne(['attribute.id' => $id]);
        $form->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($form);
            }

            if ($form->validate()) {
                $form->bindData();
                $form->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $form,
            'action' => ['/attribute/attribute/update', 'id' => $id]
        ]);
    }
}
