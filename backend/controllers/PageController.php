<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\PageForm;
use common\models\Page;
use common\models\search\PageSearch;
use common\repositories\sql\PageRepository;
use common\services\entities\PageService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class PageController
 * @package backend\controllers\location
 */
class PageController extends BackEndController
{
    /**
     * @var PageRepository
     */
    private $_pageRepository;
    /**
     * @var PageService
     */
    private $_pageService;

    /**
     * PageController constructor.
     * @param string $id
     * @param Module $module
     * @param PageService $pageService
     * @param PageRepository $pageRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, PageService $pageService, PageRepository $pageRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_pageRepository = $pageRepository;
        $this->_pageService = $pageService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new PageSearch($this->_pageRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $pageForm = new PageForm();
        $page = $this->_pageRepository->findOneByCriteria(['page.id' => $id]);
        $pageForm->_page = $page;
        $dto = $this->_pageService->getOne(['page.id' => $id]);

        $pageForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $pageForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($pageForm);
            }

            if ($pageForm->validate()) {
                $pageForm->bindData();
                $pageForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'pageForm' => $pageForm,
            'metaForm' => new MetaForm(),
            'dto' => $dto,
            'action' => ['/page/update', 'id' => $id]
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $pageForm = new PageForm(['_page' => new Page()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $pageForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($pageForm);
            }

            if ($pageForm->validate()) {
                $pageForm->bindData();
                $pageForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'pageForm' => $pageForm,
            'metaForm' => new MetaForm(),
            'action' => ['/page/create']
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_pageService->delete(['page.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }
}
