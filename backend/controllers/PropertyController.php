<?php

namespace backend\controllers;

use backend\forms\ar\BuildingForm;
use backend\forms\ar\PropertyForm;
use backend\models\search\PropertySearch;
use backend\services\FormCategoryService;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\factories\DynamicFormFactory;
use common\helpers\DataHelper;
use common\helpers\FormHelper;
use common\helpers\UtilityHelper;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\mappers\AddressTranslationsMapper;
use common\mappers\DynamicFormValuesMapper;
use common\mappers\TranslationsMapper;
use common\models\AttributeGroup;
use common\models\Building;
use common\models\Property;
use common\models\Translation;
use common\repositories\sql\PropertyRepository;
use common\services\BuildingForPropertyService;
use common\services\elasticsearch\PropertyElasticConverter;
use common\services\entities\BuildingService;
use common\services\entities\CityService;
use common\services\entities\PropertyService;
use common\services\seo\PropertySeoService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;

/**
 * Class PropertyController
 * @package backend\controllers
 */
class PropertyController extends BackEndController
{
    /**
     * @var PropertyRepository
     */
    private $_propertyRepository;
    /**
     * @var PropertyService
     */
    private $_propertyService;
    /**
     * @var BuildingService
     */
    private $_buildingService;
    /**
     * @var FormCategoryService
     */
    private $_formCategoryService;
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;
    /**
     * @var BuildingForPropertyService
     */
    private $_buildingForPropertyService;

    /**
     * PropertyController constructor.
     * @param string $id
     * @param Module $module
     * @param BuildingService $buildingService
     * @param PropertyRepository $propertyRepository
     * @param PropertyService $propertyService
     * @param FormCategoryService $formCategoryService
     * @param CategoryRepositoryInterface $categoryRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                BuildingService $buildingService,
                                PropertyRepository $propertyRepository,
                                PropertyService $propertyService,
                                FormCategoryService $formCategoryService,
                                CategoryRepositoryInterface $categoryRepository,
                                BuildingForPropertyService $buildingForPropertyService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_propertyRepository = $propertyRepository;
        $this->_propertyService = $propertyService;
        $this->_buildingService = $buildingService;
        $this->_categoryRepository = $categoryRepository;
        $this->_formCategoryService = $formCategoryService;
        $this->_buildingForPropertyService = $buildingForPropertyService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'attributes', 'delete', 'generate'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $search = new PropertySearch($this->_propertyRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['id' => SORT_DESC]
        ]);

        $queryParams = Yii::$app->request->queryParams;
        $params = $this->extractParams($queryParams);
        $properties = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $properties['items'],
            'totalCount' => $properties['pagination']->totalCount ?? count($properties['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        $search->address = $params['address'] ?? null;
        $search->lat = $params['lat'] ?? null;
        $search->lng = $params['lng'] ?? null;
        $search->category_id = $queryParams['PropertySearch']['category_id'] ?? null;

        $categories = DataHelper::getCategoryTree();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search,
            'categories' => $categories
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $buildingForm = new BuildingForm();
        $property = $this->_propertyRepository->findOneByCriteria(['property.id' => $id]);
        $propertyForm = new PropertyForm(['_property' => $property]);
        $propertyDTO = $this->_propertyService->getOne(['property.id' => $id]);
        $propertyDTO['type'] = Yii::$app->request->get('type', $propertyDTO['type']);
        $propertyDTO['category_id'] = Yii::$app->request->get('category_id', $propertyDTO['category_id']);
        $propertyForm->prepareUpdate($propertyDTO);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $propertyForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($propertyForm);
            }

            if ($propertyForm->validate()) {
                $propertyForm->bindData();
                $propertyForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        $categoryData = $this->_formCategoryService->getData($propertyForm->category->category_id);

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_PROPERTY, $propertyForm->category->category_id);
        unset($formConfig[5]['config'][1]);
        $propertyForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'propertyForm' => $propertyForm,
            'buildingForm' => $buildingForm,
            'propertyData' => $propertyDTO,
            'categories' => DataHelper::getCategories(),
            'subcategories' => $categoryData['subcategories'],
            'parentCategoryId' => $categoryData['parentCategoryId'],
            'action' => ['/property/update', 'id' => $id]
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $propertyForm = new PropertyForm(['_property' => new Property()]);
        $buildingForm = new BuildingForm();

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $propertyForm->load($input);

            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($propertyForm);
            }

            $hasToManipulateBuilding = $this->_buildingForPropertyService->hasToManipulateBuilding($propertyForm->category->category_id);

            $success = $propertyForm->validate();
            if ($success && $hasToManipulateBuilding === true) {
                $buildingForm = $this->_buildingForPropertyService->initForm($buildingForm, $propertyForm);
                if ($buildingForm->_building->isNewRecord === false) {
                    $buildingForm->type = Building::TYPE_DEFAULT;
                    $buildingForm->locale = $propertyForm->locale;
                    $buildingForm->load($input);
                    if ($buildingForm->validate()) {
                        $buildingForm->bindData();
                        if ($success = ($success && $buildingForm->save())) {
                            $propertyForm->_property->building_id = $buildingForm->_building->id;
                        }
                    }
                } else {
                    $propertyForm->_property->building_id = $buildingForm->_building->id;
                }
            }

            if ($success) {
                $propertyForm->bindData();
                if ($propertyForm->save()) {
                    $slug = TranslationsMapper::getMappedData($propertyForm->_property['translations'])['slug'];
                    return $this->redirect(['/property/property/show', 'slug' => $slug]);
                }
            }
        } else {
            $propertyForm->load([
                'CategoryForm' => [
                    'category_id' => (int)Yii::$app->request->get('category_id', 2)
                ],
                'PropertyForm' => [
                    'type' => (int)Yii::$app->request->get('type', Property::TYPE_SALE)
                ]
            ]); // проинициализировать DynamicForm
        }

        $categoryData = $this->_formCategoryService->getData($propertyForm->category->category_id);

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_PROPERTY, $propertyForm->category->category_id);
        $propertyForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'propertyForm' => $propertyForm,
            'buildingForm' => $buildingForm,
            'categories' => DataHelper::getCategories(),
            'subcategories' => $categoryData['subcategories'],
            'parentCategoryId' => $categoryData['parentCategoryId'],
            'action' => ['/property/create']
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var Property $property */
        $property = $this->_propertyRepository->findOneByCriteria(['property.id' => $id]);

        if ($property !== null) {
            $property->updateAttributes(['status' => Property::STATUS_DELETED, 'updated_at' => time()]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $category_id
     * @param $type
     * @param null $entity_id
     * @return string
     */
    public function actionAttributes($category_id, $type, $entity_id = null)
    {
        $dynamicForm = DynamicFormFactory::initByCategory(AttributeGroup::ENTITY_PROPERTY_CATEGORY, $category_id, $type);

        if (!empty($entity_id) && !empty($dynamicForm->attributes)) {
            $property = $this->_propertyService->getOne(['property.id' => $entity_id]);
            $dynamicForm->load(DynamicFormValuesMapper::getMappedData($property['attributes'], $dynamicForm->config), '');
        }

        return $this->renderAjax('partial/attributes', [
            'dynamicForm' => $dynamicForm
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionGenerate($id)
    {
        /** @var Property $property */
        $property = $this->_propertyRepository->findOneByCriteria(['property.id' => $id]);

        if ($property !== null) {
            $locales = Yii::$app->params['supportedLocales'];
            $seoService = Yii::$container->get(PropertySeoService::class);
            $success = true;

            $categoryTranslations = TranslationsMapper::getMappedData($property->category->translations, TranslationsMapper::MODE_FULL);
            $addressTranslations = AddressTranslationsMapper::getMappedData($property->addressTranslations, AddressTranslationsMapper::MODE_FULL);

            $seoParams = ArrayHelper::map($property->propertyAttributes, 'entity_alias', 'value_alias');
            $seoParams['type'] = $property['type'];
            foreach ($locales as $locale => $code) {
                $currentCategoryTranslation = array_key_exists($locale, $categoryTranslations)
                    ? $categoryTranslations[$locale]
                    : array_pop($categoryTranslations);
                if (count($addressTranslations) > 0) {
                    $addressTranslation = array_key_exists($locale, $addressTranslations)
                        ? $addressTranslations[$locale]
                        : array_values($addressTranslations)[0];
                } else {
                    $addressTranslation = [];
                }
                $seoParams['address'] = $addressTranslation['title'] ?? '';
                if ($property->locale === $locale) {
                    $seoParams['saddress'] = $addressTranslation['title'] ?? '';
                } else {
                    if (!empty($property->city_id)) {
                        /* @var CityService $cityService */
                        $cityService = Yii::$container->get(CityService::class);
                        $city = $cityService->getOne(['id' => $property->city_id]);
                        if ($city) {
                            $currentCityTranslation = array_key_exists($locale, $city['translations'])
                                ? $city['translations'][$locale]
                                : array_pop($city['translations']);
                            $seoParams['saddress'] = $currentCityTranslation['title'];
                        }
                    }
                }
                $seoParams['city'] = $addressTranslation['data']['city'] ?? '';
                $seoParams['category'] = $currentCategoryTranslation[Translation::KEY_ADDITIONAL_TITLE] ?? $currentCategoryTranslation[Translation::KEY_TITLE] ?? '';
                $seo = $seoService->getSeo($property->category->customDataArray['seoTemplate'] ?? null, $seoParams, $locale);

                if (!empty($seo['heading'])) {
                    Translation::deleteAll([
                        'key' => [Translation::KEY_TITLE, Translation::KEY_SLUG],
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property['id'],
                        'locale' => $locale
                    ]);

                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_TITLE,
                        'value' => $seo['heading'],
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);

                    $success = $success && $translation->save();

                    $translation = new Translation([
                        'locale' => $locale,
                        'key' => Translation::KEY_SLUG,
                        'value' => UtilityHelper::generateSlug($seo['heading'], 9, 100, $property->getPrimaryKey()),
                        'entity' => Translation::ENTITY_PROPERTY,
                        'entity_id' => $property->id
                    ]);
                    $success = $success && $translation->save();
                }
            }

            if ($success) {
                $property = $this->_propertyRepository->findOneByCriteria(['property.id' => $id]);
                $converter = new PropertyElasticConverter($property);
                $converter->process(true);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $queryParams
     * @return array
     */
    protected function extractParams($queryParams)
    {
        $params = $queryParams;

        if (array_key_exists('PropertySearch', $params)) {
            $propertySearch = $params['PropertySearch'];
            unset($params['PropertySearch']);

            $params = array_filter(array_merge($params, $propertySearch));
        }

        if (array_key_exists('category_id', $params)) {
            $category = $this->_categoryRepository->findOneByCriteria(['id' => $params['category_id']]);

            unset($params['category_id']);
            $params['lft'] = $category['lft'];
            $params['rgt'] = $category['rgt'];
            $params['root'] = $category['root'];
        }

        return $params;
    }
}
