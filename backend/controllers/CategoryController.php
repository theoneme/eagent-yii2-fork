<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\forms\ar\CategoryForm;
use common\models\Category;
use common\repositories\sql\CategoryRepository;
use common\services\entities\CategoryService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * Class CategoryController
 * @package backend\controllers
 */
class CategoryController extends BackEndController
{
    /**
     * @var CategoryRepository
     */
    private $_categoryRepository;
    /**
     * @var CategoryService
     */
    private $_categoryService;

    /**
     * CategoryController constructor.
     * @param string $id
     * @param Module $module
     * @param CategoryService $categoryService
     * @param CategoryRepository $categoryRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, CategoryService $categoryService, CategoryRepository $categoryRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_categoryRepository = $categoryRepository;
        $this->_categoryService = $categoryService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'item', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $categories = $this->_categoryRepository->joinWith(['translations'])->findManyByCriteria(['lvl' => 0]);

        return $this->render('index', ['models' => $categories]);
    }

    /**
     * Creates or updates WizardItem model.
     * @param null $id
     * @param null $parent_id
     * @return mixed
     */
    public function actionItem($id = null, $parent_id = null)
    {
        if ($id) {
            $form = new CategoryForm();
            $form->_category = $this->_categoryRepository->findOneByCriteria(['category.id' => $id]);
            $dto = $this->_categoryService->getOne(['category.id' => $id]);
            $form->prepareUpdate($dto);
        }
        else {
            $form = new CategoryForm(['_category' => new Category(), 'parent_id' => $parent_id]);
        }

        if ($form->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (array_key_exists('ajax', Yii::$app->request->post())) {
                return $this->validateAjax($form);
            }
//            $errors = $form->getAjaxErrors();
//            if (empty($errors)) {
                $form->bindData();
                return ['success' => $form->save(), 'id' => 'item-' . $form->_category->id];
//            } else {
//                return ['success' => false, 'errors' => $errors];
//            }
        }
        return $this->renderAjax('form', ['model' => $form, 'create' => $form->_category->isNewRecord]);
    }

    /**
     * @param $id
     */
    public function actionDelete($id)
    {
        Category::findOne($id)->deleteWithChildren();
    }
}
