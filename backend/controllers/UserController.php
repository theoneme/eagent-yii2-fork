<?php

namespace backend\controllers;

use backend\forms\ar\UserForm;
use backend\models\search\UserSearch;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\dto\UserDTO;
use common\forms\ar\composite\ContactForm;
use common\helpers\FormHelper;
use common\models\User;
use common\repositories\sql\UserRepository;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserController
 * @package backend\controllers
 */
class UserController extends BackEndController
{
    /**
     * @var UserRepository
     */
    private $_userRepository;

    /**
     * UserController constructor.
     * @param string $id
     * @param Module $module
     * @param UserRepository $userRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserRepository $userRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'render-contact'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-contact'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['render-contact'],
            ]
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function actionIndex()
    {
        $params = array_merge(Yii::$app->request->queryParams, ['is_company' => false]);
        $search = new UserSearch($this->_userRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('status asc, id desc')
        ]);
        $users = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $users['items'],
            'totalCount' => $users['pagination']->totalCount,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $this->layout = 'catalog';

        $userForm = new UserForm();
        /** @var User $user */
        $user = $this->_userRepository->findOneByCriteria(['user.id' => $id]);
        $userForm->_user = $user;
        $userDTO = new UserDTO($user);
        $userData = $userDTO->getData();

        $userForm->prepareUpdate($userData);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $userForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($userForm);
            }

            if ($userForm->validate()) {
                $userForm->bindData();
                $userForm->save();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }

                return $this->goBack();
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_USER);
        $userForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'userForm' => $userForm,
            'action' => ['/user/update', 'id' => $id],
            'userDTO' => $userData
        ]);
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderContact($iterator)
    {
        $contactForm = new ContactForm();

        return [
            'html' => $this->renderAjax('partial/contact', [
                'contactForm' => $contactForm,
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }
}
