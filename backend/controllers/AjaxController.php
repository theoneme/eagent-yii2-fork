<?php

namespace backend\controllers;

use backend\services\OsmPolygonService;
use common\controllers\BackEndController;
use common\forms\ar\composite\BuildingDocumentForm;
use common\forms\ar\composite\BuildingPhaseForm;
use common\forms\ar\composite\BuildingProgressForm;
use common\forms\ar\composite\BuildingSiteForm;
use common\mappers\forms\BuildingToPropertyFormMapper;
use common\mappers\SelectizeAttributeValuesMapper;
use common\mappers\SelectizeCountriesMapper;
use common\mappers\SelectizeUserMapper;
use common\services\entities\AttributeValueService;
use common\services\entities\BuildingService;
use common\services\entities\CityService;
use common\services\entities\CountryService;
use common\services\entities\DistrictService;
use common\services\entities\RegionService;
use common\services\entities\UserService;
use http\Exception;
use Yii;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\httpclient\Client;
use yii\web\Response;

/**
 * Class AjaxController
 * @package backend\controllers
 */
class AjaxController extends BackEndController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => ContentNegotiator::class,
                'only' => ['building', 'attribute-values', 'countries', 'regions', 'cities', 'districts', 'users', 'render-building-phase', 'render-building-progress', 'render-building-site', 'render-building-document', 'polygon-import'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['building', 'attribute-values', 'countries', 'regions', 'cities', 'districts', 'users', 'render-building-phase', 'render-building-progress', 'render-building-site', 'render-building-document', 'polygon-import'],
            ]
        ]);
    }

    /**
     * @param $lat
     * @param $lng
     * @return array
     */
    public function actionBuilding($lat, $lng)
    {
        /** @var BuildingService $buildingService */
        $buildingService = Yii::$container->get(BuildingService::class);

        $lat = round($lat, 8);
        $lng = round($lng, 8);
        $building = $buildingService->getOne(['building.lat' => $lat, 'building.lng' => $lng]);
        if ($building !== null) {
            $buildingData = BuildingToPropertyFormMapper::getMappedData($building);
            return [
                'success' => true,
                'data' => $buildingData
            ];
        }

        return ['success' => false];
    }

    /**
     * @param $attribute
     * @param $query
     * @param null $exceptId
     * @return array
     */
    public function actionAttributeValues($attribute, $query, $exceptId = null)
    {
        /** @var AttributeValueService $attributeValueService */
        $attributeValueService = Yii::$container->get(AttributeValueService::class);

        $values = $attributeValueService->getMany(['title' => $query, 'attribute_id' => $attribute, 'except_id' => $exceptId]);
        if (!empty($values['items'])) {
            return SelectizeAttributeValuesMapper::getMappedData($values['items']);
        }

        return [];
    }

    /**
     * @param $query
     * @return array
     */
    public function actionCountries($query)
    {
        /** @var CountryService $countryService */
        $countryService = Yii::$container->get(CountryService::class);

        $countries = $countryService->getMany(['request' => urldecode($query)]);

        if (!empty($countries['items'])) {
            return SelectizeCountriesMapper::getMappedData($countries['items']);
        }

        return [];
    }

    /**
     * @param $query
     * @return array
     */
    public function actionRegions($query)
    {
        /** @var RegionService $regionService */
        $regionService = Yii::$container->get(RegionService::class);

        $regions = $regionService->getMany(['request' => urldecode($query)]);

        if (!empty($regions['items'])) {
            return SelectizeCountriesMapper::getMappedData($regions['items']);
        }

        return [];
    }

    /**
     * @param $query
     * @return array
     */
    public function actionCities($query)
    {
        if (!empty($query) && mb_strlen($query) > 2) {
            /** @var CityService $service */
            $service = Yii::$container->get(CityService::class);

            $cities = $service->getMany(['request' => urldecode($query)]);

            if (!empty($cities['items'])) {
                return SelectizeCountriesMapper::getMappedData($cities['items']);
            }
        }

        return [];
    }

    /**
     * @param $query
     * @return array
     */
    public function actionDistricts($query)
    {
        /** @var DistrictService $service */
        $service = Yii::$container->get(DistrictService::class);

        $districts = $service->getMany(['request' => urldecode($query)]);

        if (!empty($districts['items'])) {
            return array_map(function($value) {
                return [
                    'label' => "{$value['title']} (город {$value['city']['title']})",
                    'id' => $value['id']
                ];
            }, $districts['items']);
        }

        return [];
    }

    /**
     * @param $request
     * @return array
     */
    public function actionUsers($request)
    {
        /** @var UserService $userService */
        $userService = Yii::$container->get(UserService::class);
        $users = $userService->getMany(['username' => $request], ['limit' => 10]);
        $users = SelectizeUserMapper::getMappedData($users['items']);

        return $users;
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderBuildingPhase($iterator)
    {
        return [
            'html' => $this->renderAjax('@backend/views/building/steps/phase-partial', [
                'model' => new BuildingPhaseForm(),
                'key' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderBuildingProgress($iterator)
    {
        return [
            'html' => $this->renderAjax('@backend/views/building/steps/progress-partial', [
                'model' => new BuildingProgressForm(),
                'key' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderBuildingSite($iterator)
    {
        return [
            'html' => $this->renderAjax('@backend/views/building/steps/site-partial', [
                'model' => new BuildingSiteForm(),
                'key' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderBuildingDocument($iterator)
    {
        return [
            'html' => $this->renderAjax('@backend/views/building/steps/document-partial', [
                'model' => new BuildingDocumentForm(),
                'key' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }

    /**
     * @param $district
     * @param $city_id
     * @return array
     */
    public function actionPolygonImport($district, $city_id)
    {
        $response = ['success' => false, 'message' => 'Полигон по заданным параметрам не найден'];
        if (!empty($district) && !empty($city_id)) {
            /* @var CityService $cityService*/
            $cityService = Yii::$container->get(CityService::class);
            $city = $cityService->getOne(['id' => $city_id]);
            if ($city !== null) {
                $query = [
                    $district,
                    $city['title'],
                    $city['country']['title'],
                ];
                $query = implode(', ', $query);
                /* @var OsmPolygonService $polygonService*/
                $polygonService = Yii::$container->get(OsmPolygonService::class);
                $result = $polygonService->getPolygon($query);
                if ($result) {
                    $response = ['success' => true, 'points' => $result];
                }
            }
        }

        return $response;
    }
}
