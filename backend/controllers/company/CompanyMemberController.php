<?php

namespace backend\controllers\company;

use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\dto\CompanyMemberDTO;
use common\forms\ar\CompanyMemberForm;
use common\models\CompanyMember;
use common\models\search\CompanyMemberSearch;
use common\repositories\sql\CompanyMemberRepository;
use common\services\entities\UserService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class CompanyMemberController
 * @package backend\controllers\company
 */
class CompanyMemberController extends BackEndController
{
    /**
     * @var CompanyMemberRepository
     */
    private $_companyMemberRepository;
    /**
     * @var UserService
     */
    private $_userService;

    /**
     * CompanyMemberController constructor.
     * @param string $id
     * @param Module $module
     * @param UserService $userService
     * @param CompanyMemberRepository $companyMemberRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, UserService $userService, CompanyMemberRepository $companyMemberRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_companyMemberRepository = $companyMemberRepository;
        $this->_userService = $userService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param $company_id
     * @return string
     */
    public function actionIndex($company_id)
    {
        $params = Yii::$app->request->queryParams;
        $search = new CompanyMemberSearch($this->_companyMemberRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['id' => SORT_DESC]
        ]);
        $companys = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $companys['items'],
            'totalCount' => $companys['pagination']->totalCount,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'companyId' => $company_id,
            'search' => $search
        ]);
    }

    /**
     * @param $company_id
     * @return mixed|string|Response
     */
    public function actionCreate($company_id)
    {
        $form = new CompanyMemberForm(['_companyMember' => new CompanyMember()]);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $input['CompanyMemberForm']['company_id'] = $company_id;
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $form->save();
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }
            }
        }

        return $this->renderAjax('form', [
            'companyMemberForm' => $form,
            'selectizeData' => [],
            'action' => ['/company/company-member/create', 'company_id' => $company_id],
        ]);
    }

    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $companyMember = $this->_companyMemberRepository->findOneByCriteria(['company_member.id' => $id]);
        $form = new CompanyMemberForm(['_companyMember' => $companyMember]);

        $companyMemberDTO = new CompanyMemberDTO($companyMember);
        $companyMemberData = $companyMemberDTO->getData();
        $form->prepareUpdate($companyMemberData);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $form->load($input);
            if (array_key_exists('ajax', $input)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($form);
            }

            if ($form->validate()) {
                $form->save();
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }

                return $this->goBack();
            }
        }

        $user = $this->_userService->getOne(['id' => $companyMemberData['user_id']]);
        $selectizeData = [$companyMemberData['user_id'] => "{$user['username']} (ID: {$user['id']}; Email: {$user['email']})"];

        return $this->renderAjax('form', [
            'companyMemberForm' => $form,
            'selectizeData' => $selectizeData,
            'action' => ['/company/company-member/update', 'id' => $id],
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var CompanyMember $member */
        $member = $this->_companyMemberRepository->findOneByCriteria(['id' => $id]);

        if ($member !== null) {
            $member->delete();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
