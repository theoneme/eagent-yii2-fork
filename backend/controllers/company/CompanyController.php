<?php

namespace backend\controllers\company;

use backend\forms\ar\CompanyForm;
use backend\models\search\CompanySearch;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\dto\CompanyDTO;
use common\forms\ar\composite\ContactForm;
use common\helpers\FormHelper;
use common\models\Company;
use common\repositories\sql\CompanyRepository;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class CompanyController
 * @package backend\controllers\company
 */
class CompanyController extends BackEndController
{
    /**
     * @var CompanyRepository
     */
    private $_companyRepository;

    /**
     * CompanyController constructor.
     * @param string $id
     * @param Module $module
     * @param CompanyRepository $companyRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, CompanyRepository $companyRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_companyRepository = $companyRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'render-contact'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            [
                'class' => ContentNegotiator::class,
                'only' => ['render-contact'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            [
                'class' => AjaxFilter::class,
                'only' => ['render-contact'],
            ]
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new CompanySearch($this->_companyRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['id' => SORT_DESC]
        ]);
        $companys = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $companys['items'],
            'totalCount' => $companys['pagination']->totalCount,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @return array|mixed|string|Response
     */
    public function actionCreate()
    {
        $companyForm = new CompanyForm(['_company' => new Company()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $companyForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($companyForm);
            }

            if ($companyForm->validate()) {
                $companyForm->bindData();
                $companyForm->save();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }

                return $this->goBack();
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_COMPANY);
        $companyForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'companyForm' => $companyForm,
            'action' => ['/company/company/create']
        ]);
    }

    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $companyForm = new CompanyForm();
        /** @var Company $company */
        $company = $this->_companyRepository->findOneByCriteria(['company.id' => $id]);
        $companyForm->_company = $company;
        $companyDTO = new CompanyDTO($company);
        $companyData = $companyDTO->getData();

        $companyForm->prepareUpdate($companyData);
        $input = Yii::$app->request->post();

        if (!empty($input)) {
            $companyForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($companyForm);
            }

            if ($companyForm->validate()) {
                $companyForm->bindData();
                $companyForm->save();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }

                return $this->goBack();
            }
        }

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_COMPANY);
        $companyForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'companyForm' => $companyForm,
            'action' => ['/company/company/update', 'id' => $id],
            'companyDTO' => $companyData
        ]);
    }

    /**
     * @param $iterator
     * @return array
     */
    public function actionRenderContact($iterator)
    {
        $contactForm = new ContactForm();

        return [
            'html' => $this->renderAjax('partial/contact', [
                'contactForm' => $contactForm,
                'iterator' => $iterator,
                'createForm' => true
            ]),
            'success' => true,
        ];
    }
}
