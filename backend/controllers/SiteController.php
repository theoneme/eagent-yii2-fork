<?php

namespace backend\controllers;

use common\controllers\BackEndController;
use common\interfaces\repositories\BuildingRepositoryInterface;
use common\interfaces\repositories\PropertyRepositoryInterface;
use common\interfaces\repositories\RequestRepositoryInterface;
use common\interfaces\repositories\UserRepositoryInterface;
use common\models\Property;
use common\models\Request;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;

/**
 * Class SiteController
 * @package backend\controllers
 */
class SiteController extends BackEndController
{
    /**
     * @var UserRepositoryInterface
     */
    private $_userRepository;
    /**
     * @var PropertyRepositoryInterface
     */
    private $_propertyRepository;
    /**
     * @var RequestRepositoryInterface
     */
    private $_requestRepository;
    /**
     * @var BuildingRepositoryInterface
     */
    private $_buildingRepository;

    /**
     * SiteController constructor.
     * @param string $id
     * @param Module $module
     * @param BuildingRepositoryInterface $buildingRepository
     * @param UserRepositoryInterface $userRepository
     * @param PropertyRepositoryInterface $propertyRepository
     * @param RequestRepositoryInterface $requestRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, BuildingRepositoryInterface $buildingRepository, UserRepositoryInterface $userRepository, PropertyRepositoryInterface $propertyRepository, RequestRepositoryInterface $requestRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->_userRepository = $userRepository;
        $this->_propertyRepository = $propertyRepository;
        $this->_requestRepository = $requestRepository;
        $this->_buildingRepository = $buildingRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'clear-cache'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $counts = [
            'totalUsers' => $this->_userRepository->select(['id'], true)->countByCriteria(['is_company' => false]),
            'totalProperties' => $this->_propertyRepository->select(['id'], true)->countByCriteria(['not', ['status' => Property::STATUS_DELETED]]),
            'totalRequests' => $this->_requestRepository->select(['id'], true)->countByCriteria(['not', ['status' => Request::STATUS_DELETED]]),
            'totalBuildings' => $this->_buildingRepository->select(['id'], true)->countByCriteria([]),
        ];

        return $this->render('index', [
            'counts' => $counts
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionClearCache()
    {
        Yii::$app->cache->flush();
        $folders = [
            Yii::getAlias('@backend') . '/runtime/cache/*',
            Yii::getAlias('@backend') . '/web/assets/*',
            Yii::getAlias('@frontend') . '/runtime/cache/*',
            Yii::getAlias('@frontend') . '/web/assets/*',
            Yii::getAlias('@console') . '/runtime/cache/*',
        ];
        foreach ($folders as $folder) {
            $files = glob($folder);
            foreach ($files as $file) {
                FileHelper::removeDirectory($file);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
