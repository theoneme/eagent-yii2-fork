<?php

namespace backend\controllers\location;

use backend\models\search\MicroDistrictSearch;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\MicroDistrictForm;
use common\forms\ar\composite\MetaForm;
use common\helpers\UtilityHelper;
use common\models\MicroDistrict;
use common\repositories\sql\MicroDistrictRepository;
use common\services\entities\MicroDistrictService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class MicroDistrictController
 * @package backend\controllers\location
 */
class MicroDistrictController extends BackEndController
{
    /**
     * @var MicroDistrictRepository
     */
    private $_microDistrictRepository;
    /**
     * @var MicroDistrictService
     */
    private $_microDistrictService;

    /**
     * MicroDistrictController constructor.
     * @param string $id
     * @param Module $module
     * @param MicroDistrictService $microDistrictService
     * @param MicroDistrictRepository $microDistrictRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, MicroDistrictService $microDistrictService, MicroDistrictRepository $microDistrictRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_microDistrictRepository = $microDistrictRepository;
        $this->_microDistrictService = $microDistrictService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new MicroDistrictSearch($this->_microDistrictRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $microDistrictForm = new MicroDistrictForm(['_microDistrict' => new MicroDistrict()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $microDistrictForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($microDistrictForm);
            }

            if ($microDistrictForm->validate()) {
                $microDistrictForm->bindData();
                $microDistrictForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $microDistrictForm,
            'action' => ['/location/micro-district/create']
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $microDistrictForm = new MicroDistrictForm();
        $microDistrict = $this->_microDistrictRepository->findOneByCriteria(['micro_district.id' => $id]);
        $microDistrictForm->_microDistrict = $microDistrict;
        $dto = $this->_microDistrictService->getOne(['micro_district.id' => $id]);

        $microDistrictForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $microDistrictForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($microDistrictForm);
            }

            if ($microDistrictForm->validate()) {
                $microDistrictForm->bindData();
                $microDistrictForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $microDistrictForm,
            'dto' => $dto,
            'action' => ['/location/micro-district/update', 'id' => $id]
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_microDistrictService->delete(['micro_district.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }
}
