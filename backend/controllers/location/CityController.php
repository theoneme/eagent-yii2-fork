<?php

namespace backend\controllers\location;

use backend\models\search\CitySearch;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\CityForm;
use common\forms\ar\composite\MetaForm;
use common\models\City;
use common\repositories\sql\CityRepository;
use common\services\entities\CityService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class CityController
 * @package backend\controllers\location
 */
class CityController extends BackEndController
{
    /**
     * @var CityRepository
     */
    private $_cityRepository;
    /**
     * @var CityService
     */
    private $_cityService;

    /**
     * CityController constructor.
     * @param string $id
     * @param Module $module
     * @param CityService $cityService
     * @param CityRepository $cityRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, CityService $cityService, CityRepository $cityRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_cityRepository = $cityRepository;
        $this->_cityService = $cityService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new CitySearch($this->_cityRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $cityForm = new CityForm();
        $city = $this->_cityRepository->findOneByCriteria(['city.id' => $id]);
        $cityForm->_city = $city;
        $dto = $this->_cityService->getOne(['city.id' => $id]);

        $cityForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $cityForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($cityForm);
            }

            if ($cityForm->validate()) {
                $cityForm->bindData();
                $cityForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'cityForm' => $cityForm,
            'metaForm' => new MetaForm(),
            'dto' => $dto,
            'action' => ['/location/city/update', 'id' => $id]
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $cityForm = new CityForm(['_city' => new City()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $cityForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($cityForm);
            }

            if ($cityForm->validate()) {
                $cityForm->bindData();
                $cityForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'cityForm' => $cityForm,
            'metaForm' => new MetaForm(),
            'action' => ['/location/city/create']
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_cityService->delete(['city.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }
}
