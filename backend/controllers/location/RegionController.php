<?php

namespace backend\controllers\location;

use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\RegionForm;
use common\models\Region;
use common\models\search\RegionSearch;
use common\repositories\sql\RegionRepository;
use common\services\entities\RegionService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class RegionController
 * @package backend\controllers\location
 */
class RegionController extends BackEndController
{
    /**
     * @var RegionRepository
     */
    private $_regionRepository;
    /**
     * @var RegionService
     */
    private $_regionService;

    /**
     * RegionController constructor.
     * @param string $id
     * @param Module $module
     * @param RegionService $regionService
     * @param RegionRepository $regionRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, RegionService $regionService, RegionRepository $regionRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_regionRepository = $regionRepository;
        $this->_regionService = $regionService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new RegionSearch($this->_regionRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $regionForm = new RegionForm();
        $region = $this->_regionRepository->findOneByCriteria(['region.id' => $id]);
        $regionForm->_region = $region;
        $dto = $this->_regionService->getOne(['region.id' => $id]);

        $regionForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $regionForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($regionForm);
            }

            if ($regionForm->validate()) {
                $regionForm->bindData();
                $regionForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'regionForm' => $regionForm,
            'metaForm' => new MetaForm(),
            'dto' => $dto,
            'action' => ['/location/region/update', 'id' => $id]
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $regionForm = new RegionForm(['_region' => new Region()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $regionForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($regionForm);
            }

            if ($regionForm->validate()) {
                $regionForm->bindData();
                $regionForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'regionForm' => $regionForm,
            'metaForm' => new MetaForm(),
            'action' => ['/location/region/create']
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_regionService->delete(['region.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }
}
