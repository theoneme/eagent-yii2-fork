<?php

namespace backend\controllers\location;

use backend\models\search\DistrictSearch;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\DistrictForm;
use common\forms\ar\composite\MetaForm;
use common\models\District;
use common\repositories\sql\DistrictRepository;
use common\services\entities\DistrictService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class DistrictController
 * @package backend\controllers\location
 */
class DistrictController extends BackEndController
{
    /**
     * @var DistrictRepository
     */
    private $_districtRepository;
    /**
     * @var DistrictService
     */
    private $_districtService;

    /**
     * DistrictController constructor.
     * @param string $id
     * @param Module $module
     * @param DistrictService $districtService
     * @param DistrictRepository $districtRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, DistrictService $districtService, DistrictRepository $districtRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_districtRepository = $districtRepository;
        $this->_districtService = $districtService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new DistrictSearch($this->_districtRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $districtForm = new DistrictForm(['_district' => new District()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $districtForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($districtForm);
            }

            if ($districtForm->validate()) {
                $districtForm->bindData();
                $districtForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $districtForm,
            'action' => ['/location/district/create']
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $districtForm = new DistrictForm();
        $district = $this->_districtRepository->findOneByCriteria(['district.id' => $id]);
        $districtForm->_district = $district;
        $dto = $this->_districtService->getOne(['district.id' => $id]);

        $districtForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $districtForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($districtForm);
            }

            if ($districtForm->validate()) {
                $districtForm->bindData();
                $districtForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'model' => $districtForm,
            'dto' => $dto,
            'action' => ['/location/district/update', 'id' => $id]
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_districtService->delete(['district.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }
}
