<?php

namespace backend\controllers\location;

use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\forms\ar\composite\MetaForm;
use common\forms\ar\CountryForm;
use common\models\Country;
use common\models\search\CountrySearch;
use common\repositories\sql\CountryRepository;
use common\services\entities\CountryService;
use Yii;
use yii\base\Module;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class CountryController
 * @package backend\controllers
 */
class CountryController extends BackEndController
{
    /**
     * @var CountryRepository
     */
    private $_countryRepository;
    /**
     * @var CountryService
     */
    private $_countryService;

    /**
     * CountryController constructor.
     * @param string $id
     * @param Module $module
     * @param CountryService $countryService
     * @param CountryRepository $countryRepository
     * @param array $config
     */
    public function __construct(string $id, Module $module, CountryService $countryService, CountryRepository $countryRepository, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_countryRepository = $countryRepository;
        $this->_countryService = $countryService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new CountrySearch($this->_countryRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => new Expression('id desc')
        ]);
        $items = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $items['items'],
            'totalCount' => $items['pagination']->totalCount ?? count($items['items']),
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string
     */
    public function actionUpdate($id)
    {
        $countryForm = new CountryForm();
        $country = $this->_countryRepository->findOneByCriteria(['country.id' => $id]);
        $countryForm->_country = $country;
        $dto = $this->_countryService->getOne(['country.id' => $id]);

        $countryForm->prepareUpdate($dto);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $countryForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($countryForm);
            }

            if ($countryForm->validate()) {
                $countryForm->bindData();
                $countryForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'countryForm' => $countryForm,
            'metaForm' => new MetaForm(),
            'action' => ['/location/country/update', 'id' => $id]
        ]);
    }

    /**
     * @return array|mixed|string
     * @throws \common\exceptions\ClassNotFoundException
     * @throws \common\exceptions\EntityNotCreatedException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionCreate()
    {
        $countryForm = new CountryForm(['_country' => new Country()]);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $countryForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($countryForm);
            }

            if ($countryForm->validate()) {
                $countryForm->bindData();
                $countryForm->save();

                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => true];
            }
        }

        return $this->renderAjax('form', [
            'countryForm' => $countryForm,
            'metaForm' => new MetaForm(),
            'action' => ['/location/country/create']
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        $this->_countryService->delete(['country.id' => $id]);

        return $this->redirect(Yii::$app->request->referrer);
    }
}
