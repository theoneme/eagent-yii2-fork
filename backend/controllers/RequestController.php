<?php

namespace backend\controllers;

use backend\forms\ar\RequestForm;
use backend\services\FormCategoryService;
use common\controllers\BackEndController;
use common\data\MyArrayDataProvider;
use common\helpers\DataHelper;
use common\helpers\FormHelper;
use common\models\Request;
use common\models\search\RequestSearch;
use common\repositories\sql\PropertyRepository;
use common\repositories\sql\RequestRepository;
use common\services\entities\PropertyService;
use common\services\entities\RequestService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class RequestController
 * @package backend\controllers
 */
class RequestController extends BackEndController
{
    /**
     * @var PropertyRepository
     */
    private $_requestRepository;
    /**
     * @var PropertyService
     */
    private $_requestService;
    /**
     * @var FormCategoryService
     */
    private $_formCategoryService;

    /**
     * RequestController constructor.
     * @param string $id
     * @param Module $module
     * @param RequestRepository $propertyRepository
     * @param FormCategoryService $formCategoryService
     * @param RequestService $propertyService
     * @param array $config
     */
    public function __construct(string $id, Module $module,
                                RequestRepository $propertyRepository,
                                FormCategoryService $formCategoryService,
                                RequestService $propertyService,
                                array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_requestRepository = $propertyRepository;
        $this->_requestService = $propertyService;
        $this->_formCategoryService = $formCategoryService;
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'create', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \ReflectionException
     * @throws \common\exceptions\RepositoryException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $search = new RequestSearch($this->_requestRepository, [
            'pagination' => true,
            'perPage' => 20,
            'orderBy' => ['status' => SORT_ASC, 'id' => SORT_DESC]
        ]);
        $properties = $search->search($params);

        $dataProvider = new MyArrayDataProvider([
            'data' => $properties['items'],
            'totalCount' => $properties['pagination']->totalCount,
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'search' => $search
        ]);
    }

    /**
     * @param $id
     * @return array|mixed|string|Response
     */
    public function actionUpdate($id)
    {
        $requestForm = new RequestForm();
        $property = $this->_requestRepository->findOneByCriteria(['request.id' => $id]);
        $requestForm->_request = $property;
        $propertyDTO = $this->_requestService->getOne(['request.id' => $id]);
        $propertyDTO['type'] = Yii::$app->request->get('type', $propertyDTO['type']);
        $propertyDTO['category_id'] = Yii::$app->request->get('category_id', $propertyDTO['category_id']);
        $requestForm->prepareUpdate($propertyDTO);

        $input = Yii::$app->request->post();
        if (!empty($input)) {
            $requestForm->load($input);
            if (array_key_exists('ajax', $input)) {
                return $this->validateAjax($requestForm);
            }

            if ($requestForm->validate()) {
                $requestForm->bindData();
                $requestForm->save();

                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => true];
                }

                return $this->goBack();
            }
        }

        $categoryData = $this->_formCategoryService->getData($requestForm->category->category_id);

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_REQUEST, $requestForm->category->category_id);
        $requestForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'requestForm' => $requestForm,
            'categories' => DataHelper::getCategories(),
            'subcategories' => $categoryData['subcategories'],
            'parentCategoryId' => $categoryData['parentCategoryId'],
            'action' => ['/request/update', 'id' => $id]
        ]);
    }

    /**
     * @return mixed|string
     */
    public function actionCreate()
    {
        $requestForm = new RequestForm();
        $requestForm->_request = new Request();
        $input = Yii::$app->request->post();
        $requestForm->load($input);

        if (array_key_exists('ajax', $input)) {
            return $this->validateAjax($requestForm);
        }

        if (!empty($input) && $requestForm->validate()) {
            $requestForm->bindData();
            $requestForm->save();
        } else {
            $requestForm->load([
                'CategoryForm' => [
                    'category_id' => (int)Yii::$app->request->get('category_id', 2)
                ],
                'RequestForm' => [
                    'type' => (int)Yii::$app->request->get('type', Request::TYPE_SALE)
                ]
            ]); // проинициализировать DynamicForm
        }

        $categoryData = $this->_formCategoryService->getData($requestForm->category->category_id);

        $formConfig = FormHelper::getFormConfig(FormHelper::ENTITY_REQUEST, $requestForm->category->category_id);
        $requestForm->buildLayout($formConfig);

        return $this->renderAjax('form', [
            'requestForm' => $requestForm,
            'categories' => DataHelper::getCategories(),
            'subcategories' => $categoryData['subcategories'],
            'parentCategoryId' => $categoryData['parentCategoryId'],
            'action' => ['/request/create']
        ]);
    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDelete($id)
    {
        /** @var Request $request */
        $request = $this->_requestRepository->findOneByCriteria(['property.id' => $id]);

        if ($request !== null) {
            $request->updateAttributes(['status' => Request::STATUS_DELETED, 'updated_at' => time()]);
        }

        return $this->goBack(Yii::$app->request->referrer);
    }
}
