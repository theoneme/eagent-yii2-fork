<?php

namespace backend\services;

use Exception;
use Yii;
use yii\httpclient\Client;

/**
 * Class OsmPolygonService
 * @package backend\services
 */
class OsmPolygonService
{
    private $_headers = [
        "scheme" => "https",
        "accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "accept-language" => "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
        "cache-control" => "max-age=0",
        "upgrade-insecure-requests" => "1",
        'pragma' => 'no-cache',
        "user-agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.122 Safari/537.36 Vivaldi/2.3.1440.61",
    ];

    /**
     * @param $query
     * @return bool|mixed
     */
    public function getPolygon($query)
    {

        $client = new Client(['responseConfig' => ['format' => Client::FORMAT_JSON]]);
        $result = false;
        try {
            $data = $client->get('https://nominatim.openstreetmap.org/search.php', [
                'q' => $query,
                'polygon_geojson' => 1,
                'format' => 'json'
            ], $this->_headers)->send();
            $isOk = $data->isOk;
            $data = $data->getData();
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            return $result;
        }
        if ($isOk && !empty($data) && is_array($data)) {
            foreach ($data as $item) {
                if ($item['geojson']['type'] === 'Polygon' && !empty($item['geojson']['coordinates'][0])) {
                    $result = array_map(function($var){ return ['lat' => $var[1], 'lng' => $var[0]];}, $item['geojson']['coordinates'][0]);
                    break;
                }
            }
        }
        return $result;
    }
}