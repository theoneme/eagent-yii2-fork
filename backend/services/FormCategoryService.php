<?php
/**
 * Created by PhpStorm.
 * User: Devour
 * Date: 28.08.2018
 * Time: 10:52
 */

namespace backend\services;

use common\helpers\DataHelper;
use common\interfaces\repositories\CategoryRepositoryInterface;
use common\models\Category;

/**
 * Class FormCategoryTreeService
 * @package common\services
 */
class FormCategoryService
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $_categoryRepository;

    /**
     * FormCategoryTreeService constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->_categoryRepository = $categoryRepository;
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getData($categoryId)
    {
        /** @var Category $currentCategory */
        $currentCategory = $this->_categoryRepository->findOneByCriteria(['id' => $categoryId]);
        $parentCategoryId = $currentCategory->id;
        $subcategories = [];
        if ($currentCategory) {
            if ($currentCategory->isLeaf()) {
                $parentCategory = $this->_categoryRepository->findOneByCriteria(['and',
                    ['<', 'lft', $currentCategory['lft']],
                    ['>', 'rgt', $currentCategory['rgt']],
                    ['lvl' => $currentCategory['lvl'] - 1]
                ]);
                $parentCategoryId = $parentCategory->id;
                $subcategories = DataHelper::getCategoryChildren($parentCategoryId);
            } else {
                $subcategories = DataHelper::getCategoryChildren($currentCategory->id);
            }
        }

        return [
            'subcategories' => $subcategories,
            'parentCategoryId' => $parentCategoryId
        ];
    }
}